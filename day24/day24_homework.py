#!/usr/bin/env python        	
# -*- coding:utf-8 -*-	 
# @Project  : s21day24
# @Author : Jack Deng
# @Time   :  2019-04-29 19:39
# @File   : day24_homework.py
import os
import sys

# 1.1计算单层目录文件夹大小
"""
val = os.path.abspath(r'xxxxx目录')
result = os.listdir(val)
file_size = 0
for item in result:
    path = os.path.join(val,item)
    file_size += os.path.getsize(path)
print(file_size)
"""

# 1.2计算嵌套文件夹的大小
"""
val2 = os.path.abspath(r'嵌套目录')
result = os.walk(val2)
file_size = 0
for a,b,c in result:
    for item in c:
        path = os.path.join(a,item)
        file_size += os.path.getsize(path)
print(file_size)
"""

# 2.拼手气红包
import random

money_list = []
def red_bag(cash, person):
    """分配红包
    红包最小0.01元,最大cash-(person+*0.01)
    """
    t = cash/person*2
    i = 1
    while i < person :
        n = random.uniform(0,t)
        i += 1
        money_list.append(n)
    t = cash - sum(money_list)
    money_list.append(t)
    print(money_list,sum(money_list))


red_bag(100,9)






def run():
    """
    主程序
    :return:
    """
    while True:
        cash = input('请输入红包金额(输入N退出)')
        if cash.upper() == 'N': break
        person = input('请输入红包数量')
        if any([not cash.isdecimal(), not person.isdecimal()]):
            print('输入错误,请重新输入')
            continue
        if int(person) < 1 or int(cash) < 0:
            print('红包金额或数量输入错误')
            continue
        val = red_bag(int(cash), int(person))
        print(val)


# 3.校验两个文件的一致性(使用字节读取大文件,避免编码问题)
import hashlib
import time


def func(a1, a2):
    hash = hashlib.md5()
    hash2 = hashlib.md5()  # 调用2个对象
    if os.path.getsize(a1) != os.path.getsize(a2):  # 先判断文件大小是否相等
        print('2者文件不一致')
    else:
        with open(a1, 'rb') as file1, open(a2, 'rb') as file2:
            file_size = os.path.getsize(a1)  # 确定读取文件的大小
            read_size = 0
            while read_size < file_size:
                row1 = file1.read(1024 * 1024 * 5)  # 每次读1024*1024*5字节,即5mb
                row2 = file2.read(1024 * 1024 * 5)
                read_size += len(row1)
                time.sleep(1)
                hash.update(row1)
                hash2.update(row2)
        if hash.hexdigest() == hash2.hexdigest():
            print('2者文件一致md5值都为:', hash.hexdigest())
            return
        print('2者文件不一致,md5值分别为:', hash.hexdigest(), hash2.hexdigest())

# func('ABC.mp4','ABC - 副本.mp4')

# 4.
