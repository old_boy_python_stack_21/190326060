# python day 24

#### 1.loggin模块

1.作用

- 记录日志的
- 用户 ：查看操作记录
- 程序员 : 
  - 统计用
  - 故障排除
  - 记录错误,做代码优化

2.基本格式logging.basicConfig()

```python
import logging

logging.basicConfig(
    filename='cmdb1.log',
    format='%(asctime)s - %(name)s - %(levelname)s -%(module)s:  %(message)s',
    datefmt='%Y-%m-%d %H:%M:%S %p',
    level=logging.ERROR
)
 #  默认的,无法更改编码,win编码不同乱码,不推荐
    
logging.error(msg,exc_info=True)  # exc_info=True,打印错误日志是把栈信息打印出来
# 使用方便
# 不能实现 编码问题；不能同时向文件和屏幕上输出
# logging.debug,logging.warning

```

3.了解logging本质

```python
#复杂
    # 创建一个logger对象
    # 创建一个文件操作符
    # 创建一个屏幕操作符
    # 创建一个格式

    # 给logger对象绑定 文件操作符
    # 给logger对象绑定 屏幕操作符
    # 给文件操作符 设定格式
    # 给屏幕操作符 设定格式
    
    # 用logger对象来操作

import logging

logger = logging.Logger('邓益新',level=10)
# logger = logging.getLogger('boss')    ,无法设置level
fh = logging.FileHandler('log.log','a',encoding = 'utf-8')
sh = logging.StreamHandler()

formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s',datefmt = '%Y-%m-%d %H-%M-%S',)
fh.setFormatter(formatter)
sh.setFormatter(formatter)

logger.addHandler(fh)
logger.addHandler(sh)

logger.warning('message')
```

4.推荐方法

```python
import logging

file_handler = logging.FileHandler(filename='x3.log', mode='a', encoding='utf-8',)
logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s -%(module)s:  %(message)s',
    datefmt='%Y-%m-%d %H:%M:%S %p',
    handlers=[file_handler,],
    level=logging.ERROR
)

logging.error('你好')
```

5.日志分割模式

```python
import logging
from logging import handlers

file_handler = handlers.TimedRotatingFileHandler(filename='x3.log', when='s', interval=5, encoding='utf-8')
logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s -%(module)s:  %(message)s',
    datefmt='%Y-%m-%d %H:%M:%S %p',
    handlers=[file_handler,],
    level=logging.ERROR
)

logging.error('你好',exc_info = True)

# when = ''取 S M H D W,不区分大小写
```

#### 2.collections 模块

```python
# 有序字典
OrderDict()

# 字典的另一种写法
dict([(1,2),(3,4),(5,6)])   # dict化 列表套元组

# namedtuple,顾名思义,可命名元组
Course = namedtuple('Course',['name','price','teacher'])
python = Course('python',19800,'alex')
print(python)
print(python.name)
print(python.price)
# 创建一个类，这个类没有方法，所有属性的值都不能修改

# deque 双端队列

# defaultDict 默认字典，可以给字典的value设置一个默认值


```

#### 3.os和sys的补充

```python
os.path.getsize('pack')        # 计算文件大小
sys.modules # 存储了当前程序中用到的所有模块，反射本文件中的内容
sys.modules[__name__]---->指向本文件
# '__main__': <module '__main__' from 'D:/code/day24/1.内容回顾.py'>
```

#### 4. hashlib摘要模块

1. 加密md5/sha233等加密算法

2. 密文验证

3. 验证文件的一致性hashlib

4. 

   ```python
   import hashlib
   hash = hashib.md5()
   hash.update()   # 无数次update,把大文件分割update进去即可.
   t = hash.hexdigest()
   print(t)
   
   
   # md5.update('hello'.encode())
   # md5.update(',wusir'.encode())
   # 46879507bf541e351209c0cd56ac160e
   # 46879507bf541e351209c0cd56ac160e  分批次update进去,最终结果相同
   ```

#### 5.json和pickle

```python
# 序列化 把其他数据类型转换成 str/bytes类型-->dump/dumps
# 反序列化 str/bytes类型 转换回去         -->load/loads
   所有编程语言  json 所有的语言都支持
    # json格式 ：
        # 1.所有的字符串都是双引号
        # 2.最外层只能是列表或者字典
        # 3.只支持 int float str list dict bool
        # 4.存在字典字典的key只能是str
        # 5.不能连续load多次
    # pickle 只支持python语言
        #  1.几乎所有的数据类型都可以写到文件中
        #  2.支持连续load多次
```

#### 6.random

```python
import random  
val33 = randrom.unifom(1,5)  # q取任意小数
val = random.choice(序列)   # -->从序列中随机抽取一个元素
val22 = ramdom.sample(序列,n)   # -->从序列中随机抽取n个元素
shuffle(序列)   # 打乱,洗牌

```

#### 7.shuttil

```python
# shutil 模块
        # import shutil
        # shutil.make_archive()  压缩文件
        # shutil.unpack_archive() 解压文件
        # shutil.rmtree() 删除目录
        # shutil.move()  重命名 移动文件
    # getpass 在命令行密文显示输入的内容
    # copy.deepcopy
    # importlib
        # importlibimport importlib
        # importlib.import_module('模块名')
        # os = __import__('os')
        # print(os.path.isdir('D:\code\day24\pack'))
        # print(os.path.isfile('D:\code\day24\pack'))
    # functools
        # reduce(add,iterable)
```

#### 8.面向对象

```python
# 面向对象
    # 基础概念
        # 什么是类 : 具有相同方法和属性的一类事物
        # 什么是对象、实例 : 一个拥有具体属性值和动作的具体个体
        # 实例化 ：从一个类 得到一个具体对象的过程
    # 组合
        # 一个类的对象作为另一个类对象的实例变量  # 组合/嵌套

    # 三大特性
        # 继承
                # 所有的查找名字（调用方法和属性）都是先找自己的，自己没有找父类
                # 如果自己和父类都有，希望自己和父类都调用，super()/指定类名直接调
            # 父类、基类、超类
            # 子类、派生类
            # 单继承 ：字类可以使用父类的方法
            # 多继承
                # 查找顺序
                    # 深度优先
                    # 广度优先
        # 多态
            # 一个类表现出来的多种状态 --> 多个类表现出相似的状态
                # vip_user
                    # 支付
                    # 浏览商品
                # svip_user
                    # 支付
                    # 浏览商品
            # 鸭子类型
                # vip_user  svip_user
                # list 和 tuple
        # 封装
            # 广义的封装 ：类中的成员
            # 狭义的封装 ：私有成员
                # __名字
                # 只能在类的内部使用，既不能在类的外部调用，也不能在子类中使用
                # _类名__名字


    # 类成员
        # 类变量
        # 绑定方法
        # 特殊成员
            # 类方法 classmethod
            # 静态方法 staticmethod
            # 属性 property
    # 双下方法/魔术方法/内置方法
        # __str__
        # __new__  构造方法
        # __init__ 初始化方法
        # __call__ 源码中很容易写这个用法
        # __enter__ __exit__  with上下文管理
        # __getitem__
        # __settitem__
        # __delitem__
        # __add__
        # __iter__
        # __dict__

    # 相关内置函数
        # isinstance 对象和类
        # issubclass 类和类
        # type 类型 类 = type(对象)
        # super 遵循mro顺序查找上一个类的

    # 新式类和经典类
        # py2 继承object就是新式类
        #     默认是经典类
        # py3 都是新式类，默认继承object

        # 新式类
            # 继承object
            # 支持super
            # 多继承 广度优先C3算法
            # mro方法
        # 经典类
            # py2中不继承object
            # 没有super语法
            # 多继承 深度优先
            # 没有mro方法
```