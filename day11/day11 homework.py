#!/usr/bin/env python
# -*- coding:utf-8 -*-

# 1.列举str、list、dict、set中的常用方法（每种至少5个），并标注是否有返回值。
"""str  upper大写  有返回值
        lower小写  有返回值
        split()切割  ,有返回值
        strip(),去除2端空格,有返回值
        replace()  替换 , 有返回值
        isdigit()  判断是否数字 ,有返回值
        startswith() /endswith()  判断开头结尾字符,有返回值
        join() 把...做连接符,有返回值------->>连接起来的字符串



list    append()添加元素,无返回值
        insert()添加元素,无返回值
        remove()删除元素,无返回值
        pop(),删除元素,有返回值,--------->被删除的元素
        clear(),清除所有元素,无返回值
        reverse()反转,无返回值
        sort()从小到大排列元素.无返回值

tuple 元组无方法.

dict    .keys()获取所有键,有返回值
        .values()获取所有值,有返回值
        .items()获取所有键值对,有返回值
        .get()获取键对应的值,有返回值,不存在键则返回None/自定义内容.  <----->和字典的索引区分开,不存咋=在键则报错
        .updata()字典添加键值对,无返回值
        .pop()删除对应键值对,有返回值,---->返回对应的值,不存在则报错.

set     add()添加集合单个元素,无返回值
        update()添加集合元素,无返回值
        discard()删除集合元素,无返回值
        intersecion()求交集,有返回值
        union()并集,有返回值
        difference()求差集,有返回值

"""


# 2.列举你了解的常见内置函数 【面试题】。
"""
输入输出print/input
强制转换:int/srt/bool/list/tuple/dict/set/folat浮点型转换
数学变换:abs绝对值/max最大值/min最小值/divmod得商和余数/sum求和
其他:len求长度/open打开文件/range/
进制相互转换:bin转换二进制/oct转换八进制/hex转换十六进制

"""


# 3.看代码分析结果
"""
def func(arg):
    return arg.replace('苍老师', '***')

def run():
    msg = "Alex的女朋友苍老师和大家都是好朋友"
    result = func(msg)
    print(result)

run()   # "Alex的女朋友***和大家都是好朋友"

"""


# 4.看代码分析结果
"""
def func(arg):
    return arg.replace('苍老师', '***')

def run():
    msg = "Alex的女朋友苍老师和大家都是好朋友"
    result = func(msg)
    print(result)

data = run()         # 调用函数run,打印 "Alex的女朋友***和大家都是好朋友",返回值为None
print(data)        # 打印 None
"""




# 5.看代码分析结果
"""
DATA_LIST = []

def func(arg):
    return DATA_LIST.insert(0, arg)

data = func('绕不死你')  # 调用函数func,
print(data)         #   None
print(DATA_LIST)   #打印 ['绕不死你',]
"""


# 6.看代码分析结果
"""
def func():
    print('你好呀')
    return '好你妹呀'

func_list = [func, func, func]

for item in func_list:
    val = item()     # 循环运行func,返回值为'好你妹呀' ,打印'你好呀'
    print(val)      # 打印返回值'好你妹呀'
############  打印 '你好呀' ,'好你妹呀'三次
"""



# 7.看代码分析结果
"""
def func():
    print('你好呀')
    return '好你妹呀'

func_list = [func, func, func]

for i in range(len(func_list)):
    val = func_list[i]()   # 循环调用函数func,返回值为'好你妹呀'
    print(val)   #打印返回值为'好你妹呀'
############  打印 '你好呀' ,'好你妹呀'三次
"""



# 8.看代码写结果
"""
tips = "啦啦啦啦"   #定义全级作用域:  tips = "啦啦啦啦"

def func():
    print(tips)
    return '好你妹呀'

func_list = [func, func, func]

tips = '你好不好'     #定义全级作用域:  tips = '你好不好',tips重新赋值
for i in range(len(func_list)):
    val = func_list[i]()   # 循环运行func,每运行一次,打印一次'你好不好'
    print(val)          # 打印返回值:'好你妹呀'

############  打印'你好不好' ,'好你妹呀'三次
"""


# 9.看代码写结果
"""
def func():
    return '烧饼'   # 返回值为'烧饼'

def bar():
    return '豆饼'   #返回值为  '豆饼'

def base(a1, a2):
    return a1() + a2()    # # 返回值为'烧饼豆饼''

result = base(func, bar)
print(result)    # # 打印返回值为'烧饼豆饼''
"""


# 10.看代码写结果
"""
def func():
    return '烧饼'

def bar():
    return '豆饼'

def base(a1, a2):
    return a1 + a2

result = base(func(), bar())  # ,<----->等价于base('烧饼', '豆饼')
print(result)   #输出打印  '烧饼豆饼'
"""


# 11.看代码写结果
"""
v1 = lambda: 100
print(v1())    # 打印返回值100

v2 = lambda vals: max(vals) + min(vals)
print(v2([11, 22, 33, 44, 55]))    # 返回打印11+55的值,即66

v3 = lambda vals: '大' if max(vals) > 5 else '小'
print(v3([1, 2, 3, 4]))          # 打印v3函数的返回值"小"
"""


# 12.看代码写结果
"""
def func():
    num = 10
    v4 = [lambda: num + 10, lambda: num + 100, lambda: num + 100, ]
    for item in v4:
        print(item())   #对列表循环函数名,打印函数的返回值.即20,110,110

func()
"""



# 13.看代码写结果
"""
for item in range(10):
    print(item)   # 循环打印0-9

print(item)    #打印 9
"""


# 14.看代码写结果
"""
def func():
    for item in range(10):
        pass
    print(item)

func()   # 打印 9
"""



# 15.看代码写结果
"""
item = '老男孩'
def func():
    item = 'alex'
    def inner():
        print(item)
    for item in range(10):
        pass
    inner()

func()   # 调用函数func(),在func函数作用域有变量名item=  'alex'
        # 在func作用域,经过10次循环,item = 9.调用inner()函数,输出打印 9
"""


# 16.看代码写结果【新浪微博面试题】
"""
def func():
    for num in range(10):
        pass
    v4 = [lambda: num + 10, lambda: num + 100, lambda: num + 100, ]
    result1 = v4[1]()
    result2 = v4[2]()
    print(result1, result2)

func()  #调用函数,经过循环结束,num = 9 ,输出打印:109,109
"""


# 17.通过代码实现如下转换

# 二进制转换成十进制：v = '0b1111011'
print(int('0b1111011',base=2))
# 十进制转换成二进制：v = 18
print(bin(18))
# 八进制转换成十进制：v = '011'
print(int('011',base=8))
# 十进制转换成八进制：v = 30
print(oct(30))
# 十六进制转换成十进制：v = '0x12'
print(int('0x12',base=16))
# 十进制转换成十六进制：v = 87
print(hex(87))


# 18.请编写一个函数实现将IP地址转换成一个整数。【面试题】
"""
如 10.3.9.12转换规则为二进制：
10     00001010
3      00000011
9      00001001
12     00001100
再将以上二进制拼接起来计算十进制结果：00001010 00000011 00001001 00001100 = ？
"""
"""
ip = '10.3.9.12'  #假设ip地址为这
ip_list = ip.split('.')
sum = ''
for item in ip_list:
    item = item.strip()
    item = bin(int(item))
    item = item.lstrip('0b')
    item = '0'*(8-len(item))+item
    sum += item
sum = int(sum,base=2)
print(sum)

"""
