
"""
1. 操作系统的作用?
答：调用电脑底层硬件，协同完成用户交付电脑的活动。
2. 列举你听过的操作系统及区别？
答：windows，win7 win8 win10 win server ，使用最广泛，用于办公和娱乐。
    linux，完全免费，占用内存小，系统文件小，运行速率快。
            centos系统，图形化界面较差。ubunant系统，图形界面较好。redhat，企业级，商用。
    mac ， 苹果系统，办公和开发都很好。
3. 列举你了解的编码及他们之间的区别？
答：ascii码，英文和标点符号，1字节表示1字符。
    unicode码，万国码，4字节表示1字符，能表示所有语言，目前才用到21位。
    utf-8码，对unicode码的压缩，最少1字节表示，中文用3字节表示字符。
4. 列举你了解的Python2和Python3的区别？
    答：
    1print ''   , print('')
    2.raw_input('')     ,input('')
    3.ascii码，utf-8码  py2  # -*- coding:utf-8 -*-
    4.py2有int和long ， py只有int
    5.py2整型除法只保留整数位，py3保留全部。  py2  from __future__ import division
5. 你了解的python都有那些数据类型？
    答：int str  bool  long
"""
# 6.补充代码，实现以下功能。

# value = 'alex"烧饼'
# print(value)  # 要求输出  alex"烧饼
# 7. 用print打印出下面内容：
#        ⽂能提笔安天下,
#        武能上⻢定乾坤.
#        ⼼存谋略何⼈胜,
#        古今英雄唯是君。
# print("""⽂能提笔安天下,
#        武能上⻢定乾坤.
#        ⼼存谋略何⼈胜,
#        古今英雄唯是君。""")

# 8. 变量名的命名规范和建议？
# 答：1.由字母数字下划线组成
#     2.数字不能用作开头
#     3.避开py的关键字
#     4.见名知意  单词间下划线连接。
# 9. 如下那个变量名是正确的？
#        name = '武沛齐'   正确
#        _ = 'alex'        正确
#        _9 = "老男孩"     正确
#        9name = "景女神"  错误   数字开头
#        oldboy(edu = 666  错误  括号
# 10. 简述你了解if条件语句的基本结构。

"""
if True:
    print(1)

if 1>0:
    print(2)
else:
    print(3)


if 1>0:
    print(4)
elif False:
    print(5)

if 1>0:
    pass
else:
    print(6)
"""
# 11. 设定一个理想数字比如：66，让用户输入数字，如果比66大，则显示猜测的结果大了；如果比66小，则显示猜测的结果小了;只有等于66，显示猜测结果正确。
"""
count = input("请输入一个理想数字：")
if int(count) > 66:
    print('猜测的结果大了')
elif int(count) < 66:
    print('猜测的结果小了')
else:
    print('猜测的结果正确')
"""

# 12. 提⽰⽤户输入⿇花藤. 判断⽤户输入的对不对。如果对, 提⽰真聪明, 如果不对, 提⽰你 是傻逼么。

"""count = input("请输入一个腾讯公司老板的别称：")
if count == '⿇花藤':
    print('真聪明')
else:
    print('你是sb吗？')
"""

#1. 猜数字，设定一个理想数字比如：66，让用户输入数字，如果比66大，则显示猜测的结果大了；如果比66小，则显示猜测的结果小了;只有等于66，显示猜测结果正确，然后退出循环。

"""
while True:
    count = input("请输入一个理想数字：")
    if int(count) > 66:
        print('猜测的结果大了')
    elif int(count) < 66:
        print('猜测的结果小了')
    else:
        print('猜测的结果正确')
        break
"""

# 2. 在上一题的基础，设置：给用户三次猜测机会，如果三次之内猜测对了，则显示猜测正确，退出循环，如果三次之内没有猜测正确，则自动退出循环，并显示‘大笨蛋’。
"""
i = 1
while i <= 3:
    count = input("请输入一个理想数字：")
    if int(count) == 66:
        print('猜测的结果正确')
        break
    elif int(count) < 66:
        print('猜测的结果小了')
    else:
        print('猜测的结果大了')
    i += 1
else:
    print('大笨蛋')
"""

# 3. 使用两种方法实现输出 1 2 3 4 5 6   8 9 10 。
"""
for i in range(1,11):
    if i == 7:
        continue
    else:
        print(i)

"""
# 第二种
"""
for i in range(1,11):
    if i == 7:
        i += 1
    print(i)
"""

# 4. 求1-100的所有数的和
"""
s = 0
for i in range(1,101):
    s += i
print(s)
"""

# 5. 输出 1-100 内的所有奇数
"""
for i in range(1,101):
    if i%2 == 1:
        print(i)
"""

#6. 输出 1-100 内的所有偶数
"""
for i in range(1,101):
    if i%2 == 0:
        print(i)
"""

# 7. 求1-2+3-4+5 ... 99的所有数的和
"""
s = 0
for i in range(1,100):
    if i%2 == 1:
        s += i
    else:
        s -= i
print(s)
"""

# 8. ⽤户登陆（三次输错机会）且每次输错误时显示剩余错误次数（提示：使⽤字符串格式化）
"""
t=3
for i in range(0,3):
    name = input('用户名;')
    paw = input('密码：')
    if name == "deng" and paw == '123':
        print('欢迎登陆')
        break
    else:
        t -= 1
        print('剩余错误次数为%d次' %(t,))
"""

# 9. 简述ASCII、Unicode、utf-8编码
# 答： ascii ，1字节表1字符，表英文和标点符号
#     unicode，万国码，4字节表1字符，能表示所有语言，
#     utf-8，对unicode码的压缩，最少1字节表示1字符，最多4字节表示1字符，中文用3字节表示

# 10. 1 byte述位和字节的关系？
# 8bit = 1byte

# 11. 猜年龄游戏要求：允许用户最多尝试3次，3次都没猜对的话，就直接退出，如果猜对了，打印恭喜信息并退出
"""
for i in range(0,3):
    age = int(input("请输入年龄："))
    if age == 25:
        print('恭喜猜对了')
        break
    else:
        print('猜错了,请重新输入')
"""

# 12. 猜年龄游戏升级版
#     要求：允许用户最多尝试3次，每尝试3次后，如果还没猜对，就问用户是否还想继续玩，如果回答Y，就继续让其猜3次，以此往复，如果回答N，就退出程序，如何猜对了，就直接退出。
"""
count = 1
while count <= 3:
    age = int(input("请输入年龄："))
    if age == 25:
        print('恭喜猜对了')
        break
    else:
        print('猜错了,请重新输入')
        count += 1
    if count == 4:
        question = input('是否还想继续玩？ Y or N :')
        if  question.upper() == 'Y':
            count = 1
        else:
            break
"""

# 13. 判断下列逻辑语句的True,False
#     - 1 > 1 or 3 < 4 or 4 > 5 and 2 > 1 and 9 > 8 or 7 < 6
#     - not 2 > 1 and 3 < 4 or 4 > 5 and 2 > 1 and 9 > 8 or 7 < 6
# 14. 求出下列逻辑语句的值。
#     - 8 or 3 and 4 or 2 and 0 or 9 and 7
#     - 0 or 2 and 3 and 4 or 6 and 0 or 3
# 15. 下列结果是什么？
#     - 6 or 2 > 1
#     - 3 or 2 > 1
#     - 0 or 5 < 4
#     - 5 < 4 or 3
#     - 2 > 1 or 6
#     - 3 and 2 > 1
#     - 0 and 3 > 1
#     - 2 > 1 and 3
#     - 3 > 1 and 0
#     - 3 > 1 and 2 or 2 < 3 and 3 and 4 or 3 >



"""
# 1. 有变量name = "aleX leNb " 完成如下操作：
#    - 移除 name 变量对应的值两边的空格,并输出处理结果
name = "aleX leNb "
print(name.strip())
#    - 判断 name 变量是否以 "al" 开头,并输出结果（用切片）
if name.startswith('al'):
    print('字符以al开头')
else:
    print('字符是不以al开头')
#    - 判断name变量是否以"Nb"结尾,并输出结果（用切片）
if name.endswith('Nd'):
    print('字符是以Nb结尾的')
else:
    print('字符是不以Nb结尾的')
#    - 将 name 变量对应的值中的 所有的"l" 替换为 "p",并输出结果
print(name.replace('l','p'))
#    - 将name变量对应的值中的第一个"l"替换成"p",并输出结果
print(name.replace('l','p',1))
#    - 将 name 变量对应的值根据 所有的"l" 分割,并输出结果
print(name.split('l'))
#    - 将name变量对应的值根据第一个"l"分割,并输出结果
print(name.split('l',1))
#    - 将 name 变量对应的值变大写,并输出结果
print(name.upper())
#    - 将 name 变量对应的值变小写,并输出结果
print(name.lower())
#    - 请输出 name 变量对应的值的第 2 个字符?
print(name[1])
#    - 请输出 name 变量对应的值的前 3 个字符?
print(name[:3])
#    - 请输出 name 变量对应的值的后 2 个字符?
print(name[-2:])

# 2. 有字符串s = "123a4b5c"

#    - 通过对s切片形成新的字符串 "123"

s = "123a4b5c"
print(s[0:3])
# #    - 通过对s切片形成新的字符串 "a4b"
print(s[3:6])
# #    - 通过对s切片形成字符串s5,s5 = "c"
print(s[-1])
print(s[7])
#    - 通过对s切片形成字符串s6,s6 = "ba2"
s = "123a4b5c"
print(s[5:0:-2])
"""

# 3. 使用while循环字符串 s="asdfer" 中每个元素。
"""
s="asdfer"
t = 0
while t < len(s):
    print(s[t])
    t += 1
    if t == len(s):
        t = 0
"""
# 4. 使用while循环对s="321"进行循环，打印的内容依次是："倒计时3秒"，"倒计时2秒"，"倒计时1秒"，"出发！"。
"""
s="321"
t = 0
while t < len(s):
    print('倒计时%s秒' %(s[t]))
    t += 1
    if t == len(s):
        print('出发')
        t = 0
"""
# 5. 实现一个整数加法计算器(两个数相加)：
#    如：content = input("请输入内容:") 用户输入：5+9或5+ 9或5 + 9（含空白），然后进行分割转换最终进行整数的计算得到结果。
"""
content = input("请输入两个数相加:")
num = content.split('+')
print(int(num[0])+int(num[1]))
"""
# 6. 计算用户输入的内容中有几个 h 字符？
#    如：content = input("请输入内容：")   # 如fhdal234slfh98769fjdla
"""
content = input("请输入内容：")
s = 0
for i in content:
    if i == 'h':
        s += 1
print('输入的内容中有%d个 h 字符' %(s))
"""

# 7. 计算用户输入的内容中有几个 h 或 H  字符？
#    如：content = input("请输入内容：")   # 如fhdal234slfH9H769fjdla
"""
content = input("请输入内容：")
s = 0
for i in content:
    if i.upper() == 'H':
        s += 1
print('输入的内容中有%d个 h or H 字符' %(num))
"""
# 8. 使用while循环分别正向和反向对字符串 message = "伤情最是晚凉天，憔悴厮人不堪言。" 进行打印。
"""
message = "伤情最是晚凉天，憔悴厮人不堪言。"
t = 0
while t < len(message):
    print(message[t])
    t += 1
    if t == len(message):
        t = 0
        while t < len(message):
            print(message[::-1][t])
            t += 1
    if t == len(message):
        t = 0
        continue
"""
# 9. 获取用户输入的内容中 前4个字符中 有几个 A ？
#    如：content = input("请输入内容：")   # 如fAdal234slfH9H769fjdla
"""
content = input("请输入内容：")
s = 0
for i in content:
    if i.upper() == 'A':
        s += 1
print('输入的内容中有%d个 A 字符' %(s))
"""

# 10. 获取用户输入的内容，并计算前四位"l"出现几次,并输出结果。
"""
content = input("请输入内容：")
s = 0
for i in range(0,5):
    if content[i] == 'l':
        s += 1
print('输入的内容前四位有%d个 l 字符' %(s))
"""

# 11. 获取用户两次输入的内容，并将所有的数据获取并进行相加，如：
"""
s1 = input('请输入内容：' )
s2 = input('请输入内容：' )
t = 0
num1 = ''
num2 = ''
for i in s1:
    if i.isdigit():
        num1 += i 
for i in s2:
    if i.isdigit():
        num2 += i    
print(int(num1)+int(num2))
"""



###第四天作业
# 1. 简述解释性语言和编译型语言的区别？
# 答：解释性语言:运行代码一行一行向下运行，边解释边运行，这样在调试语言时，可以很快速的发现语法错误，方便纠正错误。python、PHP、ruby
#     编译型语言：编译器编译成一个文件，然后交予计算机执行。不便修改。c语言，c++、java、
# 2. 列举你了解的Python的数据类型？
# 答：int整型、str字符串、bool布尔类型、long长整型、list/tuple/set/dict.

#3. 写代码，有如下列表，按照要求实现每一个功能。
li = ["alex", "WuSir", "ritian", "barry", "wenzhou"]
#   - 计算列表的长度并输出
"""
print(len(li))
#   - 请通过步长获取索引为偶数的所有值，并打印出获取后的列表
print(li[0::2])
#   - 列表中追加元素"seven",并输出添加后的列表
li.append('seven')
print(li)

#  - 请在列表的第1个位置插入元素"Tony",并输出添加后的列表
li.insert(0,'tony')
print(li)
#  - 请修改列表第2个位置的元素为"Kelly",并输出修改后的列表
li[1] ='Kelly'
print(li)
#  - 请将列表的第3个位置的值改成 "太白"，并输出修改后的列表
li[2] ='太白'
print(li)
"""

#  - 请将列表 l2=[1,"a",3,4,"heart"] 的每一个元素追加到列表li中，并输出添加后的列表
"""
li = ["alex", "WuSir", "ritian", "barry", "wenzhou"]
l2=[1,"a",3,4,"heart"]
li.extend(l2)
print(li)

#  - 请将字符串 s = "qwert"的每一个元素添加到列表li中，一行代码实现，不允许循环添加。

li = ["alex", "WuSir", "ritian", "barry", "wenzhou"]
s = "qwert"
li.extend(s)
print(li)

#  - 请删除列表中的元素"ritian",并输出添加后的列表

li = ["alex", "WuSir", "ritian", "barry", "wenzhou"]
li.remove("ritian")
print(li)

li = ["alex", "WuSir", "ritian", "barry", "wenzhou"]
li.pop(2)
print(li)

#  - 请删除列表中的第2个元素，并输出删除元素后的列表
li = ["alex", "WuSir", "ritian", "barry", "wenzhou"]
li.pop(1)
print(li)
#  - 请删除列表中的第2至第4个元素，并输出删除元素后的列表
li = ["alex", "WuSir", "ritian", "barry", "wenzhou"]
del li[1:4]
print(li)
"""

# 4. 请用三种方法实现字符串反转 name = "小黑半夜三点在被窝玩愤怒的小鸟"（步长、while、for）
"""
name = "小黑半夜三点在被窝玩愤怒的小鸟"
print(name[::-1])            #  步长反转字符

t = 0
s = ''
while t <len(name):
   s += name[-t-1]
   t += 1
print(s)                     #  while循环反转字符

t = 0
s = ''
for t in range(0,len(name)) :
    s += name[-t-1]
print(s)                      #  for循环反转字符

t = []
name = "小黑半夜三点在被窝玩愤怒的小鸟"
t.extend(name)
t.reverse()
print(''.join(t))    #join语句
"""

# 5. 写代码，有如下列表，利用切片实现每一个功能
#        li = [1, 3, 2, "a", 4, "b", 5,"c"]
"""
#    - 通过对li列表的切片形成新的列表 [1,3,2]
li = [1, 3, 2, "a", 4, "b", 5,"c"]
print(li[0:3])
#    - 通过对li列表的切片形成新的列表 ["a",4,"b"]
print(li[3:6])
#    - 通过对li列表的切片形成新的列表  [1,2,4,5]
print(li[::2])
#    - 通过对li列表的切片形成新的列表 [3,"a","b"]
print(li[1:6:2])
#    - 通过对li列表的切片形成新的列表 [3,"a","b","c"]
print(li[1::2])
#    - 通过对li列表的切片形成新的列表  ["c"]
print(li[-1:])
#    - 通过对li列表的切片形成新的列表 ["b","a",3]
print(li[-3::-2])
"""

#6. 请用代码实现循环输出元素和值：users = ["武沛齐","景女神","肖大侠"] ，如：
       # 0 武沛齐
       # 1 景女神
       # 2 肖大侠
"""
users = ["武沛齐","景女神","肖大侠"]
count = 0
for count in range(0,len(users)):
    print(count,users[count])
    count += 1
"""

# 7. 请用代码实现循环输出元素和值：users = ["武沛齐","景女神","肖大侠"] ，如：
#        1 武沛齐
#        2 景女神
#        3 肖大侠
"""
count = 0
users = ["武沛齐","景女神","肖大侠"]
for item in users :
    print(count,item)
    count += 1
"""

# 8. 写代码，有如下列表，按照要求实现每一个功能。
"""
#        lis = [2, 3, "k", ["qwe", 20, ["k1", ["tt", 3, "1"]], 89], "ab", "adv"]
#    - 将列表lis中的"k"变成大写，并打印列表。
lis = [2, 3, "k", ["qwe", 20, ["k1", ["tt", 3, "1"]], 89], "ab", "adv"]
lis[2] = lis[2].upper()
print(lis)

#    - 将列表中的数字3变成字符串"100"
lis = [2, 3, "k", ["qwe", 20, ["k1", ["tt", 3, "1"]], 89], "ab", "adv"]
lis[1] = '100'
lis[3][2][1][1]='100'
print(lis)

#    - 将列表中的字符串"tt"变成数字 101
lis = [2, 3, "k", ["qwe", 20, ["k1", ["tt", 3, "1"]], 89], "ab", "adv"]
lis[3][2][1][0]=101
print(lis)

#    - 在 "qwe"前面插入字符串："火车头"
lis = [2, 3, "k", ["qwe", 20, ["k1", ["tt", 3, "1"]], 89], "ab", "adv"]
lis[3].insert(0,"火车头")
print(lis)

"""

# 9. 写代码实现以下功能
#    - 如有变量 googs = ['汽车','飞机','火箭'] 提示用户可供选择的商品：
#          0,汽车
#          1,飞机
#          2,火箭
#    - 用户输入索引后，将指定商品的内容拼接打印，如：用户输入0，则打印 您选择的商品是汽车。
"""
googs = ['汽车','飞机','火箭']
count = int(input('请输入索引;'))
print('您选择的商品是{0},{1}'.format(count,googs[count]))
"""

# 10. 请用代码实现
#        li = "alex"
#     利用下划线将列表的每一个元素拼接成字符串"a_l_e_x"
"""
li = ['a','l','e','x']
print('_'.join(li))
"""

# 11. 利用for循环和range找出 0 ~ 100 以内所有的偶数，并追加到一个列表。
"""
list1 = []
for i in range(0,101,2):
        list1.append(i)
print(list1)
"""

# 12. 利用for循环和range 找出 0 ~ 50 以内能被3整除的数，并追加到一个列表。
"""
list2 =[]
for i in range(0,51,3):
        list2.append(i)
print(list2)
"""

# 13. 利用for循环和range 找出 0 ~ 50 以内能被3整除的数，并插入到列表的第0个索引位置，最终结果如下：
#         [48,45,42...]
"""
list3 =[]
for i in range(0,51,3):
    list3.insert(0,i)
print(list3)
"""

# 14. 查找列表li中的元素，移除每个元素的空格，并找出以"a"开头，并添加到一个新列表中,最后循环打印这个新列表。
#         li = ["TaiBai ", "alexC", "AbC ", "egon", " riTiAn", "WuSir", "  aqc"]
"""
li = ["TaiBai ", "alexC", "AbC ", "egon", " riTiAn", "WuSir", "  aqc"]
list4 = []
count = 0
for item in li:
    item1 = item.strip()
    if item1.startswith('a'):
        list4.append(item1)
print(list4)
"""

# 15. 判断是否可以实现，如果可以请写代码实现。
li = ["alex",[11,22,(88,99,100,),33], "WuSir", ("ritian", "barry",), "wenzhou"]
#     - 请将 "WuSir" 修改成 "武沛齐"
li[2]='武沛齐'
print(li)
#     - 请将 ("ritian", "barry",) 修改为 ['日天','日地']
li[3] = ['日天','日地']

#     - 请将 88 修改为 87
#######无法实现，元组不能修改其中的元素。

#     - 请将 "wenzhou" 删除，然后再在列表第0个索引位置插入 "文周"
li = ["alex",[11,22,(88,99,100,),33], "WuSir", ("ritian", "barry",), "wenzhou"]
# li.remove('wenzhou')
# li.pop(-1)
del li[-1]
li.insert(0,"wenzhou")
print(li)

###第五天作业
## 1.请将列表中的每个元素通过"_"链接起来。
# users = ['李少奇', '李启航', '渣渣辉']
# print('_'.join(users))


## 2.请将列表中的每个元素通过"_"链接起来。
# users = ['李少奇', '李启航', 666, '渣渣辉']
# users[2] = '666'
# print('_'.join(users))


## 3.请将元组 v1 = (11,22,33) 中的所有元素追加到列表 v2 = [44,55,66] 中。
# v1 = (11,22,33)
# v2 = [44,55,66]
# v2.extend(v1)
# print(v2)


## 4.请将元组v1 = (11, 22, 33, 44, 55, 66, 77, 88, 99)中的所有偶数索引位置的元素追加到列表v2 = [44, 55, 66]中。
# v1 = (11, 22, 33, 44, 55, 66, 77, 88, 99)
# v2 = [44,55,66]
# for count in range(0,len(v1),2):
#     v2.append(v1[count])
# print(v2)

## 5.将字典的键和值分别追加到key_list和value_list两个列表中，如：
"""
key_list = []
value_list = []
info = {'k1': 'v1', 'k2': 'v2', 'k3': 'v3'}
for item in info:
    key_list.append(item)
for item in info.values():
    value_list.append(item)
print(key_list)
print(value_list)
"""

##  6.字典dic = {'k1': "v1", "k2": "v2", "k3": [11, 22, 33]}
"""
dic = {'k1': "v1", "k2": "v2", "k3": [11, 22, 33]}
 ####a.请循环输出所有的key
for item in dic.keys():
    print(item)

    #b.请循环输出所有的value
for item in dic.values():
    print(item)

    #c.请循环输出所有的key和value
for item in dic.items():
    print(item)

    #d.请在字典中添加一个键值对，"k4": "v4"，输出添加后的字典
dic.update({"k4": "v4"})
print(dic)
    #e.请在修改字典中"k1"对应的值为"alex"，输出修改后的字典
dic = {'k1': "v1", "k2": "v2", "k3": [11, 22, 33]}
dic.update({"k1": "alex"})
print(dic)
    #f.请在k3对应的值中追加一个元素44，输出修改后的字典
dic = {'k1': "v1", "k2": "v2", "k3": [11, 22, 33]}
dic["k3"].append(44)
print(dic)
    #g.请在k3对应的值的第1个位置插入个元素18，输出修改后的字典
dic = {'k1': "v1", "k2": "v2", "k3": [11, 22, 33]}
dic["k3"].insert(0,18)
print(dic)
"""

## 7.请循环打印k2对应的值中的每个元素。
"""
info = {
    'k1': 'v1',
    'k2': [('alex'), ('wupeiqi'), ('oldboy')],
}
for item in info.get('k2'):
    print(item)
"""

## 8.有字符串"k: 1|k1:2|k2:3  |k3 :4"处理成字典{'k': 1, 'k1': 2....}
"""
dict1 = {}
content = "k: 1|k1:2|k2:3  |k3 :4"
content = content.split('|')
for item in content:
    b,c = item.split(':')
    dict1.update({b:c})
print(dict1)
"""

## 9.写代码有如下值 li= [11,22,33,44,55,66,77,88,99,90] ,将所有大于 66 的值保存至字典的第一个key对应的列表中，将小于 66 的值保存至第二个key对应的列表中。
#  result = {'k1':[],'k2':[]}
"""
li= [11,22,33,44,55,66,77,88,99,90]
result = {}
list1 = []
list2 = []
for item in li:
    if item > 66:
        list1.append(item)
    elif item < 66:
        list2.append(item)
result.update({'k1':list1,'k2':list2})
print(result)
"""

## 10.输出商品列表，用户输入序号，显示用户选中的商品
#   商品列表：
goods = [
         {"name": "电脑", "price": 1999},
         {"name": "鼠标", "price": 10},
         {"name": "游艇", "price": 20},
         {"name": "美女", "price": 998}
     ]
# 要求:
# 1：页面显示 序号 + 商品名称 + 商品价格，如：
#     1 电脑 1999
#     2 鼠标 10
#      ...
goods = [
        {"name": "电脑", "price": 1999},
        {"name": "鼠标", "price": 10},
        {"name": "游艇", "price": 20},
        {"name": "美女", "price": 998}
    ]
count = 0
for item in goods:
    print(count+1,item['name'],item['price'])
    count += 1

# 2：用户输入选择的商品序号，然后打印商品名称及商品价格
"""
num = int(input('请输入商品序号'))
print(goods[num-1]['name'],goods[num-1]['price'])

        # num = int(input('请输入商品序号'))
        # goods.insert(0,{})
        # print(goods[num]['name'],goods[num]['price'])
"""

# 3：如果用户输入的商品序号有误，则提示输入有误，并重新输入。
"""
while True:
    num = input('请输入商品序号')
    num = num.strip()
    if  not num.isdigit():
        print('输入的内容包含字符，请重新输入')
        continue
    num = int(num)
    if 0 < num < 5:                # 判断是否在商品序号范围内
        print(goods[num - 1]['name'], goods[num - 1]['price'])
        break
    print('输入有误，超出序号范围，请重新输入')
"""

# 4：用户输入Q或者q，退出程序。
"""
while True:
    num = input('请输入商品序号')
    if num.lower() == 'q':
        break
    num = num.strip()
    if  not num.isdigit():
        print('输入的内容包含字符，请重新输入')
        continue
    num = int(num)
    if 0 < num < 5:                # 判断是否在商品序号范围内
        print(goods[num - 1]['name'], goods[num - 1]['price'])
        break
    print('输入有误，超出序号范围，请重新输入')
"""

## 11.看代码写结果
v = {}
for index in range(10):
    v['users'] = index
print(v)                         # {'users':9}
# range(10)=[0,1,2,3,4,5,6,7,8,9]
# 第一次循环,给字典v添加键值对'users':0   ,   这时v = {'users':0}
# 后面的循环，则修改键值对，改变字典'users'对应的值，最终输出v ={'users':9}


####第六天作业
#1 .列举你了解的字典中的功能（字典独有）。
# 答：.keys()  获取字典所有键
# .values()   获取字典所有值
# .items()    获取字典所有键值对
#  .get(key)    获取键key对应的值
#  .update（)    更新字典的键值对，若键一致，重新赋值，若不存在这个键，就添加新键值对。

#2 . 列举你了解的集合中的功能（集合独有）
"""答： .add()   添加元素
        .update()    添加元素
        .discard()    删除集合的某元素
        .union()      求并集
        .difference()  求交集
        .intersection()  求差集，属于前面的集合但不属于后面的集合的元素
"""

#3 .列举你了解的可以转换为 布尔值且为False的值。
#答：0 ， '', [], (), {}, set()转换bool值时为False

#4 .请用代码实现
        #    info = {'name':'王刚蛋','hobby':'铁锤'}
#- 循环提示用户输入，根据用户输入的值为键去字典中获取对应的值并输出。
"""
info = {'name':'王刚蛋','hobby':'铁锤'}
while True:
    keys = input('请输入字典的键：')
    print(info.get(keys))
"""

#- 循环提示用户输入，根据用户输入的值为键去字典中获取对应的值并输出（如果key不存在，则获取默认“键不存在”，并输出）。
#  注意：无需考虑循环终止（写死循环即可）
"""
info = {'name':'王刚蛋','hobby':'铁锤'}
while True:
    keys = input('请输入字典的键：')
    print(info.get(keys,'键不存在'))
"""

#5 .请用代码验证 "name" 是否在字典的键中？
"""
info = {'name':'王刚蛋','hobby':'铁锤','age':'18','其他':'100个键值对'}
message = '"name" 不在字典的键中'
if 'name' in info:
    message = '"name" 在字典的键中'
print(message)
"""

#6 .请用代码验证 "alex" 是否在字典的值中？
"""
info = {'name':'王刚蛋','hobby':'铁锤','age':'18','其他省略':'100个键值对'}
message = '"alex" 不在字典的值中'
if "alex" in list(info.values()):
    message = '"alex" 在字典的值中'
print(message)
"""

#7 .有如下
v1 = {'武沛齐','李杰','太白','景女神'}
v2 = {'李杰','景女神'}
# - 请得到 v1 和 v2 的交集并输出
print(v1.intersection(v2))
# - 请得到 v1 和 v2 的并集并输出
print(v1.union(v2))
# - 请得到 v1 和 v2 的 差集并输出
print(v1.difference(v2))
# - 请得到 v2 和 v1 的 差集并输出
print(v2.difference(v1))

#8 .循环提示用户输入，并将输入内容追加到列表中（如果输入N或n则停止循环）
"""
list = []
while True:
    item = input('请输入内容：')
    if item.upper() == 'N':
        break
    list.append(item)
print(list)
"""

#9 .循环提示用户输入，并将输入内容添加到集合中（如果输入N或n则停止循环）
"""
set1 = set()
while True:
    item = input('请输入内容：')
    if item.upper() == 'N':
        break
    set1.add(item)
print(set1)
"""

#10 .写代码实现
"""
v1 = {'alex','武sir','肖大'}
v2 = []
# 循环提示用户输入，如果输入值在v1中存在，则追加到v2中，如果v1中不存在，则添加到v1中。（如果输入N或n则停止循环）
while True:
    x = input('请输入内容：')
    if x.upper() == 'N':
        break
    if x in v1:
        v2.append(x)
    else:
        v1.add(x)
print(v1,v2)
"""

#11 .判断以下值那个能做字典的key ？那个能做集合的元素？
#  1       都能做
#  -1      都能做
#  ""      都能做
#  None    都能做
#  [1,2]   可变类型,不能做
#  (1,)     是元组，都可以做
#  {11,22,33,4}     可变类型，不能做
#  {'name':'wupeiq','age':18}     可变类型,不能做

#12 .is 和 == 的区别？
# 答：== 表示比较，判断两者的值是否相等。
#      is判断两者的内存地址是否相等。

#13 .type使用方式及作用？
# 答：type(),返回值为括号里的对象的数据类型。t =type()

#14 .d的使用方式及作用？
# 答：id(),返回值为括号里的对象所指向的内存地址。t =id()

#15 .看代码写结果并解释原因
# v1 = {'k1':'v1','k2':[1,2,3]}
# v2 = {'k1':'v1','k2':[1,2,3]}
#
# result1 = v1 == v2
# result2 = v1 is v2
# print(result1)          # v1的值和v2的值相等，结果为True
# print(result2)          # v1指向的内存地址和v2指向的内存地址不同，结果为False

#16 .看代码写结果并解释原因
# v1 = {'k1':'v1','k2':[1,2,3]}
# v2 = v1                               # 指向关系相同，指向的内存地址就是同一个
#
# result1 = v1 == v2
# result2 = v1 is v2
# print(result1)      #值相同，输出True
# print(result2)      #指向的内存地址是同一个，输出True

#17 .看代码写结果并解释原因
# v1 = {'k1':'v1','k2':[1,2,3]}
# v2 = v1                # 指向关系相同，指向的内存地址就是同一个。
#
# v1['k1'] = 'wupeiqi'
# print(v2)            #引用共享，输出改动后的字典{'k1':'wupeiqi','k2':[1,2,3]}

#18 .看代码写结果并解释原因
# v1 = '人生苦短，我用Python'
# v2 = [1,2,3,4,v1]            #这里的v1表示这种引用关系，指向'人生苦短，我用Python'对应的内存地址
#
# v1 = "人生苦短，用毛线Python"   #重新赋值，改变指向，v1指向另一个内存地址。v2不变。
#
# print(v2)    #输出[1,2,3,4,'人生苦短，我用Python']

#19 .看代码写结果并解释原因
# info = [1,2,3]
# userinfo = {'account':info, 'num':info, 'money':info}      # 指向info对应的内存地址
#
# info.append(9)
# print(userinfo)        #info对应的内存地址添加，同样改变。
#                        #输出{'account':[1,2,3,9], 'num':[1,2,3,9], 'money':[1,2,3,9]}
#
# info = "题怎么这么多"     #info重新赋值，指向关系改变。userinfo不变。
# print(userinfo)          #输出{'account':[1,2,3], 'num':[1,2,3], 'money':[1,2,3]}

#20 .看代码写结果并解释原因
# info = [1,2,3]
# userinfo = [info,info,info,info,info]          #info表指向关系，指向同一个内存地址。
#
# info[0] = '不仅多，还特么难呢'            #info更新，内存地址还是那个
# print(info,userinfo)                   #输出改变后的['不仅多，还特么难呢',2,3]

#21 .看代码写结果并解释原因
# info = [1,2,3]
# userinfo = [info,info,info,info,info]
#
# userinfo[2][0] = '闭嘴'         #对userinfo[2][0]重新赋值，内存地址变更。info指向关系不变
# print(info,userinfo)            #输出['闭嘴',2,3]   [['闭嘴',2,3],['闭嘴',2,3],['闭嘴',2,3],['闭嘴',2,3],['闭嘴',2,3]]

#22 .看代码写结果并解释原因
# info = [1, 2, 3]           # info指向一内存地址
# user_list = []
# for item in range(10):
#     user_list.append(info)  #user_list = [10个info作为列表元素]，指向内存地址同一个
#
# info[1] = "是谁说Python好学的？"       #info的指向关系不变，
#
# print(user_list)          #[[1, "是谁说Python好学的？", 3],``````]

#23 .看代码写结果并解释原因
# data = {}                       # 确定data的指向的内存地址不变
# for i in range(10):
#     data['user'] = i
# print(data)                    # 一直更新'user'键对应的值，输出{'user'：9}

#24 .看代码写结果并解释原因
# data_list = []
# data = {}                                # 确定data的指向的内存地址不变
# for i in range(10):
#     data['user'] = i
#     data_list.append(data)            # data的指向的内存地址不变
# print(data_list)                   # [{'user':9},{'user':9}, ``` ```{'user':9}]

#25 .看代码写结果并解释原因
# data_list = []
# for i in range(10):
#     data = {}
#     data['user'] = i             # data的指向的内存地址因每次重新赋值而改变
#     data_list.append(data)
# print(data)                      #[{'user':0},[{'user':1},{'user':2}, ``` ```{'user':9}]]