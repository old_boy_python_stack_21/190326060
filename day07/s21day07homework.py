#!/usr/bin/env python
# -*- coding:utf-8 -*-


#1. 看代码写结果
v1 = [1,2,3,4,5]
v2 = [v1,v1,v1]

v1.append(6)    # [1,2,3,4,5.6],内存地址不变
print(v1)
print(v2)    #[[1,2,3,4,5.6],[1,2,3,4,5.6],[1,2,3,4,5.6]]

#2. 看代码写结果
v1 = [1,2,3,4,5]
v2 = [v1,v1,v1]

v2[1][0] = 111  #都在v1指向的内存地址修改
v2[2][0] = 222  #都在v1指向的内存地址修改,
print(v1)    #[222,2,3,4,5]
print(v2)    # [[222,2,3,4,5],[222,2,3,4,5],[222,2,3,4,5]]

#3 .看代码写结果,并解释每一步的流程。
v1 = [1,2,3,4,5,6,7,8,9]
v2 = {}     #创建空字典

for item in v1:
    if item < 6:
        continue       #循环剔除v1中数字小于6的元素,[6,7,8,9]
    if 'k1' in v2:
        v2['k1'].append(item)       #item取7,8,9时,'k1'对应的值,列表添加元素
    else:
        v2['k1'] = [item ]   #item取6,添加键值对'k1':[6]
print(v2)   #{'k1',[6,7,8,9]}

#4. 简述深浅拷贝？
#答:浅拷贝:只拷贝到第一层
#    深拷贝:拷贝到嵌套层次中的所有可变类型.

#5. 看代码写结果
import copy
v1 = "alex"
v2 = copy.copy(v1)
v3 = copy.deepcopy(v1)
print(v1 is v2)     # True
print(v1 is v3)     #对字符串,深浅拷贝是一样的.理应内存地址不一样,但因为小数据池的缘故.得到结果都是True

#6. 看代码写结果
import copy
v1 = [1,2,3,4,5]
v2 = copy.copy(v1)
v3 = copy.deepcopy(v1)
print(v1 is v2)  # False
print(v1 is v3)  # False ,对list/set/dict,深浅拷贝都重新创建了新内存地址存数据.

#7. 看代码写结果
import copy
v1 = [1,2,3,4,5]
v2 = copy.copy(v1)      #创建新列表,但指向1,2,3,4,5的指向相同.
v3 = copy.deepcopy(v1)   #创建新列表,但指向1,2,3,4,5的指向相同.
print(v1[0] is v2[0])    # True
print(v1[0] is v3[0])    # True
print(v2[0] is v3[0])    # True

#8. 看代码写结果
#同第7题

#9. 看代码写结果
import copy

v1 = [1,2,3,{"name":'武沛齐',"numbers":[7,77,88]},4,5]
v2 = copy.copy(v1)

print(v1 is v2)  # False

print(v1[0] is v2[0])    # True,不可变类型
print(v1[3] is v2[3])    # True,浅拷贝只到第一层

print(v1[3]['name'] is v2[3]['name'])    # True,浅拷贝只到第一层
print(v1[3]['numbers'] is v2[3]['numbers'])   # True,浅拷贝只到第一层
print(v1[3]['numbers'][1] is v2[3]['numbers'][1])   # True,浅拷贝只到第一层

#10. 看代码写结果
import copy
v1 = [1,2,3,{"name":'武沛齐',"numbers":[7,77,88]},4,5]
v2 = copy.deepcopy(v1)    # 深拷贝,到嵌套的所有可变类型

print(v1 is v2)    # False

print(v1[0] is v2[0])   #True ,不可变类型
print(v1[3] is v2[3])   #False ,可变类型,深拷贝会重新创建内存地址

print(v1[3]['name'] is v2[3]['name'])      #True ,不可变类型
print(v1[3]['numbers'] is v2[3]['numbers'])     #False ,可变类型,深拷贝会重新创建内存地址
print(v1[3]['numbers'][1] is v2[3]['numbers'][1])    #True ,不可变类型

#11. 简述文件操作的打开模式
#答:只读模式r.只能读文件,文件不存在报错,光标默认在开头.
#   只写模式w,只能写文件,文件不存在就创建,文件存在就会覆盖文件内的内容,光标默认在开头.
#   追加模式a,只能追加写文件,文件不存在就创建,文件存在就会追加写入内容,光标默认在最后.
#  读写模式,r+,能读写文件,读光标默认在开头,写入光标在当前位置.
#  读写模式,w+,能读写文件,读默认光标永远在写入的最后,写入则会清空.
#  读写模式a+,能读写文件,读默认光标永远在写入的最后,写入则会追加.

#12. 请将info中的值使用 "_" 拼接起来并写入到文件 "readme.txt" 文件中。
info = ['骗子，','不是','说','只有',"10",'个题吗？']
content = '_'.join(info)
file = open("readme.txt",'w',encoding='utf-8')
file.write(content)
file.close()

#13. 请将info中的值使用 "_" 拼接起来并写入到文件 "readme.txt" 文件中。
info = ['骗子，','不是','说','只有',10,'个题吗？']
info[4] = str(info[4])
content = '_'.join(info)
file = open("readme.txt",'w',encoding='utf-8')
file.write(content)
file.close()


#14. 请将info中的所有键 使用 "_" 拼接起来并写入到文件 "readme.txt" 文件中。
info = {'name':'骗子','age':18,'gender':'性别'}
# 1. 请将info中的所有键 使用 "_" 拼接起来并写入到文件 "readme.txt" 文件中。
content1 = '_'.join(info)
file = open("readme.txt",'w',encoding='utf-8')
file.write(content1)
file.close()
# 2. 请将info中的所有值 使用 "_" 拼接起来并写入到文件 "readme.txt" 文件中。
info['age'] = str(info['age'])
list1 = list(info.values())
content2 = '_'.join(list1)
file = open("readme.txt",'w',encoding='utf-8')
file.write(content2)
file.close()
# 3. 请将info中的所有键和值按照 "键|值,键|值,键|值" 拼接起来并写入到文件 "readme.txt" 文件中。
info = {'name':'骗子','age':18,'gender':'性别'}
s = ''
for item in info:
    t = info.get(item)
    s += ('%s|%s,' %(item,t))    #这里的18也可以用%s格式化
print(s)           # 'name|骗子,age|18,gender|性别,',最后有个逗号要去除.
s = s.rstrip(',')
file = open("readme.txt",'w',encoding='utf-8')
file.write(s)
file.close()


#15. 写代码
"""
要求：
    如文件 data.txt 中有内容如下：

    wupeiqi|oldboy|wupeiqi@xx.com
    alex|oldboy|66s@xx.com
    xxxx|oldboy|yuy@xx.com

    请用代码实现读入文件的内容，并构造成如下格式：
    info = [
        {'name':'wupeiqi','pwd':'oldboy','email':'wupeiqi@xx.com'},
        {'name':'alex','pwd':'oldboy','email':'66s@xx.com'},
        {'name':'xxxx','pwd':'oldboy','email':'yuy@xx.com'},
    ]
"""
# 在和py同文件夹下先创建data.txt.避免r模式报错.或者用r+
# 已有记事本文件（非空），转码 UTF-8，复制到pycharm中，在开始位置打印结果会出现  \ufeff,它常被用来当做标示文件是以UTF-8、UTF-16或UTF-32编码的记号。
# 通过在https://www.cnblogs.com/chongzi1990/p/8694883.html看到,只需改一下编码就行，把 UTF-8 编码 改成 UTF-8-sig,去除'\ufeff',
file = open('data.txt','r',encoding='utf-8-sig')
content = file.read().split()   #去除换行符
content = '|'.join(content)     #把列表中的元素改为字符串
list1 = content.split('|')
info = []
for item in range(0,len(list1),3):
    dict1 = {}
    dict1.update({'name': list1[item], 'pwd': list1[item+1], 'email': list1[item+2]})
    info.append(dict1)
print(info)
#第二种方法,一行一行读取,省内存.
"""
file = open('data.txt','r',encoding='utf-8-sig')
info = []
for line in  file:
    line = line.split()
    s = line[0]     #  s = ''.join(line)
    s = s.split('|')
    dict1 = {}
    dict1.update({'name':s[0],'pwd':s[1],'email':s[2]})
    info.append(dict1)      
print(info)
"""

