#!/usr/bin/env python
# -*- coding:utf-8 -*-

# !/usr/bin/env python
# -*- coding:utf-8 -*-

import socket
import os

sk = socket.socket()
sk.bind(('127.0.0.1', 8090))
sk.listen(5)
basedir = os.path.dirname(os.path.abspath(__file__))


def get(data):
    url = data[0].split(' ')[1]
    print(url)
    file = url if url != '/' else 'index.html'
    file = file.strip('/')
    path = os.path.join(basedir,'templates',file)
    with open(path, 'rb') as f:
        res = f.read()
    return res


def post(data):
    print('data>>>>',data)
    login = data[-1]
    print('login>>>>',login)
    a, b = login.split('&')
    user = a.strip('user=')
    pwd = b.strip('pwd=')
    print('a>>>>',a)
    print('b>>>>',b)
    if user == '戴永靖' and pwd == '666':
        return '<h1>404 not found </h1>'.encode('utf-8')
    else:
        return '404'.encode('utf-8')


info = {'GET': get, 'POST': post}

if __name__ == '__main__':
    while True:
        conn, addr = sk.accept()
        ret = conn.recv(4096).decode('GB2312')
        data = ret.split('\r\n')
        print(data)
        func = info.get(data[0].split(' ')[0])
        if func:
            res = func(data)
        else:
            res = '<h1>404 not found </h1>'.encode('utf-8')
        conn.send('HTTP/1.1 200 OK\r\n\r\n'.encode('utf-8'))
        conn.send(res)
        conn.close()
