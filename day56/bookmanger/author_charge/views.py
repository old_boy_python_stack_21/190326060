from django.shortcuts import render, HttpResponse, redirect
from publish_charge import models


# Create your views here.
def author_list(request):
    author_book_list = models.Author.objects.all()
    """
    for i in author_book_list:
        print(i.pk)
        print(i.author)
        for item in i.book.all():
            print(item.b_name)
    """
    return render(request, 'author_list.html', {'author_book_list': author_book_list})


def add_author(request):
    error = ''
    book_list = models.Book.objects.all()
    if request.method == "POST":
        author = request.POST.get('author')
        bid = request.POST.getlist('bid')
        if models.Author.objects.filter(author=author):
            error = "作者已存在"
        if not author:
            error = '作者名不能为空'
        if not error:
            author_obj = models.Author.objects.create(author=author)
            author_obj.book.set(bid)
            return redirect('/author_list/')
    return render(request, 'add_author.html', {'book_list': book_list, 'error': error})


def del_author(request):
    aid = request.GET.get('id')
    models.Author.objects.get(pk=aid)
    models.Author.objects.filter(pk=aid).delete()
    return redirect('/author_list/')


def edit_author(request):
    aid = request.GET.get('id')
    obj_list = models.Author.objects.filter(pk=aid)
    error = ''
    if request.method == "POST":
        author = request.POST.get('author')
        bids = request.POST.getlist('bids')
        if models.Author.objects.filter(author=author):
            error = '作者名称已重复'
        if not author:
            error = '作者名不能为空'
        if not error:
            obj = models.Author.objects.get(pk=aid)
            obj.author = author
            obj.save()
            obj.book.set(bids)
            return  redirect('/author_list/')


    book = models.Book.objects.all()
    return render(request, 'edit_author.html', {'obj_list': obj_list, 'book': book})
