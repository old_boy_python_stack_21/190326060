# Python全栈day 10

## 一. 昨日函数回顾

### 1. 基本格式

```python
def 函数名():
    函数体
    retur None  #  默认返回None  ,没有这一语句也是这样.
```

### 2. 参数

``` python
def func(a1,a2):
    函数体(可调用a1和a2)
    return 返回值
# 严格按照顺序传参数：位置方式传参。
# 实际参数可以是任意类型。

def check_age(age):  #只有1个参数:age,参数数量可以是无穷多个.
    if age >= 18:
        print('你是成年人')
    else:
        print('你是未成年人')
        
check_age(18)    #调用函数,输出你是成年人.
```

### 3. 返回值

```python
#函数没有返回值时,默认返回None
#函数有返回值时,语句为:return 返回值.返回值可以是任意东西.
def compare_num(a,b):
    return a if a>b else b  #和三目运算一起,返回a和b中较大的数.
#############################################
#  函数执行到return 语句,就返回,后面的语句不执行.
#  若返回值为多个,默认将其添加到元组中,返回一个元组.
def func(a,b,c,d)
	return a,b,c,d

v = func(1,'邓益新',[11,22,33,44,55],{'k1':'v1'})
print(v)            # (1,'邓益新',[11,22,33,44,55],{'k1':'v1'})  元组,

```

## 二 .参数详解

### 1. def  函数名(参数)

1. def  函数名(参数),这里的参数叫做形式参数,简称形参.
2. 参数可以任意个数,任意类型.str/int/bool/list/tuple/dict/set

### 2. 位置参数

```python
def func(a1,a2,a3,a4)
	print(a1,a2,a3,a4)
  
func(1,2,3,4)
# 严格按顺序输入参数,顺序是固定的.
```

### 3. 关键字传参

```python
def func(a1,a2)
	print(a1,a2)
  
func(a1 = 1 ,a2 = 2) 
func(a2 = 2,a1 = 1)  # 俩者完全一样,全是关键字可以打乱顺序.
```

3.1位置传参和关键字传参混合使用

```python
######  这时必须位置参数在前,关键字参数在后
def func(a1,a2,a3,a4)
	print(a1,a2,a3,a4)
   
func(1,10,a3 = 15,a4 = 88) 
func(1, 10, a4=15, a3=88)  # 俩者等价,正确.
func(a1=1,a2=2,a3=3,a4=4)  #正确
func(a1=1,a2=2,15,a4 = 88)  # 错误
func(1,2,a3 = 15,88)  #错误
#################################
#    必须位置参数在前,关键字参数在后, 位置参数 > 关键字参数  #
```

### 4. 默认参数

```python
def func(a1,a2,a3=9,a4=10):   #  默认a3=9,a4=10,这时可以不输入a3 和 a4 参数.默认值为 9 和 10 
    print(a1,a2,a3,a4)

func(11,22)   #正确
func(11,22,10)  #  正确,修改默认参数a3 = 10 ,参数a4不输入.默认为10 .
func(11,22,10,100)  #正确
func(11,22,10,a4=100)   #正确
func(11,22,a3=10,a4=100)    #正确
func(11,a2=22,a3=10,a4=100)   #正确
func(a1=11,a2=22,a3=10,a4=100)    #正确
```

### 5. 万能参数(重点)

5.1 *args(打散)

- **可以接受任意个数的位置参数，并将参数转换成元组。**

  1. 没有调用函数   *    字符

     ```python
     def func(*args):
         print(args)
     func(1,2,3,True,[11,22,33,44])
     #  输出为(1,2,3,True,[11,22,33,44])  元组
     ```

     

  2. 有调用函数   *    字符

     ```python
     def func(*args):
         print(args)
     func(*(1,2,3,True,[11,22,33,44]))
     #  输出为(1,2,3,True,[11,22,33,44])  元组
     ```

- *args  只能用位置传参

  ```python
  def func4(*args):
      print(args)
      
  func4(1)    元组(1,)
  func4(1,2)   元组(1,2)
  
  func4(1,2)
  func4(1,2)
  
  ```

5.2  **kwags(打散)

- 可以接受任意个数的关键字参数，并将参数转换成字典。

  1. 没有调用函数   **    字符

     ```python
     
     def func(**kwargs):   
         print(kwargs)
     
     func(k1=1,k2="alex")
     ```

  2. 有调用函数   **    字符

     ```python
     def func(**kwargs):
         print(kwargs)
     func(**{'k1':'v2','k2':'v2'}) # kwargs={'k1':'v2','k2':'v2'}
     ```

- **kwags(打散)只能用关键字传参 

### 6.不定长参数综合使用

```python
def func(*args,**kwargs):
    print(args,kwargs)

# func(1,2,3,4,5,k1=2,k5=9,k19=999)
func(*[1,2,3],k1=2,k5=9,k19=999)
func(*[1,2,3],**{'k1':1,'k2':3})
func(111,222,*[1,2,3],k11='alex',**{'k1':1,'k2':3})
```

## 三'作用域

### 1.python的作用域:

- 作用于全局,对任意一个py文件来说,全局作用域

- 对函数来说:局部作用域

  ```python
  #  在函数中定义的值,只在局部作用域中
  #  在全局作用域中找不到#
  a = 1
  b = 2
  def func():
      a = 10
      b = 20
      print(a) 
      print(b)
  func()     #  输出10  ,20
  print(a)
  print(b)   #输出 1    , 2
  
  ```

- 总结

  - 一个函数是一个作用域,一般来说就是局部作用域.

    ```python
    name = '邓益新' 
    def func():
        name = '李林元'   #在函数作用域创建一个新址,name指向这个新址.
        print(name)
    func()  #  运行函数的代码
    print(name)    # 全局的变量  '邓益新' 
    ```

  - 作用域中查找数据规则：优先在自己的作用域找数据，自己没有就去  "父级" ->  "父级" -> 直到全局，全部么有就报错。

    ```python
    a = b = 1
    def func():
        b = 5
        print(a,b)
    func()     #  1 ,5
    print(a,b)  #   1, 1
    ```

    

  - 子作用域只能找到父级作用域中的变量的值,不能重新为赋值父级作用域的变量赋值.

    - 例外,特别:  global 变量名,强制把全局的变量赋值

    ```python
    a = b = 1
    def func():
    	global b
        b = 5     # 更改全局变量b = 5
        print(a,b)
    func()     #  1 ,5
    print(a,b)  #   1, 5
    ```

    - nonlocal   ,强制把父级的变量赋值 ,一级一级向上找寻,不找全局域.找不到则报错.

    ```python
    a = b = 1
    def func():
    	nonlocal b
        b = 5     # 更改父级变量,如果找不到,再到父级的父级找寻,一直找到全局域之前(不包含全局域)
        print(a,b)
    func()      #会报错,父级就是全局域,nonlocal不包含全局域,故会报错,找不到b 
    print(a,b)  
    
    a = b = 1
    def func():
        a = 3
        b = 99
        print(a,b)
        def func1():
            nonlocal b
            b = 88
            print(a,b)
        func1()
    func()
    print(a,b)
    ```


### 2.补充:

- 全局变量必须大写

