#!/usr/bin/env python
# -*- coding:utf-8 -*-

# 1.写函数，函数可以支持接收任意数字（位置传参）并将所有数据相加并返回。
def add_num(*args):
    s = 0
    for i in args:
        s += i
    return s
"""


# 2.看代码写结果

def func():
    return 1, 2, 3   # 返回的是个元组(1,2,3)

val = func()
print(type(val) == tuple)    # True
print(type(val) == list)     # False


#3. 看代码写结果

def func(*args, **kwargs):
    pass

# a. 请将执行函数，并实现让args的值为 (1,2,3,4)
func(1,2,3,4)
# b. 请将执行函数，并实现让args的值为 ([1,2,3,4],[11,22,33])
func([1,2,3,4],[11,22,33])
# c. 请将执行函数，并实现让args的值为 ([11,22],33) 且 kwargs的值为{'k1':'v1','k2':'v2'}
func([11,22],33,k1 = 'v1' , k2 = 'v2')
# d. 如执行 func(*{'武沛齐','金鑫','女神'})，请问 args和kwargs的值分别是？
func(*{'武沛齐','金鑫','女神'})    # 一个*,args = ('武沛齐','金鑫','女神'), kwargs = {} .
# e. 如执行 func({'武沛齐','金鑫','女神'},[11,22,33])，请问 args和kwargs的值分别是？
func({'武沛齐','金鑫','女神'},[11,22,33])     # args = ({'武沛齐','金鑫','女神'},[11,22,33])  , kwargs = {}
# f. 如执行 func('武沛齐','金鑫','女神',[11,22,33],**{'k1':'栈'})，请问 args和kwargs的值分别是？
func('武沛齐','金鑫','女神',[11,22,33],**{'k1':'栈'})   #  args = ('武沛齐','金鑫','女神',[11,22,33]) ,kwargs = {'k1':'栈'}


# 4.看代码写结果


def func(name, age=19, email='123@qq.com'):
    pass

# a. 执行 func('alex') ,判断是否可执行，如可以请问 name、age、email 的值分别是？
######     可以执行,值分别为'alex'  ,    19    , '123@qq.com'

# b. 执行 func('alex',20) ,判断是否可执行，如可以请问 name、age、email 的值分别是？
######     可以执行,值分别为'alex'  ,    20    , '123@qq.com'

# c. 执行 func('alex',20,30) ,判断是否可执行，如可以请问 name、age、email 的值分别是？
######     可以执行,值分别为'alex'  ,    20    ,   30

# d. 执行 func('alex',email='x@qq.com') ,判断是否可执行，如可以请问 name、age、email 的值分别是？
######     可以执行,值分别为'alex'  ,    19    ,   'x@qq.com'

# e. 执行 func('alex',email='x@qq.com',age=99) ,判断是否可执行，如可以请问 name、age、email 的值分别是？
######     可以执行,值分别为'alex'  ,    99    ,   'x@qq.com'

# f. 执行 func(name='alex',99) ,判断是否可执行，如可以请问 name、age、email 的值分别是？
######     不能执行,位置参数 要在 关键字参数 前面,程序报错

# g. 执行 func(name='alex',99,'111@qq.com') ,判断是否可执行，如可以请问 name、age、email 的值分别是？
######     不能执行,位置参数 要在 关键字参数 前面,程序报错





# 5.看代码写结果

def func(users, name):
    users.append(name)
    return users

result = func(['武沛齐', '李杰'], 'alex')
print(result)   #  ['武沛齐', '李杰', 'alex']



# 6 .看代码写结果

def func(v1):
    return v1 * 2

def bar(arg):
    return "%s 是什么玩意？" %(arg,)


val = func('你')       #   '你你'
data = bar(val)       #
print(data)            #    '你你 是什么玩意？'


# 7.看代码写结果

def func(v1):
    return v1 * 2

def bar(arg):
    msg = "%s 是什么玩意？" % (arg,)
    print(msg)

val = func('你')   #   '你你'
data = bar(val)    #  data = None  ,运行到这里的时候,会打印'你你 是什么玩意？'
print(data)        # 打印  None




# 8.看代码写结果
v1 = '武沛齐'

def func():
    print(v1)

func()    # 打印'武沛齐'
v1  = '老男人'
func()    # # 打印'老男人'


# 9.看代码写结果
v1 = '武沛齐'

def func():
    v1 = '景女神'   #  子作用域中, v1 = '景女神'
    def inner():
        print(v1)
    v1 = '肖大侠'   # #  子作用域中, v1重新赋值 = '肖大侠'
    inner()
func()         #####   输出打印    '肖大侠'
v1 = '老男人'
func()    ######    输出打印    '肖大侠'




# 10.看代码写结果【可选】

def func():
    data = 2 * 2    # 子作用域中 ,data = 4
    return data

new_name = func      #  函数名 func 等价于new_name
val = new_name()       #函数返回值为 4
print(val)       # 4

# 注意：函数类似于变量，func代指一块代码的内存地址。



# 11.看代码写结果【可选】
def func():
    data = 2 * 2
    return data       #  调用函数时,返回值为 4


data_list = [func, func, func]     #指向3个 函数,函数类似于变量，func代指一块代码的内存地址。
for item in data_list:
    v = item()
    print(v)    #竖向打印3次  4

# 注意：函数类似于变量，func代指一块代码的内存地址。

"""
# 12.看代码写结果（函数可以做参数   进行传递）【可选】

def func(arg):
    arg()

def show():
    print('show函数')

func(show)   #--->调用func函数,以show为参数.
# -->运行show()函数
#-->输出'show函数'
