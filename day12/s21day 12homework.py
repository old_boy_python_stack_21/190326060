#!/usr/bin/env python
# -*- coding:utf-8 -*-


# 作业（二）

# 1.写出三元运算的基本格式及作用？
a = 1
b = 2
v = a if a > b else b
# 类似   前者  if 条件  else  后   ,若条件为真取前者,否则取后者
# 用于简化if条件判断语句




# 2.什么是匿名函数？
#   lambda函数,简单函数,只用一行代码,有时不用赋函数名称,就叫匿名函数.

# 3.尽量多的列举你了解的内置函数？【默写】
"""
输入输出:print input
数值转换  int bool  str   list  tuple  dict  set   float(浮点型/小数转换)
其他   len  id  type  range  open(这个忘了,补上)   
数学相关   sum  min  max  abs  oct  bin  hex  chr   ord   round  pow   divmod得2个数的商和余数

"""





# 4. filter / map / reduce函数的作用分别是什么？
#  filter  ,过滤/筛选,第一个参数为函数,第二个参数为序列,遍历序列中的元素,过滤掉不符合条件的元素,把符合条件的元素添加到一个新列表中
#  map ,第一个参数为函数,第二个参数为序列,遍历序列中的元素,函数以元素为参数来运行,把返回值添加到新列表中
#  reduce,第一个参数为函数,第二个参数为序列,遍历序列中的元素,函数先选用序列中的前2个元素作为参数,得到返回值和第3个元素进行累积,最终得到一个结果.
#


# 5.看代码写结果
"""
def func(*args, **kwargs):
    print(args, kwargs)
"""
# a. 执行 func(12,3,*[11,22]) ，输出什么？
# 输出(12,3,11,22)  {}

# b. 执行 func(('alex','武沛齐',),name='eric')
# 输出 (('alex','武沛齐',),)   {'name':'eric'}


# 6.看代码分析结果
"""

def func1(arg):
    return arg.pop(1)   # 返回值,返回删除的元素

result = func1([11, 22, 33, 44])
print(result)    # 22


# 7.看代码分析结果

func_list = []

for i in range(10):
    func_list.append(lambda: i)   # 匿名函数:返回值为i,此时没有执行.循环终了,i= 9

print(list(func_list))
v1 = func_list[0]()        #调用函数,全局变量i = 9,返回值为9
v2 = func_list[5]()       #返回值为9
# print(v1, v2)


# 8.看代码分析结果

func_list = []

for i in range(10):
    func_list.append(lambda x: x + i)

v1 = func_list[0](2)    #调用函数lambda,给定参数x = 2 ,全局变量 i = 9  ,输出11
v2 = func_list[5](1)   # 10
print(v1, v2)




# 9.看代码分析结果

func_list = []

for i in range(10):
    func_list.append(lambda x: x + i)    # 循环结束,i = 9

for i in range(0, len(func_list)):    # len(func_list) = 10 ,从循环开始,i重新赋值
    result = func_list[i](i)       #调用lambda函数,i =0,x = 0,返回值0,就打印0
    print(result)
#  循环打印出0,2,4,8,----18.


# 10.看代码分析结果

def f1():
    print('f1')  # 返回值为None

def f2():
    print('f2')
    return f1   #函数做返回值

func = f2()   # 运行f2函数,打印f2", func = f1 ,
result = func()  # 调用f1函数,打印'f1'
print(result)  # f1函数为返回值为None ,打印 None
#  依次打印 f2   ,f1     ,None


# 11.看代码分析结果【面试题】

def f1():
    print('f1')
    return f3()    # 返回f3函数的返回值,<-----> v = f3() ; return v

def f2():
    print('f2')
    return f1    #返回f1函数名

def f3():
    print('f3')  # 打印f3 ,返回值 None

func = f2()   #打印f2,返回f1函数名,func = f1
result = func()  #调用运行函数f1  ,打印f1,调用运行函数f3,打印f3,返回值为None

print(result)  # 打印None
# 即: 依次打印 f2  ,f1  ,f3  ,None


# 12.看代码分析结果

name = '景女神'

def func():
    def inner():
        print(name)
    return inner()  # func函数返回值为inner()的返回值,为None

v = func()      # '景女神',在全局变量找到变量名 name
print(v)        # 打印 None

"""



# 13.看代码分析结果
"""
name = '景女神'
def func():
    def inner():
        print(name)
        return "老男孩"
    return inner()     # func的返回值是inner()的返回值,是 "老男孩"

v = func()   #调用运行函数func(),闭包函数,打印'景女神'.返回值是 "老男孩".
print(v)   # 打印"老男孩".
"""





# 14.看代码分析结果
"""
name = '景女神'
def func():
    def inner():
        print(name)
        return '老男孩'
    return inner   # func函数返回值为函数名inner.

v = func()   #调用运行func(),不做事,v = inner
result = v()   # 调用运行inner()函数,打印 '景女神',返回值为'老男孩'
print(result)  # 打印'老男孩'
# 即 打印  '景女神'  '老男孩'
"""



#  15.看代码分析结果
"""
def func():
    name = '武沛齐'
    def inner():
        print(name)
        return '老男孩'
    return inner

v1 = func()  #  不打印,返回值为函数名inner
v2 = func()  #  不打印,返回值为函数名inner
print(v1, v2)     #v1 v2的内存地址,且不一样
"""

# 16.看代码写结果
"""
def func(name):
    def inner():
        print(name)
        return '老男孩'
    return inner

v1 = func('金老板')     #  funv函数,返回值为函数名inner.没有执行inner函数.
v2 = func('alex')       #  funv函数,返回值为函数名inner.没有执行inner函数.
print(v1, v2)    #   inner的内存地址
"""


# 17. 看代码写结果
"""
def func(name=None):
    if not name:
        name = '武沛齐'

    def inner():
        print(name)
        return '老男孩'
    return inner

v1 = func()     # v1 = inner
v2 = func('alex')   # v2 = inner
print(v1, v2)    #  2个内存地址
"""


# 18.看代码写结果【面试题】
"""
def func(name):
    v = lambda x: x + name
    return v               

v1 = func('武沛齐')    # 返回值是函数名 v
v2 = func('alex')      # 返回值是函数名 v
v3 = v1('银角')        # 调用v函数,  "银角武沛齐"
v4 = v2('金角')        # 调用v函数,  "金角alex"
print(v1, v2, v3, v4)    # 先打印2个函数内存地址 , 然后打印"银角武沛齐"   "金角alex"

"""


# 19.看代码写结果
"""
NUM = 100   # 全局变量
result = []
for i in range(10):
    func = lambda: NUM      # 注意：函数不执行，内部代码不会执行。
    result.append(func)
                              # 循环结束,i = 9

print(i)           # 打印9
print(result)      # [func的内存地址1,```````func的内存地址8,func的内存地址9,func的内存地址10]
v1 = result[0]()   #返回值NMU = 100
v2 = result[9]()   #返回值NMU = 100
print(v1, v2)      #打印100  100
"""


# 20.看代码写结果【面试题】
"""
result = []
for i in range(10):
    func = lambda: i     # 注意：函数不执行，内部代码不会执行。
    result.append(func)

print(i)           # 9
print(result)      # [9个函数func指向的内存地址]
v1 = result[0]()   # # 9
v2 = result[9]()   # # 9
print(v1, v2)
# 打印 9  ,[9个函数func指向的内存地址]  ,9   ,9
"""


# 21.看代码分析结果【面试题】

def func(num):
    def inner():
        print(num)
    return inner          # 返回值函数名   inner---->'内存地址',此时没执行

result = []
for i in range(10):
    f = func(i)            # 循环一次,执行一次func,func函数作用域创建,赋值 num = i . 循环完结,在10个func作用域.f -->指向inner函数内存地址
    result.append(f)      # [10个函数名inner指向的内存地址]

print(i)               #       9
print(result)          #    [10个函数名  inner  指向的内存地址]
v1 = result[0]()       #   调用函数inner ,打印   1
v2 = result[9]()       #   调用函数inner ,打印   9
print(v1, v2)          #   打印  None   None


# 22.程序设计题
"""
> 请设计实现一个商城系统，商城主要提供两个功能：商品管理、会员管理。
>
> 商品管理：
>
> - 查看商品列表
> - 根据关键字搜索指定商品
> - 录入商品
>
> 会员管理：【无需开发，如选择则提示此功能不可用，正在开发中，让用户重新选择】

需求细节：

1.
启动程序让用户选择进行商品管理
或
会员管理，如：
![输入图片说明](https: // images.gitee.com / uploads / images / 2019 / 0412 / 171126_0567693
f_73459.png
"屏幕截图.png")
2.
用户选择 【1】 则进入商品管理页面，进入之后显示商品管理相关的菜单，如：
![输入图片说明](https: // images.gitee.com / uploads / images / 2019 / 0412 / 171201_47
d7aa64_73459.png
"屏幕截图.png")
3.
用户选择【2】则提示此功能不可用，正在开发中，让用户重新选择。
4.
如果用户在【商品管理】中选择【1】，则按照分页去文件
goods.txt
中读取所有商品，并全部显示出来【分页功能可选】。
5.
如果用户在【商品管理】中选择【2】，则让提示让用户输入关键字，输入关键字后根据商品名称进行模糊匹配，如：
![输入图片说明](https: // images.gitee.com / uploads / images / 2019 / 0412 / 171227_049
ea3f5_73459.png
"屏幕截图.png")
6.
如果用户在【商品管理】中选择【3】，则提示让用户输入商品名称、价格、数量
然后写入到
goods.txt
文件，如：
![输入图片说明](https: // images.gitee.com / uploads / images / 2019 / 0412 / 171251
_b1080b58_73459.png
"屏幕截图.png")
"""


import  hashlib
import getpass
import pypinyin
p = pypinyin     #  调 用 汉 字 转 拼 音 模 块,网上下载的.

###### 1. 商品管理
def func_goods():       #   商品管理
    print("""******欢迎使用老子的购物商城【商品管理】******
    1. 查看商品列表
    2. 根据关键字搜索指定商品
    3. 录入商品""")
    choice = (input('请选择(输入N返回上一级):')).strip()
    info1 = {'1':look_goods,'2':check_key,'3':enter_goods}
    if choice == 'N':return        # 返回上一级(死循环),不需返回值,终止函数
    elif choice in info1: info1.get(choice)()           # 判断
    else:print('输入错误,请重新输入');return func_goods()# if 条件的冒号后可以直接跟代码 ,多行语句的代码用';'隔开
# ****************  这里用return func_goods(),相当于又执行一次func_goods()函数,

# 1.1 商品列表功能(默认10行一页)
def look_goods():
    print("""******欢迎使用老子的购物商城【商品管理】【查看商品列表】******""")
    page = input('请输入查看的页码:(输入N返回上一级)')
    if page.strip() == 'N':return func_goods()
    elif not page.strip().isdecimal():print('输入错误字符,请重新输入') ; return look_goods()
    page = int(page)
    with open('goods.txt','r',encoding='utf-8') as file:
        data = file.readlines()
        a, b = divmod(len(data), 10)    #   列表长度是行数,规定10行一页
        if b != 0:
            a += 1           # 确定总共有多少页---->a
        if page > a or page < 1:print('输入页码必须在1～%d页之间,请重新输入页码' %a);return look_goods()
        content = data[page*10-10:page*10-1]
        content = ''.join(content)
        print(content)
        return look_goods()

# 1.2 关键字检索
def check_key():
    print("""******欢迎使用老子的购物商城【商品管理】【根据关键字搜索】******""")
    key = input('请输入要查询的关键字(输入N返回上一级)：')    # 只支持一个关键字的搜索
    if key == 'N':return func_goods()   # 返回上一级
    print('***搜索结果如下***')
    with open('goods.txt','r',encoding = 'utf-8') as file:
        for line in file:          # 遍历所有商品,商品中含有key关键字的商品全部打印出来,
            line = line.strip()
            line = line.split('|')
            pinyin = p.lazy_pinyin(line[0])     # 把商品汉字的转换为不带声调的列表,元素是每个汉字的拼音字符串
            key_pinyin = p.slug(key.strip())      # 把关键字转为'pingyin'字符串
            if key_pinyin in pinyin:
                print(line)
    return check_key()

# 1.3 录入商品功能
def enter_goods():
    print("""******欢迎使用老子的购物商城【商品管理】【录入商品】******""")
    while True:
        name = input('请录入商品名称(输入N则停止录入,并返回上一级):')
        if name == 'N':return func_goods()     # 终止当前循环,回到上级循环了.即返回上一级.
        price = input('请录入商品价格:')
        count = input('请录入商品数量:')
        with open('goods.txt','a',encoding='utf-8') as file:
            data = name +'|'+ price + '|' + count + '\n'    # 在goods.txt中,数据以|分割.
            file.write(data)
        print('添加成功')

#######  2 会员管理(实现登录/注册)
def vip_charge():
    print("******欢迎使用老子的购物商城【会员管理】【登录/注册】******")
    choice = input('1 .会员登录\n2 .会员注册(输入N返回上一级)\n******请输入你选择的功能:******:')
    if choice == 'N':return     #     停止运行当前函数,返回上一级,默认返回值 None
    elif choice != '1' and choice != '2':print('输入错误字符,请重新输入') ; return vip_charge()
    if choice.strip() == '1':
        if login():print('登录成功')
        else:print('登录失败,重新开始登录') ; return vip_charge()
    elif choice.strip() == '2':
        if register():
            print('注册成功')
            choice1 = input('***您是否现在登录?***\n请输入Y or N:')
            if choice1.strip() == 'Y': return vip_charge()
            else:return   #  停止运行当前函数,返回上一级,默认返回值 None

# 2.1登录函数
def login():
    user = input("请输入登录用户名:")
    pwd = input("请输入登录密码")   #  getglass.getglass在window上py3的 啥都没有
    with open('VIP_data.txt','r',encoding='utf-8') as file:
        for line in file:
            line = line.strip().split('|')
            if user == line[0] and md5(pwd) == line[1]:return True

# 2.2注册功能
def register():
    user = input("请输入注册用户名:")
    pwd = input("请输入登录密码")  # getglass.getglass在window上py3的 啥都没有
    with open('VIP_data.txt','a+',encoding='utf-8') as file:
        for line in file:
            line = line.strip().split('|')
            if user.strip() == line[0]:print('用户名重复,请重新输入用户名和密码');return register()
        content = ''.join([user, '|', md5(pwd), '\n'])
        file.write(content)
        return True

# 2.3加密会员密码
def md5(pwd):
    hash = hashlib.md5('加盐的字符串'.encode('utf-8'))
    hash.update(pwd.encode('utf-8'))
    return hash.hexdigest()

########   主程序   #######
while True:
    print("""******欢迎使用老子的购物商城******
    1. 商品管理
    2. 会员管理""")
    info ={'1':func_goods,'2':vip_charge }
    content = (input("请选择(输入N退出系统):")).strip()
    if content == 'N':break
    elif content in info : info.get(content)()
    else : print('输入错误代码,请重新输入')