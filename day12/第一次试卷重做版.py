#!/usr/bin/env python
# -*- coding:utf-8 -*-



"""
# 1 linux/window/mac  ,线上公司服务器用centos
# 2 .8bit = 1 字节
# 3 .is比较内存地址是否一致   ,== 比较值是否相等
# 4 不做任何事,占位符
# 5  1.print ''   print(' ')
     2.raw_input('')    input('')
    3 .int和long   只有int
    4. 整除不保留小数,   整除保留所有
    5 `ascii码     utf-8码
# 6` 1 由数字字母下划线组成
        2.数字不能开头
        3. 避开py的关键字
```     4.见名知意,单词间下划线连接
# 7 .浅拷贝,只拷贝第一层
        深拷贝,拷贝到嵌套层次的所有可变类型
# 8. 解释型语言:代码从上到下一行行执行,边解释边执行,实时翻译
    编译型语言:编译器把代码编译完后,得到一个中间文件,然后交予计算机执行.for
#9 .str  upper lower  join strip split replace isdigit isdecimal center
    list  append insert  pop remove clear reverse sort extend
    bool
    int
    tuple
    dict keys()  values()   items()  get()   update()  discard()
    set add update remove intersection difference  union
"""
# 10. 0 '' [] {} () set()
# 11.
"""
v1 =(1)   int 
v2 = (1,)  元组
v3 = 1     int
 """
# 12.

"""
v4 = '全栈21期'[::-1]
print(v4)
# 13.

info[0]['k1']
info[0]['k2']['k9']
info[2][3]
info[0]['k2']['k10']
for i in info[1]:
    print(i)
info[4][-1].update({True:'真'})
info[-1][-1]['extra'][2].append(69)

# 14.
false
1
0
8 or 4 = 8
'alex' 

# 15 .
v5 =" k1|v1,k2|v2,k3|v3"
info = {}
v5 = v5.strip().split(',')
for i in v5:
    item = i.split('|')
    info.update({item[0]:item[1]})
print(info)

# 16 .

content = input('请输入数相乘:')
content = content.split('*')
s = 1
for i in content:
    i = i.strip()
    i = int(i)
    s *= i
print(s)

# 17
 # 18
info = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
list = []
for i in range(0, len(info), 2):
    list.append(str(info[i]))
print('*'.join(list))
with open('a.log','w',encoding='utf-8-sig') as file :
    file.write('*'.join(list))
"""

# 19
keys = ['苍老师','小泽老师','Alex']
with open ('a.log','r',encoding='utf-8') as file , open('a.txt','a',encoding='utf-8') as file1 :
    for line in file:
        for i in keys :
            if i in line:
                line_new = line.replace(i,'***')
                file1.write(line_new)

# 20
cars = ['鲁A32444', '鲁B12333', '京B8989M', '黑C49678', '黑C46555', '泸B25041', '黑C34567']
info = {}
for i in cars :
    if not info.get(i[0]):
        info[i[0]] = 1
    else:
        info[i[0]] += 1
print(info)


# 另一种方法
cars = ['鲁A32444', '鲁B12333', '京B8989M', '黑C49678', '黑C46555', '泸B25041', '黑C34567']
info1 = {}
for i in cars :
    info1[i[0]] = info1.get(i[0],0) + 1
print(info1)

# 22 打印99乘法表
for i in range(1,10):
    for j in range(1,i+1):
        print('%s*%s' %(i,j) ,end= ' ')
    print('')   # 理解了
