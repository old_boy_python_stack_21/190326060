# Python学习笔记

## 第一章 计算机基础

### 1.1  硬件

计算机基本的硬件由：CPU / 内存 / 主板 / 硬盘 / 网卡  / 显卡 / 显示器 等组成，只有硬件但硬件之间无法进行交流和通信。

### 1.2 操作系统

操作系统用于协同或控制硬件之间进行工作，常见的操作系统有那些:

- windows系统,应用最广泛的操作系统.
  - win xp 系统
  - win 7系统
  - win 10 系统
- linux系统,免费开源,占用内存小,运行速度快
  - centos .公司线上服务器使用,图形界面较ubuntu差
  - ubuntu,用于开发.图形界面较好
  - renhat,主要用于企业级服务器
- mac(苹果系统,对办公和开发都很好)

### 1.3  解释器或编译器

编程语言的开发者写的一个工具，将用户写的代码转换成010101交给操作系统去执行。

#### 1.3.1  解释和编译型语言

解释型语言就类似于： 实时翻译，代表：Python / PHP / Ruby / Perl

- 特点:写完代码交给解释器，解释器会从上到下一行行代码执行：边解释边执行。

编译型语言类似于：说完之后，整体再进行翻译，代表：C / C++ / Java / Go ...

- 特点:代码写完后，编译器将其变成成另外一个文件，然后交给计算机执行。

#### 1.3.2 学习编程语言

1. 安装解释器或编译器,工具准备.
2. 学习该语言的语法规则

### 1.4 软件（应用程序）

软件又称为应用程序，就是我们在电脑上使用的工具，类似于：记事本 / 图片查看 / 游戏.是由程序员编写的.

### 1.5 进制

对于计算机而言无论是文件存储 / 网络传输输入本质上都是：二进制（010101010101ainc），如：电脑上存储视频/图片/文件都是二进制； QQ/微信聊天发送的表情/文字/语言/视频 也全部都是二进制。

进制：

- 2进制，计算机内部。二进制数据是用0和1两个数码来表示的数。它的基数为2，进位规则是“逢二进一”

  计算机内部都是以二进制存储数据的.

- 8进制: 采用0，1，2，3，4，5，6，7八个数字，逢八进1 .

- 10进制，人来进行使用一般情况下计算机可以获取10进制，然后再内部会自动转换成二进制并操作。

- 16进制，一般用于表示二进制（用更短的内容表示更多的数据），一般是：\x 开头。

| 二进制 | 八进制 | 十进制 | 十六进制 |      |
| ------ | ------ | ------ | -------- | ---- |
| 0      | 0      | 0      | 0        |      |
| 1      | 1      | 1      | 1        |      |
| 10     | 2      | 2      | 2        |      |
| 11     | 3      | 3      | ```      |      |
| 100    | ```    | ```    | 9        |      |
|        | 7      | 8      | A        |      |
|        |        | 9      | ```      |      |
|        |        |        | F        |      |



## 第二章 Python入门

### 2.1 环境的安装

- 下载软件(官网下载)并安装软件
  - python 2.7.16 （2020年官方不在维护）
  - python 3.6.8 （推荐）

- 解释器：py2 / py3 （环境变量）,添加环境变量,以便以后快速调用程序![截图python0408135813](Python学习笔记.assets/截图python0408135813.png)
- 开发工具：pycharm的安装,激活(界面的调整,防止伤眼睛)

### 2.2 编码

#### 2.2.1 编码基础

- ascii码:表示英文和标点符号,1字节表示一个字符.
- unicode码:能表示世界上所有的语言,4字节表示一个字符,现在用到了27位bit.
- utf-8码:对unicode码的压缩,中文3个字节表示.
- gbk码:亚洲地区使用,gb2312码的升级版,2字节表示中文.
- gb2312码:亚洲地区使用,2字节表示中文.

#### 2.2.2 python编码相关

对于Python默认解释器的编码：

- py2： ascii
- py3： utf-8

如果想要修改默认编码，则可以使用：

```python
# -*- coding:utf-8 -*- 
```

**注意：对于操作文件时，要按照：以什么编写写入，就要用什么编码去打开。**

在linux系统中,py的文件开头有:

```python
#!/usr/bin/env python  在Linux中指定解释器的路径
# -*- coding:utf-8 -*-
```

运行： 解释器   文件路径 

在linux上有一种特殊的执行方法：

- 给文件赋予一个可执行的权限
- ./a.py  自动去找文件的第一行 =  /usr/bin/env/python  a.py

#### 2.2.3单位换算：

​	             8 bit = 1 bype

​                    1024 bype = 1 KB

​                    1024 KB = 1 MB

​                    1024 MB = 1 GB

​                    1024 GB = 1 TB

### 2.3 变量

问：为什么要有变量？

为某个值创建一个“外号”，以后在使用时候通过此外号就可以直接调用。

#### 2.3.1变量的命名规则

1. 变量名由数字,字母和下划线组成.

2. 变量名不能以数字开头

3. 变量名要避开python的关键字,如[‘and’, ‘as’, ‘assert’, ‘break’, ‘class’, ‘continue’, ‘def’, ‘del’, ‘elif’, ‘else’, ‘except’, ‘exec’, ‘finally’, ‘for’, ‘from’, ‘global’, ‘if’, ‘import’, ‘in’, ‘is’, ‘lambda’, ‘not’, ‘or’, ‘pass’, ‘print’, ‘raise’, ‘return’, ‘try’, ‘while’, ‘with’, ‘yield’]等等.

4. 建议:  见名知意:用简单明了,意思相近的单词做变量名.

   ​          单词间用下划线连接,如变量名: deng_dad.

### 2.4 python基础语句

#### 2.4.1输出/输入语句

1. 输出语句

```python
print（你想输出的内容）
```

python2中，输出是: print ”你想输出的“（注意：print和引号间有空格）

python3中，输出是: print（“你想输出的”）

2. 输入语句输入

   input语句：

   ```python
   name=input('请输入你的用户名:')
   password=input('请输入你的密码')
   print(content)
   print(password)
   ```

   注意：

   1. input语句输入得到的内容永远是字符串。
   2. python2的输入语句是:raw_input('')。
   3. python3的输入语句是;input('')。

#### 2.4.2编程的注释

编程代码一定要做注释，注释不参与代码运行,编程代码行数太多了。分为二类，如

```python
# 单行注释，不参与代码运算

"""
多行注释，
不参与程序运算
"""
```

#### 2.4.3条件判断语句

- 最简单条件判断

```python
age = input('请输入你的年龄：')
new_age=int(age)
# input输入的数据类型是字符串，需要用int语句把字符串数据转化为整型数据。
if new_age >= 18:
	print('你已经是成年了人了')
```

- 初级语句

```python
gender = input('请输入你的性别：')
# 默认不是男性就是女性
if gender == '男':
	print('走开')
else:
	print('来呀，快活呀')
```

- elif语句

```
gender = input('请输入你的性别：')
# 性别有男、女、人妖多种选择
if gender == '男':
	print('走开')
elif gender == '女':
	print('来呀，快活呀')
else：
	print（'找##去，他是gay'）
```

elif语句可以用无限次使用，如果次数过多会有其他语句使用,语句过于冗长.

- and语句，python的关键字之一，表示并且的意思。

#### 2.4.4循环语句

- while 用法

  ```python
  num = 1
  while num <= 10:
      print(num)
      num += 1
  ```

- while else 用法

  ```python
  num = 1
  while num <= 10:
      print(num)
      num += 1
  else:
      print("end")
  # else 表示while循环语句不满足组条件后执行的代码
  
  #  pass  占位符,不做任何事.
  ```

- break、continue关键字的用法,以及与if   pass语句的嵌套

#### 2.4.5运算符

- 算术运算符:加减乘除的运用，+、-、*、/ 在程序中用于算术运算。还有类似于：

  1. % ，取除法的余数，如15%4 = 3
  2. // ，取除法的商的整数，如20//6 = 3
  3. ** ，取指数的值，如2**8 = 2的8次方，为256.

- 赋值运算符：

  1. c += 1 等价于 c = c+ 1

  2. c -= 1等价于c = c - 1

  3. c *= 2 等价于 c = c * 2

     等等诸如此类

- 逻辑运算符: and 、or、not

  1. 一般用法：表示逻辑中的于、或、非，用于条件的判断

  2. 二般用法：

     - 3种数据类型的转化，int str boolen的转化

       ```python
       test=bool('')
       test1=bool(0)
       print(test)
       print(test1)
       #####   注意：只有空字符串和0转化为布尔值时为false，否则都为 true
       ```

     - ```
       value = x and y
       print(value)
       ####: 从左到右， x转化为布尔值为真，value = y，否则value= x 。
       value1 = x or y
       print(value1)
       ####: 从左到右， x转化为布尔值为真，value1 = x，否则value= x 。
       ```

     - 运算的优先顺序为： ( ) >not > and >or

- in ，逻辑运算符，判断某字符或某字符串是否在一个大的字符串中，输出得到bool型数据。

  ```python
  value = '我是中国人'
  v = '我'
  if v in value:
      print(v)
  else:
      print('出错')  #   我
  ```

  

## 第三章 数据类型

### 3.1 整型（int）

#### 3.1.1 整型的长度

py2中有：int有取值范围，对于32位系统而言：-2^31~2^31-1

​                                          对于64位系统而言：-2^63~2^63-1

​       超出范围后，py2自动转换成long(长整型)数据。

py3中有：int （int/long）只有int数据.

#### 3.1.2 整除

py2和py3中整除是不一样.

- py2:整除只保留整数,小数位会舍去.若要保留小数.则在文件头加

  ```python
  from __future__ import division
  ```

- py3整除保留所有.

### 3.2 布尔（bool）

布尔值就是用于表示真假。True和False。

其他类型转换成布尔值：只有''/0/[]/{}/()/set()转换为bool值为False,其余都是True.

### 3.3 字符串（str）

字符串是写代码中最常见的 :

- 单引号，如'王飞'
- 双引号，如”王大“
- 三引号，如“”“王小”“”，三引号支持换行。

注意：整型数据可以+和×，字符串数据也可以+和×。如

```python
name='五五开'
new_name=name*3
print（new_name）   #  '五五开五五开五五开'
```

python内存中的字符串是按照：unicode 编码存储。对于字符串是不可变数据类型。

#### 3.3.1字符串的格式化

1. 字符串格式化的意义，大部分字符过于冗长，使用字符串格式化能大大加快效率，方便程序员调用数据。

2. %s 、 %d、%%

   - ```python
     red_dad = '大红的爸爸'
     do = '教学生上课'
     thing = '%s在操场%s' %(red_dad，do,)
     print(thing)
     ```

   - 直接做占位符

     ```python
     temper = '%s在太空中%s' %('等大侠','打飞机',)
     print(temper)
     thing = '盖伦，年龄%d，喜欢在池塘里%s' %(15,'打水仗'，)
     print(thing)
     #####  #s和#d表示的类型不同，前者表示字符串数据，后者表示整型数据。
     name = '小明'
     template = "%s拿出了100%%的力气" %(name,)
     print(template)
     ######   %%，为了和字符串格式化做区分，百分号要写成%%形式。
     ```

#### 3.3.2字符串的方法

字符串自己有很多方法，如：

1. 大写： upper/isupper

   ```python
   v = 'DEng'     
   v1 = v.upper()    # DENG
   print(v1)
   v2 = v.isupper() # 判断是否全部是大写
   print(v2)  # False
   ```

2. 小写：lower/islower

   ```python
   v = 'yixin'
   v1 = v.lower()
   print(v1)
   v2 = v.islower() # 判断是否全部是小写
   print(v2)
   
   
   ############ 了解即可
   v = 'ß'  #德语大小写的转换
   # 将字符串变小写（更牛逼）
   v1 = v.casefold()
   print(v1) # ss   
   v2 = v.lower()
   print(v2)
   #lower() 方法只对ASCII编码，也就是‘A-Z’有效，对于其他语言（非汉语或英文）中把大写转换为小写的情况只能用 casefold() 方法。
   ```

3. 判断是否是数字： isdecimal(推荐使用)

   ```python
   v = '1'
   # v = '二'
   # v = '②'
   v1 = v.isdigit()  # '1'-> True; '二'-> False; '②' --> True
   v2 = v.isdecimal() # '1'-> True; '二'-> False; '②' --> False
   v3 = v.isnumeric() # '1'-> True; '二'-> True; '②' --> True
   print(v1,v2,v3)
   # 以后推荐用 isdecimal 判断是否是 10进制的数。
   
   # ############## 应用 ##############
   v = ['deng','yi','xin']
   num = input('请输入序号：')
   if num.isdecimal():
       num = int(num)
       print(v[num])
   else:
       print('你输入的不是数字')
   ```

4. split() ,去空白+\t+\n + 指定字符串

   ```python
   # 注意;该方法只能删除开头或是结尾的字符，不能删除中间部分的字符.类似的有rstip/lstrip,从字符串右端/左端来操作.
   v1 = "alex "
   print(v1.strip())
   v2 = "alex\t"    #\t等价于 Tab键,4个空格
   print(v2.strip())
   v3 = "alex\n"     #\n等价于 换行符
   print(v3.strip())
   v1 = "dnengyixin"
   print(v1.strip('dn'))  #  输出'engyixi'     # 只要头尾包含有指定字符序列中的字符就删除
   ```

5. 替换 replace

   ```python
   v = 'dengyixin'
   v1 = v.replace('i','123')
   print(v1)  #  dengy123x123n  ,从左到右替换所有字符
   v = 'dengyixindengyixin'
   v1 = v.replace('i','123',3)
   print(v1)  # dengy123x123ndengy123xin  ,从左到右替换前 3 个字符字符.
   ```

6. 开头 / 结尾startswith/endswith

   ```python
   v = 'dengyixindengyixin'    #判断以``字符开头/结尾
   flag1 = v.startswith('de')
   flag2 = v.endswith('in')
   print(flag1,flag2)   #  True  True
   ```

7. 编码encode，把字符串转换成二进制

   ```python
   
   v = '邓益新'   # 解释器读取到内存后，按照unicode编码方式存储
   v1 = v.encode('utf-8')
   print(v1)               #输出'邓益新'以utf-8码的二进制
   v2 = v1.decode('gbk')
   print(v2)    #报错,编码问题,以什么码写,就用什么码读.
   # 解码 decode，把二进制转换成字符串
   
   ```

8. format(格式化)

   ```python
   name = "我叫{0},年龄:{1}".format('老男孩',73)    #  对应的是索引位置
   print(name) 
   name = '我是{x1},爱{xx}'.format_map({'x1':"邓",'xx':18})    #format_map对应的键
   print(name)
   print(name)
   ```

9. join

   ```python
   name = 'dengixn'
   result = "_".join(name)       # 循环每个元素，并在元素和元素之间加入'_'。
   print(result)    #  d_e_n_g_i_x_n
   ```

10. split 

   ```python
   str.split("根据什么东西进行切割"，从左到右对前多少个东西进行切割)
   str.rsplit("根据什么东西进行切割"，从右到左对前多少个东西进行切割)
   name = 'dengixindeng'
   name_new = name.split('n')  #不填数字默认切割所有,从左到右切割.
   print(name_new)  #  ['de', 'gixi', 'de', 'g']
   ```

### 3.4 列表

#### 3.4.1列表介绍

```python
users = ["代永进","李林元","邓益新",99,22,88]   #表示多个事物，用列表.是有序的,可变类型
```

#### 3.4.2列表方法

1. append，在列表的最后追加一个元素

   ```python
   users = []
   while True:
       name = input('请输入姓名:')   #  利用无限循环添加用户名到列表users
       users.append(name)
       print(users)
   ```

2. insert,在列表索引位置添加元素

   ```python
   users = ["代永进","李林元","邓益新",99,22,88]
   users.insert(2,'大笨蛋')    #  在列表索引2位置添加'大笨蛋'
   ```

3. remove,从左到右删除列表第一个元素

   ```python
   users = ["代永进","李林元","邓益新",99,22,88]
   users.remove("邓益新")     #   从列表中从左到右删除第一个"邓益新",
   ```

4. pop,删除列表索引位置元素,可以赋值给一个新变量,得到被删除的值.

   ```python
   users = ["代永进","李林元","邓益新",99,22,"邓益新",88]
   result =  users.pop(2)
   print(result,users)#   从列表中删除对应索引位置 /邓益新 ['代永进', '李林元', 99, 22, '邓益新', 88]
   
   ```

5. clear,清除列表所有元素

   ```python
   users = ["代永进","李林元","邓益新",99,22,88]
   users.clear()    #   从列表清除所有元素    ,得到[]
   ```

6. reverse,反转列表元素

   ```python
   users = ["代永进","李林元","邓益新",99,22,88]
   users.reverse()
   print(users)  #  [88, '邓益新', 22, 99, '邓益新', '李林元', '代永进']
   ```

7. sort,从小到大排列列表元素

   ```python
   users = [1,2,3,8,4,6,5,9,0]
   users.sort()   # ()里默认reverse = False
   print(users)   # [0, 1, 2, 3, 4, 5, 6, 8, 9]
   users.sort(reverse = True)     # 从大到下排列列表元素
   ```

8. extent.用于在列表末尾一次性追加另一个序列中的多个值 

   ```python
   users = ["代永进","李林元","邓益新",99,22,88]
   users.extend('deng')
   users.extend([11,22,33])
   users.extend((44,55,66))
   users.extend({77,88,99})
   print(users)   # ['代永进', '李林元', '邓益新', 99, 22, 88, 'd', 'e', 'n', 'g', 11, 22, 33, 44, 55, 66, 88, 99, 77],在后面添加.
   ```

### 3.5元组tuple

#### 3.5.1元组的特点

1. ```python
   users = ["代永进","李林元","邓益新",99,22,88] # 列表可变类型[]
   users = ("代永进","李林元","邓益新",99,22,88) # 元组不可变类型()
   ```

2. 元组没有自己独有的方法.

### 3.6字典dict

#### 3.6.1字典介绍

- 是一种可变容器模型，且可存储任意类型对象 ，帮助用户去表示一个事物的信息。各种属性。

- 用{}表示,如

  ```python
  d = {key1 : value1, key2 : value2,键:值 }    #  键值对
  # 键一般是唯一的，如果重复最后的一个键值对会替换前面的。
  # 值可以取任何数据类型，但键必须是不可变的，如字符串，数字或元组，键不可以是列表和字典。
  ```

#### 3.6.2字典方法介绍

1. keys / values / items /

   ```python
   dict = {'Name': 'deng', 'Age':18, 'Class': 'First'}
   print(dict.keys())   #获取dict中所有键  
   print(dict.values())   ##获取dict中所有值
   print(dict.items())    #获取dict中所有键值对
   ```

2. ***get(key, default=None)，获取某键的值，如果字典中不包含该键，就返回得到None或自定义内容***

   ```python
   info = {'k1':'v1','k2':'v2'}
   v1 = info.get('k1')       #   v1
   v2 = info.get('k123456')    #  None,数据类型，该类型表示空（无任何功能，专门用于提供空值）字典中不存在该键是默认得到None
   v3 = info.get('k123478','不存在该键')  #不存在该键时,可自定义返回的值
   print(v1,v2,v3)   #    v1 None 不存在该键  
   ```

3. pop(key),删除字典的键值对.如果字典中没有该键，会报错。

   ```python
   info = {'k1':'v1','k2':'v2'}
   result = info.pop('k1')      #返回得到  'v1'
   print(info)                #{'k2':'v2'}
   ```

4. .update(), 字典添加元素,可添加多个.

   ```python
   info = {'k1':'v1','k2':'v2'}
   info.update({22:33}) 
   info.update({'k1':'123456'})   #  没有的键，键值对就添加进去，有的键，键对应的值就更新
   print(info)   #  {'k1': '123456', 'k2': 'v2', 22: 33}
   ```

### 3.7集合set()

#### 3.7.1集合介绍

- 集合无序，元素唯一，不允许重复.
- 用{},为和字典做区分,空集合表示为set(),类似于d = {0,1,2,3,4}或者d = set()
- 引申:
  - 其他数据类型的也可以类似表示,如:srt()/list()/tuple()/dict()/int()等等

#### 3.7.2集合方法

1. add,为集合添加单个元素

   ```python
   tiboy = set()
   tiboy.add('boy')
   print(tiboy)  #  tiboy = {1,2,3,4,56,7,8,9,'boy'}  不存在元素就添加.存在该元素集合不变/
   ```

2. .update(x), ,添加元素.x 可以是列表，元组，字典

   ```python
   print(tiboy)    #  update  x 可以是列表，元组，字典。
   tiboy.update([(1,2,3,4,5,6,7),'deng','yi','xin',1,2,3])
   print(tiboy)   
   #  {1, 2, 3, 4, 7, 8, 9, 'xin', 'deng', 'yi', 56, (1, 2, 3, 4, 5, 6, 7), 'boy'}   列表/字典/集合不能放在集合中，但元组可以。
   ```

3. .discard(x)  ,移除集合的元素x，若集合没有x，返回值为None，区别于.remove(x) ,若集合没有x,则会报错。.clear()  清除集合所有元素。

   ```python
   tiboy = {1,2,3,4,56,7,8,9,'boy'}
   tiboy.discard(1)
   print(tiboy,t)   # {2, 3, 4, 7, 8, 9, 'boy', 56}
   ```

4. .intersection()    /     .union( )      /     .difference()   /.symmetric_difference（求交集，并集 差集)

   ```python
   A = {1,2,3,4,5,6}
   B = {5,6,7,8,9,10}
   集合1和2的并集 =  A.union(B)
   print(集合1和2的并集)    #A∪B
   集合1减去2的差集 = A.difference(B)
   print(集合1减去2的差集)    # 集合A-B
   集合1和2的交集 = A.intersection(B)
   print(集合1和2的交集)    #A∩B
   deng123 = A.symmetric_difference(B)
   print(deng123)      #  A∪B-A∩B
   ```

### 3.8 公共功能

- len(),求长度.
  - str,求字符的个数.len("deng") = 4 / len("邓益新") = 3
  - 对列表/元组/集合,求元素的个数
  - 对字典,求键值对的个数
- 索引
  - 从左到右,从0开始,取前不取后.
  - str , 'deng'[0] = 'd'  /  deng'[2] = 'n'   /也可以最后一位,以-1为起始.'deng'[-1]  = 'g'
  - 对列表/元组,类似于字符串,索引对应里面的元素,   因集合无序,不具有索引功能.
  - 对字典,索引的是它的键,得到键对应的值.  {}[key] = value
- 切片
  - 通过索引取单个元素,切片可以取一片
  - str ,'deng'[0:2] = 'den'   ,其他列表/元组与此类似.
  - 字典无切片功能,集合无序,没有切片功能.
- 步长
  - str ,'dengyixin'[::2]  = 'dnyxn'  每2个字符取前一个,步长为2.步长为负数,则反转,从右到左切片.
  - list / tuple / 都与此类似.
  - 字典和集合没有此功能
- for循环
  - for i in str/list/tuple/set ,都是对里面的元素循环一次

  - for item in dict,这里指的是字典的键.

  - 注意：for和while的应用场景：有穷尽优先使用for，无穷尽用while。

    ```python
    name = 'dengxin'
    for item in name:
        print(item)    # 竖向打印d e n g x i n
        
    name = 'dengxin'   
    for item in name:
        print(item)
        break           # for循环的break语句
        print('123')
        
    name = 'dengxin'
    for item in name:
        print(item)
        continue        # for循环的continue语句
        print('123')
    ```
- 删除del    del user[索引]     数字/字符串/布尔值除外

### 3.9额外内容

#### 3.9.1判断敏感字符

- str： 使用in 即可判断

- list/tuple

  ```python
  list1 = ['alex','oldboy','deng','xin']
  if 'deng' in list1：
  	print('含敏感字符')   #  同样，对于元组也是这样
  
  ```

- dict

  ```python
  dict1 = {'k1':'v1','k2':'v2','k3':'v3'}
  #默认按照键判断
  if 'x' in dict1 ：     #  判断x是否是字典的键。
  	print('dict1的键包含x')
      
  #判断字典的值x   #第一种
  dict1 = {'k1':'v1','k2':'v2','k3':'v3'}
  if 'x' in list(dict1.values()）:    #  ，强制转化为list判断x是否是字典的值。
      print('dict1的值包含x')   
      
  #判断字典的值v2   #第二种循环
  for v in dict1.values()：
  	if v == 'v2'
      print('存在')
                 
  #判断字典的键值对 k2:v2 是否在其中
  value = list1.get('k2')
  if value == 'v2':
  	print('包含')   
  ```

### 3.10 嵌套

#### 3.10.1列表嵌套

- 列表可以嵌套多层,int、str、bool、list都可以有

#### 3.10.2元组嵌套

- 元组可以嵌套，可以有list数据,多重嵌套

- 元组和列表可混合嵌套

- 注意:元组中的元素(儿子)不可被修改/删除

  ```python
  # 元组可以嵌套，可以有list数据,多重嵌套
  v1 = (11,22,33,(44,55,66),(11,2,(99,88,),[666,999,888],["代永进","李林元","邓益新"]))
  注意：元组中嵌套列表，
       元组：只读列表，可循环查询，可切片。儿子不能改，孙子(列表的元素)可能可以改
  
  v2 = (1,2,3,'alex',[2,3,4,'abnowen'],'deng')
  v2[3] = 666      # 错误
  v2[4][3] = 123   #正确
  print(v2)        #  (1, 2, 3, 'alex', [2, 3, 4, 123], 'deng')
  ```

#### 3.10.3字典嵌套

- **列表/字典/集合（可变类型） -> 不能放在集合中+不能作为字典的key（unhashable）**

- hash函数，在内部会将值进行哈希算法并得到一个数值（对应内存地址），以后用于快速查找。值必须不可变。

- 特殊情况，True和False在集合和字典可能会和0， 1 重复。hash函数对2者得到一样的值。如：

  ```python
  
  info = {0, 2, 3, 4, False, "国风", None, (1, 2, 3)}
  print(info)  # {0, 2, 3, 4, "国风", None, (1, 2, 3)}
  
  info1 = {
       True:'alex',
       1:'oldboy'
   }
  print(info1)      #  {True:'oldboy'}
  ```

### 3.11内存问题

- 变量

  1. 第一次赋值时，即创建它，之后赋值将会改变变量的值。
  2. 变量名本身是没有类型的，类型只存在对象中，变量只是引用了对象而已。

- 对象

  1. 对象是有类型的，例如各种数据类型。
  2. 对象是分配的一块内存空间，来表示它的值。

- 引用

  在Python中从变量到对象的连接称作引用。
  引用是一种关系，以内存中的指针的形式实现。

  1. 简单引用

     ```python
     v1 = [11,22,33]  #解释器创建了一块内存空间（地址），v1指向这里，v1引用此内存位置
     v1 = [44,55,66] 
     v1 = [1,2,3]    # v1被多次赋值，即修改了v1的引用，把v1的指向关系变了，指向改为另一块内存地址。
     v2 = [1,2,3]   #解释器创建了另一块内存空间（地址），v2指向这里。
     v1 = 666   # 同理，v1，v2指向不同的内存地址
     v2 = 666
     v1 = "asdf"   # 特殊，对于赋值给字符串，v1，v2指向一个内存地址                  （字符串不可变）
     v2 = "asdf"
     
     v1 = 1 
     v2 = 1   #特殊：v1，v2指向相同的内存地址
              #解释：1. 整型：  -5 ~ 256 ，py认为它是常用的，有缓存机制，就把v1和v2指向相同的内存地址，以节省内存。
              #####  2. 字符串："alex",'asfasd asdf asdf d_asd'（字符串不可变 ），指向同一内存地址   
              #####  3."f_*" * 3  - 重新开辟内存,一旦字符串*数字,数字不为1的运算，就重新开辟内存空间
     ```

  2. 共享引用

     ```python
     #示例一
     a = 3  #解释器创建了一块内存空间（地址），a指向此处
     b = a  # b也指向次内存地址
     #示例二(内部修改)
     v1 = [11,22,33]   #解释器创建了一块内存空间（地址），v1指向此处
     v2 = v1  #v2也指向这里，如果语句为：v2 = [11,22,33]，则是解释器创建了另一块内存空间（地址），v2指向这里。只是v1和v2的值相同。
     v1.append(123) #v1指向的内存地址添加上来元素666，v2跟着变更。
     print(v2)     # [11,22,33,123] 
     #示例三（赋值）
     v1 = [11,22,33]
     v2 = [11,22,33]
     v1.append(123)
     print(v2)     #  [11,22,33] 
     #示例四（重新赋值）
     v1 = [11,22,33]  # v1指向一个内存地址
     v2 = v1  # v2指向同一个内存地址
     v1 = [44,55,66,77]  #v1的指向关系改变，解释器创建了内存地址，并将v1的指向改为指向它。
     print(v2)  # v2的指向没有变化，输出[11,22,33]
     ```

  3. == 和 is d的区别

     - == 用于比较值是否相等

     - is 用于比较内存地址是否相等/比较指向关系是否相同/比较指向的内存空间是否是同一个。

       ```python 
       id(v1)   # 返回得到 变量名v1指向的内存地址
       ```

     - 小总结

       - 变量是一个系统表的元素，拥有指向对象的连接的空间。
       - 对象是分配的一块内存，有足够的空间去表示它们所代表的值。
       - 引用是自动形成的从变量到对象的指针

### 3.12 深浅拷贝

1. 简介

   ```python
   v = [1,2,3,[4,5,6],7,8,9]
   import copy
   v1 = copy.copy(v)               #浅拷贝
   v2 = copy.deepcopy(v)           #深拷贝
   ```

2. 对str\int\bool类型而言,深浅拷贝没有区别.(不可变)

   ```python
   a = 'deng'   #a指向内存中第一个内存地址
   import copy
   b = copy.copy(a)   #b指向内存中第一个内存地址
   c= copy.deepcopy(a)   #c指向内存中第一个内存地址
   #注意:这里变量abc理应指向不同的内存地址,但因为小数据尺的缘故,内存地址相同.
   ```

3. list/dict/set类型来说,如果没有嵌套,深浅拷贝没有区别.(可变)

   ```python
   v = [1,2,3,[4,5,6],7,8,9]  有嵌套,有7个元素
   v1 = [1,2,3,4,5,6,7,8,9]   没嵌套,有9个元素
   import copy
   v2 = copy.copy(v1)               
   v3 = copy.deepcopy(v1)           
   
   ```

4. 对嵌套的list/dict/set类型来说,深浅拷贝才有意义.

   ```python
   v = [1,2,3,[4,5,6],7,8,9]  有嵌套,有7个元素
   import copy
   v1 = copy.copy(v)                 
   v2 = copy.deepcopy(v)
   ```

   - 对于其中的元素都是不可变类型时，深拷贝和浅拷贝的结果都是一样的，都是只拷贝第一层
   - 对于其中元素存在可变类型时，浅拷贝只拷贝第一层，深拷贝要拷贝所有的可变类型

5. 特殊:tuple元组(不可变)

   - 如果元组中不含有可变类型，同理字符串的深浅拷贝.
   - 如果元组中含有可变类型，同理列表的深浅拷贝.

6. 深拷贝和浅拷贝

   - 浅拷贝：只拷贝第一层
   - 深拷贝：拷贝嵌套层次中的所有可变类型
   - 拷贝只针对可变类型:在内存中重新创建内存空间,对不可变数据类型无法操作.

7. 可变数据类型和不可变数据类型

   - 可变数据类型：在id不变的情况下，value可改变（列表/字典/集合是可变类型，但是字典中的key值必须是不可变类型） 
   - 不可变数据类型：value改变，id也跟着改变。（数字，字符串，布尔类型，都是不可变类型） 



## 第四章 文件操作

### 4.1 文件基本操作

```python
obj = open('路径',mode='模式',encoding='编码')   #  打开文件
obj.write()    #  把内容写入文件
obj.read()     #  读取文件内容
obj.close()    #  关闭文件(保存文件,把内存上的数据写入到文件上-->硬盘上,01010101存储的)

# 3步:1.打开文件.  2. 操作文件   3. 关闭文件
```

### 4.2打开文件

#### 4.2.1语句

```python
file = open('文件路径',mode = 'r',encoding = 'utf-8')
# 文件路径:     D:\文件夹名字\文件夹
# encoding = 'utf-8',以utf-8编码方式打开.
#########或者用另外一种语句:
with open(''文件路径',mode = 'r',encoding = 'utf-8') as file:
#或者 同时打开2个文件.下面的代码需要缩进,且不需要.close()语句.
with open(''文件路径',mode = 'r',encoding = 'utf-8'') as file ,open(''文件路径',mode = 'r',encoding = 'utf-8'') as file2 :
	v1 = file.read()
    v2 = file2.read()
```

#### 4.2.2打开文件的模式(mode)

- r / w / a      只读只写字符串

- r+ / w+ / a+   可读可写字符串

  1. 读取：r，只能读文件,默认的模式.文件不存在就报错.
  2. 写入：w，只能写文件，文件不存在则创建，文件存在则清空内容在写入. .
  3. 追加：a，只能追加,文件不存在则创建，文件存在则不会覆盖，写内容会以追加的方式写 (写日志文件的时候常用 ).
  4. 可读可写：r+
     - 读：默认从0的光标开始读，也可以通过 seek 函数调整光标的为位置
     - 写：从光标所在的位置开始写，也可以通过 seek 调整光标的位置
  5. 可读可写：w+
     - 读：默认光标永远在写入的最后，也可以通过 seek 函数调整光标的位置
     - 写：先清空
  6. 可读可写：a+
     - 读：默认光标在最后，也可以通过 seek 函数 调整光标的位置。然后再去读取
     - 写：永远写到最后

- rb / wb / ab  只读只写二进制

  ```python
  file = open('文件路径',mode = 'wb')  # rb/wb/ab模式,不需要encoding = 'utf-8'
    #注意,如果是/rb/ab/wb模式,写入和读取的必须是二进制,即010100010 010110101 0101000,否则报错.写入数据是
  data = '你好,世界'.encode('utf-8')    #将字符串转化为utf-8编码方式的二进制数据.
  file.write(data)
  file.close()  
  print(v)
  ########
  ```

- r+b / w+b / a+b   可读可写二进制  ,  r+b/w+b/a+b模式同上.

### 4.3 文件操作

- read() , 全部读到内存

- read(1) 

  - 1表示一个字符

    ```python
    obj = open('a.txt',mode='r',encoding='utf-8')
    data = obj.read(1) # 1个字符
    obj.close()
    print(data)
    ```

  - 1表示一个字节

    ```python
    obj = open('a.txt',mode='rb')
    data = obj.read(3) # 1个字节
    obj.close()
    ```

- readlins()  

  ```python
  date_list = file.readlines() #  读取整个文件所有行，保存在一个列表(list)变量中，每行作为一个元素，但读取大文件会比较占内存.
  ```

- 大文件读取(50g的文件,内存没有这么大)

  ```python
  file = open('文件路径',mode = 'r',encoding = 'utf-8')
  ###  2.如果以后读取一个特别大的文件，可以一行一行读取
  for line in file:
      line = line.strip()     #去除换行符(默认去除换行符\n),也可以填其他.
      print(line)             #一行一行读取,
  ```

- write(字符串,utf-8)

  ```python
  obj = open('a.txt',mode='w',encoding='utf-8')
  obj.write('中午你')
  obj.close()
  ```

- write(二进制)

  ```python
  obj = open('a.txt',mode='wb')
  
  # obj.write('中午你'.encode('utf-8'))
  v = '中午你'.encode('utf-8')
  obj.write(v)
  
  obj.close()
  ```

- seek函数:(光标字节位置) **用于移动文件读取光标到指定位置**，无论模式是否带b，都是按照字节进行处理。

  ```python
  obj = open('a.txt',mode='r',encoding='utf-8')
  obj.seek(3) # 跳转到指定3字节位置
  data = obj.read()
  obj.close()
  
  print(data)
  ##################################################
  obj = open('a.txt',mode='rb')
  obj.seek(3) # 跳转到指定字节位置
  data = obj.read()
  obj.close()
  
  print(data)
  ######################################
  fileObject.seek(offset[, whence])
  #  offset -- 开始的偏移量，也就是代表需要移动偏移的字节数.以字节为单位.
  #  whence：可选，默认值为 0。给offset参数一个定义，表示要从哪个位置开始偏移；0代表从文件开头开始算起，1代表从当前位置开始算起，2代表从文件末尾算起。如seek(0,0)
  ```

- tell(), 获取光标当前所在的字节位置

  ```python
  obj = open('a.txt',mode='rb')
  # obj.seek(3) # 跳转到指定字节位置
  obj.read()
  data = obj.tell()
  print(data)
  obj.close()
  ```

- flush，强制将内存中的数据写入到硬盘

  ```python
  v = open('a.txt',mode='a',encoding='utf-8')
  while True:
      val = input('请输入：')
      v.write(val)
      v.flush()      # 避免内存泄漏
  
  v.close()
  ```

### 4.4 关闭文件

一般方法

```python
v = open('a.txt',mode='a',encoding='utf-8')
# 文件操作
v.close()
```

避免忘记输入.close()的方法.

```python
with open('a.txt',mode='a',encoding='utf-8') as v:
    v.write('这是另一种文件打开方法')
	# 缩进中的代码执行完毕后，自动关闭文件
```

### 4.5  文件内容的修改

```python
with open('a.txt',mode='r',encoding='utf-8') as f1:
    data = f1.read()
new_data = data.replace('飞洒','666')

with open('a.txt',mode='w',encoding='utf-8') as f1:
    data = f1.write(new_data)
```

- 大文件修改

```python
f1 = open('a.txt',mode='r',encoding='utf-8')
f2 = open('b.txt',mode='w',encoding='utf-8')

for line in f1:
    new_line = line.replace('阿斯','死啊')
    f2.write(new_line)
f1.close()
f2.close()
```

```python
with open('a.txt',mode='r',encoding='utf-8') as f1, open('c.txt',mode='w',encoding='utf-8') as f2:
    for line in f1:
        new_line = line.replace('阿斯', '死啊')
        f2.write(new_line)
```

## 第五章 函数

### 5.1三元运算/三目运算

```python
v = 前面  if 条件语句  else   后面
#如果条件成立,"前面"赋值给v,否则后面赋值给v.
v = a if a>b  else b  # 取a和b中值较大的赋值给v

# 让用户输入值，如果值是整数，则转换成整数，否则赋值为None

data = input('请输入值:')
value =  int(data) if data.isdecimal() else None 
```

### 5.2 函数

#### 5.2.1.函数介绍

1. 截止目前为止,都是面向过程式编程.可读性差,重复性高,容易出错.

2. 对于函数编程：

   - 本质：将N行代码拿到别处并给他起个名字，以后通过名字就可以找到这段代码并执行。
   - 场景：
     - 代码重复执行。
     - 代码量特别多超过一屏，可以选择通过函数进行代码的分割。

3. 函数是组织好的，可重复使用的，用来实现单一，或相关联功能的代码段。

   函数能提高应用的模块性，和代码的重复利用率.

   py3中给我们提供了许多内建函数,如len()/print()等等,我们也可以自定义函数,这叫做用户自定义函数 .

#### 5.2.2.函数定义和表达

1. 定义如下:

   - 函数代码块以 **def** 关键词开头，后接函数标识符名称和圆括号 **()**。

   - 任何传入参数和自变量必须放在圆括号中间，圆括号之间可以用于定义参数。

   - 函数的第一行语句可以选择性地使用文档字符串—用于存放函数说明。

   - 函数内容以冒号起始，并且缩进。

   - **return [表达式]** 结束函数，选择性地返回一个值给调用方。不带表达式的return相当于返回 None。

     注意:  遇到return语句,后面的代码不执行.

2. Python 定义函数使用 def 关键字，一般格式如下： 

   ```python
   def 函数名称(形参): #算列表中所有元素的和,括号中可以添加形式参数.简称形参,也可以不加.
       函数代码
   ```

   - 最简单的形式

     ```python
     def hello() :
        print("Hello World!")  #定义函数
     
     hello()  # 调用函数,输出"Hello World!"
     
     ```

   - 复杂一些,加上参数

     ```python
     #  计算列表中的元素的和
     def sum_list_element(list1):
         sum = 0
         for i in list1:
             sum += 1
          print(sum)
         
     #  调用函数
     sum_list_element([1,2,3,4,5,6,7,8,9])  # 输出45
             
     
     ```

   - 加上return的应用

     ```python
     # 比较2个数的大小,并返回得到较大的数
     def compare_num(a,b):
         v = a if a > b else b
         return v   # 如果没有return语句,  print(compare_num(5,10)) 输出的是None
     print(compare_num(5,10))   # 调用函数,得到a,b中较大的数,并打印出来.
     
     
     #函数没有返回值时,默认返回None
     #函数有返回值时,语句为:return 返回值.返回值可以是任意东西.
     def compare_num(a,b):
         return a if a>b else b  #和三目运算一起,返回a和b中较大的数.
     #############################################
     #  函数执行到return 语句,就返回,后面的语句不执行.
     #  若返回值为多个,默认将其添加到元组中,返回一个元组.
     def func(a,b,c,d)
     	return a,b,c,d
     
     v = func(1,'邓益新',[11,22,33,44,55],{'k1':'v1'})
     print(v)            # (1,'邓益新',[11,22,33,44,55],{'k1':'v1'})  元组,
     ```

#### 5.2.3函数参数解析

1. 参数

   ```python
   def func(a1,a2):
       函数体(可调用a1和a2)
       return 返回值
   # 严格按照顺序传参数：位置方式传参。
   # 实际参数可以是任意类型。
   
   def check_age(age):  #只有1个参数:age,参数数量可以是无穷多个.
       if age >= 18:
           print('你是成年人')
       else:
           print('你是未成年人')
           
   check_age(18)    #调用函数,输出你是成年人.
   
   ```

2. 位置参数

   ```python
   def func(a1,a2,a3,a4)
   	print(a1,a2,a3,a4)
     
   func(1,2,3,4)
   # 严格按顺序输入参数,顺序是固定的.
   ```

3. 关键字传参

   ```python
   def func(a1,a2)
   	print(a1,a2)
     
   func(a1 = 1 ,a2 = 2) 
   func(a2 = 2,a1 = 1)  # 俩者完全一样,全是关键字可以打乱顺序.
   ```

4. 位置参数和关键字传参混用

   ```python
   ######  这时必须位置参数在前,关键字参数在后
   def func(a1,a2,a3,a4)
   	print(a1,a2,a3,a4)
      
   func(1,10,a3 = 15,a4 = 88) 
   func(1, 10, a4=15, a3=88)  # 俩者等价,正确.
   func(a1=1,a2=2,a3=3,a4=4)  #正确
   func(a1=1,a2=2,15,a4 = 88)  # 错误
   func(1,2,a3 = 15,88)  #错误
   #################################
   #    必须位置参数在前,关键字参数在后, 位置参数 > 关键字参数  #
   
   ```

5. 默认参数

   ```python
   def func(a1,a2,a3=9,a4=10):   #  默认a3=9,a4=10,这时可以不输入a3 和 a4 参数.默认值为 9 和 10 
       print(a1,a2,a3,a4)
   
   func(11,22)   #正确
   func(11,22,10)  #  正确,修改默认参数a3 = 10 ,参数a4不输入.默认为10 .
   func(11,22,10,100)  #正确
   func(11,22,10,a4=100)   #正确
   func(11,22,a3=10,a4=100)    #正确
   func(11,a2=22,a3=10,a4=100)   #正确
   func(a1=11,a2=22,a3=10,a4=100)    #正确
   
   ```

6. **万能参数(重点)**

   - args(打散)**可以接受任意个数的位置参数，并将参数转换成元组。**

     - 没有调用函数   *    字符

       ```python
       def func(*args):
           print(args)
       func(1,2,3,True,[11,22,33,44])
       #  输出为(1,2,3,True,[11,22,33,44])  元组
       ```

     - 有调用函数   *    字符

       ```python
       def func(*args):
           print(args)
       func(*(1,2,3,True,[11,22,33,44]))
       #  输出为(1,2,3,True,[11,22,33,44])  元组
       ```

   - *args  只能用位置传参

     ```python
     def func4(*args):
         print(args)
         
     func4(1)    元组(1,)
     func4(1,2)   元组(1,2)
     
     func4(1,2)
     func4(1,2)
     
     ```

   - **kwags(打散)  可以接受任意个数的关键字参数，并将参数转换成字典。

     - 没有调用函数   **    字符

       ```py
       def func(**kwargs):   
           print(kwargs)
       
       func(k1=1,k2="alex")
       ```

     - 有调用函数   **    字符

       ```python
       def func(**kwargs):
           print(kwargs)
       func(**{'k1':'v2','k2':'v2'}) # kwargs={'k1':'v2','k2':'v2'}
       ```

     - **kwags(打散)只能用关键字传参 

#### 5.2.4函数参数混用

```python
def func(*args,**kwargs):
    print(args,kwargs)

# func(1,2,3,4,5,k1=2,k5=9,k19=999)
func(*[1,2,3],k1=2,k5=9,k19=999)
func(*[1,2,3],**{'k1':1,'k2':3})
func(111,222,*[1,2,3],k11='alex',**{'k1':1,'k2':3})

```

#### 5.2.5函数作用域

1. 函数的作业域

   - 作用于全局,对任意一个py文件来说,全局作用域

   - 对函数来说:局部作用域

     ```python
     #  在函数中定义的值,只在局部作用域中
     #  在全局作用域中找不到#
     a = 1
     b = 2
     def func():
         a = 10
         b = 20
         print(a) 
         print(b)
     func()     #  输出10  ,20
     print(a)
     print(b)   #输出 1    , 2
     
     
     ```

2. zu作用域产找数据规则::优先在自己的作用域找数据，自己没有就去  "父级" ->  "父级" -> 直到全局，全部么有就报错。

   ```python
   a = b = 1
   def func():
       b = 5
       print(a,b)
   func()     #  1 ,5
   print(a,b)  #   1, 1
   ```

3. 子作用域只能找到父级作用域中的变量的值,不能重新为赋值父级作用域的变量赋值.

   - 例外,特别:  global 变量名,强制把全局的变量赋值

     ```python
     a = b = 1
     def func():
     	global b
         b = 5     # 更改全局变量b = 5
         print(a,b)
     func()     #  1 ,5
     print(a,b)  #   1, 5
     ```

   - nonlocal   ,强制把父级的变量赋值 ,一级一级向上找寻,不找全局域.找不到则报错.

     ```python
     a = b = 1
     def func():
     	nonlocal b
         b = 5     # 更改父级变量,如果找不到,再到父级的父级找寻,一直找到全局域之前(不包含全局域)
         print(a,b)
     func()      #会报错,父级就是全局域,nonlocal不包含全局域,故会报错,找不到b 
     print(a,b)  
     
     a = b = 1
     def func():
         a = 3
         b = 99
         print(a,b)
         def func1():
             nonlocal b
             b = 88
             print(a,b)
         func1()
     func()
     print(a,b)
     
     ```

   - 补充: 全局变量大写

### 5.3 函数高阶

#### 5.3.1 函数中高阶

1. 函数的变换

   ```python
   def func():
       print('小米手机真的好')  #  这里func--------->指向函数的内存地址
   print(id(func))   #  33627808
   a = '小米手机真的好'
   print(id(a))     #  35196048
   #  函数的名字和变量类似,有类似的指向关系.变量指向一块内存地址放置数据,函数也指向一块内存地址放置数据
   
   ```

2. 函数名当做变量使用

   ```python
   def func():
       print('小米手机真的好')  
   print(id(func))   #  33627808
   a = '小米手机真的好'
   print(id(a))     #  35196048
   
   fuck_world = func
   print(id(fuck_world))    #33627808 ,2者内存地址一致.
   fuck_world()  # 调用函数,打印'小米手机真的好'
   
   
   ```

3. 函数名作为列表/元组/字典元素(是不可变类似,可以做字典的键<-------->str)

   ```python
   def func1():
       print('快活啊')
       
   func_list = [func1, func1, func1]
   func_list[0]()
   func_list[1]()
   func_list[2]()   # 类似的用法,调用函数运行,注意: 加上括号就可以运行该函数了
   for item in func_list:
       v = item()    #  函数语句没有retun,返回值默认为None
       print(v)   
   ```

4. 函数可以当作参数进行传递

   ```python
   def func3(arg):
       v = arg()
       print(arg())
       print(arg)
   
   
   def show():
       return 999
   
   func3(show)    # 调用函数,把show函数当做参数使用.
   
   ```

   - 面试题相关

     ```python
     def func3():
         print('话费查询')
     def func4():
         print('流量办理')
     def func5():
         print('充值')
     def func6():
         print('人工服务')
     
     info = {
         'k1':func3,
         'k2':func4,
         'k3':func5,
         'k4':func6
     }
     
     content = input('请输入你要办理的业务:')
     function_name = info.get(content)
     function_name()          # 运用字典调用函数
     ```

5. 函数可以做返回值

   ```python
   def func():
   	print('name')
       
   def show()
   	return func
   # ******************  
   v = show()
   
   v()   # 执行打印name     
   #********************
   show()()  # 相当于show()()
   ```

6. 函数的闭包

   - 闭包概念：为函数创建一块区域并为其维护自己数据，以后执行时方便调用。【应用场景：装饰器 / SQLAlchemy源码】

     ```python
     name = 'oldboy'
     def bar(name):
         def inner():
             print(name)
         return inner
     v1 = bar('alex') # { name=alex, inner }  # 闭包，为函数创建一块区域（内部变量供自己使用），为他以后执行提供数据。
     v2 = bar('eric') # { name=eric, inner }  ,2者不干扰运行.
     v1()
     v2()
     ```

   ### 

#### 5.3. 2 lambda函数(匿名函数)

用于表示非常简单的函数,就如三元运算和if 语句一样.

- lambad函数只能用一行代码

  ```python
  # lambda表达式，为了解决简单函数的情况，如：
  
  def func(a1,a2):
      return a1 + 100 
  
  func = lambda a1,a2: a1+100       #   2者等价,注意冒号后接代码,自动return 
  
  ```

- 应用

  ```python
  func = lambda a,b:a if a > b else b
  t =  func(1,5)
  print(t)   #  求2个数里面较大的数
  
  # 练习题1
  USER_LIST = []
  def func0(x):
      v = USER_LIST.append(x)
      return v 
  
  result = func0('alex')
  print(result)
  
  
  # 练习题2
  
  def func0(x):
      v = x.strip()
      return v 
  
  result = func0(' alex ')
  print(result)
  
  ############## 总结：列表所有方法基本上都是返回None；字符串的所有方法基本上都是返回新值 
  
  ```

### 5.4  python内置函数简介

#### 5.4.1自定义函数

#### 5.4.2内置函数

- 输入输出print  input

- 强制转换类型

  - dict()
  - list()
  - tuple()
  - int()
  - str()
  - bool()
  - set()

- 其他

  - len
  - open
  - range
  - id
  - type

- 数学相关

  - abs  求绝对值

  - float ,转换为浮点型(小数)

  - max  /min

  - sum  求和

  - divmod  ,获取2数字相除的商和余数

  - pow ,指数函数,获取指数

    ```python
    v = pow(2,5)
    print(v)  # 2^5 = 32
    
    ```

  - round函数,四舍五入函数

    ```python
    v= round(1.24879,2) #小数点后取2位
    print(v)  #  1.25
    ##############################################
    # 注意:由于计算机精度问题,round函数可能会出错
    round(2.355,2)   # 算得的是 2.35  ,py版本3.6.8
    ```

- 编码相关

  - chr()  ,将十进制转化unicode编码中对应的字符串

    ```python
    v =chr(152)
    print(v)
    
    i = 1
    while i < 1000:
        print(chr(i))
        i += 1
    ```

    

  - ord(), 根据字符在unicode编码的对应关系,找到对应的十进制.

    ```python
    v= ord("字")   #  一次只能索引一个字符
    print(v)
    
    content = """一天，我正在路上走着，突然一声
    雄浑有力的声音传进我的耳朵里。"""
    for i in content:
        print(ord(i))
    ```

  - 应用:   随机验证码

    ```python
    import random     # 调用随机数函数
    
    def get_random_code(length=6):
        data = []
        for i in range(length):
            v = random.randint(65,90)  # 65-90对应的是ABC-----Z
            data.append(chr(v)
        return  ''.join(data)
    
    code = get_random_code()
    print(code)
    ```

- 进制转换

  - bin() 10进制转为2进制   ,''0b"

  - oct()   10进制转为8进制   ." 0o"

  - hex()    10进制转为16进制    ."0x"

  - int()    把````数转为10进制数    ,base默认为10 ,转二进制数base = 2,base = 8,  base = 16

    ```python
    num= 192
    n1 = bin(num)
    n2 = oct(num)
    n3 - hex(num)
    
    print(n1,n2,n3)
    ```

    ```python
    # 二进制转化成十进制
    v1 = '0b1101'
    result = int(v1,base=2)
    print(result)
    
    # 八进制转化成十进制
    v1 = '0o1101'
    result = int(v1,base=8)
    print(result)
    
    # 十六进制转化成十进制
    v1 = '0x1101'
    result = int(v1,base=16)
    print(result)                #  注意,转换时0b   0o   0x可以不用加
    ```

#### 5.4.3 高级函数(必考)

- map()  循环每个元素（第二个参数），然后让每个元素执行函数（第一个参数），将每个函数执行的结果保存到新的列表中，并返回。

  第一个参数 function 以参数序列中的每一个元素调用 function 函数，返回包含每次 function 函数返回值的新列表。 

  ```python
  content = [11,22,33,44,55,66,77,88,99]
  t = map(lambda x: x*x,content)
  print(list(t))  #列表元素的个数是相同的
  ```

- filter(筛选/过滤)**filter()** 函数用于过滤序列，过滤掉不符合条件的元素，返回由符合条件元素组成的新列表。

  该接收两个参数，第一个为函数，第二个为序列，序列的每个元素作为参数传递给函数进行判，然后返回 True 或 False，最后将返回 True 的元素放到新列表中。

  ```python
  content = [11,0,33,55,66,77,88,99]
  t = filter(lambda x: x < 50,content)
  print(list(t))  #列表元素的个数是相同的
  ```

- reduce()**reduce()** 函数会对参数序列中元素进行累积。

  函数将一个数据集合（链表，元组等）中的所有数据进行下列操作：用传给 reduce 中的函数 function（有两个参数）先对集合中的第 1、2 个元素进行操作，得到的结果再与第三个数据用 function 函数运算，最后得到一个结果。

  ```python
  import functools
  
  t = [3,2,3,4,5,6,8,9]
  v = functools.reduce(lambda x,y: x ** y,t)
  print(v)
  ```

  **注意**: 在py3中,reduce的函数移到了functools模组,要用这个功能,必须 import functools

## 第六章 模块

### 6.1 模块初步(import)

```python
# import 模块名  # 调用py的内置模块,
# PY调用hashlib模块对用户的密码进行加密,hashlib模块有很多种加密方式.这里选用MD5加密

import hashlib

def md5(arg):  # 加密函数   
    hash  = hashlib.md5('dengyxiin'.encode('utf-8'))    # 加盐,避免给黑客撞库得到密码.
    hash.update(arg.encode('utf-8'))
    return hash.hexdigest()     # 返回加密的md5,<----->t = hash.hexdigest();return t

def register(user,pwd):  # 注册函数,参数为用户名和密码
    with open('data.txt','a',encoding='utf-8') as file:    #  追加模式写入文件
        file.write(user+'|'+md5(pwd)+'\n')  #用户名和密码用竖杠隔开,方便以后找寻

def login(user,pwd):  # 登录函数
    with open('data.txt','r',encoding='utf-8') as file:    # 以只读模式打开文件,遍历文件,
        for line in file:
            line = line.strip()
            line = line.split('|')
            if user == line[0] and md5(pwd) == line[1]:    #遍历存储登录数据的文件,看能否比对的上.
                return  True

item = input('1表示注册 ,2表示登录 . 请输入')
if  not item.isdecimal():
    print('输入错误字符,请重新输入')
item = int(item)
if item ==1:
    user = input('请输入用户名:')
    pwd = input('请输入密码:')
    register(user,pwd)
elif item ==2:
    user = input('请输入用户名:')
    pwd = input('请输入密码:')
    v = login(user,pwd)
    if v:
        print('登陆成功')
    else:
        print('登陆失败')
else:
    print('输入错误序号,请重新输入')
```

```python
#  让密码在键入的时候隐藏
import getpass

pwd = getpass.getpass('请输入密码：')
if pwd == '123456789':
    print('输入正确')

```



## 第七章 面向对象

## 第八章 网络编程

## 第九章 并发编程

## 第十章 数据库

## 第十一章 前端开发

## 第十二章 Django框架



## 附录 常见错误和单词

### 单词

| 单词         | 意思 | 备注                  |
| ------------ | ---- | --------------------- |
| lower        | 小写 | islower判断是否全小写 |
| upper        | 大写 | supper判断是否全大写  |
| isdigit      |      |                       |
| isdeicmal    |      |                       |
| strip        |      |                       |
| split        |      |                       |
| replace      |      |                       |
| join         |      |                       |
| formate      |      |                       |
|              |      |                       |
| append       |      |                       |
| insert       |      |                       |
| remove       |      |                       |
| pop          |      |                       |
| extend       |      |                       |
| reverse      |      |                       |
| sort         |      |                       |
| clear        |      |                       |
|              |      |                       |
| keys         |      |                       |
| values       |      |                       |
| items        |      |                       |
| get          |      |                       |
| pop          |      |                       |
| update       |      |                       |
|              |      |                       |
| add          |      |                       |
| update       |      |                       |
| discard      |      |                       |
| intersection |      |                       |
| difference   |      |                       |
| union        |      |                       |
|              |      |                       |
| if           |      |                       |
| else         |      |                       |
| while        |      |                       |
| pass         |      |                       |
| break        |      |                       |
| continue     |      |                       |
| for          |      |                       |
| in           |      |                       |
| and          |      |                       |
| or           |      |                       |
| not          |      |                       |
| is           |      |                       |
| open         |      |                       |
|              |      |                       |
|              |      |                       |
|              |      |                       |
|              |      |                       |
|              |      |                       |
|              |      |                       |
|              |      |                       |
|              |      |                       |
|              |      |                       |
|              |      |                       |
|              |      |                       |
|              |      |                       |
|              |      |                       |
|              |      |                       |
|              |      |                       |
|              |      |                       |
|              |      |                       |
|              |      |                       |
|              |      |                       |
|              |      |                       |
|              |      |                       |
|              |      |                       |
|              |      |                       |
|              |      |                       |
|              |      |                       |
|              |      |                       |
|              |      |                       |
|              |      |                       |
|              |      |                       |
|              |      |                       |
|              |      |                       |
|              |      |                       |
|              |      |                       |
|              |      |                       |
|              |      |                       |
|              |      |                       |
|              |      |                       |
|              |      |                       |
|              |      |                       |
|              |      |                       |
|              |      |                       |
|              |      |                       |
|              |      |                       |
|              |      |                       |
|              |      |                       |
|              |      |                       |
|              |      |                       |
|              |      |                       |
|              |      |                       |
|              |      |                       |
|              |      |                       |
|              |      |                       |

### 错误记录

1. 编码错误unicode/decode error![截图python0408164437](Python学习笔记.assets/截图python0408164437.png)

注意: 有\x,表面他是十六进制,因为二机制太长,用十六进制表达德更多.