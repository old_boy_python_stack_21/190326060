#!/usr/bin/env python
# -*- coding:utf-8 -*


# 2.
"""
import socket
from threading import Thread


def chat(conn):
    while True:
        msg = conn.recv(1024)
        if msg.decode('utf-8').upper() == 'Q':
            break
        conn.send(b'hello,how are you.')


sk = socket.socket()
sk.bind(('127.0.0.1', 8888))
sk.listen(5)
while True:
    conn, _ = sk.accept()
    Thread(target=chat, args=(conn,)).start()

"""


# 3.使用多线程 实现一个请求网页 并且把网页写到文件中
# 生产者消费者模型来完成


import socket
import requests
from threading import Thread
from multiprocessing import JoinableQueue


def get_url(url,q):
    re = requests.get(url)
    q.put(re.text)



def write_file(text,q):
    f = open('url_data.txt', 'w', encoding='utf-8')
    f.write(text)
    q.task_done()

q = JoinableQueue(10)
url_l = []
for url in url_l:
    p= Thread(target = get_url,args = (url,q))


# 6使用线程池，随意访问n个网址，并把网页内容记录在文件中
'''
import requests
from concurrent.futures import ThreadPoolExecutor

def get_content(url):
    res = requests.get(url)
    return {'file_name':url.rsplit('.')[1]+'.txt','content':res.text}

def write_file(ret):
    dic = ret.result()
    with open(dic['file_name'],mode='w',encoding='utf-8') as f:
        f.write(dic['content'])
        f.flush()

url_list = [
            'http://www.baidu.com',
            'http://www.tencent.com',
]

pool = ThreadPoolExecutor(5)
for url in url_list:
    ret = pool.submit(get_content,url)
    ret.add_done_callback(write_file)


#  7 死锁就是:是指两个或两个以上的进程或线程在执行过程中，
#  因争夺资源而造成的一种互相等待的现象，若无外力作用，它们都将无法推进下去。
#  此时称系统处于死锁状态或系统产生了死锁，这些永远在互相等待的进程称为死锁进程，


# 8. GIl锁,全局解释器锁,是的cpython解释器,同一时间只能有一个线程在运行
# 进程加锁  multprocessing的Lock类
# 线程加锁  threading 的 Lock类  和 递归锁Rlock()类
# 这里的Lock锁,只能acquire一次,递归锁可以多次acquire()


# 9.cpython解释器下的线程的数据不安全.
