#!/usr/bin/env python
# -*- coding:utf-8 -*-

mysql> create table class(cid int primary key,
    -> caption char(4)
    -> );


insert  into  class values(1, '三年二班'), (2, '一年三班'), (3, '三年一班');


mysql > create   table   teacher(tid int primary key,tname char(4));

insert into teacher values(1, '波多'), (2, '苍空'), (3, '饭岛');

create table student(sid int primary key,sanme char(4),gender enum('女','男'),
class_id int ,foreign key(class_id) references class(cid) on delete cascade on update cascade );


insert into student values(1,"钢蛋",'女',1), (2,'铁锤','女',1),(3,'山炮','男',2);


create table course (cid int primary key , cname char(4),teach_id int,
foreign key(teach_id) references teacher(tid) on delete cascade on update cascade );

insert into course values(1,'生物',1),(2,'体育',1),(3,'物理',2);

create table score(sid int not null unique auto_increment,
student_id int,
corse_id int,
foreign key(student_id) references student(sid) on delete cascade on update cascade,
foreign key(corse_id) references course(cid) on delete cascade on update cascade,
primary key(student_id,corse_id),
number int);


insert into score values(1,1,1,60),(2,1,2,59),(3,2,2,100);


#   ###
update class set port = 65535 where cid<2;

mysql> alter table modify port char(5) not null;

mysql> alter table class change port server int default 7424994;


mysql > select concat('<mingzi : ', emp_name, '>', '<xinzi:', salary, '>') as employee_srlary from employee;

select distinct post ,empname from employee;

select emp_name,salary*12 (as) annula_year from employee;