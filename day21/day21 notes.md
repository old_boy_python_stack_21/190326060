# python day 21

## 1.面试题

1. 谈谈你了解的面向对象

   从面向对象的三大特性来说:

   - 封装:类把函数划分和分类  ,对象封装数据到对象中
   - 继承:多个类有公共的方法,就把这些方法拿出来,放到基类去,然后继承基类的方法,避免重复编写.
   - 多态 : 对于一个函数而言,python对于传入的参数的类型没有限制,但是如果函数中有send方法,就是对传入类型的限制.(必须有send方法)类似这样的就叫鸭子模型,,只要呱呱叫的都是鸭子,只要有send方法,就是我们想要的类型

2. 类是对象什么关系?

   对象是类的一个实例

3. self 是什么 ?

   self就是形式参数,对象调用方法时,python内部自动把对象传给这个参数.

4. 类成员

   - 类变量
   - 方法
   - 静态方法
   - 类方法
   - 属性

5. 对象成员

   - 实例变量

## 2.嵌套问题

1. 函数 : 参数可以是任意类型

2. 字典 : 对象和类都可以做字典的键和值

3. 继承的查找关系

4. 对象和类的嵌套

   ```python
   # 对象做类方法的参数
   class Foo:
       pass
   
   class Base:
       def __init__(self):
           self.list1 = []
   
       def func(self,arg):
           self.list1.append(arg)
   
   obj = Foo()
   obj2 = Base()
   obj2.func(obj)
   
   # 对象封装 类到对象中
   class Foo:
       def f1(self,arg):
           self.func = arg
   
   class Base:
       def __init__(self,name,age):
           self.name = name
           self.age = age
   
   foo = Foo()
   foo.f1(Base)           # 封装类到对象中
   t = foo.func('邓益新',25)
   print(t.name,t.age)
   
   ```

## 3.类的特殊成员

### 3.1  `__init__`  和 `__new__` ,类()执行

```python
class Foo;
"""
类的注释,类是干啥的
"""
    def __init__(self,name):
        """
        初始化方法
        :param name: 
        """
        self.name= name
        
    def __new__(cls, *args, **kwargs):
        """
        创建对象的函数,用于创建空对象，构造方法,自动调用,类()
        :param args: 
        :param kwargs: 
        :return: 
        """
        pass

```

### 3.2 `__call__`  方法,对象()自动执行

```python
class Base:

    def __init__(self,name):
        self.name= name

    def __call__(self, *args, **kwargs):
        print('78945613')
        return '邓益新'

obj1 = Base('小明')
obj1()       # ---> 执行call方法
```

额外的写网站

```python
#!/usr/bin/env python
# -*- coding:utf-8 -*-
from wsgiref.simple_server import make_server

def func(environ,start_response):
    start_response("200 OK", [('Content-Type', 'text/plain; charset=utf-8')])
    return ['你好'.encode("utf-8")  ]

class Foo(object):

    def __call__(self, environ,start_response):
        start_response("200 OK", [('Content-Type', 'text/html; charset=utf-8')])
        return ['你<h1 style="color:red;">不好</h1>'.encode("utf-8")]


# 作用：写一个网站，用户只要来方法，就自动找到第三个参数并执行。
server = make_server('127.0.0.1', 8000, Foo())
server.serve_forever(
```

### 3.3 `__setitem__`      `__getitem__`  `__delitem__` 方法,类似索引的方法执行,[   ]

```python
class Base:

    def __setitem__(self, key, value):
        print({key,value})
        print('setitem方法执行,没有返回值')

    def __getitem__(self, item):
        print('getitem方法执行,可以有返回值')
        return 7989463

    def __delitem__(self, key):
        print('随你定函数方法')


objj = Base()
objj[123] = 456
v = objj[756]
del objj[7582]
print(v)
```

### 3.4 `__str__`方法,定义在类中,打印print对象时自动执行打印函数返回值

```python
# 不要相信print打印出来的东西,而是要type

class Base:

    def __str__(self):
        print('执行__str__方法,print(对象)其实是打印这个函数的返回值')
        return '对象的名字是大奔'

obj = Base()
print(obj,type(obj))   


# 执行__str__方法,print(对象)其实是打印这个函数的返回值
# 对象的名字是大奔 <class '__main__.Base'>
```

### 3.5`__dict__`方法,  把对象封装的值转为字典.`obj.__dict__`方法

```python
class Foo(object):
    def __init__(self,name,age,email):
        self.name = name
        self.age = age
        self.email = email

obj = Foo('alex',19,'xxxx@qq.com')
print(obj)
print(obj.name)
print(obj.age)
print(obj.email)
val = obj.__dict__ # 去对象中找到所有变量并将其转换为字典
print(val)
```

### 3.6上下文管理

```python
class Satcc:
    def __enter__(self):
        print('上文,在with语句之前执行,with 对象 as f的f是函数返回值')
        print('开始')
        return 123456

    def __exit__(self, exc_type, exc_val, exc_tb):
        print('下文,with语句执行完毕后再执行此程序')
        print('结束')

with Satcc() as f:
    print('中间语句开始执行')
    print(f)
    print('中间语句执行完毕')
    
    
# 类中定义 __enter__(self)  和   __exit__(self,其他3个参数)上下文管理
```

### 3.7对象相加/减/乘/除

```python
class Fass:
    def __add__(self, other):
        print('前者和后者相加')
        return 456413

obj = Fass()
v = 2
t = obj + v    # 前者和后者相加,自动调用__add__函数,前者是obj,即self,后者无所谓
print(t)       # 456413

	def __sub__(self, other):
        print(123)

    def __mul__(self, other):
        print(165)


		   
```

## 4.内置函数补充

### 4.1 type

```python
class Foo:
    pass

obj = Foo()
 # 推荐使用type判断 是否是那个类创建的对象(实例化)
if type(obj) == Foo:    # # 判断obj是否是Foo类的实例（对象）
    print('obj是Foo类的对象') 
```

### 4.2 issubclass

```python
class Base:
    pass

class Base1(Base):
    pass

class Foo(Base1):
    pass

class Bar:
    pass



print(issubclass(Bar,Base))
print(issubclass(Foo,Base))  # 判断前者是否继承后者
```

### 4.3 isinstance (不推荐)

```python
class Base(object):
    pass

class Foo(Base):
    pass

obj = Foo()

print(isinstance(obj,Foo))  # 判断obj是否是Foo类或其基类,的实例（对象）
print(isinstance(obj,Base)) # 判断obj是否是Foo类或其基类,的实例（对象）
```

## 4. super语句

```python
class Base(object): # Base -> object
    def func(self):
        super().func()
        print('base.func')

class Bar(object):
    def func(self):
        print('bar.func')

class Foo(Base,Bar): # Foo -> Base -> Bar
    pass

obj = Foo()
obj.func()          # 打印 base.func    bar.func

# super().func() 根据self对象所属类的继承关系，按照顺序挨个找func方法并执行(找到第一个就不在找了)
```

## 5.异常处理

### 5.1基本格式

```python
try:
    v = []
    v[11111] # IndexError
except ValueError as e: # 小弟先上场捕捉错误,捕捉成功则执行这个语句
    pass
except IndexError as e:
    pass
except Exception as e:  # 最nb的boss,
    print(e)   # # e是Exception类的对象，中有一个错误信息。
    # 或者做其他事
```

### 5.2 finally 语句

```python
try:
    int('邓益新')
except Exception as e:
    print(e)   # # e是Exception类的对象，中有一个错误信息。
    # 或者做其他事
finally:
    print('无论语句运行是否有错,finally语句都会执行')
    

###################t  特殊情况  ###########################

# 在函数中return之后,有finally语句,则会执行finally语句,然后再return

def func():
    print('函数开始执行')
    try:
        v = []
        print(v[5])
        return 0
    except Exception as e:
        print(e)
        return   # 这里函数应该已经终止,但finally 语句会执行
    finally :
        for i in '123':
            print(i)

```

### 5.3 主动触发异常

```python
try:
    int('123')
    raise Exception('阿萨大大是阿斯蒂') # 代码中主动抛出异常
except Exception as e:
    print(e)
    
    
    ######################
def func():
    result = True
    try:
        with open('x.log',mode='r',encoding='utf-8') as f:
            data = f.read()
        if 'alex' not in data:
            raise Exception()
    except Exception as e:
        result = False
    return result
```

### 5.4 自定义异常

```python
class MyException(Exception):   # 继承Exception
    pass

try:
    raise MyException('asdf')
except MyException as e:
    print(e)
```

```python
class MyException(Exception):
    def __init__(self,message):
        super().__init__()
        self.message = message

try:
    raise MyException('asdf')
except MyException as e:
    print(e.message)
```

