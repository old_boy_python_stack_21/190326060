#!/usr/bin/env python
# -*- coding:utf-8 -*-	 
#@Project  : s21day21 
#@Author : Jack Deng       
#@Time   :  2019-04-25 19:15
#@File   : day20_homework.py

# 1
# __init__
class Foo:


    def __init__(self,n,a):
        """
        对象初始化方法
        :param n:
        :param a:
        """
        self.name = n
        self.age = a

tt = Foo('邓益新',25)

# __new__方法
class Foo1:
    def __new__(cls, *args, **kwargs):
        """
        创建一个空对象方法
        :param args:
        :param kwargs:
        :return:
        """
        pass

tt1 = Foo1()


# __call__方法
class Foo2:
    def __call__(self, *args, **kwargs):   # 对象加()执行函数
        print(456)
        return '对象()时执行函数'

tt2 = Foo2()
print(tt2())

# __item__方法
class Foo3 :
    def __setitem__(self, key, value):
        print('执行setitem函数')

    def __getitem__(self, item):
        print('执行getitem函数')
        return '小明'

    def __delitem__(self, key):
        print('执行delitem函数')

obj = Foo3()

obj[123] = 456       # 执行set函数,把123,456当key,values传进去.
val = obj[789]       # 执行getitem函数,,把789当做item参数  获得函数的返回值,没有默认返回None
print(val)
del obj['邓益新']    # 执行delitem函数,把'邓益新'当做key参数

# __str__方法   打印对象是,打印的__str__函数的返回值
class Foo4:
    def __str__(self):
        print(456)
        return '小小星'

tt4 = Foo4()
print(tt4)   #执行__str__函数,打印返回值

#__dict__方法,把对象封装的值转成字典
class Foo5:
    def __init__(self,a,b,c):
        self.name = a
        self.age = b
        self.country = c


tt5 = Foo5('邓益新',25,'中国')
print(tt5.__dict__)

# 上下文管理enter/exit
class Foo6:
    def __exit__(self, exc_type, exc_val, exc_tb):
        print('结束')

    def __enter__(self):
        print('开头')
        return 456

with Foo6() as f:   # 把enter的返回值给f
    print(f+100)
    print('执行代码')

# 相加方法
class Foo7:
    def __add__(self, other):
        print('执行add函数')
        return '前者和后者相加'

tt7 = Foo7()
val = tt7 + 888    # 对前者有要求,必须是支持add方法的对象,后者无所谓,
print(val)
# 同理类似的有   divmod  mul  sub有类似的用法


# 2.

class Foo(object):
    def __init__(self, age):
        self.age = age
    def display(self):
        print(self.age)      # 返回值为None

data_list = [Foo(8), Foo(9)]
for item in data_list:
    print(item.age, item.display())

# 8   8    None
# 9   9    None

# 3.
class Base(object):
    def __init__(self, a1):
        self.a1 = a1
    def f2(self, arg):
        print(self.a1, arg)

class Foo(Base):          #继承
    def f2(self, arg):
        print('666')

obj_list = [Base(1), Foo(2), Foo(3)]
for item in obj_list:
    item.f2(arg = '小明')

"""结果
1   小明
666
666
"""

# 4.
class StarkConfig(object):
    def __init__(self,num):
        self.num = num
    def changelist(self,request):
        print(self.num,request)

class RoleConfig(StarkConfig):
    def changelist(self,request):
        print('666')

config_obj_list = [StarkConfig(1),StarkConfig(2),RoleConfig(3)]
for item in config_obj_list:
    print(item.num)

"""结果
1
2
3
"""

# 5.
class StarkConfig(object):
    def __init__(self,num):
        self.num = num
    def changelist(self,request):
        print(self.num,request)
class RoleConfig(StarkConfig):
    pass

config_obj_list = [StarkConfig(1),StarkConfig(2),RoleConfig(3)]
for item in config_obj_list:
    item.changelist(168)

"""结果
1  168
2  168
3  168
"""

# 6.
class StarkConfig(object):
    def __init__(self,num):
        self.num = num

    def changelist(self,request):
        print(self.num,request)

class RoleConfig(StarkConfig):
    def changelist(self,request):
        print(666,self.num)

config_obj_list = [StarkConfig(1),StarkConfig(2),RoleConfig(3)]
for item in config_obj_list:
    item.changelist(168)

"""结果
1   168
2   168
666 3
"""

# 7.
class StarkConfig(object):
    def __init__(self,num):
        self.num = num
    def changelist(self,request):
        print(self.num,request)
    def run(self):
        self.changelist(999)

class RoleConfig(StarkConfig):

    def changelist(self,request):
        print(666,self.num)

config_obj_list = [StarkConfig(1),StarkConfig(2),RoleConfig(3)]
config_obj_list[1].run()
config_obj_list[2].run()
"""结果
2   999
666 3
"""

# 8.
class StarkConfig(object):
    def __init__(self,num):
        self.num = num
    def changelist(self,request):
        print(self.num,request)
    def run(self):
        self.changelist(999)

class RoleConfig(StarkConfig):
    def changelist(self,request):
        print(666,self.num)


class AdminSite(object):
    def __init__(self):
        self._registry = {}
    def register(self,k,v):
        self._registry[k] = v

site = AdminSite()
print(len(site._registry))
site.register('range',666)
site.register('shilei',438)
print(len(site._registry))

site.register('lyd',StarkConfig(19))
site.register('yjl',StarkConfig(20))
site.register('fgz',RoleConfig(33))

print(len(site._registry))
print(site._registry)

"""结果
0
2
5
# {'range':666,'shilei':438,'lyd':StarkConfig_obj,'yjl':StarkConfig_obj2,'fgz':RoleConfig_obj}
"""

# 9.
class StarkConfig(object):
    def __init__(self,num):
        self.num = num
    def changelist(self,request):
        print(self.num,request)
    def run(self):
        self.changelist(999)

class RoleConfig(StarkConfig):
    def changelist(self,request):
        print(666,self.num)
class AdminSite(object):
    def __init__(self):
        self._registry = {}
    def register(self,k,v):
        self._registry[k] = v

site = AdminSite()
site.register('lyd',StarkConfig(19))
site.register('yjl',StarkConfig(20))
site.register('fgz',RoleConfig(33))
print(len(site._registry)) # 3

for k,row in site._registry.items():
    row.changelist(5)

"""结果
3
19  5
20  5
666 33
"""

# 10.
class StarkConfig(object):
    def __init__(self,num):
        self.num = num
    def changelist(self,request):
        print(self.num,request)
    def run(self):
        self.changelist(999)

class RoleConfig(StarkConfig):
    def changelist(self,request):
        print(666,self.num)

class AdminSite(object):
    def __init__(self):
        self._registry = {}
    def register(self,k,v):
        self._registry[k] = v

site = AdminSite()
site.register('lyd',StarkConfig(19))
site.register('yjl',StarkConfig(20))
site.register('fgz',RoleConfig(33))
print(len(site._registry)) # 3

for k,row in site._registry.items():
    row.run()

"""结果
3
19  999
20  999
666  33
"""

# 11.
class UserInfo(object):
    pass
class Department(object):
    pass

class StarkConfig(object):
    def __init__(self,num):
        self.num = num
    def changelist(self,request):
        print(self.num,request)
    def run(self):
        self.changelist(999)
class RoleConfig(StarkConfig):
    def changelist(self,request):
        print(666,self.num)
class AdminSite(object):
    def __init__(self):
        self._registry = {}
    def register(self,k,v):
        self._registry[k] = v(k)

site = AdminSite()
site.register(UserInfo,StarkConfig)
site.register(Department,StarkConfig)
print(len(site._registry))
for k,row in site._registry.items():
    row.run()

"""结果
2
class_UserInfo    999
class_Department  999
"""

# 12.

class F3(object):
    def f1(self):
        ret = super().f1()
        print(ret)
        return 123
class F2(object):
    def f1(self):
        print('123')
class F1(F3, F2):
    pass
obj = F1()
obj.f1()
"""结果
123
None
"""

# 13.
class Base(object):
    def __init__(self, name):
        self.name = name
class Foo(Base):
    def __init__(self, name):
        super().__init__(name)
        self.name = "于大爷"

obj1 = Foo('邓益新')
print(obj1.name)

obj2 = Base('代永进')
print(obj2.name)

"""结果
于大爷
代永进
"""
# 14.
class Base(object):
    pass
class Foo(Base):
    pass
obj = Foo()

print(type(obj) == Foo)
print(type(obj) == Base)
print(isinstance(obj,Foo))
print(isinstance(obj,Base))

"""结果
True
False
True
True
"""

# 15 .
class StarkConfig(object):
    def __init__(self, num):
        self.num = num
    def __call__(self, *args, **kwargs):
        print(self.num)
class RoleConfig(StarkConfig):
    def __call__(self, *args, **kwargs):
        print(self.num)
v1 = StarkConfig(1)
v2 = RoleConfig(11)
v1()
v2()

"""结果
1
11
"""

# 16.

class StarkConfig(object):
    def __init__(self, num):
        self.num = num
    def run(self):
        self()
    def __call__(self, *args, **kwargs):
        print(self.num)

class RoleConfig(StarkConfig):
    def __call__(self, *args, **kwargs):
        print(345)
    def __getitem__(self, item):
        return self.num[item]

v1 = RoleConfig('alex')
v2 = StarkConfig("wupeiqi")

print(v1[1])
print(v2(2))

"""结果
l
"wupeiqi"
None
"""

#17.
class Context:
    def do_something(self):
        print('随便做些什么事都可以')
    def __enter__(self):
        print('开头执行')
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        print('结束执行')


with Context() as ctx:
    ctx.do_something()

# 18.
class Stack(object):
    def __init__(self):
        self.data_list = []

    def push(self, val):
        self.data_list.append(val)

    def pop(self):
        return self.data_list.pop(len(self.data_list)-1)


obj = Stack()
# 调用push方法，将数据加入到data_list中。
obj.push('alex')
obj.push('武沛齐')
obj.push('金老板')

# 调用pop讲数据从data_list获取并删掉，注意顺序(按照后进先出的格式)
v1 = obj.pop()  # 金老板
print(v1)
v2 = obj.pop()  # 武沛齐
print(v2)
v3 = obj.pop()  # alex
print(v3)

# 请补全Stack类中的push和pop方法，将obj的对象维护成 后进先出 的结构。

# 19.
try:
    list1 = []
    raise Exception('我主动触发一个异常')
except Exception as e:   # e --> 自定义的异常名称,是class Exception的一个实例化对象
    print('异常已捕捉到,执行except语句')
    print(e)

# 20.
def func(arg):
    try:
        int(arg)
    except Exception as e:
        print('异常')
    finally:
        print('哦')


func('123')    # 哦
func('二货')   # 异常  哦