# python day 18-19

## 一 .补充

### 1. 数据类型补充

1. str，字符串类型，一般用于内存中做数据操作。

   ```python
   v = 'alex' # unicode编码存储在内存。
   ```

2. bytes，字节类型，一般用于数据存储和网络传输。

   ```python
   v = 'alex'.encode('utf-8')  # 将字符串转换成字节（由unicode编码转换为utf-8编码）
   v = 'alex'.encode('gbk')    # 将字符串转换成字节（由unicode编码转换为gbk编码）
   ```

3. py2和py3字符串类型不同

   - py3： str         bytes
   - py2: unicode     str

4. py2和py3字典方法得到的返回值不同

   - keys
     - py2：列表
     - py3：迭代器，可以循环但不可以索引
   - values
     - py2：列表
     - py3：迭代器，可以循环但不可以索引
   - items
     - py2：列表
     - py3：迭代器，可以循环但不可以索引

5. map/filter

   - py2：返回列表
   - py3：返回迭代器，可以循环但不可以索引

### 2.生成器推导式

```python
# def func():
#     for i in range(10):
#         yield i
# v2 = func()
v2 = (i for i in range(10)) # 生成器推导式，创建了一个生成器，内部循环为执行。


# 没有元组推导式,加括号是生成器推导式

def my_range(counter):
    for i in range(counter):
        yield i
# 生成器函数

# # 面试题：请比较 [i for i in range(10)] 和 (i for i in range(10)) 的区别？
# 前者是列表推导式,直接在内存中生成列表[1-10],后者是生成器推导式,内部循环不执行,只有for 循环才执行.
```

```python
# 示例一
# def func():
#     result = []
#     for i in range(10):
#         result.append(i)
#     return result
# v1 = func()
# for item in v1:
#    print(item)              # 列表推导式

# 示例二
# def func():
#     for i in range(10):
#         def f():
#             return i
#         yield f              # 生成器函数
#
# v1 = func()                  # 生成器,内部代码不执行
# for item in v1:
#     print(item())             #for 循环才执行
```

### 3.模块导入

注意：文件和文件夹的命名不能是导入的模块名称相同，否则就会直接在当前目录中查找。

## 二 .面向对象

### 1.面向对象基本格式

1. 遇到很多函数,给函数分个类和划分--> 封装

   ```python
   # ###### 定义类 ###### 
   class 类名:
       def 方法名(self,name):       # 分类的函数
           print(name)
           return 123
       def 方法名(self,name):
           print(name)
           return 123
       def 方法名(self,name):
           print(name)
           return 123
   # ###### 调用类中的方法 ###### 
   # 1.创建该类的对象
   obj = 类名()                  
   # 2.通过对象调用方法
   result = obj.方法名('alex')
   print(result)
   
   ```

2. 调用类中的方法

   ```python
   # 1.创建类的对象------>可以创建无数个,方法是通用的
   obj = 类名() 
   # 2.通过对象调用类的方法
   obj.方法名()    # ---->调用方法
   
   ```

3. 对象的作用

   存储一些值,以后方便自己调用

   ```python
   class Person:
   
       def __init__(self, n, a, g):  # ---> 给对象初始化,初始化方法（构造方法），给对象的内部做初始化。
           self.name = n
           self.age = a
           self.gender = g
   
       def show(self):
           msg = '我叫%s,今年%s岁,性别%s' %(self.name,self.age,self.gender)
           print(msg)
   
           
   # 类实例化对象,自动执行__init__方法.
   p1 = Person('邓益新',25,'男')
   
   p1.show()
   
   
   
   ################################################################33
   class Person:
           def show(self):
           msg = '我叫%s,今年%s岁,性别%s' %(self.name,self.age,self.gender)
           print(msg)
           
   p1 = Person()
   p1.name = '邓益新'    # -->在对象内部创建p1.name = '邓益新',
   p1.age = 25
   p1.gender = '男
   p1.show()            #执行方法,在对象内部找到值,打印
   ```

总结: def ```__init__方法``` 将数据封装到对象,方便使用

#### 1.1 总结

```python
'''
写代码时代码乱,函数多,
1. 将函数归类,相同性质的防放到一个类中
2.函数如果有可以反复使用的公共值,则可以发到对象中,方便使用
'''
```

```python
## 面向对像写法写

#      1. 循环让用户输入：用户名/密码/邮箱。 输入完成后再进行数据打印。#
class Person:

    def __init__(self, n, p, e):  # ---> 给对象初始化,初始化方法（构造方法），给对象的内部做初始化。
        self.name = n
        self.pwd = p
        self.email = e

    def show(self):
        msg = '我是%s,密码:%s,邮箱是:%s' %(self.name,self.pwd,self.email)
        print(msg)

user_list  = []
t = 0
while t < 3:
    n = input('输入名字:')
    p = input('输入密码:')
    e = input('输入邮箱:')
    p = Person(n,p,e)
    user_list.append(p)
    t += 1

for i in user_list:
    i.show()               # 直接调用方法打印
       
        
```

### 2.游戏开发相关

```python
# 2个阵营相互拼杀,定义2个类表示阵营
class Police:  # 特种兵阵营
    def __init__(self, name, hp):
        self.name = name
        self.hp = hp


    def dao(self,name):
        msg = '%s给了%s一刀' % (self.name, name)
        print(msg)


t1 = Police('小明', 1500)
t2 = Police('小红', 1000)
t3 = Police('小刚', 1800)


class Bandit:  # 匪徒阵营
    def __init__(self, nickname, hp):
        self.nickname = nickname
        self.hp = hp

    def quan(self,name):
        msg = ' %s给了%s一拳' % (self.name, name)
        print(msg)


f1 = Bandit('鲨鱼', 1800)
f2 = Bandit('恐龙', 1200)
f3 = Bandit('蜂窝', 1500)

t1.dao(f1.nickname)

```

### 3.继承特性

```python
#父类(基类)
class Father
    def f1():
        print('Father.f1')
        
# 子类(派生类)
class Son:
    def f2():
        print('Son.f2')
        
 obj = Son()
# 创建子类对象,可以继承父类的方法
# 执行对象.方法时，优先在自己的类中找，如果没有就是父类中找。
obj.f2()
obj.f1()

# 父类只有自己的方法,没有报错
```

- 当多个类有公共的方法时,可以放到基类中,避免重复编写.

- 继承关系的查找顺序

  ```python
  class Base:
      def f1(self):
          self.f2()
          print('base.f1')
  	def f2(self):
          print('base.f2')
  class Foo(Base):
      def f2(self):
          print('foo.f2')
          
  obj = Foo()
  obj.f1()
  
  
  
  ####- self 到底是谁？
  ####- self 是哪个类创建的，就从此类开始找，自己没有就找父类。
  ```

#### 3.1总结

- self 是哪个类创建的，就从此类开始找，自己没有就找父类。-->指对象

### 5.多态特性（多种形态/多种类型）鸭子模型

```python
# Python
def func(arg):
    v = arg[-1] # arg.append(9)
    print(v)          # 支持多种数据类型

# java
def func(str arg):   # 必须指定数据类型
    v = arg[-1]
    print(v) 
```

#### 5.1重点:鸭子模型

**对于一个函数而言，Python对于参数的类型不会限制，那么传入参数时就可以是各种类型，在函数中如果有例如：arg.send方法，那么就是对于传入类型的一个限制（类型必须有send方法）。**
**这就是鸭子模型，类似于上述的函数我们认为只要能呱呱叫的就是鸭子（只有有send方法，就是我们要想的类型）**

### 6.总结

#### 6.1 对象的3大特性

面向对象的三大特性：封装/继承/多态 

- 封装
  - 类封装函数,成为方法
  - 对象封装值,方便以后调用.
- 继承
  - 子类继承父类的方法
  - self到底是谁？
  - self是由于那个类创建，则找方法时候就从他开始找。

#### 6.2格式和关键字

class    创建类的对象

#### 6.3应用场景

- 函数（业务功能）比较多，可以使用面向对象来进行归类。
- 想要做数据封装（创建字典存储数据时，面向对象）。
- 游戏示例：创建一些角色并且根据角色需要再创建人物。





