#!/usr/bin/env python        	
# -*- coding:utf-8 -*-	 
#@Project  : s21day19 
#@Author : Jack Deng       
#@Time   :  2019-04-23 19:17
#@File   : day19_homewok.py

# 1.
"""
class 类名(首字母大写)
    def 方法名():
        函数代码
        return

obj = 类名()  --->创建一个类的对象
obj.方法名()    ---->调用类中的方法
"""

# 2.
"""
1.封装:类封装各种方法,对象可以封装值,方便以后调用
2.继承:类可以继承类,子类(派生类)可以继承父类(基类)的方法.对象由哪个类创建,优先去哪个类寻找方法.
3.多态:对于函数而言,python支持多种类型的参数传入.
"""

# 3.
class Functions1:
    def func(self,al):
        print(al)

obj = Functions1()
obj.func('邓益新')


# 4.
"""
self指的就是对象,类创建的对象
"""

# 5.
"""
封装特性:对象把值封装,方法以后调用.
"""

# 6.
"""
类给函数分类和划分,封装
"""

# 7.
"""
class Foo:
    def func(self):
        print('foo.func')
        
obj = Foo()
result = obj.func()   # 打印  'foo.func' 
print(result)   # None
"""

# 8.
import math
class Round:
    def __init__(self,r):
        self.radius = r

    def girth(self):
        return 4*math.pi*self.radius

    def area(self):
        return self.radius*self.radius*math.pi

obj = Round(10)
print(round(obj.girth(),3))
print(round(obj.area(),3))


# 9.
"""
有时多个类有公共的方法,就把公共的方法放在基类,避免重复编写.所以有继承基类的方法
"""

#10.对象self是哪个类创建的,就优先去哪个类来找方法,找到就停止,找不到就找基类,一路向上,都找不到就报错.

# 11.
"""
class Base1:
    def f1(self):
        print('base1.f1')

    def f2(self):
        print('base1.f2')

    def f3(self):
        print('base1.f3')
        self.f1()


class Base2:
    def f1(self):
        print('base2.f1')


class Foo(Base1, Base2):
    def f0(self):
        print('foo.f0')
        self.f3()


obj = Foo()
obj.f0()    # 先去Foo找,找到f0方法,打印 'foo.f0' ,调用f3函数,Foo没有,先去Base1找,打印'base1.f3'
            #调用f1方法,从Foo开始找,找到base1的f1(),打印'base1.f1'
"""

# 12.
"""
class Base:
    def f1(self):
        print('base.f1')
    def f3(self):
        self.f1()
        print('base.f3')

class Foo(Base):
    def f1(self):
        print('foo.f1')
    def f2(self):
        print('foo.f2')
        self.f3()

obj = Foo()   # Foo创建对象obj
obj.f2()      # 找方法,先到Foo找,找不到再到Base找.调用f2函数,打印'foo.f2'.
              # 调用f3函数,-->调用f1函数,打印'foo.f1',然后打印'base.f3'

"""

# 13.
"""
class Person:
    def __init__(self,n,p,e):
        self.name = n
        self.pwd = p
        self.email = e
    def print_data(self):
        msg = '姓名:%s , 密码:%s , 邮箱:%s' %(self.name,self.pwd,self.email)

user_list = []
while True:
    user = input('请输入用户名:')
    pwd = input('请输入密码:')
    email = input('请输入邮箱:')
    if not '@' in email:
        print('邮箱输入错误,请重新输入')
        continue
    p = Person(user,pwd,email)
    user_list.append(p)
    if len(user_list) == 3:
        break

for i in  user_list:
    i.print_data()

    
"""
# 需求
# 1. while循环提示 户输 : 户名、密码、邮箱(正则满 邮箱格式)
# 2. 为每个 户创建 个对象，并添加到 表中。
# 3. 当 表中的添加 3个对象后，跳出循环并以此循环打印所有 户的姓名和邮箱


# 14.
import os
class User:
    def __init__(self, name, pwd):
        self.name = name
        self.pwd = pwd


class Account:
    def __init__(self):
        # 用户列表，数据格式：[user对象，user对象，user对象]
        self.user_list = []

    def login(self):
        """
        用户登录，输入用户名和密码然后去self.user_list中校验用户合法性
        :return:
        """
        while True:
            user = input('请输入用户名(输入N退出):')
            if user.upper() == 'N': return
            pwd = input('请输入密码:')
            flag = False
            for i in self.user_list:
                if  user == i.name  and  pwd == i.pwd:
                    flag = True
                    print('登录成功')
            if not flag:
                print("用户名或密码错误,请重新登录")
                continue


    def register(self):
        """
        用户注册，没注册一个用户就创建一个user对象，然后添加到self.user_list中，表示注册成功。
        :return:
        """
        while True:
            user = input('请输入注册用户名(输入N退出):')
            if user.upper() == "N" : return
            for i in self.user_list:
                if i.name == user:
                    print('注册用户名已被使用,请重新注册')
                    continue
            pwd = input('请输入注册密码:')
            person = User(user,pwd)
            self.user_list.append(person)
            print('注册成功')


    def run(self):
        """
        主程序
        :return:
        """
        info = {'1':self.register,'2':self.login}
        while True:
            print('欢迎来到OOXX系统\n1.用户注册\n2.用户登录(输入N退出)')
            choice = input('请选择功能序号')
            if choice.upper() == 'N': return
            if not info.get(choice):
                print('序号输入错误,请重新输入')
                continue
            info.get(choice)()

if __name__ == '__main__':
    obj = Account()
    obj.run()


