#!/usr/bin/env python        	
# -*- coding:utf-8 -*-	 
#@Project  : s21day26 
#@Author : Jack Deng       
#@Time   :  2019-05-05 19:01
#@File   : day26 homework.py
import requests
import re

# 1.匹配一篇英文文章的标题
"[A-Z].*?\n"

# 2.匹配一个网址
"http[s]?://www\.\w+\.com"

# 3.# 匹配年月日日期 类似 2018-12-06 2018/12/06 2018.12.06
"\d{4}[\.\-\/][1]?\d[\.\-\/][0-3]\d"

# 4.匹配15位或者18位身份证号
"[1-9]\d{14}(\d{2}[\dx])?"

# 5、从lianjia.html中匹配出标题，户型和面积，结果如下：
# [('金台路交通部部委楼南北大三居带客厅   单位自持物业', '3室1厅', '91.22平米'), ('西山枫林 高楼层南向两居 户型方正 采光好', '2室1厅', '94.14平米')]


f = open(r'C:\Users\Administrator\Desktop\lianjia.html','r',encoding='utf-8')
val = f.read()
f.close()
pattern = '<div class="title">.*?>(?P<title>.*?)</a>.*?<span class="divide">/</span>(?P<house>.*?)<span class="divide">/</span>(?P<area>.*?)<span class="divide">/</span>'
ret = re.compile(pattern , flags= re.S)
result = ret.finditer(val)
house_list = []
for i in result:
    house_list.append((i.group('title'),i.group('house'),i.group('area')))
print(house_list)




# 6、1-2*((60-30+(-40/5)*(9-2*5/3+7/3*99/4*2998+10*568/14))-(-4*3)/(16-3*2))
# 从上面算式中匹配出最内层小括号以及小括号内的表达式
"\([-]?\d+[-*/+]\d+([-*/+]\d+)*\)"

# 7、从类似9-2*5/3+7/3*99/4*2998+10*568/14的表达式中匹配出乘法或除法
"\d+[*/]\d+([*/]\d+)*"

# 8、通读博客，完成三级菜单
# http://www.cnblogs.com/Eva-J/articles/7205734.html

# 1.使用堆栈实现三级菜单

menu = {
    '北京': {
        '海淀': {
            '五道口': {
                'soho': {},
                '网易': {},
                'google': {}
            },
            '中关村': {
                '爱奇艺': {},
                '汽车之家': {},
                'youku': {},
            },
            '上地': {
                '百度': {},
            },
        },
        '昌平': {
            '沙河': {
                '老男孩': {},
                '北航': {},
            },
            '天通苑': {},
            '回龙观': {},
        },
        '朝阳': {},
        '东城': {},
    },
    '上海': {
        '闵行': {
            "人民广场": {
                '炸鸡店': {}
            }
        },
        '闸北': {
            '火车战': {
                '携程': {}
            }
        },
        '浦东': {},
    },
    '山东': {},
}
# lst = [menu]
# while lst:
#     for key in lst[-1]:
#         print(key)
#     area = input('请输入你查看的地点N退出,B返回上一级')
#     if area.upper() == 'B':  lst.pop()
#     if area.upper() == 'N':  break
#     if area in lst[-1] and  lst[-1][area] :
#         lst.append(lst[-1][area])

"""
def func(menu):
    while True :
        for key in menu :
            print(key)
        area = input('请输入你查看的地点N退出,B返回上一级')
        if area.upper() == 'Q': return 'q'
        if area.upper() == 'B': return 'B'
        elif menu.get(area) :
            result = func(menu[area])
            if result == 'q' : return 'q'
func(menu)
"""
