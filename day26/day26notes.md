# python day 26

#### 1.re模块

##### 1.转义符

```python
# 正则表达式的转义符和python字符串中的转义符没有关系,且可能会有冲突, 为了避免这种冲突我们所有的正则都以在工具中的测试结果为结果然后只需要在正则和待匹配的字符串外面都加r即可
```

##### 2.re模块的方法

1. findall   和   finditer使用

   ```python
   import re         # 导入re模块(regex正则英文)
   ret = re.findall('\d','123456sedf346dfs1223')
   print(ret)    # 返回一个列表,是所有匹配到的数据组成的列表,未匹配是None
   
   # findall 会匹配字符串中所有符合规则的项,并返回一个列表.
   
   #---------------------------------------------#
   ret = re.finditer('\d','123456sedf346dfs1223')       # ret是个迭代器,这样省内存
   for item in ret :                                    # 迭代出来的都是一个个对象,使用group取值
       print(item.group())
   ```

2. search  和  match的使用

   ```python
   import re
   ret = re.search('\d','123456sedf346dfs1223')
   print(ret)                      # 是个对象
   print(ret.group())              # 取出匹配到的值,若没有匹配成功,ret = None , 会报错,None不支持.group()方法
   
   #---------------------------------------#
   if ret :
       print(ret.group())           # 正确写法
       
   # _---------------------------------------------_#
   ret2 = re.match('\d','123456sedf346dfs1223')    # 只能匹配开头字符串,开头不符合则返回None,符合返回匹配到的字符串
   
   # match 相当于  search + 正则的^(开头判定)
   
   ```

3. compile方法

   ```python
   # 节省时间, 在同一个正则表达式重复使用多次的时候使用能够减少时间的开销
   import re
   ret = re.compile('\d')       # 可选参数flags = re.S 等等
   result = ret.findall/finditer/search ('待匹配字符串')
   
   # 即可使用
   # # \d 正则表达式  ——> 字符串
       # \d  str
           # 循环str，找到所有的数字         --->每次使用重复正则表达式,py解读耗时
   ```

4. 其他不重要方法--->sub   subn    split    

   ```python
   import re
   
   ret_list = re.split('\d','sss123swde45d11cd4e51')
   # 得到以数字为分割符的列表,没有数字的
   
   # -----------------------------------# 
   ret_list = re.split('(\d)','sss123swde45d11cd4e51')
   # 得到以数字为分割符的列表,会将分割符保留在列表中.
   
   # ----------------------------# 特别的
   ret_list = re.split('\d(\d)','sss123swde45d11cd4e51')
   # 也会保存,但只会保存括号内分隔符.
   
   
   
   
   # sub  和 subn 方法  --->类似于字符串的replace方法
   ret = re.sub('\d','D','alex83wusir74taibai',1)   # 把第一个数字替换为'D',得到字符串
   
   ret2 = re.subn('\d','D','alex83wusir74taibai')   # 把第数字替换为'D',得到二元元组,前者是替换后的字符串,后者是替换了多少次的次数n
   
   ```

5. 提供效率问题

   ```python
   # re 模块的进阶
   # 1.时间复杂度  效率  compile
       # 在同一个正则表达式重复使用多次的时候使用能够减少时间的开销
   # 2.空间复杂度  内存占用率   finditer
       # 在查询的结果超过1个的情况下，能够有效的节省内存，降低空间复杂度，从而也降低了时间复杂度
   # 3.用户体验
   ```

##### 3. re模块的分组和命名

```python
import re
s1 = '<h1>wahaha</h1>'
ret = re.search('<(\w+)>(.*?)</\w+>',s1)          # 支持'group()的都能用 (search / finditer),有几个括号就有几个分组,以1开始,默认是0, 表示取整个正则匹配的结果.             (类似位置参数的思想)
print(ret)
print(ret.group(0))   # group参数默认为0 表示取整个正则匹配的结果
print(ret.group(1))   # 取第一个分组中的内容
print(ret.group(2))   # 取第二个分组中的内容

# ------------------------------------  # 
 #  给分组起个别名  --->           (?P<别名>正则表达式)   --->类似于关键字传参
s1 = '<h1>wahaha</h1>'
ret2 = re.search('<(?P<tag>\w+)>(?P<cont>.*?)</\w+>',s1)
print(ret2)                 # 对象
print(ret.group('tag'))     # 取tag分组中的内容
print(ret.group('cont'))    # 取cont分组中的内容


# 分组引用  (?P=组名)   这个组中的内容必须完全和之前已经存在的组匹配到的内容一模一样
s1 = '<h1>wahaha</h1>'
ret3 = re.search('<(?P<tag>\w+)>(?P<cont>.*?)</(?P=tag)>',s1)
print(ret3)                 # 对象
print(ret.group('tag'))     # 取tag分组中的内容

### 注意 : (?P=组名)  括号内不能有空格,会报错


# 特别的  , 另一种分组引用(类似位置传参)
s1 = '<h1>wahaha</h1>'
ret3 = re.search(r'<(?P<tag>\w+)>(?P<cont>.*?)</(\1)>',s1)           # 使用r将其转义.(\n),这个n是前面分组对应的n.第n个分组
print(ret3)                 # 对象
print(ret.group('tag'))     # 取tag分组中的内容
```

##### 4. re模块 总结

```python
re.finall()          # 正则 + 待匹配字符串,返回列表.
re.finditer()        # 于上类似,返回类,迭代的item.group()取值.

re.search()          # 正则 + 待匹配字符串,返回对象,使用.group()取值.
re.match()           # 正则 + 待匹配字符串,返回匹配的字符串.

obj = re.compile('正则')    # 对此利用重复的表达式来匹配,使用compile提供效率.
obj.search/finditer()  # 等等

re.split('正则','匹配字符串')       # 加括号保留分割符
re.sub() \ subn()     # 方法的返回值

# 分组()即可   ,  .group(0)  # 1,2,3
# 分组命名  (?P<别名>正则)   # .group('别名')
# 分组引用  (?P=别名)        或者(\1) ---> 前面要转义 + r 
#  (?P=别命) 表示这个组中的内容必须和之前已经存在的一组匹配到的内容完全一致

# 分组() 和findall的联系,列表优先显示分组内的内容(默认)   ---> 去除默认,    (?:正则)


```

- 有的时候我们想匹配的内容包含在不相匹配的内容当中，这个时候只需要把不想匹配的先匹配出来，再通过手段去掉

  如匹配只匹配整数,会把小数的整数和小数部分分别匹配,这次匹配整数或小数,再去除小数即可

```python
import re
ret=re.findall(r"\d+\.\d+|(\d+)","1-2*(60+(-40.35/5)-(-4*3))")
print(ret)
ret.remove('')
print(ret)
```

##### 5. re模块的部分方法中的flags 参数

```python
flags有很多可选值：

re.I(IGNORECASE)忽略大小写，括号内是完整的写法
re.M(MULTILINE)多行模式，改变^和$的行为
re.S(DOTALL)点可以匹配任意字符，包括换行符
re.L(LOCALE)做本地化识别的匹配，表示特殊字符集 \w, \W, \b, \B, \s, \S 依赖于当前环境，不推荐使用
re.U(UNICODE) 使用\w \W \s \S \d \D使用取决于unicode定义的字符属性。在python3中默认使用该flag
re.X(VERBOSE)冗长模式，该模式下pattern字符串可以是多行的，忽略空白字符，并可以添加注释

```

#### 2.re 和 爬虫的应用

##### 1.生成器对象的send 方法

```python
#  生成器对象是一个迭代器。但是它比迭代器对象多了一些方法，它们包括send方法，throw方法和close方法。这些方法，主要是用于外部与生成器对象的交互。本文先介绍send方法。

# send方法有一个参数，该参数指定的是上一次被挂起的yield语句的返回值。

# 本质是给yield 语句的返回值 赋值
```

- 注意 : 一般用send方法之前,先next() 一波,主要是用于外部与生成器对象的交互。 

```python
import requests
import json
import re                     # 导入3个模块

pattern = '<div class="item">.*?<em class="">(?P<id>\d+)</em>.*?<span class="title">(?P<title>.*?)</span>.*?' \
              '<span class="rating_num".*?>(?P<score>.*?)</span>.*?<span>(?P<comment_num>.*?)人评价</span>'
obj = re.compile(pattern,flags = re.S)
url = 'https://movie.douban.com/top250?start=%s&filter='

def get_page(url):                  # 构造函数,爬取网站信息,返回值是网站内容的大字符串
    ret = requests.get(url)
    return  ret.text

def parser_page(url):
    response = get_page(url)
    result = obj.finditer(response)
    for i in result:
        yield  [i.group('id'), i.group("title"), i.group('score'), i.group('comment_num')]


def write(fielname) :
    with open(fielname,'w',encoding='utf-8') as f:
        while True :
            list = yield  None
            f.write(json.dumps(list , ensure_ascii = False) + '\n')


ttt = write('小明爬知乎')
print(type(ttt))         # ---><class 'generator'>生成器类,支持write 方法
print(ttt.send(None))
num = 0
for item  in range(10) :
    result = parser_page('https://movie.douban.com/top250?start=%s&filter=' %num)
    num += 25
    for i in result :
        ttt.send(i)
ttt.close()
```

##### 

