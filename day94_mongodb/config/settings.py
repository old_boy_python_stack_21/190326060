# -*- coding:utf-8 -*-
# __author__ = Deng Jack

from pymongo import MongoClient
from pymongo.database import Database
from pymongo.collection import Collection

myclient = MongoClient()
# print(myclient.list_database_names())  # 所有数据库组成的列表


mydb = myclient['oldboys21']  # type:Database   # 可以使用  . 和 getitem 来获取,都可以

pklist = mydb['pklist']  # type:Collection  # 可以使用  . 和 getitem 来获取,都可以

Package = [
    {'name': "大砍刀", 'atc': 20, 'def': -5, 'life': 0},
    {'name': "黄金甲", 'atc': -5, 'def': 20, 'life': 200},
    {'name': "小红药", 'atc': 5, 'def': 0, 'life': 100},
    {'name': "大红药", 'atc': 10, 'def': 0, 'life': 250},
    {'name': "多兰剑", 'atc': 10, 'def': 0, 'life': 100},
    {'name': "霸者重甲", 'atc': 0, 'def': 100, 'life': 1000},
    {'name': "暴风剑", 'atc': 50, 'def': 0, 'life': 0},
    {'name': "无尽之刃", 'atc': 90, 'def': 0, 'life': 0},
]
