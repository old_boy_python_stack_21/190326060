from flask import Blueprint ,request ,render_template ,views ,redirect ,session
from pymongo.collection import Collection
from config.settings import pklist # type:Collection
from random import sample,randint
from config.settings import Package

auth = Blueprint('auth',__name__)

@auth.route('/login',methods=['get','post'])
def login():
    if request.method == 'GET':
        return render_template('login.html')
    else:
        play1 = request.form.get('username')
        pwd = request.form.get('pwd')
        res = pklist.find_one({'username':play1, 'pwd':pwd})
        if res:
            print(res)
            res['_id'] = str(res['_id'])
            print(res)
            session['user'] = res
            return redirect('/players')
        else:
            error = '用户名或者密码错误'
            return render_template('login.html',error=error)


class Reg(views.MethodView):
    def get(self,error=None):
        return render_template('reg.html',error=error)

    def post(self):
        data = request.form.to_dict()
        print(data)
        if data.get('pwd',1) == data.get('repwd',2):
            data.pop('repwd')
            data['ATC'] = randint(15,20)
            data['DEF'] = randint(50,65)
            data['LIFE'] = randint(800,950)
            data['Equip'] = []
            data['Package'] = sample(Package,3)
            pklist.insert_one(data)
            return redirect('/login')
        else:
            return self.get(error='俩次输入密码不一致,请重新注册')

auth.add_url_rule('/reg',endpoint='reg',view_func=Reg.as_view(name='xx'),)