# __author__ = Deng Jack

from flask import Blueprint, render_template, session, redirect
from config.settings import pklist
from bson import ObjectId

pb = Blueprint('play', __name__)


@pb.route('/players')
def players():
    user_info = session.get("user")
    user_info = pklist.find_one({"_id": ObjectId(user_info.get("_id"))})
    print(user_info)
    killer = pklist.find({"_id": {"$ne": ObjectId(user_info.get("_id"))}})
    return render_template("players.html", player=user_info, killer=killer)


@pb.route('/set_equip/<equip>/<_id>')
def set_equip(equip, _id):
    player = pklist.find_one({'_id': ObjectId(_id)})
    for item in player['Package']:
        print(item.get('name'))
        if item.get('name') == equip:
            print('oj')
            player['Package'].remove(item)
            player['Equip'].append(item)
            print(player)
            player['ATC'] += item.get('atc')
            player['DEF'] += item.get('def')
            player['LIFE'] += item.get('life')
            print('>>>>>>>>', player)
            break
    pklist.update_one({"_id": ObjectId(_id)}, {"$set": player})
    return redirect('/players')


@pb.route('/unset_equip/<equip>/<_id>')
def unset_equip(equip, _id):
    player = pklist.find_one({'_id': ObjectId(_id)})
    for item in player['Equip']:
        if item.get('name') == equip:
            player['Equip'].remove(item)
            player['Package'].append(item)

            player['ATC'] -= item.get('atc')
            player['DEF'] -= item.get('def')
            player['LIFE'] -= item.get('life')
            break
    pklist.update_one({"_id": ObjectId(_id)}, {"$set": player})
    return redirect('/players')


@pb.route('/skill/<_id>')
def skill(_id):
    killer_info = pklist.find_one({"_id": ObjectId(_id)})
    killer_name = killer_info.get("username")
    player_info = pklist.find_one({"_id": ObjectId(session["user"].get("_id"))})
    player_name = player_info.get("username")
    playList = [player_name, killer_name]
    pk_log = {
        'PlayList': [player_info, killer_info],
        'PK_list': []
    }
    while True:
        # player 先攻击,回合制攻击,不考虑暴击
        ret = athck(player_info, killer_info, pk_log)
        if ret:
            return congrateution(ret, playList)
        ret = athck(killer_info, player_info, pk_log)
        if ret:
            return congrateution(ret, playList)


def congrateution(loser, playList):
    winner = playList[0]
    return render_template('winner.html', winner=winner, loser=loser)


def athck(atcpl, defpl, pk_log):
    der_life = int(atcpl.get('ATC')) * (int(defpl.get('DEF')) / (int(defpl.get('DEF')) + 100))
    pk_dict1 = {
        'atcplayer': f"{atcpl['username']}",
        'defplayer': f"{defpl['username']}",
        'info': f"{atcpl['username']} atc {defpl['username']} ,{defpl['username']} life -{der_life}",
    }
    defpl['LIFE'] = round(defpl['LIFE'] - der_life)
    pk_log['PK_list'].append(pk_dict1)
    if defpl['LIFE'] <= 0:
        return defpl['username']
