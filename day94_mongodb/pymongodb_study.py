from flask import Flask ,request,Markup

from serv.auth import auth
from serv.play_start import pb

app = Flask(__name__)
app.secret_key='Deng Jack'


@app.route('/')
def index():
    return Markup('<h1>OK !!! ,Flask Server is Running</h1>')


app.register_blueprint(auth)
app.register_blueprint(pb)

if __name__ == '__main__':
    app.run('0.0.0.0',7777,debug=True)
