# -*- coding:utf-8 -*-
# __author__ = Deng Jack

import redis


def redis_pool():
    """
    数据连接池,使用connection pool来管理对一个redis server的所有连接，避免每次建立、释放连接的开销。
    :return:
    """
    pool = redis.ConnectionPool(host='127.0.0.1', port=6379, db=0)
    r = redis.Redis(connection_pool=pool)
    return r


def redis_connect():
    """
    单独的连接
    :return:
    """
    r = redis.Redis(charset='utf-8')
    return r

r = redis_connect()

r.set("name","zhangsan")

print(r.get("name")) #输出:zzangsan
r.setrange("name",2,"z333333333333")
print(r.get("name")) #输出:zzangszzzzzzz

r.hkeys()