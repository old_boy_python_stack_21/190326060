# -*- coding:utf-8 -*-
# __author__ = Deng Jack
from redis import Redis

class DebugConfig(object):
    DEBUG = True
    SECRET_KEY = 'Deng Jack'
    SESSION_TYPE = 'redis'

