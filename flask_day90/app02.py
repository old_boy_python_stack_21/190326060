# -*- coding:utf-8 -*-
# __author__ = Deng Jack

from flask import Flask, session, request, redirect, render_template, make_response, url_for ,Blueprint
from redis import Redis
from flask_session import Session
from flask.views import MethodView

import settings

app = Flask(__name__)



app.config['SESSION_REDIS'] = Redis(host='127.0.0.1', port=6379)

app.config.from_object(settings.DebugConfig)



class Login(MethodView):
    def get(self):
        return 'get login'

    def post(self):
        pass

    def deng(self):
        return 'ok'

app.add_url_rule('/login',view_func=Login.as_view(name='login3'))

@app.route('/set')
def set_session():
    session['key'] = 123
    ret = make_response('ok',)

    print(url_for('login3'))
    ret.set_cookie('user','dengn')
    return ret

@app.before_request
def aa():
    print(request.method)


if __name__ == '__main__':
    app.run()
