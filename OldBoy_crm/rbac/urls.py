from django.conf.urls import url
from rbac import views as rbac_view

urlpatterns = [
    url(r'^role_list/$', rbac_view.role_list, name='role_list'),
    url(r'^role_add/$', rbac_view.role_change, name='role_add'),
    url(r'^role_edit/(\d+)/$', rbac_view.role_change, name='role_edit'),
    # url(r'^role_del/(\d+)/$', rbac_view.role_del, name='role_del'),

    url(r'^menu_list/$', rbac_view.menu_list, name='menu_list'),
    url(r'^menu_edit/(\d+)/$', rbac_view.menu_change, name='menu_edit'),
    url(r'^menu_add/$', rbac_view.menu_change, name='menu_add'),
    # url(r'^menu_del/(\d+)/$', rbac_view.menu_del, name='menu_del'),

    url(r'^permission_add/$', rbac_view.permission_change, name='permission_add'),
    url(r'^permission_edit/(\d+)/$', rbac_view.permission_change, name='permission_edit'),
    # url(r'^permission_del/(\d+)/$', rbac_view.permission_del, name='permission_del'),

    url(r'^(role|permission|menu)/del/(\d+)/$', rbac_view.delete, name='delete'),

    url(r'^multi/permissions/$', rbac_view.multi_permissions, name='multi_permissions'),

    url(r'^distribute/permissions/$', rbac_view.distribute_permissions, name='distribute_permissions'),
]
