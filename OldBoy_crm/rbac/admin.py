from django.contrib import admin

from rbac import models


class PermissionConfig(admin.ModelAdmin):
    list_display = ['id', 'url', 'title', 'menu', 'name']
    list_editable = ['url', 'title', 'menu', 'name']


# Register your models here.
admin.site.register(models.Permission, PermissionConfig)
# admin.site.register(models.User)
admin.site.register(models.Role)
admin.site.register(models.Menu)
