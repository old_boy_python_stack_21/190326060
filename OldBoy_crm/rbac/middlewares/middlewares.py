from django.utils.deprecation import MiddlewareMixin
from rbac.models import User
from django.shortcuts import HttpResponse, redirect
from django.urls import reverse
from django.http.response import HttpResponse
from re import match
from django.conf import settings


class Auth_Permission_Url(MiddlewareMixin):

    def process_request(self, request):
        # 获取当前访问的URL
        path = request.path_info  # 不带参数
        request.breadcrumb_list = [{'title': '首页', 'url': '/crm/index/'}]
        request.current_menu_id = None

        # 白名单放过
        for pattern in settings.WHITE_LIST:
            if match(pattern, path):
                return  # 后面的代码不执行
        # 登录状态的校验

        if not request.session.get('is_login'):
            if path == reverse('login'):
                return redirect(reverse('login'))
            return redirect(reverse('login') + '?next={}'.format(path))  # 让用户登陆

        # 免认证的校验
        for pattern in settings.NO_PERMISSION_LIST:
            if match(pattern, path):
                return

        # 获取权限信息
        permissions_dict = request.session.get(settings.PERMISSION_SESSION_KEY)
        # 权限的校验
        # print(permissions_dict)
        # print(permissions_dict.values())
        for i in permissions_dict.values():
            # permission 此时是一个字典
            # print(r"^{}$".format(permission['permission__url']),'pattern')
            if match(r"^{}$".format(i['url']), path):
                request.current_menu_id = i['pid'] if i['pid'] else i['id']
                if i['pid']:
                    # 是一个子权限
                    # json的坑,序列化时,若是字典,会将字典的键自动str为字符串
                    pid = str(i['pid'])
                    request.breadcrumb_list.append(
                        {'title': permissions_dict[i['pname']]['title'], 'url': permissions_dict[i['pname']]['url']})
                    request.breadcrumb_list.append({'title': i['title'], 'url': i['url'], })
                else:
                    # 是一个父权限
                    request.breadcrumb_list.append({'title': i['title'], 'url': i['url'], })
                return
        return HttpResponse('没有访问权限,请联系管理员')
