from django.template import Library
from django.conf import settings
from collections import OrderedDict
from re import match
from django.conf import settings

register = Library()


@register.inclusion_tag('menu.html')
def show_menu(request):
    menu_dict = request.session.get(settings.MENU_SESSION_KEY)
    od = OrderedDict()

    key_list = sorted(menu_dict, key=lambda x: menu_dict[x]['weight'], reverse=True)
    for item in key_list:
        od[item] = menu_dict[item]
    # 使用正则匹配
    # for value in od.values():
    #     value['class'] = 'hide'
    #     for i in value['children']:
    #         if match(r'{}$'.format(i['url']), request.path_info):
    #             i['class'] = 'active'
    #             value['class'] = ''
    for value in od.values():
        value['class'] = 'hide'

        for i in value['children']:
            if request.current_menu_id == i['id']:
                i['class'] = 'active'
                value['class'] = ''
    return {'menu_list': od.values()}


@register.inclusion_tag('breadcrumb.html')
def breadcrumb(request):
    breadcrumb_list = request.breadcrumb_list
    # print(permissions_dict)
    return {'breadcrumb_list': breadcrumb_list}


@register.filter()
def has_permission(request, name):
    permissions_dict = request.session.get(settings.PERMISSION_SESSION_KEY, None)
    if name in permissions_dict:
        return True


@register.simple_tag
def gen_role_url(request, rid):
    params = request.GET.copy()
    params['rid'] = rid
    return params.urlencode()
