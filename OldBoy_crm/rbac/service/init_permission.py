from django.conf import settings


def init_permission(request, obj):
    # 二级菜单的设置

    # 获取用户的权限
    permissions = obj.roles.filter(permissions__url__isnull=False).values(
        'permissions__url',
        'permissions__title',
        'permissions__menu__title',
        'permissions__menu__icon',
        'permissions__menu__weight',
        'permissions__menu_id',
        'permissions__id',
        'permissions__parent_id',
        'permissions__parent__name',
        'permissions__name',
    ).distinct()
    # print(permissions)
    # 权限信息的列表
    permissions_dict = {}
    # 菜单信息的列表
    menu_dict = {}

    # print(permissions)
    for i in permissions:  # {  'permissions__url', }
        permissions_dict[i['permissions__name']] = ({'url': i['permissions__url'],
                                                   'title': i['permissions__title'],
                                                   'id': i['permissions__id'],
                                                   'pid': i['permissions__parent_id'],
                                                   'name': i['permissions__name'],
                                                   'pname': i['permissions__parent__name'],
                                                   })

        menu_id = i.get('permissions__menu_id')
        if not menu_id:
            continue  # 普通权限,没有关联菜单,是普通权限

        if menu_id not in menu_dict:
            menu_dict[menu_id] = {
                'title': i.get('permissions__menu__title'),
                'icon': i.get('permissions__menu__icon'),
                'weight': i.get('permissions__menu__weight'),
                'children': [
                    {'title': i.get('permissions__title'), 'url': i.get('permissions__url'),
                     'id': i.get('permissions__id')}]
            }
        else:
            menu_dict[menu_id]['children'].append(
                {'title': i.get('permissions__title'), 'url': i.get('permissions__url'), 'id': i.get('permissions__id')})
        # 保存到session中
    # print(menu_dict)
    # print(permissions_dict)
    request.session[settings.PERMISSION_SESSION_KEY] = permissions_dict  # json序列化
    request.session[settings.MENU_SESSION_KEY] = menu_dict  # json序列化
