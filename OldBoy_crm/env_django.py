import os
import random


def add_跟进记录():
    text = ["最灵繁的人也看不见自己的背脊。",
            "最困难的事情就是认识自己。",
            "有勇气承担命运这才是英雄好汉。",
            "与肝胆人共事，无字句处读书。",
            "阅读使人充实，会谈使人敏捷，写作使人精确。",
            "最大的骄傲于最大的自卑都表示心灵的最软弱无力。",
            "自知之明是最难得的知识。"]

    lst = []
    for i in range(1, 41):
        # if random.randint(1, 10) > 6:
        obj = models.ConsultRecord(customer_id=i,
                                   note='跟进内容是>>{}<<,'.format(random.choice(text)),
                                   status=random.choice(['A', 'B', 'C', 'D', 'E', 'F', 'G']),
                                   consultant_id=random.randint(4, 6)
                                   )
        lst.append(obj)
    # else:
    #     continue
    models.ConsultRecord.objects.bulk_create(lst)


def add_客户():
    i = 0
    lst = []
    with open('信息表.txt', 'r', encoding='utf-8-sig') as f:
        for line in f:
            line = line.strip().split(',')
            obj = models.Customer(qq=line[0], qq_name=line[1], name=line[1],
                                  sex=random.choice(['male', 'female']),
                                  course=random.choice(
                                      ['Linux', 'PythonFullStack', 'LinuxMaintenance', 'GoFullStack', 'Html前端',
                                       'Javascript语言']),
                                  status=random.choice(['signed', 'unregistered', 'studying', 'paid_in_full']),
                                  # consultant_id=random.randint(4, 6),
                                  # classlist_id=random.randint(1, 6),
                                  )
            lst.append(obj)

    models.Customer.objects.bulk_create(lst)


def add_客户_班级关联():
    all = models.Customer.objects.all()
    for i in all:
        i.class_list.set([random.randint(1, 6), ])
        i.save()


if __name__ == '__main__':
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "OldBoy_crm.settings")
    import django

    django.setup()

    from crm import models

    add_客户()
