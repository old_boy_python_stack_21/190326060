# 视图
from django.shortcuts import render

from django.conf import settings

# form组件
# 分页器


# 首页
def index(request):
    return render(request, 'index.html')

# def customer_list(request, username=None):
#     if request.path_info == reverse('customer_list'):
#         customer_list = models.Customer.objects.filter(consultant__isnull=True)
#         # 公户>>customer_list
#     else:
#         # 私户>>my_customer
#         customer_list = models.Customer.objects.filter(consultant_id=request.session.get('user_id'))
#     page = request.GET.get('page', 1)
#     total_num = len(customer_list)
#     obj = Pagination(page=page, total_num=total_num)
#     return render(request, 'customer_list.html',
#                   {"customer_list": customer_list[obj.page_start:obj.page_end],
#                    'pagination': obj.page_html})


# def add_customer(request):
#     customer = CustomerForm()
#     if request.method == 'POST':
#         customer = CustomerForm(request.POST)
#         # 对提交的数据进行校验
#         if customer.is_valid():
#             # 校验成功  把数据插入数据中
#             # models.UserProfile.objects.create(**form_obj.cleaned_data)
#             customer.save()
#             if "_save" in request.POST:
#                 return redirect('customer_list')
#             else:
#                 return redirect('add_customer')
#     return render(request, "add_customer.html", {"customer": customer})


# def edit_customer(request):
#     pk = request.GET.get('pk')
#     if request.method == 'POST':
#         print(request.POST)
#         customer_obj = models.Customer.objects.filter(pk=pk)
#         if customer_obj and len(customer_obj) == 1:
#             try:
#                 customer_obj.update(request.POST)
#             except Exception as e:
#                 print(e)
#             # customer = CustomerFrom(request.POST)
#             # # 对提交的数据进行校验
#             # if customer.is_valid():
#             #     # 校验成功  把数据插入数据中
#             #     # models.UserProfile.objects.create(**form_obj.cleaned_data)
#             #     customer.save()
#             return redirect('customer_list')
#         # print(customer.errors)
#
#     edit_customer_dic = models.Customer.objects.filter(pk=pk).values().first()
#     customer_obj = CustomerForm(edit_customer_dic)
#     return render(request, 'edit_customer.html', {"customer": customer_obj})


# 使用反射多个参查询参数和合而为一的尝试
# class BaseList(View):
#     info = {'customer_list': ('Customer', 'customer_list.html'),
#             'my_customer': ('Customer', 'customer_list.html'),
#             'consult_list': ('ConsultRecord', 'consult_list.html', 'consult_record'),
#             'enrollment_list': ('Enrollment', 'enrollment_list.html', 'all_enrollment'),
#             'payment_list': ('PaymentRecord', 'payment_list.html', 'all_payment'),
#             'study_list': ('StudyRecord', 'study_list.html', 'all_study'),
#             'course_list': ('CourseRecord', 'course_list.html', 'all_course'),
#             }
#
#     def get(self, request, pk=None):
#         for key, value in self.info.items():
#             if request.path.replace('/crm/', "").startswith(key):
#                 classname = value[0]
#                 html_name = value[1]
#                 obj_name = value[2]
#                 break
#         else:
#             classname = 'ConsultRecord'
#             html_name = 'consult_list.html'
#             obj_name = 'consult_record'
#         print(classname, getattr(models, classname))
#         try:
#             if not pk:
#                 # consultant = self.request.user_obj,
#                 # delete_status = False
#                 # Q(('consultant', self.request.user_obj)),
#                 # Q(('delete_status', False))
#                 all_obj = getattr(models, classname).objects.filter(consultant=self.request.user_obj,
#                                                                     delete_status=False)  # 获取客户对象
#             else:
#                 all_obj = getattr(models, classname).objects.filter(Q(('customer_id', pk)),
#                                                                     Q(('delete_status', False)), )
#         except Exception as e:
#             print(e)
#             if not pk:
#                 all_obj = getattr(models, classname).objects.filter(
#                     Q(('customer__in', self.request.user_obj.customers.all())),
#                     Q(('delete_status', False)), )
#                 # customer__in=self.request.user_obj.customers.all(),
#                 #                                                 delete_status=False)  # 获取客户对象
#             else:
#                 all_obj = getattr(models, classname).objects.filter(Q(('customer_id', pk)),
#                                                                     Q(('delete_status', False)), )
#                 # customer_id=pk, delete_status=False)
#
#         return render(request, html_name,
#                       {obj_name: all_obj})
