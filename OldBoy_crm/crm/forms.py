from crm import models
from django import forms
from django.core.exceptions import ValidationError
import hashlib
# 得自己导入这个模块,pip install django_multiselectfield
from multiselectfield.forms.fields import MultiSelectFormField


class BaseFrom(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for filed in self.fields.values():
            if isinstance(filed, (MultiSelectFormField, forms.BooleanField,)):
                continue
            filed.widget.attrs['class'] = 'form-control'


class RegForm(forms.ModelForm):
    password = forms.CharField(min_length=6,
                               error_messages={
                                   "required": "不能为空",
                                   "invalid": "格式错误",
                                   "min_length": "用户名最短6位"
                               },
                               widget=forms.widgets.PasswordInput(
                                   attrs={'placeholder': '您的密码', 'autocomplete': 'off', 'class': 'login_txtbx'}))
    re_password = forms.CharField(min_length=6,
                                  error_messages={
                                      "required": "不能为空",
                                      "invalid": "格式错误",
                                      "min_length": "用户名最短6位"
                                  },
                                  widget=forms.PasswordInput(
                                      attrs={'placeholder': '您的确认密码', 'autocomplete': 'off', 'class': 'login_txtbx'}))

    class Meta:
        model = models.UserProfile
        fields = '__all__'  # ['username']
        exclude = ['is_active']
        widgets = {
            'username': forms.EmailInput(attrs={'placeholder': '您的用户名', 'autocomplete': 'off', 'class': 'login_txtbx'}),
            # 'password':forms.PasswordInput(attrs={'placeholder':'您的密码','autocomplete':'off'}),
            'mobile': forms.TextInput(attrs={'placeholder': '您的手机号', 'autocomplete': 'off', 'class': 'login_txtbx'}),
            'name': forms.TextInput(attrs={'placeholder': '您的真实姓名', 'autocomplete': 'off', 'class': 'login_txtbx'}),
            'department': forms.Select(attrs={'class': 'selectinput'})
        }
        error_messages = {
            'username': {
                'required': '必填',
                'invalid': '邮箱格式不正确'
            }
        }

    def __init__(self, *args, **kwargs):
        super(RegForm, self).__init__(*args, **kwargs)
        if hasattr(self.instance, 'user_edit'):
            pass

    def clean(self):
        self._validate_unique = True
        password = self.cleaned_data.get('password', '')
        re_password = self.cleaned_data.get('re_password')
        # if len(password) < 6:
        #     self.add_error('password', '密码长度小于6')
        #     raise ValidationError('密码长度小于6!!')
        if password == re_password:
            # 对密码进行加密
            md5 = hashlib.md5()
            md5.update(password.encode('utf-8'))
            self.cleaned_data['password'] = md5.hexdigest()
            return self.cleaned_data
        else:
            self.add_error('password', '两次密码不一致')
            raise ValidationError('两次密码不一致!!')


class CustomerForm(BaseFrom):
    class Meta:
        model = models.Customer
        fields = '__all__'  # ['username']
        widgets = {
            "qq": forms.NumberInput(attrs={'placeholder': '客户的QQ号码', 'autocomplete': 'off'}),
            "qq_name": forms.widgets.Input(attrs={'placeholder': '客户的QQ昵称', 'autocomplete': 'off'}),
            # 'password':forms.PasswordInput(attrs={'placeholder':'您的密码','autocomplete':'off'}),
            'phone': forms.TextInput(
                attrs={'placeholder': '客户的手机号', 'autocomplete': 'off', }),
            'name': forms.TextInput(attrs={'placeholder': '客户的真实姓名', 'autocomplete': 'off', 'class': 'login_txtbx'}),
            'birthday': forms.widgets.DateInput(attrs={'type': 'date'}),
            'next_date': forms.widgets.DateInput(attrs={'type': 'date'}),
        }

    def __init__(self, request, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['consultant'].choices = [(request.user_obj.pk, request.user_obj)]

    # def clean(self):
    #     self._validate_unique = True
    #     password = self.cleaned_data.get('password', '')
    #     re_password = self.cleaned_data.get('re_password')
    #     # if len(password) < 6:
    #     #     self.add_error('password', '密码长度小于6')
    #     #     raise ValidationError('密码长度小于6!!')
    #     if password == re_password:
    #         # 对密码进行加密
    #         md5 = hashlib.md5()
    #         md5.update(password.encode('utf-8'))
    #         self.cleaned_data['password'] = md5.hexdigest()
    #         return self.cleaned_data
    #     else:
    #         self.add_error('password', '两次密码不一致')
    #         raise ValidationError('两次密码不一致!!')


class ConsultRecordForm(BaseFrom):
    class Meta:
        model = models.ConsultRecord
        fields = '__all__'

    def __init__(self, request, customer_id, *args, **kwargs):
        super().__init__(*args, **kwargs)

        if self.instance.customer_id:
            # 编辑的跟进记录存在,不为None,限定客户为当前客户
            self.fields['customer'].choices = [(i.pk, str(i)) for i in
                                               models.Customer.objects.filter(pk=self.instance.customer_id)]

        elif not bool(int(customer_id)):
            # 这是添加跟进记录是,若customer_id = "0",则限定客户为当前销售的私户
            self.fields['customer'].choices = [('', '---------'), ] + [(i.pk, str(i)) for i in
                                                                       request.user_obj.customers.all()]
        else:
            # 否则为当前客户customer_id对应的客户
            self.fields['customer'].choices = [(i.pk, str(i)) for i in
                                               models.Customer.objects.filter(pk=customer_id)]
        # 限制跟进人为当前销售
        self.fields['consultant'].choices = [(request.user_obj.pk, request.user_obj)]


class EnrollmentForm(BaseFrom):
    class Meta:
        model = models.Enrollment
        fields = "__all__"
        widgets = {
            'semester': forms.widgets.TextInput()
        }

    def __init__(self, request, *args, **kwargs):
        super().__init__(*args, **kwargs)
        print()
        # 限制客户为当前销售的私户
        if self.instance.customer_id != '0':
            self.fields['customer'].choices = [(i.pk, str(i)) for i in
                                               models.Customer.objects.filter(pk=self.instance.customer_id)]
            self.fields['enrolment_class'].choices = [(i.pk, str(i)) for i in self.instance.customer.class_list.all()]
        # self.instance  # models.Enrollment(customer_id=customer_id)
        elif self.instance.customer_id == '0':
            self.fields['customer'].choices = [('', '---------'), ] + [(i.pk, str(i)) for i in
                                                                       request.user_obj.customers.all()]

        # if self.instance.customer_id:
        #     # 编辑的跟进记录存在,不为None,限定客户为当前客户
        #     self.fields['customer'].choices = [(i.pk, str(i)) for i in
        #                                        models.Customer.objects.filter(pk=self.instance.customer_id)]
        #
        # elif not bool(int(customer_id)):
        #     # 这是添加跟进记录是,若customer_id = "0",则限定客户为当前销售的私户
        #     self.fields['customer'].choices = [('', '---------'), ] + [(i.pk, str(i)) for i in
        #                                                                request.user_obj.customers.all()]
        # else:
        #     # 否则为当前客户customer_id对应的客户
        #     self.fields['customer'].choices = [(i.pk, str(i)) for i in
        #                                        models.Customer.objects.filter(pk=customer_id)]
        #     # 限制跟进人为当前销售
        # self.fields['consultant'].choices = [(request.user_obj.pk, request.user_obj)]


class ClassListForm(BaseFrom):
    class Meta:
        model = models.ClassList
        fields = '__all__'
        widgets = {
            'start_date': forms.widgets.DateInput(attrs={'type': 'date'}),
            'graduate_date': forms.widgets.DateInput(attrs={'type': 'date'})

        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        if self.instance.pk:
            # 编辑班级的的时候课程名和学期无法修改

            self.fields['course'].choices = [(self.instance.course, self.instance.get_course_display(),)]
            self.fields['semester'].widget = forms.widgets.TextInput(
                attrs={'readonly': 'readonly', 'class': 'form-control'})


class CourseRecordForm(BaseFrom):
    class Meta:
        model = models.CourseRecord
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if not self.instance.pk:
            # 新增课程记录时,没有限制
            pass
        else:
            # 编辑课程记录时,限定课程名称和讲师不能改变
            self.fields['day_num'].widget = forms.widgets.TextInput(
                attrs={'readonly': 'readonly', 'class': 'form-control'})
            self.fields['teacher'].choices = [(self.instance.teacher_id, self.instance.teacher)]
            self.fields['re_class'].choices = [(self.instance.re_class.pk, self.instance.re_class)]


class StudyRecordForm(BaseFrom):
    class Meta:
        model = models.StudyRecord
        fields = '__all__'


class PaymentList(BaseFrom):
    class Meta:
        model = models.PaymentRecord
        fields = '__all__'

    def __init__(self, request, customer_id, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['consultant'].choices = [(request.user_obj.pk, request.user_obj)]
        # 限制客户为当前销售的私户
        if self.instance.pk:
            self.fields['customer'].choices = [(self.instance.pk, self.instance.customer), ]
            # self.fields['customer'].choices = [(1, '徐俊凯'), ] + [('', '---------'), ]
        elif customer_id != '0':
            # 添加当期客户的缴费记录
            self.fields['customer'].choices = [(i.pk, str(i)) for i in
                                               models.Customer.objects.filter(pk=customer_id)]
            self.fields['enrolment_class'].choices = [(i.pk, str(i)) for i in self.instance.customer.class_list.all()]
        # self.instance  # models.Enrollment(customer_id=customer_id)
        elif customer_id == '0':
            # 添加该销售的所有客户
            self.fields['customer'].choices = [('', '---------'), ] + [(i.pk, str(i)) for i in
                                                                       models.Customer.objects.all()]
