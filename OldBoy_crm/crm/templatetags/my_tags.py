from django import template
from django.urls import reverse
from django.http.request import QueryDict

register = template.Library()


@register.simple_tag
def reverse_url(request, name, *args, **kwargs):
    url = reverse(name, args=args, kwargs=kwargs)  ##e  /edit_customer/pk/
    next = request.get_full_path()
    qt = QueryDict(mutable=True)
    qt['next'] = next
    return_url = "{}?{}".format(url, qt.urlencode())
    return return_url


