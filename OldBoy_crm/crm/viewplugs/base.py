from django.views import View
from django.db.models import Q


class BaseView(View):
    def search(self, lst):
        query = self.request.GET.get('query', '')
        q = Q()
        q.connector = 'OR'
        for field in lst:
            q.children.append(Q(('{}__contains'.format(field), query)))
        return q
