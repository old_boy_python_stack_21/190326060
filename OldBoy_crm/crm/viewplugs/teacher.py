from crm import models
from django.http.response import HttpResponse
from django.shortcuts import render, redirect, reverse
from .base import BaseView

# form组件
from crm.forms import ClassListForm, CourseRecordForm, StudyRecordForm


# 班级列表
class Classlist(BaseView):
    def get(self, request, *args, **kwargs):
        q = self.search(['campuses__name', 'start_date', 'course'])
        all_class_list = models.ClassList.objects.filter(q)
        return render(request, 'teacher/class_list.html',
                      {"all_obj": all_class_list})

# 添加编辑班级
def class_change(request, pk=None, ):
    obj = models.ClassList.objects.filter(pk=pk).first()
    form_obj = ClassListForm(instance=obj)
    title = '编辑班级' if pk else '新增班级'
    if request.method == 'POST':
        print(request.POST)
        form_obj = ClassListForm(request.POST, instance=obj)
        if form_obj.is_valid():
            form_obj.save()
            next_url = request.GET.get('next')
            if next_url:
                return redirect(next_url)
            return redirect(reverse('class_list'))

    return render(request, 'form.html', {'form_obj': form_obj, 'title': title})


# 课程记录展示
class Course_list(BaseView):
    def get(self, request, class_id):
        all_course_record = models.CourseRecord.objects.filter(re_class_id=class_id)
        return render(request, 'teacher/course_list.html',
                      {"all_course_record": all_course_record, 'class_id': class_id})

    def post(self, request, *args, **kwargs):
        action = request.POST.get('action')
        if not hasattr(self, action):
            return HttpResponse('非法操作')
        ret = getattr(self, action)()
        if ret:
            return ret
        return self.get(request, *args, **kwargs)

    def multi_init(self):
        course_record_ids = self.request.POST.getlist('pk')
        # 获取['1', '2'] ,批量初始化课程记录的ID
        course_records = models.CourseRecord.objects.filter(pk__in=course_record_ids)
        for course_record in course_records:
            # 查找学生
            students = course_record.re_class.customer_set.all().filter(status='studying')
            # for student in students:
            #     models.StudyRecord.objects.create(course_record=course_record,student=student)

            study_record_list = []
            for student in students:
                study_record_list.append(models.StudyRecord(course_record=course_record, student=student))
            models.StudyRecord.objects.bulk_create(study_record_list)


# 新增或编辑课程记录
def course_record_change(request, pk=None, class_id=None, ):
    # 如果存在class_id,则是新增课程记录
    # 否则是编辑课程记录
    obj = models.CourseRecord(teacher=request.user_obj, re_class_id=class_id) \
        if class_id else models.CourseRecord.objects.filter(pk=pk).first()
    form_obj = CourseRecordForm(instance=obj)
    if request.method == 'POST':
        form_obj = CourseRecordForm(request.POST, instance=obj)
        if form_obj.is_valid():
            form_obj.save()
            return redirect(reverse('course_list', args=(class_id,)))
    title = '编辑课程记录' if pk else '新增课程记录'
    return render(request, 'form.html', {'form_obj': form_obj, 'title': title})


# 学习记录
from django.forms import modelformset_factory

# 展示新增编辑
def study_record_list(request, course_record_id):
    ModelFormSet = modelformset_factory(models.StudyRecord, StudyRecordForm, extra=0)
    form_set_obj = ModelFormSet(queryset=models.StudyRecord.objects.filter(course_record_id=course_record_id))
    if request.method == 'POST':
        form_set_obj = ModelFormSet(queryset=models.StudyRecord.objects.filter(course_record_id=course_record_id),
                                    data=request.POST)
        if form_set_obj.is_valid():
            form_set_obj.save()
            # next = request.GET.get('next')
            # if next:
            #     return redirect(next)
            # return redirect(reverse('study_record', args=(course_record_id,)))
            return HttpResponse('保存成功')

    return render(request, 'teacher/study_record_list.html', {'form_set_obj': form_set_obj})
