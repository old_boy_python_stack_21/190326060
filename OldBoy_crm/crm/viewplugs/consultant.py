# 模型
from crm import models
from django.db import transaction
# 视图
from django.shortcuts import render, redirect, HttpResponse, reverse
from django.views import View
from .base import BaseView
# form组件
from crm.forms import CustomerForm, ConsultRecordForm, EnrollmentForm, PaymentList
# 分页器
from utils.pagination import Pagination
from django.conf import settings


# 客户展示
class Customer_list(BaseView):

    def get(self, request, *args, **kwargs):
        q = self.search(['qq', 'name', 'phone', 'consultant__name'])
        if request.path_info == reverse('customer_list'):
            customer_list = models.Customer.objects.filter(q, consultant__isnull=True)
            # 公户>>customer_list
        else:
            # 私户>>my_customer
            customer_list = models.Customer.objects.filter(q, consultant=request.user_obj)
        obj = Pagination(request.GET.get('page', 1), len(customer_list), request.GET.copy())
        return render(request, 'consultant/customer_list.html',
                      {"customer_list": customer_list[obj.page_start:obj.page_end],
                       'obj': obj})

    def post(self, request, *args, **kwargs):
        self.customer_list = request.POST.getlist('customer')
        action = request.POST.get('action')

        # 跳转页面保留搜索条件
        if not action:
            request.getcopy = request.GET.copy()
            request.getcopy['page'] = request.POST.get('page', 1)
            return redirect('{}?{}'.format(request.path, request.getcopy.urlencode()))
        if hasattr(self, action):
            ret = getattr(self, action)()
            if ret:
                return ret
        else:
            return HttpResponse('非法操作')
        return self.get(request, *args, **kwargs)

    def pub_convert_pri(self):
        if len(self.customer_list) + models.Customer.objects.filter(
                consultant=self.request.user_obj).count() > settings.PRIVATE_CUSTOMER_LIMIT:
            return HttpResponse('做人不能太贪心，给别人留一点。')
        try:
            with transaction.atomic():
                # 进行操作
                customers = models.Customer.objects.filter(pk__in=self.customer_list,
                                                           consultant=None).select_for_update()
                # print(customers, len(self.customer_list), self.customer_list, customers.count())
                if len(self.customer_list) != customers.count():
                    return HttpResponse('手速太慢，客户已经被别人抢走了！')
                customers.update(consultant=self.request.user_obj)
        except Exception as e:
            print(e)

        # 公户转私户
        # 方法二
        # self.request.user_obj.customers.add(*models.Customer.objects.filter(pk__in=pk))

    # 私户转公户
    def pri_convert_pub(self):
        models.Customer.objects.filter(pk__in=self.customer_list).update(consultant=None)

        # 方法二
        # self.request.user_obj.customers.remove(*models.Customer.objects.filter(pk__in=pk))


# 编辑或新增客户,
def customer_change(request, pk=None):
    """
    # add or edit customer
    # 编辑可以next,添加没有加
    :param request:
    :param pk:
    :return:
    """
    next = request.GET.get('next')

    obj = models.Customer.objects.filter(pk=pk).first()
    form_obj = CustomerForm(request, instance=obj)
    if request.method == 'POST':
        form_obj = CustomerForm(request, data=request.POST, instance=obj)
        if form_obj.is_valid():
            form_obj.save()
            if next:
                return redirect(next)
            return redirect(reverse('customer_list'))
    title = '编辑客户' if pk else '新增客户'
    return render(request, 'form.html', {'form_obj': form_obj, 'title': title})


# 跟进记录展示
class Consult_list(View):

    def get(self, request, customer_id=None):
        if not customer_id:
            # 展示当前销售所有客户的跟进记录
            consult_record = models.ConsultRecord.objects.filter(
                consultant=self.request.user_obj, delete_status=False).order_by(
                '-date')  # 获取客户对象
        else:
            consult_record = models.ConsultRecord.objects.filter(customer_id=customer_id,

                                                                 delete_status=False).order_by('-date')
        return render(request, 'consultant/consult_list.html',
                      {"consult_record": consult_record,
                       'customer_id': customer_id})

    def post(self, request):
        pass


# 编辑或新增跟进记录
def consult_change(request, pk=None, customer_id=None):
    next = request.GET.get('next')
    # add or edit customer
    obj = models.ConsultRecord.objects.filter(pk=pk).first()
    form_obj = ConsultRecordForm(request, customer_id, instance=obj)
    if request.method == 'POST':
        form_obj = ConsultRecordForm(request, customer_id, data=request.POST, instance=obj)
        if form_obj.is_valid():
            form_obj.save()
            if next:
                return redirect(next)
            return redirect(reverse('consult_list'))
    title = '编辑跟进记录' if pk else '新增跟进记录'
    return render(request, 'form.html', {'form_obj': form_obj, 'title': title, 'customer_id': customer_id})


# 报名表的展示
class EnrollmentList(View):
    def get(self, request, customer_id=None):
        if not customer_id:
            # 展示该销售的所有客户的报名记录
            all_enrollment = models.Enrollment.objects.filter(customer__in=self.request.user_obj.customers.all(),
                                                              delete_status=False)  # 获取客户对象
        else:
            all_enrollment = models.Enrollment.objects.filter(customer_id=customer_id, delete_status=False)
        return render(request, 'consultant/enrollment_list.html',
                      {"all_enrollment": all_enrollment, 'customer_id': customer_id})

    def post(self, request):
        pass


# 新增或编辑报名表
def enrollment_change(request, pk=None, customer_id=None):
    obj = models.Enrollment(customer_id=customer_id) if customer_id else models.Enrollment.objects.filter(pk=pk).first()
    # 前者是添加报名表,后者是编辑报名表
    form_obj = EnrollmentForm(request, instance=obj)
    if request.method == 'POST':
        form_obj = EnrollmentForm(request, data=request.POST, instance=obj)
        if form_obj.is_valid():
            form_obj.save()
            return redirect(reverse('enrollment_list'))

    title = '编辑报名表' if pk else '新增报名表'
    return render(request, 'form.html', {'form_obj': form_obj, 'title': title})


# 缴费记录表
class PaymentRecordList(View):
    def get(self, request, customer_id=None):
        if not customer_id:
            # 查看所有
            all_payment = models.PaymentRecord.objects.filter(delete_status=False)  # 获取客户对象
        else:
            all_payment = models.PaymentRecord.objects.filter(customer_id=customer_id, delete_status=False)
        return render(request, 'consultant/payment_list.html',
                      {"all_payment": all_payment})


# 新增或编辑缴费记录
def payment_change(request, pk=None, customer_id=None):
    if customer_id and customer_id != '0':
        obj = models.PaymentRecord(customer_id=customer_id)
    elif customer_id == '0':
        obj = None
    else:
        obj = models.PaymentRecord.objects.filter(pk=pk).first()
    form_obj = PaymentList(request, customer_id, instance=obj)
    if request.method == 'POST':
        form_obj = PaymentList(request, customer_id, data=request.POST, instance=obj)
        if form_obj.is_valid():
            form_obj.save()
            return redirect(reverse('payment_list'))

    title = '编辑缴费表' if pk else '新增缴费表'
    return render(request, 'form.html', {'form_obj': form_obj, 'title': title})
