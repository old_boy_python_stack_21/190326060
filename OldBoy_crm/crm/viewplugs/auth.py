from crm import models
from django.shortcuts import render, redirect
from crm.forms import RegForm
from utils.entrpe_md5 import get_md5


# 登出
def logout(request):
    request.session.delete()
    return redirect('login')


from rbac.service.init_permission import init_permission


# 登陆
def login(request):
    next_url = request.GET.get('next')
    error = ""
    if request.method == "POST":
        username = request.POST.get('username')
        password = request.POST.get('password')
        # 测试用密码root1234
        obj = models.UserProfile.objects.filter(
            username=username,
            password=get_md5(password),
            is_active=True).first()
        if obj:
            ret = redirect("customer_list")
            request.session['is_login'] = True
            request.session['user_id'] = obj.pk
            init_permission(request, obj)
            if next_url:
                return redirect(next_url)
            return redirect("index")
        else:
            error = "用户名或密码错误"
    return render(request, 'login.html', {"error": error})


# 注册
def reg(request):
    form_obj = RegForm()
    if request.method == 'POST':
        form_obj = RegForm(request.POST)
        # 对提交的数据进行校验
        if form_obj.is_valid():
            # 校验成功  把数据插入数据中
            # models.UserProfile.objects.create(**form_obj.cleaned_data)
            form_obj.save()
            return redirect('login')
        # print(form_obj.errors)
    return render(request, 'reg.html', {'form_obj': form_obj})


# 用户信息
def user_profile(request):
    request.user_obj.user_edit = True
    data = user_profile
    user_id = request.session['user_id']
    form_obj = RegForm(instance=request.user_obj)
    if request.method == 'POST':
        form_obj = RegForm(request.POST)
        # 对提交的数据进行校验
        if form_obj.is_valid():
            # 校验成功  把数据插入数据中
            # models.UserProfile.objects.create(**form_obj.cleaned_data)
            form_obj.save()
            return redirect('login')
        # print(form_obj.errors)
    return render(request, 'user_profile.html', {'user_profile': request.user_obj, 'form_obj': form_obj})
