from django.conf.urls import url
from . import views
from crm.viewplugs import auth, teacher, consultant

urlpatterns = [
    # # 用户相关,登陆,登出,注册
    url(r'^login/', auth.login, name="login"),
    url(r'^reg/', auth.reg, name="reg"),
    url(r'^logout/', auth.logout, name="logout"),
    url(r'^user_profile/', auth.user_profile, name="user_profile"),

    # 首页
    url(r'^index/', views.index, name="index"),
    #### 面向销售功能
    # 私户公户的展示
    url(r'^customer_list/', consultant.Customer_list.as_view(), name="customer_list"),
    url(r'^my_customer/', consultant.Customer_list.as_view(), name="my_customer"),

    # 客户更改>添加和更新客户
    url(r'^add_customer/', consultant.customer_change, name="add_customer"),
    url(r'^edit_customer/(\d+)/', consultant.customer_change, name="edit_customer"),

    # 跟进记录展示
    url(r'^consult_list/$', consultant.Consult_list.as_view(), name="consult_list"),  # 参看该销售的客户的跟进记录
    url(r'^consult_list/(?P<customer_id>\d+)/', consultant.Consult_list.as_view(), name="one_consult_list"),
    # 查看具体某一客户的跟进记录

    # 编辑跟进记录
    url(r'^add_consult/(?P<customer_id>\d+)/', consultant.consult_change, name="add_consult"),
    url(r'^edit_consult/(?P<pk>\d+)/$', consultant.consult_change, name="edit_consult"),  # 具体编辑某一条跟进记录

    # 展示报名记录
    # 展示一个销售填写报名记录
    url(r'^enrollment_list/$', consultant.EnrollmentList.as_view(), name='enrollment_list'),
    # 展示一个客户报名记录
    url(r'^enrollment_list/(?P<customer_id>\d+)/$', consultant.EnrollmentList.as_view(), name='one_enrollment_list'),

    # 添加报名表
    url(r'^add_enrollment/(?P<customer_id>\d+)/$', consultant.enrollment_change, name='add_enrollment'),
    # 编辑报名表
    url(r'^edit_enrollment/(\d+)/$', consultant.enrollment_change, name='edit_enrollment'),

    # 展示缴费记录
    # 展示一个销售的所有客户的缴费记录
    url(r'^payment_list/$', consultant.PaymentRecordList.as_view(), name='payment_list'),
    # 展示一个客户缴费记录
    url(r'^payment_list/(?P<customer_id>\d+)/$', consultant.PaymentRecordList.as_view(), name='one_payment_list'),

    # 添加缴费记录
    url(r'^add_payment/(?P<customer_id>\d+)/$', consultant.payment_change, name='add_payment'),
    # 编辑缴费记录
    url(r'^edit_payment/(\d+)/$', consultant.payment_change, name='edit_payment'),

    ###### 班级方面的功能

    # 展示班级列表
    url(r'^class_list/$', teacher.Classlist.as_view(), name='class_list'),

    # 添加班级
    url(r'^add_class/$', teacher.class_change, name='add_class'),
    # 编辑班级
    url(r'^edit_class/(\d+)/$', teacher.class_change, name='edit_class'),

    # 课程记录表
    # 展示班级的课程记录表
    url(r'^course_list/(?P<class_id>\d+)/$', teacher.Course_list.as_view(), name='course_list'),

    # 添加课程记录表
    url(r'^add_course_record/(?P<class_id>\d+)/$', teacher.course_record_change, name='add_course_record'),
    # 编辑课程记录表
    url(r'^edit_course_record/(?P<pk>\d+)/$', teacher.course_record_change, name='edit_course_record'),


    # 展示的学习记录
    url(r'^display_study_record/(?P<course_record_id>\d+)/$', teacher.study_record_list, name='display_study_record'),

]
