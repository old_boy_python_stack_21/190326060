from django.utils.deprecation import MiddlewareMixin
from django.shortcuts import reverse, redirect
from crm import models
from django.conf import settings
from re import match


class MyMiddleware(MiddlewareMixin):
    """
    在请求到来时,验证是否是登陆用户
    """

    def process_request(self, request):
        path = request.path_info

        # 白名单放过
        for pattern in settings.WHITE_LIST:
            if match(pattern, path):
                return  # 后面的代码不执行

        # 没登录跳转到登录页面

        # 上一个中间件已经写了没登陆跳转

        # 登录成功  保存登录的用户对象
        obj = models.UserProfile.objects.filter(pk=request.session.get('user_id')).first()
        if obj:
            request.user_obj = obj

    # def process_view(self, request, view_func, view_args, view_kwargs):
    #
    #     print('执行的函数是%s\n位置参数是%s\n关键字参数%s' % (view_func, view_args, view_kwargs))
    #     return
