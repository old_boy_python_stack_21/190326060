# -*- coding:utf-8 -*-

import os


def file_save(request,html_file_key,root_path=None):
    """

    :param request: 请求,必须是存在request.FILES
    :param html_file_key : 在html的上传文件input框的name值
    :return:
    """
    # 从请求的FILES中获取上传文件的文件名，file为页面上type=files类型input的name属性值
    try:
        filename = request.FILES[html_file_key].name
    except:
        return
    # 在项目目录下新建一个文件
    if not root_path:
        filename = os.path.join(root_path,filename)
    with open(filename, "wb") as f:
        # 从上传的文件对象中一点一点读
        for chunk in request.FILES["file"].chunks():
            # 写入本地文件
            f.write(chunk)
    return True