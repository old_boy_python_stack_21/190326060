from django.http.request import QueryDict


class Pagination:
    """
    分页器
    """

    def __init__(self, page, total_num, search_text=None, page_num=10, max_show=11):
        """
        :param page: 请求的页数
        :param total_num:所有的数据量
        :param page_num:每页显示的数据量
        :param max_show:最大展示的分页数
        """

        try:
            page = int(page)
            if page <= 0:
                page = 1
        except Exception:
            page = 1
        # 每页显示数据量
        page_start = (page - 1) * page_num
        page_end = page * page_num
        self.search_text = search_text
        if not self.search_text:
            self.search_text = QueryDict(mutable=True)
            # 函数可变类型的坑

        # 分页实现,所有数据的总页数
        a, b = divmod(total_num, page_num)
        total = a + 1 if b else a

        # 最大展示的分页数
        if total < max_show:
            li_start = 1
            li_end = total
        elif page + max_show // 2 >= total:
            li_end = total
            li_start = total - max_show
        elif page - max_show // 2 <= 1:
            li_start = 1
            li_end = li_start + max_show
        else:
            li_start = page - max_show // 2
            li_end = page + max_show // 2

        self.page_start = page_start
        self.page_end = page_end
        self.li_start = li_start
        self.li_end = li_end
        self.page = page
        self.total = total

    @property
    def page_html(self):
        """
        :return: 返回分页的html代码的字符串
        """
        page_li = []
        # 上一页
        if self.page == 1:
            page_li.append(
                '<li class="disabled"><a aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>')
        else:
            self.search_text['page'] = self.page - 1
            page_li.append(
                '<li><a href="?{}" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>'.format(
                    self.search_text.urlencode()))
        # 中间页面
        for page in range(self.li_start, self.li_end + 1):
            self.search_text['page'] = page
            if page == self.page:
                page_li.append(
                    '<li class="active"><a href="?{}">{}</a></li>'.format(self.search_text.urlencode(), page))
            else:
                page_li.append('<li><a href="?{}">{}</a></li>'.format(self.search_text.urlencode(), page))
        # 下一页页面
        print(self.page)
        if self.page == self.total:
            page_li.append('<li class="disabled"><a aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>')
        else:
            self.search_text['page'] = self.page + 1
            page_li.append(
                '<li><a href="?{}" aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>'.format(
                    self.search_text.urlencode()))
        return "".join(page_li)
