[TOC]



## 第十二章 Django框架

### 12.1 web 框架

1. 所有web应用本质就是一个socket的server端
2. 所有的web服务都遵循同一个标准--http协议,来收发文件
3. HTTP协议主要规定了客户端和服务器之间的通信格式

### 12.2 HTTP协议

1. 超文本传输协议--http协议,是一种用于分布式、协作式和超媒体信息系统的应用层协议。HTTP是万维网的数据通信的基础。HTTP有很多应用，但最著名的是用于web浏览器和web服务器之间的双工通信。(广泛使用的版本是http1.1)
2. HTTP是一个客户端终端（用户）和服务器端（网站）请求和应答的标准（TCP）。默认端口为80时自动隐藏端口号.
3. 通常，由HTTP客户端发起一个请求，创建一个到服务器指定端口（默认是80端口）的TCP连接。HTTP服务器则在那个端口监听客户端的请求。一旦收到请求，服务器会向客户端返回一个状态，比如"HTTP/1.1 200 OK"，以及返回的内容，如请求的文件、错误消息、或者其它信息。

#### 12.2.1 http 工作原理

HTTP协议定义Web客户端如何从Web服务器请求Web页面，以及服务器如何把Web页面传送给客户端。HTTP协议采用了请求/响应模型。客户端向服务器发送一个请求报文，请求报文包含请求的方法、URL、协议版本、请求头部和请求数据。服务器以一个状态行作为响应，响应的内容包括协议的版本、成功或者错误代码、服务器信息、响应头部和响应数据。

http响应请求的步骤

1. 客户端连接到Web服务器
   一个HTTP客户端，通常是浏览器，与Web服务器的HTTP端口（默认为80）建立一个TCP套接字连接。例如，http://www.luffycity.com。
2. 发送HTTP请求
   通过TCP套接字，客户端向Web服务器发送一个文本的请求报文，一个请求报文由请求行、请求头部、空行和请求数据4部分组成。
3. 服务器接受请求并返回HTTP响应
   Web服务器解析请求，定位请求资源。服务器将资源复本写到TCP套接字，由客户端读取。一个响应由状态行、响应头部、空行和响应数据4部分组成。
4. 释放连接TCP连接
   若connection 模式为close，则服务器主动关闭TCP连接，客户端被动关闭连接，释放TCP连接;若connection 模式为keepalive，则该连接会保持一段时间，在该时间内可以继续接收请求;
5. 客户端浏览器解析HTML内容
   客户端浏览器首先解析状态行，查看表明请求是否成功的状态代码。然后解析每一个响应头，响应头告知以下为若干字节的HTML文档和文档的字符集。客户端浏览器读取响应数据HTML，根据HTML的语法对其进行格式化，并在浏览器窗口中显示。

```python
在浏览器地址栏键入URL，按下回车之后会经历以下流程：

浏览器向 DNS 服务器请求解析该 URL 中的域名所对应的 IP 地址;
解析出 IP 地址后，根据该 IP 地址和默认端口 80，和服务器建立TCP连接;
浏览器发出读取文件(URL 中域名后面部分对应的文件)的HTTP 请求，该请求报文作为 TCP 三次握手的第三个报文的数据发送给服务器;
服务器对浏览器请求作出响应，并把对应的 html 文本发送给浏览器;
释放 TCP连接;
浏览器将该 html 文本并显示内容; 
```

#### 12.2.2 http请求的方法

1. 主要有八种
   1. get --> 获取一个页面、图片（资源）-->请求没有请求数据
   2. post -->提交数据
   3. head 
   4. put 
   5. delete 
   6. options 
   7. trace 
   8. connect

#### 12.2.3http状态码

所有http的响应的第一行都是状态行，依次是当前HTTP版本号，3位数字组成的状态代码，以及描述状态的短语，彼此由空格分隔。

状态代码的第一个数字代表当前响应的类型：

1. 1xx消息——请求已被服务器接收，继续处理
2. 2xx成功——请求已成功被服务器接收、理解、并接受(200 ok 成功)
3. 3xx重定向——需要后续操作才能完成这一请求(重定向)
4. 4xx请求错误——请求含有词法错误或者无法被执行(404 403 402)
5. 5xx服务器错误——服务器在处理某个正确请求时发生错误

> **注意 : **RFC 2616 中已经推荐了描述状态的短语，例如"200 OK"，"404 Not Found"，但是WEB开发者仍然能够自行决定采用何种短语，用以显示本地化的状态描述或者自定义信息。 

#### 12.2.4 url 

超文本传输协议（HTTP）**统一资源定位符**将从因特网获取信息的五个基本元素包括在一个简单的地址中：

1. 传送协议。

2. 层级URL标记符号(为[//],固定不变)

3. 访问资源需要的凭证信息（可省略）

4. 服务器。（通常为域名，有时为IP地址）

5. 端口号。（以数字方式表示，若为HTTP的默认值“:80”可省略）

6. 路径。（以“/”字符区别路径中的每一个目录名称）

7. 查询。（GET模式的窗体参数，以“?”字符为起点，每个参数以“&”隔开，再以“=”分开参数名称与数据，通常以UTF8的URL编码，避开字符冲突的问题）

8. 片段。以“#”字符为起点

   ```python
   以 http://www.luffycity.com:80/news/index.html?id=250&page=1为例
   1. http--.表明协议http(https加密)http(80),https(443)
   2. www.luffycity.com --服务器
   3. :80 --服务器上的网络端口号；80的时候默认隐藏
       “80”是超文本传输协议文件的常用端口号，因此一般也不必写明。一般来说用户只要键入统一资源定位符	的一部分（www.luffycity.com/news/index.html?id=250&page=1）就可以了。
   4. /news/index.html 是路径；
   5. ?id=250&page=1，是查询。
   ```

#### 12.2.5 http 的请求格式 和 响应 格式

![1560219121217](D:\md_down_png\1560219121217.png)

浏览器向服务端请求数据,get请求没有请求数据

```python
1. 请求requset

"(请求方式)get (url路径)/ (协议版本)HTTP/1.1\r\n
k1:v1\r\n
k2:v2\r\n
k3:v3\r\n
\r\n
请求数据(请求体)"  -->get没有请求数据

2.响应(response)

服务器发送给 浏览器

"(协议版本)HTTP/1.1 状态码 状态码描述\r\n
k1:v1\r\n
k2:v2\r\n
k3:v3\r\n
\r\n
响应的数据(响应体response)"  -->html文本
```

![1560243848099](D:\md_down_png\1560243848099.png)

##### 12.2.5.1 浏览器发送请求和接受响应的过程

1. 在浏览器上的地址栏种输入URL，回车，发送get请求；
2. 服务器接受到请求,获取URL路径,根据路径做不同的操作,把返回的数据封装到响应体中,发送给浏览器.
3. 浏览器接受到响应,双方断开连接
4. 浏览器从响应体获取数据,进行解析渲染;

#### 12.2.6  web 框架的功能

1. **本质是socket 服务端**

2. 对于真实开发中的python web程序来说，一般会分为两部分：服务器程序和应用程序。

   服务器程序负责对socket服务端进行封装，并在请求到来时，对请求的各种数据进行整理。

   应用程序则负责具体的逻辑处理。为了方便应用程序的开发，就出现了众多的Web框架，例如：Django、Flask、web.py 等。不同的框架有不同的开发方式，但是无论如何，开发出的应用程序都要和服务器程序配合，才能为用户提供服务。

3. **WSGI协议**（Web Server Gateway Interface）就是一种规范，它定义了使用Python编写的web应用程序与web服务器程序之间的接口格式，实现web应用程序与web服务器程序间的解耦。

   常用的WSGI服务器有uWSGI、Gunicorn。而Python标准库提供的独立WSGI服务器叫wsgiref，Django开发环境用的就是这个模块来做服务器。

4. **主要功能有**

   1. socket收发消息    - wsgiref 测试用  uwsgi 上线用

   2. 根据不同的路径返回不同的内容

   3. 返回动态页面（字符串的替换） —— jinja2

   > django 2 3   /  flask   2   /tornado 1  2   3

   4. 我们一般用django框架(功能很多很全),其他都是轻量化的,需要引入其他模块

5. wsgiref模块(起socket服务端)的使用

   ```python
   from wsgiref.simple_server import make_server   
   # 导入模块
        
   # 将返回不同的内容部分封装成函数   
   def index(url):   
       # 读取index.html页面的内容   
       with open("index.html", "r", encoding="utf8") as f:   
           s = f.read()   
       # 返回字节数据   
       return bytes(s, encoding="utf8")   
        
        
   def home(url):   
       with open("home.html", "r", encoding="utf8") as f:   
           s = f.read()   
       return bytes(s, encoding="utf8") 
   	#return s.encode('utf-8')
        
        
   def timer(url):   
       import time   
       with open("time.html", "r", encoding="utf8") as f:   
           s = f.read()   
           s = s.replace('@@time@@', time.strftime("%Y-%m-%d %H:%M:%S"))   
       return bytes(s, encoding="utf8")   
        
        
   # 定义一个url和实际要执行的函数的对应关系   
   list1 = [   
       ("/index/", index),   
       ("/home/", home),   
       ("/time/", timer),   
   ]   
        
        
   def run_server(environ, start_response):   
   #注意>>>    
   	start_response('200 OK', [('Content-Type', 'text/html;charset=utf8'), ])  # 设置HTTP响应的状态码和头信息   
   #注意>>>    
   	url = environ['PATH_INFO']  # 取到用户输入的url   
       func = None   
       for i in list1:   
           if i[0] == url:   
               func = i[1]   
               break   
       if func:   
           response = func(url)   
       else:   
           response = b"404 not found!"   
       return [response, ]   
        
        
   if __name__ == '__main__':   
       httpd = make_server('127.0.0.1', 8090, run_server)   #注意这个
       print("我在8090等你哦...")   
       httpd.serve_forever()  
   ```

6. jinja2模块的使用(模板渲染工具,模板就是html)

   ```python
   from wsgiref.simple_server import make_server  
   from jinja2 import Template  
     
     
   def index(url):  
       # 读取HTML文件内容  
       with open("index2.html", "r", encoding="utf8") as f:  
           data = f.read()  
           template = Template(data)   # 生成模板文件  
           ret = template.render({'name': 'alex', 'hobby_list': ['抽烟', '喝酒', '烫头']})   # 把数据填充到模板中  
       return bytes(ret, encoding="utf8")  
     
     
   def home(url):  
       with open("home.html", "r", encoding="utf8") as f:  
           s = f.read()  
       return bytes(s, encoding="utf8")  
     
     
   # 定义一个url和实际要执行的函数的对应关系  
   list1 = [  
       ("/index/", index),  
       ("/home/", home),  
   ]  
     
     
   def run_server(environ, start_response):  
       start_response('200 OK', [('Content-Type', 'text/html;charset=utf8'), ])  # 设置HTTP响应的状态码和头信息  
       url = environ['PATH_INFO']  # 取到用户输入的url  
       func = None  
       for i in list1:  
           if i[0] == url:  
               func = i[1]  
               break  
       if func:  
           response = func(url)  
       else:  
           response = b"404 not found!"  
       return [response, ]  
     
     
   if __name__ == '__main__':  
       httpd = make_server('127.0.0.1', 8090, run_server)  
       print("我在8090等你哦...")  
       httpd.serve_forever() 
   ```

   ```html
   <!DOCTYPE html>
   <html lang="zh-CN">
   <head>
     <meta charset="UTF-8">
     <meta http-equiv="x-ua-compatible" content="IE=edge">
     <meta name="viewport" content="width=device-width, initial-scale=1">
     <title>Title</title>
   </head>
   <body>
       <h1>姓名：{{name}}</h1>
       <h1>爱好：</h1>
       <ul>
           {% for hobby in hobby_list %}
           <li>{{hobby}}</li>
           {% endfor %}
       </ul>
   </body>
   </html
   ```

   ```python
   # 使用mysql来获取数据
   conn = pymysql.connect(host="127.0.0.1", port=3306, user="root", passwd="xxx", db="xxx", charset="utf8")
   cursor = conn.cursor(cursor=pymysql.cursors.DictCursor)
   cursor.execute("select name, age, department_id from userinfo")
   user_list = cursor.fetchall()
   cursor.close()
   conn.close()
   ```

### 12.3  django 框架 的使用(重点)

#### 12.3.1 配置

1. 下载安装

   ```py
   1. https://www.djangoproject.com/download/  官网下载
   2. pip3 install django==1.11.21  //pip包管理器安装//注意LTS意指长期支持的版本
   3. 通过pycharm调用pip安装
   	setting ——》 解释器 ——》 点+号 ——》 输入Django ——》 选择版本 ——》 下载安装
   4. pip install django==1.11.21 -i 源
   ```

2. 创建一个django项目

   ```python
   1. 命令行创建,打开控制台,创建了一个mysite的django项目,目录结构如下
   django-admin startproject 项目名称
   ```

   ![1560226751493](D:\md_down_png\1560226751493.png)

   > **注意:** Django 启动时报错 UnicodeEncodeError ...
   >
   > 报这个错误通常是因为计算机名为中文，改成英文的计算机名重启下电脑就可以了。

   ```python
   2. pycharmIDE创建django项目
   ```

   ​	![1560227015628](D:\md_down_png\1560227015628.png)

3. 运行django项目

   `1.一路cd到项目的根目录,manger.py文件目录下`

   ​		`python3 manager.py runserver 127.0.0.1:8080`

   `2. pycharm运行`

4. 模板文件的配置setting.py

   ```python
   TEMPLATES = [
       {
           'BACKEND': 'django.template.backends.django.DjangoTemplates',
           'DIRS': [os.path.join(BASE_DIR, "template")],  # template文件夹位置
           'APP_DIRS': True,
           'OPTIONS': {
               'context_processors': [
                   'django.template.context_processors.debug',
                   'django.template.context_processors.request',
                   'django.contrib.auth.context_processors.auth',
                   'django.contrib.messages.context_processors.messages',
               ],
           },
       },
   ]
   
   #静态文件配置
   STATIC_URL = '/static/'  # HTML中使用的静态文件夹前缀,别名
   STATICFILES_DIRS = [
       os.path.join(BASE_DIR, "static"),
       os.path.join(BASE_DIR, 'static'),
       os.path.join(BASE_DIR, 'static2'),     # 静态文件存放位置
   ]
   ```
   
   静态文件配置图解

   ```html
   <link rel="stylesheet" href="/static/css/login.css">   # 别名开头
   按照STATICFILES_DIRS列表的顺序进行查找。
   ```
   
   
   
   ![1560254220109](D:\md_down_png\1560254220109.png)
   
   > **注意:**若用pycharm创建时,要注意模板文件配置

##### 12.3.1.2 简单登录实例

form表单提交数据注意的问题：

1. 提交的地址action=""  请求的方式 method="post"
2. 所有的input框有name属性
3. 有一个input框的type="submit" 或者 有一个button
4. **刚开始学习时可在配置文件中暂时禁用csrf中间件，方便表单提交测试。**提交POST请求，把settings中MIDDLEWARE的'django.middleware.csrf.CsrfViewMiddleware'注释掉, 注释掉之后就可以提交POST请求

#### 12.3.2 django 基础三件套

1. `from django.shortcuts import HttpResponse, render, redirect`

2. `HttpResponse`内部传入一个字符串参数，返回给浏览器。

   ````python
   def index(request):
       # 业务逻辑代码
       return HttpResponse("OK")
   ````

3. `render`除request参数外还接受一个待渲染的模板文件和一个保存具体数据的字典参数。

   将数据填充进模板文件，最后把结果返回给浏览器。（类似于我们上面用到的jinja2）

   ```python
   def index(request):
       # 业务逻辑代码
       return render(request, "index.html", {"name": "alex", "hobby": ["烫头", "泡吧"]})
   ```

4. `redirect`接受一个URL参数，表示跳转到指定的URL。

   ```python
   def index(request):
       # 业务逻辑代码
       return redirect("/home/")
      #return redirect("https://www.baidu.com/")
   # 本质  Location：地址
   ```
#### 12.3.3 django的MVC 和 MTV 框3

MVC ,全名是Model View Controller，是软件工程中的一种软件架构模式，把软件系统分为三个基本部分：模型(Model)、视图(View)和控制器(Controller)，具有耦合性低、重用性高、生命周期成本低等优点。

M: model  模型

V：view   视图   - HTML 

C: controller  控制器   ——路由 传递指令  业务逻辑

Django框架的设计模式借鉴了MVC框架的思想，也是分成三部分，来降低各个部分之间的耦合性。

Django框架的不同之处在于它拆分的三部分为：Model（模型）、Template（模板）和View（视图），也就是MTV框架。

MTV：

M： model  模型 ORM,负责业务对象与数据库的对象(ORM)

T:    tempalte  模板  - HTML,负责如何把页面展示给用户

V：view   业务逻辑,负责业务逻辑，并在适当的时候调用Model和Template

#### 12.3.4 django 的 app

1. 一个Django项目可以分为很多个APP，用来隔离不同功能模块的代码。

2. 创建app

   ```python
   1. 命令行创建
   python manage.py startapp app01
   2.pycharm创建
   tools  ——》  run manage.py task  ——》  startapp  app名称
   ```

   ![1560327250145](D:\md_down_png\1560327250145.png)

   ![1560310077499](D:\md_down_png\1560310077499.png)

   然后注册app,需要在settings.py中注册

   ```python
   INSTALLED_APPS = [
   	...
       'app01',
       'app01.apps.App01Config',  # 推荐写法
   ]
   ```

#### 12.3.5 django  的 urls.py

写url路径和函数的对应关系

```python
from django.conf.urls import url
from app01 import views

urlpatterns = [
    url(r'^index/', views.index),
    url(r'^login/', views.login),
    url(r'^orm_test/', views.orm_test),
]
```

#### 12.3.6 get 和post

get  获取到一个资源

发get的途径：

1.在浏览器的地址栏种输入URL，回车

2.a标签

3.form表单   不指定 method    method=‘get’

传递参数     url路径?id=1&name=alex

django种获取url上的参数    request.GET   {'id':1,'name':'alex'}   request.GET.get(key)     request.GET[key]

post   提交数据.

form表单   method=‘post’   

参数不暴露在URL    在请求体中  

django中获取post的数据  request.POST 

### 12.4  django 的views系统

#### 12.4.1 CBV和FBV

FBV   function based  view    

CBV   class based  biew

定义CBV：

```python
from django.views import View

class AddPublisher(View):
    
    def get(self,request):
        """处理get请求"""
        return response
    
    
    def post(self,request):
        """处理post请求"""
        return response
```

  使用CBV：

```python
form app01 import views

url(r'^add_publisher/', views.AddPublisher.as_view())
```

#### 12.4.2 as_view的流程

1.项目启动 加载ur.py时，执行类.as_view()    ——》  view函数

​			url(r'add_publisher/',views.AddPublisher.as_view())

​			url(r'add_publisher/', view )

2.请求到来的时候执行view函数：

1. 实例化类AddPublisher  ——》 self 

   ​	self.request = request 

2. 执行seld.dispatch(request, *args, **kwargs)

   1. 判断请求方式是否被允许：http_method_names  = []
   2. 允许    通过反射获取到对应请求方式的方法   ——》 handler
   3. 不允许   self.http_method_not_allowed  ——》handler
   4. 执行handler（request，*args,**kwargs）

    返回响应   —— 》 浏览器

#### 12.4.3.视图加装饰器

 FBV  直接加装饰器

```python
def timer(func):
    def inner(request, *args, **kwargs):
        start = time.time()
        ret = func(request, *args, **kwargs)
        print("函数执行的时间是{}".format(time.time() - start))
        return ret

    return inner



@timer
def publisher_list(request):
    # 从数据库中查询到出版社的信息
    all_publishers = models.Publisher.objects.all().order_by('pk')
    print(all_publishers.values())
    # 返回一个包含出版社信息的页面
    return render(request, 'publisher_list.html', {'all_publishers': all_publishers})
```

CBV 

```python
from django.utils.decorators import method_decorator #方法装饰器
```

1.加在方法上

```python
@method_decorator(timer)
def get(self, request, *args, **kwargs):
    """处理get请求"""
```

2.加在dispatch方法上

```python
@method_decorator(timer)
def dispatch(self, request, *args, **kwargs):
    # print('before')
    ret = super().dispatch(request, *args, **kwargs)
    # print('after')
    return ret


@method_decorator(timer,name='dispatch')
class AddPublisher(View):

```

3.加在类上

```python
@method_decorator(timer,name='post')
@method_decorator(timer,name='get')
class AddPublisher(View):
```

区别：

不适用method_decorator

func   ——》 <function AddPublisher.get at 0x000001FC8C358598>

args  ——》 (<app01.views.AddPublisher object at 0x000001FC8C432C50>, <WSGIRequest: GET '/add_publisher/'>)



使用method_decorator之后：

func ——》 <function method_decorator.<locals>._dec.<locals>._wrapper.<locals>.bound_func at 0x0000015185F7C0D0>

args ——》 (<WSGIRequest: GET '/add_publisher/'>,)

#### 12.4.4 request对象

```python
# 属性
request.methot   请求方式  GET
request.GET    url上携带的参数
request.POST   POST请求提交的数据
request.path_info   URL的路径    不包含ip和端口  不包含参数
request.body    请求体  b'',字节类型
request.FILES   上传的文件
	1.enctype = "multipart/form_data"
	2.f1.chunks()
request.META    请求头 全大写    HTTP_      -  ——》 _

request.COOKIES  cookie
request.session	 session

# 方法
request.get_host()  获取主机的ip和端口
request.get_full_path()   URL的路径   不包含ip和端口 包含参数
request.is_ajax()    判断是否是ajax请求
```

#### 12.4.5 response对象

```python
from django.shortcuts import render, redirect, HttpResponse

HttpResponse('字符串')    ——》  ’字符创‘
render(request,'模板的文件名',{k1:v1})   ——》 返回一个完整的TML页面
redirect('重定向的地址')    ——》 重定向   Location ： 地址

from django.http.response import JsonResponse
J
sonResponse({})  , content-type ='application/json' 

JsonResponse([],safe=False)    -->非字典
```

写url对应的函数,功能

```python
def login(request):
    
request.method   ——》 请求方式 GET  POST 
request.POST     ——》 form表单提交POST请求的数据  {}  request.POST['xxx'] request.POST.get('xxx',)

返回值
from django.shortcuts import HttpResponse, render, redirect
HttpResponse   —— 》 字符串 
render(request,'模板的文件名')  ——》 返回一个HTML页面 
redirect('重定向的地址')   ——》 重定向  /  响应头  Location：‘地址’  
```

> **注意:**get请求--->获取到一个页面,提交的数据暴露在URL上的,传递参数 <http://127.0.0.1:8000/index/?id=2&name=alex> ,获取数据  request.GET
>
> post请求--->提交数据,数据隐藏在请求体,获取数据  request.POST



###  12.5 django 的 模型系统ORM

#### 12.5.1 简介

对象关系映射（Object Relational Mapping，简称ORM）模式是一种为了解决面向对象与关系数据库存在的互不匹配的现象的技术。ORM是通过使用描述对象和数据库之间映射的元数据，将程序中的对象自动持久化到关系数据库中。django辅助我们写数据库的

1. 它通常将一个类和一张表一一对应，类的每个实例对应表中的一条记录，类的每个属性对应表中的每个字段。 

   ORM提供了对数据库的映射，不用直接编写SQL代码，只需操作对象就能对数据库操作数据。

   让软件开发人员专注于业务逻辑的处理，提高了开发效率。

2. ORM的缺点是会在一定程度上牺牲程序的执行效率。

   ORM的操作是有限的，也就是ORM定义好的操作是可以完成的，一些复杂的查询操作是完成不了。

   ORM用多了SQL语句就不会写了，关系数据库相关技能退化...

#### 12.5.2 django使用ORM的流程

创建一个MySQL数据库。在控制台创建,django无法帮助我们创建库.

1. 在mysql数据库内创建一个库.ORM不能创库

2. 在settings中配置，Django连接MySQL数据库：

   ```python
   DATABASES = {
       'default': {
           'ENGINE': 'django.db.backends.mysql',    # 引擎	
           'NAME': 'day53',						# 数据库名称
           'HOST': '127.0.0.1',					# ip地址
           'PORT':3306,							# 端口
           'USER':'root',							# 用户
           'PASSWORD':'123'						# 密码
           
       }
   }
   ```

3. 在与settings同级目录下的 init文件中写：

   ```python
   #告诉django使用pymysql连接数据库
   
   import pymysql
   pymysql.install_as_MySQLdb()
   #使用pymysql模块连接mysql数据库
   ```

4. 创建表,在app下的models.py中写类(必须注册了app)

   ```python
   from django.db import models
   
   class Book(models.Model):
       name = models.CharField(max_length=32)
       pub = models.ForeignKey('Publisher',on_delete=models.CASECADE) # -->外建,默认以id为外键
   	
   class User(models.Model):
       username = models.CharField(max_length=32)   # username varchar(32)
       password = models.CharField(max_length=32)   # username varchar(32)
       
      
   ```

   多对多关系的创建

   ![1560737933527](D:\md_down_png\1560737933527.png)

5. 执行数据库迁移的命令

   ```python
   在终端迁移数据库
   
   python manage.py makemigrations   #  检测每个注册app下的model.py   记录model的变更记录
   
   python manage.py migrate   #  同步变更记录到数据库中
   ```

#### 12.5.3 orm操作

```python
# 获取表中所有的数据
ret = models.User.objects.all()  # QuerySet 对象列表  【对象】
# 获取一个对象（有且唯一）
obj = models.User.objects.get(username='alex')   # 获取不到或者获取到多个对象会报错
# 获取满足条件的对象
ret = models.User.objects.filter(username='alex1',password='dasb')  # QuerySet 对象列表
```

7. orm的对应关系

   类     ——》   表

   对象  ——》  数据行（记录）

   属性  ——》  字段

8. orm的操作:

   1. 操作表

   2. 操作数据行(查)

      ```python
      from app01 import models 
      
      models.User.objects.all()  # ——》  queryset  对象列表# 查找所有的数据
      obj = models.User.objects.get(name='alex')  # 没有或者多个 就报错,# 查找一条数据
      obj.name   ——》 'alex'
      models.User.objects.filter(name='alex')  # queryset  对象列表# 查询满足条件的所有数据
      
      book_obj.pub  #  ——》 出版社对象 ,外键获取到的是对象
      book_obj.pub.pk 
      book_obj.pub_id
      ```
      
   3. 增删改
   
      ```python
      
      ### 新增
      
      obj = models.Publisher.objects.create(name=publisher_name
      
      obj = models.Publisher(name='xxx')  #——》 存在在内存中的对象
      obj.save()      # ——》   提交到数据库中  新增
                                            
      author_obj = models.Author.objects.create(name=author_name) # 只插入book表中的内容
      author_obj.books.set(books)  # 设置作者和书籍多对多的关系,这里的books是一个列表
         
      ### 删除
         
      
      obj_list = models.Publisher.objects.filter(pk=pk)
      obj_list.delete()
         
      obj = models.Publisher.objects.get(pk=pk)
      obj.delete()
      
      ### 编辑
      # 修改数据
      book_obj.name='xxxxx'
      book_obj.pub = 出版社对象
      book_obj.pub_id = 出版社的id
      book_obj.save()   # ——》 保存到数据库中
      
                                            
                                            
      ### 多对多的修改                                     
      books = request.POST.getlist('books')
      
      # 修改对象的数据
      author_obj.name = name
      author_obj.save()
      # 多对多的关系
      author_obj.books.set(books)  #  每次重新设置
      ```

#### 12.5.4  设置多对多关系的3方式

1.django帮我们生成第三张表

```
class Author(models.Model):
    name = models.CharField(max_length=32)
    books = models.ManyToManyField('Book')  # 不在Author表中生产字段，生产第三张表
```

2.自己创建第三张表

```python
class AuthorBook(models.Model):
    author = models.ForeignKey(Author, on_delete=models.CASCADE)
    book = models.ForeignKey(Book, on_delete=models.CASCADE)
    date = models.DateField()
```

3.自建的表和 ManyToManyField 联合使用

```python
class Author(models.Model):
    name = models.CharField(max_length=32)
    books = models.ManyToManyField('Book',through='AuthorBook')  
    # 不在Author表中生产字段，生产第三张表


class AuthorBook(models.Model):
    author = models.ForeignKey(Author, on_delete=models.CASCADE)
    book = models.ForeignKey(Book, on_delete=models.CASCADE)
    date = models.DateField()
```

#### 12.5.5 ORM的字段和参数

```python
(Big)AutoField    # 主键,自增的整形字段，必填参数primary_key=True，则成为数据库的主键。
			# 无该字段时，django自动创建,一个model(类/表单)不能有两个AutoField字段。
    		# 加Big,自增列，必须填入参数 primary_key=True,则是大范围,类似的有
IntegerField # 整数,数值的范围是 -2147483648 ~ 2147483647。
			# Small --->小整数 -32768 ～ 32767-32768 ～ 32767
    		# PositiveSmall --->正小整数 0 ～ 32767
        	# Big --->长整型(有符号的) -9223372036854775808 ～ 9223372036854775807
CharField   -->varchar # 字符串,必须提供max_length参数。max_length表示字符的长度。对应varchar类型 -->
BooleanField  布尔值 -->布尔值类型
			# gender = models.BooleanField('性别', choices=((0, '女'), (1, '男')))
			# NullBooleanField(Field) -->可以为空的布尔值
TextField    文本
DateTimeField DateField  日期时间
# 日期类型，日期格式为YYYY-MM-DD，相当于Python中的datetime.date的实例。
# 日期时间字段，格式为YYYY-MM-DD HH:MM[:ss[.uuuuuu]][TZ]，相当于Python中的datetime.datetime的实例。
    auto_now_add=True    # 新增数据的时候会自动保存当前的时间
    auto_now=True        # 新增、修改数据的时候会自动保存当前的时间
    参数 :auto_now：每次修改时修改为当前日期时间。
          auto_now_add：新创建对象时自动添加当前日期时间。
          auto_now和auto_now_add和default参数是互斥的，不能同时设置
DecimalField   十进制的小数
	max_digits       小数总长度   5
    decimal_places   小数位长度   2
FloatField(Field) --> 浮点型
```

##### 1. 自定义字段 char

```python
class MyCharField(models.Field):
    """
    自定义的char类型的字段类
    """
    def __init__(self, max_length, *args, **kwargs):
        self.max_length = max_length
        super(MyCharField, self).__init__(max_length=max_length, *args, **kwargs)
 
    def db_type(self, connection):
        """
        限定生成数据库表的字段类型为char，长度为max_length指定的值
        """
        return 'char(%s)' % self.max_length
    	# char(32)

# 使用
Class Class(models.Model):
    id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=25)
    # 使用自定义的char类型的字段
    cname = MyCharField(max_length=25)
    
# --------------------分割线----------------------------# 
class UnsignedIntegerField(models.IntegerField):
    def db_type(self, connection):
        return 'integer UNSIGNED'
# PS: 返回值为字段在数据库中的属性。
# Django字段与数据库字段类型对应关系如下：
    'AutoField': 'integer AUTO_INCREMENT',
    'BigAutoField': 'bigint AUTO_INCREMENT',
    'BinaryField': 'longblob',
    'BooleanField': 'bool',
    'CharField': 'varchar(%(max_length)s)',
    'CommaSeparatedIntegerField': 'varchar(%(max_length)s)',
    'DateField': 'date',
    'DateTimeField': 'datetime',
    'DecimalField': 'numeric(%(max_digits)s, %(decimal_places)s)',
    'DurationField': 'bigint',
    'FileField': 'varchar(%(max_length)s)',
    'FilePathField': 'varchar(%(max_length)s)',
    'FloatField': 'double precision',
    'IntegerField': 'integer',
    'BigIntegerField': 'bigint',
    'IPAddressField': 'char(15)',
    'GenericIPAddressField': 'char(39)',
    'NullBooleanField': 'bool',
    'OneToOneField': 'integer',
    'PositiveIntegerField': 'integer UNSIGNED',
    'PositiveSmallIntegerField': 'smallint UNSIGNED',
    'SlugField': 'varchar(%(max_length)s)',
    'SmallIntegerField': 'smallint',
    'TextField': 'longtext',
    'TimeField': 'time',
    'UUIDField': 'char(32)',
```

##### 2. 字段的参数

```python
null                #  数据库中字段是否可以为空
blank=True          #  form表单填写时可以为空
db_column           #  数据库中字段的列名
default             #  字段的默认值
db_index            #  数据库中字段是否可以建立索引
unique=True              #  唯一
choices=((0, '女'), (1, '男'))    #   可填写的内容和提示
	gender = models.BooleanField('性别', choices=((0, '女'), (1, '男')))
verbosename   显示中文
null=True         数据库中该字段可以为null
blank=True      填写form表单时该字段可以为空

```

##### 3. 表的参数 Class Meta

```python
class Meta:
    db_table = "person"  # 表名
    
	# admin中显示的表名称
    verbose_name = '个人信息'
    
	# verbose_name加s
    verbose_name_plural = '个人信息'
    
	# 联合索引 
    index_together = [
        ("name", "age"),  # 应为两个存在的字段
    ]
    # 联合唯一索引
    unique_together = (("name", "age"),)   # 应为两个存在的字段
```

#### 12.5.6 ORM查询13条必备

```python
返回Queryset,对象列表
1.ret = models.Person.objects.all() #  -->all()   获取所有的数据   ——》 QuerySet   对象列表
2..filter() # -->获取满足条件的所有数据   ——》 QuerySet   对象列表
3..exclude(pk=1) # -->exclude()    获取不满足条件的所有数据   ——》 QuerySet   对象列表
4...values('pid','name') # -->values() 拿到对象所有的字段和字段的值    QuerySet  [{},{}]字典
					   # -->values('字段') 拿到对象指定的字段和字段的值    QuerySet  [{} ,{}]字典
5..values_list('name','pid') # -->values_list()     拿到对象所有的字段的值 QuerySet  [ () ,() ],元组
						# -->values_list("字段")  拿到对象所有的字段的值 QuerySet  [ () ,() ],元	
6..order_by('age','-pid') # -->先根据字段排序,在排序,排- 降序    ——》 QuerySet  [ () ,() ]
7..reverse() #--># 反向排序   只能对已经排序的QuerySet进行反转
8..distinct() #-->distinct 去重   不支持按字段去重   想去重 数据必须完全一致

返回对象
9..get() # -->获取满足条件的一个数据   ——》 对象,获取不到或大于1个报错
10.first() #-->first  取第一元素   没有元素 None
11..last()  取最后一元素   没有元素 None

返回数字
10..count()  # -->count()  计数  <-->len

返回布尔值
13..exists() #-->True/False
```

#### 12.5.7 双下划线

```python
# 大于 小于
ret = models.Person.objects.filter(pk__gt=1)   # gt  greater than   大于
ret = models.Person.objects.filter(pk__lt=3)   # lt  less than   小于
ret = models.Person.objects.filter(pk__gte=1)   # gte  greater than   equal    大于等于
ret = models.Person.objects.filter(pk__lte=3)   # lte  less than  equal  小于等于

# in 和 range
ret = models.Person.objects.filter(pk__range=[2,3])   # range  范围,范围bettwen and
ret = models.Person.objects.filter(pk__in=[1,3,10,100])   # in  成员判断
ret = models.Person.objects.exclude(pk__in=[1,3,10,100])  # not in

#contains
ret = models.Person.objects.filter(name__contains='A')    #包含
ret = models.Person.objects.filter(name__icontains='A')   # 忽略大小写

#其他 startswith，istartswith, endswith, iendswith,
ret = models.Person.objects.filter(name__startswith='a')  # 以什么开头
ret = models.Person.objects.filter(name__istartswith='A')

ret = models.Person.objects.filter(name__endswith='a')  # 以什么结尾
ret = models.Person.objects.filter(name__iendswith='I')

# order by
models.Tb1.objects.filter(name='seven').order_by('id')    # asc
models.Tb1.objects.filter(name='seven').order_by('-id')   # desc


# isnull
ret  = models.Person.objects.filter(phone__isnull=False)


# group by
from django.db.models import Count, Min, Max, Sum
models.Tb1.objects.filter(c1=1).values('id').annotate(c=Count('num'))


ret  = models.Person.objects.filter(birth__year='2019')
ret  = models.Person.objects.filter(birth__contains='2018-06-24')


# limit 、offset
models.Tb1.objects.all()[10:20]

# regex正则匹配，iregex 不区分大小写
Entry.objects.get(title__regex=r'^(An?|The) +')
Entry.objects.get(title__iregex=r'^(an?|the) +')


```

#### 12.5.8 外键操作

##### 1. 表示一对多的关系

   ```python
class Publisher(models.Model):
    name = models.CharField(max_length=32, verbose_name="名称")
    def __str__(self):
        return self.name

class Book(models.Model):
    title = models.CharField(max_length=32)
    pub=models.ForeignKey(Publisher,
                         related_name='books',
                         related_query_name='xxx',
                         on_delete=models.CASCADE)
   def __str__(self):
            return self.title

    # 基于对象的查询
    # 正向
    book_obj = models.Book.objects.get(title='菊花怪大战MJJ')
    print(book_obj.pub) -->所关联的对象

    # 反向
    pub_obj = models.Publisher.objects.get(pk=1)
    pub_obj.book_set   -->一个关系管理对象,（类名小写_set）
    print(pub_obj.book_set.all())  # 没有指定related_name时,使用(类名小写_set.all())获取所有  		
    pub_obj.book_set  -->一个关系管理对象,（类名小写_set）
    print(pub_obj.books.all())  #  指定related_name='books'


    # 基于字段的查询
    #正向
    ret = models.Book.objects.filter(title='菊花怪大战MJJ')
    #  查询老男孩出版的书
    ret = models.Book.objects.filter(pub__name='老男孩出版社')
    # pub=Publisher Object/pub_id = 1`````


    #反向
    # 查询出版菊花怪大战MJJ的出版社

    # 没有指定related_name   ,使用类名的小写
    ret= models.Publisher.objects.filter(book__title='菊花怪大战MJJ')

    # 指定了related_name='books'
    ret= models.Publisher.objects.filter(books__title='菊花怪大战MJJ')

    # related_query_name='xxx'
    ret= models.Publisher.objects.filter(xxx__title='菊花怪大战MJJ')
    #优先级由低到高
   ```

##### 2.关系管理对象的特殊方法

```python
print(pub_obj.books.all()) #-->关系管理对象,relative_name =books
1.set方法
pub_obj.books.set(models.Book.objects.filter(pk__in=[4,5]))  
	# 不能用id 只能用对象
2.add方法
pub_obj.books.add(*models.Book.objects.filter(pk__in=[1,2]))
	
3.remove方法 和 clear方法
pub = models.ForeignKey(Publisher, 
                        related_name='books', 		
                        related_query_name='xxx',
                        null=True, 
                        on_delete=models.CASCADE)
# 外键可为空时有remove  clear
pub_obj.books.remove(*models.Book.objects.filter(pk__in=[1,2]))

pub_obj.books.clear()

4.create方法通过关联创建
obj = pub_obj.books.create(title='用python养猪')
```

##### 3.多对多的关系

```python
mjj = models.Author.objects.get(pk=1)
1.all()  所关联的所有的对象
	mjj.books.all()
2.set  设置多对多的关系    [id,id]    [ 对象，对象 ]
	mjj.books.set([1,2])
	mjj.books.set(models.Book.objects.filter(pk__in=[1,2,3]))
3.add  添加多对多的关系   (id,id)   (对象，对象)
    mjj.books.add(4,5)
    mjj.books.add(*models.Book.objects.filter(pk__in=[4,5]))
4.remove 删除多对多的关系  (id,id)   (对象，对象)
    mjj.books.remove(4,5)
    mjj.books.remove(*models.Book.objects.filter(pk__in=[4,5]))
5.clear()   清除所有的多对多关系
	mjj.books.clear()
6.create()
obj = mjj.books.create(title='跟MJJ学前端',pub_id=1)
print(obj)
book__obj = models.Book.objects.get(pk=1)

obj = book__obj.authors.create(name='taibai')
print(obj)

```

#### 15.5.9 聚合分组

```python
from app01 import models
from django.db.models import Max, Min, Avg, Sum, Count     # 先导入
ret = models.Book.objects.filter(pk__gt=3).aggregate(Max('price'),
                                                     avg=Avg('price'))
print(ret)

# 分组
# 统计每一本书的作者个数
ret = models.Book.objects.annotate(count=Count('author')) # annotate 注释


# 统计出每个出版社的最便宜的书的价格
# 方式一
ret = models.Publisher.objects.annotate(Min('book__price')).values()
# 方式二
ret = models.Book.objects.values('pub_id').annotate(min=Min('price'))

```

#### 12.5.10 F 和 Q

```python
from django.db.models import F

# 比较两个字段的值
ret=models.Book.objects.filter(sale__gt=F('kucun'))

# 只更新sale字段
models.Book.objects.all().update(sale=100)

# 取某个字段的值进行操作
models.Book.objects.all().update(sale=F('sale')*2+10)
```

Q(条件)

|  或

&  与 

~ 非

```python
from django.db.models import Q

ret = models.Book.objects.filter(Q(Q(pk__gt=3) | Q(pk__lt=2)) & Q(price__gt=50))
print(ret)
```

#### 12.5.11 事务

```python
from django.db import transaction

try:
    with transaction.atomic():
        # 进行一系列的ORM操作

        models.Publisher.objects.create(name='xxxxx')
        models.Publisher.objects.create(name='xxx22')

except Exception as e :
    print(e)
```



### 12.6 django的模板系统 template

#### 12.6.1. 常用语法

   ```html
   1.{{ 变量 }}  -->{{ }}表示变量，在模板渲染的时候替换成值,变量名由字母数字和下划线组成
   2.{%   %}   -->{% %}表示逻辑相关的操作。
   3. (.) -->在模板语言中有特殊的含义，用来获取对象的相应属性值。
   	{# 取l中的第一个参数 #}
       {{ l.0 }}
       {# 取字典中key的值 #}
       {{ d.name }}
       {# 取对象的name属性 #}
       {{ person_list.0.name }}
       {# .操作只能调用不带参数的方法 #}
       {{ person_list.0.dream }}
   
   	注意:当模板系统遇到一个（.）时，会按照如下的顺序去查询：
                   在字典中查询
                   属性或者方法
                   数字索引
   .索引  .key .属性  .方法
   ```

#### 12.6.2.过滤器

   `filter过滤器,来改变变量的显示结果`

   `{{ value|filter_name:参数 }}`**:冒号左右没有空格,没有空格,没有空格**

   1. defalut
      	`{{ value|default:"nothing"}}`
         	如果value值没传的话or bool(value)的值为空就显示nothing,bool()的值为空
   
         	注：TEMPLATES的OPTIONS可以增加一个选项：string_if_invalid：'找不到'，可以替代default的的作用。
   
   2. filesizeformat
   
      `{{value|filesizeformat}}`
   
      ​	将值格式化为一个 “人类可读的” 文件尺寸 （例如 '13 KB', '4.1 MB', '102 bytes', 等等）
      ​	{{ value|filesizeformat }}这里的value是以字节为单位的int或者str,float会舍弃小数部分.
      ​	如果 value 是 123456789，输出将会是 117.7 MB。
   
   3. add 给变量加参数
      	`{{ value|add:"2" }}`-->value是数字4，则输出结果为6。
         	`{{ first|add:second }}`-->如果first是 [1,.2,3] ，second是 [4,5,6] ，那输出结果是[1,2,3,4,5,6] 
          优先转换为数值运算,add过滤器
   
   4. length--返回value的长度`{{[1,2,3,4]|length}}   display:4`
   
      `length_is:arg`
   
   5. slice切片-->`{{value|slice:"2:-1"}}`,取首元素{{ value|first }},取最后元素{{ value|last }},可以切片和步长,一般是列表.**使用与Python列表切片相同的语法**。
   
   6. dictsort and dictsortreversed(反序)
      	获取字典列表并返回按参数中给出的键对应的值排序的列表。
   
      ```python
      1. [
         	   {'name': 'zed', 'age': 19},
             {'name': 'amy', 'age': 22},
             {'name': 'joe', 'age': 31},
         ]
             {{value|dictsort:'age'}},
      2. 更高级的用法
      [
              {'title': '1984', 'author': {'name': 'George', 'age': 45}},
              {'title': 'Timequake', 'author': {'name': 'Kurt', 'age': 75}},
              {'title': 'Alice', 'author': {'name': 'Lewis', 'age': 33}},
      ]
      {% for book in books|dictsort:"author.age" %}
      	{{ book.title }} ({{ book.author.name }})
      {% endfor %} 
      
      3. [
          ('a', '42'),
          ('c', 'string'),
          ('b', 'foo'),
      ]
      {{ value|dictsort:0 }} display:根据索引处的元素排序列表,此时arg必须是int,str则输出为空
      ```
   
   7. floatformat 
   
      floatformat,没有arg时,舍入到1个小数位
   
      - 和数字整数参数arg一起使用，则将数字floatformat四舍五入到arg个小数位,不足的补0
      - 有用的是传递0（零）作为参数，它将浮点数舍入为最接近的整数。
      - 传递给的参数floatformat为负数，它会将数字四舍五入到多个小数位 - 但只有在显示小数部分时才会这样。,整数还是整数,小数补足道arg个小数位
   
   8. join 拼接,和str.join(eq)类似,`{{value|join:"//"}}`使用字符串连接列表，如Python `str.join(list)`
   
   9. random,返回给定列表的随机项

   ```python
   1.divisibleby,如果value可以被arg整除，则返回True.
   2.get_digit,给定一个整数，返回请求的数字，其中1是最右边的数字，2是第二个最右边的数	字，等等,value和arg都是int,返回int,从右向左,以1为起始数字.
   3.lower小写/upperdaxie/title标题/capfirst首字母大写-->{{value|upper/lower/title/capfirst}}
   4.ljust左对齐/右对齐rjust/center居中-->{{value|center:"15"}}
   5.wordcount 返回单词数,"Joel is a slug"4
   9.truncatechars截断,参数为：截断的字符数,如果字符串字符多于指定的字符数量，那么会被截断。截断的字符串将以可翻译的省略号序列（“...”）结尾。
       {{ value|truncatechars:9}}
   10.date 日期格式化-->{{ value|date:"Y-m-d H:i:s"}}
   	更多字符串格式化关注
       https://docs.djangoproject.com/en/1.11/ref/templates/builtins/#date
   11.若value="<a href='#'>点我</a>",添加{{ value|safe}},
   	是一个单独的变量我们可以通过过滤器“|safe”的方式告诉Django这段代码是安全的不必转义。
   12.cut:arg,从给定的字符串中删除arg的所有值。 
   
   ```

   ![1560859985675](D:\md_down_png\1560859985675.png)

   ![1560860005470](D:\md_down_png\1560860005470.png)

   ![1560860052069](D:\md_down_png\1560860052069.png)

#### 12.6.3.for 循环

   ```python
   <ul>
   {% for user in user_list %}
       <li>{{ user.name }}</li>
   {% endfor %}
   </ul>
   
   #-------------------------#
   一些可用参数
       forloop.counter	当前循环的索引值（从1开始）
       forloop.counter0	当前循环的索引值（从0开始）
       forloop.revcounter	当前循环的倒序索引值（到1结束）
       forloop.revcounter0	当前循环的倒序索引值（到0结束）
       forloop.first	当前循环是不是第一次循环（布尔值）
       forloop.last	当前循环是不是最后一次循环（布尔值）
       forloop.parentloop{}	本层循环的外层循环,父循环的字典
   #循环完了执行empty的代码
   <ul>
   {% for user in user_list %}
       <li>{{ user.name }}</li>
   {% empty %} -->for循环没有循环出内容
       <li>空空如也</li>
   {% endfor %}
   </ul>
   
   ```

#### 12.6.4.if-else-elif

   ```python
   {% if user_list %}
     用户人数：{{ user_list|length }}
   {% elif black_list %}
     黑名单数：{{ black_list|length }}
   {% else %}
     没有用户
   {% endif %}
   
   注意:if语句支持 and 、or、==、>、<、!=、<=、>=、in、not in、is、is not判断。
   注意:if语句支持 and 、or、==、>、<、!=、<=、>=、in、not in、is、is not判断。
   注意:if语句支持 and 、or、==、>、<、!=、<=、>=、in、not in、is、is not判断。
   注意 : 模板中  不支持连续连续判断  也不支持算数运算（过滤器）
   ```

#### 12.6.5. csrf_token 标签(跨站请求伪造)

   放到一个form表单里面,放在form表单内   有一个隐藏的input标签   name=‘csrfmiddlewaretoken’  

   ![1560911471554](D:\md_down_png\1560911471554.png)   ![1560911934451](D:\md_down_png\1560911934451-1561376183656.png)

   这样注释的中间件就解除

#### 12.6.6. with 定义一个智能关键变量

   ```python
   {% with total=business.employees.count %}
       {{ total }} employee{{ total|pluralize }}
   {% endwith %}
   
   {%  with 变量 as new  %}
   	{{ new }}
   {% endwith %}
   ```

#### 12.6.7. 注意事项

   ```python
   1. Django的模板语言不支持连续判断，即不支持以下写法：
   
   {% if a > b > c %}
   ...
   {% endif %}
   2. Django的模板语言中属性的优先级大于方法
   
   def xx(request):
       d = {"a": 1, "b": 2, "c": 3, "items": "100"}
       return render(request, "xx.html", {"data": d})
   如上，我们在使用render方法渲染一个页面的时候，传的字典d有一个key是items并且还有默认的 d.items() 方法，此时在模板语言中:
   
   {{ data.items }}
   默认会取d的items key的值。
   ```

#### 12.6.8. 母版和继承,

   1. 母版	

      就是一个普通HTML提取多个页面的公共部分 定义block块

   2. 继承：
      1. {% extends ‘base.html’ %}
      2. 重写block块   —— 写子页面独特的内容

   3. 注意的点：
      1. {% extends 'base.html' %} 写在第一行   前面不要有内容 有内容会显示
      2. {% extends 'base.html' %}  'base.html' 加上引号   不然当做变量去查找
      3. 把要显示的内容写在block块中
      4. 定义多个block块，定义 css  js 块

   1. 继承母版

      在子页面中在页面最上方使用下面的语法来继承母板。

      ```
      {% extends 'layouts.html' %}
      ```

   2. 块bolck

      ```
      {% block page-main %}
        <p>世情薄</p>
        <p>人情恶</p>
        <p>雨送黄昏花易落</p>
      {% endblock %}
      ```

   3. 组件

      一小段HTML代码段   ——》 nav.html 

      {% include ‘nav.html ’  %}

      可以将常用的页面内容如导航条，页尾信息等组件保存在单独的文件中，然后在需要使用的地方按如下语法导入即可。

      ```
      {% include 'navbar.html' %}
      ```

   4. 静态文件相关

      ```
      
      {% load static %}
      
       <link rel="stylesheet" href="{% static '/plugins/bootstrap-3.3.7/css/bootstrap.css' %}">
       <link rel="stylesheet" href="{% static '/css/dsb.css' %}">
       
      {%  static ‘静态文件的相对路径’ %}   这样使用
      {% static '/plugins/bootstrap-3.3.7/css/bootstrap.css' %} 
      
      {% get_static_prefix %}   ——》 获取别名
      
      {%  get_static_prefix  %}      ——》   STATIC_URL  
            
      引用JS文件时使用：
      {% load static %}
      <script src="{% static "mytest.js" %}"></script>
      
      某个文件多处被用到可以存为一个变量
      {% load static %}
      {% static "images/hi.jpg" as myphoto %}
      <img src="{{ myphoto }}"></img>
      
      {% load static %}
      {% get_static_prefix as STATIC_PREFIX %}
      
      <img src="{{ STATIC_PREFIX }}images/hi.jpg" alt="Hi!" />
      <img src="{{ STATIC_PREFIX }}images/hi2.jpg" alt="Hello!" />
      ```

#### 12.6.9.自定义simpletag,filter,inclusiontag

1. 自定义过滤器`filter`

   ```python
   # 	前提,自定义过滤器
   
   1.在app下创建一个名为tempplatetags的python包；#必须是这个名字
   
   2.在python中创建py文件,文件名自定义（my_tags.py）;#随意起名
   
   3.在py文件中写：
   
   from django import template
   register = template.Library()  # register也不能变
   
   4.写函数+装饰器:
   @register.filter
   def add_xx(value, arg):  # 最多有两个参数
   	#逻辑代码
       return '{}-{}'.format(value, arg)
   
   ##----我是华丽的分割线-----##
   
   #	使用
   # 在用使用的html文件内先导入,再使用
   {% load my_tags %}
   {{ 'alex'|add_xx:'dsb' }}
   ```

2. 自定义方法`simplet tag`

   和自定义filter类似，只不过接收更灵活的参数。

   ```python
   # 定义注册simple tag
   @register.simple_tag(name="plus")
   def plus(a, b, c):
       return "{} + {} + {}".format(a, b, c)
   
   #使用自定义simple tag
   {% load app01_demo %}
   
   {# simple tag #}
   {% plus "1" "2" "abc" %} # --><参数间空格隔开
   ```

3. 自定义标签`inclusion_tag`,多用于返回html代码片段

   ```python
   # 1.定义注册
   from django import template
   register = template.Library()
   @register.inclusion_tag('result.html')
   def show_results(n):
       n = 1 if n < 1 else int(n)
       data = ["第{}项".format(i) for i in range(1, n+1)]
       return {"data": data}
   
   # 2.在模板文件内-->templates/result.html
   
   <ul>
     {% for choice in data %}
       <li>{{ choice }}</li>
     {% endfor %}
   </ul>
   
   # 3.在要返回的html页面内使用---->templates/index.html
   <!DOCTYPE html>
   <html lang="en">
   <head>
     <meta charset="UTF-8">
     <meta http-equiv="x-ua-compatible" content="IE=edge">
     <meta name="viewport" content="width=device-width, initial-scale=1">
     <title>inclusion_tag test</title>
   </head>
   <body>
   
   {% load my_inclusion %}
   
   {% show_results 10 %}
   </body>
   </html>
   ```

   ​		![1560992896941](D:\md_down_png\1560992896941.png)

   ​				![1560917495352](D:\md_down_png\1560917495352.png)

### 12.7 django的路由系统

#### 12.7.1 urlconf 配置

```python
urlpatterns = [
    # url(r'^admin/', admin.site.urls),
    url(r'^blog/$', views.blog),
    url(r'^blog/[0-9]{4}/$', views.blogs),
]

#----------------------分割线---------------------------------#
urlpatterns = [
     url(正则表达式, views视图，参数，别名),
]
正则表达式：一个正则表达式字符串
views视图：一个可调用对象，通常为一个视图函数
参数：可选的要传递给视图函数的默认参数（字典形式）
别名：一个可选的name参数
```

> **注意: django2.0版本路由系统的写法变更了,也支持re_path写法,和1.11版本的url一致**
>
> urlconf在请求的URL 上查找，将它当做一个普通的Python 字符串。不包括GET和POST参数以及域名。
>
> urlconf不检查请求的方法。换句话讲，所有的请求方法 —— 同一个URL的`POST`、`GET`、`HEAD`等等 —— 都将路由到相同的函数。

##### 1. 通过url的字典参数传递额外的参数

可以接收一个可选的第三个参数，它是一个字典，表示想要传递给视图函数的额外关键字参数。

该优先级是最高的,当传递额外参数的字典中的参数和URL中捕获值的命名关键字参数同名时，函数调用时将使用的是字典中的参数，而不是URL中捕获的参数。

#### 12.7.2 正则表达式

```python
^   $    [0-9]   [a-zA-Z]     \d   \w   .    *  +   ？
```

1. urlpatterns中的元素按照书写顺序从上往下逐一匹配正则表达式，一旦匹配成功则不再继续。

2. 若要从URL中捕获一个值，只需要在它周围放置一对圆括号（分组匹配）。

3. 不需要添加一个前导的反斜杠，因为每个URL 都有。例如，应该是^articles 而不是 ^/articles。

4. 每个正则表达式前面的'r' 是可选的但是建议加上。

5. Django settings.py配置文件中默认没有 APPEND_SLASH 这个参数，但 Django **默认这个参数为 APPEND_SLASH = True**。 其作用就是自动在网址结尾加'/'。

6. 如果在settings.py中设置了 **APPEND_SLASH=False**，此时我们再请求 http://www.example.com/blog 时就会提示找不到页面。

   > **补充说明:是否开启URL访问地址后面不为/跳转至带有/的路径的配置项
   > APPEND_SLASH=True**

#### 12.7.3 分组和命名分组

```python
1. 分组
	url(r'^blog/([0-9]{4})/$', views.blogs)
    # —— 》 分组  将捕获的参数按位置传参传递给视图函数
2.命名分组
	url(r'^blog/(?P<year>[0-9]{4})/$', views.blogs)
    # —— 》 命名分组  将捕获的参数按关键字传参传递给视图函数
类似于 -->views.month_archive(request, year="2017", month="12")
注意 -->捕获到的永远是字符串,另外可以在view视图处设置year和month的默认值,page()将使用正则表达式捕获到的num值的优先级更高.但在url的字典参数中,更高的优先级.
```

#### 12.7.4 路由分发include其他的url

```python
from django.conf.urls import include, url

urlpatterns = [
    # url(r'^admin/', admin.site.urls),
    url(r'^app01/', include('app01.urls')),# 可以包含其他的URLconfs文件
]

#-------------------分割线-----------------------#
# app01目录下的urls.py

from django.conf.urls import url
from . import views
# from app01 import views
urlpatterns = [
    # url(r'^admin/', admin.site.urls),
    url(r'^blog/$', views.blog),
    url(r'^blog/(?P<year>[0-9]{4})/(?P<month>\d{2})/$', views.blogs),
]
```

#### 12.7.5 url反向解析

​	为了不要硬编码这些url（费力、不可扩展且容易产生错误）或者设计一种与URLconf 毫不相关的专门的URL 生成机制，因为这样容易导致一定程度上产生过期的URL。django提供了一种方法,双向映射,URL 映射是URL .

```python
1.根据用户/浏览器发起的URL 请求，它调用正确的Django 视图，并从URL 中提取它的参数需要的值。
2.根据Django 视图的标识和将要传递给它的参数的值，获取与之关联的URL。

url反查 -->反向解析
	1.在模板中：使用url模板标签。
	2.在Python 代码中：使用django.core.urlresolvers.reverse() 函数。
	3.在更高层的与处理Django 模型实例相关的代码中：使用get_absolute_url() 方法。
    
简单来说就是可以给我们的URL匹配规则起个名字，一个URL匹配模式起一个名字。
这样我们以后就不需要写死URL代码了，只需要通过名字来调用当前的URL。
```

1. 对静态的url

   ```python
   url(r'^blog/$', views.blog, name='blog'), -->别名
   
   1. 反向解析
   	templates
       	{% url 'blog' %}    -->  /blog/
   	py文件-->views
       	from django.urls import reverse
   		reverse('blog')   --> '/blog/'
           #例如 return redirect(reverse("blog"))
   ```

2. 普通分组

   ```python
   url(r'^blog/([0-9]{4})/(\d{2})/$', views.blogs, name='blogs'),
   	templates
       	{% url 'blogs' 2222 12 %}"   ——》  /blog/2222/12/
       py文件 -->views
       	from django.urls import reverse
   		reverse('blogs',args=('2019','06',))   ——》 /app01/blog/2019/06/ 
   # 注意args=("12345",)  --->这个逗号不能少
   ```

3. 命名分组

   ```python
   url(r'^blog/(?P<year>[0-9]{4})/(?P<month>\d{2})/$', views.blogs, name='blogs')
   	templates
       	{% url 'blogs' 2222 12 %}"   ——》  /blog/2222/12/
   		{% url 'blogs' year=2222 month=12 %}"   ——》  /blog/2222/12/
       py文件 -->views
       	from django.urls import reverse
   		reverse('blogs',args=('2019','06'))   ——》 /app01/blog/2019/06/ 
   		reverse('blogs',kwargs={'year':'2019','month':'06'})   ——》 /app01/blog/2019/06/ 
   ```

4. 总结

   1. url上捕获参数  都是字符串

   2. 命名你的URL 模式时，请确保使用的名称不会与其它应用中名称冲突。如果你的URL 模式叫做`comment`，而另外一个应用中也有一个同样的名称，当你在模板中使用这个名称的时候不能保证将插入哪个URL。

   在URL 名称中加上一个前缀，比如应用的名称，将减少冲突的可能。我们建议使用`myapp-comment` 而不是`comment`。

#### 12.7.6 url反向解析 -命名空间模式namespace

即使不同的APP使用相同的URL名称，URL的命名空间模式也可以让你唯一反转命名的URL。

```python
urlpatterns = [
    # url(r'^admin/', admin.site.urls),
    url(r'^app01/', include('app01.urls', namespace='app01')),
    url(r'^app02/', include('app02.urls', namespace='app02')),
]
# 在app01 和 app02 中都有url的name="blogs"
# 我的两个app中 url名称重复了，我反转URL的时候就可以通过命名空间的名称得到我当前的URL
语法 : "命名空间名称:URL名称"
templates
	{% url 'app01:blogs' 2222 12 %}
py文件
	reverse('app01:blogs',args=('2019','06'))
# 这样即使app中URL的命名相同，我也可以反转得到正确的URL了。
```







### 12.3.8 导入js/css的路径问题

- **`./`**：代表目前所在的目录
- **`. ./`**：代表上一层目录
- **`/`**：代表根目录**

1. 案例解析

   ```html
   1.文件在当前目录下,以当前项目文件为中心
   “./example.jpg” 或 “example.jpg”
   
   2.文件在上层目录
   （1）在上层目录下
   “../example.jpg”
   
   （2）在上层目录下的一个Image文件夹下
   “../Image/example.jpg”
   
   （3）在上上层目录下
   "../../example.jpg"
   
   3、文件在下一层目录
   “./Image/example.jpg”
   
   4、根目录
   “C:/Image/example.jpg”
   
   ```
   

