#!/usr/bin/env python        	
# -*- coding:utf-8 -*-	 
# @Project  : 选课系统
# @Author : Jack Deng
# @Time   :  2019-04-30 15:29
# @File   : md5.py

import hashlib
import json
import pickle
import random


def md5(content,pwd):
    md5 = hashlib.md5(content.encode('utf-8'))
    md5.update(pwd.encode('utf-8'))
    return md5.hexdigest()








