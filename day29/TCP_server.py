#!/usr/bin/env python        	
# -*- coding:utf-8 -*-	 
#@Project  : s21day29 
#@Author : Jack Deng       
#@Time   :  2019-05-08 19:07
#@File   : TCP_server.py

import socket
import struct
from md5 import md5
import json

def islogin(conn):
    while True :
        ret = conn.recv(1024)
        print(ret)
        val = json.loads(ret.decode('utf-8'))
        print(val)
        f = open('user_data.txt','r',encoding='utf-8')
        CUENTYUSER = None
        for line in f:
            line = line.strip().split('|')
            if val['QQ'] == line[0] and  md5(line[0],val['pwd']) == line[2] :
                CUENTYUSER = {'QQ':val['QQ'],'status': '1','color':line[3]}      #   qq号    状态
                print(' 成功登录 ')
                conn.send(json.dumps(CUENTYUSER).encode('utf-8'))
                return
        if not CUENTYUSER :
            CUENTYUSER = {'QQ':val['QQ'],'status': '0','color':None}
            conn.send(json.dumps(CUENTYUSER).encode('utf-8'))
            continue


def getfile(conn):
    print('开始接受')
    len_dic = conn.recv(4)
    len_dic = struct.unpack('i',len_dic)           # 得到字典的长度,元组(长度,)
    str_dic = conn.recv(len_dic[0])
    dic = json.loads(str_dic)
    filesize = dic['filesize']
    with open(dic['filename'], mode='ab') as f :
        while filesize > 0 :
            content = conn.recv(1024)
            print(filesize)
            f.write(content)
            filesize -= 1024
    print('传输完成')


def run():
    sk = socket.socket()
    sk.bind(('127.0.0.1' , 8888))
    sk.listen(5)
    conn , ddr = sk.accept()
    islogin(conn)
    print('登录成功')
    info = {'getfile':getfile}
    getfile(conn)
    conn.close()
    sk.close()

run()

# 190326060
# D:\day29\6.粘包问题.mp4