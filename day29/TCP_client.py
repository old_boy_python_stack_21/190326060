#!/usr/bin/env python        	
# -*- coding:utf-8 -*-	 
# @Project  : s21day29
# @Author : Jack Deng
# @Time   :  2019-05-08 19:07
# @File   : TCP_client.py

import socket
import struct
import json
import os

CURRENTY_USER = {'status': None}

def warpper(func):
    def inner(*args,**kwargs):
        if not CURRENTY_USER['status'] :
            print('请先登录!!!!!')
            return
        v = func(*args,**kwargs)
        return v
    return inner



def login(sk):
    """
    用户登录
    :return:
    """
    while True:
        user = input('请输入QQ号(输入Q退出客户端)')
        if user.upper() == 'Q': exit(0)
        pwd = (input('请输入密码'))
        user_dic = json.dumps({'QQ': user, 'pwd': pwd}).encode('utf-8')
        sk.send(user_dic)
        ret = sk.recv(1024)
        if json.loads(ret.decode('utf-8'))['status'] == '1':
            print('登录成功')
            CURRENTY_USER['status'] = True
            return json.loads(ret.decode('utf-8'))
        print('用户名或者密码错误,请重新登陆')

@warpper
def fileupload(sk):
    file_path = input('输入文件路径>>>')
    if os.path.isabs(file_path):
        filename = os.path.basename(file_path)  # 文件名
        filesize = os.path.getsize(file_path)  # 文件大小
        dic = {'filename': filename, 'filesize': filesize}
        str_dic = json.dumps(dic).encode('utf-8')  # 字节
        len_dic = struct.pack('i', len(str_dic))  # 4个字节(表示字典的长度)
        sk.send(len_dic + str_dic)
        with open(file_path, mode='rb') as f:
            while filesize > 0:
                content = f.read(1024)
                sk.send(content)
                filesize -= 1024
        print('上传%s完成' %filename)
    else:
        print('路径错误')

@warpper
def filedown(sk):
    pass

def run():
    sk = socket.socket()
    sk.connect(('127.0.0.1', 8888))
    info = {'login': login, 'fileupload': fileupload, 'filedown': filedown}
    print('************请选择功能***************')
    for i in info :
        print(i)
    while True:
        choice = input('请输入功能>>>(输入Q退出)')
        if choice.upper() == 'Q': break
        func = info.get(choice)
        print(func)
        if not func:
            print('输入错误,请重新输入')
            continue
        func(sk)
    sk.close()


run()
