#!/usr/bin/env python
# -*- coding:utf-8 -*-


####一、有变量name = "aleX leNb " 完成如下操作：

      ### 移除 name 变量对应的值两边的空格,并输出处理结果
"""
name = "aleX leNb "
print(name.strip())
"""

      ### 判断 name 变量是否以 "al" 开头,并输出结果（用切片）
"""
name = "aleX leNb "
new_name = name[0:2]
if new_name == 'al':
    print(new_name)
"""

      ### 判断name变量是否以"Nb"结尾,并输出结果（用切片）
"""
name = "aleX leNb "
new_name = name[-2:]
if new_name == 'Nb':
    print(new_name)
"""

      ### 将 name 变量对应的值中的 所有的"l" 替换为 "p",并输出结果
"""
name = "aleX leNb "
new_name = name.replace('l','p')
print(new_name)
"""

       ###将 name 变量对应的值根据 所有的"l" 分割,并输出结果
"""
name = "aleX leNb "
new_name = name.split('l')
print(new_name)
"""

      ###将 name 变量对应的值变大写,并输出结果
"""
name = "aleX leNb "
new_name = name.upper()
print(new_name)
"""

      ###将 name 变量对应的值变小写,并输出结果
"""
name = "aleX leNb "
new_name = name.lower()
print(new_name)
"""

      ###请输出 name 变量对应的值的第 2 个字符?
"""
name = "aleX leNb "
new_name = name[1]
print(new_name)
"""

      ###请输出 name 变量对应的值的前 3 个字符?
"""
name = "aleX leNb "
new_name = name[0:3]
print(new_name)
"""

      ###请输出 name 变量对应的值的后 2 个字符?
"""
name = "aleX leNb "
new_name = name[-2:]
print(new_name)
"""


########  二、有字符串s = "123a4b5c"

##### - 通过对s切片形成新的字符串 "123"
"""
text = "123a4b5c"
text1 = text[:3]      
#  text1 = text[0:3]
print(text1)

"""
# - 通过对s切片形成新的字符串 "a4b"
"""
text = "123a4b5c"
text1 = text[3:6]
print(text1)
##或者
text = "123a4b5c"
text1 = text[3:-2]
print(text1)
"""

# - 通过对s切片形成字符串s5,s5 = "c"
"""
text = "123a4b5c"
text2 = text[-1]
print(text2)
"""


# - 通过对s切片形成字符串s6,s6 = "ba2"
"""
text = "123a4b5c"
s1 = text[1]
s2 = text[3]
s3 = text[5]
print(s3+s2+s1)
"""

#三、使用while循环字符串 s="asdfer" 中每个元素。

"""通过while循环语句，无限循环打印"asdfer"的每个元素
s="asdfer"
s_len=len(s)
times = 0
while times <= s_len -1:
    s1 = s[times]
    print(s1)
    times += 1
    if times == s_len:
        times =0
"""

#四、使用while循环对s="321"进行循环，打印的内容依次是："倒计时3秒"，"倒计时2秒"，"倒计时1秒"，"出发！"。

"""
s = "321"
s_len = len(s)
times = 0
while times <= s_len -1:
    s1 = s[times]
    print('倒计时%s秒' %(s1,))
    times += 1
    if times == s_len:
        times =0
        print('出发')
"""

#五、实现一个整数加法计算器(两个数相加)：content = input("请输入内容:") 用户输入：5+9或5+ 9或5 + 9（含空白），然后进行分割转换最终进行整数的计算得到结果。

"""
content = input("请输入两个数相加:")
t = content.replace(" ","")
t1,t2 = t.split("+")                 #百度查到split的用法，只计算2个数相加。
print(int(t1)+int(t2))

"""

###六、计算用户输入的内容中有几个 h 字符？

"""
text = input("请输入内容：")
text_len = len(text)
times = 0
num = 0
while times <= text_len-1:
    if text[times] == 'h':
        num += 1
    times += 1
print('输入的内容有%d个h字符' %(num,))

"""

####七、计算用户输入的内容中有几个 h 或 H  字符？

"""
# text = input("请输入内容：")
# text_len = len(text)
# times = 0
# num = 0
# while times <= text_len-1:
#     if text[times] == 'h':
#         num += 1
#     elif text[times] == 'H':
#         num += 1
#     times += 1
# print('输入的内容有%d个h或H字符' %(num,))

###第七题的方法，运用第六题的代码，加上upper语句和lower语句都可实现。
###第三种方法：

text = input("请输入内容：")
text_len = len(text)
times = 0
num = 0
while times <= text_len-1:
    if text[times] == 'h'or text[times] == 'H':
        num += 1
    times += 1
print('输入的内容有%d个h或H字符' %(num,))

"""


###八、使用while循环分别正向和反向对字符串 message = "伤情最是晚凉天，憔悴厮人不堪言。"进行打印
"""
count = 0
message = "伤情最是晚凉天，憔悴厮人不堪言。"
message_new1 = ''
message_new2 = ''
message_len = len(message)
while count <= message_len - 1:
    message_new1 = message_new1 + message[count]
    count += 1
print(message_new1)
count = 0
while count <= message_len - 1:
    message_new2 = message_new2 + message[message_len - 1 - count]
    count += 1
print(message_new2)

"""

"""
count = 0
message = "伤情最是晚凉天，憔悴厮人不堪言。"
message_len = len(message)
while count <= message_len - 1:
    print(message[count])
    count += 1
    if count == message_len:
        count = 0
        while count <= message_len - 1:
            print((message[message_len - 1 - count]))
            count += 1
        if count == message_len:
            count = 0
            continue    #继续，到最初的循环？效果达到了，不是很理解。

"""


###九、获取用户输入的内容中 前4个字符中 有几个 A ？

"""
content = input("请输入内容：")
text = (content[0:4])
num = 0
times = 0
while  times <= 3:
    if text[times] == 'A':
        num += 1
    times += 1
print('用户输入的内容前4个字符中有%d个A' %(num,))
"""

###十、如果判断name变量对应的值前四位"l"出现几次,并输出结果。

"""
content = input("请输入内容：")
text = (content[0:4])
num = 0
times = 0
while  times <= 3:
    if text[times] == 'l':
        num += 1
    times += 1
print('用户输入的内容前4位有%d个l' %(num,))
"""

###十一、获取用户两次输入的内容，并将所有的数据获取并进行相加。

"""
###利用isdigir语句和索引和len判断输入字符串的单个字符是否能转化为int数据，
###利用字符串的加法把所有数字拼接起来，转化为int类型相加。
t1 = 0
t2 = 0
content1 = ''
content2 = ''
num1 = input("请输入：")
num2 = input("请输入：")
while t1 <= len(num1)-1 :
    if  num1[t1].isdigit():
        content1 = content1 + num1[t1]
    t1 += 1
while t2 <= len(num2)-1 :
    if  num2[t2].isdigit():
        content2 = content2 + num2[t2]
    t2 += 1
print(int(content1)+int(content2))
"""

######十一题另一种方法尝试：

###利用isdigir语句和索引和len判断输入字符串的单个字符是否能转化为int数据，
###利用replace语句把不能转为int的一个字符替换为' ',最后再把' '替换为'',就得到完全有数字组成的字符串。

"""
t1 = 0
content1 = input("请输入：")
while t1 <= len(content1)-1 :
    if not content1[t1].isdigit():
        content1 = content1.replace(content1[t1],' ',1)
    t1 += 1
count1 = content1.replace(' ','')
t2 = 0
content2 = input("请输入：")
while t2 <= len(content2)-1 :
    if not content2[t2].isdigit():
        content2 = content2.replace(content2[t2],' ',1)
    t2 += 1
count2 = content2.replace(' ', '')
print(int(count1)+int(count2))

"""


# a=123456
# print(a[2])
#######   int 数据不能索引和切分