from django.apps import AppConfig


class BookmgrConfig(AppConfig):
    name = 'book_system'
