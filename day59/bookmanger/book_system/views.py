import time
from django.views import View
from django.shortcuts import render, redirect, HttpResponse
from django.shortcuts import render, HttpResponse, redirect
from book_system import models
from django import template


# Create your views here.
def publish_list(request):
    obj_list = models.Publisher.objects.all().order_by('pk')
    if request.method == 'POST':
        pid = request.POST.get('id')
        obj_list = models.Publisher.objects.limit()
        if not obj_list:
            return HttpResponse('搜索的数据不存在')

    # for item in obj_list:
    #     print(item.p_name)
    return render(request, 'publish_list.html', {"obj_list": obj_list})


def add_publisher(request):
    error = ''
    if request.method == 'POST':
        p_name = request.POST['p_name']
        if models.Publisher.objects.filter(p_name=p_name):
            error = '出版社已存在'
            print(error)
        elif not p_name:
            error = '出版社名不能为空'
            print(error)
        else:
            models.Publisher.objects.create(p_name=p_name)
            return redirect('/publish_list/')
    return render(request, 'add_publish.html', {'error': error})


def edit_publisher(request):
    error = ''
    pid = request.GET.get('id')  # # url上携带的参数  不是GET请求提交参数,
    obj_list = models.Publisher.objects.filter(pk=pid)
    if not obj_list:
        error = '编辑的出版社不存在'
    if request.method == "POST":
        obj = obj_list[0]
        p_name = request.POST.get('p_name')
        if models.Publisher.objects.filter(p_name=p_name):
            error = '新修改的名称已存在'
        elif obj.p_name == p_name:
            error = '名称未修改'
        elif not p_name:
            error = '名称不能为空'
        if not error:
            # 新修改的名称不在数据库内,不为空,并且已经修改(和原来的名称不同)
            obj.p_name = p_name
            obj.save()
            return redirect('/publish_list/')
    return render(
        request, 'edit_publish.html', {
            'error': error, 'obj': obj_list})


def del_publisher(request):
    pid = request.GET.get('id')
    obj_list = models.Publisher.objects.filter(pk=pid)
    if not obj_list:
        return HttpResponse('要删除的数据不存在')
    obj_list.delete()
    return redirect('/publish_list/')


def search(request):
    msg = '<h1 style="text-align: center;margin-top: 80px;color: darkred">{}</h1>'
    wd = request.POST.get('wd')
    if not wd or wd.strip() == '':
        return HttpResponse(msg.format("关键词不能为空"))
    wd = wd.strip()
    a_obj = models.Author.objects.filter(author=wd)
    b_obj = models.Book.objects.filter(b_name=wd)
    p_obj = models.Publisher.objects.filter(p_name=wd)
    if not any([a_obj, b_obj, p_obj]):
        return HttpResponse(msg.format('您搜索的数据不存在'))
    return render(
        request, 'base.html', {
            'a_obj': a_obj, 'b_obj': b_obj, 'p_obj': p_obj})


def index(request):
    lst = [1, 100, 500, 250, 360]
    url = 'https://www.baidu.com'

    return render(request, "text.html", {'lst': lst, 'url': url})


# Create your views here.
def author_list(request):
    author_book_list = models.Author.objects.all()
    """
    for i in author_book_list:
        print(i.pk)
        print(i.author)
        for item in i.book.all():
            print(item.b_name)
    """
    return render(
        request, 'author_list.html', {
            'author_book_list': author_book_list})


def add_author(request):
    error = ''
    book_list = models.Book.objects.all()
    if request.method == "POST":
        author = request.POST.get('author')
        bid = request.POST.getlist('bid')
        if models.Author.objects.filter(author=author):
            error = "作者已存在"
        if not author:
            error = '作者名不能为空'
        if not error:
            author_obj = models.Author.objects.create(author=author)
            author_obj.book.set(bid)
            return redirect('/author_list/')
    return render(
        request, 'add_author.html', {
            'book_list': book_list, 'error': error})


def del_author(request):
    aid = request.GET.get('id')
    models.Author.objects.get(pk=aid)
    models.Author.objects.filter(pk=aid).delete()
    return redirect('/author_list/')


def edit_author(request):
    aid = request.GET.get('id')
    obj_list = models.Author.objects.filter(pk=aid)
    error = ''
    if request.method == "POST":
        author = request.POST.get('author')
        bids = request.POST.getlist('bids')
        if models.Author.objects.filter(author=author):
            error = '作者名称已重复'
        if not author:
            error = '作者名不能为空'
        if not error:
            obj = models.Author.objects.get(pk=aid)
            obj.author = author
            obj.save()
            obj.book.set(bids)
            return redirect('/author_list/')

    book = models.Book.objects.all()
    return render(
        request, 'edit_author.html', {
            'obj_list': obj_list, 'book': book})


# Create your views here.

def book_list(request):
    obj_list = models.Book.objects.all().order_by('pun_id')
    return render(request, 'book_list.html', {'obj_list': obj_list})


def add_book(request):
    error = ''
    pub_list = models.Publisher.objects.all()
    if request.method == 'POST':
        b_name = request.POST['b_name']
        p_id = request.POST['pub_id']
        print(b_name, p_id)
        if models.Book.objects.filter(b_name=b_name):
            error = '书籍已存在'
        elif not b_name:
            error = '书籍名不能为空'
        else:
            models.Book.objects.create(b_name=b_name, pun_id=p_id)
            return redirect('/book_list/')
    return render(
        request, 'add_book.html', {
            'error': error, 'pub_list': pub_list})


class Add_book(View):

    def dispatch(self, request, *args, **kwargs):
        start_time = time.time()
        ret = super().dispatch(request, *args, **kwargs)
        end_time = time.time()
        print("耗费{}秒".format(start_time - end_time))
        return ret

    def get(self, request):
        print('走get')
        pub_list = models.Publisher.objects.all()
        return render(request, 'add_book.html', {'pub_list': pub_list})

    def post(self, request):
        print('走post')
        error = ''
        pub_list = models.Publisher.objects.all()
        b_name = request.POST['b_name']
        p_id = request.POST['pub_id']
        if models.Book.objects.filter(b_name=b_name):
            error = '书籍已存在'
        elif not b_name:
            error = '书籍名不能为空'
        if not error:
            models.Book.objects.create(b_name=b_name, pun_id=p_id)
            return redirect('/book_list/')
        return render(
            request, 'add_book.html', {
                'error': error, 'pub_list': pub_list})


def del_book(request):
    bid = request.GET.get('id')
    obj_list = models.Book.objects.filter(pk=bid)
    if not obj_list:
        return HttpResponse('要删除的数据不存在')
    obj_list.delete()
    return redirect('/book_list/')


def edit_book(request):
    error = ''
    bid = request.GET.get('id')  # # url上携带的参数  不是GET请求提交参数,
    obj_list = models.Book.objects.filter(pk=bid)
    if not obj_list:
        error = '编辑的书籍不存在'

    # 编辑的书籍存在
    pub_list = models.Publisher.objects.all()

    if request.method == "POST":
        obj = obj_list[0]
        b_name = request.POST.get('b_name')
        p_id = request.POST.get('pub_id')
        if models.Book.objects.filter(b_name=b_name):
            error = '新修改的书籍名称已存在'
        elif obj.b_name == b_name:
            error = '名称未修改'
        elif not b_name:
            error = '名称不能为空'
        if not error:
            # 新修改的名称不在数据库内,不为空,并且已经修改(和原来的名称不同)
            obj.b_name = b_name
            obj.pun_id = p_id
            obj.save()
            return redirect('/book_list/')

    return render(
        request, 'edit_book.html', {
            'error': error, 'obj': obj_list, 'pub_list': pub_list})


# 文件上传操作
def up_load(request):
    message = ""
    if request.method == 'POST':
        print(request.FILES)
        file_obj = request.FILES.get('file')
        print(file_obj.name)
        try:
            print(file_obj.content_type)
            print(file_obj.size)
            print(file_obj.charset)
            print(file_obj.content_type_extra)


        except Exception as e:
            print(e)
        with open(file_obj.name, 'wb') as f:
            for item in file_obj.chunks():
                f.write(item)
            message = '上传完成'
    return render(request, 'up_load.html',{'message':message})
