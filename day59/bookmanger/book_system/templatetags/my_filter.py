#!/usr/bin/env python
# -*- coding:utf-8 -*-

from django import template
from django.utils import safestring

register = template.Library()


@register.filter()
def mul(value, arg):
    try:
        return int(value) * int(arg)
    except TypeError:
        return ""


@register.filter()
def div(value, arg):
    try:
        return round(int(value) / int(arg), 3)
    except TypeError:
        return value


@register.filter(is_safe=True)
def show_a(value, arg):
    try:
        text = str(arg)
    except TypeError:
        return value
    html_text = "<a href='{}'>{}</a>".format(value, text)
    # return html_text
    return safestring.mark_safe(html_text)


@register.simple_tag()
def foreach(*args,**kwargs):
    return args


@register.inclusion_tag('page.html')
def page(num):
    return {"data":range(1,num+1)}

@register.inclusion_tag('dropdowm_list.html')
def sqr_list(num):
    return {"num":range(1,num+1)}