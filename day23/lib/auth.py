#!/usr/bin/env python        	
# -*- coding:utf-8 -*-	 
#@Project  : day23homework 
#@Author : Jack Deng       
#@Time   :  2019-04-28 22:56
#@File   : auth.py
import os
import sys

from db import glo
CURRENT_USER = glo.get_value('CURRENT_USER')

def auth(func):
    def inner(*args, **kwargs):
        print('进入装饰器此时用户名为')
        if CURRENT_USER:
            return func(*args, **kwargs)
        return func1()
    return inner

def func1():
    print('此时没有用户登录')
    return False