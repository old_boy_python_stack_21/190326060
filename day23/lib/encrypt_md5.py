#!/usr/bin/env python        	
# -*- coding:utf-8 -*-	 
#@Project  : day23homework 
#@Author : Jack Deng       
#@Time   :  2019-04-28 22:47
#@File   : encrypt_md5.py


import hashlib

def encrypt_md5(arg):
    """
    md5加密
    :param arg
    :return:
    """
    m = hashlib.md5()
    m.update(arg.encode('utf-8'))
    return m.hexdigest()