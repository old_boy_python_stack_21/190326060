# python day23

#### 1. 字符串格式化补充

```python
# 一般的格式化
msg = '我今年%s岁,叫做%s ,爱好%s' %(25,'邓益新','女')
print(msg)

msg1 = '我今年{}岁,叫做{} ,爱好是这个{}'.format(25,'邓益新','女')
print(msg1 )
```

字符串格式化补充

```python
#  %(关键字)s
msg = '我今年%(intt)s岁,叫做%(name)s ' %{'intt':25,'name':'邓益新'}    # 只能用字典
print(msg)

# v1 = "我是{0},年龄{1}".format('alex',19)
v1 = "我是{0},年龄{1}".format(*('alex',19,))
print(v1)

# v2 = "我是{name},年龄{age}".format(name='alex',age=18)    #关键字传参
v2 = "我是{name},年龄{age}".format(**{'name':'alex','age':18})
print(v2)

###########################
msg1 = '我今年{nain}岁,叫做{name} '.format(**{'nain':25,'name':'邓益新',})
msg2 = '我今年{nain}岁,叫做{name}'.format(nain= 25,name= '邓益新',)

```

#### 2.有序字典

```python
# 调用模块,字典有序
from collections import OrderedDict

info  = OrderedDict()
```

#### 3.反射提高你代码的逼格

```python
# 反射提高你代码的逼格

# 1.构造字典,避免重复的if else
method_dict = {'up':self.upload, 'down':self.download}
method = method_dict.get(action)
method()

# 2. 利用反射实现
method = getattr(self,action) # upload  # self.upload
method()

# 3.getattr也支持返回值为None
class Foo(object):
    def get(self):
        pass

obj = Foo()
# if hasattr(obj,'post'): 
#     getattr(obj,'post')

v1 = getattr(obj,'get',None) # ,反射值默认为None推荐  
print(v1)

v2 = getattr(obj,'get1573453',None) # 推荐
print(v2)
```

### 一.今日内容

#### 1.单例模式(23种设计模式之一)

无论实例化多少次，永远用的都是第一次实例化出的对象。

1. 单例模式标准

   ```python
   class Singleton(object):
       __instance = None
   
       def __new__(cls, *args, **kwargs):
           if not cls.instance :
               cls.instance = object.__new__(cls)
           return cls.instance
   
   obj1 = Singleton()
   obj2 = Singleton()
   print(obj1,obj2)
   
   # 注意 : 此时还不是最终状态,-->加锁
   ```

   单例模式例子

   ```python
   # 文件链接池
   
   class FileHelper(object):
       instance = None
       def __init__(self, path):
           self.file_object = open(path,mode='r',encoding='utf-8')   #网络链接过与费时,只允许一个链接访问
   
       def __new__(cls, *args, **kwargs):
           if not cls.instance:
               cls.instance = object.__new__(cls)
           return cls.instance
   
   obj1 = FileHelper('x')
   obj2 = FileHelper('x')
   
   ```

#### 2.模块导入

- 多次导入模块只加载一次--->单例模式

  ```python
  # 文件名  --> jd.py
  print(123)
  
  
  ###################################
  import jd # 第一次加载：会加载一遍jd中所有的内容。
  import jd # 由已经加载过，就不在加载。   # 只打印一次123
  print(456)
  
  
  # 重加载 importilb.reload
  import importlib
  import jd
  importlib.reload(jd)
  print(456)   # 打印2次123  和456
  ```

  **额外  :  **通过模块导入的特性也可以实现单例模式

  ```python
  # jd.py
  class Foo(object):
      pass
  
  obj = Foo()
  
  ############################
  # app.py
  import jd
  print(jd.obj)       # 单例模式
  ```

#### 3.日志操作loggin模块

1. 基本应用( 默认的,无法更改编码,win编码不同乱码,不推荐)

   ```python
   import logging
   
   logging.basicConfig(
       filename='cmdb1.log',
       format='%(asctime)s - %(name)s - %(levelname)s -%(module)s:  %(message)s',
       datefmt='%Y-%m-%d %H:%M:%S %p',
       level=logging.ERROR
   )
    #  默认的,无法更改编码,win编码不同乱码,不推荐
       
   logging.error(msg,exc_info=True)  # exc_info=True,打印错误日志是把栈信息打印出来
   ```

2. 日志处理的本质(Logger/FileHandler/Formatter)

   ```python
   import logging
   
   file_handler1 = logging.FileHandler('x2.log', 'a', encoding='utf-8')
   fmt1 = logging.Formatter(fmt="%(asctime)s - %(name)s - %(levelname)s -%(module)s:  %(message)s")
   file_handler1.setFormatter(fmt1)
   
   # file_handler2 = logging.FileHandler('x2.log', 'a', encoding='utf-8')
   # fmt2 = logging.Formatter(fmt="%(asctime)s:  %(message)s")
   # file_handler2.setFormatter(fmt2)
   
   logger = logging.Logger('xxxxxx', level=logging.ERROR)
   logger.addHandler(file_handler1)
   # logger.addHandler(file_handler2)
   
   
   logger.error('你好')
   
   ```

3. 推荐处理方式

   ```python
   import logging
   
   file_handler = logging.FileHandler(filename='x3.log', mode='a', encoding='utf-8',)
   logging.basicConfig(
       format='%(asctime)s - %(name)s - %(levelname)s -%(module)s:  %(message)s',
       datefmt='%Y-%m-%d %H:%M:%S %p',
       handlers=[file_handler,],
       level=logging.ERROR
   )
   
   logging.error('你好')
   
   ```

4. rz日志处理 + 分割

   ```python
   import logging
   from logging import handlers
   
   file_handler = handlers.TimedRotatingFileHandler(filename='x3.log', when='s', interval=5, encoding='utf-8')
   logging.basicConfig(
       format='%(asctime)s - %(name)s - %(levelname)s -%(module)s:  %(message)s',
       datefmt='%Y-%m-%d %H:%M:%S %p',
       handlers=[file_handler,],
       level=logging.ERROR
   )
   
   logging.error('你好')
   
   # when = ''取 S M H D W,不区分大小写
   
   ```

5. 在应用日志时，如果想要保留异常的堆栈信息。

   ```python
   logging.error('你好',exc_info=True)  # 即可
   ```

#### 4.项目结构(重点)

##### 1.脚本

特别小的py文件,单一功能,项目下就一个py文件就可以了

![1556457649559](day23notes .assets/1556457649559.png)

##### 2.单个可执行文件

- config-->配置文件

- db-->database数据文件

- lib-->公共功能-->分页/日志

- src-->功能

- app.py  或者  run.py  --->代码越少越好

  ![1556457817139](day23notes .assets/1556457817139.png)

##### 3.多个可执行文件

- bin--->多个可执行文件---theacher/student/admin --->代码越少越好

- config-->配置文件

- db-->database数据文件

- lib-->公共功能-->分页/日志

- src-->功能

  ![1556457924934](day23notes .assets/1556457924934.png)

  



