#!/usr/bin/env python        	
# -*- coding:utf-8 -*-	 
# @Project  : day23homework
# @Author : Jack Deng
# @Time   :  2019-04-28 22:36
# @File   : app.py

from src.run import run




if __name__ == '__main__':
    run()
