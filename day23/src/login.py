#!/usr/bin/env python        	
# -*- coding:utf-8 -*-	 
#@Project  : day23homework 
#@Author : Jack Deng       
#@Time   :  2019-04-28 22:51
#@File   : login.py

import os
import sys
from db.user import USER_DICT
from lib.encrypt_md5 import encrypt_md5






def login():
    """
    用户登录
    :return:
    """
    print('用户登录')
    while True:
        user = input('请输入用户名(N返回上一级)：')
        if user.upper() == 'N':
            return
        pwd = input('请输入密码：')
        if user not in USER_DICT:
            print('用户名不存在')
            continue

        encrypt_password = USER_DICT.get(user)
        if encrypt_md5(pwd) != encrypt_password:
            print('密码错误')
            continue
        print('登录成功')
        global CURRENT_USER
        CURRENT_USER = user
        return
