#!/usr/bin/env python        	
# -*- coding:utf-8 -*-	 
#@Project  : day23homework 
#@Author : Jack Deng       
#@Time   :  2019-04-28 22:37
#@File   : run.py
import os
import sys
from src.login import login
from src.register import register
from src.article_list import article_list


def run():
    """
    主函数
    :return:
    """
    print('=================== 系统首页 ===================')
    func_dict = {'1': register, '2': login, '3': article_list}
    while True:
        print('1.注册；2.登录；3.文章列表')
        choice = input('请选择序号：')
        if choice.upper() == 'N':
            return
        func = func_dict.get(choice)
        if not func:
            print('序号选择错误')
            continue
        func()

