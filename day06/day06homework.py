#!/usr/bin/env python
# -*- coding:utf-8 -*-
#1 .列举你了解的字典中的功能（字典独有）。
# 答：.keys()  获取字典所有键
# .values()   获取字典所有值
# .items()    获取字典所有键值对
#  .get(key)    获取键key对应的值
#  .update（)    更新字典的键值对，若键一致，重新赋值，若不存在这个键，就添加新键值对。

#2 . 列举你了解的集合中的功能（集合独有）
"""答： .add()   添加元素
        .update()    添加元素
        .discard()    删除集合的某元素
        .union()      求并集
        .difference()  求交集
        .intersection()  求差集，属于前面的集合但不属于后面的集合的元素

"""

#3 .列举你了解的可以转换为 布尔值且为False的值。
#答：0 ， '', [], (), {}, set()转换bool值时为False

#4 .请用代码实现
        #    info = {'name':'王刚蛋','hobby':'铁锤'}
#- 循环提示用户输入，根据用户输入的值为键去字典中获取对应的值并输出。
"""
info = {'name':'王刚蛋','hobby':'铁锤'}
while True:
    keys = input('请输入字典的键：')
    print(info.get(keys))
"""

#- 循环提示用户输入，根据用户输入的值为键去字典中获取对应的值并输出（如果key不存在，则获取默认“键不存在”，并输出）。
#  注意：无需考虑循环终止（写死循环即可）
"""
info = {'name':'王刚蛋','hobby':'铁锤'}
while True:
    keys = input('请输入字典的键：')
    if  not info.get(keys):
        print('键不存在')
        continue
    print(info.get(keys))
"""

#5 .请用代码验证 "name" 是否在字典的键中？
"""
info = {'name':'王刚蛋','hobby':'铁锤','age':'18','其他':'100个键值对'}
message = '"name" 不在字典的键中'
if 'name' in info:
    message = '"name" 在字典的键中'
print(message)
"""

#6 .请用代码验证 "alex" 是否在字典的值中？
"""
info = {'name':'王刚蛋','hobby':'铁锤','age':'18','其他省略':'100个键值对'}
message = '"alex" 不在字典的值中'
if "alex" in list(info.values()):
    message = '"alex" 在字典的值中'
print(message)
"""

#7 .有如下
v1 = {'武沛齐','李杰','太白','景女神'}
v2 = {'李杰','景女神'}
# - 请得到 v1 和 v2 的交集并输出
print(v1.intersection(v2))
# - 请得到 v1 和 v2 的并集并输出
print(v1.union(v2))
# - 请得到 v1 和 v2 的 差集并输出
print(v1.difference(v2))
# - 请得到 v2 和 v1 的 差集并输出
print(v2.difference(v1))

#8 .循环提示用户输入，并将输入内容追加到列表中（如果输入N或n则停止循环）
"""
list = []
while True:
    item = input('请输入内容：')
    if item.upper() == 'N':
        break
    list.append(item)
print(list)
"""

#9 .循环提示用户输入，并将输入内容添加到集合中（如果输入N或n则停止循环）
"""
set1 = set()
while True:
    item = input('请输入内容：')
    if item.upper() == 'N':
        break
    set1.add(item)
print(set1)
"""

#10 .写代码实现
"""
v1 = {'alex','武sir','肖大'}
v2 = []
# 循环提示用户输入，如果输入值在v1中存在，则追加到v2中，如果v1中不存在，则添加到v1中。（如果输入N或n则停止循环）
while True:
    x = input('请输入内容：')
    if x.upper() == 'N':
        break
    if x in v1:
        v2.append(x)
    else:
        v1.add(x)
print(v1,v2)
"""

#11 .判断以下值那个能做字典的key ？那个能做集合的元素？
#  1       都能做
#  -1      都能做
#  ""      都能做
#  None    都能做
#  [1,2]   可变类型,不能做
#  (1,)     是元组，都可以做
#  {11,22,33,4}     可变类型，不能做
#  {'name':'wupeiq','age':18}     可变类型,不能做

#12 .is 和 == 的区别？
# 答：== 表示比较，判断两者的值是否相等。
#      is判断两者的内存地址是否相等。

#13 .type使用方式及作用？
# 答：type(),返回值为括号里的对象的数据类型。t =type()

#14 .d的使用方式及作用？
# 答：id(),返回值为括号里的对象所指向的内存地址。t =id()

#15 .看代码写结果并解释原因
# v1 = {'k1':'v1','k2':[1,2,3]}
# v2 = {'k1':'v1','k2':[1,2,3]}
#
# result1 = v1 == v2
# result2 = v1 is v2
# print(result1)          # v1的值和v2的值相等，结果为True
# print(result2)          # v1指向的内存地址和v2指向的内存地址不同，结果为False

#16 .看代码写结果并解释原因
# v1 = {'k1':'v1','k2':[1,2,3]}
# v2 = v1                               # 指向关系相同，指向的内存地址就是同一个
#
# result1 = v1 == v2
# result2 = v1 is v2
# print(result1)      #值相同，输出True
# print(result2)      #指向的内存地址是同一个，输出True

#17 .看代码写结果并解释原因
# v1 = {'k1':'v1','k2':[1,2,3]}
# v2 = v1                # 指向关系相同，指向的内存地址就是同一个。
#
# v1['k1'] = 'wupeiqi'
# print(v2)            #引用共享，输出改动后的字典{'k1':'wupeiqi','k2':[1,2,3]}

#18 .看代码写结果并解释原因
# v1 = '人生苦短，我用Python'
# v2 = [1,2,3,4,v1]            #这里的v1表示这种引用关系，指向'人生苦短，我用Python'对应的内存地址
#
# v1 = "人生苦短，用毛线Python"   #重新赋值，改变指向，v1指向另一个内存地址。v2不变。
#
# print(v2)    #输出[1,2,3,4,'人生苦短，我用Python']

#19 .看代码写结果并解释原因
# info = [1,2,3]
# userinfo = {'account':info, 'num':info, 'money':info}      # 指向info对应的内存地址
#
# info.append(9)
# print(userinfo)        #info对应的内存地址添加，同样改变。
#                        #输出{'account':[1,2,3,9], 'num':[1,2,3,9], 'money':[1,2,3,9]}
#
# info = "题怎么这么多"     #info重新赋值，指向关系改变。userinfo不变。
# print(userinfo)          #输出{'account':[1,2,3], 'num':[1,2,3], 'money':[1,2,3]}

#20 .看代码写结果并解释原因
# info = [1,2,3]
# userinfo = [info,info,info,info,info]          #info表指向关系，指向同一个内存地址。
#
# info[0] = '不仅多，还特么难呢'            #info更新，内存地址还是那个
# print(info,userinfo)                   #输出改变后的['不仅多，还特么难呢',2,3]

#21 .看代码写结果并解释原因
# info = [1,2,3]
# userinfo = [info,info,info,info,info]
#
# userinfo[2][0] = '闭嘴'         #对userinfo[2][0]重新赋值，内存地址变更。info指向关系不变
# print(info,userinfo)            #输出['闭嘴',2,3]   [['闭嘴',2,3],['闭嘴',2,3],['闭嘴',2,3],['闭嘴',2,3],['闭嘴',2,3]]

#22 .看代码写结果并解释原因
# info = [1, 2, 3]           # info指向一内存地址
# user_list = []
# for item in range(10):
#     user_list.append(info)  #user_list = [10个info作为列表元素]，指向内存地址同一个
#
# info[1] = "是谁说Python好学的？"       #info的指向关系不变，
#
# print(user_list)          #[[1, "是谁说Python好学的？", 3],``````]

#23 .看代码写结果并解释原因
# data = {}                       # 确定data的指向的内存地址不变
# for i in range(10):
#     data['user'] = i
# print(data)                    # 一直更新'user'键对应的值，输出{'user'：9}

#24 .看代码写结果并解释原因
# data_list = []
# data = {}                                # 确定data的指向的内存地址不变
# for i in range(10):
#     data['user'] = i
#     data_list.append(data)            # data的指向的内存地址不变
# print(data_list)                   # [{'user':9},{'user':9}, ``` ```{'user':9}]

#25 .看代码写结果并解释原因
# data_list = []
# for i in range(10):
#     data = {}
#     data['user'] = i             # data的指向的内存地址因每次重新赋值而改变
#     data_list.append(data)
# print(data)                      #[{'user':0},[{'user':1},{'user':2}, ``` ```{'user':9}]]