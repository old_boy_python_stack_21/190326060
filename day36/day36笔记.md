## 第九章 并发编程

### 9.1 基础知识

#### 9.1.1 操作系统发展史

##### 9.1.1.1 人机矛盾

- cpu利用率低

##### 9.1.1.2 磁带存储+批处理

- 降低数据的读取时间
- 提高cpu利用率

##### 9.1.1.3 多道操作系统

- 数据隔离
- 时空复用
- 能够在一个任务遇到io操作时，主动把cpu让出来给其他任务使用。
- 任务切换过程也占用时间（操作系统来进行切换）

##### 9.1.1.4 分时操作系统

给时间分片，让多个任务轮流使用cpu

- 时间分片
  - cpu的轮转
  - 每个程序分配一个时间片
- 切换占用了时间，降低了cpu的利用率
- 提高了用户体验

##### 9.1.1.5 分时操作系统+多道操作系统

多个程序一起在计算机中执行

- 一个程序如果遇到了io操作，就切出去让出cpu
- 一个程序没有遇到io操作，但时间片到了，就让出cpu

补充：python中的分布式框架celery

#### 9.1.2 进程

##### 9.1.2.1 定义

运行中的程序就是一个进程。

##### 9.1.2.2 程序和进程的区别

程序：是一个文件。

进程：这个文件被cpu运行起来了

- 进程和程序之间的区别

  - 运行的程序就是一个进程

- 进程的调度 ：由操作系统完成

-  三状态 ：

  - 就绪 -system call-><-时间片到了- 运行 -io-> 阻塞-IO结束->就绪

  ​     阻塞 影响了程序运行的效率

##### 9.1.2.3 基础知识

- 进程是计算机中最小的资源分配单位
- 进程在操作系统中的唯一标识符：pid
- 操作系统调度进程的算法
  - 短作业优先算法
  - 先来先服务算法
  - 时间片轮转算法
  - 多级反馈算法

##### 9.1.2.4 并发和并行区别

并行：两个程序，两个cpu，每个程序分别占用一个cpu，自己执行自己的。

并发：多个程序，一个cpu，多个程序交替在一个cpu上执行，看起来在同时执行，但实际上仍然是串行。

#### 9.1.3 同步和异步

同步：一个动作执行时，要想执行下一个动作，必须将上一个动作停止。

异步：一个动作执行的同时，也可以执行另一个动作

#### 9.1.4 阻塞和非阻塞

阻塞：cpu不工作

非阻塞：cpu工作

同步阻塞：如conn.revc()

同步非阻塞：调用一个func()函数，函数内没有io操作

异步非阻塞：把func()函数(无io操作)放到其他任务里去执行，自己执行自己的任务。

异步阻塞：

####  9.1.5并发并行

- 并发 ：并发是指多个程序 公用一个cpu轮流使用
-  并行 ：多个程序 多个cpu 一个cpu上运行一个程序，
  - 在一个时间点上看，多个程序同时在多个cpu上运行
- 单核 并发
  多核 多个程序跑在多个cpu上，在同一时刻 并行
  10个进程 ：
      可以利用多核  并行
      也有并发的成分

### 9.2 进程和线程

#### 9.2.1进程是计算机中最小的资源分配单位

- 进程是数据隔离的
  - 歪歪 陌陌 飞秋 qq 微信 腾讯视频
- 一个进程

​        1.和一个人通信2.一边缓存 一边看另一个电影的直播

#### 9.2.2进程

- 创建进程 时间开销大

- 销毁进程 时间开销大

- 进程之间切换 时间开销大

  ```python
  如果两个程序 分别要做两件事儿
      起两个进程
  如果是一个程序 要分别做两件事儿
      视频软件
          下载A电影
          下载B电影
          播放C电影
      启动三个进程来完成上面的三件事情，但是开销大
  ```



#### 9.2.4进程模块

```python
from multiprocessing import Process
# pid   process id
# ppid  parent process id
# os.getpid()    /   os.getppid()  找到进程的id  ,找到父进程id

```

1. 在pycharm中启动的所有py程序都是pycharm的子进程

2. 父进程 在父进程中创建子进程

   ```python
   import os
   import time
   from multiprocessing import Process
   
   def func():
       print('start',os.getpid())
       time.sleep(1)
       print('end',os.getpid())
   
   if __name__ == '__main__':
       p = Process(target=func)
       p.start()   异步 调用开启进程的方法 但是并不等待这个进程真的开启
       print('main :',os.getpid())
   ```

   ```python
   import os
   import time
   from multiprocessing import Process
   
   def eat():
       print('start eating',os.getpid())
       time.sleep(1)
       print('end eating',os.getpid())
   
   def sleep():
       print('start sleeping',os.getpid())
       time.sleep(1)
       print('end sleeping',os.getpid())
   
   if __name__ == '__main__':
       p1 = Process(target=eat)    # 创建一个即将要执行eat函数的进程对象
       p1.start()                  # 开启一个进程
       p2 = Process(target=sleep)  # 创建一个即将要执行sleep函数的进程对象
       p2.start()                  # 开启进程
       print('main :',os.getpid())
   ```

##### 9.2.4.1注意win和linux系统在创建进程是的区别

1. 操作系统创建进程的方式不同
2. windows操作系统执行开启进程的代码
3. 实际上新的子进程需要通过import父进程的代码来完成数据的导入工作
   - 所以有一些内容我们只希望在父进程中完成，就写在if `__name__` == `__main__`:缩进处即可

##### 8.2.4.2父进程和子进程的关系

```python
import os
import time
from multiprocessing import Process
def func():
    print('start',os.getpid())
    time.sleep(10)
    print('end',os.getpid())
    
if __name__ == '__main__':
    p = Process(target=func)
    p.start()   #  异步 调用开启进程的方法 但是并不等待这个进程真的开启
    print('main :',os.getpid())
    #  主进程没结束 ：等待子进程结束
    #  主进程负责回收子进程的资源
    #  如果子进程执行结束，父进程没有回收资源，那么这个子进程会变成一个僵尸进程
```

1. 主进程的结束逻辑
   1. 主进程的代码结束
   2. 所有的子进程结束
   3. 给子进程回收资源
   4. 主进程结束

8.2.4.3 Process模块的join方法

1. join方法 ：阻塞，直到子进程结束就结束

   ```python
   # 开启10个进程，给公司的5000个人发邮件，发送完邮件之后，打印一个消息“5000封邮件已发送完毕”
   
   import time
   import random
   from multiprocessing import Process
   def send_mail(a):
       time.sleep(random.random())
       print('发送了一封邮件',a)
   
   if __name__ == '__main__':
       l = []
       for i in range(10):
           p = Process(target=send_mail,args=(i,))
           p.start()   #  # 异步 非阻塞
           l.append(p)
       print(l)
       for p in l:p.join()     # # 同步 阻塞 直到p对应的进程结束之后才结束阻塞  
       print('5000封邮件已发送完毕')
   ```

##### 9.2.4.3 总结

1. 开启一个进程

   - 函数名(参数1,参数2)
   - from multiprocessing import Process
   - p = Process(target=函数名,args=(参数1,参数2))
   - p.start()

2. 父进程  和 子进程

3. 父进程会等待着所有的子进程结束之后才结束

   - 为了回收资源

4. 进程开启的过程中windows和 linux/ios之间的区别

   - 开启进程的过程需要放在if `__name__` == '`__main__`'下

   ​        windows中 相当于在子进程中把主进程文件又从头到尾执行了一遍
   ​            除了放在if `__name__` == '`__main__`'下的代码
   ​        linux中 不执行代码,直接执行调用的func函数

5. join方法

   ​    把一个进程的结束事件封装成一个join方法
   ​    执行join方法的效果就是 阻塞直到这个子进程执行结束就结束阻塞

```python
#  在多个子进程中使用join
p_l= []
for i in range(10):
    p = Process(target=函数名,args=(参数1,参数2))
    p.start()
    p_l.append(p)
for p in p_l:p.join()
# 所有的子进程都结束之后要执行的代码写在这里
```
##### 9.2.4.4 `__name__`补充讲解

```python
if __name__ == '__main__':
    # 控制当这个py文件被当作脚本直接执行的时候，就执行这里面的代码
    # 当这个py文件被当作模块导入的时候，就不执行这里面的代码
    print('hello hello')
# __name__ == '__main__'
    # 执行的文件就是__name__所在的文件
# __name__ == '文件名'
    # __name__所在的文件被导入执行的时候
```

##### 9.2.4.5协程相关

1. 并发编程

   - 不会有大量的例子和习题

   ​        1. 进程 在我们目前完成的一些项目里是不常用到的
   ​        2.线程 后面的爬虫阶段经常用
   ​            前端的障碍
   ​        3.协程
   ​            异步的框架 异步的爬虫模块

2. 为什么进程用的不多，但是还要来讲

   ​        1.你可能用不到，但是未来你去做非常复杂的数据分析或者是高计算的程序
   ​        2.进程和线程的很多模型很多概念是基本一致的

#### 9.2.5进程模块进阶

1. 线程

   - 线程是进程的一部分，每个进程中至少有一个线程, 是能被CPU调度的最小单位 .   

   - 一个进程中的多个线程是可以共享这个进程的数据的  —— 数据共享
   - 线程的创建、销毁、切换 开销远远小于进程  —— 开销小

2. multiprocessing 进程

   1. 使用方法 : p = Process(target=函数名,args=(参数1,))

   2. 如何创建一个进程对象

      - 对象和进程之间的关系
      - 进程对象和进程并没有直接的关系
      - 只是存储了一些和进程相关的内容
      - 此时此刻，操作系统还没有接到创建进程的指令

   3. 如何开启一个进程

      - 通过p.start()开启了一个进程--这个方法相当于给了操作系统一条指令
      -  start方法 的 非阻塞和异步的特点
      -  在执行开启进程这个方法的时候
      -  我们既不等待这个进程开启，也不等待操作系统给我们的响应
      -  这里只是负责通知操作系统去开启一个进程
      -  开启了一个子进程之后，主进程的代码和子进程的代码完全异步

   4. 父进程和子进程之间的关系

      - 父进程会等待子进程结束之后才结束
      - 为了回收子进程的资源

   5. 不同操作系统中进程开启的方式

      -  windows 通过（模块导入）再一次执行父进程文件中的代码来获取父进程中的数据
        -  所以只要是不希望被子进程执行的代码，就写在if `__name__` == `__main__`下
      -  因为在进行导入的时候父进程文件中的if `__name__` == `__main__`
      - linux/ios
        -  正常的写就可以，没有if __name__ == '__main__'这件事情了

      ​    5.如何确认一个子进程执行完毕
      ​        Process().join方法
      ​        开启了多个子进程，等待所有子进程结束

#### 9.2.6守护进程

```python
# 有一个参数可以把一个子进程设置为一个守护进程
if __name__ == '__main__':
    p = Process(target=son1,args=(1,2))
    p.daemon = True    # daemon = True   
    p.start()      # 把p子进程设置成了一个守护进程
    p2 = Process(target=son2)
    p2.start()
    time.sleep(2)
# 守护进程是随着主进程的代码结束而结束的
    # 生产者消费者模型的时候
    # 和守护线程做对比的时候
# 所有的子进程都必须在主进程结束之前结束，由主进程来负责回收资源
```

1. 会随着主进程的结束而结束。

   主进程创建守护进程

   　　其一：守护进程会在主进程代码执行结束后就终止

   　　其二：守护进程内无法再开启子进程,否则抛出异常：AssertionError: daemonic processes are not allowed to have children

   注意：进程之间是互相独立的，主进程代码运行结束，守护进程随即终止

#### 9.2.7   terminate()方法

```python
#  p.terminate():强制终止进程p，不会进行任何清理操作，如果p创建了子进程，该子进程就成了僵尸进程，使用该方法需要特别小心这种情况。如果p还保存了一个锁那么也将不会被释放，进而导致死锁
```

```python
import time
from multiprocessing import Process

def son1():
    while True:
        print('is alive')
        time.sleep(0.5)

if __name__ == '__main__':
    p = Process(target=son1)
    p.start()      # 异步 非阻塞
    print(p.is_alive())
    time.sleep(1)
    p.terminate()   # 异步的 非阻塞
    print(p.is_alive())   # 进程还活着 因为操作系统还没来得及关闭进程
    time.sleep(0.01)
    print(p.is_alive())   # 操作系统已经响应了我们要关闭进程的需求，再去检测的时候，得到的结果是进程已经结束了
```

### 9.3 Process模块面向对象

```python
# Process类
# 开启进程的方式
    面向函数
        def 函数名:要在子进程中执行的代码
        p = Process(target= 函数名,args=(参数1，))
    面向对象
        class 类名(Process):
            def __init__(self,参数1，参数2):   如果子进程不需要参数可以不写
                self.a = 参数1
                self.b = 参数2
                super().__init__()
            def run(self):      # 必须为run方法
                要在子进程中执行的代码
        p = 类名(参数1，参数2)
    Process提供的操作进程的方法
        p.start() 开启进程      异步非阻塞
        p.terminate() 结束进程  异步非阻塞

        p.join()     同步阻塞
        p.isalive()  获取当前进程的状态
        daemon = True 设置为守护进程，守护进程永远在主进程的代码结束之后自动结束
```

####  9.3.1 并发和加锁

- 实现并发完成了许多,但如果当多个进程使用同一份数据资源的时候，就会引发数据安全或顺序混乱问题。 

1. 在这里我们通过给程序加锁(Lock),这种情况虽然使用加锁的形式实现了顺序的执行，但是程序又重新变成串行了，这样确实会浪费了时间，却保证了数据的安全。 

   ```python
   # 由并发变成了串行,牺牲了运行效率,但避免了竞争
   import os
   import time
   import random
   from multiprocessing import Process,Lock
   
   def work(lock,n):
       lock.acquire()            # 
       print('%s: %s is running' % (n, os.getpid()))
       time.sleep(random.random())
       print('%s: %s is done' % (n, os.getpid()))
       lock.release()            #
   if __name__ == '__main__':
       lock=Lock()
       for i in range(3):
           p=Process(target=work,args=(lock,i))
           p.start()
           
    # 在主进程中实例化 lock = Lock()
   # 把这把锁传递给子进程
   # 在子进程中 对需要加锁的代码 进行 with lock：
       # with lock相当于lock.acquire()和lock.release()
   # 在进程中需要加锁的场景
       # 共享的数据资源（文件、数据库）
       # 对资源进行修改、删除操作
   # 加锁之后能够保证数据的安全性 但是也降低了程序的执行效率
   
   
   
   # lock.acquire()  /  lock.release() 支持上下文.可以with lock:
   ```

#### 9.3.2进程间的通信**IPC**(Inter-Process Communication)

**队列 : (Queue  --> 先进先出)**

1. 进程间是数据隔离的

2. 创建共享的进程队列，Queue是多进程安全的队列，可以使用Queue实现多进程之间的数据传递。 

3. ```python
   # Queue基于 天生就是数据安全的
       # 文件家族的socket pickle lock
   # pipe 管道(不安全的) = 文件家族的socket pickle
   # 队列 = 管道 + 锁
   ```

4. Queue的使用

   ```python
   import queue
   
   from multiprocessing import Queue
   q = Queue(5)
   q.put(1)
   q.put(2)
   q.put(3)
   q.put(4)
   q.put(5)   # 当队列为满的时候再向队列中放数据 队列会阻塞
   print('5555555')
   try:
       q.put_nowait(6)  # 当队列为满的时候再向队列中放数据 会报错并且会丢失数据.推荐使用
   except queue.Full:
       pass
   print('6666666')
   
   print(q.get())
   print(q.get())
   print(q.get())   # 在队列为空的时候会发生阻塞
   print(q.get())   # 在队列为空的时候会发生阻塞
   print(q.get())   # 在队列为空的时候会发生阻塞
   try:
       print(q.get_nowait())   # 在队列为空的时候 直接报错  ,推荐使用
   except queue.Empty:pass
   
   ```

   我们记住使用get_nowait()  和  put_nowait()   

#### 9.3.3多进程实现并发socket服务端

```python
import socket
from multiprocessing import Process
def chat(conn):
    while True:
        try:
            ret = conn.recv(1024).decode('utf-8')
            conn.send(ret.upper().encode('utf-8'))
        except ConnectionResetError:
            break
a
if __name__ == '__main__':
    sk = socket.socket()
    sk.bind(('127.0.0.1', 9000))
    sk.listen()
    while True:
        conn,_ = sk.accept()
        Process(target=chat,args=(conn,)).start()
```

#### 9.3.4 ipc机制

- 进程之间相互通信
- ipc机制-->队列   管道
- 第三方工具(软件)提供给我们的IPC机制
  - redis
  - memcache
  - kafka等等
  - 并发需求   ,高可用  ,断电保存数据

### 9.4生产者和消费者模式

**在并发编程中使用生产者和消费者模式能够解决绝大多数并发问题。该模式通过平衡生产线程和消费线程的工作能力来提高程序的整体处理数据的速度。**

- 一个进程就是一个生产者,一个进程就是一个消费者

- 队列:生产者和消费者之间的容器就是队列

  什么是生产者消费者模式

  **生产者消费者模式是通过一个容器来解决生产者和消费者的强耦合问题。生产者和消费者彼此之间不直接通讯，而通过阻塞队列来进行通讯，所以生产者生产完数据之后不用等待消费者处理，直接扔给阻塞队列，消费者不找生产者要数据，而是直接从阻塞队列里取，阻塞队列就相当于一个缓冲区，平衡了生产者和消费者的处理能力。**

- 在生产者在生产完后就结束了,往队列中再发一个结束信号，这样消费者在接收到结束信号后就可以break出死循环。 
  结束信号可以生产者发送,也可以主进程在生产者进程结束后发送.

- 但若是有多个生产者和消费者时 , 就必须得在多个升舱者全部生产完毕后,才能发送结束信号,并且有多少个消费者,就需要发送几次结束信号.

- 具体描述

  ```py
  什么是生产者消费者模型
      把一个产生数据并且处理数据的过程解耦
  	让生产的数据的过程和处理数据的过程达到一个工作效率上的平衡
      中间的容器，在多进程中我们使用队列或者可被join的队列，做到控制数据的量
          当数据过剩的时候，队列的大小会控制这生产者的行为
          当数据严重不足的时候，队列会控制消费者的行为
          并且我们还可以通过定期检查队列中元素的个数来调节生产者消费者的个数
  ```

  

#### 9.4.1 共享队列(**JoinableQueue([maxsize])**  

我们使用共享队列,创建可连接的共享进程队列。这就像是一个Queue对象，但队列允许项目的使用者通知生产者项目已经被成功处理。通知进程是使用共享的信号和条件变量来实现的。  

```python
"""JoinableQueue的实例p除了与Queue对象相同的方法之外，还具有以下方法：

q.task_done() 
使用者使用此方法发出信号，表示q.get()返回的项目已经被处理。如果调用此方法的次数大于从队列中删除的项目数量，将引发ValueError异常。

q.join() 
生产者将使用此方法进行阻塞，直到队列中所有项目均被处理。阻塞将持续到为队列中的每个项目均调用q.task_done()方法为止。 
下面的例子说明如何建立永远运行的进程，使用和处理队列上的项目。生产者将项目放入队列，并等待它们被处理。"""
```

```python
from multiprocessing import Process,JoinableQueue
import time,random,os
def consumer(q):
    while True:
        res=q.get()
        time.sleep(random.randint(1,3))
        print('\033[45m%s 吃 %s\033[0m' %(os.getpid(),res))
        q.task_done() #向q.join()发送一次信号,证明一个数据已经被取走了

def producer(name,q):
    for i in range(10):
        time.sleep(random.randint(1,3))
        res='%s%s' %(name,i)
        q.put(res)
        print('\033[44m%s 生产了 %s\033[0m' %(os.getpid(),res))
    q.join() #生产完毕，使用此方法进行阻塞，直到队列中所有项目均被处理。


if __name__ == '__main__':
    q=JoinableQueue()
    #生产者们:即厨师们
    p1=Process(target=producer,args=('包子',q))
    p2=Process(target=producer,args=('骨头',q))
    p3=Process(target=producer,args=('泔水',q))

    #消费者们:即吃货们
    c1=Process(target=consumer,args=(q,))
    c2=Process(target=consumer,args=(q,))
    c1.daemon=True
    c2.daemon=True

    #开始
    p_l=[p1,p2,p3,c1,c2]
    for p in p_l:
        p.start()

    p1.join()
    p2.join()
    p3.join()
    print('主') 
    
    #主进程等--->p1,p2,p3等---->c1,c2
    #p1,p2,p3结束了,证明c1,c2肯定全都收完了p1,p2,p3发到队列的数据
    #因而c1,c2也没有存在的价值了,不需要继续阻塞在进程中影响主进程了。应该随着主进程的结束而结束,所以设置成守护进程就可以了。
```

#### 9.4.2 使用manager 来进程间互相通信

```python
from multiprocessing import Manager,Process,Lock

def func(dic,lock):
    with lock:
        dic['count'] -= 1

if __name__ == '__main__':
    # m = Manager()
    with Manager() as m:
        l = Lock()
        dic = m.dict({'count':100})
        p_l = []
        for i in range(100):
            p = Process(target=func,args=(dic,l))
            p.start()
            p_l.append(p)
        for p in p_l:p.join()
        print(dic)


# mulprocessing中有一个manager类
# 封装了所有和进程相关的 数据共享 数据传递
# 相关的数据类型
# 但是对于 字典 列表这一类的数据操作的时候会产生数据不安全
# 需要加锁解决问题，并且需要尽量少的使用这种方式
```

### 9.5进程和线程的区别



#### 9.5 线程

#### 9.5.1线程

1. 是进程中的一部分，每一个进程中至少有一个线程

- 进程是计算机中最小的资源分配单位（进程是负责圈资源）
- 线程是计算机中能被CPU调度的最小单位 （线程是负责执行具体代码的）

1. 开销

   - 线程的创建，也需要一些开销（一个存储局部变量的结构，记录状态）
   - 创建、销毁、切换开销远远小于进程

2. python中的线程比较特殊，所以进程也有可能被用到

   - 进程 ：数据隔离 开销大 同时执行几段代码
   - 线程 ：数据共享 开销小 同时执行几段代码

3. **线程**：**开销小 数据共享 是进程的一部分，不能独立存在 本身可以利用多核**

4. cpython解释器 不能实现多线程利用多核

   **原因 :** 锁 ：GIL 全局解释器锁

   ```python
   #     保证了整个python程序中，只能有一个线程被CPU执行
   """原因：cpython解释器中特殊的垃圾回收机制
   GIL锁导致了线程不能并行，可以并发
   所以使用所线程并不影响高io型的操作
   只会对高计算型的程序由效率上的影响
   遇到高计算 ： 多进程 + 多线程
                 分布式"""
   ```

5. GIL锁
           全局解释器锁
           cpython解释器中的机制
           导致了在同一个进程中多个线程不能同时利用多核 —— python的多线程只能是并发不能是并行

#### 9.5.2 threading类创建线程

1. multiprocessing 是完全仿照这threading的类写的,方法几乎一致

   ```python
   import os
   import time
   from threading import Thread
   multiprocessing 是完全仿照这threading的类写的
   def func():
       print('start son thread')
       time.sleep(1)
       print('end son thread',os.getpid())
     #    启动线程 start
   Thread(target=func).start()
   print('start',os.getpid())
   time.sleep(0.5)
   print('end',os.getpid())
   ```

2. 开启多个子线程

   ```python
   开启多个子线程
   def func(i):
       print('start son thread',i)
       time.sleep(1)
       print('end son thread',i,os.getpid())
   #
   for i in range(10):
       Thread(target=func,args=(i,)).start()
   print('main')
    # 主线程什么时候结束？等待所有子线程结束之后才结束
   #  主线程如果结束了，主进程也就结束了
   ```

3. join方法

   ```python
   # join方法  阻塞 直到子线程执行结束
   # def func(i):
   #     print('start son thread',i)
   #     time.sleep(1)
   #     print('end son thread',i,os.getpid())
   # t_l = []
   # for i in range(10):
   #     t = Thread(target=func,args=(i,))
   #     t.start()
   #     t_l.append(t)
   # for t in t_l:t.join()
   # print('子线程执行完毕')
   ```

4. 使用面向对象的方式启动线程

   ```python
   class MyThread(Thread):
       def __init__(self,i):
           self.i = i
           super().__init__()
       def run(self):
           print('start',self.i,self.ident)
           time.sleep(1)
           print('end',self.i)
    # 线程就不用了main 了
   for i in range(10):
       t = MyThread(i)
       t.start()
       print(t.ident)
   ```

5. 线程里的一些其他方法

   ```python
   from threading import current_thread,enumerate,active_count
   def func(i):
       t = current_thread()
       print('start son thread',i,t.ident)
       time.sleep(1)
       print('end son thread',i,os.getpid())
   
   t = Thread(target=func,args=(1,))
   t.start()
   print(t.ident)
   print(current_thread().ident)   #  水性杨花 在哪一个线程里，current_thread()得到的就是这个当前线程的信息
   print(enumerate())  # 返回一个包含正在运行的线程的list。正在运行指线程启动后、结束前，不包括启动前和终止后的线程。
   print(active_count())   =====len(enumerate())  # 返回正在运行的线程数量，与len(threading.enumerate())有相同的结果。
   
   
   ```

6. terminate 结束进程
   在线程中不能从主线程结束一个子线程

7. 守护线程

   ```python
   import time
   from threading import Thread
   def son1():
       while True:
           time.sleep(0.5)
           print('in son1')
   def son2():
       for i in range(5):
           time.sleep(1)
           print('in son2')
   t =Thread(target=son1)
   t.daemon = True
   t.start()
   Thread(target=son2).start()
   time.sleep(3)
   守护线程一直等到所有的非守护线程都结束之后才结束
   除了守护了主线程的代码之外也会守护子线程
   小绿本 ：p38 ：34题
   
   ```

   **无论是进程还是线程，都遵循：守护xx会等待主xx运行完毕后被销毁。****需要强调的是：运行完毕并非终止运行**

   ```
   #1.对主进程来说，运行完毕指的是主进程代码运行完毕
   #2.对主线程来说，运行完毕指的是主线程所在的进程内所有非守护线程统统运行完毕，主线程才算运行完毕
   ```

   ```
   #1 主进程在其代码结束后就已经算运行完毕了（守护进程在此时就被回收）,然后主进程会一直等非守护的子进程都运行完毕后回收子进程的资源(否则会产生僵尸进程)，才会结束，
   #2 主线程在其他非守护线程运行完毕后才算运行完毕（守护线程在此时就被回收）。因为主线程的结束意味着进程的结束，进程整体的资源都将被回收，而进程必须保证非守护线程都运行完毕后才能结束
   ```

#### 9.5.3 总结

- threading模块
  
  -  创建线程 ：面向函数 面向对象
  
-  线程中的几个方法：
         1. 属于线程对象t   的 方法 `.start(),t.join()`
     
      2. 守守护线程 `t.daemon = True` 等待所有的非守护子线程都结束之后才结束
     
      3. 非守护线程不结束，主线程也不结束
                主线程结束了，主进程也结束
                 结束顺序 ：`  非守护线程结束 -->主线程结束-->主进程结束-->主进程结束 --> 守护线程也结束`
     
      4. threading模块的函数 ：
                   1.  current_thread 在哪个线程中被调用，就返回当前线程的对象
                   2.  活着的线程，包括主线程
                   3.  enumerate 返回当前活着的线程的对象列表
                4.  active_count 返回当前或者的线程的个数 ,`-->len(enumerate())测试`
     
      5. 进程和线程的效率差，线程的开启、关闭、切换效率更高
     
      6. 线程间的数据共享的效果
     
            ```python
            #   += -= *= /= ，多个线程对同一个文件进行写操作,线程间的数据不是很安全
            ```
     
            

### 9.6互斥锁 和 递归锁

1. 线程中会产生数据不安全,(共享内存)

2. ```python
   # 即便是线程 即便有GIL 也会出现数据不安全的问题
       # 1.操作的是全局变量
       # 2.做一下操作
           # += -= *= /+ 先计算再赋值才容易出现数据不安全的问题
           # 包括 lst[0] += 1  dic['key']-=1
   ```

3. 从threading类中也有锁lock,保护程序间的数据安全`---->互斥锁`

   ```python
   # 互斥锁是锁中的一种:在同一个线程中，不能连续acquire多次
   # from threading import Lock
   # lock = Lock()
   # lock.acquire()
   # print('*'*20)
   # lock.release()
   # lock.acquire()
   # print('-'*20)
   # lock.release()
   ```

4. 单例模式的加锁形式

   ```python
   from threading import Lock
   class A:
       __instance = None
       lock = Lock()
   
       def __new__(cls, *args, **kwargs):
           with cls.lock:
               if not cls.__instance:
                   time.sleep(0.1)
                   cls.__instance = super().__new__(cls)
           return cls.__instance
   ```

5. 互斥锁在多次acquire 后,会出现死锁现象,我们引入了递归锁来解决

   ```python
   # 锁
       # 互斥锁
           # 在一个线程中连续多次acquire会死锁
       # 递归锁
           # 在一个线程中连续多次acquire不会死锁
       # 死锁现象
           # 死锁现象是怎么发生的？
               # 1.有多把锁，一把以上
               # 2.多把锁交替使用
       # 怎么解决
           # 递归锁 —— 将多把互斥锁变成了一把递归锁
               # 快速解决问题
               # 效率差
           # ***递归锁也会发生死锁现象，多把锁交替使用的时候
           # 优化代码逻辑
               # 可以使用互斥锁 解决问题
               # 效率相对好
               # 解决问题的效率相对低
   ```

6. 递归锁

   ```python
    # 在同一个线程中，可以连续acuqire多次不会被锁住-->实际是把万能钥匙
   
   # 递归锁：
   # 好 ：在同一个进程中多次acquire也不会发生阻塞
   # 不好 ：占用了更多资源
   
   import time
   from threading import RLock, Thread
   # noodle_lock = RLock()
   # fork_lock = RLock()
   noodle_lock = fork_lock = RLock()
   print(noodle_lock, fork_lock)
   
   
   def eat1(name, noodle_lock, fork_lock):
       noodle_lock.acquire()
       print('%s抢到面了' % name)
       fork_lock.acquire()
       print('%s抢到叉子了' % name)
       print('%s吃了一口面' % name)
       time.sleep(0.1)
       fork_lock.release()
       print('%s放下叉子了' % name)
       noodle_lock.relea()
       print('%s放下面了' % sename)
   
   
   def eat2(name, noodle_lock, fork_lock):
       fork_lock.acquire()
       print('%s抢到叉子了' % name)
       noodle_lock.acquire()
       print('%s抢到面了' % name)
       print('%s吃了一口面' % name)
       time.sleep(0.1)
       noodle_lock.release()
       print('%s放下面了' % name)
       fork_lock.release()
       print('%s放下叉子了' % name)
   
   
   lst = ['alex', 'wusir', 'taibai', 'yuan']
   Thread(target=eat1, args=(lst[0], noodle_lock, fork_lock)).start()
   Thread(target=eat2, args=(lst[1], noodle_lock, fork_lock)).start()
   Thread(target=eat1, args=(lst[2], noodle_lock, fork_lock)).start()
   Thread(target=eat2, args=(lst[3], noodle_lock, fork_lock)).start()
   ```

7. 换种思路,互斥锁解决死锁问题

   ```python
   import time
   from threading import Lock, Thread
   lock = Lock()
   
   
   def eat1(name, noodle_lock, fork_lock):
       lock.acquire()
       print('%s抢到面了' % name)
       print('%s抢到叉子了' % name)
       print('%s吃了一口面' % name)
       time.sleep(0.1)
       print('%s放下叉子了' % name)
       print('%s放下面了' % name)
       lock.release()
   
   
   def eat2(name, noodle_lock, fork_lock):
       lock.acquire()
       print('%s抢到叉子了' % name)
       print('%s抢到面了' % name)
       print('%s吃了一口面' % name)
       time.sleep(0.1)
       print('%s放下面了' % name)
       print('%s放下叉子了' % name)
       lock.release()
   ```

#### 9.6.1 锁总结

1. 锁 - 都可以维护线程之间的数据安全
   - 互斥锁 ：一把锁不能在一个线程中连续acquire，开销小
   - 递归锁  ：一把锁可以连续在一个线程中acquire多次，acquire多少次就release多少次，开销大 
2. 死锁现象
   - 在某一些线程中出现陷入阻塞并且永远无法结束阻塞的情况就是死锁现象
   - 出现死锁.多把锁+交替使用
   - 互斥锁在一个线程中连续acquire
   - 避免死锁
         在一个线程中只有一把锁，并且每一次acquire之后都要release

### 9.7队列模块

```python
import queue
from queue import Queue    # 先进先出队列
from queue import LifoQueue  # 后进先出队列`数据结构栈`   
"""
应用场景:
# 先进先出
    # 写一个server，所有的用户的请求放在队列里
        # 先来先服务的思想
# 后进先出
    # 算法
# 优先级队列
    # 自动的排序
    # 抢票的用户级别 100000 100001
    # 告警级别
"""

lfq = LifoQueue(4)
lfq.put(1)
lfq.put(3)
lfq.put(2)
print(lfq.get())
print(lfq.get())
print(lfq.get())


# 
from queue import PriorityQueue          # 优先级队列
pq = PriorityQueue()
pq.put((10,'alex'))
pq.put((6,'wusir'))
pq.put((20,'yuan'))
print(pq.get())
print(pq.get())
print(pq.get())
```

### 9.8 进程池和线程池

#### 9.8.1预先的开启固定个数的进程数，

当任务来临的时候，直接提交给已经开好的进程

让这个进程去执行就可以了
节省了进程，线程的开启 关闭 切换都需要时间
并且减轻了操作系统调度的负担

1. 模块 concurrent.futures  创建池

   ```python
   import os
   import time
   import random
   from concurrent.futures import ProcessPoolExecutor
   # submit + shutdown
   
   def func():
       print('start',os.getpid())
       time.sleep(random.randint(1,3))
       print('end', os.getpid())
   if __name__ == '__main__':
       p = ProcessPoolExecutor(5)
       for i in range(10):
           p.submit(func)      # 把任务提交到进程池
       p.shutdown()   # 关闭池之后就不能继续提交任务，并且会阻塞，直到已经提交的任务完成
       print('main',os.getpid())
   ```

   ```python
   #  任务的参数 + 返回值
   def func(i,name):
       print('start',os.getpid())
       time.sleep(random.randint(1,3))
       print('end', os.getpid())
       return '%s * %s'%(i,os.getpid())
   if __name__ == '__main__':
       p = ProcessPoolExecutor(5)
       ret_l = []
       for i in range(10):
           ret = p.submit(func,i,'alex')   # 直接传参数
           ret_l.append(ret)
       for ret in ret_l:
           print('ret-->',ret.result())  #    ret.result() 同步阻塞
       print('main',os.getpid())
   ```

2. 进程池的缺点 : 一个池中的任务个数限制了我们程序的并发个数

#### 8.8.2线程池的开启

```python
from concurrent.futures import ThreadPoolExecutor
def func(i):
    print('start', os.getpid())
    time.sleep(random.randint(1,3))
    print('end', os.getpid())
    return '%s * %s'%(i,os.getpid())

tp = ThreadPoolExecutor(20)

ret_l = []
for i in range(10):
    ret = tp.submit(func,i)
    ret_l.append(ret)
    
tp.shutdown()
print('main')
for ret in ret_l:
    print('------>',ret.result())
```

```python
from concurrent.futures import ThreadPoolExecutor

def func(i):
    print('start', os.getpid())
    time.sleep(random.randint(1,3))
    print('end', os.getpid())
    return '%s * %s'%(i,os.getpid())
tp = ThreadPoolExecutor(20)
#  使用map替换for  和  submit的用法
ret = tp.map(func,range(20))
for i in ret:
    print(i)
ret_l = []
for i in range(10):
    ret = tp.submit(func,i)
    ret_l.append(ret)
tp.shutdown()
print('main')
```

#### 9.8.3 回调函数(add_done_callback)

```python
import requests
from concurrent.futures import ThreadPoolExecutor


def get_page(url):
    res = requests.get(url)
    return {'url': url, 'content': res.text}


def parserpage(ret):
    dic = ret.result()
    print(dic['url'])


tp = ThreadPoolExecutor(5)
url_lst = [
    'http://www.baidu.com',   3
    'http://www.cnblogs.com',  1
    'http://www.douban.com',  1
    'http://www.tencent.com',
    'http://www.cnblogs.com/Eva-J/articles/8306047.html',
    'http://www.cnblogs.com/Eva-J/articles/7206498.html',
]
ret_l = []
for url in url_lst:
    ret = tp.submit(get_page, url)
    ret_l.append(ret)
    ret.add_done_callback(parserpage)
```

#### 9.8.4 总结

```python
# ThreadPoolExcutor
# ProcessPoolExcutor

# 创建一个池子
tp = ThreadPoolExcutor(池中线程/进程的个数)
# 异步提交任务
ret = tp.submit(函数,参数1，参数2....)
# 获取返回值
ret.result()
# 在异步的执行完所有任务之后，主线程/主进程才开始执行的代码
tp.shutdown() 阻塞 直到所有的任务都执行完毕
# map方法
# ret = tp.map(func,iterable) 迭代获取iterable中的内容，作为func的参数，让子线程来执行对应的任务
for i in ret: 每一个都是任务的返回值
# 回调函数
ret.add_done_callback(函数名)
# 要在ret对应的任务执行完毕之后，直接继续执行add_done_callback绑定的函数中的内容，并且ret的结果会作为参数返回给绑定的函数
```

1. 应用场景

   ```python
   # 池
       # from concurrent.futrues import ThreadPoolExecutor
       # 1.是单独开启线程进程还是池？
           # 如果只是开启一个子线程做一件事情，就可以单独开线程
           # 有大量的任务等待程序去做，要达到一定的并发数，开启线程池
           # 根据你程序的io操作也可以判定是用池还是不用池？
               # socket的server 大量的阻塞io   recv recvfrom socketserver
               # 爬虫的时候 池
       # 2.回调函数add_done_callback
               # 执行完子线程任务之后直接调用对应的回调函数
               # 爬取网页 需要等待数据传输和网络上的响应高IO的 -- 子线程
               # 分析网页 没有什么IO操作 -- 这个操作没必要在子线程完成，交给回调函数
       # 3.ThreadPoolExecutor中的几个常用方法
           # tp = ThreadPoolExecutor(cpu*5)
           # obj = tp.submit(需要在子线程执行的函数名,参数)
           # obj
               # 1.获取返回值 obj.result() 是一个阻塞方法
               # 2.绑定回调函数 obj.add_done_callback(子线程执行完毕之后要执行的代码对应的函数)
           # ret = tp.map(需要在子线程执行的函数名,iterable)
               # 1.迭代ret，总是能得到所有的返回值
           # shutdown
               # tp.shutdown()
   ```

2. 锁的问题

   ```python
   # 进程 和 线程都有锁
       # 所有在线程中能工作的基本都不能在进程中工作
       # 在进程中能够使用的基本在线程中也可以使用
   ```

3. 在多进程里开多线程

   ```python
   在多进程里启动多线程
   import os
   from multiprocessing import Process
   from threading import Thread
   #
   def tfunc():
       print(os.getpid())
   def pfunc():
       print('pfunc-->',os.getpid())
       Thread(target=tfunc).start()
   #
   if __name__ == '__main__':
       Process(target=pfunc).start()
   ```

### 9.9协程

- 进程是计算机最小的资源调度单位

- 线程是能被CPU执行的最小单位-->线程是由 操作系统 调度,由操作系统负责切换的

- 协程
  - 用户级别的,由我们自己写的python代码来控制切换的
    是操作系统不可见的

#### 9.9.1 协程解析

```python
# 在Cpython解释器下 - 协程和线程都不能利用多核,都是在一个CPU上轮流执行
    # 由于多线程本身就不能利用多核
    # 所以即便是开启了多个线程也只能轮流在一个CPU上执行
    # 协程如果把所有任务的IO操作都规避掉,只剩下需要使用CPU的操作
    # 就意味着协程就可以做到题高CPU利用率的效果
```

1. 多线程和协程
   - 线程 切换需要操作系统,开销大,操作系统不可控,给操作系统的压力大
        操作系统对IO操作的感知更加灵敏
   - 协程 切换需要python代码,开销小,用户操作可控,完全不会增加操作系统的压力
         用户级别能够对IO操作的感知比较低
   - 协程 :能够在一个线程下的多个任务之间来回切换,那么每一个任务都是一个协程
     - 两种切换方式
       -  原生python完成   `yield  asyncio`
       -  C语言完成的python模块    `greenlet    gevent`

#### 9.9.2 gevent 模块 

1. grennlet 模块

```PYTHON
# greenlet 第三方模块
import time
from  greenlet import greenlet
#
def eat():
    print('wusir is eating')
    time.sleep(0.5)
    g2.switch()
    print('wusir finished eat')
#
def sleep():
    print('小马哥 is sleeping')
    time.sleep(0.5)
    print('小马哥 finished sleep')
    g1.switch()
#
g1 = greenlet(eat)
g2 = greenlet(sleep)
g1.switch()
```

2. gvent 模块

   ```python
   import time
   print('-->',time.sleep)
   import gevent
   from gevent import monkey
   monkey.patch_all()             # 魔术方法,在里面重写py的各种阻塞代码
   def eat():
       print('wusir is eating')
       print('in eat: ',time.sleep)
       time.sleep(1)
       print('wusir finished eat')
   #
   def sleep():
       print('小马哥 is sleeping')
       time.sleep(1)
       print('小马哥 finished sleep')
   #
   g1 = gevent.spawn(eat)  创造一个协程任务
   g2 = gevent.spawn(sleep)  创造一个协程任务
   g1.join()   阻塞 直到g1任务完成为止
   g2.join()   阻塞 直到g1任务完成为止
   
   # 升级版
   
   
   gevent.joinall([g1,g2,g3])           # 全部阻塞
   g_l = []
   for i in range(10):
       g = gevent.spawn(eat)
       g_l.append(g)
   gevent.joinall(g_l)
   
     #############################################################################
     # 返回值的版本  
       
   import time
   import gevent
   from gevent import monkey
   monkey.patch_all()
   def eat():
       print('wusir is eating')
       time.sleep(1)
       print('wusir finished eat')
       return 'wusir***'
   #
   def sleep():
       print('小马哥 is sleeping')
       time.sleep(1)
       print('小马哥 finished sleep')
       return '小马哥666'
   #
   g1 = gevent.spawn(eat)
   g2 = gevent.spawn(sleep)
   gevent.joinall([g1,g2])
   print(g1.value)
   print(g2.value)
   ```

#### 9.9.3 async  模块协程

```
import asyncio

# 起一个任务
# async def demo():   # 协程方法
#     print('start')
#     await asyncio.sleep(1)  # 阻塞
#     print('end')
#
# loop = asyncio.get_event_loop()  # 创建一个事件循环
# loop.run_until_complete(demo())  # 把demo任务丢到事件循环中去执行

# 启动多个任务,并且没有返回值
# async def demo():   # 协程方法
#     print('start')
#     await asyncio.sleep(1)  # 阻塞
#     print('end')
#
# loop = asyncio.get_event_loop()  # 创建一个事件循环
# wait_obj = asyncio.wait([demo(),demo(),demo()])
# loop.run_until_complete(wait_obj)

# 启动多个任务并且有返回值
# async def demo():   # 协程方法
#     print('start')
#     await asyncio.sleep(1)  # 阻塞
#     print('end')
#     return 123
#
# loop = asyncio.get_event_loop()
# t1 = loop.create_task(demo())
# t2 = loop.create_task(demo())
# tasks = [t1,t2]
# wait_obj = asyncio.wait([t1,t2])
# loop.run_until_complete(wait_obj)
# for t in tasks:
#     print(t.result())

# 谁先回来先取谁的结果
# import asyncio
# async def demo(i):   # 协程方法
#     print('start')
#     await asyncio.sleep(10-i)  # 阻塞
#     print('end')
#     return i,123
#
# async def main():
#     task_l = []
#     for i in range(10):
#         task = asyncio.ensure_future(demo(i))
#         task_l.append(task)
#     for ret in asyncio.as_completed(task_l):
#         res = await ret
#         print(res)
#
# loop = asyncio.get_event_loop()
# loop.run_until_complete(main())



# import asyncio
#
# async def get_url():
#     reader,writer = await asyncio.open_connection('www.baidu.com',80)
#     writer.write(b'GET / HTTP/1.1\r\nHOST:www.baidu.com\r\nConnection:close\r\n\r\n')
#     all_lines = []
#     async for line in reader:
#         data = line.decode()
#         all_lines.append(data)
#     html = '\n'.join(all_lines)
#     return html
#
# async def main():
#     tasks = []
#     for url in range(20):
#         tasks.append(asyncio.ensure_future(get_url()))
#     for res in asyncio.as_completed(tasks):
#         result = await res
#         print(result)
#
#
# if __name__ == '__main__':
#     loop = asyncio.get_event_loop()
#     loop.run_until_complete(main())  # 处理一个任务


# python原生的底层的协程模块
    # 爬虫 webserver框架
    # 题高网络编程的效率和并发效果
# 语法
    # await 阻塞 协程函数这里要切换出去，还能保证一会儿再切回来
    # await 必须写在async函数里，async函数是协程函数
    # loop 事件循环
    # 所有的协程的执行 调度 都离不开这个loop
```

### 9.10 并发总结

```
# 操作系统
    # 1.计算机中所有的资源都是由操作系统分配的
    # 2.操作系统调度任务：时间分片、多道机制
    # 3.CPU的利用率是我们努力的指标
# 并发
    # 进程 开销大 数据隔离 资源分配单位 cpython下可以利用多核
        # 进程的三状态：就绪 运行 阻塞
        # multiprocessing模块
            # Process-开启进程
            # Lock - 互斥锁
                # 为什么要在进程中加锁
                    # 因为进程操作文件也会发生数据不安全
            # Queue -队列 IPC机制（Pipe,redis,memcache,rabbitmq,kafka）
                # 生产者消费者模型
            # Manager - 提供数据共享机制
    # 线程 开销小 数据共享 cpu调度单位  cpython下不能利用多核
        # GIL锁
            # 全局解释器锁
            # Cpython解释器提供的
            # 导致了一个进程中多个线程同一时刻只有一个线程能当问CPU -- 多线程不能利用多核
        # threading
            # Thread类 - 能开启线程start，等待线程结束join
            # Lock-互斥锁  不能在一个线程中连续acquire，效率相对高
            # Rlock-递归锁  可以在一个线程中连续acquire，效率相对低
            # 死锁现象如何发生?如何避免？
        # 线程队列 queue模块
            # Queue
            # LifoQueue
            # PriorityQueue
    # 池
        # concurrent.futrues.ThreadPoolExecutor,ProcessPoolExecutor
            # 实例化一个池 tp = ThreadPoolExecutor(num),pp = ProcessPoolExecutor(num)
            # 提交任务到池中，返回一个对象 obj = tp.submit(func,arg1,arg2...)
                # 使用这个对象获取返回值 obj.result()
                # 回调函数 obj.add_done_callback(函调函数)
            # 阻塞等待池中的任务都结束 tp.shutdown()
# 概念
    # IO操作
    # 同步异步
    # 阻塞非阻塞

# 1.所有讲过的概念都记住
# 2.数据安全问题
# 3.数据隔离和通信
# 4.会用基本的进程 线程 池
```





