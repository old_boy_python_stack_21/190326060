#!/usr/bin/env python
# -*- coding:utf-8 -*-

# 1.flaga   --->flagb 0秒,此时t是异步阻塞的

# 2.         0秒,守护进程,

# 3. 60秒,此时是同步阻塞的

# 4. 不一定,多线程间内存数据是共享的,不清楚什么时候就把数据变动了

# 5. 不一定

# 7. 长度一定为1

# 8. 长度一定为1

# 协程
# 1. # 线程和进程的创建和切换都是操作系统做的,只提供接口.
# 协程则我我们利用python代码完成的,若线程遇到了io操作,人为地切换到其他线程,
# 使CPU一直饱和工作,这样协程就可以做到提高CPU利用率的效果.


# 2. 起阻塞作用,协程才会切换到协程任务执行,一直阻塞到该协程任务完成为止.
# 没有阻塞,协程就不会执行.

# 3.

import gevent
import socket

from gevent import monkey
monkey.patch_all()


def chat(conn: object):
    print('开启运行')
    while True:
        msg = conn.recv(1024).decode('utf-8')
        if msg == 'Q':
            break
        msg = msg.upper().encode('utf-8')
        conn.send(msg)
    conn.close()


sk = socket.socket()
sk.bind(('127.0.0.1', 8888))
sk.listen(5)
while True:
    conn, _ = sk.accept()         # 阻塞 ,下一次循环进来,accept堵塞,开启协程的运行
    g1 = gevent.spawn(chat, conn)




# 4、在一个列表中有多个url，请使用协程访问所有url，将对应的网页内容写入文件保存

"""
import gevent
import requests
from gevent import monkey
monkey.patch_all()

url_list = [
    'http://www.baidu.com',
    'http://tool.chinaz.com/'
]


def func(url, file_name):
    ret = requests.get(url)
    with open(file_name, mode='w', encoding='utf-8') as f:
        f.write(ret.text)
        f.flush()


g_list = []
for i in range(len(url_list)):
    g = gevent.spawn(func, url_list[i], '%s.txt' % (i,))
    g_list.append(g)

gevent.joinall(g_list)
"""


# 综合
# 1、进程和线程的区别
'''
进程开销大，可以利用多核，数据隔离，是最小的资源分配单位
线程开销小，在Cpython解释器下不能利用多核，数据共享，是最小的cpu调度单位
'''


# 2、进程池、线程池的优势和特点
'''
进程池开销较大，但可以实现并行
线程池开销较小，但不能实现并行
'''
# 3、线程和协程的异同?
'''
线程和协程都不能实现并行，都是在轮转使用同一个cpu
但是线程的切换是由操作系统来执行的，对操作系统压力大，但对io操作的感知更灵敏
协程的切换是由用户来执行的，不会增加操作系统的压力，但是对io操作不如线程灵敏
'''


# 4、请简述一下互斥锁和递归锁的异同？
'''
相同点：都可以解决线程的数据安全问题
不同点：
互斥锁：在同一个线程中，同一把互斥锁不能连续acquire多次，开销较小，效率较高
递归锁：在同一个线程中，同一把递归锁可以连续acquire多次，但也需要release同样的次数，开销较大，效率较慢
'''


# 5、请列举一个python中数据安全的数据类型？
'''
元组
'''


# 6、Python中如何使用线程池和进程池
'''
# 线程池


def func(i):
    print(i)


pool = ThreadPoolExecutor(20)
for i in range(10):
    pool.submit(func, i)
'''
'''
# 进程池


def func(i):
    print(i)


if __name__ == '__main__':
    pool = ProcessPoolExecutor(5)
    for i in range(10):
        pool.submit(func, i)
'''


# 7、简述 进程、线程、协程的区别 以及应用场景？
'''
进程：开销大，数据隔离，可以实现并行。用于高计算的场景
线程：开销小，数据共享，不能实现并行，切换由操作系统完成，对操作系统压力大，但对io操作更灵敏。用于多io操作的场景
协程：开销最小，不能实现并行，由用户完成任务切换，对操作系统无压力，对io操作不如线程灵敏。用于多io操作场景
'''


# 8、什么是并行，什么是并发？
'''
并行：多个任务分别在不同的cpu上执行
并发：多个任务在同一个cpu上轮流执行
'''
