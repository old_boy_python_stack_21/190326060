from django.shortcuts import render, HttpResponse, redirect
from publish_charge import models


# Create your views here.
def publish_list(request):
    obj_list = models.Publisher.objects.all().order_by('pk')
    if request.method == 'POST':
        pid = request.POST.get('id')
        obj_list = models.Publisher.objects.limit()
        if not obj_list:
            return HttpResponse('搜索的数据不存在')

    # for item in obj_list:
    #     print(item.p_name)
    return render(request, 'publish_list.html', {"obj_list": obj_list})


def add_publisher(request):
    error = ''
    if request.method == 'POST':
        p_name = request.POST['p_name']
        if models.Publisher.objects.filter(p_name=p_name):
            error = '出版社已存在'
            print(error)
        elif not p_name:
            error = '出版社名不能为空'
            print(error)
        else:
            models.Publisher.objects.create(p_name=p_name)
            return redirect('/index/publish_list/')
    return render(request, 'add_publish.html', {'error': error})


def edit_publisher(request):
    error = ''
    pid = request.GET.get('id')  # # url上携带的参数  不是GET请求提交参数,
    obj_list = models.Publisher.objects.filter(pk=pid)
    if not obj_list:
        error = '编辑的出版社不存在'
    if request.method == "POST":
        obj = obj_list[0]
        p_name = request.POST.get('p_name')
        if models.Publisher.objects.filter(p_name=p_name):
            error = '新修改的名称已存在'
        elif obj.p_name == p_name:
            error = '名称未修改'
        elif not p_name:
            error = '名称不能为空'
        if not error:
            # 新修改的名称不在数据库内,不为空,并且已经修改(和原来的名称不同)
            obj.p_name = p_name
            obj.save()
            return redirect('/index/publish_list/')
    return render(request, 'edit_publish.html', {'error': error, 'obj': obj_list})


def del_publisher(request):
    pid = request.GET.get('id')
    obj_list = models.Publisher.objects.filter(pk=pid)
    if not obj_list:
        return HttpResponse('要删除的数据不存在')
    obj_list.delete()
    return redirect('/publish_list/')


def index(request):
    request_type = ""

    if request.path == '/index/book_list/':
        obj_list = models.Book.objects.all().order_by('pun_id')
        request_type='book_list'
    else:
        obj_list = models.Publisher.objects.all().order_by('pk')
        request_type = "publish_list"
    if request.method == 'POST':
        wd = request.POST.get('wd')
        if request_type =='book_list':
            obj_list = models.Book.objects.filter(b_name=wd)
        else:
            obj_list = models.Publisher.objects.filter(p_name=wd)

    # for item in obj_list:
    #     print(item.p_name)
    return render(request, 'index.html', {"obj_list": obj_list,'type':request_type})
