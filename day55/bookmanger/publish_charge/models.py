from django.db import models

# Create your models here.
class Publisher(models.Model):

    class Meta:
        db_table = 'publishers'

    pid = models.AutoField(primary_key=True)
    p_name = models.CharField(max_length=32,blank=False,unique=True)

class Book(models.Model):

    class Meta:
        db_table = 'books'

    bid = models.AutoField(primary_key=True)
    b_name = models.CharField(max_length=32,blank=False)
    #外键,默认以连接的表的主键为外链,级联删除
    pun = models.ForeignKey('Publisher',on_delete=models.CASCADE)