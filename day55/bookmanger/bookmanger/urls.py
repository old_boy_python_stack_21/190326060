"""bookmanger URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from publish_charge import views
from bookcharge import views as book_views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^publish_list/', views.publish_list),
    url(r'^add_publisher/', views.add_publisher),
    url(r'^edit_publisher/', views.edit_publisher),
    url(r'^del_publisher/', views.del_publisher),
    url(r'^index/', views.index),
    url(r'^book_list/', book_views.book_list),
    url(r'^add_book/', book_views.add_book),
    url(r'^del_book/', book_views.del_book),
    url(r'^edit_book/', book_views.edit_book),
    url(r'^index/publish_list/',views.index),
    url(r'^index/book_list/',views.index)
]
