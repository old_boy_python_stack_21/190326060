## 第十二章 Django框架

### 12.1 web 框架

1. 所有web应用本质就是一个socket的server端
2. 所有的web服务都遵循同一个标准--http协议,来收发文件
3. HTTP协议主要规定了客户端和服务器之间的通信格式

### 12.2 HTTP协议

1. 超文本传输协议--http协议,是一种用于分布式、协作式和超媒体信息系统的应用层协议。HTTP是万维网的数据通信的基础。HTTP有很多应用，但最著名的是用于web浏览器和web服务器之间的双工通信。(广泛使用的版本是http1.1)
2. HTTP是一个客户端终端（用户）和服务器端（网站）请求和应答的标准（TCP）。默认端口为80时自动隐藏端口号.
3. 通常，由HTTP客户端发起一个请求，创建一个到服务器指定端口（默认是80端口）的TCP连接。HTTP服务器则在那个端口监听客户端的请求。一旦收到请求，服务器会向客户端返回一个状态，比如"HTTP/1.1 200 OK"，以及返回的内容，如请求的文件、错误消息、或者其它信息。

#### 12.2.1 http 工作原理

HTTP协议定义Web客户端如何从Web服务器请求Web页面，以及服务器如何把Web页面传送给客户端。HTTP协议采用了请求/响应模型。客户端向服务器发送一个请求报文，请求报文包含请求的方法、URL、协议版本、请求头部和请求数据。服务器以一个状态行作为响应，响应的内容包括协议的版本、成功或者错误代码、服务器信息、响应头部和响应数据。

http响应请求的步骤

1. 客户端连接到Web服务器
   一个HTTP客户端，通常是浏览器，与Web服务器的HTTP端口（默认为80）建立一个TCP套接字连接。例如，http://www.luffycity.com。
2. 发送HTTP请求
   通过TCP套接字，客户端向Web服务器发送一个文本的请求报文，一个请求报文由请求行、请求头部、空行和请求数据4部分组成。
3. 服务器接受请求并返回HTTP响应
   Web服务器解析请求，定位请求资源。服务器将资源复本写到TCP套接字，由客户端读取。一个响应由状态行、响应头部、空行和响应数据4部分组成。
4. 释放连接TCP连接
   若connection 模式为close，则服务器主动关闭TCP连接，客户端被动关闭连接，释放TCP连接;若connection 模式为keepalive，则该连接会保持一段时间，在该时间内可以继续接收请求;
5. 客户端浏览器解析HTML内容
   客户端浏览器首先解析状态行，查看表明请求是否成功的状态代码。然后解析每一个响应头，响应头告知以下为若干字节的HTML文档和文档的字符集。客户端浏览器读取响应数据HTML，根据HTML的语法对其进行格式化，并在浏览器窗口中显示。

```python
在浏览器地址栏键入URL，按下回车之后会经历以下流程：

浏览器向 DNS 服务器请求解析该 URL 中的域名所对应的 IP 地址;
解析出 IP 地址后，根据该 IP 地址和默认端口 80，和服务器建立TCP连接;
浏览器发出读取文件(URL 中域名后面部分对应的文件)的HTTP 请求，该请求报文作为 TCP 三次握手的第三个报文的数据发送给服务器;
服务器对浏览器请求作出响应，并把对应的 html 文本发送给浏览器;
释放 TCP连接;
浏览器将该 html 文本并显示内容; 
```

#### 12.2.2 http请求的方法

1. 主要有八种
   1. get --> 获取一个页面、图片（资源）-->请求没有请求数据
   2. post -->提交数据
   3. head 
   4. put 
   5. delete 
   6. options 
   7. trace 
   8. connect

#### 12.2.3http状态码

所有http的响应的第一行都是状态行，依次是当前HTTP版本号，3位数字组成的状态代码，以及描述状态的短语，彼此由空格分隔。

状态代码的第一个数字代表当前响应的类型：

1. 1xx消息——请求已被服务器接收，继续处理
2. 2xx成功——请求已成功被服务器接收、理解、并接受(200 ok 成功)
3. 3xx重定向——需要后续操作才能完成这一请求(重定向)
4. 4xx请求错误——请求含有词法错误或者无法被执行(404 403 402)
5. 5xx服务器错误——服务器在处理某个正确请求时发生错误

> **注意 : **RFC 2616 中已经推荐了描述状态的短语，例如"200 OK"，"404 Not Found"，但是WEB开发者仍然能够自行决定采用何种短语，用以显示本地化的状态描述或者自定义信息。 

#### 12.2.4 url 

超文本传输协议（HTTP）**统一资源定位符**将从因特网获取信息的五个基本元素包括在一个简单的地址中：

1. 传送协议。

2. 层级URL标记符号(为[//],固定不变)

3. 访问资源需要的凭证信息（可省略）

4. 服务器。（通常为域名，有时为IP地址）

5. 端口号。（以数字方式表示，若为HTTP的默认值“:80”可省略）

6. 路径。（以“/”字符区别路径中的每一个目录名称）

7. 查询。（GET模式的窗体参数，以“?”字符为起点，每个参数以“&”隔开，再以“=”分开参数名称与数据，通常以UTF8的URL编码，避开字符冲突的问题）

8. 片段。以“#”字符为起点

   ```python
   以 http://www.luffycity.com:80/news/index.html?id=250&page=1为例
   1. http--.表明协议http(https加密)http(80),https(443)
   2. www.luffycity.com --服务器
   3. :80 --服务器上的网络端口号；80的时候默认隐藏
       “80”是超文本传输协议文件的常用端口号，因此一般也不必写明。一般来说用户只要键入统一资源定位符	的一部分（www.luffycity.com/news/index.html?id=250&page=1）就可以了。
   4. /news/index.html 是路径；
   5. ?id=250&page=1，是查询。
   ```

#### 12.2.5 http 的请求格式 和 响应 格式

![1560219121217](D:\md_down_png\1560219121217.png)

浏览器向服务端请求数据,get请求没有请求数据

```python
1. 请求requset

"(请求方式)get (url路径)/ (协议版本)HTTP/1.1\r\n
k1:v1\r\n
k2:v2\r\n
k3:v3\r\n
\r\n
请求数据(请求体)"  -->get没有请求数据

2.响应(response)

服务器发送给 浏览器

"(协议版本)HTTP/1.1 状态码 状态码描述\r\n
k1:v1\r\n
k2:v2\r\n
k3:v3\r\n
\r\n
响应的数据(响应体response)"  -->html文本
```

![1560243848099](D:\md_down_png\1560243848099.png)

##### 12.2.5.1 浏览器发送请求和接受响应的过程

1. 在浏览器上的地址栏种输入URL，回车，发送get请求；
2. 服务器接受到请求,获取URL路径,根据路径做不同的操作,把返回的数据封装到响应体中,发送给浏览器.
3. 浏览器接受到响应,双方断开连接
4. 浏览器从响应体获取数据,进行解析渲染;

#### 12.2.6 web 框架的功能

1. **本质是socket 服务端**

2. 对于真实开发中的python web程序来说，一般会分为两部分：服务器程序和应用程序。

   服务器程序负责对socket服务端进行封装，并在请求到来时，对请求的各种数据进行整理。

   应用程序则负责具体的逻辑处理。为了方便应用程序的开发，就出现了众多的Web框架，例如：Django、Flask、web.py 等。不同的框架有不同的开发方式，但是无论如何，开发出的应用程序都要和服务器程序配合，才能为用户提供服务。

3. **WSGI协议**（Web Server Gateway Interface）就是一种规范，它定义了使用Python编写的web应用程序与web服务器程序之间的接口格式，实现web应用程序与web服务器程序间的解耦。

   常用的WSGI服务器有uWSGI、Gunicorn。而Python标准库提供的独立WSGI服务器叫wsgiref，Django开发环境用的就是这个模块来做服务器。

4. **主要功能有**

   1. socket收发消息    - wsgiref 测试用  uwsgi 上线用

   2. 根据不同的路径返回不同的内容

   3. 返回动态页面（字符串的替换） —— jinja2

   > django 2 3   /  flask   2   /tornado 1  2   3

   4. 我们一般用django框架(功能很多很全),其他都是轻量化的,需要引入其他模块

5. wsgiref模块(起socket服务端)的使用

   ```python
   from wsgiref.simple_server import make_server   
   # 导入模块
        
   # 将返回不同的内容部分封装成函数   
   def index(url):   
       # 读取index.html页面的内容   
       with open("index.html", "r", encoding="utf8") as f:   
           s = f.read()   
       # 返回字节数据   
       return bytes(s, encoding="utf8")   
        
        
   def home(url):   
       with open("home.html", "r", encoding="utf8") as f:   
           s = f.read()   
       return bytes(s, encoding="utf8") 
   	#return s.encode('utf-8')
        
        
   def timer(url):   
       import time   
       with open("time.html", "r", encoding="utf8") as f:   
           s = f.read()   
           s = s.replace('@@time@@', time.strftime("%Y-%m-%d %H:%M:%S"))   
       return bytes(s, encoding="utf8")   
        
        
   # 定义一个url和实际要执行的函数的对应关系   
   list1 = [   
       ("/index/", index),   
       ("/home/", home),   
       ("/time/", timer),   
   ]   
        
        
   def run_server(environ, start_response):   
   #注意>>>    
   	start_response('200 OK', [('Content-Type', 'text/html;charset=utf8'), ])  # 设置HTTP响应的状态码和头信息   
   #注意>>>    
   	url = environ['PATH_INFO']  # 取到用户输入的url   
       func = None   
       for i in list1:   
           if i[0] == url:   
               func = i[1]   
               break   
       if func:   
           response = func(url)   
       else:   
           response = b"404 not found!"   
       return [response, ]   
        
        
   if __name__ == '__main__':   
       httpd = make_server('127.0.0.1', 8090, run_server)   #注意这个
       print("我在8090等你哦...")   
       httpd.serve_forever()  
   ```

6. jinja2模块的使用(模板渲染工具,模板就是html)

   ```python
   from wsgiref.simple_server import make_server  
   from jinja2 import Template  
     
     
   def index(url):  
       # 读取HTML文件内容  
       with open("index2.html", "r", encoding="utf8") as f:  
           data = f.read()  
           template = Template(data)   # 生成模板文件  
           ret = template.render({'name': 'alex', 'hobby_list': ['抽烟', '喝酒', '烫头']})   # 把数据填充到模板中  
       return bytes(ret, encoding="utf8")  
     
     
   def home(url):  
       with open("home.html", "r", encoding="utf8") as f:  
           s = f.read()  
       return bytes(s, encoding="utf8")  
     
     
   # 定义一个url和实际要执行的函数的对应关系  
   list1 = [  
       ("/index/", index),  
       ("/home/", home),  
   ]  
     
     
   def run_server(environ, start_response):  
       start_response('200 OK', [('Content-Type', 'text/html;charset=utf8'), ])  # 设置HTTP响应的状态码和头信息  
       url = environ['PATH_INFO']  # 取到用户输入的url  
       func = None  
       for i in list1:  
           if i[0] == url:  
               func = i[1]  
               break  
       if func:  
           response = func(url)  
       else:  
           response = b"404 not found!"  
       return [response, ]  
     
     
   if __name__ == '__main__':  
       httpd = make_server('127.0.0.1', 8090, run_server)  
       print("我在8090等你哦...")  
       httpd.serve_forever() 
   ```

   ```html
   <!DOCTYPE html>
   <html lang="zh-CN">
   <head>
     <meta charset="UTF-8">
     <meta http-equiv="x-ua-compatible" content="IE=edge">
     <meta name="viewport" content="width=device-width, initial-scale=1">
     <title>Title</title>
   </head>
   <body>
       <h1>姓名：{{name}}</h1>
       <h1>爱好：</h1>
       <ul>
           {% for hobby in hobby_list %}
           <li>{{hobby}}</li>
           {% endfor %}
       </ul>
   </body>
   </html
   ```

   ```python
   # 使用mysql来获取数据
   conn = pymysql.connect(host="127.0.0.1", port=3306, user="root", passwd="xxx", db="xxx", charset="utf8")
   cursor = conn.cursor(cursor=pymysql.cursors.DictCursor)
   cursor.execute("select name, age, department_id from userinfo")
   user_list = cursor.fetchall()
   cursor.close()
   conn.close()
   ```

### 12.3  django 框架 的使用(重点)

#### 12.3.1 配置

1. 下载安装

   ```py
   1. https://www.djangoproject.com/download/  官网下载
   2. pip3 install django==1.11.21  //pip包管理器安装//注意LTS意指长期支持的版本
   3. 通过pycharm调用pip安装
   	setting ——》 解释器 ——》 点+号 ——》 输入Django ——》 选择版本 ——》 下载安装
   4. pip install django==1.11.21 -i 源
   ```

2. 创建一个django项目

   ```python
   1. 命令行创建,打开控制台,创建了一个mysite的django项目,目录结构如下
   django-admin startproject 项目名称
   ```

   ![1560226751493](D:\md_down_png\1560226751493.png)

   > **注意:** Django 启动时报错 UnicodeEncodeError ...
   >
   > 报这个错误通常是因为计算机名为中文，改成英文的计算机名重启下电脑就可以了。

   ```python
   2. pycharmIDE创建django项目
   ```

   ​	![1560227015628](D:\md_down_png\1560227015628.png)

3. 运行django项目

   `1.一路cd到项目的根目录,manger.py文件目录下`

   ​		`python3 manager.py runserver 127.0.0.1:8080`

   `2. pycharm运行`

4. 模板文件的配置setting.py

   ```python
   TEMPLATES = [
       {
           'BACKEND': 'django.template.backends.django.DjangoTemplates',
           'DIRS': [os.path.join(BASE_DIR, "template")],  # template文件夹位置
           'APP_DIRS': True,
           'OPTIONS': {
               'context_processors': [
                   'django.template.context_processors.debug',
                   'django.template.context_processors.request',
                   'django.contrib.auth.context_processors.auth',
                   'django.contrib.messages.context_processors.messages',
               ],
           },
       },
   ]
   
   #静态文件配置
   STATIC_URL = '/static/'  # HTML中使用的静态文件夹前缀,别名
   STATICFILES_DIRS = [
       os.path.join(BASE_DIR, "static"),
       os.path.join(BASE_DIR, 'static'),
       os.path.join(BASE_DIR, 'static2'),     # 静态文件存放位置
   ]
   ```
   
   静态文件配置图解

   ```html
   <link rel="stylesheet" href="/static/css/login.css">   # 别名开头
   按照STATICFILES_DIRS列表的顺序进行查找。
   ```
   
   
   
   ![1560254220109](D:\md_down_png\1560254220109.png)
   
   > **注意:**若用pycharm创建时,要注意模板文件配置

##### 12.3.1.2 简单登录实例

form表单提交数据注意的问题：

1. 提交的地址action=""  请求的方式 method="post"
2. 所有的input框有name属性
3. 有一个input框的type="submit" 或者 有一个button
4. **刚开始学习时可在配置文件中暂时禁用csrf中间件，方便表单提交测试。**提交POST请求，把settings中MIDDLEWARE的'django.middleware.csrf.CsrfViewMiddleware'注释掉, 注释掉之后就可以提交POST请求

#### 12.3.2 django 基础三件套

1. `from django.shortcuts import HttpResponse, render, redirect`

2. `HttpResponse`内部传入一个字符串参数，返回给浏览器。

   ````python
   def index(request):
       # 业务逻辑代码
       return HttpResponse("OK")
   ````

3. `render`除request参数外还接受一个待渲染的模板文件和一个保存具体数据的字典参数。

   将数据填充进模板文件，最后把结果返回给浏览器。（类似于我们上面用到的jinja2）

   ```python
   def index(request):
       # 业务逻辑代码
       return render(request, "index.html", {"name": "alex", "hobby": ["烫头", "泡吧"]})
   ```

4. `redirect`接受一个URL参数，表示跳转到指定的URL。

   ```python
   def index(request):
       # 业务逻辑代码
       return redirect("/home/")
      #return redirect("https://www.baidu.com/")
   ```
#### 12.3.3 django 的 app

1. 一个Django项目可以分为很多个APP，用来隔离不同功能模块的代码。

2. 创建app

   ```python
   1. 命令行创建
   python manage.py startapp app01
   2.pycharm创建
   tools  ——》  run manage.py task  ——》  startapp  app名称
   ```

   ![1560327250145](D:\md_down_png\1560327250145.png)

   ![1560310077499](D:\md_down_png\1560310077499.png)

   然后注册app,需要在settings.py中注册

   ```python
   INSTALLED_APPS = [
   	...
       'app01',
       'app01.apps.App01Config',  # 推荐写法
   ]
   ```

#### 12.3.4 django  的 urls.py

写url路径和函数的对应关系

```python
from django.conf.urls import url
from app01 import views

urlpatterns = [
    url(r'^index/', views.index),
    url(r'^login/', views.login),
    url(r'^orm_test/', views.orm_test),
]
```

#### 12.3.5 django 的views.py

写url对应的函数,功能

```python
def login(request):
    
request.method   ——》 请求方式 GET  POST 
request.POST     ——》 form表单提交POST请求的数据  {}  request.POST['xxx'] request.POST.get('xxx',)

返回值
from django.shortcuts import HttpResponse, render, redirect
HttpResponse   —— 》 字符串 
render(request,'模板的文件名')  ——》 返回一个HTML页面 
redirect('重定向的地址')   ——》 重定向  /  响应头  Location：‘地址’  
```

> **注意:**get请求--->获取到一个页面,提交的数据暴露在URL上的,传递参数 <http://127.0.0.1:8000/index/?id=2&name=alex> ,获取数据  request.GET
>
> post请求--->提交数据,数据隐藏在请求体,获取数据  request.POST

##### 12.3.5.1 get 和post

get  获取到一个资源

发get的途径：

1.在浏览器的地址栏种输入URL，回车

2.a标签

3.form表单   不指定 method    method=‘get’

传递参数     url路径?id=1&name=alex

django种获取url上的参数    request.GET   {'id':1,'name':'alex'}   request.GET.get(key)     request.GET[key]

post   提交数据.

form表单   method=‘post’   

参数不暴露在URL    在请求体中  

django中获取post的数据  request.POST 

####  12.3.6 django 的 ORM

对象关系映射（Object Relational Mapping，简称ORM）模式是一种为了解决面向对象与关系数据库存在的互不匹配的现象的技术。ORM是通过使用描述对象和数据库之间映射的元数据，将程序中的对象自动持久化到关系数据库中。django辅助我们写数据库的

1. 它通常将一个类和一张表一一对应，类的每个实例对应表中的一条记录，类的每个属性对应表中的每个字段。 

   ORM提供了对数据库的映射，不用直接编写SQL代码，只需操作对象就能对数据库操作数据。

   让软件开发人员专注于业务逻辑的处理，提高了开发效率。

2. ORM的缺点是会在一定程度上牺牲程序的执行效率。

   ORM的操作是有限的，也就是ORM定义好的操作是可以完成的，一些复杂的查询操作是完成不了。

   ORM用多了SQL语句就不会写了，关系数据库相关技能退化...

1. 创建一个MySQL数据库。在控制台创建,django无法帮助我们创建库.

2. 在settings中配置，Django连接MySQL数据库：

```python
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',    # 引擎	
        'NAME': 'day53',						# 数据库名称
        'HOST': '127.0.0.1',					# ip地址
        'PORT':3306,							# 端口
        'USER':'root',							# 用户
        'PASSWORD':'123'						# 密码
        
    }
}
```

3. 在与settings同级目录下的 init文件中写：

```python
import pymysql
pymysql.install_as_MySQLdb()
#使用pymysql模块连接mysql数据库
```

4. 创建表（在app下的models.py中写类）:

```python
from django.db import models

class User(models.Model):
    username = models.CharField(max_length=32)   # username varchar(32)
    password = models.CharField(max_length=32)   # username varchar(32)
```

5. 执行数据库迁移的命令

python manage.py makemigrations   #  检测每个注册app下的model.py   记录model的变更记录

python manage.py migrate   #  同步变更记录到数据库中

6. orm操作

   ```python
   # 获取表中所有的数据
   ret = models.User.objects.all()  # QuerySet 对象列表  【对象】
   # 获取一个对象（有且唯一）
   obj = models.User.objects.get(username='alex')   # 获取不到或者获取到多个对象会报错
   # 获取满足条件的对象
   ret = models.User.objects.filter(username='alex1',password='dasb')  # QuerySet 对象列表
   ```

7. orm的对应关系

   类     ——》   表

   对象  ——》  数据行（记录）

   属性  ——》  字段

8. orm的操作:

   1. 操作表

   2. 操作数据行(查)

      ```python
      from app01 import models 
      
      models.User.objects.all()  # ——》  queryset  对象列表# 查找所有的数据
      obj = models.User.objects.get(name='alex')  # 没有或者多个 就报错,# 查找一条数据
      obj.name   ——》 'alex'
      models.User.objects.filter(name='alex')  # queryset  对象列表# 查询满足条件的所有数据
      ```
      
   3. 增删改查
   
   ```python
      ### 新增
   
      ​```python
      obj = models.Publisher.objects.create(name=publisher_name
                                            
      obj = models.Publisher(name='xxx')  ——》 存在在内存中的对象
      obj.save()      ——》   提交到数据库中  新增
      ​```
      
      ### 删除
      
      ​```python
      obj_list = models.Publisher.objects.filter(pk=pk)
      obj_list.delete()
      
      obj = models.Publisher.objects.get(pk=pk)
      obj.delete()
      ​```
      
      ### 编辑
      # 修改数据
      obj = models.Publisher.objects.get(pk=1)
      obj.name = publisher_name
      obj.save()  # 保存数据到数据库中
      ```

#### 12.3.7django的 模板系统 template.html

1. 常用语法

   ```html
   1.{{ 变量 }}  -->{{ }}表示变量，在模板渲染的时候替换成值
   2.{%   %}   -->{% %}表示逻辑相关的操作。
   3. (.) -->在模板语言中有特殊的含义，用来获取对象的相应属性值。
   	{# 取l中的第一个参数 #}
       {{ l.0 }}
       {# 取字典中key的值 #}
       {{ d.name }}
       {# 取对象的name属性 #}
       {{ person_list.0.name }}
       {# .操作只能调用不带参数的方法 #}
       {{ person_list.0.dream }}
   
   	注意:当模板系统遇到一个（.）时，会按照如下的顺序去查询：
                   在字典中查询
                   属性或者方法
                   数字索引
   ```

2. 过滤器

   `filter过滤器,来改变变量的显示结果`

   `{{ value|filter_name:参数 }}`**左右没有空格,没有空格,没有空格**

   ```python
   1.defalut
   	{{ value|default:"nothing"}}
   	如果value值没传的话就显示nothing
   
   	注：TEMPLATES的OPTIONS可以增加一个选项：string_if_invalid：'找不到'，可以替代default的的作用。
   2.filesizeformat
   	将值格式化为一个 “人类可读的” 文件尺寸 （例如 '13 KB', '4.1 MB', '102 bytes', 等等）
   	{{ value|filesizeformat }}
   	如果 value 是 123456789，输出将会是 117.7 MB。
   3.add 给变量加参数
   	{{ value|add:"2" }}-->value是数字4，则输出结果为6。
   	{{ first|add:second }}-->如果first是 [1,.2,3] ，second是 [4,5,6] ，那输出结果是[1,2,3,4,5,6] 
   4.lower小写/upperdaxie/title标题/capfirst首字母大写-->{{value|upper/lower/title}}
   5.ljust左对齐/右对齐rjust/center居中-->{{value|center:"15"}}
   6.length--返回value的长度{{[1,2,3,4]|length}}-->4
   7.slice切片-->{{value|slice:"2:-1"}},取首元素{{ value|first }},取最后元素{{ value|last }}
   8.拼接,和str.join(eq)类似,{{value|join:"//"}}
   9.truncatechars截断,参数为：截断的字符数,如果字符串字符多于指定的字符数量，那么会被截断。
   	截断的字符串将以可翻译的省略号序列（“...”）结尾。
       {{ value|truncatechars:9}}
   10.date 日期格式化-->{{ value|date:"Y-m-d H:i:s"}}
   	更多字符串格式化关注https://docs.djangoproject.com/en/1.11/ref/templates/builtins/#date
   11.若value="<a href='#'>点我</a>",添加{{ value|safe}},
   	是一个单独的变量我们可以通过过滤器“|safe”的方式告诉Django这段代码是安全的不必转义。
   12.自定义过滤器
   ```

3. for 循环

   ```python
   <ul>
   {% for user in user_list %}
       <li>{{ user.name }}</li>
   {% endfor %}
   </ul>
   
   #-------------------------#
   一些可用参数
       forloop.counter	当前循环的索引值（从1开始）
       forloop.counter0	当前循环的索引值（从0开始）
       forloop.revcounter	当前循环的倒序索引值（到1结束）
       forloop.revcounter0	当前循环的倒序索引值（到0结束）
       forloop.first	当前循环是不是第一次循环（布尔值）
       forloop.last	当前循环是不是最后一次循环（布尔值）
       forloop.parentloop	本层循环的外层循环
   #循环完了执行empty的代码
   <ul>
   {% for user in user_list %}
       <li>{{ user.name }}</li>
   {% empty %}
       <li>空空如也</li>
   {% endfor %}
   </ul>
   
   ```

4. `if-else-elif`

   ```python
   {% if user_list %}
     用户人数：{{ user_list|length }}
   {% elif black_list %}
     黑名单数：{{ black_list|length }}
   {% else %}
     没有用户
   {% endif %}
   
   注意:if语句支持 and 、or、==、>、<、!=、<=、>=、in、not in、is、is not判断。
   注意:if语句支持 and 、or、==、>、<、!=、<=、>=、in、not in、is、is not判断。
   注意:if语句支持 and 、or、==、>、<、!=、<=、>=、in、not in、is、is not判断。
   ```

5. with 定义一个智能关键变量

   ```python
   {% with total=business.employees.count %}
       {{ total }} employee{{ total|pluralize }}
   {% endwith %}
   ```

6. 注意事项

   ```python
   1. Django的模板语言不支持连续判断，即不支持以下写法：
   
   {% if a > b > c %}
   ...
   {% endif %}
   2. Django的模板语言中属性的优先级大于方法
   
   def xx(request):
       d = {"a": 1, "b": 2, "c": 3, "items": "100"}
       return render(request, "xx.html", {"data": d})
   如上，我们在使用render方法渲染一个页面的时候，传的字典d有一个key是items并且还有默认的 d.items() 方法，此时在模板语言中:
   
   {{ data.items }}
   默认会取d的items key的值。
   ```

7. 母版,详细请移步--><https://www.cnblogs.com/maple-shaw/articles/9333821.html>查看

#### 12.3.8 导入js/css的路径问题

- **`./`**：代表目前所在的目录
- **`. ./`**：代表上一层目录
- **`/`**：代表根目录**

1. 案例解析

   ```html
   1.文件在当前目录下,以当前项目文件为中心
   “./example.jpg” 或 “example.jpg”
   
   2.文件在上层目录
   （1）在上层目录下
   “../example.jpg”
   
   （2）在上层目录下的一个Image文件夹下
   “../Image/example.jpg”
   
   （3）在上上层目录下
   "../../example.jpg"
   
   3、文件在下一层目录
   “./Image/example.jpg”
   
   4、根目录
   “C:/Image/example.jpg”
   
   ```
   
