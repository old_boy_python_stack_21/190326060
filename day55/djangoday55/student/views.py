from django.shortcuts import render, redirect, HttpResponse
from classes import models


def stu_list(request):
    error = ''
    stu_obj_list = models.Student.objects.all().order_by('class_id_id')
    if request.method == 'POST':
        wd = request.POST.get('wd').strip()
        stu_obj_list = models.Student.objects.filter(s_name=wd)
        print(stu_obj_list)
        cla_obj_list = models.Classes.objects.filter(c_name=wd)
        if cla_obj_list:
            stu_obj_list = models.Student.objects.filter(class_id=cla_obj_list[0])
        elif all([ stu_obj_list, cla_obj_list]):
            print(all([not stu_obj_list,not cla_obj_list]))
            error = '要查询的数据不存在'
    return render(request, 'stu_list.html', {'stu_obj_list': stu_obj_list, 'error': error})


# 增加学生
def add_stu(request):
    error = ''
    cal_obj_list = models.Classes.objects.all()
    if request.method == 'POST':
        cid = request.POST.get('cid')
        s_name = request.POST.get('s_name')
        print(s_name, cid)
        if models.Student.objects.filter(s_name=s_name):
            error = '新增的学生已存在'
        elif not s_name:
            error = '新增的名称不能为空'
        if not error:
            # models.Classes.objects.create(c_name=c_name)
            obj = models.Student(s_name=s_name, class_id_id=cid)
            obj.save()
            # 新增的另一种写法
            return redirect('/stu_list/')
    return render(request, 'add_stu.html', {'error': error, 'cal_obj_list': cal_obj_list})


# 编辑学生
def edit_stu(request):
    sid = request.GET.get('id')
    error = ''
    stu_obj_list = models.Student.objects.filter(pk=sid)
    cal_obj_list = models.Classes.objects.all()
    if not stu_obj_list:
        error = '要修改的数据不存在'
    if request.method == 'POST':
        s_name = request.POST.get('s_name')
        cid = request.POST.get('cid')
        if models.Student.objects.filter(s_name=s_name):
            error = '修改后的学生名重复'
        if not s_name:
            error = '修改后的学生名称不能为空'
        if not error:
            obj = models.Student.objects.get(pk=sid)
            obj.s_name = s_name
            obj.class_id_id = cid
            obj.save()

            return redirect('/stu_list/')

    return render(request, 'edit_stu.html',
                  {'error': error, 'stu_obj_list': stu_obj_list, 'cal_obj_list': cal_obj_list})


# 删除学生
def del_stu(request):
    cid = request.GET.get('id')
    cla_obj_list = models.Student.objects.filter(pk=cid)
    if not cla_obj_list:
        return HttpResponse('删除的数据不存在')
    cla_obj_list.delete()
    return redirect('/stu_list/')
