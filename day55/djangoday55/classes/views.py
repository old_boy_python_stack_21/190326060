from django.shortcuts import render, redirect, HttpResponse
from classes import models


# 班级主页
def index(request):
    error = ''
    cla_obj = models.Classes.objects.all()
    if request.method == 'POST':
        wd = request.POST.get('wd')
        cla_obj = models.Classes.objects.filter(c_name=wd)
        if not cla_obj:
            error = '要查询的数据不存在'
    return render(request, 'index.html', {'cla_obj': cla_obj,'error':error})


# 增加班级
def add_class(request):
    error = ''
    if request.method == 'POST':
        c_name = request.POST.get('c_name')
        if models.Classes.objects.filter(c_name=c_name):
            error = '新增的名称已存在'
        elif not c_name:
            error = '新增的名称不能为空'
        if not error:
            models.Classes.objects.create(c_name=c_name)
            return redirect('/index/')
            # obj = models.Classes(c_name=c_name)
            # obj.save()
            # 新增的另一种写法

    return render(request, 'add_class.html', {'error': error})


# 编辑班级
def edit_class(request):
    cid = request.GET.get('id')
    error = ''
    cla_obj_list = models.Classes.objects.filter(pk=cid)
    if not cla_obj_list:
        error = '要修改的数据不存在'

    if request.method == 'POST':
        c_name = request.POST.get('c_name')
        if models.Classes.objects.filter(c_name=c_name):
            error = '修改后的班级名重复'
        if not c_name:
            error = '要修改后的名称不能为空'
        if not error:
            cla_obj_list = models.Classes.objects.filter(pk=cid)
            cla_obj = cla_obj_list[0]
            cla_obj.c_name = c_name
            cla_obj.save()

            return redirect('/index/')

    return render(request, 'edit_class.html', {'error': error, 'cla_obj_list': cla_obj_list})


# 删除班级
def del_class(request):
    cid = request.GET.get('id')
    cla_obj_list = models.Classes.objects.filter(pk=cid)
    if not cla_obj_list:
        return HttpResponse('删除的数据不存在')
    cla_obj_list.delete()
    return redirect('/index/')
