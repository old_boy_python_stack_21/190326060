from django.db import models


# Create your models here.

class Classes(models.Model):
    class Meta:
        db_table = 'classes'

    cid = models.AutoField(primary_key=True)
    c_name = models.CharField(max_length=32, blank=False)


class Student(models.Model):
    class Meta:
        db_table = 'student'

    sid = models.AutoField(primary_key=True)
    s_name = models.CharField(max_length=32, blank=False)
    class_id = models.ForeignKey('Classes', on_delete=models.CASCADE)
