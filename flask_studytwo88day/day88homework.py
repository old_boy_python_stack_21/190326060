# -*- coding:utf-8 -*-
# __author__ = Deng Jack

from flask import Flask, session, render_template, request, redirect, url_for

app = Flask(__name__)
app.secret_key = '\xc3i\x80\x84\x0f\x0f~!Io\x01\xbaypC\x9e.R\xe6\xd0lF50\xd9\xde\x11\xc5\xbaA\xb6}'

STUDENT_DICT = {
    1: {'name': 'Old', 'age': 38, 'gender': '中'},
    2: {'name': 'Boy', 'age': 73, 'gender': '男'},
    3: {'name': 'EDU', 'age': 84, 'gender': '女'},
}
from werkzeug.local import LocalProxy

import functools
def auth(func):
   #  @functools.wraps(func)
    def inner(*args, **kwargs):
        if session.get('user'):
            # 是登陆用户
            return func(*args, **kwargs)
        return redirect(url_for('login'))

    return inner

def one():
    print(request)

"""
中间件的方法
@app.before_request
app.before_request_funcs={None:[one]}
"""


@app.route('/login/', methods=['GET', "POST"])
def login():
    if request.method == 'POST':
        user = request.form.get('user')
        pwd = request.form.get('pwd')
        if user == 'admin' and pwd == '123':
            # 登陆成功,保存信息和登陆次数
            print(session, type(session))
            session['user'] = request.form.get('user')
            session[f'{user}_count'] = session.get(f'{user}_count', 0) + 1
            return redirect(url_for('stu_pro'))
    return render_template('login.html')


@app.route('/logout/')
def logout():
    session.pop('user')
    return redirect('/login/')


@app.route('/stu_pro/', endpoint='stu_pro')
@auth
def stu_pro():
    return render_template('stu_pro.html', stu_p=STUDENT_DICT, title='学生概况')


@app.route('/stu_info/<int:id>/', endpoint='stu_info')
@auth
def stu_info(id):
    msg = STUDENT_DICT.get(id, None)
    return render_template('stu_info.html', msg=msg, id=id, title='学生详情')


if __name__ == '__main__':
    app.debug = True
    app.run()
