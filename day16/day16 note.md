# Python全栈day 16

## 1.内容回顾

### 1.模块

- 内置模块

  - os
  - sys
  - shutil
  - json
  - random

- **面试题**

  列举常用的内置模块   :  json  /  time   /datetime   /`````````````` os/sys

- 第三方模块

  - pip的用法

### 2.自定义模块

- 对于自定义模块

  - 可以把一个py文件,或者一个包(文件夹)当做一个模块导入,方便以后调用其中的函数
  - 对于包 :
    - py2  ---->包内必须要有 _ _ _init_ _ _.py文件,
    - py3则不需要,不过为了适用性的需要,一般都在包内加上这个文件

- 导入自定义模块

  1. 模块和执行的py在同一目录,且需要调用很多函数

     ```python
     # 在1.py文件中调用xxxx.py
     import xxxx
     xxxx.func()     #调用函数
     xxxx.func2()    #调用函数2
     
     
     ```

  2. 包和执行的py在同一目录

     ```python
     from 包 import jd模块
     jd模块.func()    # 调用包内的jd模块的所有函数---
     
     from 包.jd模块 import func
     func()           #调用jd的func函数
     
     ################################
     from 包.jd模块 import func , show ,func2     #可以用都逗号连接
     func()
     show()
     func2() 
     
     
     #################################
     from 包.jd模块 import *  # 调用包.jd模块的全部功能
     
     ```

  3. - 模块和要执行的py文件在同一目录 且 需要 模块中的很多功能时，推荐用： import 模块
     - 其他推荐：from 模块 import 模块       模块.函数()
     - 其他推荐：from 模块.模块 import 函数   函数() 

  4. 注意 : 要从其他路径调用模块时的用法

     ```python
     import sys
     sys.path.append(r'D:\自建模块包\项目1模块')  # ---->把路径添加到python寻找模块时会去寻找的路径中,在调用模块
     import xxxx  # xxxx.py在'D:\自建模块包\项目1模块'目录下
     xxxx.func()
     
     # sys,path 本质是一个列表,是py解释器寻找模块时会去寻找的路径.path指定用于模块搜索路径的字符串列表。
     ```

## 2.内置模块

1. os

2. sys 

3. time

4. json

   - dumps  把python数据转换为json型的字符串  序列化

   - loads 把json字符串转换为python能够识别的数据类型  反序列还

   - 注意 :若字典或列表中有中文,序列化是想要保留中文显示

     ```python
     v = {'k1':'alex','k2':'李杰'}   #e nsure_ascii=False
     
     import json
     val = json.dumps(v,ensure_ascii=False)
     print(val)
     
     ```

   - dump(内容,文件句柄)   有2个参数

     ```python
     import json
     
     v = {'k1':'alex','k2':'李杰'}
     
     f = open('x.txt',mode='w',encoding='utf-8')
     val = json.dump(v,f)
     print(val)
     f.close()
     
     ## Json.dump()这个方法：它在底层做了两件事，一件事是将对象（列表）转换为字符串，第二件事是转换## 成功以后，将转换后的数据写入到文件中。
     
     ```

   - load

     ```python
     import json
     
     v = {'k1':'alex','k2':'李杰'}
     
     f = open('x.txt',mode='r',encoding='utf-8')
     
     data = json.load(f)
     f.close()
     
     print(data,type(data))
     
     # 做了两件事，一件事：先来读取文件里的内容，第二件事是：将读取出来的内容进行数据类型的转换。
     ```

5. hashlib

6. random

7. getpass

8. shutil

9. copy

## 3.今日内容

### 1.json 和 pickle

- json，优点：所有语言通用；缺点：只能序列化基本的数据类型 list/dict/int...   

- pickle，优点：python中所有的东西都能被他序列化（除了socket对象）；缺点：序列化的内容只有python认识。

  ```python
  import pickle
  
  ######################   dumps / loads  #######################3
  v = {1,2,3,4}
  val = pickle.dumps(v)
  print(val)
  data = pickle.loads(val)
  print(data,type(data))
  ########################  dump  / load  用法和json类似,这里不再赘述####
  ```

### 2.shutil 模块重点

```python
import shutil


##删除目录(只能是文件夹)
shutil.rmtree('test')

## 重命名
shutil.move(old,new)

##压缩文件
shutil.make_archive('压缩后文件名可以加文件路径','压缩格式zip等等','被压缩的文件/文件夹')

shutil.make_archive('zzh','zip','D:\code\s21day16\lizhong')

##解压文件
shutil.unpack_archive('要解压的文件夹',extract_dir=r'D:\code\xxxxxx\xxxx',格式:'zip')
 #                                    解压的路径
shutil.unpack_archive('zzh.zip',extract_dir=r'D:\code\xxxxxx\xxxx',format='zip')

```

举例

```python
import shutil

shutil.rmtree('4567')     # 删除目录(只能是文件夹)

shutil.move(前文件名,后文件名)   # 重命名/移动,看后一个文件是文件夹还是文件名

shutil.make_archive(r'E:\159','zip','456')   # 把py文件目录的456压缩到E:\159,即E盘的根目录下159.zip

shutil.unpack_archive('159.zip',r'F:\159\158','zip')  # 把159.zip解压文件到F:\159\158,解压格式是zip.
```

### 3.time 和 datetime 模块

1. time模块

   time.time()   时间戳,显示的是1970.1.1.凌晨0.00到现在经过的秒数,得到的是float数据类型

   time.sleep()   程序运行等待的秒数

   time.timezone   :  属 性time.timezone是当地时区（未启动夏令时）距离格林威治的偏移秒数（>0，美洲;<=0大部分欧洲，亚洲，非洲） ,和电脑设置的逝去==时区有关

2. datetime 模块

   ```python
   #!/usr/bin/env python
   # -*- coding:utf-8 -*-
   import time
   from datetime import datetime,timezone,timedelta
   
   # ######################## 获取datetime格式时间 ##############################
   """
   v1 = datetime.now() # 当前本地时间
   print(v1)
   tz = timezone(timedelta(hours=7)) # 当前东7区时间
   v2 = datetime.now(tz)
   print(v2)
   v3 = datetime.utcnow() # 当前UTC时间
   print(v3)
   """
   
   # ######################## 把datetime格式转换成字符串 ##############################
   # v1 = datetime.now()
   # print(v1,type(v1))
   # val = v1.strftime("%Y-%m-%d %H:%M:%S")
   # print(val)
   
   # ######################## 字符串转成datetime ##############################
   # v1 = datetime.strptime('2011-11-11','%Y-%m-%d')
   # print(v1,type(v1))
   
   # ######################## datetime时间的加减 ##############################
   # v1 = datetime.strptime('2011-11-11','%Y-%m-%d')
   # v2 = v1 - timedelta(days=140)
   # date = v2.strftime('%Y-%m-%d')
   # print(date)
   
   # ######################## 时间戳和datetime关系 ##############################
   # ctime = time.time()
   # print(ctime)
   # v1 = datetime.fromtimestamp(ctime)
   # print(v1)
   
   # v1 = datetime.now()
   # val = v1.timestamp()
   # print(val)
   ```

   

### 4 . 异常处理

```python
try:         # 试运行try缩进的语句,若出错,运行except Exception as e:后面的语句,避免程序出错.
    val = input('请输入数字:')
    num = int(val) 
except Exception as e:
    print('操作异常') 
```

