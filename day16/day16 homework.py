#!/usr/bin/env python
# -*- coding:utf-8 -*-	 
#@Project  : s21day16 
#@Author : Jack Deng       
#@Time   :  2019-04-18 19:05
#@File   : day16 homework.py

import json


# 1. 常用的内置函数
"""
输入输出:print input
数值转换型 : int str list tuple set dict float bool
编码 : chr ord
进制相关 : bin oct int  hex
其他 : len id type  range
高阶 : map filter reduce
"""

# 2. 常见的内置模块
"""
json / pickle / shutil  / random  /getpass   /hashlib  
/time  /datetime  /os/sys
"""

# 3. json序列化时保持中文
"""
v = [1,2,3,'邓益新']
print(json.dumps(v,ensure_ascii=False))
"""

# 4.
"""
import datetime
def regisrer():
    # 用户注册
    user = input('请输入注册用户名:')
    pwd = input('请输入注册密码:')
    now_time = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    with open('data.txt','a',encoding='utf-8') as file_data:
        file_data.write(user+'|'+ pwd +"|" + now_time +'\n')
    print('注册成功')

def login():
    # 用户登录
    count = 0
    while True :
        user = input('请输入登录用户名:')
        with open('frozen_user.txt','r',encoding='utf-8') as file1:
            for line in file1 :
                if user == line.strip():
                    print('用户被冻结,无法登录')
                    return
        pwd = input('请输入登录密码:')
        with open('data.txt', 'r', encoding='utf-8') as file_data:
            for line in file_data:
                line = line.strip().split('|')
                if user == line[0]  and  pwd == line[1]  :
                    print('登录成功')
                    return
                elif user == line[0] :
                    print('用户密码错误,请重新输入')
                    count += 1
                    if count == 3 :
                        with open('frozen_user.txt', 'a', encoding='utf-8') as file2:
                            file2.write(user+'\n')
                        print('用户被冻结')
                        return
            print('用户不存在,请注册后登录')
            return

def main_process():
    # 主程序用户管理
    info = {'1': login ,'2' : regisrer}
    while  True:
        print('******欢迎来到用户管理系统******\n1.用户登录\n2.用户注册')
        choice = input('请选择序号(输入N退出)')
        if choice == 'N': break
        if not info.get(choice):
            print('序号选择错误,请重新选择')
            continue
        info.get(choice)()

main_process()
"""
                

# 5.文件名为'goods.txt'

"""
while True:
    page = input('请输入要查看的页码(输入N退出):')
    if page == 'N': break
    with open('goods.txt','r',encoding='utf-8') as goods_file:
        content = goods_file.readlines()
        start = content[0]
        start = start.strip().split("|")
        a ,b = divmod(len(content),9)   # 9行商品一页加上文件头
        sum_page = a if b == 0 else  a+1  #总页数
        if  int(page) < 1 or int(page) > sum_page:
            print('输入页码必须在1～%d页之间,请重新输入' %sum_page)
            continue
        print(start[0], start[1])
        for i in content[int(page)*9-8:int(page)*9+1]:
            i = i.strip().split("|")
            print(i[0],i[1])
"""


# 6.
"""
while True:
    page = input('请输入要查看的页码(输入N退出):')
    if page == 'N': break
    elif int(page) < 1:
        print('页码不能小于等于0,请重新输入')
        continue
    with open('goods.txt','r',encoding='utf-8') as goods_file:
        t = 0
        print('商品 价格')
        for line in goods_file:
            if int(page)*9-8 <= t  < int(page)*9+1:
                print(' '.join(line.strip().split('|')))
            t += 1   
        if int(page)*9-8 > t:
            print("超出页面范围,请重新输入")
            continue
"""

# 7 .购物车

from datetime import datetime
import json
def shopping_car():
    SHOPPING_CAR = {}
    GOODS_LIST = [
    {'id':1,'title':'飞机','price':1000},
    {'id':3,'title':'大炮','price':1000},
    {'id':8,'title':'迫击炮','price':1000},
    {'id':9,'title':'手枪','price':1000},
    ]
    while True:
        print("*****欢迎来到购物商城,请把商品列表中的商品添加到购物车*****\n以下是商品列表:")
        list1 = []
        for i in GOODS_LIST:
            list1.append(i.get('id'))
            print(i['id'],i['title'],i['price'])
        choice = input('请选择购买商品序号(输入N结束购买,并显示购物车所有商品):')
        if choice == 'N':
            path = datetime.now().strftime('%Y{}%m{}%d{}.txt')
            path = path.format('年', '月', '日')
            with open( path ,'w',encoding='utf-8') as file_shopping:
                file_shopping.write(json.dumps(SHOPPING_CAR,ensure_ascii=False))
                print("******你购买的商品如下:******\n商品  数量")
                for a  in SHOPPING_CAR:                    
                    print( a ,SHOPPING_CAR.get(a) )
                return
        if  not choice.isdecimal() or not int(choice.strip()) in list1   :
            print('商品序号不存在,请重新选择')
            continue
        num = input('请选择购买商品数量:')
        for  i in GOODS_LIST:
            if i.get('id') == int(choice.strip()):
                SHOPPING_CAR[i['title']] = SHOPPING_CAR.get(i['title'],0)+ int(num)
                print('商品添加完成')
shopping_car()



# 8.
from datetime import datetime
import  hashlib
import json
import os

def md5(pwd):
    """
    会员密码加密
    :param pwd:
    :return: 加密后的字符串
    """
    hash = hashlib.md5('加盐的字符串'.encode('utf-8'))
    hash.update(pwd.encode('utf-8'))
    return hash.hexdigest()

#  装饰器实现商品浏览和购物车判断用户状态
def wrapper(func):
    def inner(*args,**kwargs):
        if not User_Status:
            print('只有登录后才能访问,请先登录')
            return
        v = func(*args,**kwargs)
        return v
    return inner

# 装饰器实现不可重复登录
def wrapper1(func):
    def inner(*args, **kwargs):
        if  User_Status:
            print('您已登录成功,无需再次登录')
            return
        v = func(*args, **kwargs)
        return v
    return inner


def regisrer():
    """
    用户注册
    :return:
    """
    while True:
        user = input('请输入注册用户名:')
        with open('user_data.txt','a+',encoding='utf-8') as file_data:
            file_data.seek(0)                                          # 追加模式光标默认在最后,修改到最前
            for line in file_data:
                line = line.strip().split('|')
                if user.strip() == line[0]:
                    print('用户名重复,请重新输入用户名和密码注册')
                    continue
            pwd = input('请输入注册密码:')                             #  此时没有关闭文件
            now_time = datetime.now().strftime('%Y-%m-%d %H:%M:%S')    #  加上注册时间,精确到秒
            file_data.write(user+'|'+ md5(pwd) +"|" + now_time +'\n')  # 追加模式,以|隔开,加密密码,光标自动移到最后
        print('注册成功')                                              # 文件自动关闭,注册用户数据录入文件
        return                                                         # 注册成功,结束函数

@wrapper1
def login():
    """
    用户登录
    :return:
    """
    count = 0
    while True :
        user = input('请输入登录用户名:')
        # 判断是否是冻结用户,文件frozen_user.txt存储冻结用户名单
        with open('frozen_user.txt','r',encoding='utf-8') as file1 :
            for line in file1 :
                if user == line.strip():
                    print('用户被冻结,无法登录')
                    return
        pwd = input('请输入登录密码:')
        with open('user_data.txt', 'r', encoding='utf-8') as file_data:
            list1 = []
            for line in file_data:
                line = line.strip().split('|')
                list1.append(line[0])                        # 获取所有用户名单,后续判断用户名是否存在
                if user == line[0]  and  md5(pwd) == line[1]  :
                    print('登录成功')
                    global User_Status                        # 获取用户登录状态
                    global USER                               # 获取用户名,在其他函数调用,创建用户名文件夹,存储数据
                    User_Status = True                        # 用户登录状态变更
                    USER = user
                    return
                elif user == line[0] :                        # 用户名存在,但密码错误
                    print('用户密码错误,请重新输入')
                    count += 1
                    if count == 3 :
                        with open('frozen_user.txt', 'a', encoding='utf-8') as file2:
                            file2.write(user+'\n')
                        print('用户被冻结')                  # 用户名存在,但密码错误3次,冻结
                        return
            if not user in list1 :                           # 判断用户名不存在
                print('用户名不存在,请注册后登录')
                return


@wrapper
def look_goods():
    """
    商品浏览及购买
    :return:
    """
    while True:
        page = input('请输入要查看的页码(输入N退出):')
        if page == 'N': break
        with open('goods.txt', 'r', encoding='utf-8-sig') as goods_file:
            start = goods_file.readline()
            start = start.strip().split("|")        # 读第一行文件头,注意此时光标位置
            content = goods_file.readlines()        # 小文件,读取剩下的商品,作为列表
            row = 9                                 #  每页展示9行商品+第一行说明
            a, b = divmod(len(content),row )
            sum_page = a if b == 0 else a + 1       # 总页数
            if int(page) < 1 or int(page) > sum_page:
                print('输入页码必须在1～%d页之间,请重新输入' % sum_page)
                continue
            global GOODS_LIST
            print(' '.join(start))                  # 打印第一行说明
            for i in content[(int(page)-1) * row :int(page) * row ]:
                i = i.strip().split("|")
                GOODS_LIST.add((int(i[0]),i[1],i[2]))          # (序号  商品名  价格)
                print(' '.join(i))
        shopping_car()

def shopping_car():
    global SHOPPING_CAR
    while True:
        print("*****请添加商品到购物车*****")
        list1 = []
        for i in GOODS_LIST:
            list1.append(i[0])
        choice = input('请选择购买商品序号:\n输入N结束购买,返回商品浏览:\n输入J结束购买,回购物车结算:')
        if choice == 'N':
            return
        elif choice == 'J':               #  回购物车结算
            if not SHOPPING_CAR :
                print('您还没有选择商品购买')
                return
            path1 = datetime.now().strftime('%Y-%m-%d-%H-%M.txt')
            path2 = os.path.join('shopping_car',USER)
            if not os.path.exists(path2):
                os.makedirs(path2)
            path3 = os.path.join(path2,path1)
            with open(path3, 'w', encoding='utf-8') as file_shopping:
                json.dump(SHOPPING_CAR,file_shopping, ensure_ascii=False)
            print("******你购买的商品如下:******\n商品  数量  价格")
            for key in SHOPPING_CAR:
                t = SHOPPING_CAR.get(key)
                key = key.split('|')
                key = ' '.join(key)
                print(key,t)
            return
        elif not choice.isdecimal() or not int(choice.strip()) in list1:
            print('商品序号不存在,请重新选择')
            continue
        num = input('请选择购买商品数量:')
        for i in GOODS_LIST:
            if i[0] == int(choice.strip()):
                SHOPPING_CAR[i[1]+'|'+str(i[2])] = SHOPPING_CAR.get(i[1], 0) + int(num)
                print('商品添加完成')

@wrapper
def user_shopping_car():
    """
    我的购物车
    :return:
    """
    path = os.path.join('shopping_car',USER)
    if not os.path.exists(path):
        os.makedirs(path)               # 用户名文件夹不存在则创建
    for i in os.listdir(path):          # 用户名路径的下一层目录,都是txt文件
        print(i.replace('.txt',''))
        i = os.path.join(path ,i)       # 获取txt文件路径,并打开
        with open(i,'r',encoding='utf-8') as file:
            data = json.load(file)
            for i in data:
                print('    '+i+'|',data.get(i))


def main_process():
    """
    主程序
    :return:
    """
    info = {'1': login, '2': regisrer, '3': look_goods, '4': user_shopping_car}
    while True:
        print('******欢迎来到沙河超市******\n1.用户登录\n2.用户注册\n3.商品浏览\n4.购物车查看')
        choice = input('请选择功能序号(输入N退出)')
        if choice == 'N': break
        if not info.get(choice):
            print('序号选择错误,请重新选择')
            continue
        info.get(choice)()



GOODS_LIST = set()          # 在用户浏览商品的时候,商品添加到GOODS_LIST中,集合中,去重
SHOPPING_CAR = {}          # 用户购物车
User_Status = False        # 没有登录,默认False
USER = None                # 用户名操作
main_process()










"""

# 9.
import xlrd
workbook = xlrd.open_workbook('python21期班级学员.xlsx')
print(workbook.sheet_names())   # 打开excel文件并获取所有sheet  ['班级学员表']

sheet1_name = workbook.sheet_names()[0]
sheet1 = workbook.sheet_by_index(0)
print(sheet1.name,sheet1.nrows,sheet1.ncols)   #根据sheet索引或者名称获取sheet内容，同时获取sheet名称、行数、列数

print(sheet1.cell(1,1))  # 根据行数和列数取值

"""