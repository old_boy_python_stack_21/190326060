

# 1. 让python解释器把/root/mods路径加入到系统找寻模块的路径列表中来,方便以后调用第三方模块

# 2.步长
v = '你是个好人'
print(v[::-1])

# 3. a,b = b,a

# 4. 万能参数,拆包用的,
# *args能接受任意个数的位置参数,并将其转换为元组
# **kwargs 能接受任意个数的关键字参数,并将其转换为字典

# 5. 参数传递的是地址

# 6 . {'a':1,'b':1,'c':2}

# 7
#  lambda  参数 : 代码   ,自动将:后面的结果作为返回值return  ,只能用一行代码.也叫做匿名函数.

# 8.
# py2 有range 和  xrange
# range()   一旦创建,就在内存中创建了列表
# xrange()    ,在访问的时候才生成对应的值
# py3 只有range

# 9.
get_list = '1,2,3'.split(',')
print(get_list)

# 10 .
get2_list =list(map(lambda x : int(x) ,['1','2','3']))
print(get2_list)

# 11. def f(a,b = [])
# Python函数在定义的时候。默认参数b的值就被计算出来了，即[],
# 因为默认参数b也是一个变量，它指向对象即[],每次调用这个函数，如果改变b的内容每次调用时候默认参数也就改变了，不在是定义时候的[]了.应该换成None  .

# 12.
get3_list = [ x*x  for x in range(1,11)]
print(get3_list)

# 13.
get4_list = [i for i in range(1,101) if i%2 == 0 ]
get5_list = list(filter(lambda x : x % 2 == 0 ,range(0,101)))


# 14.
def func():
    result = []
    for i in range(10):
        if i % 3 == 0:
            result.append(i)
    return result
v = lambda : [ i for i in range(10)  if  i % 3 == 0 ]


# 15
"""
import shutil
shutil.rmtree(path = '文件路径')
"""
# 16 .
"""
import os
os.rename(old_path,new_path) #只能对相应的文件进行重命名, 不能重命名文件的上级目录名.
##   os.renames(old_path,new_path)  #是os.rename的升级版, 既可以重命名文件, 也可以重命名文件的上级目录名
"""

# 17 .
import random
print(random.randint(1,6))
print(random.random())  # 取0-1之间的随机实数
print(random.randrange(1,10))

# 18.
get6_list = set()
while True :
    get6_list.add(random.randint(0,99))
    if len(get6_list) == 10 :break
print(get6_list)

# 19.
"""
for i in range(1,10):
    for j in range(1,i+1):
        print('%s*%s' %(i,j,) ,end = ' ')
    print('')
"""
list1 = ('%s*%s ' %(i,j,) if i!=j  else '%s*%s\n' %(i,j,) for i  in range(1,10)  for j in range(1,i+1))

print(i for i in list1)







"""
# 20.
def dict_updater(k,v,dic={}):
    dic[k] = v
    print(dic)
dict_updater('one',1)   # dic = {'one':1}
dict_updater('two',2)   # dic = {'one':1 ,'two': 2}
dict_updater('three',3,{})   # dic = {'three': 3 }
# 当函数定义时,作用域内创建了变量dic={},前2次调用函数, 没有给出dic.默认使用的创建时的变量.改变了dic的值.
#  第三次调用,给出dic = {} ,使用的是给的参数 .

# 21 .

def xxx(counter):
    def wrapper(func):
        def inner(*args,**kwargs):
            get7_list = []
            for i in range(5):
                v = func(*args,**kwargs)
                get7_list.append(v)
            return get7_list
        return inner
    return wrapper

@xxx(9)
def index(al):
    print('%s是个什么东西' %al)
    return  666

index('小明')


# 22
def wrapper(func):
    def inner(*arg,**kwargs):
        v = func(*arg,**kwargs)
        print('它的元素个数是%s个' %v)
        return v
    return  inner

@wrapper
def index(al):
    return len(al)

index('我的小名叫大奔')   # 它的元素个数是7个

# 23.
def log(func):
    def inner(*args,**kwargs):
        print(str(func))
        v = func(*args,**kwargs)
        return v
    return inner

@log
def now():
    print('2013-12-25')

now()

"""
# 24.

import requests
import json

response = requests.get('https://www.luffycity.com/api/v1/course_sub/category/list/')
print(response.text)

file = open('catelog.txt','w',encoding='utf-8')
for i in (json.loads(response.text)['data']):
    file.write(i['name']+'\n')
file.close()

# 25.博客园 52破解  csdn博客

# 26. 爬虫技术  ,在b站看视频

# 27.微信公众好:码农翻身
小程序:python入门教程,每日一问


