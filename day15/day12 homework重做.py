#!/usr/bin/env python
# -*- coding:utf-8 -*-

# 22.程序设计题
"""
> 请设计实现一个商城系统，商城主要提供两个功能：商品管理、会员管理。
>
> 商品管理：
>
> - 查看商品列表
> - 根据关键字搜索指定商品
> - 录入商品
>
> 会员管理：【无需开发，如选择则提示此功能不可用，正在开发中，让用户重新选择】

需求细节：

1.
启动程序让用户选择进行商品管理
或
会员管理，如：
![输入图片说明](https: // images.gitee.com / uploads / images / 2019 / 0412 / 171126_0567693
f_73459.png
"屏幕截图.png")
2.
用户选择 【1】 则进入商品管理页面，进入之后显示商品管理相关的菜单，如：
![输入图片说明](https: // images.gitee.com / uploads / images / 2019 / 0412 / 171201_47
d7aa64_73459.png
"屏幕截图.png")
3.
用户选择【2】则提示此功能不可用，正在开发中，让用户重新选择。
4.
如果用户在【商品管理】中选择【1】，则按照分页去文件
goods.txt
中读取所有商品，并全部显示出来【分页功能可选】。
5.
如果用户在【商品管理】中选择【2】，则让提示让用户输入关键字，输入关键字后根据商品名称进行模糊匹配，如：
![输入图片说明](https: // images.gitee.com / uploads / images / 2019 / 0412 / 171227_049
ea3f5_73459.png
"屏幕截图.png")
6.
如果用户在【商品管理】中选择【3】，则提示让用户输入商品名称、价格、数量
然后写入到
goods.txt
文件，如：
![输入图片说明](https: // images.gitee.com / uploads / images / 2019 / 0412 / 171251
_b1080b58_73459.png
"屏幕截图.png")
"""
import  hashlib
import getpass
import sys

###### 1. 商品管理
def func_goods():
    """
    商品管理
    :return:
    """
    info = {'1': look_goods, '2': check_key, '3': enter_goods}
    while True:
        print("""******欢迎使用老子的购物商城【商品管理】******
        1. 查看商品列表
        2. 根据关键字搜索指定商品
        3. 录入商品""")
        choice = input('请选择(输入N返回上一级):').strip()
        if choice == 'N': return        # 返回上一级(死循环),不需返回值,终止函数
        func = info.get(choice)
        if not func : print('序号输入错误,请重新输入');continue
        func()


# 1.1 商品列表功能(默认10行一页)
def look_goods():
    """
    查看商品列表
    :return:
    """
    while True:
        print("""******欢迎使用老子的购物商城【商品管理】【查看商品列表】******""")
        page = input('请输入查看的页码:(输入N返回上一级)').strip()
        if page == 'N': return
        if not page.isdecimal():print('输入错误字符,请重新输入') ; continue
        page = int(page)
        with open('goods.txt','r',encoding='utf-8') as file:
            data = file.readlines()
            a, b = divmod(len(data), 10)    #   列表长度是行数,规定10行一页
            if b != 0:
                a += 1           # 确定总共有多少页---->a
            if page > a or page < 1:print('输入页码必须在1～%d页之间,请重新输入页码' %a) ; continue
            content = data[page*10-10:page*10-1]
            content = ''.join(content)
            print(content)


# 1.2 关键字检索
def check_key():
    while True:
        print("""******欢迎使用老子的购物商城【商品管理】【根据关键字搜索】******""")
        key = input('请输入要查询的关键字(输入N返回上一级)：')
        if key == 'N':return   # 返回上一级
        print('***搜索结果如下***')
        with open('goods.txt','r',encoding = 'utf-8') as file:
            for line in file:          # 遍历所有商品,商品中含有key关键字的商品全部打印出来,
                line = line.strip()
                line = line.split('|')
                if key in line[0]:
                    print(' '.join(line))


# 1.3 录入商品功能
def enter_goods():
    while True:
        print("""******欢迎使用老子的购物商城【商品管理】【录入商品】******""")
        name = input('请录入商品名称(输入N则停止录入,并返回上一级):')
        if name == 'N': return      # 返回上一级.
        price = input('请录入商品价格:')
        count = input('请录入商品数量:')
        with open('goods.txt','a',encoding='utf-8') as file:
            data = name +'|'+ price + '|' + count + '\n'    # 在goods.txt中,数据以|分割.
            file.write(data)
        print('添加成功')

#######  2 会员管理(实现登录/注册)
def vip_charge():
    """
    会员管理
    :return:
    """
    info = {'1': login, '2': register}
    while True:
        print("******欢迎使用老子的购物商城【会员管理】【登录/注册】******")
        choice = input('1 .会员登录\n2 .会员注册(输入N返回上一级)\n******请输入你选择的功能:******:')
        if choice.strip() == 'N': return     #     停止运行当前函数,返回上一级,默认返回值 None
        func = info.get(choice)
        if not func : print('序号输入错误,请重新输入') ; continue
        func()


# 2.1登录函数,参数为user和pwd
def login():
    """
    登录程序
    :return: True/None
    """
    global VIP_login_status
    while True:
        if  VIP_login_status : print('您已登录成功,无需再次登录') ; return
        user = input("请输入登录用户名(输入N返回上一级):")
        if user.strip() == 'N': return
        pwd = input("请输入登录密码")    #  getglass.getglass在window上py3的 啥都没有
        with open('VIP_data.txt','r',encoding='utf-8') as file:
            for line in file:
                line = line.strip().split('|')
                if user == line[0] and md5(pwd) == line[1]:
                    VIP_login_status = True
                    print('登陆成功')
                    return
            print('用户名或密码错误,请重新登录') ; continue

# 2.2注册功能
def register():
    """
    会员注册
    :return: True/None
    """
    while True:
        user = input("请输入注册用户名:")
        pwd = input("请输入登录密码")         # getglass.getglass在window上py3的 啥都没有
        with open('VIP_data.txt','a+',encoding='utf-8') as file:
            file.seek(0)
            for line in file:
                line = line.strip().split('|')
                if user.strip() == line[0]:print('用户名重复,请重新输入用户名和密码注册');continue
            content = ''.join([user, '|', md5(pwd), '\n'])
            file.write(content)     # 光标自动移到最后
            return True

# 2.3加密会员密码
def md5(pwd):
    """
    会员密码加密
    :param pwd:
    :return: 加密后的字符串
    """
    hash = hashlib.md5('加盐的字符串'.encode('utf-8'))
    hash.update(pwd.encode('utf-8'))
    return hash.hexdigest()



#########   主程序,启动入口   #######
def main():
    info = {'1': func_goods, '2': vip_charge}
    while True:
        print("""******欢迎使用老子的购物商城******
        1. 商品管理
        2. 会员管理""")
        content = (input("请选择(输入N退出系统):")).strip()
        if content == 'N': sys.exit(0)      # 正常退出系统
        func = info.get(content)
        if not func: print('序号输入错误,请重新输入') ; continue
        func()

VIP_login_status = False
main()
