# python day 15  

## 一.模块

1. 内置模块

   ```python
   # 重点:  命令行参数输入
   import sys
   t1 = sys.argv[1]
   t2 = sys.argv[2]
   ```

2. 第三方模块

   - 下载和安装

     pip的使用

     - ```python
       # 把pip.exe 所在的目录添加到环境变量中。
       
       pip install 要安装的模块名称  # pip install requests
       ```

     - python36  -m pip install --upgrade pip 语句的使用

     - python3 -m pip install -U --force-reinstall pip   若pip更新失败,则如此

     - 网络不好一直抱超时错误的话可以换一个源，粘贴下边代码运行
       pip3 install xlrd -i http://pypi.douban.com/simple/ —trusted-host pypi.douban.com

       #### 注意 : 这里的pip  /  pip3     都是自己定义的环境变量

     - 安装完成后，如果导入不成功。

       - 重启pycharm。
       - 安装错了。

3. 自定义模块的导入

   xxxx.py  # 自定义模块,实现某功能

   ```python
   def func():
       print(123)  #实现打印123
   ```

   x2.py 

   ```python
   # 在x2.py中希望调用xxxx.py的函数
   ####1. xxxx.py 和  x2.py 在同一文件夹下
   import xxxx
   xxxx.func()
   
   ####2. 不在同一文件夹下
   import sys
   sys.path.append('xxxx.p文件的绝对路径')
   import xxxx
   xxxx.func()
   
   #####在cmd中运行x2.py文件
   python3 x2.py
   
   ```

4. 内置模块补充

   - sys

     1. sys.argv
     2. sys.path    # 默认Python去导入模块时，会按照sys.path中的路径挨个查找。 

   - json 模块

     json的本质(为着各程序间信息的传递而规定的一种通用格式)

     1. 本质是一个字符串(看起来像[],{},int/ bool )
     2. 最外层必须是list/dict
     3. 如果包含字符串,必须双引号--->其他程序字符串只识别双引号

     用法

     ```python
     import json
     # 序列化，将python的值转换为json格式的字符串。
     # v = [12,3,4,{'k1':'v1'},True,'asdf']
     # v1 = json.dumps(v)
     # print(v1)
     json.dumps()    ---->返回值是json格式--->字符串
     
     
     # 反序列化，将json格式的字符串转换成python的数据类型
     # v2 = '["alex",123]'
     # print(type(v2))
     # v3 = json.loads(v2)
     # print(v3,type(v3))
     json.loads()   ---->返回值是python能识别的格式,外层一定是[]或{}
     
     ```

     python与json的对照表

     ```python
         +-------------------+---------------+
         | Python            | JSON          |
         +===================+===============+
         | dict              | object        |
         +-------------------+---------------+
         | list, tuple       | array         |
         +-------------------+---------------+
         | str               | string        |
         +-------------------+---------------+
         | int, float        | number        |
         +-------------------+---------------+
         | True              | true          |
         +-------------------+---------------+
         | False             | false         |
         +-------------------+---------------+
         | None              | null          |
         +-------------------+---------------+
     ```

   - os模块的用法

     1. os.makedirs,创建目录和子目录--->比makedir 好用,只能创建一层目录

        ```python
        import os
        file_path = r'E:\小莫的黄片库\绝对不能偷看\请输入密码\****.MP4'
        
        file_folder = os.path.dirname(file_path)
        if not os.path.exists(file_folder):
            os.makedirs(file_folder)
        
        with open(file_path,mode='w',encoding='utf-8') as f:
            f.write('asdf')
        
        ```

     2. os.rename  重命名

        ```python
        import os
        os.rename('db','sb')    # 原文件 ,现文件
        ```

     3. os.path.join

     4. os.path.dirname

     5. os.path.abspath

     6. os.path.exists

     7. os.stat('文件路径')

     8. os.listdir

     9. os.walk

