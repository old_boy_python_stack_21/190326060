bat常用命令

##### 1.rem 和 ::注释

rem能够回显,::不能回显

##### 2.@和echo

@关闭该命令的回显(显示)

1. echo  on|off  >打开回显或关闭回显功能,在前面加@关闭该命令回显

2. echo   >显示echo设置状态

3. echo  提示信息   >类似以控制台打印

4. 在控制台输入echo off,能够关闭dos提示符的显示使屏幕只留下光标，直至键入echo on，提示符才会重新出现。

5. echo.  >回车,输入一个空行,，：；”／[\]＋等任一符号替代。

6. echo 答复语|命令文件名 >简化人机交互

7. echo 文件内容 > 文件名    >新建文件,把内容添加到文件中

   echo 文件内容 >> 文件名  > 把内容追加到到文件中

8. echo ^g  >使喇叭鸣响 ,“^g”是在dos窗口中用ctrl＋g或alt＋007输入，输入多个^g可以产生多声鸣响。使用方法是直接将其加入批处理文件中或做成批处理文件调用。

##### 3.pause 暂停一下

```bat
1.
pause
::display请按任意键继续. . .


2.
::要显示其他提示语，可以这样用：
echo 其他提示语 & pause > nul
del nul
```

##### 4.errorlevel 获取程序运行返回码

```bat
echo %errorlevel%
::每个命令运行结束，可以用这个命令行格式查看返回码,默认0,出错是1
```

##### 5.title cmd窗口运行时的标题

##### 6.color  2个16进制数字,前者背景色,后者前景

```bat
0 = 黑色       8 = 灰色
1 = 蓝色       9 = 淡蓝色
2 = 绿色       a = 淡绿色
3 = 湖蓝色     b = 淡浅绿色
4 = 红色       c = 淡红色
5 = 紫色       d = 淡紫色
6 = 黄色       e = 淡黄色
7 = 白色       f = 亮白色
```

##### 7.mode 配置系统设备,了解即可

设置cmd宽口大小

```bat
MODE CON: COLS=宽度 LINES=高度
```

##### 8.goto 和 :跳转到标签

##### 9.find 在文件中搜索字符串

```bat
1.在文件中搜索字符串。
find [/v] [/c] [/n] [/i] [/off[line]] "要搜索的文字串" [[drive:][path]filename[ ...]]
   /v        #显示所有未包含指定字符串的行。
   /c        #仅显示包含字符串的行数。
   /n        #显示行号。
   /i        #搜索字符串时忽略大小写。
   /off[line] #不要跳过具有脱机属性集的文件。
   "string"   #指定要搜索的文字串，
   [drive:][path]filename   #指定要搜索的文件
   
2.和type连用
type [drive:][path]filename | find "string" [>tmpfile]     #挑选包含string的行
type [drive:][path]filename | find /v "string"             #剔除文件中包含string的行
type [drive:][path]filename | find /c                      #显示文件行数 
```

##### 10. start 和 call

1. start >处理中调用外部程序的命令（该外部程序在新窗口中运行，批处理程序继续往下执行，不理会外部程序的运行状况），如果直接运行外部程序则必须等外部程序完成后才继续执行剩下的指令

2. call >call命令可以在批处理执行过程中调用另一个批处理，当另一个批处理执行完后，再继续执行原来的批处理

   ```bat
   1. call command
   调用一条批处理命令，和直接执行命令效果一样，特殊情况下很有用，比如变量的多级嵌套，见教程后面。在批处理编程中，可以根据一定条件生成命令字符串，用call可以执行该字符串，见例子。
   
   2.call [drive:][path]filename [batch-parameters]
   调用的其它批处理程序。filename 参数必须具有 .bat 或 .cmd 扩展名。
   
   3.call :label arguments
   调用本文件内命令段，相当于子程序。被调用的命令段以标签:label开头
   以命令goto :eof结尾。
   ```

##### 11. md  pushd  popd

```bat
1.md  diver:dir
2.切换当前目录 pushd 新目录  ># 保存当前目录,并切换到新目录
3.popd #恢复当前目录为刚才保存的目录,和pushd连用
```

##### 12.if 语句 + choice

```bat
1.if [not] errorlevel number command
    # if errorlevel这个句子必须放在某一个命令的后面，执行命令后由if errorlevel 来判断命令的返回值。
    # number的数字取值范围0~255，判断时值的排列顺序应该由大到小。返回的值大于等于指定的值时，条件成立

2.if [not] string1==string2 command
    # 英文内字符的大小写将看作不同,2个等于号,绝对相等

3.if [not] exist filename command
	#	为文件或目录存在的意思
	
4.choice 
	CHOICE /C YNC /M "确认请按 Y，否请按 N，或者取消请按 C。"
	确认请按 Y，否请按 N，或者取消请按 C。 [Y,N,C]?
	/m 加提示语
	/cs 区分大小写,默认不区分
	/n 在提示符中隐藏选项列表。提示前面的消息得到显示，
                       选项依旧处于启用状态。
   CHOICE /C ab /M "选项 1 请选择 a，选项 2 请选择 b。"
   CHOICE /C ab /N /M "选项 1 请选择 a，选项 2 请选择 b。"
   

```

##### 13 setlocal 和 变量延迟

在复合语句中,没有变量延迟,变量的值是执行到上一句是变量的值

```bat
setlocal enabled elayed expansion
并且变量要用!变量!括起来
# 由于启动了变量延迟，所以批处理能够实时感知到动态变化
1.用法一 :交换两个变量的值，且不用中间变量
	
```

##### 14.for循环

```bat
FOR %variable IN (set) DO command [command-parameters]
    %variable   指定一个单一字母可替换的参数。
    (set)       指定一个或一组文件。可以使用通配符。
    command     指定对每个文件执行的命令。
    command-parameters
             为特定命令指定参数或命令行开关。
```

参数:for有4个参数 /d   /l   /r   /f   他们的作用我在下面用例子解释

1. 参数/d

   for /d %%variable in (set) do command [command-parameters]

   /D参数只能显示当前目录下的目录名字,这个大家要注意!

2. /r

   FOR /R [[drive:]path] %%variable IN (set) DO command [command-parameters]

   检查以 [drive:]path 为根的目录树，指向每个目录中的FOR 语句。如果在 /R 后没有指定目录，则使用当前目录。如果集仅为一个单点(.)字符，则枚举该目录树。

   **注意:**他可以把当前或者你指定路径下的文件名字全部读取,注意是文件名字,有什么用看例子!

   > @echo off
   > for /r c:\ %%i in (*.exe) do echo %%i
   > pause
   >
   > @echo off
   > for /r c:\ %%i in (boot.ini) do if exist %%i echo %%i
   > pause
   >
   > 列举C盘下所有boot.ini存在的目录

3. /L参数

   FOR /L %%variable IN (start,step,end) DO command [command-parameters]
   该集表示以增量形式从开始到结束的一个数字序列。

   (起始值,增量,终止值)-->(1,1,5) 将产生序列 1 2 3 4 5  /   

   (5,-1,1) 将产生序列 (5 4 3 2 1)

4. /F参数

   迭代及文件解析

   FOR /F ["options"] %%variable IN (file-set) DO command [command-parameters]
   FOR /F ["options"] %%variable IN ("string") DO command [command-parameters]
   FOR /F ["options"] %%variable IN ('command') DO command [command-parameters]

   带引号的字符串"options"包括一个或多个
   指定不同解析选项的关键字。这些关键字为:
   eol=c - 指一个行注释字符的结尾(就一个)(备注：默认以使用；号为行首字符的为注释行)
   skip=n - 指在文件开始时忽略的行数，(备注：最小为1，n可以大于文件的总行数，默认为1。)
   delims=xxx - 指分隔符集。这个替换了空格和跳格键的默认分隔符集。
   tokens=x,y,m-n - 指每行的哪一个符号被传递到每个迭代
   的 for 本身。这会导致额外变量名称的分配。m-n
   格式为一个范围。通过 nth 符号指定 mth。如果
   符号字符串中的最后一个字符星号，
   那么额外的变量将在最后一个符号解析之后
   分配并接受行的保留文本。经测试，该参数最多
   只能区分31个字段。(备注：默认为1，则表示只显示分割后的第一列的内容，最大是31，超过最大则无法表示)
   usebackq - 使用后引号（键盘上数字1左面的那个键`）。
   未使用参数usebackq时：file-set表示文件，但不能含有空格
   双引号表示字符串，即"string"
   单引号表示执行命令，即'command'
   使用参数usebackq时：file-set和"file-set"都表示文件
   当文件路径或名称中有空格时，就可以用双引号括起来
   单引号表示字符串，即'string'
   后引号表示命令执行，即`command`







##### 15 通配符 和其他

*表示一个字符串              ?表示多个字符,	

**注意:配符只能通配文件名或扩展名，不能全都表示**

#### 备注 : 常用命令

1. tree,以图形显示驱动器或路径的文件夹结构。

   `TREE [drive:][path] [/F] [/A]`

   - /F   显示每个文件夹中文件的名称。
   - /A   使用 ASCII 字符，而不使用扩展字符。

2. md 创建目录

3. CD切换不同盘符时候需要加上/d

4. Xcopy C:\bat_example2 D:\bat_example2  /s /e /y

   在复制文件的同时也复制空目录或子目录，如果目标路径已经有相同文件了，使用覆盖方式而不进行提示

   RD /Q /S bat_example2   安静模式删除所有目录,包含目录树

5.  /Y 表示目标路径存在该文件则不提示直接覆盖
   `COPY /Y tree_list2.txt + tree_list3.txt C:\`

6. del /f 强制删除文件,/q安静模式,/s表示子目录都要删除该文件 

   `del filepath /f /s /q /a`

   ```bat
   move不能跨分区移动文件夹
   要移动至少一个文件:
   MOVE [/Y | /-Y] [drive:][path]filename1[,...] destination
   
   要重命名一个目录:
   MOVE [/Y | /-Y] [drive:][path]dirname1 dirname2
   ```

7. 网络命令

   ```bat
   PING www.baidu.com
   IPCONFIG #Windows IP 配置
   arp #显示和修改地址解析协议(ARP)使用的“IP 到物理”地址转换表。
   ```

8. 系统相关

   ```bat
   NET USER #显示计算机用户
   TASKLIST # 显示进程列表
   ```

9. 命令和返回值

   ```bat
   backup 
   0 备份成功 
   1 未找到备份文件 
   2 文件共享冲突阻止备份完成 
   3 用户用ctrl-c中止备份 
   4 由于致命的错误使备份操作中止
   
   diskcomp 
   0 盘比较相同 
   1 盘比较不同 
   2 用户通过ctrl-c中止比较操作 
   3 由于致命的错误使比较操作中止 
   4 预置错误中止比较
   
   diskcopy 
   0 盘拷贝操作成功 
   1 非致命盘读/写错 
   2 用户通过ctrl-c结束拷贝操作 
   3 因致命的处理错误使盘拷贝中止 
   4 预置错误阻止拷贝操作
   
   format 
   0 格式化成功 
   3 用户通过ctrl-c中止格式化处理 
   4 因致命的处理错误使格式化中止 
   5 在提示“proceed with format（y/n）?”下用户键入n结束
   
   xcopy 
   0 成功拷贝文件 
   1 未找到拷贝文件 
   2 用户通过ctrl-c中止拷贝操作 
   4 预置错误阻止文件拷贝操作 
   5 拷贝过程中写盘错误
   ```

#### 备注2: 开机自启动脚本的创建

```html
例如我们要开机自启动一个脚本：C:\abc\script.bat。 

如果直接开机启动该脚本会弹出一个黑框，我们希望能后台执行它。 

此时我们需要建一个.vbs脚本来后台执行该脚本，脚本内容为： 
复制代码 代码如下:

set ws=WScript.CreateObject("WScript.Shell") 
ws.Run "C:\abc\script.bat /start",0 

然后将该文件保存为script.vbs，放入“开始 --> 所有程序 --> 启动”内即可。
```

#### 备注3 :常用命令总结

##### 目录命令

1. d/mkdir
   作用：创建一个子目录（make directory）。
   语法：md[C:][path]〈subPath〉

2. cd
   作用：改变或显示当前目录（change directory）。
   语法：cd [C:][path]
   PS：路径可以使用绝对路径和相对路径两种。
   cd\ 表示退回到根目录。
   cd.. 表示退回到上级目录。
   如果只有cd而没有参数，则只显示当前路径。
   注意：子目录中一定有两个“特殊目录”，即“.”“..”，其中一点表示当前目录，两点表示上一层目录。
   从简单实用的角度来看，我们只要学会逐层进入（cd 下一层某目录名），和逐层退出（cd..）就可以解决所有问题。

3. rd
   作用：删除空子目录（remove directory）。
   语法：rd [c:][path]
   PS：rd是专门删除空子目录的命令。
   del 删除文件命令。
   注意两条：一是不能删除非空目录；二是不能删除当前目录。

4. dir
   作用：主要用来显示一个目录下的文件和子目录。(directory)
   语法：`dir [C:][path][filename][/o][/s][/p][/w][/a]`
   PS：斜杠表示后面的内容是参数。
   /p 显示信息满一屏时，暂停显示，按任意键后显示下一屏
   /o 排序显示。o后面可以接不同意义的字母
   /w 只显示文件名目录名，每行五个文件名。即宽行显示
   /s 将目录及子目录的全部目录文件都显示
   /a 显示隐藏文件

5. path
   作用：设备可执行文件的搜索路径，只对文件有效。
   语法：path[盘符1：][路径1][盘符2：][路径2]…
   PS：当运行一个可执行文件时，dos会先在当前目录中搜索该文件，若找到则运行之；若找不到该文件，则根据path命令所设置的路径，顺序逐条地到目录中搜索该文件

6. tree
   作用：显示指定驱动器上所有目录路径和这些目录下的所有文件名。
   语法：tree [盘符：][/f][>prn]

7. deltree
   作用：删除目录树。
   语法：DELTREE [C1:][path1]
   PS：这个命令将整个指定目录树全部消灭，而不管它是否是只读、隐藏与否。使用应特别小心。它是一个危险命令。

8. tasklist
   作用：将整个计算机的进程显示出来，同任务管理器。
   语法：tasklist


##### 磁盘命令

1. format
   作用：磁盘格式化。
   语法：`format〈盘符：〉[/s][/4][/q]`

2. unformat
   作用：对进行过格式化误操作丢失数据的磁盘进行恢复。
   语法：`unformat〈盘符〉[/l][/u][/p][/test]`

3. chkdsk
   作用：显示磁盘状态、内存状态和指定路径下指定文件的不连续数目。
   语法：`chkdsk [盘符：][路径][文件名][/f][/v]`
   PS：PS：例如要检查A盘使用情况，就输入chkdsk　A: ，检查c盘使用情况，就输入chkdsk　C: ，如果直接输入chkdsk，就检查当前磁盘的使用情况。

4. diskcopy　
   作用：复制格式和内容完全相同的软盘。
   语法：`diskcopy[盘符1：][盘符2：]`

5. label
   作用：建立、更改、删除磁盘卷标。
   语法：`label[盘符：][卷标名]`

6. vol
   作用：查看磁盘卷标号。
   语法：vol[盘符：]

7. scandisk
   作用：检测磁盘的fat表、目录结构、文件系统等是否有问题，并可将检测出的问题加以修复。
   语法：scandisk[盘符1：]{[盘符2：]…}[/all]

8. defrag
   作用：整理磁盘，消除磁盘碎块。
   语法：`defrag[盘符：][/f]`
   PS：选用/f参数，将文件中存在盘上的碎片消除，并调整磁盘文件的安排，确保文件之间毫无空隙。从而加快读盘速度和节省磁盘空间。

9. sys
   作用：将当前驱动器上的dos系统文件io.sys,msdos.sys和command 传送到指定的驱动器上。
   语法：`sys[盘符：]`


##### 文件命令

1. copy
   作用：拷贝一个或多个文件到指定盘上。
   语法：`copy [源盘][路径]（源文件名） [目标盘][路径](目标文件名）`

2. xcopy
   作用：复制指定的目录和目录下的所有文件连同目录结构。
   语法：xcopy [源盘：]〈源路径名〉[目标盘符：][目标路径名][/s][/v][/e]
   PS：xcopy是copy的扩展，可以把指定的目录连文件和目录结构一并拷贝，但不能拷贝隐藏文件和系统文件。

3. type
   作用：显示ascii码文件的内容。
   语法：type [C:][path][filename.ext]
   PS：type命令用来在屏幕上快速、简便地显示文本文件的内容，扩展名为TXT的文件是文本文件。

4. ren
   作用：对指定磁盘、目录中的一个文件或一组文件更改名称（rename）。
   语法：ren[盘符：][路径]〈旧文件名〉〈新文件名〉
   PS：改名操作只限于某个文件某组文件的名称，它不会更改文件所在的目录。

5. fc
   作用：比较文件的异同，并列出差异处。
   语法：fc[盘符：][路径名]〈文件名〉[盘符：][路径名][文件名][/a][/c][/n]

6. attrib
   作用：修改指定文件的属性。
   语法：attrib[文件名][r][—r][a][—a][h][—h][—s]

7. del
   作用：删除指定的文件。
   语法：del[盘符：][路径]〈文件名〉[/p]

8. undelete
   作用：恢复被误删除文件。
   语法：undelete[盘符：][路径名]〈文件名〉[/dos][/list][/all]

##### 其他命令

```bat
cls——清屏幕命令
ver查看系统版本号命令
date日期设置命令
	date[mm——dd——yy]
time系统时钟设置命令
	time[hh：mm：ss：xx]
mem显示系统的硬件和操作系统的状况。
	mem[/c][/f][/m][/p]
msg显示系统的硬件和操作系统的状况。
	msg[/s]
```

1. ping命令

   Ping命令的独特用法
   作用：Ping命令不仅可以测试网络是否通，而且还可以粗略的判断网络传输质量。
   语法：ping +空格+“IP地址或者域名” [-t][-l][-n]
   PS：-t：不停的Ping对方的机器，直到用户按Ctrl＋C键终止。因为如果想用Ping命令测试网络传输质量，至少要查看Ping命令三分钟到五分钟的结果。
   -l：定义echo数据包大小。我们可以将数据包的大小定义在极限值附近，以此可以测试出网络传输质量的优劣，尤其是测试外网的传输质量，非常明显。
   -n：在默认情况下，Ping命令一般都会发送四个数据包，通过这个命令可以自己定义发送的个数，对测试网络传输质量很有帮助。我们结合实例说明一下如何通过Ping命令的测试结果判断网络传输质量。

2. tracert命令的使用技巧
   作用：tracert命令可以测试路由器的工作是否正常（部分网站无法访问）。
   我们根据返回的结果来判断，哪一个环节的网络出现了问题。
   语法：tracert +空格+“IP地址或者域名”

3. 用netstat命令判断是否被攻击
   作用：netstat命令可以查看单位的网络是否被攻击。
   语法：netstat [-a][-n][-b]
   PS：-a：显示所有连接和监听端口
   　　-n：以数字形式显示地址和端口号
   　　-b：显示包含于创建每个连接或监听端口的可执行组件。另外，使用该参数之后，还可以显示占用TCP协议端口的一些程序名称

4. 巧用ARP命令防范ARP病毒
   作用：Ping命令不仅可以测试网络是否通，而且还可以粗略的判断网络传输质量。
   语法：arp -s ip地址 MAC

5. 灵活使用ipconfig命令
   作用：ipconfig这个命令查看计算机当前的网络配置信息。
   ps：Ipconfig /all：完全显示计算机的网络信息，IP地址、MAC地址及其他相关的信息，都可以显示出来。
   　　Ipconfig /release：释放计算机当前获得的IP地址。对于使用动态IP地址的单位来说，如果发现机器无法上网，而计算机从DHCP服务器处获得的IP地址等相关信息不完全，可以将该地址释放。
   　　Ipconfig /renew：从DHCP服务器重新获得IP地址。释放了IP地址及相关信息之后，必须重新获得一个IP地址，直接输入此命令之后，便可以从DHCP服务器处获得一个IP地址。如果不用此命令，要想重新获得一个IP地址信息，需要重新启动计算机或注销计算机才行。


##### 其他
1. net 系列
   tlist -t 以树行列表显示进程（为系统的附加工具，默认是没有安装的，在安装目录的support/tools文件夹内）
   kill -f 进程名 加-f参数后强制结束某进程（为系统的附加工具，默认是没有安装的，在安装目录的support/tools文件夹内）

2. shutdown
   shutdown.exe -a　取消关机
   shutdown.exe -s 关机
   shutdown.exe -f　强行关闭应用程序。
   shutdown.exe -m \计算机名　控制远程计算机。
   shutdown.exe -i　显示图形用户界面，但必须是Shutdown的第一个参数。
   shutdown.exe -l　注销当前用户。
   shutdown.exe -r　关机并重启。
   shutdown.exe -t时间　设置关机倒计时。
   shutdown.exe -c”消息内容”　输入关机对话框中的消息内容（不能超127个字符）。
   示例：
   电脑要在24:00关机 – at 24:00 Shutdown -s
   倒计时的方式关机 – Shutdown.exe -s -t 7200

##### 杂项

1. nbtstat
   作用：该命令使用TCP/IP上的NetBIOS显示协议统计和当前TCP/IP连接，使用这个命令你可以得到远程主机的NETBIOS信息，比如用户名、所属的工作组、网卡的MAC地址等。在此我们就有必要了解几个基本的参数。
   PS：-a 使用这个参数，只要你知道了远程主机的机器名称，就可以得到它的NETBIOS信息（下同）。
   -A 这个参数也可以得到远程主机的NETBIOS信息，但需要你知道它的IP。
   -n 列出本地机器的NETBIOS信息。

2. netstat
   作用：这是一个用来查看网络状态的命令，操作简便功能强大。
   PS：-a 查看本地机器的所有开放端口，可以有效发现和预防木马，可以知道机器所开的服务等信息
   这里可以看出本地机器开放有FTP服务、Telnet服务、邮件服务、WEB服务等。用法：netstat -a IP。
   -r 列出当前的路由信息，告诉我们本地机器的网关、子网掩码等信息。用法：netstat -r IP。

3. tracert
   作用：跟踪路由信息，使用此命令可以查出数据从本地机器传输到目标主机所经过的所有途径，这对我们了解网络布局和结构很有帮助。

4. net
   作用：这个命令是网络命令中最重要的一个，必须透彻掌握它的每一个子命令的用法，因为它的功能实在是太强大了在这里，我们重点掌握几个常用的子命令。

5. net view
   使用此命令查看远程主机的所有共享资源。命令格式为net view \IP。
   net use
   把远程主机的某个共享资源影射为本地盘符，图形界面方便使用。命令格式为net use x: \IP\sharename。
   net start
   使用它来启动远程主机上的服务。用法：net start servername
   net stop
   入侵后发现远程主机的某个服务碍手碍脚，怎么办？利用这个命令停掉就ok了，用法和net start同。
   net user
   查看和帐户有关的情况，包括新建帐户、删除帐户、查看特定帐户、激活帐户、帐户禁用等。
   1，net user abcd 1234 /add，新建一个用户名为abcd，密码为1234的帐户，默认为user组成员。
   2，net user abcd /del，将用户名为abcd的用户删除。
   3，net user abcd /active:no，将用户名为abcd的用户禁用。
   4，net user abcd /active:yes，激活用户名为abcd的用户。
   5，net user abcd，查看用户名为abcd的用户的情况
   net localgroup　
   查看所有和用户组有关的信息和进行相关操作。
   net time
   这个命令可以查看远程主机当前的时间。

6. at
   作用：这个命令的作用是安排在特定日期或时间执行某个特定的命令和程序。
   用法：at time command \computer

7. ftp
   作用：首先在命令行键入ftp回车，出现ftp的提示符，这时候可以键入“help”来查看帮助（任何DOS命令都可以使用此方法查看其帮助）。
   1.ftp
   2.open 主机IP ftp端口
   3.录入用户名和密码，就可以进行相应操作了。
   dir 跟DOS命令一样，用于查看服务器的文件，直接敲上dir回车，就可以看到此ftp服务器上的文件。
   cd 进入某个文件夹。
   get 下载文件到本地机器。
   put 上传文件到远程服务器。这就要看远程ftp服务器是否给了你可写的权限了，如果可以，呵呵，该怎么 利用就不多说了，大家就自由发挥去吧。
   delete 删除远程ftp服务器上的文件。这也必须保证你有可写的权限。
   bye 退出当前连接。
   quit 同上。

8. telnet
   作用：功能强大的远程登陆命令，几乎所有的入侵者都喜欢用它，屡试不爽。为什么？它操作简单，如同使用自己的机器一样，只要你熟悉DOS命令，在成功以administrator身份连接了远程机器后，就可以用它来**想干的一切了。下面介绍一下使用方法，首先键入telnet回车，再键入help查看其帮助信息。

##### 特殊命令

1. 向上箭头”↑”和向下箭头”↓”—–回看上一次执行的命令
   “Ctrl+C” 组合键或”Break”键 —–中断操作
   鼠标操作”标记” —————–用来选中文本
   鼠标操作”粘贴” —————–用来把剪贴板内容粘贴到提示符下

2. 程序进程
   作用：ntsd 是一条dos命令，功能是用于结束一些常规下结束不了的死进程。
   使用：
   1.利用进程的PID结束进程
   命令格式：ntsd -c q -p pid
   命令范例：ntsd -c q -p 1332 （结束explorer.exe进程）
   2.利用进程名结束进程
   命令格式：ntsd -c q -pn .exe （.exe 为进程名，exe不能省）
   命令范例：ntsd -c q -pn explorer.exe
   3.taskkill结束进程
   命令格式：taskkill /pid 1234 /f （ 也可以达到同样的效果）用命令总结