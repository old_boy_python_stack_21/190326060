# -*- coding:utf-8 -*-
# __author__ = Deng Jack

# http://192.168.12.18
from pymongo.collection import  Collection
from flask import Flask ,request ,render_template ,jsonify
from config.settings import USERTABLE # type:Collection
from utils.entrpe_md5 import get_md5

app = Flask(__name__)
app.debug = True

@app.route('/login',methods=['post'])
def login():
    if request.method == "POST":
        user_info = request.form.to_dict()
        user_info['pwd']= get_md5(user_info.get('pwd'))
        res = USERTABLE.find_one(user_info)
        if res:
            return {'code':0,'msg':'登陆成功','status':'success'}
        else:
            return {'code':1,'msg':'用户名或密码错误','status':'data error'}
    else:
        return {'code':999,'msg':'非法操作,请使用get请求提交数据','status':'valid'}

@app.route('/reg',methods=['post'])
def reg():
    if request.method == "POST":
        user_info = request.form.to_dict()
        user_info['pwd'] = get_md5(user_info.get('pwd'))
        res = USERTABLE.find_one(user_info)
        if res:
            return {'code': 1, 'status':'data error','msg': '用户名重复'}
        USERTABLE.insert_one(user_info)
        return {'code':0,'status':'success','msg':'注册成功'}
    else:
        return {'code':999,'status':'method error','msg':'非法操作,请使用get请求提交数据'}

if __name__ == '__main__':
    app.run('0.0.0.0',7777)