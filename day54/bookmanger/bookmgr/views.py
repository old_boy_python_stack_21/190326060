from django.shortcuts import render, HttpResponse, redirect
from bookmgr import models


# Create your views here.
def index(request):
    obj_list = models.Publisher.objects.all().order_by('pk')
    # for item in obj_list:
    #     print(item.p_name)
    return render(request, 'index.html', {"obj_list": obj_list})


def addpublisher(request):
    error = ''
    if request.method == 'POST':
        p_name = request.POST['p_name']
        if models.Publisher.objects.filter(p_name=p_name):
            error = '出版社已存在'
            print(error)
        elif not p_name:
            error = '出版社名不能为空'
            print(error)
        else:
            models.Publisher.objects.create(p_name=p_name)
            return redirect('/index')
    return render(request, 'add_publish.html', {'error': error})


def editpublisher(request):
    error = ''
    pid = request.GET.get('id')  # # url上携带的参数  不是GET请求提交参数,
    obj_list = models.Publisher.objects.filter(pk=pid)
    if not obj_list:
        error = '编辑的出版社不存在'
    if request.method == "POST":
        obj = obj_list[0]
        p_name = request.POST.get('p_name')
        if models.Publisher.objects.filter(p_name=p_name):
            error = '新修改的名称已存在'
        elif obj.p_name == p_name :
            error = '名称未修改'
        elif not p_name:
            error = '名称不能为空'
        if not error:
            # 新修改的名称不在数据库内,不为空,并且已经修改(和原来的名称不同)
            obj.p_name = p_name
            obj.save()
            return redirect('/index/')
    return render(request, 'edit_publish.html',{'error':error,'obj':obj_list})


def delpublisher(request):
    pid = request.GET.get('id')
    obj_list = models.Publisher.objects.filter(pk=pid)
    if not obj_list:
        return HttpResponse('要删除的数据不存在')
    obj_list.delete()
    return redirect('/index/')

def hhh(request):
    obj_list = models.Publisher.objects.all().order_by('pk')
    # for item in obj_list:
    #     print(item.p_name)
    return render(request, 'hhh.html', {"obj_list": obj_list})