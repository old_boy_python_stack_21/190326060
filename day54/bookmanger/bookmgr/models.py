from django.db import models

# Create your models here.
class Publisher(models.Model):
    pid = models.AutoField(primary_key=True)
    p_name = models.CharField(max_length=32,blank=False,unique=True)