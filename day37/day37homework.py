# 数据库练习
# 1.创建一个数据库，ftp
# create database ftp;


# 2.创建一张表，userinfo表
# use ftp;
# create table userinfo(id int,username char(20),password char50));
# 存储三个字段：id,username,password
# 3.向这张表中插入3条数据
# | 1 | alex | alex3714 |
# | 2 | wusir | 6666 |
# | 3 | yuan | 9999 |
# insert into userinfo values (1,'alex','alex3714');
# insert into userinfo values (2,'wusir','6666');
# insert into userinfo values (3,'yuan','9999');
# 4.查看表中的所有数据
# select * from userinfo
# 5.修改wusir的密码,变成666
# update userinfo set password = 666 where id=2;
# 6.删除yuan这一行信息
# delete from userinfo where id=3;



# python练习
# 1.有一个文件，这个文件中有20000行数据，开启一个线程池，为每100行创建一个任务，打印这100行数据。

"""
from concurrent.futures import ThreadPoolExecutor
from threading import Lock
tp = ThreadPoolExecutor(20)
f = open('data.txt','r',encoding='utf-8')
lock = Lock()

def print100rows(f,lock):
    with lock:
        total = ''
        for i in range(100):
            val = f.readline()
            total+=val
        return total

def get100rows(p):
    print(p.result(),p.result)


for i in range(0,20000,100):
    p = tp.submit(print100rows,f,lock)
    p.add_done_callback(get100rows)

"""

# 2.写一个socket 的udp代码，client端发送一个时间格式，server端按照client的格式返回当前时间
# 例如 client发送'%Y-%m-%d'，server端返回'2019-5-20'
from datetime import datetime
import socket
sk = socket.socket(type=socket.SOCK_DGRAM)
sk.bind(('127.0.0.1',8888))
while True:
    msg,addr = sk.recvfrom(1024)
    dmt = msg.decode('utf-8')
    n_time = datetime.now()
    res = n_time.strftime(dmt).encode('utf-8')
    sk.sendto(res,addr)



# 3.请总结进程、线程、协程的区别。和应用场景。
# 进程,开销大,数据隔离,能利用多核,数据是不安全的,只能有操作系统调度
# 线程,开销小,数据共享,由于cpython的GIL锁的存在,无法利用多核,数据也不安全,只能有操作系统调度
# 协程 开销非常小,数据共享,数据是安全的,有用户调控(代码)

# 应用场景 :    纯计算类的,没有堵塞的,就开多个进程,是CPU效率达到最高.(一半电脑核心1-2倍左右)
                # 网络爬虫时,很多是堵塞,开多个线程和协程,完成爬虫任务.