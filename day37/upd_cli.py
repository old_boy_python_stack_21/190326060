#!/usr/bin/env python
# -*- coding:utf-8 -*-

import socket , time
sk = socket.socket(type=socket.SOCK_DGRAM)
ip = (('127.0.0.1', 8888))
while True:
    sk.sendto('%Y-%m-%d %H:%M:%S'.encode('utf-8'), ip)
    msg, _ = sk.recvfrom(1024)
    print(msg.decode('utf-8'))
    print(_)
    time.sleep(2)
