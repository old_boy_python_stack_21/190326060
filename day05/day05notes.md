# Day05

## 一、内容补充

### 1. int

- py2和py3的2种区别
  1. py2有int和long，int的取值范围为-2^31~2^31-1，超出范围自动转为long，长整型。
  2. py2的整型除法只保留整数，py3保留所有。
  3. 强制转换：只有''和0转为bool值时为False，其余为True。

### 2. bool

- 强制转换

  bool(int),  0转换为False

  bool(str)，''转换为False

  bool(list) , []转换为False

  bool(tuple),()转换为False

### 3. str

- 独有功能:

  ```python
  t = " naME "
  t.lower()     t.upper()      #大小写
  t.split()    t.lstrip()      t.rstrip()     #俩端去空格，左端去空格，右端去空格
  t.replace('','',n)   #替换
  t.isdigit()     #是否能转为数字
  t.strip()    t.rsplit()     #分割
  t.startswith('')    t.endswith('')     #是否以''开头，结尾
  t.format()        #字符串格式化
  t.encode()        #以制定的编码格式编码字符串  较为重要
  '-5-'.join(t)     #循环t的每个字符，把'-5-'当做分割符加入到里面，得到新的字符串
  ```

- 公共功能

  索引/长度len/切片/步长/for循环/删除（无此功能）/修改（无此功能）  # str是不可变类型

- 强制转换：

  ```python
  print(type(str(["唐开发",'李忠伟'])))    #str   ["唐开发",'李忠伟'],看着像list，其实是字符串
  print(type(["唐开发",'李忠伟']))     #list   ["唐开发",'李忠伟']
  ```

### 4. list

- 独有功能

  ```python
  t = [11,22,33,44,55,33,66]
  t.append(77)        #  在列表后添加上77，列表[11,22,33,44,55,33,66，77]
  t.insert(0,'-11')   #  在列表索引位置0，添加'-11'
  t.remove('-11')       #  从左到右，删除第一个'-11'
  t.pop(1)              # 删除索引位置1的元素，不填默认最后一个元素
  t.clear()           #清除列表所有元素
  t.extend([99,88,77,12,23])       # 把[99,88,77,12,23]的元素循环添加到列表t中，也可以是str-
  ```

- 公共功能

  索引/长度len/切片/步长/for循环/删除/修改

  - 删除del

    ```python
    t = [11,22,33,44,55,33,66]
    del t[0:2]   #删除列表t索引0-1的元素，注意，仅仅是删除
    t.pop(2)  #可以得到别删除的数据
    deteled = t.pop(2)   #   33
    
    ```

  - 修改，通过赋值即可修改，list是可变类型。

- 强制转换

  ```python
  t = list('998877456dengyixin')  str 
  print(t)     #  ['9', '9', '8', '8', '7', '7', '4', ······]
  m = （'9', '9', '8', '8', '7'）   tuple  
  print(list(m))     #   ['9', '9', '8', '8', '7']
  ```

  

### 5. tuple

元组tuple是不可变类型，没有独有功能

- 公共功能

   索引/长度len/切片/步长/for循环/删除（无此功能）/修改（无此功能）  # tuple是不可变类型

- 元组和列表可混合嵌套

- 强制转换

  ```python
  t = tuple('998877456dengyixin')  #   str
  m = ['9', '9', '8', '8', '7']     
  print(Tuple(m))        #   （'9', '9', '8', '8', '7'）
  
  ```

### 6. 转换的重要类型

- int 和 str 的相互转换
- 列表和元组的转换
- bool转换的内容

## 二、新内容

### 1.  Dictionary（字典）

- 字典是一种可变容器模型，且可存储任意类型对象 ，帮助用户去表示一个事物的信息。各种属性。

- {}表示，如

  ```python
  d = {key1 : value1, key2 : value2,键:值 }    #  键值对
  # 键一般是唯一的，如果重复最后的一个键值对会替换前面的。
  # 值可以取任何数据类型，但键必须是不可变的，如字符串，数字或元组，键不可以是列表和字典。
  ```

- 独有功能

  ```python
  dict = {'Name': 'deng', 'Age':18, 'Class': 'First'}
  print(dict.keys())   #获取dict中所有键  
  print(dict.values())   ##获取dict中所有值
  print(dict.items())    #获取dict中所有键值对
  ```

- 公共功能

  索引/长度len /for循环/删除/修改    # dict是无序的，不能切片和步长

  ```python
  dict = {'Name': 'deng', 'Age':18, 'Class': 'First'}  # 修改
  dict['Name'] = 'wang'  # 改值
  dict['gender'] = '男' #  添加键值对，如果要修改键值对，得先删除键值对，删除后再添加键值对
  del dict['Name']    # 删除键值对
  dict['New_Name'] = 'mao'
  len(dict)   #键值对的个数
  dict['Age']   #  18
  ```