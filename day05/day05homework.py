#!/usr/bin/env python
# -*- coding:utf-8 -*-

## 1.请将列表中的每个元素通过"_"链接起来。
# users = ['李少奇', '李启航', '渣渣辉']
# print('_'.join(users))


## 2.请将列表中的每个元素通过"_"链接起来。
# users = ['李少奇', '李启航', 666, '渣渣辉']
# users[2] = '666'
# print('_'.join(users))


## 3.请将元组 v1 = (11,22,33) 中的所有元素追加到列表 v2 = [44,55,66] 中。
# v1 = (11,22,33)
# v2 = [44,55,66]
# v2.extend(v1)
# print(v2)


## 4.请将元组v1 = (11, 22, 33, 44, 55, 66, 77, 88, 99)中的所有偶数索引位置的元素追加到列表v2 = [44, 55, 66]中。
# v1 = (11, 22, 33, 44, 55, 66, 77, 88, 99)
# v2 = [44,55,66]
# for count in range(0,len(v1),2):
#     v2.append(v1[count])
# print(v2)


## 5.将字典的键和值分别追加到key_list和value_list两个列表中，如：
"""
key_list = []
value_list = []
info = {'k1': 'v1', 'k2': 'v2', 'k3': 'v3'}
for item in info.keys():
    key_list.append(item)
for item in info.values():
    value_list.append(item)
print(key_list)
print(value_list)
"""

##  6.字典dic = {'k1': "v1", "k2": "v2", "k3": [11, 22, 33]}
dic = {'k1': "v1", "k2": "v2", "k3": [11, 22, 33]}
 ####a.请循环输出所有的key
for item in dic.keys():
    print(item)

    #b.请循环输出所有的value
for item in dic.values():
    print(item)

    #c.请循环输出所有的key和value
for item in dic.items():
    print(item)

    #d.请在字典中添加一个键值对，"k4": "v4"，输出添加后的字典
dic["k4"] = "v4"
print(dic)
    #e.请在修改字典中"k1"对应的值为"alex"，输出修改后的字典
dic = {'k1': "v1", "k2": "v2", "k3": [11, 22, 33]}
dic['k1'] = "alex"
print(dic)
    #f.请在k3对应的值中追加一个元素44，输出修改后的字典
dic = {'k1': "v1", "k2": "v2", "k3": [11, 22, 33]}
dic["k3"].append(44)
print(dic)
    #g.请在k3对应的值的第1个位置插入个元素18，输出修改后的字典
dic = {'k1': "v1", "k2": "v2", "k3": [11, 22, 33]}
dic["k3"].insert(0,18)
print(dic)

## 7.请循环打印k2对应的值中的每个元素。
"""
info = {
    'k1': 'v1',
    'k2': [('alex'), ('wupeiqi'), ('oldboy')],
}
for item in info['k2']:
    print(item)
"""


## 8.有字符串"k: 1|k1:2|k2:3  |k3 :4"处理成字典{'k': 1, 'k1': 2....}
"""
dict1 = {}
content = "k: 1|k1:2|k2:3  |k3 :4"
content = content.split('|')
item1 = []
print(content)
for item in content:
    item1 = item.split(':')
    item1[0] = item1[0].strip()
    dict1[item1[0]]  = int(item1[1])
print(dict1)
"""


## 9.写代码有如下值 li= [11,22,33,44,55,66,77,88,99,90] ,将所有大于 66 的值保存至字典的第一个key对应的列表中，将小于 66 的值保存至第二个key对应的列表中。
#  result = {'k1':[],'k2':[]}
"""
li= [11,22,33,44,55,66,77,88,99,90]
result = {}
list1 = []
list2 = []
for item in li:
    if item > 66:
        list1.append(item)
    elif item < 66:
        list2.append(item)
result['k1'] = list1
result['k2'] = list2
print(result)
"""

## 10.输出商品列表，用户输入序号，显示用户选中的商品
#   商品列表：
goods = [
         {"name": "电脑", "price": 1999},
         {"name": "鼠标", "price": 10},
         {"name": "游艇", "price": 20},
         {"name": "美女", "price": 998}
     ]
# 要求:
# 1：页面显示 序号 + 商品名称 + 商品价格，如：
#     1 电脑 1999
#     2 鼠标 10
#      ...
goods = [
        {"name": "电脑", "price": 1999},
        {"name": "鼠标", "price": 10},
        {"name": "游艇", "price": 20},
        {"name": "美女", "price": 998}
    ]
count = 0
for item in goods:
    print(count+1,item['name'],item['price'])
    count += 1

# 2：用户输入选择的商品序号，然后打印商品名称及商品价格
"""
num = int(input('请输入商品序号'))
print(goods[num-1]['name'],goods[num-1]['price'])

        # num = int(input('请输入商品序号'))
        # goods.insert(0,{})
        # print(goods[num]['name'],goods[num]['price'])
"""

# 3：如果用户输入的商品序号有误，则提示输入有误，并重新输入。
"""
while True:
    num = input('请输入商品序号')
    num = num.strip()
    if  not num.isdigit():
        print('输入的内容包含字符，请重新输入')
        continue
    num = int(num)
    if 0 < num < 5:                # 判断是否在商品序号范围内
        print(goods[num - 1]['name'], goods[num - 1]['price'])
        break
    print('输入有误，超出序号范围，请重新输入')
"""

# 4：用户输入Q或者q，退出程序。
"""
while True:
    num = input('请输入商品序号')
    if num.lower() == 'q':
        break
    num = num.strip()
    if  not num.isdigit():
        print('输入的内容包含字符，请重新输入')
        continue
    num = int(num)
    if 0 < num < 5:                # 判断是否在商品序号范围内
        print(goods[num - 1]['name'], goods[num - 1]['price'])
        break
    print('输入有误，超出序号范围，请重新输入')
"""


## 11.看代码写结果
v = {}
for index in range(10):
    v['users'] = index
print(v)                         # {'users':9}
# range(10)=[0,1,2,3,4,5,6,7,8,9]
# 第一次循环,给字典v添加键值对'users':0   ,   这时v = {'users':0}
# 后面的循环，则修改键值对，改变字典'users'对应的值，最终输出v ={'users':9}