#!/usr/bin/env python        	
# -*- coding:utf-8 -*-	 
#@Project  : s21day33 
#@Author : Jack Deng       
#@Time   :  2019-05-14 19:34
#@File   : 进程并发实现.py

from multiprocessing import Process
import socket
sk = socket.socket()
sk.setsockopt(socket.SOL_SOCKET,socket.SO_REUSEADDR,1)
sk.bind(('127.0.0.1',8830))
sk.listen()


def server(conn,addr):
    while True:
        try :
            msg = conn.recv(1024)
            if msg == b'Q': break
            conn.send(b'hello')
        except Exception :
            break

if __name__ == '__main__':
    while True:
        conn,addr= sk.accept()
        p = Process(target = server,args = (conn,addr))
        p.start()

