#!/usr/bin/env python        	
# -*- coding:utf-8 -*-	 
#@Project  : s21day33 
#@Author : Jack Deng       
#@Time   :  2019-05-14 19:46
#@File   : client.py

from socket import *

client=socket(AF_INET,SOCK_STREAM)
client.connect(('127.0.0.1',8830))


while True:
    msg=input('>>: ').strip()
    if not msg:continue
    client.send(msg.encode('utf-8'))
    msg=client.recv(1024)
    print(msg.decode('utf-8'))