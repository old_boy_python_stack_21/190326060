## 一、循环语句

- while 用法

  ```python
  num = 1
  while num <= 10:
      print(num)
      num += 1
  ```

- while else 用法

  ```python
  num = 1
  while num <= 10:
      print(num)
      num += 1
  else:
      print("end")
  # else 表示while循环语句不满足组条件后执行的代码
  ```

- break、continue关键字的用法,以及与if   pass语句的嵌套

  ```python
  num = 1
  while num <= 100:
      if num == 55:
          pass
      else:
          print(num)
      num += 1
  # 通过if pass的使用，打印1-100内除了55的整数。
  
  num = 1
  while num <= 100:
  	 if num == 45:
  	 	break
  	 print(num)
  	 num += 1
  print('end')
  # break 打断当前while的循环，向下继续运行。运行输出1-44的数字
  
  num = 1
  while num <= 10:
  	 if num == 8:
  	 	num += 1 
  	 	continue
  	 print(num)
  	 num += 1
  #  continue 继续，运行到此处，不向下运行，回到while循环开头  	
  
  ```

- 补充：if 语句的嵌套，if 语句可以嵌套很多层。

  ```python
  gender = input('请输入你的性别：')
  if gender == '男':
  	print("""感谢你的问询，请在下列业务中选择一项：
  	1.男鞋用品
  	2.男性春装用品
  	3.男性护肤品""")
  	num = input('请输入选择业务的序号：')
  	if num == '1':
  		print('感谢购买男士皮鞋')
  	else: print('请移步其他分店购买')
  else:
      print('抱歉，本店只售男士用品')	
  ```

  

## 二、字符串格式化

1. 字符串格式化的意义，大部分字符过于冗长，使用字符串格式化能大大加快效率，方便程序员调用数据。

2. %s 、 %d、%%

   -  ```python
     red_dad = '大红的爸爸'
     do = '教学生上课'
     thing = '%s在操场%s' %(red_dad，do,)
     print(thing)
      ```

   - 直接做占位符

     ```python
     temper = '%s在太空中%s' %('等大侠','打飞机',)
     print(temper)
     thing = '盖伦，年龄%d，喜欢在池塘里%s' %(15,'打水仗'，)
     print(thing)
     #####  #s和#d表示的类型不同，前者表示字符串数据，后者表示整型数据。
     name = '小明'
     template = "%s拿出了100%%的力气" %(name,)
     print(template)
     ######   %%，为了和字符串格式化做区分，百分号要写成%%形式。
     ```

## 三、运算符的运用

- 算术运算符:加减乘除的运用，+、-、*、/ 在程序中用于算术运算。还有类似于：

  1. % ，取除法的余数，如15%4 = 3
  2. // ，取除法的商的整数，如20//6 = 3
  3. ** ，取指数的值，如2**8 = 2的8次方，为256.

- 赋值运算符：

  1. c += 1 等价于 c = c+ 1

  2. c -= 1等价于c = c - 1

  3. c *= 2 等价于 c = c * 2

     等等诸如此类

- 逻辑运算符: and 、or、not

  1. 一般用法：表示逻辑中的于、或、非，用于条件的判断

  2. 二般用法：

     - 3种数据类型的转化，int str boolen的转化

       ```python
       test=bool('')
       test1=bool(0)
       print(test)
       print(test1)
       #####   注意：只有空字符串和0转化为布尔值时为false，否则都为 true
       ```

     - ```
       value = x and y
       print(value)
       ####: 从左到右， x转化为布尔值为真，value = y，否则value= x 。
       value1 = x or y
       print(value1)
       ####: 从左到右， x转化为布尔值为真，value1 = x，否则value= x 。
       ```

     - 运算的优先顺序为： ( ) >not > and >or

## 四、编码补充

- gbk码、gb2312码:都是亚洲地区使用的编码，都用2个字节表示一个汉字。

- 单位换算：8 bit = 1 bype

  ​                    1024 bype = 1 KB

  ​                    1024 KB = 1 MB

  ​                    1024 MB = 1 GB

  ​                    1024 GB = 1 TB