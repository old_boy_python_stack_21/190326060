



1. 猜数字，设定一个理想数字比如：66，让用户输入数字，如果比66大，则显示猜测的结果大了；如果比66小，则显示猜测的结果小了;只有等于66，显示猜测结果正确，然后退出循环。

   答：

   ```python
   while True:
       count = input('请输入猜测数字：')
       count = int(count)
       if count ==66:
           print('猜测结果正确')
           break
       else:
           if count > 66:
               print('猜测结果偏大')
           else:
               print('猜测结果偏小')
   ```

   

   

2. 在上一题的基础，设置：给用户三次猜测机会，如果三次之内猜测对了，则显示猜测正确，退出循环，如果三次之内没有猜测正确，则自动退出循环，并显示‘大笨蛋’。

   ```python
   
   times = 3
   while times >= 1:
       count = input('请输入猜测数字：')
       count = int(count)
       if count ==66:
           print('猜测结果正确')
           break
       else:
           if count > 66:
               print('猜测结果偏大')
               times = times - 1
           else:
               print('猜测结果偏小')
               times = times - 1
       if times == 0:
           print('大笨蛋')
   ```

   

3. 使用两种方法实现输出 1 2 3 4 5 6   8 9 10 。

   ```python
   count = 1
   while count <= 10:
   	 if count == 7:
   	 	count = count + 1
   	 	continue
   	 print(count)
   	 count = count + 1
   	 
   ###
   count = 1
   while count <= 10:
   	if count == 7:
   		pass
   	else:
   		print(count)
   	count = count + 1
       
   ```

   

4. 求1-100的所有数的和

   ```python
   toral = 0
   count = 1
   while count <= 100:
       toral = toral + count
       count = count + 1
   print(toral)
   ```

   

5. 输出 1-100 内的所有奇数

   ```python
   count = 1
   while count <= 100:
       if count%2 == 1:
           print(count)
       count =count +1
   ```

   

6. 输出 1-100 内的所有偶数

   ```
   count = 1
   while count <= 100:
       if count%2 == 0:
           print(count)
       count =count +1
   ```

   

7. 求1-2+3-4+5 ... 99的所有数的和

   ```PYTHON
   count = 1
   toral = 0
   while count <= 99:
      if count%2 == 0:
          toral = toral - count
      else:
          toral = toral + count
      count = count + 1
   print(toral)
   ```

   

8. 用户登陆（三次输错机会）且每次输错误时显示剩余错误次数（提示：使用字符串格式化） 

   ```
   times = 3
   while times >= 1:
       name = input('请输入用户名称：')
       password = input('请输入密码：')
       if name == '123' and password == '123':
           print('欢迎登陆')
           break
       else:
           times = times - 1
           print('剩余错误次数为%d次'%(times,))
   	
   ```

   

9. 简述ASCII、Unicode、utf-8编码

   答：

   - ascii码，，国际英文和标点的表示方法，用8bit表示一个字符，有2**8种表示方式。
   - Unicode码，又叫万国码，用32bit表示一个字符，有2**32种表示方式，能表示所有语言。
   - utf-8编码，对unicode码的压缩，略取没有意义的数据，精简文件大小，中文用24bit表示一个字符。

   

10. 简述位和字节的关系？

   答：8位等价于1字节。即8bit = 1 bype ，都是存储数据的单位。

11. 猜年龄游戏 
    要求：允许用户最多尝试3次，3次都没猜对的话，就直接退出，如果猜对了，打印恭喜信息并退出

    ```
    times = 3
    while times >= 1:
        age = input('请输入猜测年龄：')
        age = int(age)
        if age == 25:
            print('恭喜猜测正确')
            break
        else:
            times = times - 1
    ```

    

12. 猜年龄游戏升级版
    要求：允许用户最多尝试3次，每尝试3次后，如果还没猜对，就问用户是否还想继续玩，如果回答Y，就继续让其猜3次，以此往复，如果回答N，就退出程序，如何猜对了，就直接退出。

    ```python
    times = 3
    while times >= 1:
        age = input('请输入猜测年龄：')
        age = int(age)
        if age == 25:
            print('恭喜猜测正确')
            break
        else:
            times = times - 1
        if times == 0:
            question = input("""是否还想继续玩?
                Y or N """)
            if question == "Y":
                times = 3
            else:
                break
    ```

    

13. 判断下列逻辑语句的True,False
    - 1 > 1 or 3 < 4 or 4 > 5 and 2 > 1 and 9 > 8 or 7 < 6

      答：true

    - not 2 > 1 and 3 < 4 or 4 > 5 and 2 > 1 and 9 > 8 or 7 < 6 

      答：false

14. 求出下列逻辑语句的值。
    - 8 or 3 and 4 or 2 and 0 or 9 and 7

      答：8

    - 0 or 2 and 3 and 4 or 6 and 0 or 3

      答：4

15. 下列结果是什么？
    - 6 or 2 > 1= 6 or T = 6

    - 3 or 2 > 1 = 3 or T = 3

    - 0 or 5 < 4 = 0 or F = F

    - 5 < 4 or 3 = F or 3 = 3

    - 2 > 1 or 6 =T or 6 = T

    - 3 and 2 > 1 = 3 and T = T

    - 0 and 3 > 1 = 0 and T = 0

    - 2 > 1 and 3 = T and 3 =3

    - 3 > 1 and 0 = T and 0 = 0

    - 3 > 1 and 2 or 2 < 3 and 3 and 4 or 3 > 2

      =T and 2 or T and 3 and 4 or T

      =2 or 3 and 4 or T

      =2 or 4 or T

      = 2