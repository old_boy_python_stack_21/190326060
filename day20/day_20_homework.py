#!/usr/bin/env python        	
# -*- coding:utf-8 -*-	 
#@Project  : s21day20 
#@Author : Jack Deng       
#@Time   :  2019-04-24 17:44
#@File   : day_20_homework.py

# 1.
"""
#1 .封装 类把函数做划分和分类     ,对象封装值,以便后期调用
class Foo:
    def __init__(self,n):
        self.name = n

    def func1(self):
        print(123)

    def func2(self):
        print(789)
# 2. 继承  子类可以继承父类的方法和静态字段 找数据优先到本身来找(创建该对象的类中找),一路向上找
class Foo1:
    def f2(self):
        print('f2')


class Base(Foo1):
    def f1(self):
        print('f1')

v = Base()
v.f2()   #继承父类的方法
#3. 多态
def func3(a1):
    print(a1[6])
# python函数支持多种类型的参数传递
"""

# 2.
# 鸭子模型 : 对于一个函数而言,python对函数传入的参数没有限制,例如只有函数内采用的索引方法就是对传入类型的限制.
# 如上就是鸭子模型.只要呱呱叫的都是鸭子,只要能满足索引方法的数据类型,就是我们想要找的类型.

# 3.
"""
class Foo:    # --->Foo就是类

    country = '中国'    # -->类成员1 :类变量(静态字段),对象.变量 / 类.变量调用
    def __init__(self,n,a):
        self.name = n
        self.age = a

    @staticmethod
    def func1():            # --->类成员2 : 静态方法,方法对参数无限制  ,类.方法() / 对象.方法()调用
        print('小红真美')

    @classmethod
    def func2(cls,name):    #--->类成员3 :类方法,方法至少要一个cla参数 ,类.方法() / 对象.方法() 调用
        print(name)

    def func3(self):        #--->类成员4 :方法(普通方法/绑定方法),至少要一个self参数 对象.方法() 调用
        print(self.name)

    @property
    def func4(self):   #--->类成员5 :属性,至少有一个self参数,执行不加括号,只能
        return self.country + self.name + self.age


obj = Foo('邓益新',25)  # --->对象
print(obj.name)   # -->对象成员1 :实例变量(普通字段) ,只能对象.方法调用
"""


# 4.
# @staticmethod   @classmethod
# 前者是静态方法,参数无限制,类.方法名()和对象.方法名()都能调用 .
# 后者是类方法,至少需要一个cls参数,类.方法名()和对象.方法名()都能调用 .


# 5.
# 成员修饰符,加上"__"表明是私有成员,只有内部才能访问,外部无法访问.其子类也无法访问.
# 有例外   " _类名__私有成员"可以强制访问私有成员.

# 6.
"""
class Base:
    x = 1
    
obj = Base()    # 创建对象obj


print(obj.x)    # 1
obj.y = 123     # 对象封装值     
print(obj.y)    # 123
obj.x = 123     #对象封装值,类变量不变x = 1
print(obj.x)    # 123
print(Base.x)   # 1
"""

# 7 .
"""
class Parent:
    x = 1              # 类变量x = 1
    
class Child1(Parent):
    pass               # 继承父类

class Child2(Parent):  # 继承父类
    pass

print(Parent.x,Child1.x,Child2.x)      #  1  1   1
Child2.x = 2                           # Child2类封装值-->类变量x = 2
print(Parent.x,Child1.x,Child2.x)      #  1  1   2
Child1.x = 3                           # Child1类封装值-->类变量x = 3 
print(Parent.x,Child1.x,Child2.x)      # 1   3  2
"""

# 8.
"""
class Foo(object):
    n1 = '武沛齐'
    n2 = '金老板'
    def __init__(self):
        self.n1 = '女神'

obj = Foo()       # 创建对象,封装值 '女神'
print(obj.n1)     #   '女神'
print(obj.n2)     # 对象没有,取类中找, '金老板'
"""

# 9.
"""
class Foo(object):
    n1 = '武沛齐'              #创建类,封装值
    def __init__(self,name):
        self.n2 = name
obj = Foo('太白')              # 创建对象,封装值
print(obj.n1)                  # 对象没有,找到类封装的值n1,打印'武沛齐'
print(obj.n2)                  # 打印对象封装的n2,             '太白

print(Foo.n1)                  # 类调用类变量,             打印'武沛齐'
print(Foo.n2)                  # 类找不到对象封装的值,报错
"""

# 10.
"""
class Foo(object):
    a1 = 1
    __a2 = 2                  # 创建对象,封装值和私有值,只有内部调用

    def __init__(self,num):
        self.num = num
        self.__salary = 1000

    def show_data(self):
        print(self.num+self.a1)

obj = Foo(666)          # 创建对象,对象封装值,num = 666

print(obj.num)          # 666
print(obj.a1)           # 1
# print(obj.__salary)     #报错
# print(obj.__a2)         #报错
print(Foo.a1)           # 1
# print(Foo.__a2)         #报错
"""

# 11.
"""
class Foo(object):
    a1 = 1
    
    def __init__(self,num):
        self.num = num
    def show_data(self):
        print(self.num+self.a1)
    
obj1 = Foo(666)              # 创建对象1,封装值666
obj2 = Foo(999)              # 创建对象2,封装值999
print(obj1.num)              # 666
print(obj1.a1)               # 1

obj1.num = 18                # 更新对象封装的值 18
obj1.a1 = 99                 # 对象封装新值 99

print(obj1.num)              # 18
print(obj1.a1)               # 99

print(obj2.a1)               # 1
print(obj2.num)              # 999
print(obj2.num)              # 999
print(Foo.a1)                # 1
print(obj1.a1)               # 99
"""

# 12.
"""
class Foo(object):

    def f1(self):
        return 999
    
    def f2(self):
        v = self.f1()
        print('f2')
        return v
    
    def f3(self):
        print('f3')
        return self.f2()
    
    def run(self):
        result = self.f3()
        print(result)

obj = Foo()                  # 创建对象
v1 = obj.run()               # 调用方法run,打印f3,打印f2,打印999,返回值None
print(v1)                    # 打印None
"""

# 13.
"""
class Foo(object):
    
    def f1(self):
        print('f1')

    @staticmethod        # 静态方法,参数无限制
    def f2():
        print('f2')
obj = Foo()
obj.f1()                 # f1
obj.f2()                 # f2

# Foo.f1()               # 报错,Foo.f1 是内存地址
Foo.f2()                 # 打印f2

#  注意  Foo.f1(obj) 可以执行-->给了self参数

"""

# 14.
"""
class Foo(object):

    def f1(self):
        print('f1')

    @classmethod           #类方法,必须cls参数,类.方法()/对象.方法()调用
    def f2(cls):
        print('f2')
obj = Foo()                #创建对象
obj.f1()                   #  f1
obj.f2()                   #  f2

# Foo.f1()                   #  报错
Foo.f2()                   #  f2
"""

# 15.
"""
class Foo(object):
    
    def f1(self):
        print('f1')
        self.f2()
        self.f3()

    @classmethod
    def f2(cls):
          print('f2')

    @staticmethod
    def f3():
          print('f3')

obj = Foo()    # 创建对象
obj.f1()       # f1  ,f2   ,f3  ,对象可以调用方法,类方法,静态方法
"""

# 16.
"""
class Base(object):
    @classmethod
    def f2(cls):
          print('f2')

    @staticmethod
    def f3():
          print('f3')

class Foo(object):
    def f1(self):
        print('f1')
        self.f2()
        self.f3()

obj = Foo()    # 创建对象
obj.f1()       # f1  ,Foo和Base都是新型类,无派生关系,报错.
"""

#17.
"""
class Foo(object):
    def __init__(self, num):
        self.num = num


v1 = [Foo for i in range(10)]
v2 = [Foo(5) for i in range(10)]
v3 = [Foo(i) for i in range(10)]

print(v1)          # [Foo  *   10  ]   内存地址列表,同一的内存地址
print(v2)          # [创建了10 个Foo(5)对象的列表]
print(v3)          # [创建了10个Foo对象,封装的值不同的列表]
"""

# 18.
"""
class StarkConfig(object):

    def __init__(self, num):
        self.num = num

    def changelist(self, request):
        print(self.num, request)


config_obj_list = [StarkConfig(1), StarkConfig(2), StarkConfig(3)]
for item in config_obj_list: #3个不同对象的列表
    print(item.num)  # 1  ,  2   ,  3
"""

# 19.
"""
class StarkConfig(object):
    def __init__(self, num):
        self.num = num
    def changelist(self, request):
        print(self.num, request)
config_obj_list = [StarkConfig(1), StarkConfig(2), StarkConfig(3)]
for item in config_obj_list:
    item.changelist(666)

# 1 666   2 666  3 666
"""

# 20.
"""
class Department(object):
    def __init__(self, title):
        self.title = title


class Person(object):
    def __init__(self, name, age, depart):
        self.name = name
        self.age = age
        self.depart = depart


d1 = Department('人事部')
d2 = Department('销售部')

p1 = Person('武沛齐', 18, d1)
p2 = Person('alex', 18, d1)
p3 = Person('安安', 19, d2)

print(p1.name)   # '武沛齐'
print(p2.age)    # 18
print(p3.depart) # Department-object
print(p3.depart.title)   # '销售部'
"""

#21.
"""
class Department(object):
    def __init__(self, title):
        self.title = title


class Person(object):
    def __init__(self, name, age, depart):
        self.name = name
        self.age = age
        self.depart = depart

    def message(self):
        msg = "我是%s,年龄%s,属于%s" % (self.name, self.age, self.depart.title)
        print(msg)


d1 = Department('人事部')
d2 = Department('销售部')

p1 = Person('武沛齐', 18, d1)
p2 = Person('alex', 18, d1)
p1.message()    # 我是武沛齐,年龄18,属于人事部
p2.message()    # 我是Alex,年龄18,属于人事部

"""

# 22
from datetime import datetime , timedelta
class School:
    def __init__(self,address):
        self.address = address

bj = School('北京')
sh = School('上海')
sz = School('深圳')   # 创建学校

class Course:
    def __init__(self,name,school):
        self.name= name
        self.school = school

py1 = Course('Python',bj)
py2 = Course('Python',sh)
py3 = Course('Python',sz)

li1 = Course('linux',bj)
li2 = Course('linux',sh)

go1 = Course('Go语言',bj)




class Classroom:
    def __init__(self,course,name,startime,):
        self.name = name
        self.startime = startime
        self.endtime = (datetime.strptime('2018-4-26','%Y-%m-%d') + timedelta(days = 180)).strftime('%Y-%m-%d')
        self.people_num = 60
        self.course = course
        print(self.course.school.address,self.course.name,self.name,str(self.people_num)+'人数','开课时间'+ self.startime,'结课时间'+self.endtime)



cl1 = Classroom(py1,'21期','2018-4-26')
cl2 = Classroom(py1,'22期','2018-5-26')
cl3 = Classroom(li1,'2期','2018-4-26')
cl4 = Classroom(li1,'3期','2018-5-26')
cl5 = Classroom(go1,'1期','2018-5-26')
cl6 = Classroom(go1,'2期','2018-5-26')
cl7 = Classroom(py2,'1期','2018-5-26')
cl8 = Classroom(py2,'2期','2018-6-26')
cl9 = Classroom(li2,'2期','2018-4-26')
cl10 = Classroom(py3 ,'1期','2018-4-26')
cl11 = Classroom(py3,'2期','2018-5-26')




