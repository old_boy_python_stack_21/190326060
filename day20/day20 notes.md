# day 20



## 1.对象成员

1. 实例变量(普通字段)

   - 对象封装值到对象中,值在对象所在的内存地址中.

     ![1556095466074](day20 notes.assets/1556095466074.png)

## 2. 类的成员

1. 类变量(静态字段)

   - 定义 : 写在类的下一级,和方法同级--->存在于类所在的内存中

   - 访问 : 类.类变量    /       对象.类变量

   - 1. 注意 :  对象.变量--->先去对象中找,找不到再去类中找,再去类的基类中找,找不到报错.

     2. 注意 : 修改或赋值只能改自己内部的封装的值

        ```python
        class Foo:
            xx = 123
        
        obj = Foo()
        print(obj.xx)
        print(Foo.xx)  # 123  123
        
        Foo.xx = 789
        print(obj.xx)
        print(Foo.xx)  # 789   ,789
        
        obj.xx = 2356   # obj对象封装值xx =2356,Foo.xx值不变
        print(obj.xx)  # 2356
        print(Foo.xx)   # 789
        ```

2. 方法(普通方法或者绑定方法)

   - 定义 : 至少要有self参数
   - 执行 : 先创建对象,然后对象调用方法--->对象.方法名()

3. 静态方法

   - 定义 : 

     - @staticmethod在方法上一行
     - 参数无限制(不需要self)

   - 执行

     - 执行静态方法   类.静态方法名()
     - 对象.静态方法名()    **不推荐容易混淆**

     ```python
     class Foo:
         @staticmethod
         def func():
             print(13465)
      
     # 调用方法
     
     Foo.func()
     
     v = Foo()
     v.func()
     
     ```

4. 类方法

   - 定义 : 
     - @classmethod装饰器在方法上一行
     - 必须要有一个参数cls --->指代类本身
   - 执行
     - 执行类方法   类.类方法名()
     - 对象.类方法名()    **不推荐容易混淆**

   ```python
   class Foo:
       @classmethod
       def func(cls,name):
           print(name)
           
   v = Foo()
   v.func('传入的参数name')
   
   Foo.func('54649+45')   # ----> 推荐使用
   ```

   总结 : 

   ```python
   # 问题： @classmethod和@staticmethod的区别？
   """
   一个是类方法一个静态方法。 
   定义：
   	类方法：用@classmethod做装饰器且至少有一个cls参数。
   	静态方法：用staticmethod做装饰器且参数无限制。
   调用：
   	类.方法直接调用。
   	对象.方法也可以调用。 
   """
   
   ```

5. 属性

   - 定义 : 

     - @property装饰器
     - 只有一个self参数

   - 执行

     - 对象.方法名   **注意:不要加括号**

     ```python
     class Foo:
         @property
         def func(self):
             return 4561
     
     obj = Foo()
     print(obj.func)
     ```

     作用 : 

     - 注意：属性存在意义是：访问属性时可以制造出和访问字段完全相同的假象
     - 属性由方法变种而来，如果Python中没有属性，方法完全可以代替其功能
     - Python的属性的功能是：属性内部进行一系列的逻辑计算，最终将计算结果返回。 

### 2.1总结

- 类成员 有 : 
  - 类变量(静态字段)
  - 方法(普通方法/绑定方法)
  - 静态方法@staticmethod,下一行跟方法,类.方法()调用
  - 类方法@classmethon , 下一行跟方法,类.方法()调用,必须要有一个cls参数
  - 属性,@property装饰,下一行跟方法,对象.方法,不加括号调用,是方法的变种
- 对象成员
  - 实例变量(普通字段)--->对象封装的值

## 3.成员修饰符

1. 区分公有和私有

2. 不加任何符号就是公有成员,任何地方都能调用

3. __加上符号,就是私有成员,只有自己才能方法,派生类也无法访问

   ```python
   class Foo:
       __x = 1 #私有成员
       def __init__(self,name):
           self.__name = name   # 私有成员
   
       def func(self):
           print(self.__name)
           
       def __func1(self):
           print('这是私有成员')  #只有经内部其他函数调用,外部对象调用出错
   
   obj = Foo('邓益新')
   # print(obj.__name)           # 报错,无法外部访问,只有通过内部func方法间接访问
   obj.func()
   
   ```

## 4.补充

1. 新式类和经典类

   ```python
   class Foo:
       pass
   
   class Foo(object):
       pass
   
   # 在python3中这俩的写法是一样，因为所有的类默认都会继承object类，全部都是新式类。
   
   
   # 如果在python2中这样定义，则称其为：经典类
   class Foo:
       pass 
   # 如果在python2中这样定义，则称其为：新式类
   class Foo(object):
       pass 
   
   class Base(object):
       pass
   class Bar(Base):
       pass
   
   ```

2. 强制访问私有成员(例外)

   ```python
   # 强制访问私有成员
   
   class Foo:
       def __init__(self,name):
           self.__x = name
   
   
   obj = Foo('alex')
   
   print(obj._Foo__x) # 强制访问私有实例变量--->在私有成员前加上'_类名__私有成员'
   ```

3. 对象之间的嵌套---->列表的嵌套

   ```python
   class School(object):               # 创建类school
       def __init__(self,title,address):
           self.title = title
           self.address = address
           
   class ClassRoom(object):            # 创建教室类
       
       def __init__(self,name,school_object):
           self.name = name
           self.school = school_object
           
   s1 = School('北京','沙河')        # school_object封装title和address
   s2 = School('上海','浦东')
   s3 = School('深圳','南山')
   
   c1 = ClassRoom('全栈21期',s1)    # 嵌套在这里classRoom_object封装相对应的name和school_object
   c1.name
   c1.school.title 
   c1.school.address                # 对象点点点取值即可.类比于列表的嵌套
   ```

   





