#!/usr/bin/env python
# -*- coding:utf-8 -*-

#2 .写函数，检查获取传入列表或元组对象的所有奇数位索引对应的元素，并将其作为新列表返回。
def check(li):
    list_new = []
    for i in range(1,len(li),2):
        list_new.append(li[i])
    return list_new

#3 .写函数，判断用户传入的一个对象（字符串或列表或元组任意）长度是否大于5，并返回真假。

def check_input(content):
    if len(content) > 5:return True
    else:return False

# content = input("请输入内容:")
# check_input()

#4 .写函数，接收两个数字参数，返回比较大的那个数字。
def compare_num(a,b):
    c = a if a >b else b
    return c

#5 .写函数，函数接收四个参数分别是：姓名，性别，年龄，学历。用户通过输入这四个内容，然后将这四个内容传入到函数中，此函数接收到这四个内容，将内容根据"*"拼接起来并追加到一个student_msg文件中。
def file_cperate(name,gender,age,edu):
    content = [name,gender,age,edu]
    content = '\n'+'*'.join(content)
    with open('student_msg.txt','a',encoding='utf-8') as file1:   # a模式默认光标在最后面,追加前先换行.
        file1.write(content)
while True :
    name = input('请输入姓名:')
    if name.upper() == 'Q':
        break
    gender = input('请输入性别:')
    age = input('请输入年龄:')
    edu = input('请输入学历:')
    file_cperate(name,gender,age,edu)

#6 .写函数，在函数内部生成如下规则的列表  [1,1,2,3,5,8,13,21,34,55…]（斐波那契数列），并返回。
# F(1)=1，F(2)=1, F(n)=F(n-1)+F(n-2)
def num_operate(num):  # num -->指定斐波那契数列列表元素不能大于的数
    list1 = [1,1]
    i = 2
    while True:
        if list1[i - 1] + list1[i - 2] > num: break
        list1.append(list1[i-1]+list1[i-2])
        i += 1
    print(list1)
num_operate()
#7 .写函数，验证用户名在文件 data.txt 中是否存在，如果存在则返回True，否则返回False。（函数有一个参数，用于接收用户输入的用户名）

def check_users(user_name):
    with open('data.txt','r',encoding='utf-8') as file1:
        for line in file1:
            line = line.strip()
            line = line.split('|')
            if user_name.strip() == line[1]:return True
            else:return False

# user_name = input('请输入用户名:')
# print(check_users(user_name))
