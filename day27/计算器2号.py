#!/usr/bin/env python        	
# -*- coding:utf-8 -*-	 
# @Project  : s21day26
# @Author : Jack Deng
# @Time   :  2019-05-06 21:42
# @File   : 计算器2号.py
# import numpy as np
# np.set_printoptions(suppress=True)
import re


def re_mul_div(exp):
    """匹配第一个乘除法"""
    ret = re.search('\d+(\.\d+)?[*/]-?\d+(\.\d+)?', exp)
    if not ret:
        return None
    return ret.group()


def mul_div(exp):
    """
    对匹配到的加减法做运算
    :param exp:
    :return:
    """
    a, b, c = re.split('([*/])', exp, 1)
    if b == '*':
        res = float(a) * float(c)
    else:
        res = float(a) / float(c)
    return str(res)


def re_add_sub(exp):
    """匹配第一个加减法"""
    ret = re.search('\d+(\.\d+)?(e\-\d+)?[-+]-?\d+(\.\d+)?(e\-\d+)?', exp)             # 可能会出现科学计数法的问题
    if not ret:
        return None
    print(ret.group())
    return ret.group()

def add_sub(exp):
    """
    对匹配到的加减法做运算
    :param exp:
    :return: 字符串的结果
    """
    lst = re.split('((?:e-\d+)?[-+])', exp, 1)
    if lst[1] == '+':
        res = float(lst[0]) + float(lst[2])
    elif lst[1] == '-':
        res = float(lst[0]) - float(lst[2])
    else :
        if lst[2] == '+':
            res = float(lst[0]+lst[1])+ float(lst[3])
        else:
            res = float(lst[0]+lst[1])- float(lst[3])
    print(res)
    return str(res)



# content = '1-2*((60-30+(-40/5)+(9-25/3+7/42998/399+10568/14))-(-43)/(16-3*2))'
# while True :
#     ret = re.search(r'\([^()]+\)',content)
#     if not ret:
#         print(content)
#         break
#     xxx = ret.group()
#     t = xxx.strip('()')
#     while True:
#         val = re_mul_div(t)
#         if not val:
#             break
#         result = mul_div(val)
#         t = t.replace(val, result, 1)
#     while True:
#         val = re_add_sub(t)
#         if not val:
#             break
#         result = add_sub(val)
#         t = t.replace(val, result, 1)
#     content = content.replace(xxx,t,1)





lst = re.split('[-+])', '9.132168686e-10-8.333333333e-10', 1)
print(lst)

# ((?:e-\d+)?[-+])
# (e\-\d+)?([-+])