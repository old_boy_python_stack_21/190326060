#!/usr/bin/env python        	
# -*- coding:utf-8 -*-	 
# @Project  : s21day26
# @Author : Jack Deng
# @Time   :  2019-05-05 22:15
# @File   : 大作业计算器.py
import re

def re_mul_div(exp):
    """匹配第一个乘除法"""
    ret = re.search('\d+(\.\d+)?[*/]-?\d+(\.\d+)?',exp)
    if not ret :
        return None
    return ret.group()

def mul_div(exp):
    """
    对匹配到的加减法做运算
    :param exp:
    :return:
    """
    a, b, c = re.split('([*/])', exp, 1)
    if b == '*':
        res = float(a) * float(c)
    else:
        res = float(a) / float(c)
    return str(res)

def re_add_sub(exp):
    """匹配第一个加减法"""
    ret = re.search('\d+(\.\d+)?(e\-\d+)?[-+]-?\d+(\.\d+)?(e\-\d+)?', exp)             # 可能会出现科学计数法的问题
    if not ret:
        return None
    print(ret.group())
    return ret.group()


def add_sub(exp):
    """
    对匹配到的加减法做运算
    :param exp:
    :return: 字符串的结果
    """
    print(exp)
    lst = re.split('((?:e-\d+)?[-+])', exp, 1)
    print(lst)
    if lst[1] == '+':
        res = float(lst[0]) + float(lst[2])
    elif lst[1] == '-':
        res = float(lst[0]) - float(lst[2])
    else :
        print(lst)
        if lst[2] == '+':
            res = float(lst[0]+lst[1])+ float(lst[3])
        else:
            res = float(lst[0]+lst[1])- float(lst[3])
    print(res)
    return str(res)



def func(content):
    ret = re.search(r'\([^()]+\)',content)
    xxx = ret.group()
    t = xxx.strip('()')
    while True:
        val = re_mul_div(t)
        if not val:
            break
        result = mul_div(val)
        t = t.replace(val, result, 1)
    while True:
        val = re_add_sub(t)
        if not val:
            break
        result = add_sub(val)
        t = t.replace(val, result, 1)
    content = content.replace(xxx,t,1)
    if not re.search(r'\([^()]+\)',content):
        return content                #  已经没有括号了
    else:
        return func(content)

v = func('1-2*((60-30+(-40/5)+(9-25/3+7/4299/399+10568/14))-(-43)/(16-3*2))')
print(eval('1-2*((60-30+(-40/5)+(9-25/3+7/4299/399+10568/14))-(-43)/(16-3*2))'))
print(v)
