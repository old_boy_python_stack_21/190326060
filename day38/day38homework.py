# !/usr/bin/env python
# -*- coding:utf-8 -*-

create table class(cid int primary key,
caption char(12)
);
insert into class values (1,'三年二班');

insert into class values (2,'一年三班');


insert into class values(3, '三年一班');


create table teacher(tid int primary key,
tname char(12)
);
insert into teacher values(1, '波多');
insert into teacher values(2, '苍空');
insert into teacher values(3, '饭岛');

create table student(sid int,
sname char(12),
gender enum('女','男'),
class_id int(11) NOT NULL,
foreign key(class_id) references class(cid)
on delete cascade
on update cascade
);

insert into student values(1,'钢蛋','女',1);
insert into student values(1,'铁锤','女',1);
insert into student values(1,'山炮','男',2);


create table course(cid int,
cname char(12),
teach_id int(11) NOT NULL,
foreign key(teach_id) references teacher(tid)
on delete cascade
on update cascade
);



insert into course values(1,'生物',1);
insert into course values(2,'体育',1);
insert into course values(3,'物理',2);



create table scorse(sid int not null unique auto_increment,
student_id int,
constraint fk_student foreign key(student_id) references student(sid)
on delete cascade
on update cascade,
corse_id int,
constraint fk_course foreign key(corse_id) references course(cid)
on delete cascade
on update cascade,
number int
);