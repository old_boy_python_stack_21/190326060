## 第十章数据库

### 10.1 引言

1. 数据库

   - 很多功能如果只是通过操作文件来改变数据是非常繁琐的
   - 程序员需要做很多事情
   - 对于多台机器或者多个进程操作用一份数据
   - 程序员自己解决并发和安全问题比较麻烦
   - 自己处理一些数据备份，容错的措施

2. 为了工作方便,引入了数据库

   -  数据库 是一个可以在一台机器上独立工作的，并且可以给我们提供高效、便捷的方式对数据进行增删改查的一种工具。

3. 数据库的优势

   ```python
     1.程序稳定性 ：这样任意一台服务所在的机器崩溃了都不会影响数据和另外的服务。
   
   　　2.数据一致性 ：所有的数据都存储在一起，所有的程序操作的数据都是统一的，就不会出现数据不一致的现象
   
   　　3.并发 ：数据库可以良好的支持并发，所有的程序操作数据库都是通过网络，而数据库本身支持并发的网络操作，不需要我们自己写socket
   
   　　4.效率 ：使用数据库对数据进行增删改查的效率要高出我们自己处理文件很多
   ```

4. 数据库是一个C/S架构的 操作数据文件的一个管理软件
   1. 帮助我们解决并发问题
   2. 能够帮助我们用更简单更快速的方式完成数据的增删改查
   3. 能够给我们提供一些容错、高可用的机制
   4. 权限的认证
   
5. 数据库管理系统DBMS
   - 文件夹 --数据库database   db
   - 数据库管理员 --  DBA

#### 10.1.1分类

1. 关系型数据库
   - sql server
   - oracle 收费、比较严谨、安全性比较 高
             国企 事业单位
             银行 金融行业
   - mysql  开源的
             小公司
             互联网公司
   - sqllite 
2. 非关系型数据库
   - redis
   - mongodb

#### 10.1.2  sql 语言

1. ddl 定义语言
   - 创建用户
     - create user '用户名'@'%'  表示网络可以通讯的所有ip地址都可以使用这个用户名
     - create user '用户名'@'192.168.12.%' 表示192.168.12.0网段的用户可以使用这个用户名
     - create user '用户名'@'192.168.12.87' 表示只有一个ip地址可以使用这个用户名
   - 创建库
     - create  database day38;
   - 创建表
     - create table 表名(字段名 数据类型(长度)，字段名 数据类型(长度)，)
2. dml 操作语言
   1. 数据的
      - 增 insert into
      - 删 delete from
      - 改 update
      - 查 select
   2. select user(); 查看当前用户
      - select database(); 查看当前所在的数据库
      - show
      - show databases:  查看当前的数据库有哪些
      - show tables；查看当前的库中有哪些表
      - desc 表名；查看表结构
      - use 库名；切换到这个库下
3. dcl 控制语言
   1. 给用户授权
      - grant select on 库名.* to '用户名'@'ip地址/段' identified by '密码'
      - grant select,insert
      - grant all

### 10. 2MySql的安装和配置

#### 10.2.1 mysql的CS架构

1. mysqld install  安装数据库服务
2. net start mysql 启动数据库的server端
   - 停止server net stop mysql
3. 客户端可以是python代码也可以是一个程序
           mysql.exe是一个客户端
           mysql -u用户名 -p密码
4. mysql中的用户和权限
       在安装数据库之后，有一个最高权限的用户root
5. mysql -h192.168.12.87 -uroot -p123
6. 我们的mysql客户端不仅可以连接本地的数据库
   - 也可以连接网络上的某一个数据库的server端

#### 10.2.2 mysql 的 一些命令行

1. `mysql>select user();`
       查看当前用户是谁

2. `mysql>set password = password('密码')`
       设置密码

3. 创建用户
   `create user 's21'@'192.168.12.%' identified by '123';`
   `mysql -us21 -p123 -h192.168.12.87`

4. 授权
       `grant all on day37.* to 's21'@'192.168.12.%';`
       授权并创建用户
      ` grant all on day37.* to 'alex'@'%' identified by '123';`

5. 查看文件夹
       `show databases;`
   创建文件夹
       `create databases day37;`

6. **DROP TABLE** ：删除表

7. 库 表 数据
       创建库、创建表  DDL数据库定义语言
       存数据，删除数据,修改数据,查看  DML数据库操纵语句
       grant/revoke  DCL控制权限
   库操作
       `create database 数据库名; ` 创建库
       `show databases;` 查看当前有多少个数据库
      ` select database();`查看当前使用的数据库
       `use 数据库的名字; `切换到这个数据库(文件夹)下
   表操作
       查看当前文件夹中有多少张表
           `show tables;`
       创建表
          ` create table student(id int,name char(4));`
       删除表
           `drop table student;`
       查看表结构
           `desc 表名;`

   操作表中的数据
       数据的增加
          ` insert into student values (1,'alex');`
         `  insert into student values (2,'wusir');`
       数据的查看
          ` select * from student;`
       修改数据
          ` update 表 set 字段名=值`
         `  update student set name = 'yuan';`
          ` update student set name = 'wusir' where id=2;`
       删除数据
           `delete from 表名字;`
           `delete from student where id=1;`

### 10.3 mysql 表的操作

#### 10.3.1 存储引擎

1. 表的存储方式
   1. 存储方式1：MyISAM5.5以下默认存储方式
      - 存储的文件个数：表结构、表中的数据、索引
      - 支持表级锁
      - 不支持行级锁不支持事务不支持外键
   2. 存储方式2：InnoDB5.6以上默认存储方式
      - 存储的文件个数：表结构、表中的数据
      - 支持行级锁、支持表锁
      - 支持事务
      - 支持外键
   3. 存储方式3：MEMORY内存
      - 存储的文件个数：表结构
      - 优势：增删改查都很快
      - 劣势：重启数据消失、容量有限

#### 10.3.2 表相关命令

1. 查看配置项:
   `showvariableslike'%engine%';`
2. 创建表
   ` create table t1 (id int,name char(4));`
   ` create table t2 (id int,name char(4)) engine=myisam;`
   ` create table t3 (id int,name char(4)) engine=memory;`
3. 查看表的结构
       `show create table 表名; `能够看到和这张表相关的所有信息
      ` desc 表名  `             只能查看表的字段的基础信息
           describe 表名

#### 10.3.3为什么用mysql数据库

1. 用什么数据库 ： mysql

   版本是什么 ：5.6
   都用这个版本么 ：不是都用这个版本
   存储引擎 ：innodb
   为什么要用这个存储引擎：
       支持事务 支持外键 支持行级锁
                            能够更好的处理并发的修改问题

#### 10.3.4 表的 数据类型

1. 数值型
   - 整数 int
     `create table t4 (id1 int(4),id2 int(11));`
   - 注意 : -->int默认是有符号的
     - 它能表示的数字的范围不被宽度约束
     - 它只能约束数字的显示宽度
     - `create table t5 (id1 int unsigned,id2 int);`
   - 小数  float
     `create table t6 (f1 float(5,2),d1 double(5,2));`
     `create table t7 (f1 float,d1 double);`
     `create table t8 (d1 decimal,d2 decimal(25,20));`

2. 日期时间

   year 年 ,范围>>>>1901/2155

   date 年月日,范围>>>1000-01-01/9999-12-31

   time 时分秒,

   `datetime、timestamp`  年月日时分秒,前者>>>1000-01-01 00:00:00/9999-12-31 23:59:59,后者>>>1970-01-01 00:00:00/2038

   - `create table t9(
     y year,d date,
     dt datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
     ts timestamp);`

3. 字符串

   char(15)  定长的单位
       alex  alex
       alex
   varchar(15) 变长的单位
       alex  alex4

   哪一个存储方式好？
       varchar ：节省空间、存取效率相对低
       char ：浪费空间，存取效率相对高 长度变化小的

4. enum 和 set型

   enum枚举型,ENUM只允许从值集合中选取单个值，而不能一次取多个值。

   SET和ENUM非常相似，也是一个字符串对象，里面可以包含0-64个成员。根据成员的不同，存储上也有所不同。set类型可以**允许值集合中任意选择1或多个元素进行组合**。对超出范围的内容将不允许注入，而对重复的值将进行自动去重。

   `create table t12(
   name char(12),
   gender ENUM('male','female'),
   hobby set('抽烟','喝酒','烫头','洗脚')
   );`

   `insert into t12 values('alex','不详','抽烟,喝酒,洗脚,洗脚,按摩');`

   `insert into teacher values(1, '波多');`

   