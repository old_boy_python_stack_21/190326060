from django.apps import AppConfig


class AuthorChargeConfig(AppConfig):
    name = 'author_charge'
