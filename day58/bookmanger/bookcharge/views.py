from django.shortcuts import render , redirect , HttpResponse
from publish_charge import models
# Create your views here.

def book_list(request):
    obj_list = models.Book.objects.all().order_by('pun_id')
    return render(request,'book_list.html',{'obj_list':obj_list})

def add_book(request):
    error = ''
    pub_list = models.Publisher.objects.all()
    if request.method == 'POST':
        b_name = request.POST['b_name']
        p_id = request.POST['pub_id']
        print(b_name,p_id)
        if models.Book.objects.filter(b_name=b_name):
            error = '书籍已存在'
        elif not b_name:
            error = '书籍名不能为空'
        else:
            models.Book.objects.create(b_name=b_name,pun_id=p_id)
            return redirect('/book_list/')
    return render(request, 'add_book.html', {'error': error,'pub_list':pub_list})


def del_book(request):
    bid = request.GET.get('id')
    obj_list = models.Book.objects.filter(pk=bid)
    if not obj_list:
        return HttpResponse('要删除的数据不存在')
    obj_list.delete()
    return redirect('/book_list/')


def edit_book(request):
    error = ''
    bid = request.GET.get('id')  # # url上携带的参数  不是GET请求提交参数,
    obj_list = models.Book.objects.filter(pk=bid)
    if not obj_list:
        error = '编辑的书籍不存在'

    # 编辑的书籍存在
    pub_list = models.Publisher.objects.all()

    if request.method == "POST":
        obj = obj_list[0]
        b_name = request.POST.get('b_name')
        p_id = request.POST.get('pub_id')
        if models.Book.objects.filter(b_name=b_name):
            error = '新修改的书籍名称已存在'
        elif obj.b_name == b_name:
            error = '名称未修改'
        elif not b_name:
            error = '名称不能为空'
        if not error:
            # 新修改的名称不在数据库内,不为空,并且已经修改(和原来的名称不同)
            obj.b_name = b_name
            obj.pun_id = p_id
            obj.save()
            return redirect('/book_list/')

    return render(request, 'edit_book.html', {'error': error, 'obj': obj_list,'pub_list':pub_list})