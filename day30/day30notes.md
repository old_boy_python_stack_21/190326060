### python day30

1. TCP协议的黏包现象
   - 什么是黏包现象 : 
     1. 发生在发送端的粘包
        - 由于2个数据的发送时间间隔  +  数据的长度小
        - 突出tcp的优化机制,将2条信息作为一条信息发送出去; 了
        - 为了减少tcp协议中的'确认收到的网络延迟时间
     2. 接收端的黏包
        - TCP协议中数据的传输没有边界,来不及接受多条
        - 数据会在接受端的内核的缓存处黏在一起
     3. 本质  :  接受信息的边界不清晰
2. 解决tcp的黏包问题
   1. 自定义协议(信息)
      - 首先发送报头
        1. 报头是4个字节
        2. 内容是 即将发送的报文的字节长度
        3. struct 模块 
           - pack 方法可以把所有的数字都固定的转换成4字节(即将发送的报文的字节长度)
      - 然后发送报文
   2. 自定义协议2(文件)
      - 我们专门用来做文件发送的协议
        1. 先发送报头字典的字节长度
        2. 咋发送字典(包含:文件名/文件大小..........)
        3. 在发送文件的内容
3. TCP和UDP协议的特点
   - TCP是一个面向连接的,流式的(如同流水),可靠的,慢的  全双工通信
     - 邮件 文件 http web
   - UDP是一个面向数据包的,无连接的,不可靠的,快的,能完成一对一、一对多、多对一、多对多的高效通讯协议
     -  即时聊天工具 视频的在线观看
4. 三次握手
   - accept接受过程中等待客户端的连接
   -  connect客户端发起一个syn链接请求
     - 如果得到了server端响应ack的同时还会再收到一个由server端发来的syc链接请求
     - client端进行回复ack之后，就建立起了一个tcp协议的链接
   - 三次握手的过程再代码中是由accept和connect共同完成的，具体的细节再socket中没有体现出来
5. 四次挥手
   - server和client端对应的在代码中都有close方法
   - 每一端发起的close操作都是一次fin的断开请求，得到'断开确认ack'之后，就可以结束一端的数据发送
   - 如果两端都发起close，那么就是两次请求和两次回复，一共是四次操作
   - 可以结束两端的数据发送，表示链接断开了

#### 2.验证客户端的合法性

1. 客户端是提供给 用户用的 —— 登陆验证 

   - 你的用户 就能看到你的client源码了,不需要自己写客户端了

2. 客户端给机器使用(在公司内部使用客户端获取每日服务端出登录用户数据(日志模块的使用))

   ```python
   ## 方法一
   假定自定义了一个模块md5(),传入了二个字符窜,返回值是加密成md5值的字符窜
   
   # server端
   import os
   import socket
   
   def chat(conn):
       while True:
           msg = conn.recv(1024).decode('utf-8')
           print(msg)
           conn.send(msg.upper().encode('utf-8'))
           
   sk = socket.socket()
   sk.bind(('127.0.0.1',9000))
   sk.listen()
   secret_key = b'alexsb'              # 密钥
   while True:
       conn,addr = sk.accept()
       randseq = os.urandom(32)
       conn.send(randseq)
       md5code = md5(secret_key,randseq)
       ret = conn.recv(32).decode('utf-8')
       print(ret)
       if ret == md5code:
           print('是合法的客户端')
           chat(conn)
       else:
           print('不是合法的客户端')
           conn.close()
   sk.close()
   
   
   # # client.py
   
   import socket
   import time
   
   def chat(sk):
       while True:
           sk.send(b'hello')
           msg = sk.recv(1024).decode('utf-8')
           print(msg)
           time.sleep(0.5)       # 模拟用户操作的时间
   sk = socket.socket()
   sk.connect(('127.0.0.1',9000))
   secret_key = b'alexsb'
   randseq = sk.recv(32)
   md5code = get_md5(secret_key,randseq)
   sk.send(md5code.encode('utf-8'))
   chat(sk)
   sk.close()
   ```

   3 . 利用hamc模块实现摘要算法

   ```python
   def get_hmac(secret_key,randseq):          # 传入密钥  和随机的字节--->都是字节
       h = hmac.new(secret_key,randseq)
       res = h.digest()        # 返回值是个32位的字节
       
   import os
     
   os.urandom(32)    # 返回一个随机的   32个字节
   ```

#### 3.socket的不阻塞实现并发

1. socket的阻塞和非阻塞的区别

   1. 建立连接
      - 阻塞方式下，connect首先发送SYN请求到服务器，当客户端收到服务器返回的SYN的确认时，则connect返回，否则的话一直阻塞。 
      - 非阻塞方式，connect将启用TCP协议的三次握手，但是connect函数并不等待连接建立好才返回，而是立即返回，返回的错误码为EINPROGRESS,表示正在进行某种过程。 
   2. 接收连接
      - 阻塞模式下调用accept()函数，而且没有新连接时，进程会进入睡眠状态，直到有可用的连接，才返回。(类似于input等待输入时) 
      - 非阻塞模式下调用accept()函数立即返回，有连接返回客户端套接字描述符，没有新连接时，将报错误，表示本来应该阻塞。 
   3. 读操作时
      - 阻塞模式下调用read(),recv()等读套接字函数会一直阻塞住，直到有数据到来才返回。当socket缓冲区中的数据量小于期望读取的数据量时，返回实际读取的字节数。当sockt的接收缓冲区中的数据大于期望读取的字节数时，读取期望读取的字节数，返回实际读取的长度。 
      - 对于非阻塞socket而言，socket的接收缓冲区中有没有数据，read调用都会立刻返回。接收缓冲区中有数据时，与阻塞socket有数据的情况是一样的，如果接收缓冲区中没有数据，则报错, 表示该操作本来应该阻塞的，但是由于本socket为非阻塞的socket，因此立刻返回。遇到这样的情况，可以在下次接着去尝试读取。如果返回值是其它负值，则表明读取错误 。

2. 通过socket设置非阻塞实现多个client连接

   ```python
   import socket
   sk = socket.socket()
   sk.bind(('127.0.0.1',9000))
   sk.setblocking(False)             # sk.setblocking(False)     # 设置为非阻塞
   sk.listen()
   
   conn_l = []
   del_l = []
   while True:
       try:
           conn,addr = sk.accept()     # 阻塞，直到有一个客户端来连我
           print(conn)
           conn_l.append(conn)        # 有链接就把该连接对象加入到conn_l列表中操作
       except BlockingIOError:
           for c in conn_l:
               try:
                   msg = c.recv(1024).decode('utf-8')
                   if not msg:              # 接收为'', C端断开连接,利用del_l列表保存断开的C段的conn对象,循环结束后删除
                       del_l.append(c)
                       continue
                   print('-->',[msg])
                   c.send(msg.upper().encode('utf-8'))
               except BlockingIOError:pass
           for c in del_l:
               conn_l.remove(c)
           del_l.clear()
   sk.close()
   
   # socket的非阻塞io模型 + io多路复用实现的
       # 虽然非阻塞，提高了CPU的利用率，但是耗费CPU做了很多无用功
   ```

   client端

   ```python
   import socket
   import time
   sk = socket.socket()
   sk.connect(('127.0.0.1',9000))
   while True:
       sk.send(b'wusir')
       msg = sk.recv(1024)
       print(msg)
       time.sleep(0.2)
   sk.close()
   ```

#### 4.使用socketserver模块实现并发

```python
import socketserver

class Myserver(socketserver.BaseRequestHandler):
    def handle(self):  # 自动触发了handle方法，并且self.request == conn
        msg = self.request.recv(1024).decode('utf-8')
        self.request.send('1'.encode('utf-8'))
        msg = self.request.recv(1024).decode('utf-8')
        self.request.send('2'.encode('utf-8'))
        msg = self.request.recv(1024).decode('utf-8')
        self.request.send('3'.encode('utf-8'))

server = socketserver.ThreadingTCPServer(('127.0.0.1',9000),Myserver)
server.serve_forever()     # 永远连接-->接收



################################
# client1.py
import socket
sk = socket.socket()
sk.connect(('127.0.0.1',9000))
for i in range(3):
    sk.send(b'hello,yuan')
    msg = sk.recv(1024)
    print(msg)
sk.close()
```



