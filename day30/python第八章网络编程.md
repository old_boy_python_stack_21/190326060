## 第八章网络编程

### 8.1   网络应用开发架构

#### 8.1.1.两台机器通过网络来实现通信

####8.1.2.网络开发架构

- C/S架构  迅雷,qq,微信,飞秋等
  - client  客户端
  - server  服务端
- B/S架构   淘宝  游戏  百度   博客园
  - browser   浏览器
  - server  服务端
- 统一程序的入口

#### 8.1.3. 网络基础

1. B/S是特殊的C/S架构

2. 网络基础

   - 网卡 ：是一个实际存在在计算机中的硬件
   - mac地址 ：每一块网卡上都有一个全球唯一的mac地址
   - 交换机 ：是连接多台机器并帮助通讯的物理设备，只认识mac地址
   - 协议 ：两台物理设备之间对于要发送的内容，长度，顺序的一些约定

3. ip地址

   1. ipv4协议  位 的点分十进制  32位2进制表示

      - 0.0.0.0       255.255.255.255

   2. ipv6协议 6位的冒分十六进制 128位2进制表示

      - 0:0:0:0:0:0       F:F:F:F:F:F

   3. 公网ip 个数是有限的.

      - 每一个ip地址要想被所有人访问到，那么这个ip地址必须是你申请的.必须花费费用.(云服务器等等)
      - 内网ip
        - 192.168.0.0 - 192.168.255.255
        - 172.16.0.0 - 172.31.255.255
        - 10.0.0.0 - 10.255.255.255
        - 约定俗成的空出来的ip,用于局域网ip地址的分配

   4. 交换机实现的arp协议

      - 通过ip地址获取一台机器的mac地址
      - 地址解析协议，即ARP（Address Resolution Protocol），是根据IP地址获取物理地址的一个TCP/IP协议。 

   5. 网关ip 一个局域网的网络出口，访问局域网之外的区域都需要经过路由器和网关

      1. 网段 指的是一个地址段 x.x.x.0  x.x.0.0  x.0.0.0

      2. 子网掩码 判断两台机器是否在同一个网段内的

         - 所谓”子网掩码”，就是表示子网络特征的一个参数。它在形式上等同于IP地址，也是一个32位二进制数字，它的网络部分全部为1，主机部分全部为0。比如，IP地址172.16.10.1，如果已知网络部分是前24位，主机部分是后8位，那么子网络掩码就是11111111.11111111.11111111.00000000，写成十进制就是255.255.255.0。

           知道”子网掩码”，我们就能判断，任意两个IP地址是否处在同一个子网络。方法是将两个IP地址与子网掩码分别进行AND运算（两个数位都为1，运算结果为1，否则为0），然后比较结果是否相同，如果是的话，就表明它们在同一个子网络中，否则就不是。 

           ​	一般255.0.0.0    -----       255.255.255.0

           ```python
           # 255.255.255.0 子网掩码
           # 11111111.11111111.11111111.00000000
           
           # 192.168.12.87
           # 11000000.10101000.00001100.01010111
           # 11111111.11111111.11111111.00000000
           # 11000000.10101000.00001100.00000000   192.168.12.0
           
           # 192.168.12.7
           # 11000000.10101000.00001100.00000111
           # 11111111.11111111.11111111.00000000
           # 11000000.10101000.00001100.00000000  192.168.12.0
           ```

           

      3. ip 地址能够确认一台机器

         - IP协议的作用主要有两个，一个是为每一台计算机分配IP地址，另一个是确定哪些地址在同一个子网络 

      4. port       端口

         - 0 - 65535
         - 计算机通过ip + 端口 识别应用程序

####  8.1.4  总结

- 物理设备
  - 网卡--->mac地址,全球唯一
  - 交换机--->完成局域网内部多台计算机的通信
    - 通信方式 : 广播 ,单播 ,组播
    - 只能识别mac地址
    - arp协议   --->地址解析协议
      - 通过ip获取它的mac地址
      - 有交换机完成
      - 广播 单播
  - 路由器 -   -->完成局域网和局域网间的通信
    - 能识别IP地址
    - 网段
    - 网关-->网关ip :
      - 访问局域网外部服务的一个出口ip
- 使用IP地址在网络上定位一台机器
- port +  ip  能在网络上定位一台机器上的一个服务
  - 一台拥有IP地址的主机可以提供许多服务，比如Web服务、FTP服务、SMTP服务等，这些服务完全可以通过1个IP地址来实现。那么，主机是怎样区分不同的网络服务呢？显然不能只靠IP地址，因为IP 地址与网络服务的关系是一对多的关系。实际上是通过“IP地址+端口号”来区分不同的服务的。 



### 8.2  socket 模块的使用

```python
# server.py 服务端
import socket
sk = socket.socket() # 买手机
sk.bind(('127.0.0.1',9000))  # 绑定卡号
sk.listen()                   # 开机
conn,addr = sk.accept()     # 等着接电话
conn.send(b'hello')
msg = conn.recv(1024)
print(msg)
conn.close()    # 挂电话
sk.close()      # 关机

# client.py 客户端
import socket
sk = socket.socket()
sk.connect(('127.0.0.1',9000))
msg = sk.recv(1024)
print(msg)
sk.send(b'byebye')
sk.close()

# 注意：运行时，先执行server.py，再执行client.py
```

- - 

### 8.3 .TCP和UDP协议

##### 8.3.1.TCP协议

- **TCP**（Transmission Control Protocol）可靠的、面向连接的协议（eg:打电话）、传输效率低全双工通信（发送缓存&接收缓存）、面向字节流。使用TCP的应用：Web浏览器；电子邮件、文件传输程序。 当客户和服务器彼此交换数据前，必须先在双方之间建立一个TCP连接，之后才能传输数据。TCP提供超时重发，丢弃重复数据，检验数据，流量控制等功能，保证数据能从一端传到另一端。  

- 类似于打电话,可靠,为了数据和安全和完整性牺牲了效率,慢.是一种全双工通信.

- 建立连接 : 3次握手

  - 

- 断开连接 : 4次挥手

  ![QQ图片20190507145318](D:/python/s21day28/day28notes.assets/QQ%E5%9B%BE%E7%89%8720190507145318.jpg)

  - 

- 在建立连接之后 :

  - 发送的每一条消息都有回执
  - 为了保证数据的完整性,还有重传机制
  - 长连接,会一直占用连接,占用2方的端口
  - 能够传输的数据长度几乎没有限制

- 文件的上传和下载(邮件 ,网盘)

##### 8.3.2  UDP协议

- **UDP**（User Datagram Protocol）不可靠的、无连接的服务，传输效率高（发送前时延小），一对一、一对多、多对一、多对多、面向报文，尽最大努力服务，无拥塞控制。使用UDP的应用：域名系统 (DNS)；视频流；IP语音(VoIP)。，它只是把应用程序传给IP层的数据报发送出去，但是并不能保证它们能到达目的地。由于UDP在传输数据报前不用在客户和服务器之间建立一个连接，且没有超时重发等机制，故而传输速度很快 
- 类似于发短信 ,只负责发短信,能够发送到目的地不清楚,传输很快
- 特点 : 
  - 无连接的,速度很快
  - 可能会丢消息
  - 能够传递的数据的长度是有限的,和设备有关
- 即时通讯类   微信  qq   飞秋  

##### 8.3.3. IO （input,output）操作

1. 输入和输出是相对内存来说的:

- write   send    --->output
- read    recv    --->input

##### 8.3.4    互联网协议   和  osi模型 

1. 按功能不同分为osi七层或tcp/ip五层或tcp/ip四层 模型

   ![1557215039629](D:/python/s21day28/day28notes.assets/1557215039629.png)

------------>应表会传网数物

1. osi五层协议

   |  五层模型  |              协议              |       物理设备        |
   | :--------: | :----------------------------: | :-------------------: |
   |   应用层   | http/https/ftp/smtp/python代码 |                       |
   |   传输层   |      tcp/udp协议    端口       | 四层路由器,四层交换机 |
   |   网络层   |   ipv4  / ipv6协议(ip协议v)    |   路由器,三层交换机   |
   | 数据链接层 |      mac地址     arp协议       |     网卡  交换机      |
   |   物理层   |                                | 中继线,集线器  双绞线 |

### 8.4  socket套接字进阶

1. python的socket模块----->完成socket的功能

2. 工作在应用层和传输层之间的抽象层

   - 帮助我们完成所有信息的组织和拼接

   ![1557216028142](D:/python/s21day28/day28notes.assets/1557216028142.png)

   socket对于程序猿来说是网络操作的底层了

3. Socket是应用层与TCP/IP协议族通信的中间软件抽象层，它是一组接口。在设计模式中，Socket其实就是一个门面模式，它把复杂的TCP/IP协议族隐藏在Socket接口后面，对用户来说，一组简单的接口就是全部，让Socket去组织数据，以符合指定的协swv 1.

4. 议。 

   ```python
   站在自己的角度上看，socket就是一个模块。我们通过调用模块中已经实现的方法建立两个进程之间的连接和通信。
   也有人将socket说成ip+port，因为ip是用来标识互联网中的一台主机的位置，而port是用来标识这台机器上的一个应用程序。
   所以我们只要确立了ip和port就能找到一个应用程序，并且使用socket模块来与之通信
   
   ```

5. socket小问题

   ![1557217002680](D:/python/s21day28/day28notes.assets/1557217002680.png) 

6. tcp端

   ```python
   import socket
   sk = socket.socket()
   sk.bind(('127.0.0.1',8898))  #把地址绑定到套接字
   sk.listen()          #监听链接
   conn,addr = sk.accept() #接受客户端链接
   ret = conn.recv(1024)  #接收客户端信息
   print(ret)       #打印客户端信息
   conn.send(b'hi')        #向客户端发送信息
   conn.close()       #关闭客户端套接字
   sk.close()        #关闭服务器套接字(可选)
   
   
   import socket
   sk = socket.socket()           # 创建客户套接字
   sk.connect(('127.0.0.1',8898))    # 尝试连接服务器
   sk.send(b'hello!')
   ret = sk.recv(1024)         # 对话(发送/接收)
   print(ret)
   sk.close()            # 关闭客户套接字
   
   ```

7. udp端

   ```python
   import socket
   udp_sk = socket.socket(type=socket.SOCK_DGRAM)   #创建一个服务器的套接字
   udp_sk.bind(('127.0.0.1',9000))        #绑定服务器套接字
   msg,addr = udp_sk.recvfrom(1024)
   print(msg)
   udp_sk.sendto(b'hi',addr)                 # 对话(接收与发送)
   udp_sk.close()                         # 关闭服务器套接字
   
   
   
   import socket
   ip_port=('127.0.0.1',9000)
   udp_sk=socket.socket(type=socket.SOCK_DGRAM)
   udp_sk.sendto(b'hello',ip_port)
   back_msg,addr=udp_sk.recvfrom(1024)
   print(back_msg.decode('utf-8'),addr)
   ```

8. 控制台打印颜色

   ```python
   #格式：
   　　设置颜色开始 ：\033[显示方式;前景色;背景色m
   
   #说明：
   前景色            背景色           颜色
   ---------------------------------------
   30                40              黑色
   31                41              红色
   32                42              绿色
   33                43              黃色
   34                44              蓝色
   35                45              紫红色
   36                46              青蓝色
   37                47              白色
                  
                  
                  
   显示方式           意义
   -------------------------
   0                终端默认设置
   1                高亮显示
   4                使用下划线
   5                闪烁
   7                反白显示
   8                不可见
    
   #例子：
   \033[1;31;40m    <!--1-高亮显示 31-前景色红色  40-背景色黑色-->
   \033[0m          <!--采用终端默认设置，即取消颜色设置-->
   ```

   

![1557315007568](D:\python\s21day28\day28notes.assets\1557315007568.png)

- i p + port 确认一台机器上的一个应用

1. ### python day 29

    

   #### 1.socket模块tcp黏包

   1. TCP协议的黏包问题

      同时执行多条命令之后，得到的结果很可能只有一部分，在执行其他命令的时候又接收到之前执行的另外一部分结果，这种显现就是黏包 

   2. 黏包成因

      tcp的拆包机制

      ```python
      当发送端缓冲区的长度大于网卡的MTU时，tcp会将这次发送的数据拆成几个数据包发送出去。 
      MTU是Maximum Transmission Unit的缩写。意思是网络上传送的最大数据包。MTU的单位是字节。 大部分网络设备的MTU都是1500。如果本机的MTU比网关的MTU大，大的数据包就会被拆开来传送，这样会产生很多数据包碎片，增加丢包率，降低网络速度。
      ```

      ```python
      TCP（transport control protocol，传输控制协议）是面向连接的，面向流的，提供高可靠性服务。
      收发两端（客户端和服务器端）都要有一一成对的socket，因此，发送端为了将多个发往接收端的包，更有效的发到对方，使用了优化方法（Nagle算法），将多次间隔较小且数据量小的数据，合并成一个大的数据块，然后进行封包。
      这样，接收端，就难于分辨出来了，必须提供科学的拆包机制。 即面向流的通信是无消息保护边界的。 
      对于空消息：tcp是基于数据流的，于是收发的消息不能为空，这就需要在客户端和服务端都添加空消息的处理机制，防止程序卡住，而udp是基于数据报的，即便是你输入的是空内容（直接回车），也可以被发送，udp协议会帮你封装上消息头发送过去。 
      可靠黏包的tcp协议：tcp的协议数据不会丢，没有收完包，下次接收，会继续上次继续接收，己端总是在收到ack时才会清除缓冲区内容。数据是可靠的，但是会粘包。
      
      ```

   3. udp和tcp一次发送数据长度的限制

      ```python
          用UDP协议发送时，用sendto函数最大能发送数据的长度为：65535- IP头(20) – UDP头(8)＝65507字节。用sendto函数发送数据时，如果发送数据长度大于该值，则函数会返回错误。（丢弃这个包，不进行发送） 
      
          用TCP协议发送时，由于TCP是数据流协议，因此不存在包大小的限制（暂不考虑缓冲区的大小），这是指在用send函数时，数据长度参数不受限制。而实际上，所指定的这段数据并不一定会一次性发送出去，如果这段数据比较长，会被分段发送，如果比较短，可能会等待和下一次数据一起发送。
      
      ```

   4. tcp会发生黏包的2种情况

      - 发送端需要等待缓冲区满才发送出去,造成黏包（发送数据时间间隔很短，数据了很小，会合到一起，产生粘包） 

        ```python
        #_*_coding:utf-8_*_
        import socket
        BUFSIZE=1024
        ip_port=('127.0.0.1',8080)
        
        s=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
        res=s.connect_ex(ip_port)
        
        
        s.send('hello'.encode('utf-8'))
        s.send('egg'.encode('utf-8'))
        
        客户端
        ```

      - 接收方不及时接收缓冲区的包，造成多个包接收,TCP协议的数据没有边界.（客户端发送了一段数据，服务端只收了一小部分，服务端下次再收的时候还是从缓冲区拿上次遗留的数据，产生粘包）  

        ```python
        # 客户端
        #_*_coding:utf-8_*_
        import socket
        BUFSIZE=1024
        ip_port=('127.0.0.1',8080)
        
        s=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
        res=s.connect_ex(ip_port)
        
        
        s.send('hello egg'.encode('utf-8'))
        
        # 服务端
        #_*_coding:utf-8_*_
        from socket import *
        ip_port=('127.0.0.1',8080)
        
        tcp_socket_server=socket(AF_INET,SOCK_STREAM)
        tcp_socket_server.bind(ip_port)
        tcp_socket_server.listen(5)
        
        
        conn,addr=tcp_socket_server.accept()
        
        
        data1=conn.recv(2) #一次没有收完整
        data2=conn.recv(10)#下次收的时候,会先取旧的数据,然后取新的
        
        print('----->',data1.decode('utf-8'))
        print('----->',data2.decode('utf-8'))
        
        conn.close()
        
        
        ```

   5. 总结

      - 黏包只发生在tcp协议中
      - 从表面上看，黏包问题主要是因为发送方和接收方的缓存机制、tcp协议面向流通信的特点。 
      - 实际上，**主要还是因为接收方不知道消息之间的界限，不知道一次性提取多少字节的数据所造成的** 

   #### 2.tcp黏包解决办法

   1. 问题的根源在于，接收端不知道发送端将要传送的字节流的长度，所以解决粘包的方法就是围绕，如何让发送端在发送数据前，把自己将要发送的字节流总大小让接收端知晓，然后接收端来一个死循环接收完所有数据。 

   2. 使用struct模块

      - 我们可以借助一个模块，这个模块可以把要发送的数据长度转换成固定长度的字节。这样客户端每次接收消息之前只要先接受这个固定长度字节的内容看一看接下来要接收的信息大小，那么最终接受的数据只要达到这个值就停止，就能刚好不多不少的接收完整的数据了。 

   3. struct模块

      ```python
      import struct
      struct.pack('i','待发送的数据长度,是一个int类型')---> -2147483648 <= number <= 2147483647
      # 借助struct模块，我们知道长度数字可以被转换成一个标准大小的4字节数字。因此可以利用这个特点来预先发送数据长度。
      ```

      ![1557306460611](D:/python/s21day29/day29notes.assets/1557306460611.png)

      我们还可以把报头做成字典，字典里包含将要发送的真实数据的详细信息，然后json序列化，然后用struck将序列化后的数据长度打包成4个字节（4个字节足够用了） 

### 

1. TCP协议的黏包现象
   - 什么是黏包现象 : 
     1. 发生在发送端的粘包
        - 由于2个数据的发送时间间隔  +  数据的长度小
        - 突出tcp的优化机制,将2条信息作为一条信息发送出去; 了
        - 为了减少tcp协议中的'确认收到的网络延迟时间
     2. 接收端的黏包
        - TCP协议中数据的传输没有边界,来不及接受多条
        - 数据会在接受端的内核的缓存处黏在一起
     3. 本质  :  接受信息的边界不清晰
2. 解决tcp的黏包问题
   1. 自定义协议(信息)
      - 首先发送报头
        1. 报头是4个字节
        2. 内容是 即将发送的报文的字节长度
        3. struct 模块 
           - pack 方法可以把所有的数字都固定的转换成4字节(即将发送的报文的字节长度)
      - 然后发送报文
   2. 自定义协议2(文件)
      - 我们专门用来做文件发送的协议
        1. 先发送报头字典的字节长度
        2. 咋发送字典(包含:文件名/文件大小..........)
        3. 在发送文件的内容
3. TCP和UDP协议的特点
   - TCP是一个面向连接的,流式的(如同流水),可靠的,慢的  全双工通信
     - 邮件 文件 http web
   - UDP是一个面向数据包的,无连接的,不可靠的,快的,能完成一对一、一对多、多对一、多对多的高效通讯协议
     - 即时聊天工具 视频的在线观看
4. 三次握手
   - accept接受过程中等待客户端的连接
   - connect客户端发起一个syn链接请求
     - 如果得到了server端响应ack的同时还会再收到一个由server端发来的syc链接请求
     - client端进行回复ack之后，就建立起了一个tcp协议的链接
   - 三次握手的过程再代码中是由accept和connect共同完成的，具体的细节再socket中没有体现出来
5. 四次挥手
   - server和client端对应的在代码中都有close方法
   - 每一端发起的close操作都是一次fin的断开请求，得到'断开确认ack'之后，就可以结束一端的数据发送
   - 如果两端都发起close，那么就是两次请求和两次回复，一共是四次操作
   - 可以结束两端的数据发送，表示链接断开了

#### 8.4.2 .验证客户端的合法性

1. 客户端是提供给 用户用的 —— 登陆验证 

   - 你的用户 就能看到你的client源码了,不需要自己写客户端了

2. 客户端给机器使用(在公司内部使用客户端获取每日服务端出登录用户数据(日志模块的使用))

   ```python
   ## 方法一
   假定自定义了一个模块md5(),传入了二个字符窜,返回值是加密成md5值的字符窜
   
   # server端
   import os
   import socket
   
   def chat(conn):
       while True:
           msg = conn.recv(1024).decode('utf-8')
           print(msg)
           conn.send(msg.upper().encode('utf-8'))
           
   sk = socket.socket()
   sk.bind(('127.0.0.1',9000))
   sk.listen()
   secret_key = b'alexsb'              # 密钥
   while True:
       conn,addr = sk.accept()
       randseq = os.urandom(32)
       conn.send(randseq)
       md5code = md5(secret_key,randseq)
       ret = conn.recv(32).decode('utf-8')
       print(ret)
       if ret == md5code:
           print('是合法的客户端')
           chat(conn)
       else:
           print('不是合法的客户端')
           conn.close()
   sk.close()
   
   
   # # client.py
   
   import socket
   import time
   
   def chat(sk):
       while True:
           sk.send(b'hello')
           msg = sk.recv(1024).decode('utf-8')
           print(msg)
           time.sleep(0.5)       # 模拟用户操作的时间
   sk = socket.socket()
   sk.connect(('127.0.0.1',9000))
   secret_key = b'alexsb'
   randseq = sk.recv(32)
   md5code = get_md5(secret_key,randseq)
   sk.send(md5code.encode('utf-8'))
   chat(sk)
   sk.close()
   ```

   3 . 利用hamc模块实现摘要算法

   ```python
   def get_hmac(secret_key,randseq):          # 传入密钥  和随机的字节--->都是字节
       h = hmac.new(secret_key,randseq)
       res = h.digest()        # 返回值是个32位的字节
       
   import os
     
   os.urandom(32)    # 返回一个随机的   32个字节
   ```

#### 8.4.3   socket的不阻塞实现并发

1. socket的阻塞和非阻塞的区别

   1. 建立连接
      - 阻塞方式下，connect首先发送SYN请求到服务器，当客户端收到服务器返回的SYN的确认时，则connect返回，否则的话一直阻塞。 
      - 非阻塞方式，connect将启用TCP协议的三次握手，但是connect函数并不等待连接建立好才返回，而是立即返回，返回的错误码为EINPROGRESS,表示正在进行某种过程。 
   2. 接收连接
      - 阻塞模式下调用accept()函数，而且没有新连接时，进程会进入睡眠状态，直到有可用的连接，才返回。(类似于input等待输入时) 
      - 非阻塞模式下调用accept()函数立即返回，有连接返回客户端套接字描述符，没有新连接时，将报错误，表示本来应该阻塞。 
   3. 读操作时
      - 阻塞模式下调用read(),recv()等读套接字函数会一直阻塞住，直到有数据到来才返回。当socket缓冲区中的数据量小于期望读取的数据量时，返回实际读取的字节数。当sockt的接收缓冲区中的数据大于期望读取的字节数时，读取期望读取的字节数，返回实际读取的长度。 
      - 对于非阻塞socket而言，socket的接收缓冲区中有没有数据，read调用都会立刻返回。接收缓冲区中有数据时，与阻塞socket有数据的情况是一样的，如果接收缓冲区中没有数据，则报错, 表示该操作本来应该阻塞的，但是由于本socket为非阻塞的socket，因此立刻返回。遇到这样的情况，可以在下次接着去尝试读取。如果返回值是其它负值，则表明读取错误 。

2. 通过socket设置非阻塞实现多个client连接

   ```python
   import socket
   sk = socket.socket()
   sk.bind(('127.0.0.1',9000))
   sk.setblocking(False)             # sk.setblocking(False)     # 设置为非阻塞
   sk.listen()
   
   conn_l = []
   del_l = []
   while True:
       try:
           conn,addr = sk.accept()     # 阻塞，直到有一个客户端来连我
           print(conn)
           conn_l.append(conn)        # 有链接就把该连接对象加入到conn_l列表中操作
       except BlockingIOError:
           for c in conn_l:
               try:
                   msg = c.recv(1024).decode('utf-8')
                   if not msg:              # 接收为'', C端断开连接,利用del_l列表保存断开的C段的conn对象,循环结束后删除
                       del_l.append(c)
                       continue
                   print('-->',[msg])
                   c.send(msg.upper().encode('utf-8'))
               except BlockingIOError:pass
           for c in del_l:
               conn_l.remove(c)
           del_l.clear()
   sk.close()
   
   # socket的非阻塞io模型 + io多路复用实现的
       # 虽然非阻塞，提高了CPU的利用率，但是耗费CPU做了很多无用功
   ```

   client端

   ```python
   import socket
   import time
   sk = socket.socket()
   sk.connect(('127.0.0.1',9000))
   while True:
       sk.send(b'wusir')
       msg = sk.recv(1024)
       print(msg)
       time.sleep(0.2)
   sk.close()
   ```

#### 8.4.4   使用socketserver模块实现并发

```python
import socketserver

class Myserver(socketserver.BaseRequestHandler):
    def handle(self):  # 自动触发了handle方法，并且self.request == conn
        msg = self.request.recv(1024).decode('utf-8')
        self.request.send('1'.encode('utf-8'))
        msg = self.request.recv(1024).decode('utf-8')
        self.request.send('2'.encode('utf-8'))
        msg = self.request.recv(1024).decode('utf-8')
        self.request.send('3'.encode('utf-8'))

server = socketserver.ThreadingTCPServer(('127.0.0.1',9000),Myserver)
server.serve_forever()     # 永远连接-->接收



################################
# client1.py
import socket
sk = socket.socket()
sk.connect(('127.0.0.1',9000))
for i in range(3):
    sk.send(b'hello,yuan')
    msg = sk.recv(1024)
    print(msg)
sk.close()
```

server = 

