



# 21期python全栈day01

## 一、计算机认识

1. 用户
2. 软件，类似微信、QQ、游戏等应用程序，由程序员编写，在系统中运行，完成各种活动，方便人们使用。
3. 操作系统，主要分为windows系统、Linux系统、Mac系统。
   1. windows系统，主要有win xp、win 7、win 8、win 10、win server系统等等
   2. Linux系统，主要有centos系统，图形化界面差；ubuntu系统 , 个人开发（图形化比较好）；redhat，企业级，主要商业用途。
   3. Mac系统，对办公和开发都很友好。
4. 硬件，主要有CPU(中央处理器)、主板、内存、硬盘、显卡、显示器。

## 二、安装解释器

学习编程语言，需要学会改语言的语法规则和安装解释器，解释器又名编译器/虚拟机。

- 官网下载python 2.7.16（2020不在维护）

- 下载python 3.6.8（推荐使用，稳定）

- 安装完成后，添加环境变量，以便于以后快速找到python解释器，注意最好将环境变量添加到系统变量中。

  ![1553686459283](C:\Users\ADMINI~1\AppData\Local\Temp\1553686459283.png)

## 三、第一个脚本

1. 打开电脑终端， win键+R，打开 cmd 端口

2. 输入命令：解释器路径     脚本路径

   ```python
   print('你好，世界')
   ```

   

## 四、编码方式

1. 编码主要3种，分别为：

   - ASCII编码，英文和字符的编码，一个字母由8位二进制数表示，类似01010010。有2^8次表达方式。

   - Unicode编码，又名万国码，能表示所有语言（还有空余），由32位二进制表示，有2^32种表达方式。

   - utf-8编码，对Unicode码压缩表达，最少8位表示一个东西。

   注意：8位等于1字节，utf-8码中，中文用3字节表示。

2. python解释器编码

   - python2，默认编码为ASCII，需要在文件头添加

     ```python
     # -*- coding:utf-8 -*-
     print('你好')
     ```

   - python3：默认编码为utf-8 

   

3. 文件编码

   - 编写文件是，保存文件要用utf-8编码方式保存
   - 用什么编码写文件保存，就要用什么编码读取文件。

## 五、输出

```python
print（你想输出的内容）
```

python2中，输出是: print ”你想输出的“（注意：print和引号间有空格）

python3中，输出是: print（“你想输出的”）

## 六、数据类型

- 字符串
  - 单引号，如'王飞'
  - 双引号，如”王大“
  - 三引号，如“”“王小”“”，三引号支持换行。
- 整型（整数类型）
- 布尔类型（true、false）

注意：整型数据可以+和×，字符串数据也可以+和×。如

```
name='邓益新'
new_name=name*3
print（new_name）
```



## 七、变量

```python
content = '你的名字是小王'
age = 666
```

创建一个变量，给变量赋值，需要

1. 变量名只能包含：字母/数字/下划线

2. 数字不能开头

3. 不能是python的关键字。

   [‘and’, ‘as’, ‘assert’, ‘break’, ‘class’, ‘continue’, ‘def’, ‘del’, ‘elif’, ‘else’, ‘except’, ‘exec’, ‘finally’, ‘for’, ‘from’, ‘global’, ‘if’, ‘import’, ‘in’, ‘is’, ‘lambda’, ‘not’, ‘or’, ‘pass’, ‘print’, ‘raise’, ‘return’, ‘try’, ‘while’, ‘with’, ‘yield’]

4. 建议：

   - 见名知意： name = "jack"   age= 18 
   - 用下划线连接：jack_dad = "王军"  

## 八、输入

input语句：

```python
name=input('请输入你的用户名:')
password=input('请输入你的密码')
print(content)
print(password)
```

注意：

1. input语句输入得到的内容永远是字符串。
2. python2的输入语句是:raw_input('')。
3. python3的输入语句是;input('')。

## 九、注释

编程代码一定要做注释，编程代码行数太多了。分为二类，如

```python
# 单行注释，不参与代码运算

"""
多行注释，
不参与程序运算
"""
```

## 十、条件判断

- 最简单条件判断

```python
age = input('请输入你的年龄：')
new_age=int(age)
# input输入的数据类型是字符串，需要用int语句把字符串数据转化为整型数据。
if new_age >= 18:
	print('你已经是成年了人了')
```

- 初级语句

```python
gender = input('请输入你的性别：')
# 默认不是男性就是女性
if gender == '男':
	print('走开')
else:
	print('来呀，快活呀')
```

- elif语句

```
gender = input('请输入你的性别：')
# 性别有男、女、人妖多种选择
if gender == '男':
	print('走开')
elif gender == '女':
	print('来呀，快活呀')
else：
	print（'找##去，他是gay'）
```

elif语句可以用无限次使用，如果次数过多会有其他语句使用。

- and语句，python的关键字之一，表示并且的意思。
