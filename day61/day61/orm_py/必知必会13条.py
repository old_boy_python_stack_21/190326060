#!/usr/bin/env python
# -*- coding:utf-8 -*-

import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "day61.settings")

import django

django.setup()
from orm_test import models

查找所有书名里包含金老板的书
ret = models.Book.objects.filter(title__contains="金老板")

查找出版日期是2018年的书
ret = models.Book.objects.filter(publish_date__year=2018)

查找出版日期是2017年的书名
ret = models.Book.objects.filter(publish_date__year=2017).values('title')

查找价格大于10元的书
ret = models.Book.objects.filter(price__gt =10)

查找价格大于10元的书名和价格
ret = models.Book.objects.filter(price__gt=10).values('title','price')

查找memo字段是空的书
ret = models.Book.objects.filter(memo__isnull=True)

查找在北京的出版社
ret = models.Publisher.objects.filter(city="北京")

查找名字以沙河开头的出版社
ret = models.Publisher.objects.filter(name__startswith='沙河')



查找每个出版社出版价格最高的书籍价格
from django.db.models import Count, Min, Max, Sum
ret = models.Book.objects.values("publisher_name").annotate(max=Max("price"))


查找每个出版社的名字以及出的书籍数量


查找作者名字里面带“小”字的作者
ret= models.Author.objects.filter(name__contains='小')

查找年龄大于30岁的作者
ret = models.Author.objects.filter(age__gt=30)

查找手机号是155开头的作者
ret = models.Author.objects.filter(phone__startswith='155')


查找手机号是155开头的作者的姓名和年龄
ret = models.Author.objects.filter(phone__startswith='155').values("name","age")


查找每个作者写的价格最高的书籍价格
ret = models.Book.objects.values("author_name").annotate(max=Max("price"))

查找每个作者的姓名以及出的书籍数量
ret = models.Book.objects.values("author_name").count()

查找书名是“跟金老板学开车”的书的出版社
ret = models.Publisher.objects.filter(book__title='跟金老板学开车')


查找书名是“跟金老板学开车”的书的出版社的名称
ret = models.Publisher.objects.filter(book__title='跟金老板学开车').values("name")


查找书名是“跟金老板学开车”的书的出版社出版的其他书籍的名字和价格
ret = models.Publisher.objects.filter(book__title='跟金老板学开车')
obj = models.Book.objects.filter(publisher=ret[0])


查找书名是“跟金老板学开车”的书的所有作者
ret = models.Author.objects.filter(book__title='跟金老板学开车')


查找书名是“跟金老板学开车”的书的作者的年龄
ret = models.Author.objects.filter(book__title='跟金老板学开车').values("age")


#!/usr/bin/env python
# -*- coding:utf-8 -*-

import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "day61.settings")

import django

django.setup()
from orm_test import models

查找所有书名里包含金老板的书
ret = models.Book.objects.filter(title__contains="金老板")

查找出版日期是2018年的书
ret = models.Book.objects.filter(publish_date__year=2018)

查找出版日期是2017年的书名
ret = models.Book.objects.filter(publish_date__year=2017).values('title')

查找价格大于10元的书
ret = models.Book.objects.filter(price__gt =10)

查找价格大于10元的书名和价格
ret = models.Book.objects.filter(price__gt=10).values('title','price')

查找memo字段是空的书
ret = models.Book.objects.filter(memo__isnull=True)

查找在北京的出版社
ret = models.Publisher.objects.filter(city="北京")

查找名字以沙河开头的出版社
ret = models.Publisher.objects.filter(name__startswith='沙河')



查找每个出版社出版价格最高的书籍价格
from django.db.models import Count, Min, Max, Sum
ret = models.Book.objects.values("publisher_name").annotate(max=Max("price"))


查找每个出版社的名字以及出的书籍数量


查找作者名字里面带“小”字的作者
ret= models.Author.objects.filter(name__contains='小')

查找年龄大于30岁的作者
ret = models.Author.objects.filter(age__gt=30)

查找手机号是155开头的作者
ret = models.Author.objects.filter(phone__startswith='155')


查找手机号是155开头的作者的姓名和年龄
ret = models.Author.objects.filter(phone__startswith='155').values("name","age")


查找每个作者写的价格最高的书籍价格
ret = models.Book.objects.values("author_name").annotate(max=Max("price"))

查找每个作者的姓名以及出的书籍数量
ret = models.Book.objects.values("author_name").count()

查找书名是“跟金老板学开车”的书的出版社
ret = models.Publisher.objects.filter(book__title='跟金老板学开车')


查找书名是“跟金老板学开车”的书的出版社的名称
ret = models.Publisher.objects.filter(book__title='跟金老板学开车').values("name")


查找书名是“跟金老板学开车”的书的出版社出版的其他书籍的名字和价格
ret = models.Publisher.objects.filter(book__title='跟金老板学开车')
obj = models.Book.objects.filter(publisher=ret[0])


查找书名是“跟金老板学开车”的书的所有作者
ret = models.Author.objects.filter(book__title='跟金老板学开车')


查找书名是“跟金老板学开车”的书的作者的年龄
ret = models.Author.objects.filter(book__title='跟金老板学开车').values("age")


