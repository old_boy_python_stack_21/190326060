## 第七章面向对象

### 7.4 对象详解

#### 7.4.1 数据结构栈

```python
# 后进先出
class Stack(object):
    def __init__(self):
        self.data_list = []

    def push(self, val):
        """
        向栈中压入一个数据（入栈）
        :param val:
        :return:
        """
        self.data_list.append(val)

    def pop(self):
        """
        从栈中拿走一个数据（出栈）
        :return:
        """
        return self.data_list.pop()    # 默认删除最后一个拿出来
```

#### 7.4.2可迭代对象

1. #### 表象：可以被for循环对象就可以称为是可迭代对象

2. 如何让一个对象变成可迭代对象？

   在类中实现`__iter__`方法且返回一个迭代器（生成器）

```python
# __iter__方法,返回一个迭代器
class Foo:
    def __iner__(self):
        return iter([1,2,3,4,5,6,7])
    
obj = Foo()
for i in obj:    # 字动执行__iter__方法
    print(i)
```

#### 7.4.3约束

```python
# 约束字类中必须写send方法，如果不写，则调用时候就报抛出 NotImplementedError 
class Interface(object):
    def send(self):
        raise NotImplementedError('自动抛出异常')    # 其派生类中必须要有send方法
        
class Message(Interface):
    def send(self):
        print('发送短信')z
        
class Email(Interface):
    def send(self):
        print('发送邮件')
```

#### 7.4.4 反射

根据字符粗的形式去某个对象中 操作 他的成员。 

- getattr--->getattr(对象,"字符串")     根据字符粗的形式去某个对象中 获取 对象的成员。 

  ```python
  class Foo(object):
      country = '中国'
  
      def __init__(self,name,age):
          self.name = name
          self.age = age
  
      def func(self):
          print(123)
  
      @staticmethod
      def funcxxx():
          print('邓益新')
  
  obj = Foo('小明',30)
  getattr(Foo,'funcxxx')()
  v = getattr(obj,'name')
  print(v)
  val = getattr(obj,'func')()
  print(getattr(Foo,'country'))
  
  ```

- hasattr--->对象,'字符串')   根据字符粗的形式去某个对象中判断是否有该成员。 

  ```python
  class Foo(object):
      country = '中国'
  
      def __init__(self,name,age):
          self.name = name
          self.age = age
  
      def func(self):
          print(123)
  
      @staticmethod
      def funcxxx():
          print('邓益新')
  
  obj = Foo('小明',30)
  print(hasattr(Foo,'funcxxx'))
  v = hasattr(obj,'xiaoming')
  print(v)
  
  print(hasattr(Foo,'country'))
  ```

- setattr,(对象,'变量','值')   根据字符粗的形式去某个对象中设置成员。 

  ```python
  class Foo(object):
      country = '中国'
  
  
      def func(self):
          print(123)
  
      @staticmethod
      def funcxxx():
          print('邓益新')
  
  def func123():
      print(12316512)
      return 456
  
  obj = Foo()
  obj.k1 = 999
  setattr(obj,'k1',func123) # obj.k1 = 123
  obj.k1()
  
  
  # 可以设置函数和变量,都可以
  
  #!/usr/bin/env python
  # -*- coding:utf-8 -*-
  from wsgiref.simple_server import make_server
  
  class View(object):
      def login(self):
          return '登陆'
  
      def logout(self):
          return '等处'
  
      def index(self):
          return '首页'
  
  
  def func(environ,start_response):
      start_response("200 OK", [('Content-Type', 'text/plain; charset=utf-8')])
      #
      obj = View()
      # 获取用户输入的URL
      method_name = environ.get('PATH_INFO').strip('/')
      if not hasattr(obj,method_name):
          return ["sdf".encode("utf-8"),]
      response = getattr(obj,method_name)()
      return [response.encode("utf-8")  ]
  
  # 作用：写一个网站，用户只要来方法，就自动找到第三个参数并执行。
  server = make_server('192.168.12.87', 8000, func)
  server.serve_forever()
  
  ```

- delattr-->delattr(对象,'变量')   根据字符粗的形式去某个对象中删除成员。

  ```python
  class Foo:
      pass
  
  obj = Foo()
  obj.k1 = 999
  delattr(obj,'k1')
  print(obj.k1)
  
  ###############################
  class Foo(object):
      country = '中国'
  
  
      def func(self):
          print(123)
  
      @staticmethod
      def funcxxx():
          print('邓益新')
  
  
  def func123():
      print(12316512)
      return 456
  
  obj = Foo()
  obj.k1 = 999
  delattr(obj,'funcxxx')
  obj.funcxxx()              # 报错
  ```

#### 7.4.5.python一切皆对象

- py文件
- 包
- 类
- 对象

python一切皆对象，所以以后想要通过字符串的形式操作其内部成员都可以通过反射的机制实现

#### 7.4.6模块  : importlib

根据字符串的形式导入模块。

```python
import importlib

# 用字符串的形式导入模组
val= importlib.import_module('123.456')

print(getattr(val,'func333')())
```

![1556279304314](day 22notes.assets/1556279304314.png)

