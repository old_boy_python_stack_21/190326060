#!/usr/bin/env python        	
# -*- coding:utf-8 -*-	 
#@Project  : s21dya22 
#@Author : Jack Deng       
#@Time   :  2019-04-26 16:48
#@File   : day22_homework.py


# 1

class Foo1:
    """
    栈数据结构,后进先出
    """

    def __init__(self):
        self.math_list = []

    def push(self,val):
        """
        向栈中压入一个数据
        :param val:
        :return:
        """
        self.math_list.append(val)

    def pop(self):
        """
        往栈中取出一个数据,从最后取
        :return:
        """
        self.math_list.pop()

# 2.请是用面向对象实现队列（先进先出）
class Base:
    """
    实现先进先出
    """

    def __init__(self):
        self.math_list = []

    def push(self,val):
        """
        向栈中压入一个数据
        :param val:
        :return:
        """
        self.math_list.insert(0,val)

    def pop(self):
        """
        往栈中取出一个数据,从最后取
        :return:
        """
        self.math_list.pop()

# 3.如何实现一个可迭代对象？-->类中加入__iter__方法即可
class Oppo:

    def __iter__(self):
        print('执行iter方法')
        return iter(range(100))
"""
obj = Oppo()
for item in obj:
    print(item)
"""

# 4 . 看代码写结果
"""
class Foo(object):

    def __init__(self):
        self.name = '武沛齐'
        self.age = 100


obj = Foo()
setattr(Foo, 'email', 'wupeiqi@xx.com')   # 类变量创建

v1 = getattr(obj, 'email')
v2 = getattr(Foo, 'email')

print(v1, v2)
"""
# 结果 :   wupeiqi@xx.com      wupeiqi@xx.com

# 5.补充代码
"""
li = ['李杰','女神','金鑫','武沛齐']

name = input('请输入要删除的姓氏：')   # 如输入“李”，则删除所有姓李的人。

class XiaoMi:
    
# #方法一:倒序遍历列表,可以避免这种索引问题
    def func1(self,list,name):
        for i in list[::-1]:
            if name in i:
                list.remove(i)
        return list

# 方法二 ,创建一个相同的列表:
    def func2(self,list,name):
        for i in li[::-1]:
            if name in i:
                li.remove(i)
        print(li)


"""

# 6. 字典删除指定数据

"""
class User(object):
    def __init__(self, name, age):
        self.name = name
        self.age = age

    @staticmethod
    def del_name(info,name):
        for item in info:
            if getattr(item,'name') == name:
                info.remove(item)
        return info



info = [User('武沛齐', 19), User('李杰', 73), User('景女神', 16)]  # 列表嵌套对象

name = input('请输入要删除的用户姓名：')
print(User.del_name(info,name))
"""

# 7.补充代码实现：校园管理系统。


class User(object):
    def __init__(self, name, email, age):
        self.name = name
        self.email = email
        self.age = age

    def __str__(self):
        return self.name


class School(object):
    # """学校"""

    def __init__(self):
        # 员工字典，格式为：{"销售部": [用户对象,用户对象,] }
        self.user_dict = {}

    def invite(self, department, user_object):
        # """
        # 招聘，到用户信息之后，将用户按照指定结构添加到 user_dict结构中。
        # :param department: 部门名称，字符串类型。
        # :param user_object: 用户对象，包含用户信息。
        # :return:
        # """
        if department not in self.user_dict:
            self.user_dict[department] = [user_object]
        else:
            self.user_dict[department].append(user_object)
        print('添加员工信息成功')

    def dimission(self, username, department=None):
        # """
        # 离职，将员工从字典中移除。
        # :param username: 员工姓名
        # :param department: 部门名称，如果未指定部门名称，则遍历找到所有员工信息，并将在员工字典中移除。
        # :return:
        # """
        flag = False
        if not department:                   # 未指定部门名称
            for item in self.user_dict:
                for  user_object in self.user_dict[item]:
                    if user_object == username :
                        self.user_dict[item].remove(user_object)
                        flag = True
        else :
            if not self.user_dict.get(department):
                print('员工部门输入错误,请重新输入')
                return
            for  user_object in self.user_dict[department]:
                if user_object == username:
                    self.user_dict[department].remove(user_object)
                    flag = True
        if not flag :
            print('没找到该员工')
            return
        print('移除员工成功')


    def run(self):
        # """
        # 主程序
        # :return:
        # """
        while True :
            choice = input('请选择你要进行的功能:invite/dimission:')
            if not hasattr(self,choice):
                print('选择功能不存在,请重新选择')
                continue
            try :
                if choice == 'invite' :
                    content = input('请输入你的部门,姓名,email,年龄,以逗号隔开')
                    content = content.split(',')
                    user_object = User(*content[1:])
                    getattr(self, choice)(content[0],user_object)
                else :
                    content = input('请输入离职员工的姓名,部门(部门可忽略)')
                    content = content.split(',')
                    getattr(self, choice)(*content)
            except Exception as e:
                print('输入错误,请重新输入')


if __name__ == '__main__':
    obj = School()
    obj.run()



# 8.请编写网站实现如下功能。
"""需求：

实现 MIDDLEWARE_CLASSES 中的所有类，并约束每个类中必须有process方法。

用户访问时，使用importlib和反射 让 MIDDLEWARE_CLASSES 中的每个类对 login、logout、index 方法的返回值进行包装，最终让用户看到包装后的结果。
如：

用户访问 : http://127.0.0.1:8000/login/ ，
页面显示： 【csrf】【auth】【session】 登录 【session】 【auth】 【csrf】

用户访问 : http://127.0.0.1:8000/index/ ，
页面显示： 【csrf】【auth】【session】 登录 【session】 【auth】 【csrf】

即：每个类都是对view中返回返回值的内容进行包装。
"""

MIDDLEWARE_CLASSES = [
    'utils.session.SessionMiddleware',
    'utils.auth.AuthMiddleware',
    'utils.csrf.CrsfMiddleware',
]

import importlib
from wsgiref.simple_server import make_server


class View(object):
    def login(self):
        return '登陆'

    def logout(self):
        return '等处'

    def index(self):
        return '首页'


def func(environ,start_response):
    start_response("200 OK", [('Content-Type', 'text/plain; charset=utf-8')])
    obj = View()
    method_name = environ.get('PATH_INFO').strip('/')
    if not hasattr(obj,method_name):
        return ["".encode("utf-8"),]
    response = getattr(obj,method_name)()

    for item in MIDDLEWARE_CLASSES:
        path, class_name = item.rsplit('.', 1)
        val = importlib.import_module(path)
        response = getattr(val, class_name)().process(response)

    return [response.encode("utf-8")  ]


server = make_server('127.0.0.1', 8000, func)
server.serve_forever()