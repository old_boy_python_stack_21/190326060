#!/usr/bin/env python        	
# -*- coding:utf-8 -*-	 
#@Project  : s21dya22 
#@Author : Jack Deng       
#@Time   :  2019-04-27 16:04
#@File   : MIDDLEWARE_CLASSES.py

from utils.utils import utils

class CrsfMiddleware(utils):

    def process(self,text):
        return '【csrf】 %s 【csrf】' %text
