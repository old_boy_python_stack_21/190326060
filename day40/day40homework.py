create table book (bid int primary key auto_increment,
book_name char(12),
author char(12),
publish char(12),
price float(5,2),
publish_date date
);


insert into book(book_name,author,publish,price,publish_date) values
('倚天屠龙记' , 'egon' , '北京工业地雷出版社' , 70 , 20190701),
('九阳神功' , 'alex' , '人民音乐不好听出版社' , 5 , 20180704),
('九阴真经' , 'yuan' , '北京工业地雷出版社' , 62 , 20170712),
('九阴白骨爪' , 'jin' , '人民音乐不好听出版社' , 40 , 20190807),
('独孤九剑' , 'alex' , '北京工业地雷出版社' , 12 , 20170901),
('降龙十巴掌' , 'egon' , '知识产权没有用出版社' , 20 , 20190705),
('葵花宝典' , 'yuan' , '知识产权没有用出版社' , 33 , 20190802);


1.查询egon写的所有书和价格

select book_name ,price from book where author = 'egon';



2.找出最贵的图书的价格
select max(price) from book;
selectavg()
3.求所有图书的均价
select avg(price) from book;

4.将所有图书按照出版日期排序
select book_name from book order by publish_date;(从小到大,即最早出版在前)

select book_name from book order by publish_date desc;  (最新出版在前)

5.查询alex写的所有书的平均价格
select author,avg(price) from book where author = 'alex' ;

6.查询人民音乐不好听出版社出版的所有图书
select book_name from book where publish = '人民音乐不好听出版社';

7.查询人民音乐出版社出版的alex写的所有图书和价格
select book_name, price from book where publish = '人民音乐不好听出版社' and author = 'alex';


8.找出出版图书均价最高的作者
select author,avg(price) from book group by author order by avg(price) desc limit

select avg(price) as p ,author from book group by author order by p desc limit 1;

select max(p) from
(select avg(price) as p from book as e group by author)
as list;

9.找出最新出版的图书的作者和出版社
select author , publish ,max(publish_date) from book;


10.显示各出版社出版的所有图书
 select publish,  group_concat(book_name) as books from book group by publish;


11.查找价格最高的图书，并将它的价格修改为50元

select bid ,max(price) from book;
update book set price = 50 where bid =1;

# #
update book set price = 50 where price in (select p.p from (select max(price) p  from book) as p);

12.删除价格最低的那本书对应的数据
select  bid ,min(price) from book;
delete from book where bid = 7;

13.将所有alex写的书作业修改成alexsb
update book set book_name = 'alexsb' where author = 'alex';

14.select year(publish_date) from book;


delete from book where year(publish_date)=2017;

15.
