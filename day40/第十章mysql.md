## 第十章数据库

### 10.1 引言

1. 数据库

   - 很多功能如果只是通过操作文件来改变数据是非常繁琐的
   - 程序员需要做很多事情
   - 对于多台机器或者多个进程操作用一份数据
   - 程序员自己解决并发和安全问题比较麻烦
   - 自己处理一些数据备份，容错的措施

2. 为了工作方便,引入了数据库

   -  数据库 是一个可以在一台机器上独立工作的，并且可以给我们提供高效、便捷的方式对数据进行增删改查的一种工具。

3. 数据库的优势

   ```python
     1.程序稳定性 ：这样任意一台服务所在的机器崩溃了都不会影响数据和另外的服务。
   
   　　2.数据一致性 ：所有的数据都存储在一起，所有的程序操作的数据都是统一的，就不会出现数据不一致的现象
   
   　　3.并发 ：数据库可以良好的支持并发，所有的程序操作数据库都是通过网络，而数据库本身支持并发的网络操作，不需要我们自己写socket
   
   　　4.效率 ：使用数据库对数据进行增删改查的效率要高出我们自己处理文件很多
   ```

4. 数据库是一个C/S架构的 操作数据文件的一个管理软件
   1. 帮助我们解决并发问题
   2. 能够帮助我们用更简单更快速的方式完成数据的增删改查
   3. 能够给我们提供一些容错、高可用的机制
   4. 权限的认证
   
5. 数据库管理系统DBMS
   - 文件夹 --数据库database   db
   - 数据库管理员 --  DBA

#### 10.1.1分类

1. 关系型数据库
   - sql server
   - oracle 收费、比较严谨、安全性比较 高
             国企 事业单位
             银行 金融行业
   - mysql  开源的
             小公司
             互联网公司
   - sqllite 
2. 非关系型数据库
   - redis
   - mongodb

#### 10.1.2  sql 语言

1. ddl 定义语言
   - 创建用户
     - create user '用户名'@'%'  表示网络可以通讯的所有ip地址都可以使用这个用户名
     - create user '用户名'@'192.168.12.%' 表示192.168.12.0网段的用户可以使用这个用户名
     - create user '用户名'@'192.168.12.87' 表示只有一个ip地址可以使用这个用户名
   - 创建库
     - create  database day38;
   - 创建表
     - create table 表名(字段名 数据类型(长度)，字段名 数据类型(长度)，)
2. dml 操作语言
   1. 数据的
      - 增 insert into
      - 删 delete from
      - 改 update
      - 查 select
   2. select user(); 查看当前用户
      - select database(); 查看当前所在的数据库
      - show
      - show databases:  查看当前的数据库有哪些
      - show tables；查看当前的库中有哪些表
      - desc 表名；查看表结构
      - use 库名；切换到这个库下
3. dcl 控制语言
   1. 给用户授权
      - grant select on 库名.* to '用户名'@'ip地址/段' identified by '密码'
      - grant select,insert
      - grant all

### 10. 2MySql的安装和配置

#### 10.2.1 mysql的CS架构

1. mysqld install  安装数据库服务
2. net start mysql 启动数据库的server端
   - 停止server net stop mysql
3. 客户端可以是python代码也可以是一个程序
           mysql.exe是一个客户端
           mysql -u用户名 -p密码
4. mysql中的用户和权限
       在安装数据库之后，有一个最高权限的用户root
5. mysql -h192.168.12.87 -uroot -p123
6. 我们的mysql客户端不仅可以连接本地的数据库
   - 也可以连接网络上的某一个数据库的server端

#### 10.2.2 mysql 的 一些命令行

1. `mysql>select user();`
       查看当前用户是谁

2. `mysql>set password = password('密码')`
       设置密码

3. 创建用户
   `create user 's21'@'192.168.12.%' identified by '123';`
   `mysql -us21 -p123 -h192.168.12.87`

4. 授权
       `grant all on day37.* to 's21'@'192.168.12.%';`
       授权并创建用户
      ` grant all on day37.* to 'alex'@'%' identified by '123';`

5. 查看文件夹
       `show databases;`
   创建文件夹
       `create databases day37;`

6. DROP TABLE ：删除表

7. 库 表 数据
       创建库、创建表  DDL数据库定义语言
       存数据，删除数据,修改数据,查看  DML数据库操纵语句
       grant/revoke  DCL控制权限
   库操作
       `create database 数据库名; ` 创建库
       `show databases;` 查看当前有多少个数据库
      ` select database();`查看当前使用的数据库
       `use 数据库的名字; `切换到这个数据库(文件夹)下
   表操作
       查看当前文件夹中有多少张表
           `show tables;`
       创建表
          ` create table student(id int,name char(4));`
       删除表
           `drop table student;`
       查看表结构
           `desc 表名;`

   操作表中的数据
       数据的增加
          ` insert into student values (1,'alex');`
         `  insert into student values (2,'wusir');`
       数据的查看
          ` select * from student;`
       修改数据
          ` update 表 set 字段名=值`
         `  update student set name = 'yuan';`
          ` update student set name = 'wusir' where id=2;`
       删除数据
           `delete from 表名字;`
           `delete from student where id=1;`

#### 10.2.3 mysql的存储引擎

1. myisam ：适合做读 插入数据比较频繁的，对修改和删除涉及少的，不支持事务、行级锁和外键。有表级锁,索引和数据分开存储的，mysql5.5以下默认的存储引擎
2. innodb ：适合并发比较高的，对事务一致性要求高的， 相对更适应频繁的修改和删除操作，有行级锁，外键且支持事务, 索引和数据是存在一起的，mysql5.6以上默认的存储引擎
3. memory ：数据存在内存中，表结构存在硬盘上，查询速度快，重启数据丢失 . 

### 10.3 mysql 表的操作

#### 10.3.1 存储引擎

1. 表的存储方式
   1. 存储方式1：MyISAM5.5以下默认存储方式
      - 存储的文件个数：表结构、表中的数据、索引
      - 支持表级锁
      - 不支持行级锁不支持事务不支持外键
   2. 存储方式2：InnoDB5.6以上默认存储方式
      - 存储的文件个数：表结构、表中的数据
      
      - 支持行级锁、支持表锁
      
      - 支持事务
      
      - 支持外键
      
        事务
      
        ![事务](D:\md_down_png\事务.png)
   3. 存储方式3：MEMORY内存
      - 存储的文件个数：表结构
      - 优势：增删改查都很快
      - 劣势：重启数据消失、容量有限

#### 10.3.2 表相关命令

1. 查看配置项:
   `show variables like '%engine%';`
2. 创建表
   ` create table t1 (id int,name char(4));`
   ` create table t2 (id int,name char(4)) engine=myisam;`
   ` create table t3 (id int,name char(4)) engine=memory;`
3. 查看表的结构
       `show create table 表名; `能够看到和这张表相关的所有信息
      ` desc 表名  `             只能查看表的字段的基础信息
           describe 表名

#### 10.3.3为什么用mysql数据库

1. 用什么数据库 ： mysql

   版本是什么 ：5.6
   都用这个版本么 ：不是都用这个版本
   存储引擎 ：innodb
   为什么要用这个存储引擎：
       支持事务 支持外键 支持行级锁
                            能够更好的处理并发的修改问题

#### 10.3.4 表的 数据类型

1. 数值型
   - 整数 int
     `create table t4 (id1 int(4),id2 int(11));`
   - 注意 : -->int默认是有符号的
     - 它能表示的数字的范围不被宽度约束
     - 它只能约束数字的显示宽度
     - `create table t5 (id1 int unsigned,id2 int);`
   - 小数  float
     `create table t6 (f1 float(5,2),d1 double(5,2));`
     `create table t7 (f1 float,d1 double);`
     `create table t8 (d1 decimal,d2 decimal(25,20));`

2. 日期时间

   year 年 ,范围>>>>1901/2155

   date 年月日,范围>>>1000-01-01/9999-12-31

   time 时分秒,

   `datetime、timestamp`  年月日时分秒,前者>>>1000-01-01 00:00:00/9999-12-31 23:59:59,后者>>>1970-01-01 00:00:00/2038

   - `create table t9(
     y year,d date,
     dt datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
     ts timestamp);`

3. 字符串

   char(15)  定长的单位
       alex  alex
       alex
   varchar(15) 变长的单位
       alex  alex4

   哪一个存储方式好？
       varchar ：节省空间、存取效率相对低
       char ：浪费空间，存取效率相对高 长度变化小的

4. enum 和 set型

   enum枚举型,ENUM只允许从值集合中选取单个值，而不能一次取多个值。

   SET和ENUM非常相似，也是一个字符串对象，里面可以包含0-64个成员。根据成员的不同，存储上也有所不同。set类型可以**允许值集合中任意选择1或多个元素进行组合**。对超出范围的内容将不允许注入，而对重复的值将进行自动去重。

   `create table t12(
   name char(12),
   gender ENUM('male','female'),
   hobby set('抽烟','喝酒','烫头','洗脚')
   );`

   `insert into t12 values('alex','不详','抽烟,喝酒,洗脚,洗脚,按摩');`

   `insert into teacher values(1, '波多');`

#### 10.3.5 总结

```python
数据类型
    数字类型 ：int，float(5,2)
    字符串类型 ：char(10)，varchar(10)
    时间类型：datetime，date，time
    enum和set：enum(单选项1,单选项2)，set(多选项1，多选项2)

create table 表名(
字段名 数据类型(宽度/选项)，
字段名 数据类型(宽度/选项)，
)

创建库
create database day39
查看有哪些库
show databases;
查看当前所在的数据库
select database();
切换库
use 库名
```

### 10.4表的约束 和 修改

为了防止不符合规范的数据进入数据库，在用户对数据进行插入、修改、删除等操作时，DBMS自动按照一定的约束条件对数据进行监测，使不符合规范的数据不能进入数据库，以确保数据库中存储的数据正确、有效、相容。 

1. unsigned  设置某一个数字无符号(指没有负号,也就是负数)

2. not null 某一个字段不能为空

3. default 给某个字段设置默认值

   ```python
   我们约束某一列不为空，如果这一列中经常有重复的内容，就需要我们频繁的插入，这样会给我们的操作带来新的负担，于是就出现了默认值的概念。
   默认值，创建列时可以指定默认值，当插入数据时如果未主动设置，则自动添加默认值
   ```

   严格模式设置

   ```python
   设置严格模式：
       不支持对not null字段插入null值
       不支持对自增长字段插入”值
       不支持text字段有默认值
   
   直接在mysql中生效(重启失效):
   mysql>set sql_mode="STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION";
   
   配置文件添加(永久失效)：
   sql-mode="STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION"
   ```

4. unique  设置某一个字段不能重复,指定某列或者几列组合不能重复
   联合唯一

   ```python
   create table service(
   id int primary key auto_increment,
   name varchar(20),
   host varchar(15) not null,
   port int not null,
   unique(host,port) #联合唯一
   );
   
   mysql> insert into service values
       -> (1,'nginx','192.168.0.10',80),
       -> (2,'haproxy','192.168.0.20',80),
       -> (3,'mysql','192.168.0.30',3306)
       -> ;
   Query OK, 3 rows affected (0.01 sec)
   Records: 3  Duplicates: 0  Warnings: 0
   
   mysql> insert into service(name,host,port) values('nginx','192.168.0.10',80);
   ERROR 1062 (23000): Duplicate entry '192.168.0.10-80' for key 'host'.
   # 即(host,port),相当于一个元祖,合在一起不能重复
   ```

5. auto_increment 设置某一个int类型的字段 自动增加

   **注意**:>>1. auto_increment自带not null效果 ////  2.  设置条件 int  unique ///3. 自增字段 必须是数字 且 必须是唯一的

6. primary key    设置某一个字段非空且不能重复,(主键)

   **注意**:>>>1. 约束力相当于 not null + unique ///2. 一张表只能由一个主键///3. 会将第一个设置

   ```python
   # 多字段主键
   create table service(
   ip varchar(15),
   port char(5),
   service_name varchar(10) not null,
   primary key(ip,port)
   );
   
   
   mysql> desc service;
   +--------------+-------------+------+-----+---------+-------+
   | Field        | Type        | Null | Key | Default | Extra |
   +--------------+-------------+------+-----+---------+-------+
   | ip           | varchar(15) | NO   | PRI | NULL    |       |
   | port         | char(5)     | NO   | PRI | NULL    |       |
   | service_name | varchar(10) | NO   |     | NULL    |       |
   +--------------+-------------+------+-----+---------+-------+
   3 rows in set (0.00 sec)
   
   mysql> insert into service values
       -> ('172.16.45.10','3306','mysqld'),
       -> ('172.16.45.11','3306','mariadb')
       -> ;
   Query OK, 2 rows affected (0.00 sec)
   Records: 2  Duplicates: 0  Warnings: 0
   
   mysql> insert into service values ('172.16.45.10','3306','nginx');
   ERROR 1062 (23000): Duplicate entry '172.16.45.10-3306' for key 'PRIMARY'
   ```

   1. ```python
      # 一张表只能设置一个主键
      # 一张表最好设置一个主键
      # 约束这个字段 非空（not null） 且 唯一（unique）
      
      
      ############################
      # create table t6(
      #     id int not null unique,     # 你指定的第一个非空且唯一的字段会被定义成主键
      #     name char(12) not null unique
      # )
      ```

   2. 联合主键  : # primary key(字段1，字段2)

7. key    外键

   1. references

   2. foreign key(本表字段-外键名) references 外表名(外表字段)；

   3. 外键关联的那张表中的字段必须 unique

   4. 级联删除和更新

      ```python
      # 外键 foreign key 涉及到两张表
      # 员工表
      create table staff(
      id  int primary key auto_increment,
      age int,
      gender  enum('male','female'),
      salary  float(8,2),
      hire_date date,
      post_id int,
      foreign key(post_id) references post(pid)
      );
      
      
      #  部门表
       # pid postname post_comment post_phone
      create table post(
          pid  int  primary key,
          postname  char(10) not null unique,
          comment   varchar(255),
          phone_num  char(11)
      );
      
      update post set pid=2 where pid = 1;
      delete from post where pid = 1;
      
      # 级联删除和级联更新
      create table staff2(
      id  int primary key auto_increment,
      age int,
      gender  enum('male','female'),
      salary  float(8,2),
      hire_date date,
      post_id int,
      foreign key(post_id) references post(pid) 
      on delete cascade     # 级连删除
      on update cascade     # 级连更新
      );
      
      
      # 删父表department，子表employee中对应的记录跟着删
      # 更新父表department，子表employee中对应的记录跟着改
      
      ##########################################
         . cascade方式
      在父表上update/delete记录时，同步update/delete掉子表的匹配记录 
      
         . set null方式
      在父表上update/delete记录时，将子表上匹配记录的列设为null
      要注意子表的外键列不能为not null  
      
         . No action方式
      如果子表中有匹配的记录,则不允许对父表对应候选键进行update/delete操作  
      
         . Restrict方式
      同no action, 都是立即检查外键约束
      
         . Set default方式
      父表有变更时,子表将外键列设置成一个默认的值 但Innodb不能识别
      ```

#### 10.4.2 表的修改(alter)

```python
# alter table 表名 add 添加字段
# alter table 表名 drop 删除字段
# alter table 表名 modify 修改已经存在的字段  的类型 宽度 约束
# alter table 表名 change 修改已经存在的字段  的类型 宽度 约束 和 字段名字

# alter table 表名 add 字段名 数据类型(宽度) 约束 first/after name
# alter table 表名 drop 字段名
# alter table 表名 modify name varchar(12) not null
# alter table 表名 change name new_name varchar(12) not null

# id name age
# alter table 表名 modify age int not null after id;
# alter table 表名 modify age int not null first;
```

#### 10.4.3 表的关系

```python
多对一   foreign key  永远是在多的那张表中设置外键
    # 多个学生都是同一个班级的
    # 学生表 关联 班级表
    # 学生是多    班级是一
# 一对一   foreign key +unique  # 后出现的后一张表中的数据 作为外键，并且要约束这个外键是唯一的
    # 客户关系表 ： 手机号码  招生老师  上次联系的时间  备注信息
    # 学生表 ：姓名 入学日期 缴费日期 结业
# 多对多  产生第三张表，把两个关联关系的字段作为第三张表的外键
    # 书
    # 作者

    # 出版社
    # 书
```

#### 10.4.4 表关系的写法

1. 多对一

   ```python
   foreign key(多) references 表(一)
       # 多个学生对应一个班级
       # 多个员工对应一个部门
       # 多本书对应一个作者
       # 多个商品对应一个店铺、订单
   ```

2. 一对一(外键必须唯一)

   ```
       # 一对一
           # 后一 类型 unique
           # foreign key(后一) references 表(先一)
                 # 一个客户对应一个学生 学生表有外键
                 # 一个商品 有一个商品详情 详情页中有外键
   ```

3. 多对多

   - 第三张表(前2张表没有任何关联)

   - ```python
     # foreign key(外键名1) references 表1(主键)
             # foreign key(外键名2) references 表2(主键)
                 # 多个学生对应一个班级，多个班级对应一个学生
                 # 多个员工对应一个部门，多个部门对应一个员工
                 # 多本书对应一个作者，多个作者对应一本书
                 # 多个商品对应一个店铺、订单，多个店铺对应一个商品
                 # 一个学生学习多门课程，一门课程被多个学上学习
     ```

   ```python
   # 作者与书多对多
   # create table author(
   # aid primary key auto_increment,
   # name char(12) not null,
   # birthday date,
   # gender enum('male','female') default 'male'
   # )
   
   # create table book(
   # id  int  primary key,
   # name char(12) not null,
   # price float(5,2)
   # )
   
   # create table book_author(
   # id int primary key auto_increment,
   # book_id int not null,
   # author_id int not null,
   # foreign key(book_id) references book(id),
   # foreign key(author_id) references author(aid),
   # );
   ```

   

4. ![多对多](D:\md_down_png\多对多.png)

### 10.5 单表查询

#### 10.5.1 单表查询语法

`SELECT DISTINCT 字段1,字段2... FROM 表名
                              WHERE 条件
                              GROUP BY field
                              HAVING 筛选
                              ORDER BY field
                              LIMIT 限制条数`

执行的优先级为`from,where,group by,select,distinct,having,order by,limit`

**1.找到表:from**

**2.拿着where指定的约束条件，去文件/表中取出一条条记录**

**3.将取出的一条条记录进行分组group by，如果没有group by，则整体作为一组**

**4.执行select（去重）**

**5.将分组的结果进行having过滤**

**6.将结果按条件排序：order by**

**7.限制结果的显示条数**

#### 10.5.2 简单查询

```python
select * (字段名) from `table` ;
SELECT DISTINCT post FROM employee;   (dintinct去重),可以对数值型数据进行+-*/运算
```

1. concat(定义显示格式)
   `select concat('名字',name,"年龄",age) from table`

   `select concat_ws('<>',name,age) from table`     连接符,

##### 10.5.2.1 where 语句

1. 比较运算符：> < >= <= <> !=

2. between 80 and 100 值在80到100之间

3. in(80,90,100) 值是80或90或100

4. like 'e%'
    通配符可以是%或_，
    %表示任意多字符
    _表示一个字符 
    
    ```python
        通配符’%’
        SELECT * FROM employee 
                WHERE emp_name LIKE 'eg%';
    
        通配符’_’
        SELECT * FROM employee 
                WHERE emp_name LIKE 'al__';
    ```
    
5. 逻辑运算符：在多个条件直接可以使用逻辑运算符 and or not

6. 判断某个字段是否为null,必须用`is    not  is`

7. 使用正则查询

   ```python
   SELECT * FROM employee WHERE emp_name REGEXP '^ale';
   
   SELECT * FROM employee WHERE emp_name REGEXP 'on$';
   
   SELECT * FROM employee WHERE emp_name REGEXP 'm{2}';  # regexp
   
   select * from employee where emp_name regexp '^jin.*[gn]$';
   
   
   小结：对字符串匹配的方式
   WHERE emp_name = 'egon';
   WHERE emp_name LIKE 'yua%';
   WHERE emp_name REGEXP 'on$';
   ```

##### 10.5.2.2 group  by分组

```python
# select * from employee group by post
# 会把在group by后面的这个字段，也就是post字段中的每一个不同的项都保留下来
# 并且把值是这一项的的所有行归为一组

单独使用GROUP BY关键字分组
    SELECT post FROM employee GROUP BY post;
    注意：我们按照post字段分组，那么select查询的字段只能是post，想要获取组内的其他相关信息，需要借助函数

GROUP BY关键字和GROUP_CONCAT()函数一起使用
    SELECT post,GROUP_CONCAT(emp_name) FROM employee GROUP BY post;#按照岗位分组，并查看组内成员名
    SELECT post,GROUP_CONCAT(emp_name) as emp_members FROM employee GROUP BY post;

GROUP BY与聚合函数一起使用
    select post,count(id) as count from employee group by post;#按照岗位分组，并查看每个组有多少人
```

##### 10.5.2.3 聚合 语函数(count ,sum ,max , min , avg)

```python
#强调：聚合函数聚合的是组的内容，若是没有分组，则默认一组

示例：
    SELECT COUNT(*) FROM employee;
    SELECT COUNT(*) FROM employee WHERE depart_id=1;
    SELECT MAX(salary) FROM employee;
    SELECT MIN(salary) FROM employee;
    SELECT AVG(salary) FROM employee;
    SELECT SUM(salary) FROM employee;
    SELECT SUM(salary) FROM employee WHERE depart_id=3;
```

##### 10.5.2.4 having (过滤,筛选)

```
#！！！执行优先级从高到低：where > group by > having 
#1. Where 发生在分组group by之前，因而Where中可以有任意字段，但是绝对不能使用聚合函数。
#2. Having发生在分组group by之后，因而Having中可以使用分组的字段，无法直接取到其他字段,可以使用聚合函数
```

```python
# 总是根据会重复的项来进行分组
# 分组总是和聚合函数一起用 最大 最小 平均 求和 有多少项

# 1.执行顺序 总是先执行where 再执行group by分组
#   所以相关先分组 之后再根据分组做某些条件筛选的时候 where都用不上
# 2.只能用having来完成

# 平均薪资大于10000的部门
# select post from employee group by post having avg(salary) > 10000
```

##### 10.5.2.5 order by 查询排序 

默认从小到大(日期从小到大则是年份最小的,则里现在最远) >>>>asc

```
# order by
    # order by 某一个字段 asc;  默认是升序asc 从小到大
    # order by 某一个字段 desc;  指定降序排列desc 从大到小
    # order by 第一个字段 asc,第二个字段 desc;
        # 指定先根据第一个字段升序排列，在第一个字段相同的情况下，再根据第二个字段排列
```

```
按单列排序
    SELECT * FROM employee ORDER BY salary;
    SELECT * FROM employee ORDER BY salary ASC;
    SELECT * FROM employee ORDER BY salary DESC;

按多列排序:先按照age排序，如果年纪相同，则按照薪资排序
    SELECT * from employee
        ORDER BY age,
        salary DESC;
```

##### 10.5.2.6  limit 限制

```
# limit
    # 取前n个  limit n   ==  limit 0,n
        # 考试成绩的前三名
        # 入职时间最晚的前三个
    # 分页    limit m,n   从m+1开始取n个
    # 员工展示的网页
        # 18个员工
        # 每一页展示5个员工
    # limit n offset m == limit m,n  从m+1开始取n个
```

### 10.6 多表查询

#### 10.6.1  两张表连在一起查询(推荐)

```python
select * from emp,department;
```

#### 10.6.2 连表查询(推荐使用,效率更高)

```
# 连表查询
    # 把两张表连在一起查
    # 内链接 inner join   两张表条件不匹配的项不会出现再结果中
    # select * from emp inner join department on emp.dep_id = department.id;
    # 外连接
        # 左外连接 left join  永远显示全量的左表中的数据
        # select * from emp left join department on emp.dep_id = department.id;
        # 右外连接 right join 永远显示全量的右表中的数据
        # select * from emp right join department on emp.dep_id = department.id;
        # 全外连接
        # select * from emp left join department on emp.dep_id = department.id
        # union
        # select * from department right join emp  on emp.dep_id = department.id;
```

1. 连接的语法

   ```
   # 连接的语法
   # select 字段 from 表1 xxx join 表2 on 表1.字段 = 表2.字段;
       # 常用
       # 内链接
       # 左外链接
       
       
   # 找技术部门的所有人的姓名
       # select * from emp inner join department on emp.dep_id = department.id;
   # +----+-----------+--------+------+--------+------+--------------+
   # | id | name      | sex    | age  | dep_id | id   | name         |
   # +----+-----------+--------+------+--------+------+--------------+
   # |  1 | egon      | male   |   18 |    200 |  200 | 技术         |
   # |  2 | alex      | female |   48 |    201 |  201 | 人力资源     |
   # |  3 | wupeiqi   | male   |   38 |    201 |  201 | 人力资源     |
   # |  4 | yuanhao   | female |   28 |    202 |  202 | 销售         |
   # |  5 | liwenzhou | male   |   18 |    200 |  200 | 技术         |
   # +----+-----------+--------+------+--------+------+--------------+
   # select * from emp inner join department on emp.dep_id = department.id where department.name = '技术'
   # select emp.name from emp inner join department d on emp.dep_id = d.id where d.name = '技术'
   
   # 找出年龄大于25岁的员工以及员工所在的部门名称
   # select emp.name,d.name from emp inner join department as d on emp.dep_id = d.id where age>25;
   
   # 根据age的升序顺序来连表查询emp和department
   # select * from emp inner join department as d on emp.dep_id = d.id order by age;
   ```

#### 10.6.3 子查询

```python
# 子查询
        # 找技术部门的所有人的姓名
        # 先找到部门表技术部门的部门id
        # select id from department where name = '技术';
        # 再找emp表中部门id = 200
        # select name from emp where dep_id = (select id from department where name = '技术');

        # 找到技术部门和销售部门所有人的姓名
        # 先找到技术部门和销售部门的的部门id
        # select id from department where name = '技术' or name='销售'
        # 找到emp表中部门id = 200或者202的人名
        # select name from emp where dep_id in (select id from department where name = '技术' or name='销售');
        # select emp.name from emp inner join department on emp.dep_id = department.id where department.name in ('技术','销售');

```

### 10.7 表总结

增加 insert
删除 delete
修改 update
查询 select

1. 增加 insert
   insert into 表名 values (值....)
       所有的在这个表中的字段都需要按照顺序被填写在这里
   insert into 表名(字段名，字段名。。。) values (值....)
       所有在字段位置填写了名字的字段和后面的值必须是一一对应
   insert into 表名(字段名，字段名。。。) values (值....),(值....),(值....)
       所有在字段位置填写了名字的字段和后面的值必须是一一对应

   ```python
   value单数            values复数
   一次性写入一行数据   一次性写入多行数据
   
   t1 id,name,age
   insert into t1 value (1,'alex',83)
   insert into t1 values (1,'alex',83),(2,'wusir',74)
   
   insert into t1(name,age) value ('alex',83)
   insert into t1(name,age) values ('alex',83),('wusir',74)
   
   #################################
   第一个角度
       写入一行内容还是写入多行
       insert into 表名 values (值....)
       insert into 表名 values (值....)，(值....)，(值....)
   
   第二个角度
       是把这一行所有的内容都写入
       insert into 表名 values (值....)
       指定字段写入
       insert into 表名(字段1，字段2) values (值1，值2)
   ```

2. 删除 delete
       delete from 表 where 条件;

3. 更新 update
       update 表 set 字段=新的值 where 条件；

4. 查询
       select语句
           select * from 表
           select 字段,字段.. from 表
           select distinct 字段,字段.. from 表  按照查出来的字段去重
           select 字段*5 from 表  按照查出来的字段去重
           select 字段  as 新名字,字段 as 新名字 from 表  按照查出来的字段去重
           select 字段 新名字 from 表  按照查出来的字段去重
   
   ​		select concat(''名字",name,'薪水',sralry) as auully from employee;
   
   select concat_ws('分割符",name,salary,post) from employee;
   
   连表查询>>>子查询

### 10.8  pymysql 模块的使用

#### 10.8.1 安装

1. 先安装  `pip install pymysql`

2. 导入并使用

   ```python
   import pymysql
   
   conn = pymysql.connect(host='127.0.0.1', user='root', password="123",
                    database='day40')
   cur = conn.cursor()   # 获取数据库操作符 游标
   cur.execute('insert into employee(emp_name,sex,age,hire_date) '
               'values ("郭凯丰","male",40,20190808)')
   cur.execute('delete from employee where id = 18')
   conn.commit()
   conn.close()
   
   
   # ##############查询
   conn = pymysql.connect(host='127.0.0.1', user='root', password="123",
                    database='day40')
   cur = conn.cursor(pymysql.cursors.DictCursor)   # 数据库操作符 游标>>>得到的结果转为字典
   cur.execute('select * from employee '
               'where id > 10')
   ret = cur.fetchone()      # 一行数据
   print(ret['emp_name'])
   ret = cur.fetchmany(5)
   ret = cur.fetchall()
   print(ret)
   conn.close()
   
   ```

   

