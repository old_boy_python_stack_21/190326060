# -*- coding:utf-8 -*-
import json

from flask import Flask, request, render_template, make_response
from flask_session import Session

from geventwebsocket.handler import WebSocketHandler  # 处理 http请求 和 websocket 请求
from geventwebsocket.server import WSGIServer  # 替换Flask原生的WSGI服务
from geventwebsocket.websocket import WebSocket  # 语法提示使用

app = Flask(__name__)
socket_dict = {}


@app.after_request
def logger(response):
    app.logger.error('%s %s url: %s' % (request.method, response._status, request.url))
    return response


@app.route('/chat_room')
def chat_room():
    return render_template('w2s.html')


@app.route('/ws/<username>')
def get_chat(username):
    web_socket_link = request.environ.get('wsgi.websocket', None) # type:WebSocket
    # print(web_socket_link)
    if not web_socket_link:
        ret = make_response('请使用websocket请求', 404)
        ret.headers['error'] = 'Request error'
        return ret

    socket_dict[username] = web_socket_link

    while 1:
        msg = web_socket_link.receive()

        msg_dict = json.loads(msg)
        receiver = msg_dict.get("receiver")
        receiver_socket = socket_dict.get(receiver)

        receiver_socket.send(msg)


if __name__ == '__main__':
    http_server = WSGIServer(('0.0.0.0', 8848), application=app, handler_class=WebSocketHandler)
    http_server.serve_forever()
