# -*- coding:utf-8 -*-
# __author__ = Deng Jack

import base64
import hashlib
def get_accept(key):
    GUID = "258EAFA5-E914-47DA-95CA-C5AB0DC85B11"
    accept = base64.b64encode(
        hashlib.sha1((key + GUID).encode("latin-1")).digest()
    ).decode("latin-1")
    return accept