# -*- coding:utf-8 -*-
# __author__ = Deng Jack

from flask import Flask , Markup
from Config import DebugConfig

from serv.content import cbp
from serv.user import ubp




app = Flask(__name__)
app.config.from_object(DebugConfig)
app.register_blueprint(cbp)
app.register_blueprint(ubp)

@app.route('/')
def index():
    return Markup("<h1>Server Is Running</h1>")


if __name__ == '__main__':
    app.run('192.168.11.18',8888)