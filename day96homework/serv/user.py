from flask import Blueprint, jsonify, request
from Config import AI_TOY
from bson import ObjectId

ubp = Blueprint("user", __name__)


@ubp.route("/reg", methods=['post'])
def reg():
    """{
	"username":username,
	"password":password,
	"nickname":nickname,
	"gender":gender,1 女 2 男
	"avatar":avatar.jpg
}"""
    default = {'1': 'mama.jpg', '2': 'baba,jpg'}
    user_info = request.form.to_dict()
    if not user_info.get('avatar'):
        user_info['avatar'] = default[user_info['gender']]
    user_info['bind_toys'] = '5ca17f85ea512d215cd9b079'
    user_info['friend_list'] = []

    AI_TOY.Users.insert_one(user_info)
    response_data = {
        "CODE": 0,
        "MSG": "注册成功",
        "data": {}
    }
    user_info.pop('password')
    user_info['_id'] = str(user_info['_id'])
    response_data['data'] = user_info
    return response_data


@ubp.route("/login", methods=['post'])
def login():
    user_info = request.form.to_dict()
    res = AI_TOY.Users.find_one(user_info, {'password': 0})
    response_data = {
        "CODE": 0,
        "MSG": "登录成功",
        "DATA":
            {}
    }
    if res:
        res['_id'] = str(res['_id'])
        response_data['DATA'] = res
    response_data['CODE'] = 1
    response_data['MSG'] = "登录失败"
    return response_data


@ubp.route("/auto_login", methods=['post'])
def auto_login():
    user_info = request.form.to_dict()
    user_info['_id'] = ObjectId(user_info['_id'])
    res = AI_TOY.Users.find_one(user_info, {'password': 0})
    response_data = {
        "CODE": 0,
        "MSG": "登录成功",
        "DATA":
            {}
    }
    if res:
        res['_id'] = str(res['_id'])
        response_data['DATA'] = res
        return response_data
    response_data['CODE'] = 1
    response_data['MSG'] = "登录失败"
    return response_data


# 用于App扫描Toy对应二维码进行识别
@ubp.route('/scan_qr', methods=['post'])
def scan_qr():
    user_info = request.form.to_dict()
    res = AI_TOY.Devices.find_one(user_info)
    print(user_info)
    if not res:  # 二维码扫描失败,扫描的条码不是设备库中存在的
        return {
            "CODE": 0,
            "MSG": "请扫描玩具二维码",
            "DATA": {}
        }
    # 验证该设备是否已经绑定
    ret = AI_TOY.Toys.find_one(user_info)
    if ret:
        # 该玩具已经绑定了
        return {
            "CODE": 2,
            "MSG": "设备已经进行绑定",
            "DATA":
                {
                    "toy_id": str(ret.get('_id'))
                }}

    # 扫描成功并且设备未进行绑定
    return {
        "CODE": 0,
        "MSG": "二维码扫描成功",
        "DATA":
            {
                "device_key": user_info.get('device_key')
            }
    }


#  App绑定设备接口:
@ubp.route('/bind_toy', methods=['post'])
def bind_toy():
    user_info = request.form.to_dict()

    return {
	"code":0,
	"msg":"绑定完成",
	"data":{}
}