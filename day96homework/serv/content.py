# -*- coding:utf-8 -*-
# __author__ = Deng Jack
import os

import requests
from flask import Blueprint, jsonify, send_file
from Config import AI_TOY, COVER_PATH, MUSIC_PATH, AVATAR_PATH, QR_URl, DEVICE_SCAN_PATH

cbp = Blueprint('content', __name__)


@cbp.route('/content_list', methods=['post'])
def content_list():
    """{


	"data":
	[
		{
            "_id" : "5c8f58eb268d79173c97bac5",
            "music" : "21b61cc0-9292-4e51-bf58-72be8ee6f962.mp3",
            "cover" : "21b61cc0-9292-4e51-bf58-72be8ee6f962.jpg",
            "title" : "一只哈巴狗"
		},
		{
			"_id" : "5c8f58eb268d79173c97bac4",
            "music" : "aa523ebe-95f9-4641-9478-1663cc74c6a6.mp3",
            "cover" : "aa523ebe-95f9-4641-9478-1663cc74c6a6.jpg",
            "title" : "学习雷锋好榜样"
		}
	]
}"""
    content = {
        "code": 0,
        "msg": "获取内容资源列表",
        "data": []
    }
    ret = AI_TOY.Content.find()
    res = map(process_data, ret)
    content["data"] = list(res)
    return jsonify(content)


@cbp.route('/get_cover/<filename>', methods=['get'])
def get_cover(filename):
    filepath = os.path.join(COVER_PATH, filename)
    return send_file(filepath)


@cbp.route('/get_music/<musicname>', methods=['get'])
def get_music(musicname):
    filepath = os.path.join(MUSIC_PATH, musicname)
    return send_file(filepath)


@cbp.route('/avatar/<avatarname>', methods=['get'])
def avatar(avatarname):
    filepath = os.path.join(AVATAR_PATH, avatarname)
    return send_file(filepath)


@cbp.route('/get_qr/<scanname>', methods=['get'])
def get_qr(scanname):
    device_scan_file = os.path.join(DEVICE_SCAN_PATH, scanname)
    return send_file(device_scan_file)

# utils
def process_data(item):
    item['_id'] = str(item['_id'])
    return item


def get_scan_qr(device_key):
    """
    :param device_key: 设备的id ,唯一
    :return: 在当期目录的device_scan下存放以设备的id为名称的jpg二维码图片
    """
    params = {
        'text': device_key
    }
    """
        http://qr.liantu.com/api.php?&bg=ffffff&fg=cc0000&text=x
        参数	描述	赋值例子
        bg	背景颜色	bg=颜色代码，例如：bg=ffffff
        fg	前景颜色	fg=颜色代码，例如：fg=cc0000
        gc	渐变颜色	gc=颜色代码，例如：gc=cc00000
        el	纠错等级	el可用值：h\q\m\l，例如：el=h
        w	尺寸大小	w=数值（像素），例如：w=300
        m	静区（外边距）	m=数值（像素），例如：m=30
        pt	定位点颜色（外框）	pt=颜色代码，例如：pt=00ff00
        inpt	定位点颜色（内点）	inpt=颜色代码，例如：inpt=000000
        logo	logo图片	logo=图片地址，例如：logo=http://www.liantu.com/images/2013/sample.jpg
    """
    res = requests.get(QR_URl, params=params)
    device_scan_file = os.path.join(DEVICE_SCAN_PATH, device_key + '.jpg')
    with open(device_scan_file, 'wb') as fd:
        fd.write(res.content)
