# -*- coding:utf-8 -*-
# __author__ = Deng Jack

# -*- coding: utf-8 -*-
# @Time    : 2019/7/19 19:05

# -*- coding: utf-8 -*-
# @Time    : 2019/7/19 19:05
import requests
import os
import re
from bs4 import BeautifulSoup
import lxml
import json
import execjs   # 操作js代码的库

headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36',
    'Accept': 'text/html,application/xhtml+ xml,application/xml;q = 0.9,image/webp,image/apng,*/*;q=0.8, application/signe-exchange;v = b3',
    'Host': 'www.ximalaya.com'
}
content = []

'''爬取喜马拉雅服务器系统时间戳，用于生成xm-sign'''
def getxmtime():
    url="https://www.ximalaya.com/revision/time"
    response = requests.get(url, headers=headers)
    html = response.text
    return html

'''利用xmSign.js生成xm-sign'''
def exec_js():
    #获取喜马拉雅系统时间戳
    time = getxmtime()
    print(time)
    #读取同一路径下的js文件
    with open('xmSign.js',"r",encoding='utf-8') as f:
        js = f.read()

    # 通过compile命令转成一个js对象
    docjs = execjs.compile(js)
    # 调用js的function生成sign
    res = docjs.call('python',time)
    return res

"""获取专辑一共有多少页"""
def getPage():
    url = "https://www.ximalaya.com/ertong/424529/"
    html = requests.get(url,headers=headers).text
    # 创建BeautifulSoup对象
    suop = BeautifulSoup(html,'lxml')  # 实例化对象,使用lxml进行解析
    # 根据属性获取 最大页码
    max_page = suop.find("input",placeholder="请输入页码").attrs["max"]
    return max_page


response_list = []
"""请求歌曲源地址"""
def gethtml():
    # 调用exec_js函数生成xm-sign
    xm_sign = exec_js()
    # 将生成的xm-sign添加到请求投中
    headers["xm-sign"] = xm_sign
    max_page = getPage()
    for page in range(1,int(max_page)+1):
        url = "https://www.ximalaya.com/revision/play/album?albumId=424529&pageNum={}&sort=1&pageSize=30".format(page)
        # 下载
        response= requests.get(url,headers=headers).text
        response = json.loads(response)
        response_list.append(response)

"""数据持久化"""
def write_data():
    # 请求歌曲地址拿到响应数据json
    gethtml()
    for res in response_list:
        data_list = res["data"]["tracksAudioPlay"]
        for data in data_list:
            trackName = data["trackName"] # 歌名
            trackCoverPath = data["trackCoverPath"] # 封面地址
            music_path = data["src"] # url
            content.append({
                'trackName':trackName,
                'trackCoverPath':trackCoverPath,
                'music_path':music_path,
            })
    print(content)
# write_data()
write_data()