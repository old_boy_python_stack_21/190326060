# 数据库配置
from pymongo import MongoClient

MC = MongoClient()  # 默认设置
AI_TOY = MC.AI_TOY # 数据库

# app的配置
class DebugConfig:
    DEBUG = True



# 媒体文件的配置
COVER_PATH = 'D:\python\day96\Cover'
MUSIC_PATH = 'D:\python\day96\Music'
AVATAR_PATH = 'D:\python\day96\Avatar'
DEVICE_SCAN_PATH = 'D:\python\day96\Device_Scan'

# 联图二维码接口调用
QR_URl = "http://qr.liantu.com/api.php"