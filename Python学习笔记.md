# Python学习笔记

## 第一章 计算机基础

### 1.1  硬件

计算机基本的硬件由：CPU / 内存 / 主板 / 硬盘 / 网卡  / 显卡 / 显示器 等组成，只有硬件但硬件之间无法进行交流和通信。

### 1.2 操作系统

操作系统用于协同或控制硬件之间进行工作，常见的操作系统有那些:

- windows系统,应用最广泛的操作系统.
  - win xp 系统
  - win 7系统
  - win 10 系统
- linux系统,免费开源,占用内存小,运行速度快
  - centos .**公司线上服务器使用,图形界面较ubuntu差**
  - ubuntu,用于开发.图形界面较好
  - renhat,主要用于企业级服务器
- mac(苹果系统,对办公和开发都很好)

### 1.3  解释器或编译器

编程语言的开发者写的一个工具，将用户写的代码转换成010101交给操作系统去执行。

#### 1.3.1  解释和编译型语言

解释型语言就类似于： 实时翻译，代表：Python / PHP / Ruby / Perl

- 特点:写完代码交给解释器，解释器会从上到下一行行代码执行：边解释边执行。

编译型语言类似于：说完之后，整体再进行翻译，代表：C / C++ / Java / Go ...

- 特点:代码写完后，编译器将其变成成另外一个文件，然后交给计算机执行。

#### 1.3.2 学习编程语言

1. 安装解释器或编译器,工具准备.
2. 学习该语言的语法规则

### 1.4 软件（应用程序）

软件又称为应用程序，就是我们在电脑上使用的工具，类似于：记事本 / 图片查看 / 游戏.是由程序员编写的.

### 1.5 进制

对于计算机而言无论是文件存储 / 网络传输输入本质上都是：二进制（010101010101ainc），如：电脑上存储视频/图片/文件都是二进制； QQ/微信聊天发送的表情/文字/语言/视频 也全部都是二进制。

进制：

- 2进制，计算机内部。二进制数据是用0和1两个数码来表示的数。它的基数为2，进位规则是“逢二进一”

  计算机内部都是以二进制存储数据的.

- 8进制: 采用0，1，2，3，4，5，6，7八个数字，逢八进1 .

- 10进制，人来进行使用一般情况下计算机可以获取10进制，然后再内部会自动转换成二进制并操作。

- 16进制，一般用于表示二进制（用更短的内容表示更多的数据），一般是：\x 开头。

| 二进制 | 八进制 | 十进制 | 十六进制 |      |
| ------ | ------ | ------ | -------- | ---- |
| 0      | 0      | 0      | 0        |      |
| 1      | 1      | 1      | 1        |      |
| 10     | 2      | 2      | 2        |      |
| 11     | 3      | 3      | ```      |      |
| 100    | ```    | ```    | 9        |      |
|        | 7      | 8      | A        |      |
|        |        | 9      | ```      |      |
|        |        |        | F        |      |



## 第二章 Python入门

### 2.1 环境的安装

- 下载软件(官网下载)并安装软件
  - python 2.7.16 （2020年官方不在维护）
  - python 3.6.8 （推荐）

- 解释器：py2 / py3 （环境变量）,添加环境变量,以便以后快速调用程序

  ![环境变量](D:\md_down_png\环境变量.png)

- 开发工具：pycharm的安装,激活(界面的调整,防止伤眼睛)

### 2.2 编码

#### 2.2.1 编码基础

- ascii码:表示英文和标点符号,1字节表示一个字符.
- unicode码:能表示世界上所有的语言,4字节表示一个字符,现在用到了27位bit.
- utf-8码:对unicode码的压缩,中文3个字节表示.
- gbk码:亚洲地区使用,gb2312码的升级版,2字节表示中文.
- gb2312码:亚洲地区使用,2字节表示中文.

#### 2.2.2 python编码相关

对于Python默认解释器的编码：

- py2： ascii
- py3： utf-8

如果想要修改默认编码，则可以使用：

```python
# -*- coding:utf-8 -*- 
```

**注意：对于操作文件时，要按照：以什么编写写入，就要用什么编码去打开。**

在linux系统中,py的文件开头有:

```python
#!/usr/bin/env python  在Linux中指定解释器的路径
# -*- coding:utf-8 -*-
```

运行： 解释器   文件路径 

在linux上有一种特殊的执行方法：

- 给文件赋予一个可执行的权限
- ./a.py  自动去找文件的第一行 =  /usr/bin/env/python  a.py

#### 2.2.3单位换算

​	             8 bit = 1 bype

​                    1024 bype = 1 KB

​                    1024 KB = 1 MB

​                    1024 MB = 1 GB

​                    1024 GB = 1 TB

### 2.3 变量

问：为什么要有变量？

为某个值创建一个“外号”，以后在使用时候通过此外号就可以直接调用。

#### 2.3.1变量的命名规则

1. 变量名由数字,字母和下划线组成.

2. 变量名不能以数字开头

3. 变量名要避开python的关键字,如[‘and’, ‘as’, ‘assert’, ‘break’, ‘class’, ‘continue’, ‘def’, ‘del’, ‘elif’, ‘else’, ‘except’, ‘exec’, ‘finally’, ‘for’, ‘from’, ‘global’, ‘if’, ‘import’, ‘in’, ‘is’, ‘lambda’, ‘not’, ‘or’, ‘pass’, ‘print’, ‘raise’, ‘return’, ‘try’, ‘while’, ‘with’, ‘yield’]等等.

4. 建议:  见名知意:用简单明了,意思相近的单词做变量名.

   ​          单词间用下划线连接,如变量名: deng_dad.

### 2.4 python基础语句

#### 2.4.1输出/输入语句

1. 输出语句

```python
print（你想输出的内容）
```

python2中，输出是: print ”你想输出的“（注意：print和引号间有空格）

python3中，输出是: print（“你想输出的”）

2. 输入语句输入

   input语句：

   ```python
   name=input('请输入你的用户名:')
   password=input('请输入你的密码')
   print(content)
   print(password)
   ```

   注意：

   1. input语句输入得到的内容永远是字符串。
   2. python2的输入语句是:raw_input('')。
   3. python3的输入语句是;input('')。

#### 2.4.2编程的注释

编程代码一定要做注释，注释不参与代码运行,编程代码行数太多了。分为二类，如

```python
# 单行注释，不参与代码运算

"""
多行注释，
不参与程序运算
"""
```

#### 2.4.3条件判断语句

- 最简单条件判断

```python
age = input('请输入你的年龄：')
new_age=int(age)
# input输入的数据类型是字符串，需要用int语句把字符串数据转化为整型数据。
if new_age >= 18:
	print('你已经是成年了人了')
```

- 初级语句

```python
gender = input('请输入你的性别：')
# 默认不是男性就是女性
if gender == '男':
	print('走开')
else:
	print('来呀，快活呀')
```

- elif语句

```
gender = input('请输入你的性别：')
# 性别有男、女、人妖多种选择
if gender == '男':
	print('走开')
elif gender == '女':
	print('来呀，快活呀')
else：
	print（'找##去，他是gay'）
```

elif语句可以用无限次使用，如果次数过多会有其他语句使用,语句过于冗长.

- and语句，python的关键字之一，表示并且的意思。

#### 2.4.4循环语句

- while 用法

  ```python
  num = 1
  while num <= 10:
      print(num)
      num += 1
  ```

- while else 用法

  ```python
  num = 1
  while num <= 10:
      print(num)
      num += 1
  else:
      print("end")
  # else 表示while循环语句不满足组条件后执行的代码
  
  #  pass  占位符,不做任何事.
  ```

- break、continue关键字的用法,以及与if   pass语句的嵌套

#### 2.4.5运算符

- 算术运算符:加减乘除的运用，+、-、*、/ 在程序中用于算术运算。还有类似于：

  1. % ，取除法的余数，如15%4 = 3
  2. // ，取除法的商的整数，如20//6 = 3
  3. ** ，取指数的值，如2**8 = 2的8次方，为256.

- 赋值运算符：

  1. c += 1 等价于 c = c+ 1

  2. c -= 1等价于c = c - 1

  3. c *= 2 等价于 c = c * 2

     等等诸如此类

- 逻辑运算符: and 、or、not

  1. 一般用法：表示逻辑中的于、或、非，用于条件的判断

  2. 二般用法：

     - 3种数据类型的转化，int str boolen的转化

       ```python
       test=bool('')
       test1=bool(0)
       print(test)
       print(test1)
       #####   注意：只有空字符串和0转化为布尔值时为false，否则都为 true
       ```

     - ```
       value = x and y
       print(value)
       ####: 从左到右， x转化为布尔值为真，value = y，否则value= x 。
       value1 = x or y
       print(value1)
       ####: 从左到右， x转化为布尔值为真，value1 = x，否则value= x 。
       ```

     - 运算的优先顺序为： ( ) >not > and >or

- in ，逻辑运算符，判断某字符或某字符串是否在一个大的字符串中，输出得到bool型数据。

  ```python
  value = '我是中国人'
  v = '我'
  if v in value:
      print(v)
  else:
      print('出错')  #   我
  ```

  

## 第三章 数据类型

### 3.1 整型（int）

#### 3.1.1 整型的长度

py2中有：int有取值范围，对于32位系统而言：-2^31~2^31-1

​                                          对于64位系统而言：-2^63~2^63-1

​       超出范围后，py2自动转换成long(长整型)数据。

py3中有：int （int/long）只有int数据.

#### 3.1.2 整除

py2和py3中整除是不一样.

- py2:整除只保留整数,小数位会舍去.若要保留小数.则在文件头加

  ```python
  from __future__ import division
  ```

- py3整除保留所有.

### 3.2 布尔（bool）

布尔值就是用于表示真假。True和False。

其他类型转换成布尔值：只有''/0/[]/{}/()/set()转换为bool值为False,其余都是True.

### 3.3 字符串（str）

字符串是写代码中最常见的 :

- 单引号，如'王飞'
- 双引号，如”王大“
- 三引号，如“”“王小”“”，三引号支持换行。

注意：整型数据可以+和×，字符串数据也可以+和×。如

```python
name='五五开'
new_name=name*3
print（new_name）   #  '五五开五五开五五开'
```

python内存中的字符串是按照：unicode 编码存储。对于字符串是不可变数据类型。

#### 3.3.1字符串的格式化

1. 字符串格式化的意义，大部分字符过于冗长，使用字符串格式化能大大加快效率，方便程序员调用数据。

2. %s 、 %d、%%

   - ```python
     red_dad = '大红的爸爸'
     do = '教学生上课'
     thing = '%s在操场%s' %(red_dad，do,)
     print(thing)
     ```

   - 直接做占位符

     ```python
     temper = '%s在太空中%s' %('等大侠','打飞机',)
     print(temper)
     thing = '盖伦，年龄%d，喜欢在池塘里%s' %(15,'打水仗'，)
     print(thing)
     #####  #s和#d表示的类型不同，前者表示字符串数据，后者表示整型数据。
     name = '小明'
     template = "%s拿出了100%%的力气" %(name,)
     print(template)
     ######   %%，为了和字符串格式化做区分，百分号要写成%%形式。
     ```

3.  

   ```python
   name = "我叫{0},年龄:{1}".format('老男孩',73)    #  对应的是索引位置
   print(name) 
   name = '我是{x1},爱{xx}'.format_map({'x1':"邓",'xx':18})    #format_map对应的键
   print(name)
   print(name)
   
   ```

4. 格式化补充

   ```python
   # 一般的格式化
   msg = '我今年%s岁,叫做%s ,爱好%s' %(25,'邓益新','女')
   print(msg)
   
   msg1 = '我今年{}岁,叫做{} ,爱好是这个{}'.format(25,'邓益新','女')
   print(msg1 )
   ```

   字符串格式化补充

   ```python
   #  %(关键字)s
   msg = '我今年%(intt)s岁,叫做%(name)s ' %{'intt':25,'name':'邓益新'}    # 只能用字典
   print(msg)
   
   # v1 = "我是{0},年龄{1}".format('alex',19)
   v1 = "我是{0},年龄{1}".format(*('alex',19,))
   print(v1)
   
   # v2 = "我是{name},年龄{age}".format(name='alex',age=18)    #关键字传参
   v2 = "我是{name},年龄{age}".format(**{'name':'alex','age':18})
   print(v2)
   
   ###########################
   msg1 = '我今年{nain}岁,叫做{name} '.format(**{'nain':25,'name':'邓益新',})
   msg2 = '我今年{nain}岁,叫做{name}'.format(nain= 25,name= '邓益新',)
   
   ```



#### 3.3.2字符串的方法

字串自己有很多方法，如：

1. 大写： upper/isupper

   ```python
   v = 'DEng'     
   v1 = v.upper()    # DENG
   print(v1)
   v2 = v.isupper() # 判断是否全部是大写
   print(v2)  # False
   ```

2. 小写：lower/islower

   ```python
   v = 'yixin'
   v1 = v.lower()
   print(v1)
   v2 = v.islower() # 判断是否全部是小写
   print(v2)
   
   
   ############ 了解即可
   v = 'ß'  #德语大小写的转换
   # 将字符串变小写（更牛逼）
   v1 = v.casefold()
   print(v1) # ss   
   v2 = v.lower()
   print(v2)
   #lower() 方法只对ASCII编码，也就是‘A-Z’有效，对于其他语言（非汉语或英文）中把大写转换为小写的情况只能用 casefold() 方法。
   ```

3. 判断是否是数字： isdecimal(推荐使用)

   ```python
   v = '1'
   # v = '二'
   # v = '②'
   v1 = v.isdigit()  # '1'-> True; '二'-> False; '②' --> True
   v2 = v.isdecimal() # '1'-> True; '二'-> False; '②' --> False
   v3 = v.isnumeric() # '1'-> True; '二'-> True; '②' --> True
   print(v1,v2,v3)
   # 以后推荐用 isdecimal 判断是否是 10进制的数。
   
   # ############## 应用 ##############
   v = ['deng','yi','xin']
   num = input('请输入序号：')
   if num.isdecimal():
       num = int(num)
       print(v[num])
   else:
       print('你输入的不是数字')
   ```

4. split() ,去空白+\t+\n + 指定字符串

   ```python
   # 注意;该方法只能删除开头或是结尾的字符，不能删除中间部分的字符.类似的有rstip/lstrip,从字符串右端/左端来操作.
   v1 = "alex "
   print(v1.strip())
   v2 = "alex\t"    #\t等价于 Tab键,4个空格
   print(v2.strip())
   v3 = "alex\n"     #\n等价于 换行符
   print(v3.strip())
   v1 = "dnengyixin"
   print(v1.strip('dn'))  #  输出'engyixi'     # 只要头尾包含有指定字符序列中的字符就删除
   ```

5. 替换 replace

   ```python
   v = 'dengyixin'
   v1 = v.replace('i','123')
   print(v1)  #  dengy123x123n  ,从左到右替换所有字符
   v = 'dengyixindengyixin'
   v1 = v.replace('i','123',3)
   print(v1)  # dengy123x123ndengy123xin  ,从左到右替换前 3 个字符字符.
   ```

6. 开头 / 结尾startswith/endswith

   ```python
   v = 'dengyixindengyixin'    #判断以``字符开头/结尾
   flag1 = v.startswith('de')
   flag2 = v.endswith('in')
   print(flag1,flag2)   #  True  True
   ```

7. 编码encode，把字符串转换成二进制

   ```python
   
   v = '邓益新'   # 解释器读取到内存后，按照unicode编码方式存储
   v1 = v.encode('utf-8')
   print(v1)               #输出'邓益新'以utf-8码的二进制
   v2 = v1.decode('gbk')
   print(v2)    #报错,编码问题,以什么码写,就用什么码读.
   # 解码 decode，把二进制转换成字符串
   
   ```
8. join方法

  ```python
  name = 'dengixn'
  result = "_".join(name)       # 循环每个元素，并在元素和元素之间加入'_'。
  print(result)    #  d_e_n_g_i_x_n
  ```

9. split 

  ```python
  str.split("根据什么东西进行切割"，从左到右对前多少个东西进行切割)
  str.rsplit("根据什么东西进行切割"，从右到左对前多少个东西进行切割)
  name = 'dengixindeng'
  name_new = name.split('n')  #不填数字默认切割所有,从左到右切割.
  print(name_new)  #  ['de', 'gixi', 'de', 'g']
  ```

### 3.3 字符串补充

#### **3.3.1字节类型数据bytes**

1. str，字符串类型，一般用于内存中做数据操作。

   ```python
   v = 'alex' # unicode编码存储在内存。
   ```

2. bytes，字节类型，一般用于数据存储和网络传输。

   ```python
   v = 'alex'.encode('utf-8')  # 将字符串转换成字节（由unicode编码转换为utf-8编码）
   v = 'alex'.encode('gbk')    # 将字符串转换成字节（由unicode编码转换为gbk编码）
   ```

3. py2和py3字符串类型不同

   - py3： str         bytes
   - py2: unicode     str

4. py2和py3字典方法得到的返回值不同

   - keys
     - py2：列表
     - py3：迭代器，可以循环但不可以索引
   - values
     - py2：列表
     - py3：迭代器，可以循环但不可以索引
   - items
     - py2：列表
     - py3：迭代器，可以循环但不可以索引

5. map/filter

   - py2：返回列表
   - py3：返回迭代器，可以循环但不可以索引



#### 3.3.2py2 和py 3的五点不同点

答 : 

- 默认解释器编码

- 输入输出

- 整数的除法 / int  long 

- py3： str         bytes

  py2: unicode     str

- 字典方法得到的东东不同

  - keys
    - py2：列表
    - py3：迭代器，可以循环但不可以索引

  - values
    - py2：列表
    - py3：迭代器，可以循环但不可以索引
  - items
    - py2：列表
    - py3：迭代器，可以循环但不可以索引

- map/filter

  - py2：返回列表

  - py3：返回迭代器，可以循环但不可以索引

    

### 3.4列表

#### **3.4.1列表介绍**

```python
users = ["代永进","李林元","邓益新",99,22,88]   #表示多个事物，用列表.是有序的,可变类型
```

#### **3.4.2列表方法**

1. append，在列表的最后追加一个元素

   ```python
   users = []
   while True:
       name = input('请输入姓名:')   #  利用无限循环添加用户名到列表users
       users.append(name)
       print(users)
   ```

2. insert,在列表索引位置添加元素

   ```python
   users = ["代永进","李林元","邓益新",99,22,88]
   users.insert(2,'大笨蛋')    #  在列表索引2位置添加'大笨蛋'
   ```

3. remove,从左到右删除列表第一个元素

   ```python
   users = ["代永进","李林元","邓益新",99,22,88]
   users.remove("邓益新")     #   从列表中从左到右删除第一个"邓益新",
   ```

4. pop,删除列表索引位置元素,可以赋值给一个新变量,得到被删除的值.

   ```python
   users = ["代永进","李林元","邓益新",99,22,"邓益新",88]
   result =  users.pop(2)
   print(result,users)#   从列表中删除对应索引位置 /邓益新 ['代永进', '李林元', 99, 22, '邓益新', 88]
   
   ```

5. clear,清除列表所有元素

   ```python
   users = ["代永进","李林元","邓益新",99,22,88]
   users.clear()    #   从列表清除所有元素    ,得到[]
   ```

6. reverse,反转列表元素

   ```python
   users = ["代永进","李林元","邓益新",99,22,88]
   users.reverse()
   print(users)  #  [88, '邓益新', 22, 99, '邓益新', '李林元', '代永进']
   ```

7. sort,从小到大排列列表元素

   ```python
   users = [1,2,3,8,4,6,5,9,0]
   users.sort()   # ()里默认reverse = False
   print(users)   # [0, 1, 2, 3, 4, 5, 6, 8, 9]
   users.sort(reverse = True)     # 从大到下排列列表元素
   ```

8. extend.用于在列表末尾一次性追加另一个序列中的多个值 

   ```python
   users = ["代永进","李林元","邓益新",99,22,88]
   users.extend('deng')
   users.extend([11,22,33])
   users.extend((44,55,66))
   users.extend({77,88,99})
   print(users)   # ['代永进', '李林元', '邓益新', 99, 22, 88, 'd', 'e', 'n', 'g', 11, 22, 33, 44, 55, 66, 88, 99, 77],在后面添加.
   ```

### 3.5元组tuple

#### 3.5.1元组的特点

1. ```python
   users = ["代永进","李林元","邓益新",99,22,88] # 列表可变类型[]
   users = ("代永进","李林元","邓益新",99,22,88) # 元组不可变类型()
   ```

2. 元组没有自己独有的方法.

3. 元组可索引,切片,步长, 不可删除,修改 ..

4. 注意:元组中的元素(儿子)不可被修改/删除

   ```python
   # 元组可以嵌套，可以有list数据,多重嵌套
   v1 = (11,22,33,(44,55,66),(11,2,(99,88,),[666,999,888],["代永进","李林元","邓益新"]))
   注意：元组中嵌套列表，
        元组：只读列表，可循环查询，可切片。儿子不能改，孙子(列表的元素)可能可以改
   
   v2 = (1,2,3,'alex',[2,3,4,'abnowen'],'deng')
   v2[3] = 666      # 错误
   v2[4][3] = 123   #正确
   print(v2)        #  (1, 2, 3, 'alex', [2, 3, 4, 123], 'deng')
   ```

### 3.6字典dict

#### 3.6.1字典介绍

- 是一种可变容器模型，且可存储任意类型对象 ，帮助用户去表示一个事物的信息。各种属性。

- 用{}表示,如

  ```python
  d = {key1 : value1, key2 : value2,键:值 }    #  键值对
  # 键一般是唯一的，如果重复最后的一个键值对会替换前面的。
  # 值可以取任何数据类型，但键必须是不可变的，如字符串，数字或元组，键不可以是列表和字典。
  ```

#### 3.6.2字典方法介绍

1. keys / values / items /

   ```python
   dict = {'Name': 'deng', 'Age':18, 'Class': 'First'}
   print(dict.keys())   #获取dict中所有键  
   print(dict.values())   ##获取dict中所有值
   print(dict.items())    #获取dict中所有键值对
   ```

2. ***get(key, default=None)，获取某键的值，如果字典中不包含该键，就返回得到None或自定义内容***

   ```python
   info = {'k1':'v1','k2':'v2'}
   v1 = info.get('k1')       #   v1
   v2 = info.get('k123456')    #  None,数据类型，该类型表示空（无任何功能，专门用于提供空值）字典中不存在该键是默认得到None
   v3 = info.get('k123478','不存在该键')  #不存在该键时,可自定义返回的值
   print(v1,v2,v3)   #    v1 None 不存在该键  
   ```

3. pop(key),删除字典的键值对.如果字典中没有该键，会报错。

   ```python
   info = {'k1':'v1','k2':'v2'}
   result = info.pop('k1')      #返回得到  'v1'
   print(info)                #{'k2':'v2'}
   ```

4. .update(), 字典添加元素,可添加多个.

   ```python
   info = {'k1':'v1','k2':'v2'}
   info.update({22:33}) 
   info.update({'k1':'123456'})   #  没有的键，键值对就添加进去，有的键，键对应的值就更新
   print(info)   #  {'k1': '123456', 'k2': 'v2', 22: 33}
   ```

### 3.7集合set()

#### 3.7.1集合介绍

- 集合无序，元素唯一，不允许重复.
- 用{},为和字典做区分,空集合表示为set(),类似于d = {0,1,2,3,4}或者d = set()
- 引申:
  - 其他数据类型的也可以类似表示,如:srt()/list()/tuple()/dict()/int()等等

#### 3.7.2集合方法

1. add,为集合添加单个元素

   ```python
   tiboy = set()
   tiboy.add('boy')
   print(tiboy)  #  tiboy = {1,2,3,4,56,7,8,9,'boy'}  不存在元素就添加.存在该元素集合不变/
   ```

2. .update(x), ,添加元素.x 可以是列表，元组，字典

   ```python
   print(tiboy)    #  update  x 可以是列表，元组，字典。
   tiboy.update([(1,2,3,4,5,6,7),'deng','yi','xin',1,2,3])
   print(tiboy)   
   #  {1, 2, 3, 4, 7, 8, 9, 'xin', 'deng', 'yi', 56, (1, 2, 3, 4, 5, 6, 7), 'boy'}   列表/字典/集合不能放在集合中，但元组可以。
   ```

3. .discard(x)  ,移除集合的元素x，若集合没有x，返回值为None，区别于.remove(x) ,若集合没有x,则会报错。.clear()  清除集合所有元素。

   ```python
   tiboy = {1,2,3,4,56,7,8,9,'boy'}
   tiboy.discard(1)
   print(tiboy,t)   # {2, 3, 4, 7, 8, 9, 'boy', 56}
   ```

4. .intersection()    /     .union( )      /     .difference()   /.symmetric_difference（求交集，并集 差集)

   ```python
   A = {1,2,3,4,5,6}
   B = {5,6,7,8,9,10}
   集合1和2的并集 =  A.union(B)
   print(集合1和2的并集)    #A∪B
   集合1减去2的差集 = A.difference(B)
   print(集合1减去2的差集)    # 集合A-B
   集合1和2的交集 = A.intersection(B)
   print(集合1和2的交集)    #A∩B
   deng123 = A.symmetric_difference(B)
   print(deng123)      #  A∪B-A∩B
   ```

### 3.8 公共功能

- len(),求长度.(排除 int  / bool)
  - str,求字符的个数.len("deng") = 4 / len("邓益新") = 3
  - 对列表/元组/集合,求元素的个数
  - 对字典,求键值对的个数

- 索引 ( int / bool  /set  )
  - 从左到右,从0开始,取前不取后.
  - str , 'deng'[0] = 'd'  /  deng'[2] = 'n'   /也可以最后一位,以-1为起始.'deng'[-1]  = 'g'
  - 对列表/元组,类似于字符串,索引对应里面的元素,   因集合无序,不具有索引功能.
  - 对字典,索引的是它的键,得到键对应的值.  {}[key] = value

- 切片 ( int / bool  /set  /dict)
  - 通过索引取单个元素,切片可以取一片
  - str ,'deng'[0:2] = 'den'   ,其他列表/元组与此类似.
  - 字典 无切片 功能,  集 合 无序,没有切片功能.

- 步长  ( int / bool  /set  /dict )
  - str ,'dengyixin'[::2]  = 'dnyxn'  每2个字符取前一个,步长为2.步长为负数,则反转,从右到左切片.
  - list / tuple / 都与此类似.
  - 字典和集合没有此功能

- for循环
  - for i in str/list/tuple/set ,都是对里面的元素循环一次

  - for item in dict,这里指的是字典的键.

  - 注意：for和while的应用场景：有穷尽优先使用for，无穷尽用while。

    ```python
    name = 'dengxin'
    for item in name:
        print(item)    # 竖向打印d e n g x i n
        
    name = 'dengxin'   
    for item in name:
        print(item)
        break           # for循环的break语句
        print('123')
        
    name = 'dengxin'
    for item in name:
        print(item)
        continue        # for循环的continue语句
        print('123')
    ```

- 删除del    del user[索引]     数字/字符串/布尔值除外

- 1. 索引（排除：int/bool）

     ```python
     users = (11,22,33,"老男孩")
     
     print(users[0])
     print(users[-1])
     ```

  2. 切片（排除：int/bool）

     ```python
     users = (11,22,33,"老男孩")
     print(users[0:2])
     ```

  3. 步长（排除：int/bool）

     ```python
     users = (11,22,33,"老男孩")
     print(users[0:2:2])
     ```

  4. 删除（排除：tuple/str/int/bool）

  5. 修改（排除：tuple/str/int/bool）

  6. for循环（排除：int/bool）

     ```python
     users = (11,22,33,"老男孩")
     for item in users:
         print(item)
     ```

  7. len（排除：int/bool）

     ```python
     users = (11,22,33,"老男孩")
     print(len(users))
     ```

  

### 3.9额外内容

#### 3.9.1判断敏感字符

- str： 使用in 即可判断

- list/tuple

  ```python
  list1 = ['alex','oldboy','deng','xin']
  if 'deng' in list1：
  	print('含敏感字符')   #  同样，对于元组也是这样
  
  ```

- dict

  ```python
  dict1 = {'k1':'v1','k2':'v2','k3':'v3'}
  #默认按照键判断
  if 'x' in dict1 ：     #  判断x是否是字典的键。
  	print('dict1的键包含x')
      
  #判断字典的值x   #第一种
  dict1 = {'k1':'v1','k2':'v2','k3':'v3'}
  if 'x' in list(dict1.values()）:    #  ，强制转化为list判断x是否是字典的值。
      print('dict1的值包含x')   
      
  #判断字典的值v2   #第二种循环
  for v in dict1.values()：
  	if v == 'v2'
      print('存在')
                 
  #判断字典的键值对 k2:v2 是否在其中
  value = list1.get('k2')
  if value == 'v2':
  	print('包含')   
  ```

### 3.10 嵌套

#### 3.10.1列表嵌套

- 列表可以嵌套多层,int、str、bool、list都可以有

#### 3.10.2元组嵌套

- 元组可以嵌套，可以有list数据,多重嵌套

- 元组和列表可混合嵌套

- 注意:元组中的元素(儿子)不可被修改/删除

  ```python
  # 元组可以嵌套，可以有list数据,多重嵌套
  v1 = (11,22,33,(44,55,66),(11,2,(99,88,),[666,999,888],["代永进","李林元","邓益新"]))
  注意：元组中嵌套列表，
       元组：只读列表，可循环查询，可切片。儿子不能改，孙子(列表的元素)可能可以改
  
  v2 = (1,2,3,'alex',[2,3,4,'abnowen'],'deng')
  v2[3] = 666      # 错误
  v2[4][3] = 123   #正确
  print(v2)        #  (1, 2, 3, 'alex', [2, 3, 4, 123], 'deng')
  ```

#### 3.10.3字典嵌套(重点)

- **列表/字典/集合（可变类型） -> 不能放在集合中+不能作为字典的key（unhashable）**

- hash函数，在内部会将值进行哈希算法并得到一个数值（对应内存地址），以后用于快速查找。值必须不可变。

- 特殊情况，True和False在集合和字典可能会和0， 1 重复。hash函数对2者得到一样的值。如：

  ```python
  
  info = {0, 2, 3, 4, False, "国风", None, (1, 2, 3)}
  print(info)  # {0, 2, 3, 4, "国风", None, (1, 2, 3)}
  
  info1 = {
       True:'alex',
       1:'oldboy'
   }
  print(info1)      #  {True:'oldboy'}
  ```

### 3.11内存问题

- 变量

  1. 第一次赋值时，即创建它，之后赋值将会改变变量的值。
  2. 变量名本身是没有类型的，类型只存在对象中，变量只是引用了对象而已。

- 对象

  1. 对象是有类型的，例如各种数据类型。
  2. 对象是分配的一块内存空间，来表示它的值。

- 引用

  在Python中从变量到对象的连接称作引用。
  引用是一种关系，以内存中的指针的形式实现。

  1. 简单引用

     ```python
     v1 = [11,22,33]  #解释器创建了一块内存空间（地址），v1指向这里，v1引用此内存位置
     v1 = [44,55,66] 
     v1 = [1,2,3]    # v1被多次赋值，即修改了v1的引用，把v1的指向关系变了，指向改为另一块内存地址。
     v2 = [1,2,3]   #解释器创建了另一块内存空间（地址），v2指向这里。
     v1 = 666   # 同理，v1，v2指向不同的内存地址
     v2 = 666
     v1 = "asdf"   # 特殊，对于赋值给字符串，v1，v2指向一个内存地址                  （字符串不可变）
     v2 = "asdf"
     
     v1 = 1 
     v2 = 1   #特殊：v1，v2指向相同的内存地址
              #解释：1. 整型：  -5 ~ 256 ，py认为它是常用的，有缓存机制，就把v1和v2指向相同的内存地址，以节省内存。
              #####  2. 字符串："alex",'asfasd asdf asdf d_asd'（字符串不可变 ），指向同一内存地址   
              #####  3."f_*" * 3  - 重新开辟内存,一旦字符串*数字,数字不为1的运算，就重新开辟内存空间
     ```

  2. 共享引用

     ```python
     #示例一
     a = 3  #解释器创建了一块内存空间（地址），a指向此处
     b = a  # b也指向次内存地址
     #示例二(内部修改)
     v1 = [11,22,33]   #解释器创建了一块内存空间（地址），v1指向此处
     v2 = v1  #v2也指向这里，如果语句为：v2 = [11,22,33]，则是解释器创建了另一块内存空间（地址），v2指向这里。只是v1和v2的值相同。
     v1.append(123) #v1指向的内存地址添加上来元素666，v2跟着变更。
     print(v2)     # [11,22,33,123] 
     #示例三（赋值）
     v1 = [11,22,33]
     v2 = [11,22,33]
     v1.append(123)
     print(v2)     #  [11,22,33] 
     #示例四（重新赋值）
     v1 = [11,22,33]  # v1指向一个内存地址
     v2 = v1  # v2指向同一个内存地址
     v1 = [44,55,66,77]  #v1的指向关系改变，解释器创建了内存地址，并将v1的指向改为指向它。
     print(v2)  # v2的指向没有变化，输出[11,22,33]
     ```

  3. == 和 is d的区别(重点)

     - == 用于比较值是否相等

     - is 用于比较内存地址是否相等/比较指向关系是否相同/比较指向的内存空间是否是同一个。

       ```python 
       id(v1)   # 返回得到 变量名v1指向的内存地址
       ```

     - 小总结

       - 变量是一个系统表的元素，拥有指向对象的连接的空间。
       - 对象是分配的一块内存，有足够的空间去表示它们所代表的值。
       - 引用是自动形成的从变量到对象的指针

### 3.12 深浅拷贝

1. 简介

   ```python
   v = [1,2,3,[4,5,6],7,8,9]
   import copy
   v1 = copy.copy(v)               #浅拷贝
   v2 = copy.deepcopy(v)           #深拷贝
   ```

2. 对str\int\bool类型而言,深浅拷贝没有区别.(不可变)

   ```python
   a = 'deng'   #a指向内存中第一个内存地址
   import copy
   b = copy.copy(a)   #b指向内存中第一个内存地址
   c= copy.deepcopy(a)   #c指向内存中第一个内存地址
   #注意:这里变量abc理应指向不同的内存地址,但因为小数据尺的缘故,内存地址相同.
   ```

3. list/dict/set类型来说,如果没有嵌套,深浅拷贝没有区别.(可变)

   ```python
   v = [1,2,3,[4,5,6],7,8,9]  有嵌套,有7个元素
   v1 = [1,2,3,4,5,6,7,8,9]   没嵌套,有9个元素
   import copy
   v2 = copy.copy(v1)               
   v3 = copy.deepcopy(v1)           
   
   ```

4. 对嵌套的list/dict/set类型来说,深浅拷贝才有意义.

   ```python
   v = [1,2,3,[4,5,6],7,8,9]  有嵌套,有7个元素
   import copy
   v1 = copy.copy(v)                 
   v2 = copy.deepcopy(v)
   ```

   - 对于其中的元素都是不可变类型时，深拷贝和浅拷贝的结果都是一样的，都是只拷贝第一层
   - 对于其中元素存在可变类型时，浅拷贝只拷贝第一层，深拷贝要拷贝所有的可变类型

5. 特殊:tuple元组(不可变)

   - 如果元组中不含有可变类型，同理字符串的深浅拷贝.
   - 如果元组中含有可变类型，同理列表的深浅拷贝.

6. 深拷贝和浅拷贝

   - 浅拷贝：只拷贝第一层
   - 深拷贝：拷贝嵌套层次中的所有可变类型
   - 拷贝只针对可变类型:在内存中重新创建内存空间,对不可变数据类型无法操作.

7. 可变数据类型和不可变数据类型

   - 可变数据类型：在id不变的情况下，value可改变（列表/字典/集合是可变类型，但是字典中的key值必须是不可变类型） 
   - 不可变数据类型：value改变，id也跟着改变。（数字，字符串，布尔类型，都是不可变类型） 



## 第四章 文件操作

### 4.1 文件基本操作

```python
obj = open('路径',mode='模式',encoding='编码')   #  打开文件
obj.write()    #  把内容写入文件
obj.read()     #  读取文件内容
obj.close()    #  关闭文件(保存文件,把内存上的数据写入到文件上-->硬盘上,01010101存储的)

# 3步:1.打开文件.  2. 操作文件   3. 关闭文件
```

### 4.2打开文件

#### 4.2.1语句

```python
file = open('文件路径',mode = 'r',encoding = 'utf-8')
# 文件路径:     D:\文件夹名字\文件夹
# encoding = 'utf-8',以utf-8编码方式打开.
#########或者用另外一种语句:
with open(''文件路径',mode = 'r',encoding = 'utf-8') as file:
#或者 同时打开2个文件.下面的代码需要缩进,且不需要.close()语句.
with open(''文件路径',mode = 'r',encoding = 'utf-8'') as file ,open(''文件路径',mode = 'r',encoding = 'utf-8'') as file2 :
	v1 = file.read()
    v2 = file2.read()
```

#### 4.2.2打开文件的模式(mode)

- r / w / a      只读只写字符串

- r+ / w+ / a+   可读可写字符串

  1. 读取：r，只能读文件,默认的模式.文件不存在就报错.
  2. 写入：w，只能写文件，文件不存在则创建，文件存在则清空内容在写入. .
  3. 追加：a，只能追加,文件不存在则创建，文件存在则不会覆盖，写内容会以追加的方式写 (写日志文件的时候常用 ).
  4. 可读可写：r+
     - 读：默认从0的光标开始读，也可以通过 seek 函数调整光标的为位置
     - 写：从光标所在的位置开始写，也可以通过 seek 调整光标的位置
  5. 可读可写：w+
     - 读：默认光标永远在写入的最后，也可以通过 seek 函数调整光标的位置
     - 写：先清空
  6. 可读可写：a+
     - 读：默认光标在最后，也可以通过 seek 函数 调整光标的位置。然后再去读取
     - 写：永远写到最后

- rb / wb / ab  只读只写二进制

  ```python
  file = open('文件路径',mode = 'wb')  # rb/wb/ab模式,不需要encoding = 'utf-8'
    #注意,如果是/rb/ab/wb模式,写入和读取的必须是二进制,即010100010 010110101 0101000,否则报错.写入数据是
  data = '你好,世界'.encode('utf-8')    #将字符串转化为utf-8编码方式的二进制数据.
  file.write(data)
  file.close()  
  print(v)
  ########
  ```

- r+b / w+b / a+b   可读可写二进制  ,  r+b/w+b/a+b模式同上.

### 4.3 文件操作

- read() , 全部读到内存

- read(1) 

  - 1表示一个字符

    ```python
    obj = open('a.txt',mode='r',encoding='utf-8')
    data = obj.read(1) # 1个字符
    obj.close()
    print(data)
    ```

  - 1表示一个字节

    ```python
    obj = open('a.txt',mode='rb')
    data = obj.read(3) # 1个字节
    obj.close()
    ```

- readlins()  

  ```python
  date_list = file.readlines() #  读取整个文件所有行，保存在一个列表(list)变量中，每行作为一个元素，但读取大文件会比较占内存.
  ```

- 大文件读取(50g的文件,内存没有这么大)

  ```python
  file = open('文件路径',mode = 'r',encoding = 'utf-8')
  ###  2.如果以后读取一个特别大的文件，可以一行一行读取
  for line in file:
      line = line.strip()     #去除换行符(默认去除换行符\n),也可以填其他.
      print(line)             #一行一行读取,
  ```

- write(字符串,utf-8)

  ```python
  obj = open('a.txt',mode='w',encoding='utf-8')
  obj.write('中午你')
  obj.close()
  ```

- write(二进制)

  ```python
  obj = open('a.txt',mode='wb')
  
  # obj.write('中午你'.encode('utf-8'))
  v = '中午你'.encode('utf-8')
  obj.write(v)
  
  obj.close()
  ```

- seek函数:(光标字节位置) **用于移动文件读取光标到指定位置**，无论模式是否带b，都是按照字节进行处理。

  ```python
  obj = open('a.txt',mode='r',encoding='utf-8')
  obj.seek(3) # 跳转到指定3字节位置
  data = obj.read()
  obj.close()
  
  print(data)
  ##################################################
  obj = open('a.txt',mode='rb')
  obj.seek(3) # 跳转到指定字节位置
  data = obj.read()
  obj.close()
  
  print(data)
  ######################################
  fileObject.seek(offset[, whence])
  #  offset -- 开始的偏移量，也就是代表需要移动偏移的字节数.以字节为单位.
  #  whence：可选，默认值为 0。给offset参数一个定义，表示要从哪个位置开始偏移；0代表从文件开头开始算起，1代表从当前位置开始算起，2代表从文件末尾算起。如seek(0,0)
  ```

- tell(), 获取光标当前所在的字节位置

  ```python
  obj = open('a.txt',mode='rb')
  # obj.seek(3) # 跳转到指定字节位置
  obj.read()
  data = obj.tell()
  print(data)
  obj.close()
  ```

- flush，强制将内存中的数据写入到硬盘

  ```python
  v = open('a.txt',mode='a',encoding='utf-8')
  while True:
      val = input('请输入：')
      v.write(val)
      v.flush()      # 避免内存泄漏
  
  v.close()
  ```

### 4.4 关闭文件

一般方法

```python
v = open('a.txt',mode='a',encoding='utf-8')
# 文件操作
v.close()
```

避免忘记输入.close()的方法.

```python
with open('a.txt',mode='a',encoding='utf-8') as v:
    v.write('这是另一种文件打开方法')
	# 缩进中的代码执行完毕后，自动关闭文件
```

### 4.5  文件内容的修改

```python
with open('a.txt',mode='r',encoding='utf-8') as f1:
    data = f1.read()
new_data = data.replace('飞洒','666')

with open('a.txt',mode='w',encoding='utf-8') as f1:
    data = f1.write(new_data)
```

- 大文件修改

```python
f1 = open('a.txt',mode='r',encoding='utf-8')
f2 = open('b.txt',mode='w',encoding='utf-8')

for line in f1:
    new_line = line.replace('阿斯','死啊')
    f2.write(new_line)
f1.close()
f2.close()
```

```python
with open('a.txt',mode='r',encoding='utf-8') as f1, open('c.txt',mode='w',encoding='utf-8') as f2:
    for line in f1:
        new_line = line.replace('阿斯', '死啊')
        f2.write(new_line)
```

## 第五章 函数

### 5.1三元运算/三目运算

```python
v = 前面  if 条件语句  else   后面
#如果条件成立,"前面"赋值给v,否则后面赋值给v.
v = a if a>b  else b  # 取a和b中值较大的赋值给v

# 让用户输入值，如果值是整数，则转换成整数，否则赋值为None

data = input('请输入值:')
value =  int(data) if data.isdecimal() else None 
```

### 5.2 函数

#### 5.2.1.函数介绍

1. 截止目前为止,都是面向过程式编程.可读性差,重复性高,容易出错.

2. 对于函数编程：

   - 本质：将N行代码拿到别处并给他起个名字，以后通过名字就可以找到这段代码并执行。
   - 场景：
     - 代码重复执行。
     - 代码量特别多超过一屏，可以选择通过函数进行代码的分割。

3. 函数是组织好的，可重复使用的，用来实现单一，或相关联功能的代码段。

   函数能提高应用的模块性，和代码的重复利用率.

   py3中给我们提供了许多内建函数,如len()/print()等等,我们也可以自定义函数,这叫做用户自定义函数 .

#### 5.2.2.函数定义和表达

1. 定义如下:

   - 函数代码块以 **def** 关键词开头，后接函数标识符名称和圆括号 **()**。

   - 任何传入参数和自变量必须放在圆括号中间，圆括号之间可以用于定义参数。

   - 函数的第一行语句可以选择性地使用文档字符串—用于存放函数说明。

   - 函数内容以冒号起始，并且缩进。

   - **return [表达式]** 结束函数，选择性地返回一个值给调用方。不带表达式的return相当于返回 None。

     注意:  遇到return语句,后面的代码不执行.

2. Python 定义函数使用 def 关键字，一般格式如下： 

   ```python
   def 函数名称(形参): #算列表中所有元素的和,括号中可以添加形式参数.简称形参,也可以不加.
       函数代码
   ```

   - 最简单的形式

     ```python
     def hello() :
        print("Hello World!")  #定义函数
     
     hello()  # 调用函数,输出"Hello World!"
     
     ```

   - 复杂一些,加上参数

     ```python
     #  计算列表中的元素的和
     def sum_list_element(list1):
         sum = 0
         for i in list1:
             sum += 1
          print(sum)
         
     #  调用函数
     sum_list_element([1,2,3,4,5,6,7,8,9])  # 输出45
             
     
     ```

   - 加上return的应用(重点) --> 返回多个值自动装换为元组

     ```python
     # 比较2个数的大小,并返回得到较大的数
     def compare_num(a,b):
         v = a if a > b else b
         return v   # 如果没有return语句,  print(compare_num(5,10)) 输出的是None
     print(compare_num(5,10))   # 调用函数,得到a,b中较大的数,并打印出来.
     
     
     #函数没有返回值时,默认返回None
     #函数有返回值时,语句为:return 返回值.返回值可以是任意东西.
     def compare_num(a,b):
         return a if a>b else b  #和三目运算一起,返回a和b中较大的数.
     #############################################
     #  函数执行到return 语句,就返回,后面的语句不执行.
     #  若返回值为多个,默认将其添加到元组中,返回一个元组.
     def func(a,b,c,d)
     	return a,b,c,d
     
     v = func(1,'邓益新',[11,22,33,44,55],{'k1':'v1'})
     print(v)            # (1,'邓益新',[11,22,33,44,55],{'k1':'v1'})  元组,
     ```



#### 5.2.3函数参数解析

1. 参数

   ```python
   def func(a1,a2):
       函数体(可调用a1和a2)
       return 返回值
   # 严格按照顺序传参数：位置方式传参。
   # 实际参数可以是任意类型。
   
   def check_age(age):  #只有1个参数:age,参数数量可以是无穷多个.
       if age >= 18:
           print('你是成年人')
       else:
           print('你是未成年人')
           
   check_age(18)    #调用函数,输出你是成年人.
   
   ```

2. 位置参数

   ```python
   def func(a1,a2,a3,a4)
   	print(a1,a2,a3,a4)
     
   func(1,2,3,4)
   # 严格按顺序输入参数,顺序是固定的.
   ```

3. 关键字传参

   ```python
   def func(a1,a2)
   	print(a1,a2)
     
   func(a1 = 1 ,a2 = 2) 
   func(a2 = 2,a1 = 1)  # 俩者完全一样,全是关键字可以打乱顺序.
   ```

4. 位置参数和关键字传参混用

   ```python
   ######  这时必须位置参数在前,关键字参数在后
   def func(a1,a2,a3,a4)
   	print(a1,a2,a3,a4)
      
   func(1,10,a3 = 15,a4 = 88) 
   func(1, 10, a4=15, a3=88)  # 俩者等价,正确.
   func(a1=1,a2=2,a3=3,a4=4)  #正确
   func(a1=1,a2=2,15,a4 = 88)  # 错误
   func(1,2,a3 = 15,88)  #错误
   #################################
   #    必须位置参数在前,关键字参数在后, 位置参数 > 关键字参数  #
   
   ```

5. 默认参数

   ```python
   def func(a1,a2,a3=9,a4=10):   #  默认a3=9,a4=10,这时可以不输入a3 和 a4 参数.默认值为 9 和 10 
       print(a1,a2,a3,a4)
   
   func(11,22)   #正确
   func(11,22,10)  #  正确,修改默认参数a3 = 10 ,参数a4不输入.默认为10 .
   func(11,22,10,100)  #正确
   func(11,22,10,a4=100)   #正确
   func(11,22,a3=10,a4=100)    #正确
   func(11,a2=22,a3=10,a4=100)   #正确
   func(a1=11,a2=22,a3=10,a4=100)    #正确
   
   ```

6. **万能参数(重点)**

   - args(打散)**可以接受任意个数的位置参数，并将参数转换成元组。**

     - 没有调用函数   *    字符

       ```python
       def func(*args):
           print(args)
       func(1,2,3,True,[11,22,33,44])
       #  输出为(1,2,3,True,[11,22,33,44])  元组
       func(*(1,2,3,4,5)) # 如果不加*号,函数会将序列当成一个元素加入到元组中,*相当于拆包.
       ```

     - 有调用函数   *    字符

       ```python
       def func(*args):
           print(args)
       func(*(1,2,3,True,[11,22,33,44]))    
       #  输出为(1,2,3,True,[11,22,33,44])  元组
       ```

   - *args  只能用位置传参

     ```python
     def func4(*args):
         print(args)
         
     func4(1)    元组(1,)
     func4(1,2)   元组(1,2)
     
     func4(1,2)
     func4(1,2)
     
     ```

   - **kwags(打散)  可以接受任意个数的关键字参数，并将参数转换成字典。

     - 没有调用函数   **    字符

       ```py
       def func(**kwargs):   
           print(kwargs)
       
       func(k1=1,k2="alex")
       ```

     - 有调用函数   **    字符

       ```python
       def func(**kwargs):
           print(kwargs)
       func(**{'k1':'v2','k2':'v2'}) # kwargs={'k1':'v2','k2':'v2'}func(*(1,2,3,4,5)) # 如果不加**号,函数会将序列当成一个元素加入到元组中,相当于只有键没没有值,就会报错.
       ```

     - **kwags(打散)只能用关键字传参 

#### 5.2.4函数参数默认值使用可变类型--->有坑(重点)

1. 补充：对于函数的默认值慎用可变类型。

```python
# 如果要想给value设置默认是空列表

# 不推荐(坑)
def func(data,value=[]): 
    pass 

# 推荐
def func(data,value=None):
    if not value:
        value = []

```

```python
def func(data,value=[]): 
    value.append(data)
    return value 

v1 = func(1) # [1,]
v2 = func(1,[11,22,33]) # [11,22,33,1]
```

- 原因

  ```python
  def func(data,value=[]): 
      value.append(data)
      return value 
  
  v1 = func(1) # [1,]     # 调用函数,作用域得到value=[],添加value=[1,]
  v2 = func(2)  # [1,2,]   #调用函数,value=[1,]再次添加元素2,
  
  v2 = func(1,[11,22,33]) # [11,22,33,1]
  
  ##############    def func(a,b=[]) 有什么陷阱？   #######
  答: Python函数在定义的时候。默认参数b的值就被计算出来了，即[],因为默认参数b也是一个变量，它指向对象即[],每次调用这个函数，如果改变b的内容每次调用时候默认参数也就改变了，不在是定义时候的[]了.应该换成None  .
  
  ```

  

#### 5.2.5函数参数混用

```python
def func(*args,**kwargs):
    print(args,kwargs)

# func(1,2,3,4,5,k1=2,k5=9,k19=999)
func(*[1,2,3],k1=2,k5=9,k19=999)
func(*[1,2,3],**{'k1':1,'k2':3})
func(111,222,*[1,2,3],k11='alex',**{'k1':1,'k2':3})

```

#### 5.2.6函数作用域

1. 函数的作业域

   - 作用于全局,对任意一个py文件来说,全局作用域

   - 对函数来说:局部作用域

     ```python
     #  在函数中定义的值,只在局部作用域中
     #  在全局作用域中找不到#
     a = 1
     b = 2
     def func():
         a = 10
         b = 20
         print(a) 
         print(b)
     func()     #  输出10  ,20
     print(a)
     print(b)   #输出 1    , 2
     
     
     ```

2. 作用域查找数据规则::优先在自己的作用域找数据，自己没有就去  "父级" ->  "父级" -> 直到全局，全部么有就报错。

   ```python
   a = b = 1
   def func():
       b = 5
       print(a,b)
   func()     #  1 ,5
   print(a,b)  #   1, 1
   ```

3. 子作用域只能找到父级作用域中的变量的值,不能重新为赋值父级作用域的变量赋值.

   - 例外,特别:  global 变量名,强制把全局的变量赋值

     ```python
     a = b = 1
     def func():
     	global b
         b = 5     # 更改全局变量b = 5
         print(a,b)
     func()     #  1 ,5
     print(a,b)  #   1, 5
     ```

   - nonlocal   ,强制把父级的变量赋值 ,一级一级向上找寻,不找全局域.找不到则报错.

     ```python
     a = b = 1
     def func():
     	nonlocal b
         b = 5     # 更改父级变量,如果找不到,再到父级的父级找寻,一直找到全局域之前(不包含全局域)
         print(a,b)
     func()      #会报错,父级就是全局域,nonlocal不包含全局域,故会报错,找不到b 
     print(a,b)  
     
     a = b = 1
     def func():
         a = 3
         b = 99
         print(a,b)
         def func1():
             nonlocal b
             b = 88
             print(a,b)
         func1()
     func()
     print(a,b)
     
     ```

   - 补充: 全局变量大写

4. 递归函数(效率低下,不推荐)

   ```python
   # 递归的返回值
   ##函数在内部调用自己--->  递归函数
   # 效率很低,python对栈,即函数递归的次数有限制-->1000次
   # 尽量不要使用 递归函数
   # 递归的返回值
   def func(a):
       if a == 5:
           return 100000
       result = func(a+1) + 10
   
   v = func(1)
   name = 'alex'
   def func():
       def inner():
           print(name)
        return inner
   v =func()
   ```

   ```python
   def func(i):
       print(i)
       func(i+1)
       
   func(1)
   
   
   递归次数有限制 1000次
   ```

   

#### 5.2.7执行函数

- 函数不被调用，内部代码永远不执行。

  ```python
  def func():
      return i
  func_list = []
  for i in range(10):
      func_list.append(func)
  
  print(i) # 9
  v1 = func_list[4]()
  v2 = func_list[0]()
  ```

  ```python
  func_list = []
  for i in range(10):
      # func_list.append(lambda :x) # 函数不被调用，内部永远不执行（不知道是什么。）
      func_list.append(lambda :i) # 函数不被调用，内部永远不执行（不知道是什么。）
  
  print(func_list)
  
  func_list[2]()=
  ```

  - 执行函数时，会新创建一块内存保存自己函数执行的信息 => 闭包

    ```python
    def base():
        return i 
    
    def func(arg):
        def inner():
            return arg
        return inner
    
    base_list = [] # [base,base,]
    func_list = [] # [由第一次执行func函数的内存地址，内部arg=0 创建的inner函数，有arg=1的inner函数 ]
    for i in range(10): # i = 0 ，1
        base_list.append(base)
        func_list.append(func(i))
        
    # 1. base_list 和 func_list中分别保存的是什么？
    """
    base_list中存储都是base函数。
    func_list中存储的是inner函数，特别要说的是每个inner是在不同的地址创建。
    """
    # 2. 如果循环打印什么？
    for item in base_list:
    	v = item() # 执行base函数
        print(v) # 都是9
    for data in func_list:
        v = data()
        print(v) # 0 1 2 3 4 
    ```

总结：

- 传参：位置参数 > 关键字参数 
- 函数不被调用，内部代码永远不执行。 
- 每次调用函数时，都会为此次调用开辟一块内存，内存可以保存自己以后想要用的值。
- 函数是作用域，如果自己作用域中没有，则往上级作用域找。 



### 5.3 函数高阶

#### 5.3.1 函数中高阶(重点)

1. 函数的变换

   ```python
   def func():
       print('小米手机真的好')  #  这里func--------->指向函数的内存地址
   print(id(func))   #  33627808
   a = '小米手机真的好'
   print(id(a))     #  35196048
   #  函数的名字和变量类似,有类似的指向关系.变量指向一块内存地址放置数据,函数也指向一块内存地址放置数据
   
   ```

2. 函数名当做变量使用

   ```python
   def func():
       print('小米手机真的好')  
   print(id(func))   #  33627808
   a = '小米手机真的好'
   print(id(a))     #  35196048
   
   fuck_world = func
   print(id(fuck_world))    #33627808 ,2者内存地址一致.
   fuck_world()  # 调用函数,打印'小米手机真的好'
   
   
   ```

3. 函数名作为列表/元组/字典元素(是不可变类似,可以做字典的键<-------->str)

   ```python
   def func1():
       print('快活啊')
       
   func_list = [func1, func1, func1]
   func_list[0]()
   func_list[1]()
   func_list[2]()   # 类似的用法,调用函数运行,注意: 加上括号就可以运行该函数了
   for item in func_list:
       v = item()    #  函数语句没有retun,返回值默认为None
       print(v)   
   ```

4. 函数可以当作参数进行传递    --->构造字典和函数的对应关系,避免重复if else 

   ```python
   def func3(arg):
       v = arg()
       print(arg())
       print(arg)
   
   
   def show():
       return 999
   
   func3(show)    # 调用函数,把show函数当做参数使用.
   
   ```

   - 面试题相关

     ```python
     def func3():
         print('话费查询')
     def func4():
         print('流量办理')
     def func5():
         print('充值')
     def func6():
         print('人工服务')
     
     info = {
         'k1':func3,
         'k2':func4,
         'k3':func5,
         'k4':func6
     }
     
     content = input('请输入你要办理的业务:')
     function_name = info.get(content)
     function_name()          # 运用字典调用函数
     ```

5. 函数可以做返回值

   ```python
   def func():
   	print('name')
       
   def show()
   	return func
   # ******************  
   v = show()
   
   v()   # 执行打印name     
   #********************
   show()()  # 相当于show()()
   ```

6. 函数的闭包--->封装了值,内层函数调用才是闭包,缺一不可 .

   - 闭包概念：为函数创建一块区域并为其维护自己数据，以后执行时方便调用。【应用场景：装饰器 / SQLAlchemy源码】

     ```python
     name = 'oldboy'
     def bar(name):
         def inner():
             print(name)
         return inner
     v1 = bar('alex') # { name=alex, inner }  # 闭包，为函数创建一块区域（内部变量供自己使用），为他以后执行提供数据。
     v2 = bar('eric') # { name=eric, inner }  ,2者不干扰运行.
     v1()
     v2()
     
     # 不是闭包
     def func1(name):
         def inner():
             return 123
         return inner 
     
     # 是闭包：封装值 + 内层函数需要使用。
     def func2(name):
         def inner():
             print(name)
             return 123
         return inner 
     ```

7. 数据类型中那些有返回值

   ```python
   # 无返回值
     v = [11,22,33]
     v.append(99) # 无返回值
     
   # 仅有返回值：
     v = "alex"
     result = v.split('l')
     
     v = {'k1':'v2'}
     result1 = v.get('k1')
     result2 = v.keys()
   # 有返回+修改数据 (少)
     v = [11,22,33]
     result = v.pop()
   ```

8. 常用数据类型方法返回值(重点)

   ```python
   - str
     - strip，返回字符串
     - split，返回列表
     - replace，返回字符串
     - join，返回字符串。
   - list
     - append，无
     - insert，无
     - pop，返回要删除的数据
     - remove，无
     - find/index，返回索引的位置。
   - dict
     - get
     - keys
     - values
     - items
   ```

   

#### 5.3. 2 lambda函数(匿名函数)

用于表示非常简单的函数,就如三元运算和if 语句一样.

- lambad函数只能用一行代码

  ```python
  # lambda表达式，为了解决简单函数的情况，如：
  
  def func(a1,a2):
      return a1 + 100 
  
  func = lambda a1,a2: a1+100       #   2者等价,注意冒号后接代码,自动return 
  
  ```

- 应用

  ```python
  func = lambda a,b:a if a > b else b
  t =  func(1,5)
  print(t)   #  求2个数里面较大的数
  
  # 练习题1
  USER_LIST = []
  def func0(x):
      v = USER_LIST.append(x)
      return v 
  
  result = func0('alex')
  print(result)
  
  
  # 练习题2
  
  def func0(x):
      v = x.strip()
      return v 
  
  result = func0(' alex ')
  print(result)
  
  ############## 总结：列表所有方法基本上都是返回None；字符串的所有方法基本上都是返回新值 
  
  ```

### 5.4  python内置函数简介

#### 5.4.1自定义函数

#### 5.4.2内置函数

- 输入输出print  input

- 强制转换类型

  - dict()
  - list()
  - tuple()
  - int()
  - str()
  - bool()
  - set()

- 其他

  - len
  - open
  - range
  - id
  - type

- 数学相关

  - abs  求绝对值

  - float ,转换为浮点型(小数)

  - max  /min

  - sum  求和

  - divmod  ,获取2数字相除的商和余数

  - pow ,指数函数,获取指数

    ```python
    v = pow(2,5)
    print(v)  # 2^5 = 32
    
    ```

  - round函数,四舍五入函数

    ```python
    v= round(1.24879,2) #小数点后取2位
    print(v)  #  1.25
    ##############################################
    # 注意:由于计算机精度问题,round函数可能会出错
    round(2.355,2)   # 算得的是 2.35  ,py版本3.6.8
    ```

- 编码相关

  - chr()  ,将十进制转化unicode编码中对应的字符串

    ```python
    v =chr(152)
    print(v)
    
    i = 1
    while i < 1000:
        print(chr(i))
        i += 1
    ```

    

  - ord(), 根据字符在unicode编码的对应关系,找到对应的十进制.

    ```python
    v= ord("字")   #  一次只能索引一个字符
    print(v)
    
    content = """一天，我正在路上走着，突然一声
    雄浑有力的声音传进我的耳朵里。"""
    for i in content:
        print(ord(i))
    ```

- 进制转换

  - bin() 10进制转为2进制   ,''0b"

  - oct()   10进制转为8进制   ." 0o"

  - hex()    10进制转为16进制    ."0x"

  - int()    把````数转为10进制数    ,base默认为10 ,转二进制数base = 2,base = 8,  base = 16

    ```python
    num= 192
    n1 = bin(num)
    n2 = oct(num)
    n3 - hex(num)
    
    print(n1,n2,n3)
    ```

    ```python
    # 二进制转化成十进制
    v1 = '0b1101'
    result = int(v1,base=2)
    print(result)
    
    # 八进制转化成十进制
    v1 = '0o1101'
    result = int(v1,base=8)
    print(result)
    
    # 十六进制转化成十进制
    v1 = '0x1101'
    result = int(v1,base=16)
    print(result)                #  注意,转换时0b   0o   0x可以不用加
    ```

#### 5.4.3 高级函数(必考)

- map()  循环每个元素（第二个参数），然后让每个元素执行函数（第一个参数），将每个函数执行的结果保存到新的列表中，并返回。

  第一个参数 function 以参数序列中的每一个元素调用 function 函数，返回包含每次 function 函数返回值的新列表。 

  ```python
  content = [11,22,33,44,55,66,77,88,99]
  t = map(lambda x: x*x,content)
  print(list(t))  #列表元素的个数是相同的
  ```

- filter(筛选/过滤)**filter()** 函数用于过滤序列，过滤掉不符合条件的元素，返回由符合条件元素组成的新列表。

  该接收两个参数，第一个为函数，第二个为序列，序列的每个元素作为参数传递给函数进行判，然后返回 True 或 False，最后将返回 True 的元素放到新列表中。

  ```python
  content = [11,0,33,55,66,77,88,99]
  t = filter(lambda x: x < 50,content)
  print(list(t))  #列表元素的个数是相同的
  ```

- reduce()**reduce()** 函数会对参数序列中元素进行累积。

  函数将一个数据集合（链表，元组等）中的所有数据进行下列操作：用传给 reduce 中的函数 function（有两个参数）先对集合中的第 1、2 个元素进行操作，得到的结果再与第三个数据用 function 函数运算，最后得到一个结果。

  ```python
  import functools
  
  t = [3,2,3,4,5,6,8,9]
  v = functools.reduce(lambda x,y: x ** y,t)
  print(v)
  ```

  **注意**: 在py3中,reduce的函数移到了functools模组,要用这个功能,必须 import functools

### 5.5  装饰器(重点)

装饰器：在不改变原函数内部代码的基础上，在函数执行之前和之后自动执行某个功能。 

#### 5.5.1装饰器

```python
示例:
# #########################################
def func(arg):
    def inner():
        arg()
    return inner

def f1():
    print(123)

v1 = func(f1)
v1()
# ###########################################
def func(arg):
    def inner():
        arg()
    return inner

def f1():
    print(123)
    return 666

v1 = func(f1)   # 运行func(),创建一块空间,定义 inner函数.返回值 inner
result = v1() # 执行inner函数 / f1含函数 -> 123 
print(result) # None
# ###########################################
def func(arg):
    def inner():
        return arg()
    return inner

def f1():
    print(123)
    return 666

v1 = func(f1)
result = v1() # 执行inner函数 / f1含函数 -> 123
print(result) # 666
```

```python
示例:
# #########################################
def func(arg):
    def inner():
        arg()
    return inner

def f1():
    print(123)

v1 = func(f1)
v1()
# ###########################################
def func(arg):
    def inner():
        arg()
    return inner

def f1():
    print(123)
    return 666

v1 = func(f1)   # 运行func(),创建一块空间,定义 inner函数.返回值 inner
result = v1() # 执行inner函数 / f1含函数 -> 123 
print(result) # None
# ###########################################
def func(arg):
    def inner():
        return arg()
    return inner

def f1():
    print(123)
    return 666

v1 = func(f1)
result = v1() # 执行inner函数 / f1含函数 -> 123
print(result) # 666

```

#### 5.5.2装饰器的本质

```python
def func(arg):
    def inner():
        print('before')  
        v = arg()
        print('after')
        return v 
    return inner 

def index():
    print('123')
    return '666'

index = func(index)  # --->指向inner函数.
index()    # ---> 运行inner函数-->运行打印'before'/原index函数()/运行打印'after'

```

1. 装饰器的格式

   ```python
   def func(arg):
       def inner():
           v = arg()
           return v 
       return inner 
   
   # 第一步：执行func函数并将下面的函数参数传递，相当于：func(index)
   # 第二步：将func的返回值重新赋值给下面的函数名。 index = func(index)
   @func    #  @func等价于上面2步
   def index():
       print(123)
       return 666
   
   print(index)
   
   ```

2. 应用

   ```python
   # 计算函数执行时间
   def wrapper(func):
       def inner():
           start_time = time.time()
           v = func()
           end_time = time.time()
           print(end_time-start_time)
           return v
       return inner
   
   @wrapper
   def func1():
       print(123454987+865484)
       
   func1()   # --->自动启动装饰器
       
   
   ```

3. 总结

   - 目的：在不改变原函数的基础上，再函数执行前后自定义功能。 

   - 编写装饰器 和应用

     ```python
     # 装饰器的编写
     def x(func):
         def y():
             # 前
             ret = func()
             # 后
             return ret 
        	return y 
     
     # 装饰器的应用
     @x
     def index():
         return 10
     
     @x
     def manage():
         pass
     
     # 执行函数，自动触发装饰器了
     v = index()
     print(v)
     
     
     @xx  # index = xx(index)
     def index():
         pass
     index()
     ```

   - 应用场景：想要为函数扩展功能时，可以选择用装饰器。

4. 重点内容装饰器(重点)

   ```python
   def 外层函数(参数): 
       def 内层函数(*args,**kwargs):   # -->无论被装饰函数有多少个参数,都能用万能参数传入.
           return 参数(*args,**kwargs)
       return 内层函数
   
   @外层函数
   def index():
       pass
   
   index()    # --->自动调用装饰器
   ```

5. 装饰器建议写法

   ```python
   def x1(func):
       def inner(*args,**kwargs):
           data = func(*args,**kwargs)
           return data
       return inner 
   
   def x1(func):
       def inner(*args,**kwargs):
           print('调用原函数之前')
           data = func(*args,**kwargs) # 执行原函数并获取返回值
           print('调用员函数之后')
           return data
       return inner 
   
   @x1
   def index():
       print(123)
       
   index()  
   ```

#### 5.5.3带参数的装饰器

1. 基本格式

   ```python
   def wrapper(func):
       def inner(*args,**kwargs):
           
           v = func()
           
           return v
       return i
   ```

2. 装饰器加上参数(外层函数必须以函数名作为参数,要给装饰器加上参数,在外层再套一层函数)

   ```python
   def super_wrapper(counter):  # counter 装饰器参数
       print(123)
       def wrapper(func):
           print(456)
           def inner(*args, **kwargs):  # 参数统一的目的是为了给原来的index函数传参
               data = []
               for i in range(counter):
                   v = func(*args, **kwargs)  # # 参数统一的目的是为了给原来的index函数传参
                   data.append(v)
               return data
           return inner
       return wrapper
   
   @super_wrapper(9):    # 这时已经打印 123   456   # # func = 原来的index函数u
   											# index = inner
                                     # 已经运行super_wrapper函数和wrapper函数
           
   def index(i):
       print(i)
       return i+1000
   
   print(index(456))          # 调用index函数(带装饰器)
   ```

   ```python
   # ################## 普通装饰器 #####################
   def wrapper(func):
       def inner(*args,**kwargs):
           print('调用原函数之前')
           data = func(*args,**kwargs) # 执行原函数并获取返回值
           print('调用员函数之后')
           return data
       return inner 
   
   @wrapper
   def index():
       pass
   
   # ################## 带参数装饰器 #####################
   def x(counter):
       def wrapper(func):
           def inner(*args,**kwargs):
               data = func(*args,**kwargs) # 执行原函数并获取返回值
               return data
           return inner 
   	return wrapper 
   
   @x(9)
   def index():
       pass
   
   ```

   - 基本装饰器更重要8成
   - 带参数的装饰器2成
   - 装饰器本质上就是一个python函数,它可以让其他函数在不需要任何代码变动的前提下增加额外的功能 ，装饰器的返回值也是一个函数对象，她有很多的应用场景，比如：插入日志，事物处理，缓存，权限装饰器就是为已经存在的对象 添加额外功能 

### 5.6 推导式

#### 5.6.1列表推导式

- 目的: 为了方便生成一个列表

  ```python
  #####基本格式#####
  v1 = [i for i in range(9)]    # 创建列表10个元素
  v2 = [i for i in range(9) if i > 5]   #对每个i,判断if i > 5,为真则添加到列表中
  v3 = [99 if i > 55 else 66 for i in range(9)]   # 三木运算
  """
  v1 = [i for i in 可迭代对象 ]
  v2 = [i for i in 可迭代对象 if 条件 ] # 条件为true才进行append
  """
  ######函数######
  
  def func():
      return i
  v6 = [func for i in range(10)]  # 10个func组成的列表,内存地址一致.
  result = v6[5]()     # 运行报错,提示 i 没有被定义,
  
  v7 = [lambda :i for i in range(10)]  # 10个lambda函数组成的列表,内存地址不一致,有10个
  result = v7[5]()   # 运行不报错,输出result 为 9
  
  ## 第一个func和列表生成器函数作用域都在第一级,找不到 i 
  ## 第二个lambda上级是列表生成器函数作用域
  
  
  ###面试题
  v8 = [lambda x:x*i for i in range(10)] # 新浪微博面试题
  # 1.请问 v8 是什么？    10个lambda函数,内存地址不一样,构建的列表
  # 2.请问 v8[0](2) 的结果是什么？    i 在列表生成式函数作用域里面,lambda是奇子集,i = 9,输出18
  
  ### 面试题
  def num():
      return [lambda x:i*x for i in range(4)]
  # num() -> [函数,函数,函数,函数]
  print([ m(2) for m in num() ]) # [6,6,6,6]
  
  ```

  

#### 5.6.2集合推导式

- 和列表推导式类似,这不再赘述.

#### 5.6.3字典推导式

```python
v1 = { 'k'+str(i):i for i in range(10) }  # 和列表类似,中间有:区分键和值.
```

### 5.7 迭代器

#### 5.7.1自己不用写迭代器,只.

```python
# 一一展示列表中的元素
# 1.for 循环    2. while + 索引 + 计数器
# 使用迭代器--->对 某种对象(str/list/tuple/dict/set类创建的对象)-可迭代对象中的元素进行逐一获取，表象：具有`__next__`方法且每次调用都获取可迭代对象中的元素（从前到后一个一个获取）。
# 可被for 循环的---> 可迭代对象


#***************************************************************#
v1 = [1,2,3,4]
v2 = iter(v1)    # --->v2是一个迭代器
#或者  v2 = v1.__iter__()

# 迭代器想要获取每个值:-->反复调用
while 1 :
	val = v1.__next__() 
    print(val)
 
##### *********   ######
v3 = "alex"
v4 = iter(v1)
while True:
    try:
        val = v2.__next__()
        print(val)
    except Exception as e:
        break

        
        
# 直到报错：StopIteration错误，表示已经迭代完毕。

```

1. 判定是否是迭代器  --> 内部是否有`__next__方法` 

2. for 循环---->为这简便有了for 循环

   ```python
   v1 = [11,22,33,44]
   
   # 1.内部会将v1转换成迭代器
   # 2.内部反复执行 迭代器.__next__()
   # 3.取完不报错
   
   for item in v1:
       print(item)
   ```

#### 5.7.2可迭代对象

- 内部具有 `__iter__()` 方法且返回一个迭代器。（*）

  ```python
  v1 = [11,22,33,44]
  result = v1.__iter__()
  ```

- 可以被for循环

#### 5.7.3 小结

- 迭代器，对可迭代对象中的元素进行逐一获取，迭代器对象的内部都有一个 __next__方法，用于以一个个获取数据。
- 可迭代对象，可以被for循环且此类对象中都有 __iter__方法且要返回一个迭代器（生成器）。

### 5.8 生成器

生成器(变异函数---->特殊的迭代器)

生成器，函数内部有yield则就是生成器函数，调用函数则返回一个生成器，循环生成器时，则函数内部代码才会执行。

1. 函数样式

   ```python
   # 函数
   def func():
       return 123
   func()
   
   ```

2. 生成器

   ```python
   # 生成器函数（内部是否包含yield）---->只要有yeild就是生成器,不论是否有return
   def func():
       print('F1')
       yield 1
       print('F2')
       yield 2
       print('F3')
       yield 100
       print('F4')
   # 函数内部代码不会执行，返回一个 生成器对象 。
   v1 = func()
   # 生成器是可以被for循环，一旦开始循环那么函数内部代码就会开始执行。
   for item in v1:
       print(item)
   ```

- 总结：函数中如果存在yield，那么该函数就是一个生成器函数，调用生成器函数会返回一个生成器，生成器只有被for循环时，生成器函数内部的代码才会执行，每次循环都会获取yield返回的值

  ```python
  def func():
      count = 1
      while 1 :
          yield count
          count += 1
          if count == 100 :
              return
  val = func()     # --->调用生成器函数 ---->返回值时一个生成器 --->内部代码没有执行
  for item in val :
      print(item)  # 此时才执行生成器内部代码,并yield.就冻结,直到下次再继续运行.
  ```

  - 示例 读文件

    ```python
    def func():
        """
            分批去读取文件中的内容，将文件的内容返回给调用者。
        """
        cursor = 0
        while 1 :
            f =  open(r'D:\Python学习\python\s21day16\goods.txt','r',encoding='utf-8-sig')
            f.seek(cursor)
            data_list = []                       # 光标移到最前
            for i in range(10):
                line = f.readline()             # 一次读一行
                if not line:
                    return
                data_list.append(line)
            cursor = f.tell()             #获取光标
            f.close()                             # 关闭与redis的连接
            for row in data_list:
                yield  row
    
    
    for itemm in func():
        p = input('qingsr123456498')
        print(itemm)
    ```

#### 5.8.1 range和 xrange区别

- range 函数说明：range([start,] stop[, step])，根据start与stop指定的范围以及step设定的步长，生成一个序列。 
- xrange 函数说明：用法与range完全相同，所不同的是生成的不是一个数组，而是一个生成器。 
- py3中只有range,是生成器,

#### 5.8.2 生成器推导式







生成器有什么好处呢？就是不会一下子在内存中生成太多数据 

```python
# def func():
#     for i in range(10):
#         yield i
# v2 = func()
v2 = (i for i in range(10)) # 生成器推导式，创建了一个生成器，内部循环为执行。


# 没有元组推导式,加括号是生成器推导式

def my_range(counter):
    for i in range(counter):
        yield i
# 生成器函数

# # 面试题：请比较 [i for i in range(10)] 和 (i for i in range(10)) 的区别？
# 前者是列表推导式,直接在内存中生成列表[1-10],后者是生成器推导式,内部循环不执行,只有for 循环才执行.
```

```python
# 示例一
# def func():
#     result = []
#     for i in range(10):
#         result.append(i)
#     return result
# v1 = func()
# for item in v1:
#    print(item)              # 列表推导式

# 示例二
# def func():
#     for i in range(10):
#         def f():
#             return i
#         yield f              # 生成器函数
#
# v1 = func()                  # 生成器,内部代码不执行
# for item in v1:
#     print(item())             #for 循环才执行


import time


def tail(filename):
    f = open(filename)
    f.seek(0, 2) #从文件末尾算起
    while True:
        line = f.readline()  # 读取文件中新的文本行
        if not line:
            time.sleep(0.1)
            continue
        yield line

tail_g = tail('tmp')
for line in tail_g:
    print(line) 
    #生成器监听文件输入
```

#### 5.8.3 生成器函数的send方法

```
def generator():
    print(123)
    content = yield 1
    print('=======',content)
    print(456)
    yield2

g = generator()
ret = g.__next__()
print('***',ret)
ret = g.send('hello')   #send的效果和next一样
print('***',ret)

#send 获取下一个值的效果和next基本一致
#只是在获取下一个值的时候，给上一yield的位置传递一个数据
#使用send的注意事项
    # 第一次使用生成器的时候 是用next获取下一个值
    # 最后一个yield不能接受外部的值
```

## 第六章 模块

### 6.1前言

1. 什么是模块
   - py文件 写好了的 对程序员直接提供某方面功能的文件
   - import
   - rom import 名字
2. 什么是包
   - 文件夹 存储了多个py文件的文件夹
   - 如果导入的是一个包，这个包里的模块默认是不能用的
   - 导入一个包相当于执行`__init__`py文件中的内容

### 6.1 模块初步(import)

```python
# import 模块名  # 调用py的内置模块,
```

#### 6.1.1 sys.模块

```python
### py  解释器相关的数据
import sys
sys.getrefcount()   # 获取一个值的引用次数,用得少,不重要
a = [11,22,33]
b = a
print(sys.getrefcount(a))

###sys.getrecursionlimit()
print(sys.getrecursionlimit())  # 1000次,python默认支持的递归数量.实际使用用个人的电脑配置有关

###sys.stdout.write  --> print    (进度)
import time
for i in range(100):
    msg = '进度条跑到%s%%\r' %i    # \r 光标回到当前行的起始位置(不是覆盖.,是清空)
    print(msg , end = '')
    time.sleep(0.5)  # 暂停0.5s后再运行
    
### sys.agrv  #### 获取用户执行脚本时，传入的参数。得到的是一个列表,第一个元素是脚本文件.py
##  Sys.argv[ ]其实就是一个列表，里边的项为用户输入的参数，关键就是要明白这参数是从程序外部输入的，而非代码本身的什么地方，要想看到它的效果就应该将程序保存了，从外部来运行程序并给出参数。
```

```python
#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
让用户执行脚本传入要删除的文件路径，在内部帮助用将目录删除。
C:\Python36\python36.exe D:/code/s21day14/7.模块传参.py D:/test
C:\Python36\python36.exe D:/code/s21day14/7.模块传参.py

"""
import sys

# 获取用户执行脚本时，传入的参数。
# C:\Python36\python36.exe D:/code/s21day14/7.模块传参.py D:/test
# sys.argv = [D:/code/s21day14/7.模块传参.py, D:/test]
path = sys.argv[1]

# 删除目录
import shutil
shutil.rmtree(path)
```

1. sys.argv

2. sys.path    # 默认Python去导入模块时，会按照sys.path中的路径挨个查找。 ---> 本质是列表

3. sys.exit( 0 )    是正常退出 sys.exit( 1) 异常退出

4. sys.modules

   ```python
   sys.modules # 存储了当前程序中用到的所有模块，反射本文件中的内容
   sys.modules[__name__]---->指向本文件
   # '__main__': <module '__main__' from 'D:/code/day24/1.内容回顾.py'>
   ```

#### 6.1.2 os 模块

和操作系统相关的数据。

- os.path.exists(path)      ， 如果path存在，返回True；如果path不存在，返回False

- os.stat('20190409_192149.mp4').st_size  ， 获取文件大小

- os.path.getsize()-->获取文件大小

- os.path.abspath()   ， 获取一个文件的绝对路径

  ```python
  import os
  # 1. 读取文件大小（字节）
  file_size = os.stat('20190409_192149.mp4').st_size
  # 2.一点一点的读取文件
  read_size = with open('20190409_192149.mp4',mode='rb') as f1,open('a.mp4',mode='wb') as f2:    
      while read_size < file_size:        
          chunk = f1.read(1024) # 每次最多去读取1024字节        
          f2.write(chunk)        
          read_size += len(chunk)        
          val = int(read_size / file_size * 100)        
          print('%s%%\r' %val ,end='')
  ```

  

```python
import os

os.path.exists(path)      #如果path存在，返回True；如果path不存在，返回False.文件路径

os.stat('20190409_192149.mp4').st_size   #  获取文件大小(字节大小)

os.path.abspath()   #  获取一个文件的绝对路径

os.path.dirname # 获取路径的上级目录(文件夹)


```

- **os.path.join ，路径的拼接** 

  ```python
  import os
  path = "D:\code\s21day14" # user/index/inx/fasd/
  v = 'n.txt'     
  
  result = os.path.join(path,v)
  print(result)        # # D:\code\s21day14\n.txt
  result = os.path.join(path,'n1','n2','n3')
  print(result)      # D:\code\s21day14\n1\n2\n3
  ```

- os.listdir ， 查看一个目录下所有的文件【第一层】

  ```python
  import os
  
  result = os.listdir(r'D:\code\s21day14')     # r,在字符创=串前面,转义符,把\n\t\r当做字符串.
  for path in result:
      print(path)
  ```

- **os.walk ， 查看一个目录下所有的文件【所有层】**

  ```python
  import os
  
  result = os.walk(r'D:\code\s21day14')
  for a,b,c in result:
      # a,正在查看的目录 b,此目录下的文件夹  c,此目录下的文件
      for item in c:
          path = os.path.join(a,item)
          print(path)   # 还有其他很多用途
          
  获取文件夹大小，即遍历文件夹，将所有文件大小加和。遍历文件夹使用os.walk函数
  
  　　os.walk()可以得到一个三元tupple(dirpath, dirnames, filenames)，
  
  　　1、第一个为起始路径，
  
  　　2、第二个为起始路径下的文件夹，
  
  　　3、第三个是起始路径下的文件。
  
  　　其中dirpath是一个string，代表目录的路径，dirnames是一个list，包含了dirpath下所有子目录的名字。filenames是一个list，包含了非目录文件的名字。这些名字不包含路径信息，如果需要得到全路径，需要使用os.path.join(dirpath, name).
  
  复制代码
  import os
  from os.path import join, getsize
  
  
  def getdirsize(dir):
      size = 0
      for root, dirs, files in os.walk(dir):
          size += sum([getsize(join(root, name)) for name in files])
      return size
  
  
  if __name__ == '__main__':
      filesize = getdirsize('E:\chengd')
      print('There are %.3f' % (filesize / 1024 / 1024 ), 'Mbytes in E:\\chengd')
  
  执行结果：
  There are 4747.763 Mbytes in E:\chengd
  ```

- 转义符的使用

  ```python
  v1 = r"D:\code\s21day14\n1.mp4"  (推荐)
  print(v1)
  
  
  v2 = "D:\\code\\s21day14\\n1.mp4"
  print(v2)
  ```

- os.makedirs,创建目录和子目录--->比makedir 好用,只能创建一层目录

  ```python
  import os
  file_path = r'E:\小莫的黄片库\绝对不能偷看\请输入密码\****.MP4'
  
  file_folder = os.path.dirname(file_path)
  if not os.path.exists(file_folder):
      os.makedirs(file_folder)
  
  with open(file_path,mode='w',encoding='utf-8') as f:
      f.write('asdf')
  
  ```

- os.rename  重命名

  ```python
  import os
  os.rename('db','sb')    # 原文件 ,现文件
  ```

  

#### 6.1.3 shutil 模块

- 删除目录

  ```python
  import shutil
  shutil.rmtree(path)
  ```

- 

  ```python
  import shutil
  
  
  ##删除目录(只能是文件夹)
  shutil.rmtree('test')
  
  ## 重命名
  shutil.move(old,new)  ,移动文件
  
  ##压缩文件
  shutil.make_archive('压缩后文件名可以加文件路径','压缩格式zip等等','被压缩的文件/文件夹')
  
  shutil.make_archive('zzh','zip','D:\code\s21day16\lizhong')
  
  ##解压文件
  shutil.unpack_archive('要解压的文件夹',extract_dir=r'D:\code\xxxxxx\xxxx',格式:'zip')
   #                                    解压的路径
  shutil.unpack_archive('zzh.zip',extract_dir=r'D:\code\xxxxxx\xxxx',format='zip')
  
  ```

  举例

  ```python
  import shutil
  
  shutil.rmtree('4567')     # 删除目录(只能是文件夹)
  
  shutil.move(前文件名,后文件名)   # 重命名/移动,看后一个文件是文件夹还是文件名
  
  shutil.make_archive(r'E:\159','zip','456')   # 把py文件目录的456压缩到E:\159,即E盘的根目录下159.zip
  
  shutil.unpack_archive('159.zip',r'F:\159\158','zip')  # 把159.zip解压文件到F:\159\158,解压格式是zip.
  ```

#### 6.1.4 json 模块

1. 用法

   - son的本质(为着各程序间信息的传递而规定的一种通用格式)

     1. 本质是一个字符串(看起来像[],{},int/ bool )
     2. 最外层必须是list/dict
     3. 如果包含字符串,必须双引号--->其他程序字符串只识别双引号

     用法

     ```python
     import json
     # 序列化，将python的值转换为json格式的字符串。
     # v = [12,3,4,{'k1':'v1'},True,'asdf']
     # v1 = json.dumps(v)
     # print(v1)
     json.dumps()    ---->返回值是json格式--->字符串
     
     
     # 反序列化，将json格式的字符串转换成python的数据类型
     # v2 = '["alex",123]'
     # print(type(v2))
     # v3 = json.loads(v2)
     # print(v3,type(v3))
     json.loads()   ---->返回值是python能识别的格式,外层一定是[]或{}
     
     ```

     python与json的对照表

     ```python
         +-------------------+---------------+
         | Python            | JSON          |
         +===================+===============+
         | dict              | object        |
         +-------------------+---------------+
         | list, tuple       | array         |
         +-------------------+---------------+
         | str               | string        |
         +-------------------+---------------+
         | int, float        | number        |
         +-------------------+---------------+
         | True              | true          |
         +-------------------+---------------+
         | False             | false         |
         +-------------------+---------------+
         | None              | null          |
         +-------------------+---------------+
     ```

2. - dumps  把python数据转换为json型的字符串  序列化

   - loads 把json字符串转换为python能够识别的数据类型  反序列还

   - 注意 :若字典或列表中有中文,序列化是想要保留中文显示

     ```python
     v = {'k1':'alex','k2':'李杰'}   #e nsure_ascii=False
     
     import json
     val = json.dumps(v,ensure_ascii=False)
     print(val)
     
     ```

   - dump(内容,文件句柄)   有2个参数

     ```python
     import json
     
     v = {'k1':'alex','k2':'李杰'}
     
     f = open('x.txt',mode='w',encoding='utf-8')
     val = json.dump(v,f)
     print(val)
     f.close()
     
     ## Json.dump()这个方法：它在底层做了两件事，一件事是将对象（列表）转换为字符串，第二件事是转换## 成功以后，将转换后的数据写入到文件中。
     
     ```

   - load

     ```python
     import json
     
     v = {'k1':'alex','k2':'李杰'}
     
     f = open('x.txt',mode='r',encoding='utf-8')
     
     data = json.load(f)
     f.close()
     
     print(data,type(data))
     
     # 做了两件事，一件事：先来读取文件里的内容，第二件事是：将读取出来的内容进行数据类型的转换。
     ```

#### 6.1.5 pickle 模块

- json，优点：所有语言通用；缺点：只能序列化基本的数据类型 list/dict/int...   

- pickle，优点：python中所有的东西都能被他序列化（除了socket对象）；缺点：序列化的内容只有python认识。

  ```python
  import pickle
  
  ######################   dumps / loads  #######################3
  v = {1,2,3,4}
  val = pickle.dumps(v)
  print(val)
  data = pickle.loads(val)
  print(data,type(data))
  ########################  dump  / load  用法和json类似,这里不再赘述####
  
  ######################################################
  # 序列化 把其他数据类型转换成 str/bytes类型-->dump/dumps
  # 反序列化 str/bytes类型 转换回去         -->load/loads
     所有编程语言  json 所有的语言都支持
      # json格式 ：
          # 1.所有的字符串都是双引号
          # 2.最外层只能是列表或者字典
          # 3.只支持 int float str list dict bool
          # 4.存在字典字典的key只能是str
          # 5.不能连续load多次
      # pickle 只支持python语言
          #  1.几乎所有的数据类型都可以写到文件中
          #  2.支持连续load多次
  ```

#### 6.1.6time 和 datetime 模块

1. time模块

   time.time()   时间戳,显示的是1970.1.1.凌晨0.00到现在经过的秒数,得到的是float数据类型

   time.sleep()   程序运行等待的秒数

   time.timezone   :  属 性time.timezone是当地时区（未启动夏令时）距离格林威治的偏移秒数（>0，美洲;<=0大部分欧洲，亚洲，非洲） ,和电脑设置的逝去==时区有关

2. datetime 模块

   ```python
   #!/usr/bin/env python
   # -*- coding:utf-8 -*-
   import time
   from datetime import datetime,timezone,timedelta
   
   # ######################## 获取datetime格式时间 ##############################
   """
   v1 = datetime.now() # 当前本地时间
   print(v1)
   tz = timezone(timedelta(hours=7)) # 当前东7区时间
   v2 = datetime.now(tz)
   print(v2)
   v3 = datetime.utcnow() # 当前UTC时间
   print(v3)
   """
   
   # ######################## 把datetime格式转换成字符串 ##############################
   # v1 = datetime.now()
   # print(v1,type(v1))
   # val = v1.strftime("%Y-%m-%d %H:%M:%S")
   # print(val)
   
   # ######################## 字符串转成datetime ##############################
   # v1 = datetime.strptime('2011-11-11','%Y-%m-%d')
   # print(v1,type(v1))
   
   # ######################## datetime时间的加减 ##############################
   # v1 = datetime.strptime('2011-11-11','%Y-%m-%d')
   # v2 = v1 - timedelta(days=140)
   # date = v2.strftime('%Y-%m-%d')
   # print(date)
   
   # ######################## 时间戳和datetime关系 ##############################
   # ctime = time.time()
   # print(ctime)
   # v1 = datetime.fromtimestamp(ctime)
   # print(v1)
   
   # v1 = datetime.now()
   # val = v1.timestamp()
   # print(val)
   ```

#### 6.1.7 collections 模块

```python
# 调用模块,字典有序
from collections import OrderedDict

info  = OrderedDict()

# 有序字典
OrderDict()

# 字典的另一种写法
dict([(1,2),(3,4),(5,6)])   # dict化 列表套元组

# namedtuple,顾名思义,可命名元组
Course = namedtuple('Course',['name','price','teacher'])
python = Course('python',19800,'alex')
print(python)
print(python.name)
print(python.price)
# 创建一个类，这个类没有方法，所有属性的值都不能修改

# deque 双端队列

# defaultDict 默认字典，可以给字典的value设置一个默认值


```

#### 6.1.8 hashlib 模块 摘要算法

1. 将密码加密

   ```python
   # import 模块名  # 调用py的内置模块,
   # PY调用hashlib模块对用户的密码进行加密,hashlib模块有很多种加密方式.这里选用MD5加密
   
   import hashlib
   
   def md5(arg):  # 加密函数   
       hash  = hashlib.md5('dengyxiin'.encode('utf-8'))    # 加盐,避免给黑客撞库得到密码.
       hash.update(arg.encode('utf-8'))
       return hash.hexdigest()     # 返回加密的md5,<----->t = hash.hexdigest();return t
   
   def register(user,pwd):  # 注册函数,参数为用户名和密码
       with open('data.txt','a',encoding='utf-8') as file:    #  追加模式写入文件
           file.write(user+'|'+md5(pwd)+'\n')  #用户名和密码用竖杠隔开,方便以后找寻
   
   def login(user,pwd):  # 登录函数
       with open('data.txt','r',encoding='utf-8') as file:    # 以只读模式打开文件,遍历文件,
           for line in file:
               line = line.strip()
               line = line.split('|')
               if user == line[0] and md5(pwd) == line[1]:    #遍历存储登录数据的文件,看能否比对的上.
                   return  True
   
   ```

2. 密文验证

3. 验证文件的一致性hashlib

   ```python
   import hashlib
   hash = hashib.md5()
   hash.update()   # 无数次update,把大文件分割update进去即可.
   t = hash.hexdigest()
   print(t)
   
   
   # md5.update('hello'.encode())
   # md5.update(',wusir'.encode())
   # 46879507bf541e351209c0cd56ac160e
   # 46879507bf541e351209c0cd56ac160e  分批次update进去,最终结果相同
   ```

#### 6.1.9 random模块(应用)

```python
import random
random.unifom(1,5)  # 取1-5间的小数
val = random.choice(序列)   # -->从序列中随机抽取一个元素
val22 = ramdom.sample(序列,n)   # -->从序列中随机抽取n个元素
shuffle(序列)   # 打乱,洗牌

import random     # 调用随机数函数

def get_random_code(length=6):
    data = []
    for i in range(length):
        v = random.randint(65,90)  # 65-90对应的是ABC-----Z
        data.append(chr(v)
    return  ''.join(data)

code = get_random_code()
print(code)

```

#### 6.1.10 getpass 模块

```python
#  让密码在键入的时候隐藏
import getpass

pwd = getpass.getpass('请输入密码：')
if pwd == '123456789':
    print('输入正确')

    
import time
print(time.time())
time.sleep(2)    # --> 睡 2秒

```













### 6.2 模块分类

#### 6.2.1内置模块

1. random

2. hashlib

3. getpass 

4. time

5. copy

6. os

   - os.makedirs
   - os.path.join
   - rename    重命名
   - os.path.dirname
   - os.path.abspath
   - os.path.exists
   - os.stat('文件路径')
   - os.listdir
   - os.walk

7. sys

   - sys.argv
   - sys.path

8. shutil

9. json

   - json 模块

     json的本质(为着各程序间信息的传递而规定的一种通用格式)

     1. 本质是一个字符串(看起来像[],{},int/ bool )
     2. 最外层必须是list/dict
     3. 如果包含字符串,必须双引号--->其他程序字符串只识别双引号

     用法

     ```python
     import json
     # 序列化，将python的值转换为json格式的字符串。
     # v = [12,3,4,{'k1':'v1'},True,'asdf']
     # v1 = json.dumps(v)
     # print(v1)
     json.dumps()    ---->返回值是json格式--->字符串
     
     
     # 反序列化，将json格式的字符串转换成python的数据类型
     # v2 = '["alex",123]'
     # print(type(v2))
     # v3 = json.loads(v2)
     # print(v3,type(v3))
     json.loads()   ---->返回值是python能识别的格式,外层一定是[]或{}
     
     ```

     python与json的对照表

     ```python
         +-------------------+---------------+
         | Python            | JSON          |
         +===================+===============+
         | dict              | object        |
         +-------------------+---------------+
         | list, tuple       | array         |
         +-------------------+---------------+
         | str               | string        |
         +-------------------+---------------+
         | int, float        | number        |
         +-------------------+---------------+
         | True              | true          |
         +-------------------+---------------+
         | False             | false         |
         +-------------------+---------------+
         | None              | null          |
         +-------------------+---------------+
     ```

10. 内置模块补充

    - sys

      1. 

      

#### 6.2.2第三方模块

1. 下载和安装

   pip的使用

   - ```python
     # 把pip.exe 所在的目录添加到环境变量中。
     
     pip install 要安装的模块名称  # pip install requests
     ```

   - python36  -m pip install --upgrade pip 语句的使用

   - python3 -m pip install -U --force-reinstall pip   若pip更新失败,则如此

   - 网络不好一直抱超时错误的话可以换一个源，粘贴下边代码运行
     pip3 install xlrd -i http://pypi.douban.com/simple/ —trusted-host pypi.douban.com

     #### 注意 : 这里的pip  /  pip3     都是自己定义的环境变量

   - 安装完成后，如果导入不成功。

     - 重启pycharm。
     - 安装错了。

2. 你了解的第三方模块：

   - xlrd
   - requests

#### 6.2.3自定义模块

1. 自定义模块的导入

   xxxx.py  # 自定义模块,实现某功能

   ```python
   def func():
       print(123)  #实现打印123
   ```

   x2.py 

   ```python
   # 在x2.py中希望调用xxxx.py的函数
   ####1. xxxx.py 和  x2.py 在同一文件夹下
   import xxxx
   xxxx.func()
   
   ####2. 不在同一文件夹下
   import sys
   sys.path.append('xxxx.p文件的绝对路径')
   import xxxx
   xxxx.func()
   
   #####在cmd中运行x2.py文件
   python3 x2.py
   
   ```

2. 1. 模块和执行的py在同一目录,且需要调用很多函数

      ```python
      # 在1.py文件中调用xxxx.py
      import xxxx
      xxxx.func()     #调用函数
      xxxx.func2()    #调用函数2
      
      
      ```

   2. 包和执行的py在同一目录

      ```python
      from 包 import jd模块
      jd模块.func()    # 调用包内的jd模块的所有函数---
      
      from 包.jd模块 import func
      func()           #调用jd的func函数
      
      ################################
      from 包.jd模块 import func , show ,func2     #可以用都逗号连接
      func()
      show()
      func2() 
      
      
      #################################
      from 包.jd模块 import *  # 调用包.jd模块的全部功能
      
      ```

   3. - 模块和要执行的py文件在同一目录 且 需要 模块中的很多功能时，推荐用： import 模块
      - 其他推荐：from 模块 import 模块       模块.函数()
      - 其他推荐：from 模块.模块 import 函数   函数() 

   4. 注意 : 要从其他路径调用模块时的用法

      ```python
      import sys
      sys.path.append(r'D:\自建模块包\项目1模块')  # ---->把路径添加到python寻找模块时会去寻找的路径中,在调用模块
      import xxxx  # xxxx.py在'D:\自建模块包\项目1模块'目录下
      xxxx.func()
      
      # sys,path 本质是一个列表,是py解释器寻找模块时会去寻找的路径.path指定用于模块搜索路径的字符串列表。
      ```

   5. 模组调用

   6. import 

      - import 模块1                                    模块1.函数()
      - import 模块1.模块2.模块3                模块1.模块2.模块3.函数()

   7. from  xx import xxx

      - from 模块.模块 import 函数		函数()
      - from 模块.模块 import 函数 as  f         f()
      - from 模块.模块 import *                      函数1()    函数2()
      - from 模块 import  模块                        模块.函数()
      - from 模块 import  模块  as m              m.函数()
      - 特殊情况：
        - import 文件夹                        加载```__init__.py```
      - from 文件 import * 

   8. **注意：文件和文件夹的命名不能是导入的模块名称相同，否则就会直接在当前目录中查找。**



### 6.3 模块导入(重点)

#### 6.3.1多次导入模块只加载一次--->单例模式

```python
# 文件名  --> jd.py
print(123)


###################################
import jd # 第一次加载：会加载一遍jd中所有的内容。
import jd # 由已经加载过，就不在加载。   # 只打印一次123
print(456)


# 重加载 importilb.reload
import importlib
import jd
importlib.reload(jd)
print(456)   # 打印2次123  和456
```

**额外  :  **通过模块导入的特性也可以实现单例模式

```python
# jd.py
class Foo(object):
    pass

obj = Foo()

############################
# app.py
import jd
print(jd.obj)       # 单例模式
```

#### 6.3.2日志操作loggin模块

1. 作用:

   - 记录日志的
   - 用户 ：查看操作记录
   - 程序员 : 
     - 统计用
     - 故障排除
     - 记录错误,做代码优化

2. 基本应用( 默认的,无法更改编码,win编码不同乱码,不推荐)

   ```python
   import logging
   
   logging.basicConfig(
       filename='cmdb1.log',
       format='%(asctime)s - %(name)s - %(levelname)s -%(module)s:  %(message)s',
       datefmt='%Y-%m-%d %H:%M:%S %p',
       level=logging.ERROR
   )
    #  默认的,无法更改编码,win编码不同乱码,不推荐
       
   logging.error(msg,exc_info=True)  # exc_info=True,打印错误日志是把栈信息打印出来
   
   # 使用方便
   # 不能实现 编码问题；不能同时向文件和屏幕上输出
   # logging.debug,logging.warning
   ```

3. 日志处理的本质(Logger/FileHandler/Formatter)

   ```python
   #复杂
       # 创建一个logger对象
       # 创建一个文件操作符
       # 创建一个屏幕操作符
       # 创建一个格式
   
       # 给logger对象绑定 文件操作符
       # 给logger对象绑定 屏幕操作符
       # 给文件操作符 设定格式
       # 给屏幕操作符 设定格式
       
       # 用logger对象来操作
   import logging
   
   logger = logging.Logger('xxxxxx', level=logging.ERROR)
   
   logger = logging.Logger('邓益新',level=10)
   # logger = logging.getLogger('boss')    ,无法设置level
   fh = logging.FileHandler('log.log','a',encoding = 'utf-8')
   sh = logging.StreamHandler()
   
   
   formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s',datefmt = '%Y-%m-%d %H-%M-%S',)
   fh.setFormatter(formatter)
   sh.setFormatter(formatter)
   
   logger.addHandler(fh)
   logger.addHandler(sh)
   
   logger.warning('message')
   
   # file_handler2 = logging.FileHandler('x2.log', 'a', encoding='utf-8')
   # fmt2 = logging.Formatter(fmt="%(asctime)s:  %(message)s")
   # file_handler2.setFormatter(fmt2)
   
   
   logger.error('你好')
   
   ```

4. 推荐处理方式

   ```python
   import logging
   
   file_handler = logging.FileHandler(filename='x3.log', mode='a', encoding='utf-8',)
   logging.basicConfig(
       format='%(asctime)s - %(name)s - %(levelname)s -%(module)s:  %(message)s',
       datefmt='%Y-%m-%d %H:%M:%S %p',
       handlers=[file_handler,],
       level=logging.ERROR
   )
   
   logging.error('你好')
   
   ```

5. 日志处理 + 分割

   ```python
   import logging
   from logging import handlers
   
   file_handler = handlers.TimedRotatingFileHandler(filename='x3.log', when='s', interval=5, encoding='utf-8')
   logging.basicConfig(
       format='%(asctime)s - %(name)s - %(levelname)s -%(module)s:  %(message)s',
       datefmt='%Y-%m-%d %H:%M:%S %p',
       handlers=[file_handler,],
       level=logging.ERROR
   )
   
   logging.error('你好')
   
   # when = ''取 S M H D W,不区分大小写
   
   ```

6. 在应用日志时，如果想要保留异常的堆栈信息。

   ```python
   logging.error('你好',exc_info=True)  # 即可
   ```



## 第七章 面向对象

### 7.1.面向对象基本格式

#### 7.1.1 概念

    # 基础概念
        # 什么是类 : 具有相同方法和属性的一类事物
        # 什么是对象、实例 : 一个拥有具体属性值和动作的具体个体
        # 实例化 ：从一个类 得到一个具体对象的过程
    # 组合
        # 一个类的对象作为另一个类对象的实例变量  # 组合/嵌套
#### 7.1.1特性1-->封装

1. 遇到很多函数,给函数分个类和划分--> 封装

   ```python
   # ###### 定义类 ###### 
   class 类名:
       def 方法名(self,name):       # 分类的函数
           print(name)
           return 123
       def 方法名(self,name):
           print(name)
           return 123
       def 方法名(self,name):
           print(name)
           return 123
   # ###### 调用类中的方法 ###### 
   # 1.创建该类的对象
   obj = 类名()                  
   # 2.通过对象调用方法
   result = obj.方法名('alex')
   print(result)
   
   ```

2. 调用类中的方法

   ```python
   # 1.创建类的对象------>可以创建无数个,方法是通用的
   obj = 类名() 
   # 2.通过对象调用类的方法
   obj.方法名()    # ---->调用方法
   
   ```

3. 对象的作用

   存储一些值,以后方便自己调用

   ```python
   class Person:
   
       def __init__(self, n, a, g):  # ---> 给对象初始化,初始化方法（构造方法），给对象的内部做初始化。
           self.name = n
           self.age = a
           self.gender = g
   
       def show(self):
           msg = '我叫%s,今年%s岁,性别%s' %(self.name,self.age,self.gender)
           print(msg)
   
           
   # 类实例化对象,自动执行__init__方法.
   p1 = Person('邓益新',25,'男')
   
   p1.show()
   
   
   
   ################################################################33
   class Person:
           def show(self):
           msg = '我叫%s,今年%s岁,性别%s' %(self.name,self.age,self.gender)
           print(msg)
           
   p1 = Person()
   p1.name = '邓益新'    # -->在对象内部创建p1.name = '邓益新',
   p1.age = 25
   p1.gender = '男
   p1.show()            #执行方法,在对象内部找到值,打印
   ```

总结: def ```__init__方法``` 将数据封装到对象,方便使用

4. 总结

```python
'''
写代码时代码乱,函数多,
1. 将函数归类,相同性质的防放到一个类中
2.函数如果有可以反复使用的公共值,则可以发到对象中,方便使用
'''
```

```python
## 面向对像写法写

#      1. 循环让用户输入：用户名/密码/邮箱。 输入完成后再进行数据打印。#
class Person:

    def __init__(self, n, p, e):  # ---> 给对象初始化,初始化方法（构造方法），给对象的内部做初始化。
        self.name = n
        self.pwd = p
        self.email = e

    def show(self):
        msg = '我是%s,密码:%s,邮箱是:%s' %(self.name,self.pwd,self.email)
        print(msg)

user_list  = []
t = 0
while t < 3:
    n = input('输入名字:')
    p = input('输入密码:')
    e = input('输入邮箱:')
    p = Person(n,p,e)
    user_list.append(p)
    t += 1

for i in user_list:
    i.show()               # 直接调用方法打印
       
        
```

7.1.2 游戏开发相关

```python
# 2个阵营相互拼杀,定义2个类表示阵营
class Police:  # 特种兵阵营
    def __init__(self, name, hp):
        self.name = name
        self.hp = hp


    def dao(self,name):
        msg = '%s给了%s一刀' % (self.name, name)
        print(msg)


t1 = Police('小明', 1500)
t2 = Police('小红', 1000)
t3 = Police('小刚', 1800)


class Bandit:  # 匪徒阵营
    def __init__(self, nickname, hp):
        self.nickname = nickname
        self.hp = hp

    def quan(self,name):
        msg = ' %s给了%s一拳' % (self.name, name)
        print(msg)


f1 = Bandit('鲨鱼', 1800)
f2 = Bandit('恐龙', 1200)
f3 = Bandit('蜂窝', 1500)

t1.dao(f1.nickname)

```

#### 7.1. 2特性2-->继承特性

```python
#父类(基类)
class Father
    def f1():
        print('Father.f1')
        
# 子类(派生类)
class Son:
    def f2():
        print('Son.f2')
        
 obj = Son()
# 创建子类对象,可以继承父类的方法
# 执行对象.方法时，优先在自己的类中找，如果没有就是父类中找。
obj.f2()
obj.f1()

# 父类只有自己的方法,没有报错
```

- 当多个类有公共的方法时,可以放到基类中,避免重复编写.

- 继承关系的查找顺序

  ```python
  class Base:
      def f1(self):
          self.f2()
          print('base.f1')
  	def f2(self):
          print('base.f2')
  class Foo(Base):
      def f2(self):
          print('foo.f2')
          
  obj = Foo()
  obj.f1()
  
  
  
  ####- self 到底是谁？
  ####- self 是哪个类创建的，就从此类开始找，自己没有就找父类。
  ```

7.1.4总结

- self 是哪个类创建的，就从此类开始找，自己没有就找父类。-->指对象

#### 7.1.3特性3--> 多态特性（多种形态/多种类型）鸭子模型

```python
# Python
def func(arg):
    v = arg[-1] # arg.append(9)
    print(v)          # 支持多种数据类型

# java
def func(str arg):   # 必须指定数据类型
    v = arg[-1]
    print(v) 
```

#### 7.1.4重点:鸭子模型

**对于一个函数而言，Python对于参数的类型不会限制，那么传入参数时就可以是各种类型，在函数中如果有例如：arg.send方法，那么就是对于传入类型的一个限制（类型必须有send方法）。**
**这就是鸭子模型，类似于上述的函数我们认为只要能呱呱叫的就是鸭子（只有有send方法，就是我们要想的类型）**



#### 7.1.5对象的3大特性总结

面向对象的三大特性：封装/继承/多态 

- 三大特性

  1. 继承

  - 所有的查找名字（调用方法和属性）都是先找自己的，自己没有找父类

  - 如果自己和父类都有，希望自己和父类都调用，super()/指定类名直接调
    - 父类、基类、超类
    - 子类、派生类
      - 单继承 ：字类可以使用父类的方法
      - 多继承
        - 查找顺序
        - 深度优先
        - 广度优先
  - 多态
  - 一个类表现出来的多种状态 --> 多个类表现出相似的状态
  - 鸭子类型
    - vip_user  svip_user
    - list 和 tuple
  - 封装
  - 广义的封装 ：类中的成员
  - 狭义的封装 ：私有成员
    - __名字
    - 只能在类的内部使用，既不能在类的外部调用，也不能在子类中使用
    - _类名__名字

- 封装
  - 类封装函数,成为方法
  - 对象封装值,方便以后调用.
- 继承
  - 子类继承父类的方法
  - self到底是谁？
  - self是由于那个类创建，则找方法时候就从他开始找。

#### 7.1.6应用场景

- 函数（业务功能）比较多，可以使用面向对象来进行归类。
- 想要做数据封装（创建字典存储数据时，面向对象）。
- 游戏示例：创建一些角色并且根据角色需要再创建人物。

#### 7.1.7面试相关

1. 谈谈你了解的面向对象

   从面向对象的三大特性来说:

   - 封装:类把函数划分和分类  ,对象封装数据到对象中
   - 继承:多个类有公共的方法,就把这些方法拿出来,放到基类去,然后继承基类的方法,避免重复编写.
   - 多态 : 对于一个函数而言,python对于传入的参数的类型没有限制,但是如果函数中有send方法,就是对传入类型的限制.(必须有send方法)类似这样的就叫鸭子模型,,只要呱呱叫的都是鸭子,只要有send方法,就是我们想要的类型

2. 类是对象什么关系?

   对象是类的一个实例

3. self 是什么 ?

   self就是形式参数,对象调用方法时,python内部自动把对象传给这个参数.

4. 类成员

   - 类变量
   - 方法
   - 静态方法
   - 类方法
   - 属性

5. 对象成员

   - 实例变量

### 7.2 类的成员和对象成员

#### 7.2.1对象成员

1. 实例变量(普通字段)

   - 对象封装值到对象中,值在对象所在的内存地址中.

     ![1556095466074](D:/python/s21day20/day20%20notes.assets/1556095466074.png)

#### 7.2.2.类的成员

1. 类变量(静态字段)

   - 定义 : 写在类的下一级,和方法同级--->存在于类所在的内存中

   - 访问 : 类.类变量    /       对象.类变量

   - 1. 注意 :  对象.变量--->先去对象中找,找不到再去类中找,再去类的基类中找,找不到报错.

     2. 注意 : 修改或赋值只能改自己内部的封装的值

        ```python
        class Foo:
            xx = 123
        
        obj = Foo()
        print(obj.xx)
        print(Foo.xx)  # 123  123
        
        Foo.xx = 789
        print(obj.xx)
        print(Foo.xx)  # 789   ,789
        
        obj.xx = 2356   # obj对象封装值xx =2356,Foo.xx值不变
        print(obj.xx)  # 2356
        print(Foo.xx)   # 789
        ```

2. 方法(普通方法或者绑定方法)

   - 定义 : 至少要有self参数
   - 执行 : 先创建对象,然后对象调用方法--->对象.方法名()

3. 静态方法

   - 定义 : 

     - @staticmethod在方法上一行
     - 参数无限制(不需要self)

   - 执行

     - 执行静态方法   类.静态方法名()
     - 对象.静态方法名()    **不推荐容易混淆**

     ```python
     class Foo:
         @staticmethod
         def func():
             print(13465)
      
     # 调用方法
     
     Foo.func()
     
     v = Foo()
     v.func()
     
     ```

4. 类方法

   - 定义 : 
     - @classmethod装饰器在方法上一行
     - 必须要有一个参数cls --->指代类本身
   - 执行
     - 执行类方法   类.类方法名()
     - 对象.类方法名()    **不推荐容易混淆**

   ```python
   class Foo:
       @classmethod
       def func(cls,name):
           print(name)
           
   v = Foo()
   v.func('传入的参数name')
   
   Foo.func('54649+45')   # ----> 推荐使用
   ```

   总结 : 

   ```python
   # 问题： @classmethod和@staticmethod的区别？
   """
   一个是类方法一个静态方法。 
   定义：
   	类方法：用@classmethod做装饰器且至少有一个cls参数。
   	静态方法：用staticmethod做装饰器且参数无限制。
   调用：
   	类.方法直接调用。
   	对象.方法也可以调用。 
   """
   
   ```

5. 属性

   - 定义 : 

     - @property装饰器
     - 只有一个self参数

   - 执行

     - 对象.方法名   **注意:不要加括号**

     ```python
     class Foo:
         @property
         def func(self):
             return 4561
     
     obj = Foo()
     print(obj.func)
     ```

     作用 : 

     - 注意：属性存在意义是：访问属性时可以制造出和访问字段完全相同的假象
     - 属性由方法变种而来，如果Python中没有属性，方法完全可以代替其功能
     - Python的属性的功能是：属性内部进行一系列的逻辑计算，最终将计算结果返回。 

#### 7.2.3 总结

- 类成员 有 : 
  - 类变量(静态字段)
  - 方法(普通方法/绑定方法)
  - 静态方法@staticmethod,下一行跟方法,类.方法()调用
  - 类方法@classmethon , 下一行跟方法,类.方法()调用,必须要有一个cls参数
  - 属性,@property装饰,下一行跟方法,对象.方法,不加括号调用,是方法的变种
- 对象成员
  - 实例变量(普通字段)--->对象封装的值

#### 7.2.4嵌套问题

1. 函数 : 参数可以是任意类型

2. 字典 : 对象和类都可以做字典的键和值

3. 继承的查找关系

4. 对象和类的嵌套

   ```python
   # 对象做类方法的参数
   class Foo:
       pass
   
   class Base:
       def __init__(self):
           self.list1 = []
   
       def func(self,arg):
           self.list1.append(arg)
   
   obj = Foo()
   obj2 = Base()
   obj2.func(obj)
   
   # 对象封装 类到对象中
   class Foo:
       def f1(self,arg):
           self.func = arg
   
   class Base:
       def __init__(self,name,age):
           self.name = name
           self.age = age
   
   foo = Foo()
   foo.f1(Base)           # 封装类到对象中
   t = foo.func('邓益新',25)
   print(t.name,t.age)
   
   ```



#### 7.2.5成员修饰符(私有成员)

1. 区分公有和私有

2. 不加任何符号就是公有成员,任何地方都能调用

3. __加上符号,就是私有成员,只有自己才能方法,派生类也无法访问

   ```python
   class Foo:
       __x = 1 #私有成员
       def __init__(self,name):
           self.__name = name   # 私有成员
   
       def func(self):
           print(self.__name)
           
       def __func1(self):
           print('这是私有成员')  #只有经内部其他函数调用,外部对象调用出错
   
   obj = Foo('邓益新')
   # print(obj.__name)           # 报错,无法外部访问,只有通过内部func方法间接访问
   obj.func()
   
   ```

4. 强制访问私有成员(例外)

   ```python
   # 强制访问私有成员
   
   class Foo:
       def __init__(self,name):
           self.__x = name
   
   
   obj = Foo('alex')
   
   print(obj._Foo__x) # 强制访问私有实例变量--->在私有成员前加上'_类名__私有成员'
   ```

   

#### 7.2.6经典类和新式类

1. 新式类和经典类

   ```python
   class Foo:
       pass
   
   class Foo(object):
       pass
   
   # 在python3中这俩的写法是一样，因为所有的类默认都会继承object类，全部都是新式类。
   
   
   # 如果在python2中这样定义，则称其为：经典类
   class Foo:
       pass 
   # 如果在python2中这样定义，则称其为：新式类
   class Foo(object):
       pass 
   
   class Base(object):
       pass
   class Bar(Base):
       pass
   
   # 新式类和经典类
           # py2 继承object就是新式类
           #     默认是经典类
           # py3 都是新式类，默认继承object
   
           # 新式类
               # 继承object
               # 支持super
               # 多继承 广度优先C3算法
               # mro方法
           # 经典类
               # py2中不继承object
               # 没有super语法
               # 多继承 深度优先
               # 没有mro方法
   
   ```

2. 对象之间的组合---->列表的嵌套

   ```python
   class School(object):               # 创建类school
       def __init__(self,title,address):
           self.title = title
           self.address = address
           
   class ClassRoom(object):            # 创建教室类
       
       def __init__(self,name,school_object):
           self.name = name
           self.school = school_object
           
   s1 = School('北京','沙河')        # school_object封装title和address
   s2 = School('上海','浦东')
   s3 = School('深圳','南山')
   
   c1 = ClassRoom('全栈21期',s1)    # 嵌套在这里classRoom_object封装相对应的name和school_object
   c1.name
   c1.school.title 
   c1.school.address                # 对象点点点取值即可.类比于列表的嵌套
   ```

   

### 7.3 类的双下方法/魔术方法/内置方法

#### 7.3.1 类中的`__****__`函数

1. `__init__`  和 `__new__` ,类()执行

```python
class Foo;
"""
类的注释,类是干啥的
"""
    def __init__(self,name):
        """
        初始化方法
        :param name: 
        """
        self.name= name
        
    def __new__(cls, *args, **kwargs):
        """
        创建对象的函数,用于创建空对象，构造方法,自动调用,类()
        :param args: 
        :param kwargs: 
        :return: 
        """
        pass

```

2. `__call__`  方法,对象()自动执行

```python
class Base:

    def __init__(self,name):
        self.name= name

    def __call__(self, *args, **kwargs):
        print('78945613')
        return '邓益新'

obj1 = Base('小明')
obj1()       # ---> 执行call方法
```

额外的写网站

```python
#!/usr/bin/env python
# -*- coding:utf-8 -*-
from wsgiref.simple_server import make_server

def func(environ,start_response):
    start_response("200 OK", [('Content-Type', 'text/plain; charset=utf-8')])
    return ['你好'.encode("utf-8")  ]

class Foo(object):

    def __call__(self, environ,start_response):
        start_response("200 OK", [('Content-Type', 'text/html; charset=utf-8')])
        return ['你<h1 style="color:red;">不好</h1>'.encode("utf-8")]


# 作用：写一个网站，用户只要来方法，就自动找到第三个参数并执行。
server = make_server('127.0.0.1', 8000, Foo())
server.serve_forever(
```

3. `__setitem__`      `__getitem__`  `__delitem__` 方法,类似索引的方法执行,[   ]

```python
class Base:

    def __setitem__(self, key, value):
        print({key,value})
        print('setitem方法执行,没有返回值')

    def __getitem__(self, item):
        print('getitem方法执行,可以有返回值')
        return 7989463

    def __delitem__(self, key):
        print('随你定函数方法')


objj = Base()
objj[123] = 456
v = objj[756]
del objj[7582]
print(v)
```

4. `__str__`方法,定义在类中,打印print对象时自动执行打印函数返回值

```python
# 不要相信print打印出来的东西,而是要type

class Base:

    def __str__(self):
        print('执行__str__方法,print(对象)其实是打印这个函数的返回值')
        return '对象的名字是大奔'

obj = Base()
print(obj,type(obj))   


# 执行__str__方法,print(对象)其实是打印这个函数的返回值
# 对象的名字是大奔 <class '__main__.Base'>
```

5. `__dict__`方法,  把对象封装的值转为字典.`obj.__dict__`方法

```python
class Foo(object):
    def __init__(self,name,age,email):
        self.name = name
        self.age = age
        self.email = email

obj = Foo('alex',19,'xxxx@qq.com')
print(obj)
print(obj.name)
print(obj.age)
print(obj.email)
val = obj.__dict__ # 去对象中找到所有变量并将其转换为字典
print(val)
```

6. 上下文管理

```python
class Satcc:
    def __enter__(self):
        print('上文,在with语句之前执行,with 对象 as f的f是函数返回值')
        print('开始')
        return 123456

    def __exit__(self, exc_type, exc_val, exc_tb):
        print('下文,with语句执行完毕后再执行此程序')
        print('结束')

with Satcc() as f:
    print('中间语句开始执行')
    print(f)
    print('中间语句执行完毕')
    
    
# 类中定义 __enter__(self)  和   __exit__(self,其他3个参数)上下文管理
```

7. 对象相加/减/乘/除

```python
class Fass:
    def __add__(self, other):
        print('前者和后者相加')
        return 456413

obj = Fass()
v = 2
t = obj + v    # 前者和后者相加,自动调用__add__函数,前者是obj,即self,后者无所谓
print(t)       # 456413

	def __sub__(self, other):
        print(123)

    def __mul__(self, other):
        print(165)


		   
```

#### 7.3.2 类相关的内置函数

1. type

```python
class Foo:
    pass

obj = Foo()
 # 推荐使用type判断 是否是那个类创建的对象(实例化)
if type(obj) == Foo:    # # 判断obj是否是Foo类的实例（对象）
    print('obj是Foo类的对象') 
```

2. issubclass

```python
class Base:
    pass

class Base1(Base):
    pass

class Foo(Base1):
    pass

class Bar:
    pass



print(issubclass(Bar,Base))
print(issubclass(Foo,Base))  # 判断前者是否继承后者
```

3. isinstance (不推荐)

```python
class Base(object):
    pass

class Foo(Base):
    pass

obj = Foo()

print(isinstance(obj,Foo))  # 判断obj是否是Foo类或其基类,的实例（对象）
print(isinstance(obj,Base)) # 判断obj是否是Foo类或其基类,的实例（对象）
```

#### 7.3.3 类中的super语句(重点)

```python
class Base(object): # Base -> object
    def func(self):
        super().func()
        print('base.func')

class Bar(object):
    def func(self):
        print('bar.func')

class Foo(Base,Bar): # Foo -> Base -> Bar
    pass

obj = Foo()
obj.func()          # 打印 base.func    bar.func

# super().func() 根据self对象所属类的继承关系，按照顺序挨个找func方法并执行(找到第一个就不在找了)
```

### 7.4 对象详解

#### 7.4.1 数据结构栈

```python
# 后进先出
class Stack(object):
    def __init__(self):
        self.data_list = []

    def push(self, val):
        """
        向栈中压入一个数据（入栈）
        :param val:
        :return:
        """
        self.data_list.append(val)

    def pop(self):
        """
        从栈中拿走一个数据（出栈）
        :return:
        """
        return self.data_list.pop()    # 默认删除最后一个拿出来
```

#### 7.4.2可迭代对象

1. #### 表象：可以被for循环对象就可以称为是可迭代对象

2. 如何让一个对象变成可迭代对象？

   在类中实现`__iter__`方法且返回一个迭代器（生成器）

```python
# __iter__方法,返回一个迭代器
class Foo:
    def __iner__(self):
        return iter([1,2,3,4,5,6,7])
    
obj = Foo()
for i in obj:    # 字动执行__iter__方法
    print(i)
```

#### 7.4.3约束

```python
# 约束字类中必须写send方法，如果不写，则调用时候就报抛出 NotImplementedError 
class Interface(object):
    def send(self):
        raise NotImplementedError('自动抛出异常')    # 其派生类中必须要有send方法
        
class Message(Interface):
    def send(self):
        print('发送短信')z
        
class Email(Interface):
    def send(self):
        print('发送邮件')
```

#### 7.4.4 反射

根据字符粗的形式去某个对象中 操作 他的成员。 

- getattr--->getattr(对象,"字符串")     根据字符粗的形式去某个对象中 获取 对象的成员。 

  ```python
  class Foo(object):
      country = '中国'
  
      def __init__(self,name,age):
          self.name = name
          self.age = age
  
      def func(self):
          print(123)
  
      @staticmethod
      def funcxxx():
          print('邓益新')
  
  obj = Foo('小明',30)
  getattr(Foo,'funcxxx')()
  v = getattr(obj,'name')
  print(v)
  val = getattr(obj,'func')()
  print(getattr(Foo,'country'))
  
  ```

- hasattr--->对象,'字符串')   根据字符粗的形式去某个对象中判断是否有该成员。 

  ```python
  class Foo(object):
      country = '中国'
  
      def __init__(self,name,age):
          self.name = name
          self.age = age
  
      def func(self):
          print(123)
  
      @staticmethod
      def funcxxx():
          print('邓益新')
  
  obj = Foo('小明',30)
  print(hasattr(Foo,'funcxxx'))
  v = hasattr(obj,'xiaoming')
  print(v)
  
  print(hasattr(Foo,'country'))
  ```

- setattr,(对象,'变量','值')   根据字符粗的形式去某个对象中设置成员。 

  ```python
  class Foo(object):
      country = '中国'
  
  
      def func(self):
          print(123)
  
      @staticmethod
      def funcxxx():
          print('邓益新')
  
  def func123():
      print(12316512)
      return 456
  
  obj = Foo()
  obj.k1 = 999
  setattr(obj,'k1',func123) # obj.k1 = 123
  obj.k1()
  
  
  # 可以设置函数和变量,都可以
  
  #!/usr/bin/env python
  # -*- coding:utf-8 -*-
  from wsgiref.simple_server import make_server
  
  class View(object):
      def login(self):
          return '登陆'
  
      def logout(self):
          return '等处'
  
      def index(self):
          return '首页'
  
  
  def func(environ,start_response):
      start_response("200 OK", [('Content-Type', 'text/plain; charset=utf-8')])
      #
      obj = View()
      # 获取用户输入的URL
      method_name = environ.get('PATH_INFO').strip('/')
      if not hasattr(obj,method_name):
          return ["sdf".encode("utf-8"),]
      response = getattr(obj,method_name)()
      return [response.encode("utf-8")  ]
  
  # 作用：写一个网站，用户只要来方法，就自动找到第三个参数并执行。
  server = make_server('192.168.12.87', 8000, func)
  server.serve_forever()
  
  ```

- delattr-->delattr(对象,'变量')   根据字符粗的形式去某个对象中删除成员。

  ```python
  class Foo:
      pass
  
  obj = Foo()
  obj.k1 = 999
  delattr(obj,'k1')
  print(obj.k1)
  
  ###############################
  class Foo(object):
      country = '中国'
  
  
      def func(self):
          print(123)
  
      @staticmethod
      def funcxxx():
          print('邓益新')
  
  
  def func123():
      print(12316512)
      return 456
  
  obj = Foo()
  obj.k1 = 999
  delattr(obj,'funcxxx')
  obj.funcxxx()              # 报错
  ```

#### 7.4.5.python一切皆对象

- py文件
- 包
- 类
- 对象

python一切皆对象，所以以后想要通过字符串的形式操作其内部成员都可以通过反射的机制实现

#### 7.4.6反射提高你代码的逼格

```python
# 反射提高你代码的逼格

# 1.构造字典,避免重复的if else
method_dict = {'up':self.upload, 'down':self.download}
method = method_dict.get(action)
method()

# 2. 利用反射实现
method = getattr(self,action) # upload  # self.upload
method()

# 3.getattr也支持返回值为None
class Foo(object):
    def get(self):
        pass

obj = Foo()
# if hasattr(obj,'post'): 
#     getattr(obj,'post')

v1 = getattr(obj,'get',None) # ,反射值默认为None推荐  
print(v1)

v2 = getattr(obj,'get1573453',None) # 推荐
print(v2)
```

#### 7.4.7反射本文件内的对象

```python
import sys
sys.modules[__name__]---->指向本文件
# '__main__': <module '__main__' from 'D:/code/day24/1.内容回顾.py'>

# 反射
    # 通过 对象 来获取 实例变量、绑定方法
    # 通过 类   来获取  类变量、类方法、静态方法
    # 通过 模块名 来获取 模块中的任意变量（普通变量 函数  类）
    # 通过 本文件 来获取 本文件中的任意变量
        # getattr(sys.modules[__name__],'变量名')
```



#### 7.4.8模块  : importlib

根据字符串的形式导入模块。

```python
import importlib

# 用字符串的形式导入模组
val= importlib.import_module('123.456')

print(getattr(val,'func333')())
```

![1556279304314](D:\md_down_png\1556279304314.png)

### 7.5  对象补充

#### 7.5.1单例模式

- 单例模式(23种设计模式之一)

无论实例化多少次，永远用的都是第一次实例化出的对象。

1. 单例模式标准

   ```python
   class Singleton(object):
       __instance = None
   
       def __new__(cls, *args, **kwargs):
           if not cls.instance :
               cls.instance = object.__new__(cls)
           return cls.instance
   
   obj1 = Singleton()
   obj2 = Singleton()
   print(obj1,obj2)
   
   # 注意 : 此时还不是最终状态,-->加锁
   ```

   单例模式例子

   ```python
   # 文件链接池
   
   class FileHelper(object):
       instance = None
       def __init__(self, path):
           self.file_object = open(path,mode='r',encoding='utf-8')   #网络链接过与费时,只允许一个链接访问
   
       def __new__(cls, *args, **kwargs):
           if not cls.instance:
               cls.instance = object.__new__(cls)
           return cls.instance
   
   obj1 = FileHelper('x')
   obj2 = FileHelper('x')
   
   ```

#### 7.6 总结

学完了面向对象编程,项目结构是时候要优化一下

##### 1.脚本

特别小的py文件,单一功能,项目下就一个py文件就可以了

![1556457649559](D:/python/s21day23/day23notes%20.assets/1556457649559.png)

##### 2.单个可执行文件

- config-->配置文件

- db-->database数据文件

- lib-->公共功能-->分页/日志

- src-->功能

- app.py  或者  run.py  --->代码越少越好

  ![1556457817139](D:/python/s21day23/day23notes%20.assets/1556457817139.png)

##### 3.多个可执行文件

- bin--->多个可执行文件---theacher/student/admin --->代码越少越好start

- config-->配置文件settings

- db-->database数据文件

- lib-->公共功能-->分页/日志-->扩展模块

- src-->功能,业务逻辑

- log-->存放日志文件

  ![1556457924934](D:/python/s21day23/day23notes%20.assets/1556457924934.png)

  

### 7.6 正则表达式

#### 7.6.1.获取文件夹的大小

- os.walk  /  os. listdir

#### 7.6.2.红包问题

```python
import random

def red_pack(money,num):
    ret = random.sample(range(1,money*100),num-1)
    ret.sort()
    ret.insert(0,0)
    ret.append(money*100)
    for i in range(len(ret)-1):
        yield ((ret[i+1]-ret[i])/100)

val = red_pack(200,10)
for i in val:
    print(i)
```

#### 7.6.3 正则表达式是什么

正则表达式是一种匹配字符串的规则, 他有许多用途 .在编写处理字符串的程序或网页时，经常会有查找符合某些复杂规则的字符串的需要。**正则表达式**就是用于描述这些规则的工具。换句话说，正则表达式就是记录文本规则的代码 .

主要用途有:

- 找电话号码/身份证号/ip地址
- 表单验证
- 银行卡号
- 爬虫,从网页源码中获取一些链接、重要数据

#### 7.6.4.正则规则

##### 1.元字符

1. 第一条规则 ： 本身是哪一个字符，就匹配字符串中的哪一个字符

2. 第二条规则 ： 字符组[字符1字符2]，一个字符组就代表匹配一个字符，只要这个字符出现在字符组里，那么就说明这个字符能匹配上

   - 所有的范围都必须遵循ascii码从小到大来指定
   - 注意 : 字符组可以是范围,如[0-9]    ,   [A-Z]   ,    [a-z]

   1. \d   ,表示[0-9]  字符组,表示所有的数字.(digit)-->\是转义符   转义符转义了d，让d能够匹配所有0-9之间的数

   2. \w   ,表示 大小写字母  数字 下划线

   3. \s    ,表示空白 空格 换行符 制表符

      - \n    ,表示 换行符(next)
      - \t    ,表示 制表符(table)

   4. \D    ,表示除0-9意外的所有字符

   5. \W    ,表示除 数字字母下划线之外的所有字符

   6. \S    ,表示非空白 空格 换行符 制表符之外的所有字符

   7. 英文字符`   .`  ,表示除了换行符之外的任意字符

   8. []    字符组  :  只要在中括号内的所有字符都是符合规则的字符

   9. [^]   非字符组：只要在中括号内的所有字符都是不符合规则的字符

   10. ^   表示一个字符的开始 , 用法为  ^字符

   11. `$` 表示一个字符的结束 ,用法为   字符`  $  `

   12. | 表示或，注意，如果两个规则有重叠部分，总是长的在前面，短的在后面

   13. () 表示分组，给一部分正则规定为一组，   `     |   ` 这个符号的作用域就可以缩小了

       **注意 : **` [\d]   [0-9]  \d  没有区别 都是要匹配一位数字`

       **注意2 : **`[\d\D]  [\W\w]  [\S\s] 匹配所有一切字符`

##### 2.元字符总结记忆

1. `\d \w \s \t(table) \n(next)`
2. `\D \W \S`
3. `   .  `英文字符
4. `[]       [^]` 字符组
5. `^      $    ` 表开始和结束
6. `|  ()` 表或者,长的放在前面

#### 7.6.5.量词

1. `{n}`放在元字符后面,表示只能出现n次
2. `{n,}  `  放在元字符后面表示至少出现n次
3. `{n,m}`   放在元字符后面表示至少出现n次,至多出现m次
4. **?**   表示匹配0次或1次   表示可有可无 但是有只能有一个 比如小数点
5. `  +    `  表示匹配1次或多次
6. `  *   ` 表示匹配0次或多次,    表示可有可无 但是有可以有多个 比如小数点后n位

##### 1. 量词解析

1. 匹配任意小数或整数

   `\d+(\.\d+)?`

   `(\d+\.\d+)|\d+`         # ---->表或者,长的放在前面

2. 匹配任意2位小数

   `\d+\.\d{2}`

#### 7.6.6. 贪婪匹配

正则表达式默认是贪婪匹配,总是会在符合量词条件的范围内尽量多匹配,如 :`\d{7,12}`

1. 回溯算法,了解即可,正则表达式的匹配算法,尽量多匹配.
2. 变更为非贪婪匹配(惰性匹配)----->在量词后面再加上一个`?`

- 元字符   量词   ?    -->惰性匹配,匹配最少的
- 元字符   --->匹配单个字符
- 元字符   量词
- `\d+?x  `   --->匹配任意数字,遇到x就立即停止
- `.*?x ` 匹配任意的内容任意多次遇到x就立即停止

1. 身份证号 : 15位  全数字 首位不为0/18位  前17位全数字 首位不为0  最后一位可能是x和数字

   `[1-9](\d{14}|\d{16}[\dx])`

   `[1-9]\d{14}(\d{2}[\dx])?`

   `[1-9](\d{16}[\dx])|\d{14}`

### 7.7 re模块

#### 7.7.1.转义符

```python
# 正则表达式的转义符和python字符串中的转义符没有关系,且可能会有冲突, 为了避免这种冲突我们所有的正则都以在工具中的测试结果为结果然后只需要在正则和待匹配的字符串外面都加r即可
```

#### 7.7.2.re模块的方法

1. findall   和   finditer使用

   ```python
   import re         # 导入re模块(regex正则英文)
   ret = re.findall('\d','123456sedf346dfs1223')
   print(ret)    # 返回一个列表,是所有匹配到的数据组成的列表,未匹配是None
   # *** 注意:findall 默认会将正则表达式中加分组的匹配内容添加列表中,在括号内加上?:去除这种默认的权限.
   ret = re.findall("www.(baidu|360).com" , "www.baidu.com")
   print(ret)
   
   结果:    ["baidu"]
   
   ret = re.findall("www.(?:baidu|360).com" , "www.baidu.com")
   print(ret)
   
   结果:    ["www.baidu.com"]
   
   # findall 会匹配字符串中所有符合规则的项,并返回一个列表.
   
   #---------------------------------------------#
   ret = re.finditer('\d','123456sedf346dfs1223')       # ret是个迭代器,这样省内存
   for item in ret :                                    # 迭代出来的都是一个个对象,使用group取值
       print(item.group())
   ```

2. search  和  match的使用

   ```python
   import re
   ret = re.search('\d','123456sedf346dfs1223')
   print(ret)                      # 是个对象
   print(ret.group())              # 取出匹配到的值,若没有匹配成功,ret = None , 会报错,None不支持.group()方法
   # 只匹配从左到右的第一个,得到的不是直接的结果,而是一个变量,通过这个变量的group方法来获取结果
   
   　　　　# 如果没有匹配到,会返回None , 使用group 会报错
   
   #---------------------------------------#
   if ret :
       print(ret.group())           # 正确写法
       
   # _---------------------------------------------_#
   ret2 = re.match('\d','123456sedf346dfs1223')    # 只能匹配开头字符串,开头不符合则返回None,返回一个对象.
   
   # match 相当于  search + 正则的^(开头判定)
   
   ```

3. compile方法

   ```python
   # 节省时间, 在同一个正则表达式重复使用多次的时候使用能够减少时间的开销
   import re
   ret = re.compile('\d')       # 可选参数flags = re.S 等等
   result = ret.findall/finditer/search ('待匹配字符串')
   
   # 即可使用
   # # \d 正则表达式  ——> 字符串
       # \d  str
           # 循环str，找到所有的数字         --->每次使用重复正则表达式,py解读耗时
   ```

4. 其他不重要方法--->sub   subn    split    

   ```python
   import re
   
   ret_list = re.split('\d','sss123swde45d11cd4e51')
   # 得到以数字为分割符的列表,没有数字的
   
   # -----------------------------------# 
   ret_list = re.split('(\d)','sss123swde45d11cd4e51')
   # 得到以数字为分割符的列表,会将分割符保留在列表中.
   
   # ----------------------------# 特别的
   ret_list = re.split('\d(\d)','sss123swde45d11cd4e51')
   # 也会保存,但只会保存括号内分隔符.
   
   
   
   
   # sub  和 subn 方法  --->类似于字符串的replace方法
   ret = re.sub('\d','D','alex83wusir74taibai',1)   # 把第一个数字替换为'D',得到字符串
   
   ret2 = re.subn('\d','D','alex83wusir74taibai')   # 把第数字替换为'D',得到二元元组,前者是替换后的字符串,后者是替换了多少次的次数n
   
   ```

5. 提供效率问题

   ```python
   # re 模块的进阶
   # 1.时间复杂度  效率  compile
       # 在同一个正则表达式重复使用多次的时候使用能够减少时间的开销
   # 2.空间复杂度  内存占用率   finditer
       # 在查询的结果超过1个的情况下，能够有效的节省内存，降低空间复杂度，从而也降低了时间复杂度
   # 3.用户体验
   ```

#### 7.7.3. re模块的分组和命名

```python
import re
s1 = '<h1>wahaha</h1>'
ret = re.search('<(\w+)>(.*?)</\w+>',s1)          # 支持'group()的都能用 (search / finditer),有几个括号就有几个分组,以1开始,默认是0, 表示取整个正则匹配的结果.             (类似位置参数的思想)
print(ret)
print(ret.group(0))   # group参数默认为0 表示取整个正则匹配的结果
print(ret.group(1))   # 取第一个分组中的内容
print(ret.group(2))   # 取第二个分组中的内容

# ------------------------------------  # 
 #  给分组起个别名  --->           (?P<别名>正则表达式)   --->类似于关键字传参
s1 = '<h1>wahaha</h1>'
ret2 = re.search('<(?P<tag>\w+)>(?P<cont>.*?)</\w+>',s1)
print(ret2)                 # 对象
print(ret.group('tag'))     # 取tag分组中的内容
print(ret.group('cont'))    # 取cont分组中的内容


# 分组引用  (?P=组名)   这个组中的内容必须完全和之前已经存在的组匹配到的内容一模一样
s1 = '<h1>wahaha</h1>'
ret3 = re.search('<(?P<tag>\w+)>(?P<cont>.*?)</(?P=tag)>',s1)
print(ret3)                 # 对象
print(ret.group('tag'))     # 取tag分组中的内容

### 注意 : (?P=组名)  括号内不能有空格,会报错


# 特别的  , 另一种分组引用(类似位置传参)
s1 = '<h1>wahaha</h1>'
ret3 = re.search(r'<(?P<tag>\w+)>(?P<cont>.*?)</(\1)>',s1)           # 使用r将其转义.(\n),这个n是前面分组对应的n.第n个分组
print(ret3)                 # 对象
print(ret.group('tag'))     # 取tag分组中的内容
```

#### 7.7.4. re模块 总结

```python
re.finall()          # 正则 + 待匹配字符串,返回列表.
re.finditer()        # 于上类似,返回类,迭代的item.group()取值.

re.search()          # 正则 + 待匹配字符串,返回对象,使用.group()取值.
re.match()           # 正则 + 待匹配字符串,返回匹配的字符串.

obj = re.compile('正则')    # 对此利用重复的表达式来匹配,使用compile提供效率.
obj.search/finditer()  # 等等

re.split('正则','匹配字符串')       # 加括号保留分割符
re.sub() \ subn()     # 方法的返回值

# 分组()即可   ,  .group(0)  # 1,2,3
# 分组命名  (?P<别名>正则)   # .group('别名')
# 分组引用  (?P=别名)        或者(\1) ---> 前面要转义 + r 
#  (?P=别命) 表示这个组中的内容必须和之前已经存在的一组匹配到的内容完全一致

# 分组() 和findall的联系,列表优先显示分组内的内容(默认)   ---> 去除默认,    (?:正则)


```

- 有的时候我们想匹配的内容包含在不相匹配的内容当中，这个时候只需要把不想匹配的先匹配出来，再通过手段去掉

  如匹配只匹配整数,会把小数的整数和小数部分分别匹配,这次匹配整数或小数,再去除小数即可

```python
import re
ret=re.findall(r"\d+\.\d+|(\d+)","1-2*(60+(-40.35/5)-(-4*3))")
print(ret)
ret.remove('')
print(ret)
```

####7.7.5.内容总结

1. re模块的方法
   1. findall,返回一个列表,如果正则表达式中有(),优先把括号内的内容放到列表中,打印出来.         (?:正则)取消默认权限.
   2. search,找到第一个符合规则的项，并返回一个对象
   3. match,从头开始 找到第一个符合规则的项，并返回一个对象
   4. finditer,返回一个迭代器,找到所有符合规则的项，并返回一个迭代器
   5. compile , 编译,预编译一个正则规则，节省多次使用同一个正则的编译时间
   6. split , 分割,根据正则规则切割，返回列表，默认不保留切掉的内容
   7. sub  替换 默认替换所有，可以使用替换深度参数
   8. subn     替换 返回元组
2. 分组
   1. 分组命名-->(?P<组名>正则)
   2. 引用分组--->(?P=组名)
   3. findall 和优先分组
      - 优先显示分组中的内容,(?:正则)则取消这种默认
   4. split 默认不保留分割符,-
      - split会保留正则中分组内匹配到的内容
   5. search 和 分组
      - search取分组的内容
        - 通过索引取 obj.group(1)
        - 通过组名取 obj.group('组名')
3. 元字符  量词    ?(惰性符号)
   - `[][^  ]`  带有特殊意义的元字符到字符组内大部分都会取消它的特殊意义
     1. `[()+*.]` 会取消特殊的意义
     2. `[-]` 的位置决定了它的意义，写在字符组的第一个位置/最后一个位置就表示一个普通的横杠
        - 写在字符组的其他任何位置都表示一个范围

#### 7.7.6. re模块方法中的flags 参数

```python
flags有很多可选值：

re.I(IGNORECASE)忽略大小写，括号内是完整的写法
re.M(MULTILINE)多行模式，改变^和$的行为
re.S(DOTALL)点可以匹配任意字符，包括换行符
re.L(LOCALE)做本地化识别的匹配，表示特殊字符集 \w, \W, \b, \B, \s, \S 依赖于当前环境，不推荐使用
re.U(UNICODE) 使用\w \W \s \S \d \D使用取决于unicode定义的字符属性。在python3中默认使用该flag
re.X(VERBOSE)冗长模式，该模式下pattern字符串可以是多行的，忽略空白字符，并可以添加注释


# 注意有些方法有flags参数,有些是没有的.

```

#### 7.7.6re 和 爬虫的应用

##### 1.生成器对象的send 方法

```python
#  生成器对象是一个迭代器。但是它比迭代器对象多了一些方法，它们包括send方法，throw方法和close方法。这些方法，主要是用于外部与生成器对象的交互。本文先介绍send方法。

# send方法有一个参数，该参数指定的是上一次被挂起的yield语句的返回值。

# 本质是给yield 语句的返回值 赋值
```

- 注意 : 一般用send方法之前,先next() 一波,主要是用于外部与生成器对象的交互。 

```python
import requests
import json
import re                     # 导入3个模块

pattern = '<div class="item">.*?<em class="">(?P<id>\d+)</em>.*?<span class="title">(?P<title>.*?)</span>.*?' \
              '<span class="rating_num".*?>(?P<score>.*?)</span>.*?<span>(?P<comment_num>.*?)人评价</span>'
obj = re.compile(pattern,flags = re.S)
url = 'https://movie.douban.com/top250?start=%s&filter='

def get_page(url):                  # 构造函数,爬取网站信息,返回值是网站内容的大字符串
    ret = requests.get(url)
    return  ret.text

def parser_page(url):
    response = get_page(url)
    result = obj.finditer(response)
    for i in result:
        yield  [i.group('id'), i.group("title"), i.group('score'), i.group('comment_num')]


def write(fielname) :
    with open(fielname,'w',encoding='utf-8') as f:
        while True :
            list = yield  None
            f.write(json.dumps(list , ensure_ascii = False) + '\n')


ttt = write('小明爬知乎')
print(type(ttt))         # ---><class 'generator'>生成器类,支持write 方法
print(ttt.send(None))
num = 0
for item  in range(10) :
    result = parser_page('https://movie.douban.com/top250?start=%s&filter=' %num)
    num += 25
    for i in result :
        ttt.send(i)
ttt.close()
```



## 第八章网络编程

### 8.1   网络应用开发架构

#### 8.1.1.两台机器通过网络来实现通信

#### 8.1.2.网络开发架构

- C/S架构  迅雷,qq,微信,飞秋等
  - client  客户端
  - server  服务端
- B/S架构   淘宝  游戏  百度   博客园
  - browser   浏览器
  - server  服务端
- 统一程序的入口

#### 8.1.3. 网络基础

1. B/S是特殊的C/S架构

2. 网络基础

   - 网卡 ：是一个实际存在在计算机中的硬件
   - mac地址 ：每一块网卡上都有一个全球唯一的mac地址
   - 交换机 ：是连接多台机器并帮助通讯的物理设备，只认识mac地址
   - 协议 ：两台物理设备之间对于要发送的内容，长度，顺序的一些约定

3. ip地址

   1. ipv4协议  位 的点分十进制  32位2进制表示

      - 0.0.0.0       255.255.255.255

   2. ipv6协议 6位的冒分十六进制 128位2进制表示

      - 0:0:0:0:0:0       F:F:F:F:F:F

   3. 公网ip 个数是有限的.

      - 每一个ip地址要想被所有人访问到，那么这个ip地址必须是你申请的.必须花费费用.(云服务器等等)
      - 内网ip
        - 192.168.0.0 - 192.168.255.255
        - 172.16.0.0 - 172.31.255.255
        - 10.0.0.0 - 10.255.255.255
        - 约定俗成的空出来的ip,用于局域网ip地址的分配

   4. 交换机实现的arp协议

      - 通过ip地址获取一台机器的mac地址
      - 地址解析协议，即ARP（Address Resolution Protocol），是根据IP地址获取物理地址的一个TCP/IP协议。 

   5. 网关ip 一个局域网的网络出口，访问局域网之外的区域都需要经过路由器和网关

      1. 网段 指的是一个地址段 x.x.x.0  x.x.0.0  x.0.0.0

      2. 子网掩码 判断两台机器是否在同一个网段内的

         - 所谓”子网掩码”，就是表示子网络特征的一个参数。它在形式上等同于IP地址，也是一个32位二进制数字，它的网络部分全部为1，主机部分全部为0。比如，IP地址172.16.10.1，如果已知网络部分是前24位，主机部分是后8位，那么子网络掩码就是11111111.11111111.11111111.00000000，写成十进制就是255.255.255.0。

           知道”子网掩码”，我们就能判断，任意两个IP地址是否处在同一个子网络。方法是将两个IP地址与子网掩码分别进行AND运算（两个数位都为1，运算结果为1，否则为0），然后比较结果是否相同，如果是的话，就表明它们在同一个子网络中，否则就不是。 

           ​	一般255.0.0.0    -----       255.255.255.0

           ```python
           # 255.255.255.0 子网掩码
           # 11111111.11111111.11111111.00000000
           
           # 192.168.12.87
           # 11000000.10101000.00001100.01010111
           # 11111111.11111111.11111111.00000000
           # 11000000.10101000.00001100.00000000   192.168.12.0
           
           # 192.168.12.7
           # 11000000.10101000.00001100.00000111
           # 11111111.11111111.11111111.00000000
           # 11000000.10101000.00001100.00000000  192.168.12.0
           ```

           

      3. ip 地址能够确认一台机器

         - IP协议的作用主要有两个，一个是为每一台计算机分配IP地址，另一个是确定哪些地址在同一个子网络 

      4. port       端口

         - 0 - 65535
         - 计算机通过ip + 端口 识别应用程序

#### 8.1.4  总结

- 物理设备
  - 网卡--->mac地址,全球唯一
  - 交换机--->完成局域网内部多台计算机的通信
    - 通信方式 : 广播 ,单播 ,组播
    - 只能识别mac地址
    - arp协议   --->地址解析协议
      - 通过ip获取它的mac地址
      - 有交换机完成
      - 广播 单播
  - 路由器 -   -->完成局域网和局域网间的通信
    - 能识别IP地址
    - 网段
    - 网关-->网关ip :
      - 访问局域网外部服务的一个出口ip
- 使用IP地址在网络上定位一台机器
- port +  ip  能在网络上定位一台机器上的一个服务
  - 一台拥有IP地址的主机可以提供许多服务，比如Web服务、FTP服务、SMTP服务等，这些服务完全可以通过1个IP地址来实现。那么，主机是怎样区分不同的网络服务呢？显然不能只靠IP地址，因为IP 地址与网络服务的关系是一对多的关系。实际上是通过“IP地址+端口号”来区分不同的服务的。 



### 8.2  socket 模块的使用

```python
# server.py 服务端
import socket
sk = socket.socket() # 买手机
sk.bind(('127.0.0.1',9000))  # 绑定卡号
sk.listen()                   # 开机
conn,addr = sk.accept()     # 等着接电话
conn.send(b'hello')
msg = conn.recv(1024)
print(msg)
conn.close()    # 挂电话
sk.close()      # 关机

# client.py 客户端
import socket
sk = socket.socket()
sk.connect(('127.0.0.1',9000))
msg = sk.recv(1024)
print(msg)
sk.send(b'byebye')
sk.close()

# 注意：运行时，先执行server.py，再执行client.py
```

- - 

### 8.3 .TCP和UDP协议

##### 8.3.1.TCP协议

- **TCP**（Transmission Control Protocol）可靠的、面向连接的协议（eg:打电话）、传输效率低全双工通信（发送缓存&接收缓存）、面向字节流。使用TCP的应用：Web浏览器；电子邮件、文件传输程序。 当客户和服务器彼此交换数据前，必须先在双方之间建立一个TCP连接，之后才能传输数据。TCP提供超时重发，丢弃重复数据，检验数据，流量控制等功能，保证数据能从一端传到另一端。  

- 类似于打电话,可靠,为了数据和安全和完整性牺牲了效率,慢.是一种全双工通信.

- 建立连接 : 3次握手

  - 

- 断开连接 : 4次挥手

  ![QQ图片20190507145318](D:/python/s21day28/day28notes.assets/QQ%E5%9B%BE%E7%89%8720190507145318.jpg)

  - 

- 在建立连接之后 :

  - 发送的每一条消息都有回执
  - 为了保证数据的完整性,还有重传机制
  - 长连接,会一直占用连接,占用2方的端口
  - 能够传输的数据长度几乎没有限制

- 文件的上传和下载(邮件 ,网盘)

##### 8.3.2  UDP协议

- **UDP**（User Datagram Protocol）不可靠的、无连接的服务，传输效率高（发送前时延小），一对一、一对多、多对一、多对多、面向报文，尽最大努力服务，无拥塞控制。使用UDP的应用：域名系统 (DNS)；视频流；IP语音(VoIP)。，它只是把应用程序传给IP层的数据报发送出去，但是并不能保证它们能到达目的地。由于UDP在传输数据报前不用在客户和服务器之间建立一个连接，且没有超时重发等机制，故而传输速度很快 
- 类似于发短信 ,只负责发短信,能够发送到目的地不清楚,传输很快
- 特点 : 
  - 无连接的,速度很快
  - 可能会丢消息
  - 能够传递的数据的长度是有限的,和设备有关
- 即时通讯类   微信  qq   飞秋  

##### 8.3.3. IO （input,output）操作

1. 输入和输出是相对内存来说的:

- write   send    --->output
- read    recv    --->input

##### 8.3.4    互联网协议   和  osi模型

1. 按功能不同分为osi七层或tcp/ip五层或tcp/ip四层 模型

   ![1557215039629](D:/python/s21day28/day28notes.assets/1557215039629.png)

------------>应表会传网数物

1. osi五层协议

   |  五层模型  |              协议              |       物理设备        |
   | :--------: | :----------------------------: | :-------------------: |
   |   应用层   | http/https/ftp/smtp/python代码 |                       |
   |   传输层   |      tcp/udp协议    端口       | 四层路由器,四层交换机 |
   |   网络层   |   ipv4  / ipv6协议(ip协议v)    |   路由器,三层交换机   |
   | 数据链接层 |      mac地址     arp协议       |     网卡  交换机      |
   |   物理层   |                                | 中继线,集线器  双绞线 |

### 8.4  socket套接字进阶

1. python的socket模块----->完成socket的功能

2. 工作在应用层和传输层之间的抽象层

   - 帮助我们完成所有信息的组织和拼接

   ![1557216028142](D:/python/s21day28/day28notes.assets/1557216028142.png)

   socket对于程序猿来说是网络操作的底层了

3. Socket是应用层与TCP/IP协议族通信的中间软件抽象层，它是一组接口。在设计模式中，Socket其实就是一个门面模式，它把复杂的TCP/IP协议族隐藏在Socket接口后面，对用户来说，一组简单的接口就是全部，让Socket去组织数据，以符合指定的协swv 1.

4. 议。 

   ```python
   站在自己的角度上看，socket就是一个模块。我们通过调用模块中已经实现的方法建立两个进程之间的连接和通信。
   也有人将socket说成ip+port，因为ip是用来标识互联网中的一台主机的位置，而port是用来标识这台机器上的一个应用程序。
   所以我们只要确立了ip和port就能找到一个应用程序，并且使用socket模块来与之通信
   
   ```

5. socket小问题

   ![1557217002680](D:/python/s21day28/day28notes.assets/1557217002680.png) 

6. tcp端

   ```python
   import socket
   sk = socket.socket()
   sk.bind(('127.0.0.1',8898))  #把地址绑定到套接字
   sk.listen()          #监听链接
   conn,addr = sk.accept() #接受客户端链接
   ret = conn.recv(1024)  #接收客户端信息
   print(ret)       #打印客户端信息
   conn.send(b'hi')        #向客户端发送信息
   conn.close()       #关闭客户端套接字
   sk.close()        #关闭服务器套接字(可选)
   
   
   import socket
   sk = socket.socket()           # 创建客户套接字
   sk.connect(('127.0.0.1',8898))    # 尝试连接服务器
   sk.send(b'hello!')
   ret = sk.recv(1024)         # 对话(发送/接收)
   print(ret)
   sk.close()            # 关闭客户套接字
   
   ```

7. udp端

   ```python
   import socket
   udp_sk = socket.socket(type=socket.SOCK_DGRAM)   #创建一个服务器的套接字
   udp_sk.bind(('127.0.0.1',9000))        #绑定服务器套接字
   msg,addr = udp_sk.recvfrom(1024)
   print(msg)
   udp_sk.sendto(b'hi',addr)                 # 对话(接收与发送)
   udp_sk.close()                         # 关闭服务器套接字
   
   
   
   import socket
   ip_port=('127.0.0.1',9000)
   udp_sk=socket.socket(type=socket.SOCK_DGRAM)
   udp_sk.sendto(b'hello',ip_port)
   back_msg,addr=udp_sk.recvfrom(1024)
   print(back_msg.decode('utf-8'),addr)
   ```

8. 控制台打印颜色

   ```python
   #格式：
   　　设置颜色开始 ：\033[显示方式;前景色;背景色m
   
   #说明：
   前景色            背景色           颜色
   ---------------------------------------
   30                40              黑色
   31                41              红色
   32                42              绿色
   33                43              黃色
   34                44              蓝色
   35                45              紫红色
   36                46              青蓝色
   37                47              白色
                  
                  
                  
   显示方式           意义
   -------------------------
   0                终端默认设置
   1                高亮显示
   4                使用下划线
   5                闪烁
   7                反白显示
   8                不可见
    
   #例子：
   \033[1;31;40m    <!--1-高亮显示 31-前景色红色  40-背景色黑色-->
   \033[0m          <!--采用终端默认设置，即取消颜色设置-->
   ```

   

![1557315007568](D:\python\s21day28\day28notes.assets\1557315007568.png)

- i p + port 确认一台机器上的一个应用

1. ### python day 29

    

   #### 1.socket模块tcp黏包

   1. TCP协议的黏包问题

      同时执行多条命令之后，得到的结果很可能只有一部分，在执行其他命令的时候又接收到之前执行的另外一部分结果，这种显现就是黏包 

   2. 黏包成因

      tcp的拆包机制

      ```python
      当发送端缓冲区的长度大于网卡的MTU时，tcp会将这次发送的数据拆成几个数据包发送出去。 
      MTU是Maximum Transmission Unit的缩写。意思是网络上传送的最大数据包。MTU的单位是字节。 大部分网络设备的MTU都是1500。如果本机的MTU比网关的MTU大，大的数据包就会被拆开来传送，这样会产生很多数据包碎片，增加丢包率，降低网络速度。
      ```

      ```python
      TCP（transport control protocol，传输控制协议）是面向连接的，面向流的，提供高可靠性服务。
      收发两端（客户端和服务器端）都要有一一成对的socket，因此，发送端为了将多个发往接收端的包，更有效的发到对方，使用了优化方法（Nagle算法），将多次间隔较小且数据量小的数据，合并成一个大的数据块，然后进行封包。
      这样，接收端，就难于分辨出来了，必须提供科学的拆包机制。 即面向流的通信是无消息保护边界的。 
      对于空消息：tcp是基于数据流的，于是收发的消息不能为空，这就需要在客户端和服务端都添加空消息的处理机制，防止程序卡住，而udp是基于数据报的，即便是你输入的是空内容（直接回车），也可以被发送，udp协议会帮你封装上消息头发送过去。 
      可靠黏包的tcp协议：tcp的协议数据不会丢，没有收完包，下次接收，会继续上次继续接收，己端总是在收到ack时才会清除缓冲区内容。数据是可靠的，但是会粘包。
      
      ```

   3. udp和tcp一次发送数据长度的限制

      ```python
          用UDP协议发送时，用sendto函数最大能发送数据的长度为：65535- IP头(20) – UDP头(8)＝65507字节。用sendto函数发送数据时，如果发送数据长度大于该值，则函数会返回错误。（丢弃这个包，不进行发送） 
      
          用TCP协议发送时，由于TCP是数据流协议，因此不存在包大小的限制（暂不考虑缓冲区的大小），这是指在用send函数时，数据长度参数不受限制。而实际上，所指定的这段数据并不一定会一次性发送出去，如果这段数据比较长，会被分段发送，如果比较短，可能会等待和下一次数据一起发送。
      
      ```

   4. tcp会发生黏包的2种情况

      - 发送端需要等待缓冲区满才发送出去,造成黏包（发送数据时间间隔很短，数据了很小，会合到一起，产生粘包） 

        ```python
        #_*_coding:utf-8_*_
        import socket
        BUFSIZE=1024
        ip_port=('127.0.0.1',8080)
        
        s=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
        res=s.connect_ex(ip_port)
        
        
        s.send('hello'.encode('utf-8'))
        s.send('egg'.encode('utf-8'))
        
        客户端
        ```

      - 接收方不及时接收缓冲区的包，造成多个包接收,TCP协议的数据没有边界.（客户端发送了一段数据，服务端只收了一小部分，服务端下次再收的时候还是从缓冲区拿上次遗留的数据，产生粘包）  

        ```python
        # 客户端
        #_*_coding:utf-8_*_
        import socket
        BUFSIZE=1024
        ip_port=('127.0.0.1',8080)
        
        s=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
        res=s.connect_ex(ip_port)
        
        
        s.send('hello egg'.encode('utf-8'))
        
        # 服务端
        #_*_coding:utf-8_*_
        from socket import *
        ip_port=('127.0.0.1',8080)
        
        tcp_socket_server=socket(AF_INET,SOCK_STREAM)
        tcp_socket_server.bind(ip_port)
        tcp_socket_server.listen(5)
        
        
        conn,addr=tcp_socket_server.accept()
        
        
        data1=conn.recv(2) #一次没有收完整
        data2=conn.recv(10)#下次收的时候,会先取旧的数据,然后取新的
        
        print('----->',data1.decode('utf-8'))
        print('----->',data2.decode('utf-8'))
        
        conn.close()
        
        
        ```

   5. 总结

      - 黏包只发生在tcp协议中
      - 从表面上看，黏包问题主要是因为发送方和接收方的缓存机制、tcp协议面向流通信的特点。 
      - 实际上，**主要还是因为接收方不知道消息之间的界限，不知道一次性提取多少字节的数据所造成的** 

   #### 2.tcp黏包解决办法

   1. 问题的根源在于，接收端不知道发送端将要传送的字节流的长度，所以解决粘包的方法就是围绕，如何让发送端在发送数据前，把自己将要发送的字节流总大小让接收端知晓，然后接收端来一个死循环接收完所有数据。 

   2. 使用struct模块

      - 我们可以借助一个模块，这个模块可以把要发送的数据长度转换成固定长度的字节。这样客户端每次接收消息之前只要先接受这个固定长度字节的内容看一看接下来要接收的信息大小，那么最终接受的数据只要达到这个值就停止，就能刚好不多不少的接收完整的数据了。 

   3. struct模块

      ```python
      import struct
      struct.pack('i','待发送的数据长度,是一个int类型')---> -2147483648 <= number <= 2147483647
      # 借助struct模块，我们知道长度数字可以被转换成一个标准大小的4字节数字。因此可以利用这个特点来预先发送数据长度。
      ```

      ![1557306460611](D:/python/s21day29/day29notes.assets/1557306460611.png)

      我们还可以把报头做成字典，字典里包含将要发送的真实数据的详细信息，然后json序列化，然后用struck将序列化后的数据长度打包成4个字节（4个字节足够用了） 

### 

1. TCP协议的黏包现象
   - 什么是黏包现象 : 
     1. 发生在发送端的粘包
        - 由于2个数据的发送时间间隔  +  数据的长度小
        - 突出tcp的优化机制,将2条信息作为一条信息发送出去; 了
        - 为了减少tcp协议中的'确认收到的网络延迟时间
     2. 接收端的黏包
        - TCP协议中数据的传输没有边界,来不及接受多条
        - 数据会在接受端的内核的缓存处黏在一起
     3. 本质  :  接受信息的边界不清晰
2. 解决tcp的黏包问题
   1. 自定义协议(信息)
      - 首先发送报头
        1. 报头是4个字节
        2. 内容是 即将发送的报文的字节长度
        3. struct 模块 
           - pack 方法可以把所有的数字都固定的转换成4字节(即将发送的报文的字节长度)
      - 然后发送报文
   2. 自定义协议2(文件)
      - 我们专门用来做文件发送的协议
        1. 先发送报头字典的字节长度
        2. 咋发送字典(包含:文件名/文件大小..........)
        3. 在发送文件的内容
3. TCP和UDP协议的特点
   - TCP是一个面向连接的,流式的(如同流水),可靠的,慢的  全双工通信
     - 邮件 文件 http web
   - UDP是一个面向数据包的,无连接的,不可靠的,快的,能完成一对一、一对多、多对一、多对多的高效通讯协议
     - 即时聊天工具 视频的在线观看
4. 三次握手
   - accept接受过程中等待客户端的连接
   - connect客户端发起一个syn链接请求
     - 如果得到了server端响应ack的同时还会再收到一个由server端发来的syc链接请求
     - client端进行回复ack之后，就建立起了一个tcp协议的链接
   - 三次握手的过程再代码中是由accept和connect共同完成的，具体的细节再socket中没有体现出来
5. 四次挥手
   - server和client端对应的在代码中都有close方法
   - 每一端发起的close操作都是一次fin的断开请求，得到'断开确认ack'之后，就可以结束一端的数据发送
   - 如果两端都发起close，那么就是两次请求和两次回复，一共是四次操作
   - 可以结束两端的数据发送，表示链接断开了

#### 8.4.2 .验证客户端的合法性

1. 客户端是提供给 用户用的 —— 登陆验证 

   - 你的用户 就能看到你的client源码了,不需要自己写客户端了

2. 客户端给机器使用(在公司内部使用客户端获取每日服务端出登录用户数据(日志模块的使用))

   ```python
   ## 方法一
   假定自定义了一个模块md5(),传入了二个字符窜,返回值是加密成md5值的字符窜
   
   # server端
   import os
   import socket
   
   def chat(conn):
       while True:
           msg = conn.recv(1024).decode('utf-8')
           print(msg)
           conn.send(msg.upper().encode('utf-8'))
           
   sk = socket.socket()
   sk.bind(('127.0.0.1',9000))
   sk.listen()
   secret_key = b'alexsb'              # 密钥
   while True:
       conn,addr = sk.accept()
       randseq = os.urandom(32)
       conn.send(randseq)
       md5code = md5(secret_key,randseq)
       ret = conn.recv(32).decode('utf-8')
       print(ret)
       if ret == md5code:
           print('是合法的客户端')
           chat(conn)
       else:
           print('不是合法的客户端')
           conn.close()
   sk.close()
   
   
   # # client.py
   
   import socket
   import time
   
   def chat(sk):
       while True:
           sk.send(b'hello')
           msg = sk.recv(1024).decode('utf-8')
           print(msg)
           time.sleep(0.5)       # 模拟用户操作的时间
   sk = socket.socket()
   sk.connect(('127.0.0.1',9000))
   secret_key = b'alexsb'
   randseq = sk.recv(32)
   md5code = get_md5(secret_key,randseq)
   sk.send(md5code.encode('utf-8'))
   chat(sk)
   sk.close()
   ```

   3 . 利用hamc模块实现摘要算法

   ```python
   def get_hmac(secret_key,randseq):          # 传入密钥  和随机的字节--->都是字节
       h = hmac.new(secret_key,randseq)
       res = h.digest()        # 返回值是个32位的字节
       
   import os
     
   os.urandom(32)    # 返回一个随机的   32个字节
   ```

#### 8.4.3   socket的不阻塞实现并发

1. socket的阻塞和非阻塞的区别

   1. 建立连接
      - 阻塞方式下，connect首先发送SYN请求到服务器，当客户端收到服务器返回的SYN的确认时，则connect返回，否则的话一直阻塞。 
      - 非阻塞方式，connect将启用TCP协议的三次握手，但是connect函数并不等待连接建立好才返回，而是立即返回，返回的错误码为EINPROGRESS,表示正在进行某种过程。 
   2. 接收连接
      - 阻塞模式下调用accept()函数，而且没有新连接时，进程会进入睡眠状态，直到有可用的连接，才返回。(类似于input等待输入时) 
      - 非阻塞模式下调用accept()函数立即返回，有连接返回客户端套接字描述符，没有新连接时，将报错误，表示本来应该阻塞。 
   3. 读操作时
      - 阻塞模式下调用read(),recv()等读套接字函数会一直阻塞住，直到有数据到来才返回。当socket缓冲区中的数据量小于期望读取的数据量时，返回实际读取的字节数。当sockt的接收缓冲区中的数据大于期望读取的字节数时，读取期望读取的字节数，返回实际读取的长度。 
      - 对于非阻塞socket而言，socket的接收缓冲区中有没有数据，read调用都会立刻返回。接收缓冲区中有数据时，与阻塞socket有数据的情况是一样的，如果接收缓冲区中没有数据，则报错, 表示该操作本来应该阻塞的，但是由于本socket为非阻塞的socket，因此立刻返回。遇到这样的情况，可以在下次接着去尝试读取。如果返回值是其它负值，则表明读取错误 。

2. 通过socket设置非阻塞实现多个client连接

   ```python
   import socket
   sk = socket.socket()
   sk.bind(('127.0.0.1',9000))
   sk.setblocking(False)             # sk.setblocking(False)     # 设置为非阻塞
   sk.listen()
   
   conn_l = []
   del_l = []
   while True:
       try:
           conn,addr = sk.accept()     # 阻塞，直到有一个客户端来连我
           print(conn)
           conn_l.append(conn)        # 有链接就把该连接对象加入到conn_l列表中操作
       except BlockingIOError:
           for c in conn_l:
               try:
                   msg = c.recv(1024).decode('utf-8')
                   if not msg:              # 接收为'', C端断开连接,利用del_l列表保存断开的C段的conn对象,循环结束后删除
                       del_l.append(c)
                       continue
                   print('-->',[msg])
                   c.send(msg.upper().encode('utf-8'))
               except BlockingIOError:pass
           for c in del_l:
               conn_l.remove(c)
           del_l.clear()
   sk.close()
   
   # socket的非阻塞io模型 + io多路复用实现的
       # 虽然非阻塞，提高了CPU的利用率，但是耗费CPU做了很多无用功
   ```

   client端

   ```python
   import socket
   import time
   sk = socket.socket()
   sk.connect(('127.0.0.1',9000))
   while True:
       sk.send(b'wusir')
       msg = sk.recv(1024)
       print(msg)
       time.sleep(0.2)
   sk.close()
   ```

#### 8.4.4   使用socketserver模块实现并发

```python
import socketserver

class Myserver(socketserver.BaseRequestHandler):
    def handle(self):  # 自动触发了handle方法，并且self.request == conn
        msg = self.request.recv(1024).decode('utf-8')
        self.request.send('1'.encode('utf-8'))
        msg = self.request.recv(1024).decode('utf-8')
        self.request.send('2'.encode('utf-8'))
        msg = self.request.recv(1024).decode('utf-8')
        self.request.send('3'.encode('utf-8'))

server = socketserver.ThreadingTCPServer(('127.0.0.1',9000),Myserver)
server.serve_forever()     # 永远连接-->接收



################################
# client1.py
import socket
sk = socket.socket()
sk.connect(('127.0.0.1',9000))
for i in range(3):
    sk.send(b'hello,yuan')
    msg = sk.recv(1024)
    print(msg)
sk.close()
```

## 第九章 并发编程

### 9.1 基础知识

#### 9.1.1 操作系统发展史

##### 9.1.1.1 人机矛盾

- cpu利用率低

##### 9.1.1.2 磁带存储+批处理

- 降低数据的读取时间
- 提高cpu利用率

##### 9.1.1.3 多道操作系统

- 数据隔离
- 时空复用
- 能够在一个任务遇到io操作时，主动把cpu让出来给其他任务使用。
- 任务切换过程也占用时间（操作系统来进行切换）

##### 9.1.1.4 分时操作系统

给时间分片，让多个任务轮流使用cpu

- 时间分片
  - cpu的轮转
  - 每个程序分配一个时间片
- 切换占用了时间，降低了cpu的利用率
- 提高了用户体验

##### 9.1.1.5 分时操作系统+多道操作系统

多个程序一起在计算机中执行

- 一个程序如果遇到了io操作，就切出去让出cpu
- 一个程序没有遇到io操作，但时间片到了，就让出cpu

补充：python中的分布式框架celery

#### 9.1.2 进程

##### 9.1.2.1 定义

运行中的程序就是一个进程。

##### 9.1.2.2 程序和进程的区别

程序：是一个文件。

进程：这个文件被cpu运行起来了

- 进程和程序之间的区别

  - 运行的程序就是一个进程

- 进程的调度 ：由操作系统完成

- 三状态 ：

  - 就绪 -system call-><-时间片到了- 运行 -io-> 阻塞-IO结束->就绪

  ​     阻塞 影响了程序运行的效率

##### 9.1.2.3 基础知识

- 进程是计算机中最小的资源分配单位
- 进程在操作系统中的唯一标识符：pid
- 操作系统调度进程的算法
  - 短作业优先算法
  - 先来先服务算法
  - 时间片轮转算法
  - 多级反馈算法

##### 9.1.2.4 并发和并行区别

并行：两个程序，两个cpu，每个程序分别占用一个cpu，自己执行自己的。

并发：多个程序，一个cpu，多个程序交替在一个cpu上执行，看起来在同时执行，但实际上仍然是串行。

#### 9.1.3 同步和异步

同步：一个动作执行时，要想执行下一个动作，必须将上一个动作停止。

异步：一个动作执行的同时，也可以执行另一个动作

#### 9.1.4 阻塞和非阻塞

阻塞：cpu不工作

非阻塞：cpu工作

同步阻塞：如conn.revc()

同步非阻塞：调用一个func()函数，函数内没有io操作

异步非阻塞：把func()函数(无io操作)放到其他任务里去执行，自己执行自己的任务。

异步阻塞：

#### 9.1.5并发并行

- 并发 ：并发是指多个程序 公用一个cpu轮流使用
- 并行 ：多个程序 多个cpu 一个cpu上运行一个程序，
  - 在一个时间点上看，多个程序同时在多个cpu上运行
- 单核 并发
  多核 多个程序跑在多个cpu上，在同一时刻 并行
  10个进程 ：
      可以利用多核  并行
      也有并发的成分

### 9.2 进程和线程

#### 9.2.1进程是计算机中最小的资源分配单位

- 进程是数据隔离的
  - 歪歪 陌陌 飞秋 qq 微信 腾讯视频
- 一个进程

​        1.和一个人通信2.一边缓存 一边看另一个电影的直播

#### 9.2.2进程

- 创建进程 时间开销大

- 销毁进程 时间开销大

- 进程之间切换 时间开销大

  ```python
  如果两个程序 分别要做两件事儿
      起两个进程
  如果是一个程序 要分别做两件事儿
      视频软件
          下载A电影
          下载B电影
          播放C电影
      启动三个进程来完成上面的三件事情，但是开销大
  ```



#### 9.2.4进程模块

```python
from multiprocessing import Process
# pid   process id
# ppid  parent process id
# os.getpid()    /   os.getppid()  找到进程的id  ,找到父进程id

```

1. 在pycharm中启动的所有py程序都是pycharm的子进程

2. 父进程 在父进程中创建子进程

   ```python
   import os
   import time
   from multiprocessing import Process
   
   def func():
       print('start',os.getpid())
       time.sleep(1)
       print('end',os.getpid())
   
   if __name__ == '__main__':
       p = Process(target=func)
       p.start()   异步 调用开启进程的方法 但是并不等待这个进程真的开启
       print('main :',os.getpid())
   ```

   ```python
   import os
   import time
   from multiprocessing import Process
   
   def eat():
       print('start eating',os.getpid())
       time.sleep(1)
       print('end eating',os.getpid())
   
   def sleep():
       print('start sleeping',os.getpid())
       time.sleep(1)
       print('end sleeping',os.getpid())
   
   if __name__ == '__main__':
       p1 = Process(target=eat)    # 创建一个即将要执行eat函数的进程对象
       p1.start()                  # 开启一个进程
       p2 = Process(target=sleep)  # 创建一个即将要执行sleep函数的进程对象
       p2.start()                  # 开启进程
       print('main :',os.getpid())
   ```

##### 9.2.4.1注意win和linux系统在创建进程是的区别

1. 操作系统创建进程的方式不同
2. windows操作系统执行开启进程的代码
3. 实际上新的子进程需要通过import父进程的代码来完成数据的导入工作
   - 所以有一些内容我们只希望在父进程中完成，就写在if `__name__` == `__main__`:缩进处即可

##### 8.2.4.2父进程和子进程的关系

```python
import os
import time
from multiprocessing import Process
def func():
    print('start',os.getpid())
    time.sleep(10)
    print('end',os.getpid())
    
if __name__ == '__main__':
    p = Process(target=func)
    p.start()   #  异步 调用开启进程的方法 但是并不等待这个进程真的开启
    print('main :',os.getpid())
    #  主进程没结束 ：等待子进程结束
    #  主进程负责回收子进程的资源
    #  如果子进程执行结束，父进程没有回收资源，那么这个子进程会变成一个僵尸进程
```

1. 主进程的结束逻辑
   1. 主进程的代码结束
   2. 所有的子进程结束
   3. 给子进程回收资源
   4. 主进程结束

8.2.4.3 Process模块的join方法

1. join方法 ：阻塞，直到子进程结束就结束

   ```python
   # 开启10个进程，给公司的5000个人发邮件，发送完邮件之后，打印一个消息“5000封邮件已发送完毕”
   
   import time
   import random
   from multiprocessing import Process
   def send_mail(a):
       time.sleep(random.random())
       print('发送了一封邮件',a)
   
   if __name__ == '__main__':
       l = []
       for i in range(10):
           p = Process(target=send_mail,args=(i,))
           p.start()   #  # 异步 非阻塞
           l.append(p)
       print(l)
       for p in l:p.join()     # # 同步 阻塞 直到p对应的进程结束之后才结束阻塞  
       print('5000封邮件已发送完毕')
   ```

##### 9.2.4.3 总结

1. 开启一个进程

   - 函数名(参数1,参数2)
   - from multiprocessing import Process
   - p = Process(target=函数名,args=(参数1,参数2))
   - p.start()

2. 父进程  和 子进程

3. 父进程会等待着所有的子进程结束之后才结束

   - 为了回收资源

4. 进程开启的过程中windows和 linux/ios之间的区别

   - 开启进程的过程需要放在if `__name__` == '`__main__`'下

   ​        windows中 相当于在子进程中把主进程文件又从头到尾执行了一遍
   ​            除了放在if `__name__` == '`__main__`'下的代码
   ​        linux中 不执行代码,直接执行调用的func函数

5. join方法

   ​    把一个进程的结束事件封装成一个join方法
   ​    执行join方法的效果就是 阻塞直到这个子进程执行结束就结束阻塞

```python
#  在多个子进程中使用join
p_l= []
for i in range(10):
    p = Process(target=函数名,args=(参数1,参数2))
    p.start()
    p_l.append(p)
for p in p_l:p.join()
# 所有的子进程都结束之后要执行的代码写在这里
```

##### 9.2.4.4 `__name__`补充讲解

```python
if __name__ == '__main__':
    # 控制当这个py文件被当作脚本直接执行的时候，就执行这里面的代码
    # 当这个py文件被当作模块导入的时候，就不执行这里面的代码
    print('hello hello')
# __name__ == '__main__'
    # 执行的文件就是__name__所在的文件
# __name__ == '文件名'
    # __name__所在的文件被导入执行的时候
```

##### 9.2.4.5协程相关

1. 并发编程

   - 不会有大量的例子和习题

   ​        1. 进程 在我们目前完成的一些项目里是不常用到的
   ​        2.线程 后面的爬虫阶段经常用
   ​            前端的障碍
   ​        3.协程
   ​            异步的框架 异步的爬虫模块

2. 为什么进程用的不多，但是还要来讲

   ​        1.你可能用不到，但是未来你去做非常复杂的数据分析或者是高计算的程序
   ​        2.进程和线程的很多模型很多概念是基本一致的

#### 9.2.5进程模块进阶

1. 线程

   - 线程是进程的一部分，每个进程中至少有一个线程, 是能被CPU调度的最小单位 .   
   - 一个进程中的多个线程是可以共享这个进程的数据的  —— 数据共享
   - 线程的创建、销毁、切换 开销远远小于进程  —— 开销小

2. multiprocessing 进程

   1. 使用方法 : p = Process(target=函数名,args=(参数1,))

   2. 如何创建一个进程对象

      - 对象和进程之间的关系
      - 进程对象和进程并没有直接的关系
      - 只是存储了一些和进程相关的内容
      - 此时此刻，操作系统还没有接到创建进程的指令

   3. 如何开启一个进程

      - 通过p.start()开启了一个进程--这个方法相当于给了操作系统一条指令
      - start方法 的 非阻塞和异步的特点
      - 在执行开启进程这个方法的时候
      - 我们既不等待这个进程开启，也不等待操作系统给我们的响应
      - 这里只是负责通知操作系统去开启一个进程
      - 开启了一个子进程之后，主进程的代码和子进程的代码完全异步

   4. 父进程和子进程之间的关系

      - 父进程会等待子进程结束之后才结束
      - 为了回收子进程的资源

   5. 不同操作系统中进程开启的方式

      - windows 通过（模块导入）再一次执行父进程文件中的代码来获取父进程中的数据
        - 所以只要是不希望被子进程执行的代码，就写在if `__name__` == `__main__`下
      - 因为在进行导入的时候父进程文件中的if `__name__` == `__main__`
      - linux/ios
        - 正常的写就可以，没有if __name__ == '__main__'这件事情了

      ​    5.如何确认一个子进程执行完毕
      ​        Process().join方法
      ​        开启了多个子进程，等待所有子进程结束

#### 9.2.6守护进程

```python
# 有一个参数可以把一个子进程设置为一个守护进程
if __name__ == '__main__':
    p = Process(target=son1,args=(1,2))
    p.daemon = True    # daemon = True   
    p.start()      # 把p子进程设置成了一个守护进程
    p2 = Process(target=son2)
    p2.start()
    time.sleep(2)
# 守护进程是随着主进程的代码结束而结束的
    # 生产者消费者模型的时候
    # 和守护线程做对比的时候
# 所有的子进程都必须在主进程结束之前结束，由主进程来负责回收资源

```

1. 会随着主进程的结束而结束。

   主进程创建守护进程

   　　其一：守护进程会在主进程代码执行结束后就终止

   　　其二：守护进程内无法再开启子进程,否则抛出异常：AssertionError: daemonic processes are not allowed to have children

   注意：进程之间是互相独立的，主进程代码运行结束，守护进程随即终止

#### 9.2.7   terminate()方法

```python
#  p.terminate():强制终止进程p，不会进行任何清理操作，如果p创建了子进程，该子进程就成了僵尸进程，使用该方法需要特别小心这种情况。如果p还保存了一个锁那么也将不会被释放，进而导致死锁

```

```python
import time
from multiprocessing import Process

def son1():
    while True:
        print('is alive')
        time.sleep(0.5)

if __name__ == '__main__':
    p = Process(target=son1)
    p.start()      # 异步 非阻塞
    print(p.is_alive())
    time.sleep(1)
    p.terminate()   # 异步的 非阻塞
    print(p.is_alive())   # 进程还活着 因为操作系统还没来得及关闭进程
    time.sleep(0.01)
    print(p.is_alive())   # 操作系统已经响应了我们要关闭进程的需求，再去检测的时候，得到的结果是进程已经结束了

```

### 9.3 Process模块面向对象

```python
# Process类
# 开启进程的方式
    面向函数
        def 函数名:要在子进程中执行的代码
        p = Process(target= 函数名,args=(参数1，))
    面向对象
        class 类名(Process):
            def __init__(self,参数1，参数2):   如果子进程不需要参数可以不写
                self.a = 参数1
                self.b = 参数2
                super().__init__()
            def run(self):      # 必须为run方法
                要在子进程中执行的代码
        p = 类名(参数1，参数2)
    Process提供的操作进程的方法
        p.start() 开启进程      异步非阻塞
        p.terminate() 结束进程  异步非阻塞

        p.join()     同步阻塞
        p.isalive()  获取当前进程的状态
        daemon = True 设置为守护进程，守护进程永远在主进程的代码结束之后自动结束

```

#### 9.3.1 并发和加锁

- 实现并发完成了许多,但如果当多个进程使用同一份数据资源的时候，就会引发数据安全或顺序混乱问题。 

1. 在这里我们通过给程序加锁(Lock),这种情况虽然使用加锁的形式实现了顺序的执行，但是程序又重新变成串行了，这样确实会浪费了时间，却保证了数据的安全。 

   ```python
   # 由并发变成了串行,牺牲了运行效率,但避免了竞争
   import os
   import time
   import random
   from multiprocessing import Process,Lock
   
   def work(lock,n):
       lock.acquire()            # 
       print('%s: %s is running' % (n, os.getpid()))
       time.sleep(random.random())
       print('%s: %s is done' % (n, os.getpid()))
       lock.release()            #
   if __name__ == '__main__':
       lock=Lock()
       for i in range(3):
           p=Process(target=work,args=(lock,i))
           p.start()
           
    # 在主进程中实例化 lock = Lock()
   # 把这把锁传递给子进程
   # 在子进程中 对需要加锁的代码 进行 with lock：
       # with lock相当于lock.acquire()和lock.release()
   # 在进程中需要加锁的场景
       # 共享的数据资源（文件、数据库）
       # 对资源进行修改、删除操作
   # 加锁之后能够保证数据的安全性 但是也降低了程序的执行效率
   
   
   
   # lock.acquire()  /  lock.release() 支持上下文.可以with lock:
   
   ```

#### 9.3.2进程间的通信**IPC**(Inter-Process Communication)

**队列 : (Queue  --> 先进先出)**

1. 进程间是数据隔离的

2. 创建共享的进程队列，Queue是多进程安全的队列，可以使用Queue实现多进程之间的数据传递。 

3. ```python
   # Queue基于 天生就是数据安全的
       # 文件家族的socket pickle lock
   # pipe 管道(不安全的) = 文件家族的socket pickle
   # 队列 = 管道 + 锁
   
   ```

4. Queue的使用

   ```python
   import queue
   
   from multiprocessing import Queue
   q = Queue(5)
   q.put(1)
   q.put(2)
   q.put(3)
   q.put(4)
   q.put(5)   # 当队列为满的时候再向队列中放数据 队列会阻塞
   print('5555555')
   try:
       q.put_nowait(6)  # 当队列为满的时候再向队列中放数据 会报错并且会丢失数据.推荐使用
   except queue.Full:
       pass
   print('6666666')
   
   print(q.get())
   print(q.get())
   print(q.get())   # 在队列为空的时候会发生阻塞
   print(q.get())   # 在队列为空的时候会发生阻塞
   print(q.get())   # 在队列为空的时候会发生阻塞
   try:
       print(q.get_nowait())   # 在队列为空的时候 直接报错  ,推荐使用
   except queue.Empty:pass
   
   
   ```

   我们记住使用get_nowait()  和  put_nowait()   

#### 9.3.3多进程实现并发socket服务端

```python
import socket
from multiprocessing import Process
def chat(conn):
    while True:
        try:
            ret = conn.recv(1024).decode('utf-8')
            conn.send(ret.upper().encode('utf-8'))
        except ConnectionResetError:
            break
a
if __name__ == '__main__':
    sk = socket.socket()
    sk.bind(('127.0.0.1', 9000))
    sk.listen()
    while True:
        conn,_ = sk.accept()
        Process(target=chat,args=(conn,)).start()

```

#### 9.3.4 ipc机制

- 进程之间相互通信
- ipc机制-->队列   管道
- 第三方工具(软件)提供给我们的IPC机制
  - redis
  - memcache
  - kafka等等
  - 并发需求   ,高可用  ,断电保存数据

### 9.4生产者和消费者模式

**在并发编程中使用生产者和消费者模式能够解决绝大多数并发问题。该模式通过平衡生产线程和消费线程的工作能力来提高程序的整体处理数据的速度。**

- 一个进程就是一个生产者,一个进程就是一个消费者

- 队列:生产者和消费者之间的容器就是队列

  什么是生产者消费者模式

  **生产者消费者模式是通过一个容器来解决生产者和消费者的强耦合问题。生产者和消费者彼此之间不直接通讯，而通过阻塞队列来进行通讯，所以生产者生产完数据之后不用等待消费者处理，直接扔给阻塞队列，消费者不找生产者要数据，而是直接从阻塞队列里取，阻塞队列就相当于一个缓冲区，平衡了生产者和消费者的处理能力。**

- 在生产者在生产完后就结束了,往队列中再发一个结束信号，这样消费者在接收到结束信号后就可以break出死循环。 
  结束信号可以生产者发送,也可以主进程在生产者进程结束后发送.

- 但若是有多个生产者和消费者时 , 就必须得在多个升舱者全部生产完毕后,才能发送结束信号,并且有多少个消费者,就需要发送几次结束信号.

- 具体描述

  ```py
  什么是生产者消费者模型
      把一个产生数据并且处理数据的过程解耦
  	让生产的数据的过程和处理数据的过程达到一个工作效率上的平衡
      中间的容器，在多进程中我们使用队列或者可被join的队列，做到控制数据的量
          当数据过剩的时候，队列的大小会控制这生产者的行为
          当数据严重不足的时候，队列会控制消费者的行为
          并且我们还可以通过定期检查队列中元素的个数来调节生产者消费者的个数
  
  ```

  

#### 9.4.1 共享队列(**JoinableQueue([maxsize])**  

我们使用共享队列,创建可连接的共享进程队列。这就像是一个Queue对象，但队列允许项目的使用者通知生产者项目已经被成功处理。通知进程是使用共享的信号和条件变量来实现的。  

```python
"""JoinableQueue的实例p除了与Queue对象相同的方法之外，还具有以下方法：

q.task_done() 
使用者使用此方法发出信号，表示q.get()返回的项目已经被处理。如果调用此方法的次数大于从队列中删除的项目数量，将引发ValueError异常。

q.join() 
生产者将使用此方法进行阻塞，直到队列中所有项目均被处理。阻塞将持续到为队列中的每个项目均调用q.task_done()方法为止。 
下面的例子说明如何建立永远运行的进程，使用和处理队列上的项目。生产者将项目放入队列，并等待它们被处理。"""

```

```python
from multiprocessing import Process,JoinableQueue
import time,random,os
def consumer(q):
    while True:
        res=q.get()
        time.sleep(random.randint(1,3))
        print('\033[45m%s 吃 %s\033[0m' %(os.getpid(),res))
        q.task_done() #向q.join()发送一次信号,证明一个数据已经被取走了

def producer(name,q):
    for i in range(10):
        time.sleep(random.randint(1,3))
        res='%s%s' %(name,i)
        q.put(res)
        print('\033[44m%s 生产了 %s\033[0m' %(os.getpid(),res))
    q.join() #生产完毕，使用此方法进行阻塞，直到队列中所有项目均被处理。


if __name__ == '__main__':
    q=JoinableQueue()
    #生产者们:即厨师们
    p1=Process(target=producer,args=('包子',q))
    p2=Process(target=producer,args=('骨头',q))
    p3=Process(target=producer,args=('泔水',q))

    #消费者们:即吃货们
    c1=Process(target=consumer,args=(q,))
    c2=Process(target=consumer,args=(q,))
    c1.daemon=True
    c2.daemon=True

    #开始
    p_l=[p1,p2,p3,c1,c2]
    for p in p_l:
        p.start()

    p1.join()
    p2.join()
    p3.join()
    print('主') 
    
    #主进程等--->p1,p2,p3等---->c1,c2
    #p1,p2,p3结束了,证明c1,c2肯定全都收完了p1,p2,p3发到队列的数据
    #因而c1,c2也没有存在的价值了,不需要继续阻塞在进程中影响主进程了。应该随着主进程的结束而结束,所以设置成守护进程就可以了。

```

#### 9.4.2 使用manager 来进程间互相通信

```python
from multiprocessing import Manager,Process,Lock

def func(dic,lock):
    with lock:
        dic['count'] -= 1

if __name__ == '__main__':
    # m = Manager()
    with Manager() as m:
        l = Lock()
        dic = m.dict({'count':100})
        p_l = []
        for i in range(100):
            p = Process(target=func,args=(dic,l))
            p.start()
            p_l.append(p)
        for p in p_l:p.join()
        print(dic)


# mulprocessing中有一个manager类
# 封装了所有和进程相关的 数据共享 数据传递
# 相关的数据类型
# 但是对于 字典 列表这一类的数据操作的时候会产生数据不安全
# 需要加锁解决问题，并且需要尽量少的使用这种方式

```

### 9.5进程和线程的区别



#### 9.5 线程

#### 9.5.1线程

1. 是进程中的一部分，每一个进程中至少有一个线程

- 进程是计算机中最小的资源分配单位（进程是负责圈资源）
- 线程是计算机中能被CPU调度的最小单位 （线程是负责执行具体代码的）

1. 开销

   - 线程的创建，也需要一些开销（一个存储局部变量的结构，记录状态）
   - 创建、销毁、切换开销远远小于进程

2. python中的线程比较特殊，所以进程也有可能被用到

   - 进程 ：数据隔离 开销大 同时执行几段代码
   - 线程 ：数据共享 开销小 同时执行几段代码

3. **线程**：**开销小 数据共享 是进程的一部分，不能独立存在 本身可以利用多核**

4. cpython解释器 不能实现多线程利用多核

   **原因 :** 锁 ：GIL 全局解释器锁

   ```python
   #     保证了整个python程序中，只能有一个线程被CPU执行
   """原因：cpython解释器中特殊的垃圾回收机制
   GIL锁导致了线程不能并行，可以并发
   所以使用所线程并不影响高io型的操作
   只会对高计算型的程序由效率上的影响
   遇到高计算 ： 多进程 + 多线程
                 分布式"""
   
   ```

5. GIL锁
       全局解释器锁
       cpython解释器中的机制
       导致了在同一个进程中多个线程不能同时利用多核 —— python的多线程只能是并发不能是并行

#### 9.5.2 threading类创建线程

1. multiprocessing 是完全仿照这threading的类写的,方法几乎一致

   ```python
   import os
   import time
   from threading import Thread
   multiprocessing 是完全仿照这threading的类写的
   def func():
       print('start son thread')
       time.sleep(1)
       print('end son thread',os.getpid())
     #    启动线程 start
   Thread(target=func).start()
   print('start',os.getpid())
   time.sleep(0.5)
   print('end',os.getpid())
   
   ```

2. 开启多个子线程

   ```python
   开启多个子线程
   def func(i):
       print('start son thread',i)
       time.sleep(1)
       print('end son thread',i,os.getpid())
   #
   for i in range(10):
       Thread(target=func,args=(i,)).start()
   print('main')
    # 主线程什么时候结束？等待所有子线程结束之后才结束
   #  主线程如果结束了，主进程也就结束了
   
   ```

3. join方法

   ```python
   # join方法  阻塞 直到子线程执行结束
   # def func(i):
   #     print('start son thread',i)
   #     time.sleep(1)
   #     print('end son thread',i,os.getpid())
   # t_l = []
   # for i in range(10):
   #     t = Thread(target=func,args=(i,))
   #     t.start()
   #     t_l.append(t)
   # for t in t_l:t.join()
   # print('子线程执行完毕')
   
   ```

4. 使用面向对象的方式启动线程

   ```python
   class MyThread(Thread):
       def __init__(self,i):
           self.i = i
           super().__init__()
       def run(self):
           print('start',self.i,self.ident)
           time.sleep(1)
           print('end',self.i)
    # 线程就不用了main 了
   for i in range(10):
       t = MyThread(i)
       t.start()
       print(t.ident)
   
   ```

5. 线程里的一些其他方法

   ```python
   from threading import current_thread,enumerate,active_count
   def func(i):
       t = current_thread()
       print('start son thread',i,t.ident)
       time.sleep(1)
       print('end son thread',i,os.getpid())
   
   t = Thread(target=func,args=(1,))
   t.start()
   print(t.ident)
   print(current_thread().ident)   #  水性杨花 在哪一个线程里，current_thread()得到的就是这个当前线程的信息
   print(enumerate())  # 返回一个包含正在运行的线程的list。正在运行指线程启动后、结束前，不包括启动前和终止后的线程。
   print(active_count())   =====len(enumerate())  # 返回正在运行的线程数量，与len(threading.enumerate())有相同的结果。
   
   
   
   ```

6. terminate 结束进程
   在线程中不能从主线程结束一个子线程

7. 守护线程

   ```python
   import time
   from threading import Thread
   def son1():
       while True:
           time.sleep(0.5)
           print('in son1')
   def son2():
       for i in range(5):
           time.sleep(1)
           print('in son2')
   t =Thread(target=son1)
   t.daemon = True
   t.start()
   Thread(target=son2).start()
   time.sleep(3)
   守护线程一直等到所有的非守护线程都结束之后才结束
   除了守护了主线程的代码之外也会守护子线程
   小绿本 ：p38 ：34题
   
   
   ```

   **无论是进程还是线程，都遵循：守护xx会等待主xx运行完毕后被销毁。****需要强调的是：运行完毕并非终止运行**

   ```
   #1.对主进程来说，运行完毕指的是主进程代码运行完毕
   #2.对主线程来说，运行完毕指的是主线程所在的进程内所有非守护线程统统运行完毕，主线程才算运行完毕
   
   ```

   ```
   #1 主进程在其代码结束后就已经算运行完毕了（守护进程在此时就被回收）,然后主进程会一直等非守护的子进程都运行完毕后回收子进程的资源(否则会产生僵尸进程)，才会结束，
   #2 主线程在其他非守护线程运行完毕后才算运行完毕（守护线程在此时就被回收）。因为主线程的结束意味着进程的结束，进程整体的资源都将被回收，而进程必须保证非守护线程都运行完毕后才能结束
   
   ```

#### 9.5.3 总结

- threading模块

  - 创建线程 ：面向函数 面向对象

- 线程中的几个方法：
      1. 属于线程对象t   的 方法 `.start(),t.join()`

  2. 守守护线程 `t.daemon = True` 等待所有的非守护子线程都结束之后才结束

  3. 非守护线程不结束，主线程也不结束
        主线程结束了，主进程也结束
         结束顺序 ：`  非守护线程结束 -->主线程结束-->主进程结束-->主进程结束 --> 守护线程也结束`

  4. threading模块的函数 ：

     1. current_thread 在哪个线程中被调用，就返回当前线程的对象
     2. 活着的线程，包括主线程
     3. enumerate 返回当前活着的线程的对象列表
     4. active_count 返回当前或者的线程的个数 ,`-->len(enumerate())测试`

  5. 进程和线程的效率差，线程的开启、关闭、切换效率更高

  6. 线程间的数据共享的效果

     ```python
     #   += -= *= /= ，多个线程对同一个文件进行写操作,线程间的数据不是很安全
     
     ```

     

### 9.6互斥锁 和 递归锁

1. 线程中会产生数据不安全,(共享内存)

2. ```python
   # 即便是线程 即便有GIL 也会出现数据不安全的问题
       # 1.操作的是全局变量
       # 2.做一下操作
           # += -= *= /+ 先计算再赋值才容易出现数据不安全的问题
           # 包括 lst[0] += 1  dic['key']-=1
   
   ```

3. 从threading类中也有锁lock,保护程序间的数据安全`---->互斥锁`

   ```python
   # 互斥锁是锁中的一种:在同一个线程中，不能连续acquire多次
   # from threading import Lock
   # lock = Lock()
   # lock.acquire()
   # print('*'*20)
   # lock.release()
   # lock.acquire()
   # print('-'*20)
   # lock.release()
   
   ```

4. 单例模式的加锁形式

   ```python
   from threading import Lock
   class A:
       __instance = None
       lock = Lock()
   
       def __new__(cls, *args, **kwargs):
           with cls.lock:
               if not cls.__instance:
                   time.sleep(0.1)
                   cls.__instance = super().__new__(cls)
           return cls.__instance
   
   ```

5. 互斥锁在多次acquire 后,会出现死锁现象,我们引入了递归锁来解决

   ```python
   # 锁
       # 互斥锁
           # 在一个线程中连续多次acquire会死锁
       # 递归锁
           # 在一个线程中连续多次acquire不会死锁
       # 死锁现象
           # 死锁现象是怎么发生的？
               # 1.有多把锁，一把以上
               # 2.多把锁交替使用
       # 怎么解决
           # 递归锁 —— 将多把互斥锁变成了一把递归锁
               # 快速解决问题
               # 效率差
           # ***递归锁也会发生死锁现象，多把锁交替使用的时候
           # 优化代码逻辑
               # 可以使用互斥锁 解决问题
               # 效率相对好
               # 解决问题的效率相对低
   
   ```

6. 递归锁

   ```python
    # 在同一个线程中，可以连续acuqire多次不会被锁住-->实际是把万能钥匙
   
   # 递归锁：
   # 好 ：在同一个进程中多次acquire也不会发生阻塞
   # 不好 ：占用了更多资源
   
   import time
   from threading import RLock, Thread
   # noodle_lock = RLock()
   # fork_lock = RLock()
   noodle_lock = fork_lock = RLock()
   print(noodle_lock, fork_lock)
   
   
   def eat1(name, noodle_lock, fork_lock):
       noodle_lock.acquire()
       print('%s抢到面了' % name)
       fork_lock.acquire()
       print('%s抢到叉子了' % name)
       print('%s吃了一口面' % name)
       time.sleep(0.1)
       fork_lock.release()
       print('%s放下叉子了' % name)
       noodle_lock.relea()
       print('%s放下面了' % sename)
   
   
   def eat2(name, noodle_lock, fork_lock):
       fork_lock.acquire()
       print('%s抢到叉子了' % name)
       noodle_lock.acquire()
       print('%s抢到面了' % name)
       print('%s吃了一口面' % name)
       time.sleep(0.1)
       noodle_lock.release()
       print('%s放下面了' % name)
       fork_lock.release()
       print('%s放下叉子了' % name)
   
   
   lst = ['alex', 'wusir', 'taibai', 'yuan']
   Thread(target=eat1, args=(lst[0], noodle_lock, fork_lock)).start()
   Thread(target=eat2, args=(lst[1], noodle_lock, fork_lock)).start()
   Thread(target=eat1, args=(lst[2], noodle_lock, fork_lock)).start()
   Thread(target=eat2, args=(lst[3], noodle_lock, fork_lock)).start()
   
   ```

7. 换种思路,互斥锁解决死锁问题

   ```python
   import time
   from threading import Lock, Thread
   lock = Lock()
   
   
   def eat1(name, noodle_lock, fork_lock):
       lock.acquire()
       print('%s抢到面了' % name)
       print('%s抢到叉子了' % name)
       print('%s吃了一口面' % name)
       time.sleep(0.1)
       print('%s放下叉子了' % name)
       print('%s放下面了' % name)
       lock.release()
   
   
   def eat2(name, noodle_lock, fork_lock):
       lock.acquire()
       print('%s抢到叉子了' % name)
       print('%s抢到面了' % name)
       print('%s吃了一口面' % name)
       time.sleep(0.1)
       print('%s放下面了' % name)
       print('%s放下叉子了' % name)
       lock.release()
   
   ```

#### 9.6.1 锁总结

1. 锁 - 都可以维护线程之间的数据安全
   - 互斥锁 ：一把锁不能在一个线程中连续acquire，开销小
   - 递归锁  ：一把锁可以连续在一个线程中acquire多次，acquire多少次就release多少次，开销大 
2. 死锁现象
   - 在某一些线程中出现陷入阻塞并且永远无法结束阻塞的情况就是死锁现象
   - 出现死锁.多把锁+交替使用
   - 互斥锁在一个线程中连续acquire
   - 避免死锁
         在一个线程中只有一把锁，并且每一次acquire之后都要release

### 9.7队列模块

```python
import queue
from queue import Queue    # 先进先出队列
from queue import LifoQueue  # 后进先出队列`数据结构栈`   
"""
应用场景:
# 先进先出
    # 写一个server，所有的用户的请求放在队列里
        # 先来先服务的思想
# 后进先出
    # 算法
# 优先级队列
    # 自动的排序
    # 抢票的用户级别 100000 100001
    # 告警级别
"""

lfq = LifoQueue(4)
lfq.put(1)
lfq.put(3)
lfq.put(2)
print(lfq.get())
print(lfq.get())
print(lfq.get())


# 
from queue import PriorityQueue          # 优先级队列
pq = PriorityQueue()
pq.put((10,'alex'))
pq.put((6,'wusir'))
pq.put((20,'yuan'))
print(pq.get())
print(pq.get())
print(pq.get())

```

### 9.8 进程池和线程池

#### 9.8.1预先的开启固定个数的进程数，

当任务来临的时候，直接提交给已经开好的进程

让这个进程去执行就可以了
节省了进程，线程的开启 关闭 切换都需要时间
并且减轻了操作系统调度的负担

1. 模块 concurrent.futures  创建池

   ```python
   import os
   import time
   import random
   from concurrent.futures import ProcessPoolExecutor
   # submit + shutdown
   
   def func():
       print('start',os.getpid())
       time.sleep(random.randint(1,3))
       print('end', os.getpid())
   if __name__ == '__main__':
       p = ProcessPoolExecutor(5)
       for i in range(10):
           p.submit(func)      # 把任务提交到进程池
       p.shutdown()   # 关闭池之后就不能继续提交任务，并且会阻塞，直到已经提交的任务完成
       print('main',os.getpid())
   
   ```

   ```python
   #  任务的参数 + 返回值
   def func(i,name):
       print('start',os.getpid())
       time.sleep(random.randint(1,3))
       print('end', os.getpid())
       return '%s * %s'%(i,os.getpid())
   if __name__ == '__main__':
       p = ProcessPoolExecutor(5)
       ret_l = []
       for i in range(10):
           ret = p.submit(func,i,'alex')   # 直接传参数
           ret_l.append(ret)
       for ret in ret_l:
           print('ret-->',ret.result())  #    ret.result() 同步阻塞
       print('main',os.getpid())
   
   ```

2. 进程池的缺点 : 一个池中的任务个数限制了我们程序的并发个数

#### 8.8.2线程池的开启

```python
from concurrent.futures import ThreadPoolExecutor
def func(i):
    print('start', os.getpid())
    time.sleep(random.randint(1,3))
    print('end', os.getpid())
    return '%s * %s'%(i,os.getpid())

tp = ThreadPoolExecutor(20)

ret_l = []
for i in range(10):
    ret = tp.submit(func,i)
    ret_l.append(ret)
    
tp.shutdown()
print('main')
for ret in ret_l:
    print('------>',ret.result())

```

```python
from concurrent.futures import ThreadPoolExecutor

def func(i):
    print('start', os.getpid())
    time.sleep(random.randint(1,3))
    print('end', os.getpid())
    return '%s * %s'%(i,os.getpid())
tp = ThreadPoolExecutor(20)
#  使用map替换for  和  submit的用法
ret = tp.map(func,range(20))
for i in ret:
    print(i)
ret_l = []
for i in range(10):
    ret = tp.submit(func,i)
    ret_l.append(ret)
tp.shutdown()
print('main')

```

#### 9.8.3 回调函数(add_done_callback)

```python
import requests
from concurrent.futures import ThreadPoolExecutor


def get_page(url):
    res = requests.get(url)
    return {'url': url, 'content': res.text}


def parserpage(ret):
    dic = ret.result()
    print(dic['url'])


tp = ThreadPoolExecutor(5)
url_lst = [
    'http://www.baidu.com',   3
    'http://www.cnblogs.com',  1
    'http://www.douban.com',  1
    'http://www.tencent.com',
    'http://www.cnblogs.com/Eva-J/articles/8306047.html',
    'http://www.cnblogs.com/Eva-J/articles/7206498.html',
]
ret_l = []
for url in url_lst:
    ret = tp.submit(get_page, url)
    ret_l.append(ret)
    ret.add_done_callback(parserpage)

```

#### 9.8.4 总结

```python
# ThreadPoolExcutor
# ProcessPoolExcutor

# 创建一个池子
tp = ThreadPoolExcutor(池中线程/进程的个数)
# 异步提交任务
ret = tp.submit(函数,参数1，参数2....)
# 获取返回值
ret.result()
# 在异步的执行完所有任务之后，主线程/主进程才开始执行的代码
tp.shutdown() 阻塞 直到所有的任务都执行完毕
# map方法
# ret = tp.map(func,iterable) 迭代获取iterable中的内容，作为func的参数，让子线程来执行对应的任务
for i in ret: 每一个都是任务的返回值
# 回调函数
ret.add_done_callback(函数名)
# 要在ret对应的任务执行完毕之后，直接继续执行add_done_callback绑定的函数中的内容，并且ret的结果会作为参数返回给绑定的函数

```

1. 应用场景

   ```python
   # 池
       # from concurrent.futrues import ThreadPoolExecutor
       # 1.是单独开启线程进程还是池？
           # 如果只是开启一个子线程做一件事情，就可以单独开线程
           # 有大量的任务等待程序去做，要达到一定的并发数，开启线程池
           # 根据你程序的io操作也可以判定是用池还是不用池？
               # socket的server 大量的阻塞io   recv recvfrom socketserver
               # 爬虫的时候 池
       # 2.回调函数add_done_callback
               # 执行完子线程任务之后直接调用对应的回调函数
               # 爬取网页 需要等待数据传输和网络上的响应高IO的 -- 子线程
               # 分析网页 没有什么IO操作 -- 这个操作没必要在子线程完成，交给回调函数
       # 3.ThreadPoolExecutor中的几个常用方法
           # tp = ThreadPoolExecutor(cpu*5)
           # obj = tp.submit(需要在子线程执行的函数名,参数)
           # obj
               # 1.获取返回值 obj.result() 是一个阻塞方法
               # 2.绑定回调函数 obj.add_done_callback(子线程执行完毕之后要执行的代码对应的函数)
           # ret = tp.map(需要在子线程执行的函数名,iterable)
               # 1.迭代ret，总是能得到所有的返回值
           # shutdown
               # tp.shutdown()
   
   ```

2. 锁的问题

   ```python
   # 进程 和 线程都有锁
       # 所有在线程中能工作的基本都不能在进程中工作
       # 在进程中能够使用的基本在线程中也可以使用
   
   ```

3. 在多进程里开多线程

   ```python
   在多进程里启动多线程
   import os
   from multiprocessing import Process
   from threading import Thread
   #
   def tfunc():
       print(os.getpid())
   def pfunc():
       print('pfunc-->',os.getpid())
       Thread(target=tfunc).start()
   #
   if __name__ == '__main__':
       Process(target=pfunc).start()
   
   ```

### 9.9协程

- 进程是计算机最小的资源调度单位
- 线程是能被CPU执行的最小单位-->线程是由 操作系统 调度,由操作系统负责切换的
- 协程
  - 用户级别的,由我们自己写的python代码来控制切换的
    是操作系统不可见的

#### 9.9.1 协程解析

```python
# 在Cpython解释器下 - 协程和线程都不能利用多核,都是在一个CPU上轮流执行
    # 由于多线程本身就不能利用多核
    # 所以即便是开启了多个线程也只能轮流在一个CPU上执行
    # 协程如果把所有任务的IO操作都规避掉,只剩下需要使用CPU的操作
    # 就意味着协程就可以做到题高CPU利用率的效果

```

1. 多线程和协程
   - 线程 切换需要操作系统,开销大,操作系统不可控,给操作系统的压力大
     操作系统对IO操作的感知更加灵敏
   - 协程 切换需要python代码,开销小,用户操作可控,完全不会增加操作系统的压力
         用户级别能够对IO操作的感知比较低
   - 协程 :能够在一个线程下的多个任务之间来回切换,那么每一个任务都是一个协程
     - 两种切换方式
       - 原生python完成   `yield  asyncio`
       - C语言完成的python模块    `greenlet    gevent`

#### 9.9.2 gevent 模块 

1. grennlet 模块

```PYTHON
# greenlet 第三方模块
import time
from  greenlet import greenlet
#
def eat():
    print('wusir is eating')
    time.sleep(0.5)
    g2.switch()
    print('wusir finished eat')
#
def sleep():
    print('小马哥 is sleeping')
    time.sleep(0.5)
    print('小马哥 finished sleep')
    g1.switch()
#
g1 = greenlet(eat)
g2 = greenlet(sleep)
g1.switch()

```

2. gvent 模块

   ```python
   import time
   print('-->',time.sleep)
   import gevent
   from gevent import monkey
   monkey.patch_all()             # 魔术方法,在里面重写py的各种阻塞代码
   def eat():
       print('wusir is eating')
       print('in eat: ',time.sleep)
       time.sleep(1)
       print('wusir finished eat')
   #
   def sleep():
       print('小马哥 is sleeping')
       time.sleep(1)
       print('小马哥 finished sleep')
   #
   g1 = gevent.spawn(eat)  创造一个协程任务
   g2 = gevent.spawn(sleep)  创造一个协程任务
   g1.join()   阻塞 直到g1任务完成为止
   g2.join()   阻塞 直到g1任务完成为止
   
   # 升级版
   
   
   gevent.joinall([g1,g2,g3])           # 全部阻塞
   g_l = []
   for i in range(10):
       g = gevent.spawn(eat)
       g_l.append(g)
   gevent.joinall(g_l)
   
     #############################################################################
     # 返回值的版本  
       
   import time
   import gevent
   from gevent import monkey
   monkey.patch_all()
   def eat():
       print('wusir is eating')
       time.sleep(1)
       print('wusir finished eat')
       return 'wusir***'
   #
   def sleep():
       print('小马哥 is sleeping')
       time.sleep(1)
       print('小马哥 finished sleep')
       return '小马哥666'
   #
   g1 = gevent.spawn(eat)
   g2 = gevent.spawn(sleep)
   gevent.joinall([g1,g2])
   print(g1.value)
   print(g2.value)
   
   ```

#### 9.9.3 async  模块协程

```
import asyncio

# 起一个任务
# async def demo():   # 协程方法
#     print('start')
#     await asyncio.sleep(1)  # 阻塞
#     print('end')
#
# loop = asyncio.get_event_loop()  # 创建一个事件循环
# loop.run_until_complete(demo())  # 把demo任务丢到事件循环中去执行

# 启动多个任务,并且没有返回值
# async def demo():   # 协程方法
#     print('start')
#     await asyncio.sleep(1)  # 阻塞
#     print('end')
#
# loop = asyncio.get_event_loop()  # 创建一个事件循环
# wait_obj = asyncio.wait([demo(),demo(),demo()])
# loop.run_until_complete(wait_obj)

# 启动多个任务并且有返回值
# async def demo():   # 协程方法
#     print('start')
#     await asyncio.sleep(1)  # 阻塞
#     print('end')
#     return 123
#
# loop = asyncio.get_event_loop()
# t1 = loop.create_task(demo())
# t2 = loop.create_task(demo())
# tasks = [t1,t2]
# wait_obj = asyncio.wait([t1,t2])
# loop.run_until_complete(wait_obj)
# for t in tasks:
#     print(t.result())

# 谁先回来先取谁的结果
# import asyncio
# async def demo(i):   # 协程方法
#     print('start')
#     await asyncio.sleep(10-i)  # 阻塞
#     print('end')
#     return i,123
#
# async def main():
#     task_l = []
#     for i in range(10):
#         task = asyncio.ensure_future(demo(i))
#         task_l.append(task)
#     for ret in asyncio.as_completed(task_l):
#         res = await ret
#         print(res)
#
# loop = asyncio.get_event_loop()
# loop.run_until_complete(main())



# import asyncio
#
# async def get_url():
#     reader,writer = await asyncio.open_connection('www.baidu.com',80)
#     writer.write(b'GET / HTTP/1.1\r\nHOST:www.baidu.com\r\nConnection:close\r\n\r\n')
#     all_lines = []
#     async for line in reader:
#         data = line.decode()
#         all_lines.append(data)
#     html = '\n'.join(all_lines)
#     return html
#
# async def main():
#     tasks = []
#     for url in range(20):
#         tasks.append(asyncio.ensure_future(get_url()))
#     for res in asyncio.as_completed(tasks):
#         result = await res
#         print(result)
#
#
# if __name__ == '__main__':
#     loop = asyncio.get_event_loop()
#     loop.run_until_complete(main())  # 处理一个任务


# python原生的底层的协程模块
    # 爬虫 webserver框架
    # 题高网络编程的效率和并发效果
# 语法
    # await 阻塞 协程函数这里要切换出去，还能保证一会儿再切回来
    # await 必须写在async函数里，async函数是协程函数
    # loop 事件循环
    # 所有的协程的执行 调度 都离不开这个loop

```

### 9.10 并发总结

```
# 操作系统
    # 1.计算机中所有的资源都是由操作系统分配的
    # 2.操作系统调度任务：时间分片、多道机制
    # 3.CPU的利用率是我们努力的指标
# 并发
    # 进程 开销大 数据隔离 资源分配单位 cpython下可以利用多核
        # 进程的三状态：就绪 运行 阻塞
        # multiprocessing模块
            # Process-开启进程
            # Lock - 互斥锁
                # 为什么要在进程中加锁
                    # 因为进程操作文件也会发生数据不安全
            # Queue -队列 IPC机制（Pipe,redis,memcache,rabbitmq,kafka）
                # 生产者消费者模型
            # Manager - 提供数据共享机制
    # 线程 开销小 数据共享 cpu调度单位  cpython下不能利用多核
        # GIL锁
            # 全局解释器锁
            # Cpython解释器提供的
            # 导致了一个进程中多个线程同一时刻只有一个线程能当问CPU -- 多线程不能利用多核
        # threading
            # Thread类 - 能开启线程start，等待线程结束join
            # Lock-互斥锁  不能在一个线程中连续acquire，效率相对高
            # Rlock-递归锁  可以在一个线程中连续acquire，效率相对低
            # 死锁现象如何发生?如何避免？
        # 线程队列 queue模块
            # Queue
            # LifoQueue
            # PriorityQueue
    # 池
        # concurrent.futrues.ThreadPoolExecutor,ProcessPoolExecutor
            # 实例化一个池 tp = ThreadPoolExecutor(num),pp = ProcessPoolExecutor(num)
            # 提交任务到池中，返回一个对象 obj = tp.submit(func,arg1,arg2...)
                # 使用这个对象获取返回值 obj.result()
                # 回调函数 obj.add_done_callback(函调函数)
            # 阻塞等待池中的任务都结束 tp.shutdown()
# 概念
    # IO操作
    # 同步异步
    # 阻塞非阻塞

# 1.所有讲过的概念都记住
# 2.数据安全问题
# 3.数据隔离和通信
# 4.会用基本的进程 线程 池

```



## 第十章数据库

### 10.1 引言

1. 数据库

   - 很多功能如果只是通过操作文件来改变数据是非常繁琐的
   - 程序员需要做很多事情
   - 对于多台机器或者多个进程操作用一份数据
   - 程序员自己解决并发和安全问题比较麻烦
   - 自己处理一些数据备份，容错的措施

2. 为了工作方便,引入了数据库

   - 数据库 是一个可以在一台机器上独立工作的，并且可以给我们提供高效、便捷的方式对数据进行增删改查的一种工具。

3. 数据库的优势

   ```python
     1.程序稳定性 ：这样任意一台服务所在的机器崩溃了都不会影响数据和另外的服务。
   
   　　2.数据一致性 ：所有的数据都存储在一起，所有的程序操作的数据都是统一的，就不会出现数据不一致的现象
   
   　　3.并发 ：数据库可以良好的支持并发，所有的程序操作数据库都是通过网络，而数据库本身支持并发的网络操作，不需要我们自己写socket
   
   　　4.效率 ：使用数据库对数据进行增删改查的效率要高出我们自己处理文件很多
   ```

4. 数据库是一个C/S架构的 操作数据文件的一个管理软件

   1. 帮助我们解决并发问题
   2. 能够帮助我们用更简单更快速的方式完成数据的增删改查
   3. 能够给我们提供一些容错、高可用的机制
   4. 权限的认证

5. 数据库管理系统DBMS

   - 文件夹 --数据库database   db
   - 数据库管理员 --  DBA

#### 10.1.1分类

1. 关系型数据库
   - sql server
   - oracle 收费、比较严谨、安全性比较 高
         国企 事业单位
         银行 金融行业
   - mysql  开源的
         小公司
         互联网公司
   - sqllite 
2. 非关系型数据库
   - redis
   - mongodb

#### 10.1.2  sql 语言

1. ddl 定义语言
   - 创建用户
     - create user '用户名'@'%'  表示网络可以通讯的所有ip地址都可以使用这个用户名
     - create user '用户名'@'192.168.12.%' 表示192.168.12.0网段的用户可以使用这个用户名
     - create user '用户名'@'192.168.12.87' 表示只有一个ip地址可以使用这个用户名
   - 创建库
     - create  database day38;
   - 创建表
     - create table 表名(字段名 数据类型(长度)，字段名 数据类型(长度)，)
2. dml 操作语言
   1. 数据的
      - 增 insert into
      - 删 delete from
      - 改 update
      - 查 select
   2. select user(); 查看当前用户
      - select database(); 查看当前所在的数据库
      - show
      - show databases:  查看当前的数据库有哪些
      - show tables；查看当前的库中有哪些表
      - desc 表名；查看表结构
      - use 库名；切换到这个库下
3. dcl 控制语言
   1. 给用户授权
      - grant select on 库名.* to '用户名'@'ip地址/段' identified by '密码'
      - grant select,insert
      - grant all

### 10. 2MySql的安装和配置

#### 10.2.1 mysql的CS架构

1. mysqld install  安装数据库服务
2. net start mysql 启动数据库的server端
   - 停止server net stop mysql
3. 客户端可以是python代码也可以是一个程序
       mysql.exe是一个客户端
       mysql -u用户名 -p密码
4. mysql中的用户和权限
       在安装数据库之后，有一个最高权限的用户root
5. mysql -h192.168.12.87 -uroot -p123
6. 我们的mysql客户端不仅可以连接本地的数据库
   - 也可以连接网络上的某一个数据库的server端

#### 10.2.2 mysql 的 一些命令行

1. `mysql>select user();`
       查看当前用户是谁

2. `mysql>set password = password('密码')`
       设置密码

3. 创建用户
   `create user 's21'@'192.168.12.%' identified by '123';`
   `mysql -us21 -p123 -h192.168.12.87`

4. 授权
       `grant all on day37.* to 's21'@'192.168.12.%';`
       授权并创建用户
      ` grant all on day37.* to 'alex'@'%' identified by '123';`

5. 查看文件夹
       `show databases;`
   创建文件夹
       `create databases day37;`

6. DROP TABLE ：删除表

7. 库 表 数据
       创建库、创建表  DDL数据库定义语言
       存数据，删除数据,修改数据,查看  DML数据库操纵语句
       grant/revoke  DCL控制权限
   库操作
       `create database 数据库名; ` 创建库
       `show databases;` 查看当前有多少个数据库
      ` select database();`查看当前使用的数据库
       `use 数据库的名字; `切换到这个数据库(文件夹)下
   表操作
       查看当前文件夹中有多少张表
           `show tables;`
       创建表
          ` create table student(id int,name char(4));`
       删除表
           `drop table student;`
       查看表结构
           `desc 表名;`

   操作表中的数据
       数据的增加
          ` insert into student values (1,'alex');`
         `  insert into student values (2,'wusir');`
       数据的查看
          ` select * from student;`
       修改数据
          ` update 表 set 字段名=值`
         `  update student set name = 'yuan';`
          ` update student set name = 'wusir' where id=2;`
       删除数据
           `delete from 表名字;`
           `delete from student where id=1;`

#### 10.2.3 mysql的存储引擎

1. myisam ：适合做读 插入数据比较频繁的，对修改和删除涉及少的，不支持事务、行级锁和外键。有表级锁,索引和数据分开存储的，mysql5.5以下默认的存储引擎
2. innodb ：适合并发比较高的，对事务一致性要求高的， 相对更适应频繁的修改和删除操作，有行级锁，外键且支持事务, 索引和数据是存在一起的，mysql5.6以上默认的存储引擎
3. memory ：数据存在内存中，表结构存在硬盘上，查询速度快，重启数据丢失 . 

### 10.3 mysql 表的操作

#### 10.3.1 存储引擎

1. 表的存储方式

   1. 存储方式1：MyISAM5.5以下默认存储方式

      - 存储的文件个数：表结构、表中的数据、索引
      - 支持表级锁
      - 不支持行级锁不支持事务不支持外键

   2. 存储方式2：InnoDB5.6以上默认存储方式

      - 存储的文件个数：表结构、表中的数据

      - 支持行级锁、支持表锁

      - 支持事务

      - 支持外键

        事务

        ![事务](D:\md_down_png\事务.png)

   3. 存储方式3：MEMORY内存

      - 存储的文件个数：表结构
      - 优势：增删改查都很快
      - 劣势：重启数据消失、容量有限

#### 10.3.2 表相关命令

1. 查看配置项:
   `show variables like '%engine%';`
2. 创建表
   ` create table t1 (id int,name char(4));`
   ` create table t2 (id int,name char(4)) engine=myisam;`
   ` create table t3 (id int,name char(4)) engine=memory;`
3. 查看表的结构
       `show create table 表名; `能够看到和这张表相关的所有信息
      ` desc 表名  `             只能查看表的字段的基础信息
           describe 表名

#### 10.3.3为什么用mysql数据库

1. 用什么数据库 ： mysql

   版本是什么 ：5.6
   都用这个版本么 ：不是都用这个版本
   存储引擎 ：innodb
   为什么要用这个存储引擎：
       支持事务 支持外键 支持行级锁
                            能够更好的处理并发的修改问题

#### 10.3.4 表的 数据类型

1. 数值型

   - 整数 int
     `create table t4 (id1 int(4),id2 int(11));`
   - 注意 : -->int默认是有符号的
     - 它能表示的数字的范围不被宽度约束
     - 它只能约束数字的显示宽度
     - `create table t5 (id1 int unsigned,id2 int);`
   - 小数  float
     `create table t6 (f1 float(5,2),d1 double(5,2));`
     `create table t7 (f1 float,d1 double);`
     `create table t8 (d1 decimal,d2 decimal(25,20));`

2. 日期时间

   year 年 ,范围>>>>1901/2155

   date 年月日,范围>>>1000-01-01/9999-12-31

   time 时分秒,

   `datetime、timestamp`  年月日时分秒,前者>>>1000-01-01 00:00:00/9999-12-31 23:59:59,后者>>>1970-01-01 00:00:00/2038

   - `create table t9(
     y year,d date,
     dt datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
     ts timestamp);`

3. 字符串

   char(15)  定长的单位
       alex  alex
       alex
   varchar(15) 变长的单位
       alex  alex4

   哪一个存储方式好？
       varchar ：节省空间、存取效率相对低
       char ：浪费空间，存取效率相对高 长度变化小的

4. enum 和 set型

   enum枚举型,ENUM只允许从值集合中选取单个值，而不能一次取多个值。

   SET和ENUM非常相似，也是一个字符串对象，里面可以包含0-64个成员。根据成员的不同，存储上也有所不同。set类型可以**允许值集合中任意选择1或多个元素进行组合**。对超出范围的内容将不允许注入，而对重复的值将进行自动去重。

   `create table t12(
   name char(12),
   gender ENUM('male','female'),
   hobby set('抽烟','喝酒','烫头','洗脚')
   );`

   `insert into t12 values('alex','不详','抽烟,喝酒,洗脚,洗脚,按摩');`

   `insert into teacher values(1, '波多');`

#### 10.3.5 总结

```python
数据类型
    数字类型 ：int，float(5,2)
    字符串类型 ：char(10)，varchar(10)
    时间类型：datetime，date，time
    enum和set：enum(单选项1,单选项2)，set(多选项1，多选项2)

create table 表名(
字段名 数据类型(宽度/选项)，
字段名 数据类型(宽度/选项)，
)

创建库
create database day39
查看有哪些库
show databases;
查看当前所在的数据库
select database();
切换库
use 库名
```

### 10.4表的约束 和 修改

为了防止不符合规范的数据进入数据库，在用户对数据进行插入、修改、删除等操作时，DBMS自动按照一定的约束条件对数据进行监测，使不符合规范的数据不能进入数据库，以确保数据库中存储的数据正确、有效、相容。 

1. unsigned  设置某一个数字无符号(指没有负号,也就是负数)

2. not null 某一个字段不能为空

3. default 给某个字段设置默认值

   ```python
   我们约束某一列不为空，如果这一列中经常有重复的内容，就需要我们频繁的插入，这样会给我们的操作带来新的负担，于是就出现了默认值的概念。
   默认值，创建列时可以指定默认值，当插入数据时如果未主动设置，则自动添加默认值
   ```

   严格模式设置

   ```python
   设置严格模式：
       不支持对not null字段插入null值
       不支持对自增长字段插入”值
       不支持text字段有默认值
   
   直接在mysql中生效(重启失效):
   mysql>set sql_mode="STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION";
   
   配置文件添加(永久失效)：
   sql-mode="STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION"
   ```

4. unique  设置某一个字段不能重复,指定某列或者几列组合不能重复
   联合唯一

   ```python
   create table service(
   id int primary key auto_increment,
   name varchar(20),
   host varchar(15) not null,
   port int not null,
   unique(host,port) #联合唯一
   );
   
   mysql> insert into service values
       -> (1,'nginx','192.168.0.10',80),
       -> (2,'haproxy','192.168.0.20',80),
       -> (3,'mysql','192.168.0.30',3306)
       -> ;
   Query OK, 3 rows affected (0.01 sec)
   Records: 3  Duplicates: 0  Warnings: 0
   
   mysql> insert into service(name,host,port) values('nginx','192.168.0.10',80);
   ERROR 1062 (23000): Duplicate entry '192.168.0.10-80' for key 'host'.
   # 即(host,port),相当于一个元祖,合在一起不能重复
   ```

5. auto_increment 设置某一个int类型的字段 自动增加

   **注意**:>>1. auto_increment自带not null效果 ////  2.  设置条件 int  unique ///3. 自增字段 必须是数字 且 必须是唯一的

6. primary key    设置某一个字段非空且不能重复,(主键)

   **注意**:>>>1. 约束力相当于 not null + unique ///2. 一张表只能由一个主键///3. 会将第一个设置

   ```python
   # 多字段主键
   create table service(
   ip varchar(15),
   port char(5),
   service_name varchar(10) not null,
   primary key(ip,port)
   );
   
   
   mysql> desc service;
   +--------------+-------------+------+-----+---------+-------+
   | Field        | Type        | Null | Key | Default | Extra |
   +--------------+-------------+------+-----+---------+-------+
   | ip           | varchar(15) | NO   | PRI | NULL    |       |
   | port         | char(5)     | NO   | PRI | NULL    |       |
   | service_name | varchar(10) | NO   |     | NULL    |       |
   +--------------+-------------+------+-----+---------+-------+
   3 rows in set (0.00 sec)
   
   mysql> insert into service values
       -> ('172.16.45.10','3306','mysqld'),
       -> ('172.16.45.11','3306','mariadb')
       -> ;
   Query OK, 2 rows affected (0.00 sec)
   Records: 2  Duplicates: 0  Warnings: 0
   
   mysql> insert into service values ('172.16.45.10','3306','nginx');
   ERROR 1062 (23000): Duplicate entry '172.16.45.10-3306' for key 'PRIMARY'
   ```

   1. ```python
      # 一张表只能设置一个主键
      # 一张表最好设置一个主键
      # 约束这个字段 非空（not null） 且 唯一（unique）
      
      
      ############################
      # create table t6(
      #     id int not null unique,     # 你指定的第一个非空且唯一的字段会被定义成主键
      #     name char(12) not null unique
      # )
      ```

   2. 联合主键  : # primary key(字段1，字段2)

7. key    外键

   1. references

   2. foreign key(本表字段-外键名) references 外表名(外表字段)；

   3. 外键关联的那张表中的字段必须 unique

   4. 级联删除和更新

      ```python
      # 外键 foreign key 涉及到两张表
      # 员工表
      create table staff(
      id  int primary key auto_increment,
      age int,
      gender  enum('male','female'),
      salary  float(8,2),
      hire_date date,
      post_id int,
      foreign key(post_id) references post(pid)
      );
      
      
      #  部门表
       # pid postname post_comment post_phone
      create table post(
          pid  int  primary key,
          postname  char(10) not null unique,
          comment   varchar(255),
          phone_num  char(11)
      );
      
      update post set pid=2 where pid = 1;
      delete from post where pid = 1;
      
      # 级联删除和级联更新
      create table staff2(
      id  int primary key auto_increment,
      age int,
      gender  enum('male','female'),
      salary  float(8,2),
      hire_date date,
      post_id int,
      foreign key(post_id) references post(pid) 
      on delete cascade     # 级连删除
      on update cascade     # 级连更新
      );
      
      
      # 删父表department，子表employee中对应的记录跟着删
      # 更新父表department，子表employee中对应的记录跟着改
      
      ##########################################
         . cascade方式
      在父表上update/delete记录时，同步update/delete掉子表的匹配记录 
      
         . set null方式
      在父表上update/delete记录时，将子表上匹配记录的列设为null
      要注意子表的外键列不能为not null  
      
         . No action方式
      如果子表中有匹配的记录,则不允许对父表对应候选键进行update/delete操作  
      
         . Restrict方式
      同no action, 都是立即检查外键约束
      
         . Set default方式
      父表有变更时,子表将外键列设置成一个默认的值 但Innodb不能识别
      ```

#### 10.4.2 表的修改(alter)

```python
# alter table 表名 add 添加字段
# alter table 表名 drop 删除字段
# alter table 表名 modify 修改已经存在的字段  的类型 宽度 约束
# alter table 表名 change 修改已经存在的字段  的类型 宽度 约束 和 字段名字

# alter table 表名 add 字段名 数据类型(宽度) 约束 first/after name
# alter table 表名 drop 字段名
# alter table 表名 modify name varchar(12) not null
# alter table 表名 change name new_name varchar(12) not null

# id name age
# alter table 表名 modify age int not null after id;
# alter table 表名 modify age int not null first;

```

#### 10.4.3 表的关系

```python
多对一   foreign key  永远是在多的那张表中设置外键
    # 多个学生都是同一个班级的
    # 学生表 关联 班级表
    # 学生是多    班级是一
# 一对一   foreign key +unique  # 后出现的后一张表中的数据 作为外键，并且要约束这个外键是唯一的
    # 客户关系表 ： 手机号码  招生老师  上次联系的时间  备注信息
    # 学生表 ：姓名 入学日期 缴费日期 结业
# 多对多  产生第三张表，把两个关联关系的字段作为第三张表的外键
    # 书
    # 作者

    # 出版社
    # 书

```

#### 10.4.4 表关系的写法

1. 多对一

   ```python
   foreign key(多) references 表(一)
       # 多个学生对应一个班级
       # 多个员工对应一个部门
       # 多本书对应一个作者
       # 多个商品对应一个店铺、订单
   
   ```

2. 一对一(外键必须唯一)

   ```
       # 一对一
           # 后一 类型 unique
           # foreign key(后一) references 表(先一)
                 # 一个客户对应一个学生 学生表有外键
                 # 一个商品 有一个商品详情 详情页中有外键
   
   ```

3. 多对多

   - 第三张表(前2张表没有任何关联)

   - ```python
     # foreign key(外键名1) references 表1(主键)
             # foreign key(外键名2) references 表2(主键)
                 # 多个学生对应一个班级，多个班级对应一个学生
                 # 多个员工对应一个部门，多个部门对应一个员工
                 # 多本书对应一个作者，多个作者对应一本书
                 # 多个商品对应一个店铺、订单，多个店铺对应一个商品
                 # 一个学生学习多门课程，一门课程被多个学上学习
     
     ```

   ```python
   # 作者与书多对多
   # create table author(
   # aid primary key auto_increment,
   # name char(12) not null,
   # birthday date,
   # gender enum('male','female') default 'male'
   # )
   
   # create table book(
   # id  int  primary key,
   # name char(12) not null,
   # price float(5,2)
   # )
   
   # create table book_author(
   # id int primary key auto_increment,
   # book_id int not null,
   # author_id int not null,
   # foreign key(book_id) references book(id),
   # foreign key(author_id) references author(aid),
   # );
   
   ```

   

4. ![多对多](D:\md_down_png\多对多.png)

### 10.5 单表查询

#### 10.5.1 单表查询语法

`SELECT DISTINCT 字段1,字段2... FROM 表名
                              WHERE 条件
                              GROUP BY field
                              HAVING 筛选
                              ORDER BY field
                              LIMIT 限制条数`

执行的优先级为`from,where,group by,select,distinct,having,order by,limit`

**1.找到表:from**

**2.拿着where指定的约束条件，去文件/表中取出一条条记录**

**3.将取出的一条条记录进行分组group by，如果没有group by，则整体作为一组**

**4.执行select（去重）**

**5.将分组的结果进行having过滤**

**6.将结果按条件排序：order by**

**7.限制结果的显示条数**![QQ图片20190524090611](D:\md_down_png\QQ图片20190524090611.png)

#### 10.5.2 简单查询

```python
select * (字段名) from `table` ;
SELECT DISTINCT post FROM employee;   (dintinct去重),可以对数值型数据进行+-*/运算

```

1. concat(定义显示格式)
   `select concat('名字',name,"年龄",age) from table`

   `select concat_ws('<>',name,age) from table`     连接符,

##### 10.5.2.1 where 语句

1. 比较运算符：> < >= <= <> !=

2. and not or

3. is  is not

4. between 80 and 100 值在80到100之间(数字范围)

5. in(80,90,100) 值是80或90或100(多选一)

6. like 'e%'   (字符串范围)
   通配符可以是%或_，
   %表示任意多字符
   _表示一个字符 

   ```python
       通配符’%’
       SELECT * FROM employee 
               WHERE emp_name LIKE 'eg%';
   
       通配符’_’
       SELECT * FROM employee 
               WHERE emp_name LIKE 'al__';
   
   ```

7. 逻辑运算符：在多个条件直接可以使用逻辑运算符 and or not

8. 判断某个字段是否为null,必须用`is    is not`

9. 使用正则查询

   ```python
   SELECT * FROM employee WHERE emp_name REGEXP '^ale';
   
   SELECT * FROM employee WHERE emp_name REGEXP 'on$';
   
   SELECT * FROM employee WHERE emp_name REGEXP 'm{2}';  # regexp
   
   select * from employee where emp_name regexp '^jin.*[gn]$';
   
   
   小结：对字符串匹配的方式
   WHERE emp_name = 'egon';
   WHERE emp_name LIKE 'yua%';
   WHERE emp_name REGEXP 'on$';
   
   ```

##### 10.5.2.2 group  by分组

```python
# select * from employee group by post
# 会把在group by后面的这个字段，也就是post字段中的每一个不同的项都保留下来
# 并且把值是这一项的的所有行归为一组

单独使用GROUP BY关键字分组
    SELECT post FROM employee GROUP BY post;
    注意：我们按照post字段分组，那么select查询的字段只能是post，想要获取组内的其他相关信息，需要借助函数

GROUP BY关键字和GROUP_CONCAT()函数一起使用
    SELECT post,GROUP_CONCAT(emp_name) FROM employee GROUP BY post;#按照岗位分组，并查看组内成员名
    SELECT post,GROUP_CONCAT(emp_name) as emp_members FROM employee GROUP BY post;

GROUP BY与聚合函数一起使用
    select post,count(id) as count from employee group by post;#按照岗位分组，并查看每个组有多少人

```

##### 10.5.2.3 聚合 语函数(count ,sum ,max , min , avg)

```python
#强调：聚合函数聚合的是组的内容，若是没有分组，则默认一组

示例：
    SELECT COUNT(*) FROM employee;
    SELECT COUNT(*) FROM employee WHERE depart_id=1;
    SELECT MAX(salary) FROM employee;
    SELECT MIN(salary) FROM employee;
    SELECT AVG(salary) FROM employee;
    SELECT SUM(salary) FROM employee;
    SELECT SUM(salary) FROM employee WHERE depart_id=3;

```

##### 10.5.2.4 having (过滤,筛选)

```
#！！！执行优先级从高到低：where > group by > having 
#1. Where 发生在分组group by之前，因而Where中可以有任意字段，但是绝对不能使用聚合函数。
#2. Having发生在分组group by之后，因而Having中可以使用分组的字段，无法直接取到其他字段,可以使用聚合函数

```

```python
# 总是根据会重复的项来进行分组
# 分组总是和聚合函数一起用 最大 最小 平均 求和 有多少项

# 1.执行顺序 总是先执行where 再执行group by分组
#   所以相关先分组 之后再根据分组做某些条件筛选的时候 where都用不上
# 2.只能用having来完成

# 平均薪资大于10000的部门
# select post from employee group by post having avg(salary) > 10000

```

##### 10.5.2.5 order by 查询排序 

默认从小到大(日期从小到大则是年份最小的,则里现在最远) >>>>asc

```
# order by
    # order by 某一个字段 asc;  默认是升序asc 从小到大
    # order by 某一个字段 desc;  指定降序排列desc 从大到小
    # order by 第一个字段 asc,第二个字段 desc;
        # 指定先根据第一个字段升序排列，在第一个字段相同的情况下，再根据第二个字段排列

```

```
按单列排序
    SELECT * FROM employee ORDER BY salary;
    SELECT * FROM employee ORDER BY salary ASC;
    SELECT * FROM employee ORDER BY salary DESC;

按多列排序:先按照age排序，如果年纪相同，则按照薪资排序
    SELECT * from employee
        ORDER BY age,
        salary DESC;

```

##### 10.5.2.6  limit 限制

```
# limit
    # 取前n个  limit n   ==  limit 0,n
        # 考试成绩的前三名
        # 入职时间最晚的前三个
    # 分页    limit m,n   从m+1开始取n个
    # 员工展示的网页
        # 18个员工
        # 每一页展示5个员工
    # limit n offset m == limit m,n  从m+1开始取n个

```

### 10.6 多表查询

#### 10.6.1  两张表连在一起查询(推荐)

```python
select * from emp,department;

```

#### 10.6.2 连表查询(推荐使用,效率更高)

```
# 连表查询
    # 把两张表连在一起查
    # 内链接 inner join   两张表条件不匹配的项不会出现再结果中
    # select * from emp inner join department on emp.dep_id = department.id;
    # 外连接
        # 左外连接 left join  永远显示全量的左表中的数据
        # select * from emp left join department on emp.dep_id = department.id;
        # 右外连接 right join 永远显示全量的右表中的数据
        # select * from emp right join department on emp.dep_id = department.id;
        # 全外连接
        # select * from emp left join department on emp.dep_id = department.id
        # union
        # select * from department right join emp  on emp.dep_id = department.id;

```

1. 连接的语法

   ```
   # 连接的语法
   # select 字段 from 表1 xxx join 表2 on 表1.字段 = 表2.字段;
       # 常用
       # 内链接
       # 左外链接
       
       
   # 找技术部门的所有人的姓名
       # select * from emp inner join department on emp.dep_id = department.id;
   # +----+-----------+--------+------+--------+------+--------------+
   # | id | name      | sex    | age  | dep_id | id   | name         |
   # +----+-----------+--------+------+--------+------+--------------+
   # |  1 | egon      | male   |   18 |    200 |  200 | 技术         |
   # |  2 | alex      | female |   48 |    201 |  201 | 人力资源     |
   # |  3 | wupeiqi   | male   |   38 |    201 |  201 | 人力资源     |
   # |  4 | yuanhao   | female |   28 |    202 |  202 | 销售         |
   # |  5 | liwenzhou | male   |   18 |    200 |  200 | 技术         |
   # +----+-----------+--------+------+--------+------+--------------+
   # select * from emp inner join department on emp.dep_id = department.id where department.name = '技术'
   # select emp.name from emp inner join department d on emp.dep_id = d.id where d.name = '技术'
   
   # 找出年龄大于25岁的员工以及员工所在的部门名称
   # select emp.name,d.name from emp inner join department as d on emp.dep_id = d.id where age>25;
   
   # 根据age的升序顺序来连表查询emp和department
   # select * from emp inner join department as d on emp.dep_id = d.id order by age;
   
   ```

#### 10.6.3 子查询

```python
# 子查询
        # 找技术部门的所有人的姓名
        # 先找到部门表技术部门的部门id
        # select id from department where name = '技术';
        # 再找emp表中部门id = 200
        # select name from emp where dep_id = (select id from department where name = '技术');

        # 找到技术部门和销售部门所有人的姓名
        # 先找到技术部门和销售部门的的部门id
        # select id from department where name = '技术' or name='销售'
        # 找到emp表中部门id = 200或者202的人名
        # select name from emp where dep_id in (select id from department where name = '技术' or name='销售');
        # select emp.name from emp inner join department on emp.dep_id = department.id where department.name in ('技术','销售');


```

### 10.7 表总结

增加 insert
删除 delete
修改 update
查询 select

1. 增加 insert
   insert into 表名 values (值....)
       所有的在这个表中的字段都需要按照顺序被填写在这里
   insert into 表名(字段名，字段名。。。) values (值....)
       所有在字段位置填写了名字的字段和后面的值必须是一一对应
   insert into 表名(字段名，字段名。。。) values (值....),(值....),(值....)
       所有在字段位置填写了名字的字段和后面的值必须是一一对应

   ```python
   value单数            values复数
   一次性写入一行数据   一次性写入多行数据
   
   t1 id,name,age
   insert into t1 value (1,'alex',83)
   insert into t1 values (1,'alex',83),(2,'wusir',74)
   
   insert into t1(name,age) value ('alex',83)
   insert into t1(name,age) values ('alex',83),('wusir',74)
   
   #################################
   第一个角度
       写入一行内容还是写入多行
       insert into 表名 values (值....)
       insert into 表名 values (值....)，(值....)，(值....)
   
   第二个角度
       是把这一行所有的内容都写入
       insert into 表名 values (值....)
       指定字段写入
       insert into 表名(字段1，字段2) values (值1，值2)
   
   ```

2. 删除 delete
       delete from 表 where 条件;

3. 更新 update
       update 表 set 字段=新的值 where 条件；

4. 查询
       select语句
           select * from 表
           select 字段,字段.. from 表
           select distinct 字段,字段.. from 表  按照查出来的字段去重
           select 字段*5 from 表  按照查出来的字段去重
           select 字段  as 新名字,字段 as 新名字 from 表  按照查出来的字段去重
           select 字段 新名字 from 表  按照查出来的字段去重

   ​		select concat(''名字",name,'薪水',sralry) as auully from employee;

   select concat_ws('分割符",name,salary,post) from employee;

   连表查询>>>子查询

### 10.8  pymysql 模块的使用

#### 10.8.1 安装

1. 先安装  `pip install pymysql`

2. 导入并使用

   ```python
   import pymysql
   
   conn = pymysql.connect(host='127.0.0.1', user='root', password="123",
                    database='day40')
   cur = conn.cursor()   # 获取数据库操作符 游标
   cur.execute('insert into employee(emp_name,sex,age,hire_date) '
               'values ("郭凯丰","male",40,20190808)')
   cur.execute('delete from employee where id = 18')
   conn.commit()
   conn.close()
   
   
   # ##############查询
   conn = pymysql.connect(host='127.0.0.1', user='root', password="123",
                    database='day40')
   cur = conn.cursor(pymysql.cursors.DictCursor)   # 数据库操作符 游标>>>得到的结果转为字典
   cur.execute('select * from employee '
               'where id > 10')
   ret = cur.fetchone()      # 一行数据
   print(ret['emp_name'])
   ret = cur.fetchmany(5)
   ret = cur.fetchall()
   print(ret)
   conn.close()
   
   
   ```

### 10.9 索引原理

#### 10.9.1什么是索引 -- 目录

1. 就是建立起的一个在存储表阶段
      就有的一个存储结构能在查询的时候加速,>>>**索引在MySQL中也叫是一种“键”，是存储引擎用于快速找到记录的一种数据结构。**
2. 索引的重要
   - 一般的应用系统读写比例 ： 10：1 , 在生产环境中，我们遇到最多的，也是最容易出问题的，还是一些复杂的查询操作，因此对查询语句的优化显然是重中之重。说起加速查询，就不得不提到索引了
   - 读（查询）的速度就至关重要了

#### 10.9.2索引的原理

- 索引的目的在于提高查询效率，与我们查阅图书所用的目录是一个道理：先定位到章，然后定位到该章下的一个小节，然后找到页数。相似的例子还有：查字典，查火车车次，飞机航班等

  **本质都是：通过不断地缩小想要获取数据的范围来筛选出最终想要的结果，同时把随机的事件变成顺序的事件，也就是说，有了这种索引机制，我们可以总是用同一种查找方式来锁定数据。**

- block 磁盘预读原理
      for line in f
      4096个字节![QQ图片20190525110951](D:\md_down_png\QQ图片20190525110951.png)
  读硬盘的io操作的时间非常的长，比CPU执行指令的时间长很多
  尽量的减少IO次数才是读写数据的主要要解决的问题

  

- 考虑到磁盘IO是非常高昂的操作，计算机操作系统做了一些优化，**当一次IO时，不光把当前磁盘地址的数据，而是把相邻的数据也都读取到内存缓冲区内**，因为局部预读性原理告诉我们，当计算机访问一个地址的数据的时候，与其相邻的数据也会很快被访问到。每一次IO读取的数据我们称之为一页(page)。具体一页有多大数据跟操作系统有关，一般为4k或8k，也就是我们读取一页内的数据时候，实际上才发生了一次IO，这个理论对于索引的数据结构设计非常有帮助。

#### 10.9.3 数据库的存储方式

1. 新的数据结构 —— 树 ,它是由n （n>=1）个有限结点组成一个具有层次关系的[集合](https://baike.baidu.com/item/集合)。把它叫做“树”是因为它看起来像一棵倒挂的树，也就是说它是根朝上，而叶朝下的。

   ![1558753578778](D:\md_down_png\1558753578778.png)

2. 平衡树 balance tree - b+树![QQ图片20190525111037](D:\md_down_png\QQ图片20190525111037.jpg)![QQ图片20190525111044](D:\md_down_png\QQ图片20190525111044.jpg)

   **innodb 数据和缩影存在一起**

3. 在b树的基础上进行了改良 - b+树
       1.分支节点和根节点都不再存储实际的数据了
           让分支和根节点能存储更多的索引的信息
           就降低了树的高度
           所有的实际数据都存储在叶子节点中
       2.在叶子节点之间加入了双向的链式结构
           方便在查询中的范围条件![QQ图片20190525111053](D:\md_down_png\QQ图片20190525111053.png)
   mysql当中所有的b+树索引的高度都基本控制在3层
       1.io操作的次数非常稳定
       2.有利于通过范围查询
   什么会影响索引的效率 —— 树的高度
       1.对哪一列创建索引，选择尽量短的列做索引
       2.对区分度高的列建索引，重复率超过了10%那么不适合创建索引

#### 10.9.4聚集索引和辅助索引

- 在innodb中 聚集索引和辅助索引并存的
      聚集索引 - 主键 更快
          数据直接存储在树结构的叶子节点

- ```
  而聚集索引（clustered index）就是按照每张表的主键构造一棵B+树，同时叶子结点存放的即为整张表的行记录数据，也将聚集索引的叶子结点称为数据页。
  聚集索引的这个特性决定了索引组织表中数据也是索引的一部分。同B+树数据结构一样，每个数据页都通过一个双向链表来进行链接。
      
  #如果未定义主键，MySQL取第一个唯一索引（unique）而且只含非空列（NOT NULL）作为主键，InnoDB使用它作为聚簇索引。
      
  #如果没有这样的列，InnoDB就自己产生一个这样的ID值，它有六个字节，而且是隐藏的，使其作为聚簇索引。
  
  ```

- 辅助索引 - 除了主键之外所有的索引都是辅助索引 稍慢
  数据不直接存储在树中

- 表中除了聚集索引外其他索引都是辅助索引（Secondary Index，也称为非聚集索引），与聚集索引的区别是：辅助索引的叶子节点不包含行记录的全部数据。

  叶子节点除了包含键值以外，每个叶子节点中的索引行中还包含一个书签（bookmark）。该书签用来告诉InnoDB存储引擎去哪里可以找到与索引相对应的行数据。![QQ图片20190525111101](D:\md_down_png\QQ图片20190525111101-1558754468654.png)

- ![QQ图片20190525111106](D:\md_down_png\QQ图片20190525111106.png)

- 在myisam中 只有辅助索引，没有聚集索引

#### 10.9.5 mysql索引管理

##### 10.9.5.1 索引分类

1. primary key 主键 聚集索引  约束的作用：非空 + 唯一  ( 联合主键PRIMARY KEY(id,name):联合主键索引)
2. unique 自带索引 辅助索引   约束的作用：唯一(联合唯一UNIQUE(id,name):联合唯一索引)
3. index  辅助索引            没有约束作用(联合索引INDEX(id,name):联合普通索引)

##### 10.9.5.2 创建索引

索引的类型(了解)

```python
#我们可以在创建上述索引的时候，为其指定索引类型，分两类
hash类型的索引：查询单条快，范围查询慢
btree类型的索引：b+树，层数越多，数据量指数级增长（我们就用它，因为innodb默认支持它）

#不同的存储引擎支持的索引类型也不一样
InnoDB 支持事务，支持行级别锁定，支持 B-tree、Full-text 等索引，不支持 Hash 索引；
MyISAM 不支持事务，支持表级别锁定，支持 B-tree、Full-text 等索引，不支持 Hash 索引；
Memory 不支持事务，支持表级别锁定，支持 B-tree、Hash 等索引，不支持 Full-text 索引；
NDB 支持事务，支持行级别锁定，支持 Hash 索引，不支持 B-tree、Full-text 等索引；
Archive 不支持事务，支持表级别锁定，不支持 B-tree、Hash、Full-text 等索引；

```

1. 在已存在的表创建索引

   create index 索引名字 on 表(字段)

   ALTER TABLE 表名 ADD  [UNIQUE | FULLTEXT | SPATIAL ] INDEX
                                索引名 (字段名[(长度)]  [ASC |DESC]) ;

   ```python
   create index ix_age on t1(age);
   
   alter table t1 add index ix_sex(sex);
   alter table t1 add index(sex);
   
   ```

2. 创建表的创建索引

   ```python
   #方法一：创建表时
       　　CREATE TABLE 表名 (
                   字段名1  数据类型 [完整性约束条件…],
                   字段名2  数据类型 [完整性约束条件…],
                   [UNIQUE | FULLTEXT | SPATIAL ]   INDEX | KEY
                   [索引名]  (字段名[(长度)]  [ASC |DESC]) 
                   );
   ##################################################################3
   
   create table t1(
       id int,
       name char,
       age int,
       sex enum('male','female'),
       unique key uni_id(id),
       index ix_name(name) #  index 有 key
   );
   create table t1(
       id int,
       name char,
       age int,
       sex enum('male','female'),
       unique key uni_id(id),
       index(name) #index没有key
   );
   
   ```

   

   

3. DROP INDEX 索引名 ON 表名字;

4. 索引是如何发挥作用的
       select * from 表 where id = xxxxx
           在id字段没有索引的时候，效率低
           在id字段有索引的之后，效率高

##### 10.9.5.3 总结

```python
#1. 一定是为搜索条件的字段创建索引，比如select * from s1 where id = 333;就需要为id加上索引

#2. 在表中已经有大量数据的情况下，建索引会很慢，且占用硬盘空间，建完后查询速度加快
比如create index idx on s1(id);会扫描表中所有的数据，然后以id为数据项，创建索引结构，存放于硬盘的表中。
建完以后，再查询就会很快了。

#3. 需要注意的是：innodb表的索引会存放于s1.ibd文件中，而myisam表的索引则会有单独的索引文件table1.MYI

MySAM索引文件和数据文件是分离的，索引文件仅保存数据记录的地址。而在innodb中，表数据文件本身就是按照B+Tree（BTree即Balance True）组织的一个索引结构，这棵树的叶节点data域保存了完整的数据记录。这个索引的key是数据表的主键，因此innodb表数据文件本身就是主索引。
因为inndob的数据文件要按照主键聚集，所以innodb要求表必须要有主键（Myisam可以没有），如果没有显式定义，则mysql系统会自动选择一个可以唯一标识数据记录的列作为主键，如果不存在这种列，则mysql会自动为innodb表生成一个隐含字段作为主键，这字段的长度为6个字节，类型为长整型.

```

#### 10.9.6  正确使用索引(不命中索引的情况)重点

##### 10.9.6.1 索引不生效的原因

1. 要查询的数据的范围大

   - > < >= <= !=

   - between and (范围小就快.范围大就慢)
         select * from 表 order by age limit 0，5
         select * from 表 where id between 1000000 and 1000005;

   - like
         结果的范围大 索引不生效
         如果 abc% 索引生效，%abc索引就不生效

   - 如果一列内容的区分度不高，索引也不生效(区分度要高)

     ```python
     尽量选择区分度高的列作为索引,区分度的公式是count(distinct col)/count(*)，表示字段不重复的比例，比例越大我们扫描的记录数越少，唯一键的区分度是1，而一些状态、性别字段可能在大数据面前区分度就是0，那可能有人会问，这个比例有什么经验值吗？使用场景不同，这个值也很难确定，一般需要join的字段我们都要求是0.1以上，即平均1条扫描10条记录
     
     ```

     name列

   - 索引列不能在条件中参与计算
         select * from s1 where id*10 = 1000000;  索引不生效

   - 对两列内容进行条件查询

     1. and : and条件两端的内容，优先选择一个有索引的，并且树形结构更好的，来进行查询
            两个条件都成立才能完成where条件，先完成范围小的缩小后面条件的压力
            select * from s1 where id =1000000 and email = 'eva1000000@oldboy';

        ```python
        #2、and的工作原理
            条件：
                a = 10 and b = 'xxx' and c > 3 and d =4
            索引：
                制作联合索引(d,a,b,c)
            工作原理:
                对于连续多个and：mysql会按照联合索引，从左到右的顺序找一个区分度高的索引字段(这样便可以快速锁定很小的范围)，加速查询，即按照d—>a->b->c的顺序
        
        ```

     2. or  :  or条件的，不会进行优化，只是根据条件从左到右依次筛选
            条件中带有or的要想命中索引，这些条件中所有的列都是索引列
            select * from s1 where id =1000000 or email = 'eva1000000@oldboy';

        ```python
        #3、or的工作原理
            条件：
                a = 10 or b = 'xxx' or c > 3 or d =4
            索引：
                制作联合索引(d,a,b,c)
                
            工作原理:
                对于连续多个or：mysql会按照条件的顺序，从左到右依次判断，即a->b->c->d
        
        ```

##### 10.9.6.2联合索引  

create index ind_mix on s1 ( id, email );

1. select * from s1 where id =1000000 and email = 'eva1000000@oldboy';
       在联合索引中如果使用了or条件索引就不能生效

2. select * from s1 where id =1000000 or email = 'eva1000000@oldboy';
       **最左前缀原则** ：在联合索引中，条件必须含有在创建索引的时候的第一个索引列
           select * from s1 where id =1000000;    能命中索引
           select * from s1 where email = 'eva1000000@oldboy';  不能命中索引
           (a,b,c,d) >>>> 只要包含a,且在最前,命中索引

3. 在整个条件中，从开始出现模糊匹配的那一刻，索引就失效了
       select * from s1 where id >1000000 and email = 'eva1000001@oldboy';    >>不生效
       select * from s1 where id =1000000 and email like 'eva%';  >>生效

4. 什么时候用联合索引

   只对 a 对abc 条件进行索引

   而不会对b，对c进行单列的索引

##### 10.9.6.3 单列索引

1. 单列索引
       选择一个区分度高的列建立索引，条件中的列不要参与计算，条件的范围尽量小，使用and作为条件的连接符
2. 使用or来连接多个条件
       在满上上述条件的基础上
       对or相关的所有列分别创建索引

##### 10.9.6.4覆盖 /合并 索引

1. 覆盖索引

   ```python
       # 如果我们使用索引作为条件查询，查询完毕之后，不需要回表查，覆盖索引
       # explain select id from s1 where id = 1000000;
       # explain select count(id) from s1 where id > 1000000;
   
   ```

2. 合并索引

   ```
    对两个字段分别创建索引，由于sql的条件让两个索引同时生效了，那么这个时候这两个索引就成为了合并索引
   
   ```

3. 执行计划 explain

   ```python
   执行计划 : 如果你想在执行sql之前就知道sql语句的执行情况，那么可以使用执行计划
       # 情况1：
           # 30000000条数据
               # sql 20s
               # explain sql   --> 并不会真正的执行sql，而是会给你列出一个执行计划
       # 情况2：
           # 20条数据 --> 30000000
               # explain sql
   
   ```

##### 10.9.6.5 总结

```python
# SQL索引的创建（单个、联合）、删除
# 索引的命中：范围，条件的字段是否参与计算(不能用函数)，列的区分度(长度)，条件and/or，联合索引的最左前缀问题
# 一些名词
    # 覆盖索引
    # 合并索引
# explain执行计划
# 建表、使用sql语句的时候注意的
    # char 代替 varchar
    # 连表 代替 子查询
    # 创建表的时候 固定长度的字段放在前面

```

### 10.10 事务 和 数据备份

```python
# begin;  # 开启事务
# select * from emp where id = 1 for update;  # 查询id值，for update添加行锁；
# update emp set salary=10000 where id = 1; # 完成更新
# commit; # 提交事务



##########################################

#语法：
# mysqldump -h 服务器 -u用户名 -p密码 数据库名 > 备份文件.sql

#示例：
#单库备份
mysqldump -uroot -p123 db1 > db1.sql
mysqldump -uroot -p123 db1 table1 table2 > db1-table1-table2.sql

#多库备份
mysqldump -uroot -p123 --databases db1 db2 mysql db3 > db1_db2_mysql_db3.sql

#备份所有库
mysqldump -uroot -p123 --all-databases > all.sql

# mysqldump -uroot -p123  day40 > D:\code\s21day41\day40.sql
# mysqldump -uroot -p123  new_db > D:\code\s21day41\db.sql




###########################恢复
[root@egon backup]# mysql -uroot -p123 < /backup/all.sql

#方法二：
mysql> use db1;
mysql> SET SQL_LOG_BIN=0;   #关闭二进制日志，只对当前session生效
mysql> source /root/db1.sql

```

### 10.11 sql 注入

```python
# username = input('user >>>')
# password = input('passwd >>>')
# sql = "select * from userinfo where name = '%s' and password = '%s'"%(username,password)
# print(sql)
# -- 注释掉--之后的sql语句
# select * from userinfo where name = 'alex' ;-- and password = '792164987034';
# select * from userinfo where name = 219879 or 1=1 ;-- and password = 792164987034;
# select * from userinfo where name = '219879' or 1=1 ;-- and password = '792164987034';

import pymysql

conn = pymysql.connect(host = '127.0.0.1',user = 'root',
                       password = '123',database='day41')
cur = conn.cursor()
username = input('user >>>')
password = input('passwd >>>')
sql = "select * from userinfo where name = %s and password = %s"
cur.execute(sql,(username,password))
print(cur.fetchone())
cur.close()

```

## 第十一章 web前端开发

### 11.1 html介绍



#### 11.1.1 html

1. HTML的定义：HyperText Mackeup Language ---超文本标记语言
2. 什么是超文本
   - 比如网页的超链接、图片、音频、视频都称为超文本。
3. 什么是标记
   - 即标签

##### 11.1.1.1 html 结构

1. 文档声明头
   - 指明该页面使用哪个 HTML 版本进行编译。
   - 文档就是一个html页面
2. html 标签
   1. 最外层标签--它表示文档内容的开始。
   2. html 包含2 部分
      1. 头部声明
      2. 内容部分
3. head 标签
   1. head标签就是这个人的基本信息，虽然这个没法直接观察到，但这些内容确实存在，并且是在网页中必不可少的。head用于表示网页的中的一个基础的信息(元信息)
4. body 标签
   1. 把一个网页比作一个人的话，那么body标签就是这个人的身体，肤色，痔疮。它里面的内容是看得见摸得着的。对于人的性格，意淫的想法，性取向等body是管不着的。那么body标签包含页面中所有的可见元素，比如网页中的文本的展示内容，漂亮meinv ，动听的音乐，炫酷的电影等都属于body所管。
5. 标签有单闭合和双闭合之分

##### 11.1.1.2 html 规范

- HTML不区分大小写，也就是说`<head>`和`<HEAD>`都可以
- HTML页面的后缀名是html或者htm(win32时代，系统只能识别3位扩展名时使用的。现在一般都使用.html
- HTML的结构
  - 声明部分：主要作用是用来告诉浏览器这个页面使用的是哪个标准。是HTML5标准。
  - head部分：将页面的一些额外信息告诉服务器。不会显示在页面上。
  - body部分：我们所写的代码必须放在此标签內。

##### 11.1.1.3 编写html的规范

- 所有标签都要正确的嵌套，不能交叉嵌套

  什么是交叉嵌套呢?

  ```
  <html><head></html><head>
  ```

  正确写法

  ```
  <html><head><head></html>
  ```

- 所有的标签尽量都小写，当然也可以大写，因为html中不区分大小写，但是小写相对比大写来说，它更易于阅读。

- 所有的标签要闭合

  - 双闭合 比如`<html></html>`
  - 单闭合 比如`<meta />`

##### 11.1.1.4 html 注释

在页面中，凡是被注释掉的代码，浏览器都不会去解析。

html中的注释

```
<!--这是我关键性的内容-->
```

**注释的作用**：

- 给自己看。随着页面的内容越多，高效的注释能让我们的代码更易阅读，并且关键性的代码我们可以用注释标注出来。
- 给别人看。比如自己写的html代码，有的地方代码需要说明一下，同时要转给别人看和解读分析的，这个时候就有必要使用html注释。

**正确使用注释**

```
<!--<!--这是我关键性的内容-->-->
```

**不能**一个html注释中，再放一个html注释，不然浏览器会如下解析。
![img](D:\md_down_png\image-20190130110314308.png)

#### 11.2.1 html 之 head 标签

head标签中的相关标签，是看不见摸不着的，仅仅是对应用于网页的一些基础信息(元信息)。

##### 11.2.1.1 meta标签(单闭合)

meta标签共有两个属性，它们分别是http-equiv属性和name属性，不同的属性又有不同的参数值，这些不同的参数值就实现了不同的网页功能。

1. http-equiv属性

   它用来向浏览器传达一些有用的信息，帮助浏览器正确地显示网页内容，与之对应的属性值为content，content中的内容其实就是各个参数的变量值。

   在html4.01版本中，我们使用下面配置来规定HTML 文档的字符编码。

   ```
   <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
   ```

   但在html5版本中，我们使用更简化的方式来规定HTML 文档的字符编码。

   ```html
   <meta charset="UTF-8"/>
   ```

2. name属性

主要用于页面的关键字和描述，是写给搜索引擎看的，关键字可以有多个用 ‘,’号隔开，与之对应的属性值为content，content中的内容主要是便于搜索引擎机器人查找信息和分类信息用的。

```
<meta name="Keywords" content="网易,邮箱,游戏,新闻,体育,娱乐,女性,亚运,论坛,短信"/>
```

这些关键词，就是告诉搜索引擎，这个网页是干嘛的，能够提高搜索命中率。让别人能够找到你，搜索到。

```
<meta name="Description" content="网易是中国领先的互联网技术公司，为用户提供免费邮箱、游戏、搜索引擎服务，开设新闻、娱乐、体育等30多个内容频道，及博客、视频、论坛等互动交流，网聚人的力量。"/>
```

设置Description页面描述，那么百度搜索结果，就能够显示这些语句，这个技术叫做**SEO**（search engine optimization，搜索引擎优化)。

![img](D:\md_down_png\image-20190510120735876.png)

##### 11.1.2.2 title 标签(双闭合)

主要用来告诉用户和搜索引擎这个网页的主要内容是什么，搜索引擎可以通过网页标题，迅速的判断出当前网页的主题。

#### 11.3.1 html 之 body 标签

##### 11.3.1.1 文本标签

- 标题和段落

  - h1 ~ h6

  - p(paragraph)

    `&nbsp;&nbsp;&nbsp;&nbsp;`是html'的4个空格

##### 11.3.1.3 字体标签

- u >>
- strong >>加粗和强调
- em >>斜体
- i >>斜体

- hr >>水平横线(`<hr/>`)
- br >>折行符号(分行显示文本`<br/>`)---相当于word文档中的回车键
- HTML特征
  对换行和空格不敏感
  空白折叠

##### 11.3.1.4 列表标签

1. ul - li无须列表
2. ol - li 有序列表

##### 11.3.1.5 div 标签 和 span 标签

1. div便签
   1. 盒子标签,divsion --分割
   2. 把网页分为成不同的独立逻辑的区域
2. span 标签--span的意思是“范围、跨度”

##### 11.3.1.6 table 标签--表格

1. 创建表格的四个元素：

   table、tr、th、td

   1. `<tabel>...</table>`:整个表格以`<table>`标记开始、`</table>` 标记结束。
   2. `<tr>...</tr>`：表格的一行，那么以为着有几个tr，表格就有几行。
   3. `<td>...</td>`:表格的一个单元格，一行中包含几对`<td></td>`,说明一行中就有几列。
   4. `<th></th>`:表格的头部的一个单元格，**表格表头**。
   5. 表格中列的个数，取决于一行中数据单元格的个数

   效果展示：
   ![img](D:\md_down_png\image-20190214152414338.png)

   总结：

   1. table表格在没有添加css样式之前，在浏览器中显示是没有表格线的
   2. 表头，也就是th标签中的文本默认为**粗体**并且**居中**显示

**给`<table>`添加border属性，为表格添加边框**

table表格在没有添加css样式之前，是没有边框的。我们可以给`<table>`添加一个border属性，并且设置该值为1.

语法：

```
<table border='1'>....</table>

```

效果展示：

![img](D:\md_down_png\image-20190214155641224.png)

> 如果想实现很细的表格边线，
>
> <table cellspacing='0' border='1'>......</table>

**caption标签，为表格添加标题**

上述的表格我们还需要添加一些标签进行优化，可以添加**标题**。代码如下：

标题--用来描述表格内容，标题的显示位置：表格的上方。

语法

```html
<table border="1" cellspacing='0'>
    <caption>商品清单</caption>
    <tr>
        <th>产品名称</th>
        <th>品牌</th>
        <th>总量</th>
        <th>入库时间</th>
    </tr>
    <tr>
        <td>电视机</td>
        <td>小米</td>
        <td>100</td>
        <td>2018-09</td>
    </tr>
    <tr>
        <td>音响</td>
        <td>小米</td>
        <td>200</td>
        <td>2018-08</td>
    </tr>
    <tr>
        <td>电冰箱</td>
        <td>海尔</td>
        <td>189</td>
        <td>2018-12</td>
    </tr>
</table>

```

##### 11.3.1.7 超链接标签  `<a>`

1.使用`</a><a>`标签，链接到另一个页面

网页中`</a><a>`标签，全称：anchor、锚点的意思。它在html中称为超链接标签，可以说它在网页中是无处不在的，只要有链接的地方，就有会这个标签。

语法：

```
<a href="目标地址" title="鼠标滑过显示的文本">链接显示的文本</a>

```

举个栗子:

比如我们我们做个效果。点击`百度一下`,会跳转到百度的首页。

代码如下：

```
<a href="http://www.baidu.com" title="点击进入百度">百度一下</a>

```

1. href指的是链接指向的页面的地址
2. title属性的作用，鼠标滑过链接文本时会显示这个属性的文本内容。这个属性在实际网页中开发中作用不大，主要方便搜索引擎了解链接地址的内容

> 注意：主要给文本加入了a标签后，文字的颜色就会自动变为**蓝色**（被点击过后的文本颜色为紫色），颜色很丑陋吧，不过没有关系后面我们学习了css样式就可以设置过来。后面讲解。

2.在新建浏览器窗口打开新的链接

<a>标签在默认情况下，链接的网页是在当前浏览器窗口打开的，有时候我们需要在新的浏览器窗口打开。

语法：

```
</a><a href="目标地址" target="_blank">点我！</a>

```

3.在当前浏览器中进行跳转

1. 网页的最顶部编写一个空标签，并且设置该标签的属性name=‘top’,`<div name="top"></div>`其实这个标签的name属性值，相当于一个锚的名字
2. 然后在网页的底部设置超链接，并且设置该`</a><a>`标签的href属性值一定为`#top`。表示点击a标签跳到锚名为top的指定的位置。
3. 好像还忘了一件事情，怎么然后网页滚动起来呢？其实这个很简单，当我们的内容大于了浏览器的高度，浏览器就会自动出现滚动条。

4.给`<a>`标签添加mailto属性，实现邮件链接

代码举例：

```
</a><a href="mailto:zhaoxu@tedu.cn">联系我们</a>

```

##### 11.3.1.8 img 标签

1. 插入图片使网页美观

   ```html
   <img src="图片地址" alt="下载失败时的替换文本" title="提示文本"/>
   
   ```

11.3.1.9 表单标签--用户交互

```html
<form   method="传送方式"   action="服务器地址和端口">

```

</form>

```
1. 文本输入框   密码输入框

   ```html
   <form>
       <!--文本输入框-->
      <input type="text" name="名称" value="文本" palceholder='请输入用户名'/>
        <!--密码输入框-->
      <input type="password" name="名称" value="密码" palceholder='请输入密码'/>
   </form>

```

- type
  - 当type为’**text**‘时，输入框为文本输入框
  - 当type为’**password**‘时，输入框为密码输入框
- **name:**为文本框命名，后期用来给后台程序Java、PHP使用
- **value:**为文本输入框设置默认值。(一般起到提示作用)
- **placeholder**：提示用户内容的输入格式

2. 单选框--radio  多选框 --checkbox

   checked=‘checked’时，该选项被默认选中

   示例代码：

   ```html
   <form>
       您的性别是？<br>
       男:<input type="radio" name="sex" value='0' checked='checked'>
       女:<input type="radio" name="sex" value='1'>
       <hr>
       您的爱好是？<br>
       吃饭<input type="checkbox" name="hobby" value="eat" checked='checked'>
       睡觉<input type="checkbox" name="hobby" value="sleep" checked='checked'>
       打豆豆<input type="checkbox" name="hobby" value="love">
   </form>
   
   ```

   效果展示：

   ![img](D:\md_down_png\image-20190215145744104.png)

   > 注意：同一组的单选按钮，name的值一定要一致，比如上面的例子为同一个名称’sex’，这样同一组的单选按钮才可以起到单选的作用。

3. 使用下拉列表框，节省空间

   下拉列表在网页中也时常用到，它可以有效的节省网页空间。既可以单选、又可以多选。

   代码如下：

   ```html
   <form>
       您要报名的课程?<br>
       <select name="course">
           <option value="python">python中级</option>
           <option value="go">go精讲</option>
           <option value="web" selected='selected'>web全栈</option>
           <option value="linux">linux深入挖掘</option>
       </select>
   </form>
   ```

   - select标签是下拉列表框标签

     select标签是下拉列表框标签

     - name：下拉列表的名字
     - value
       - ![img](D:\md_down_png\image-20190218090238734.png)

   效果展示：

   ![img](D:\md_down_png\6.gif)

4. 下拉列表进行多选

   下拉列表也可以进行多选操作，在`<select>`标签中设置`multiple="multiple"`属性，就可以实现多选功能，在windows操作系统中，进行多选时按下`ctrl`键同时进行`单击`(在Mac下使用Command+单击),可以选择多个选项。

   上节课案例修改：

   ```
   <form>
       您要报名的课程?<br>
       <select name="course" multiple='multiple'>
           <option value="python">python中级</option>
           <option value="go">go精讲</option>
           <option value="web" selected='selected'>web全栈</option>
           <option value="linux">linux深入挖掘</option>
       </select>
   </form>
   
   ```

   效果展示：

   ![img](D:\md_down_png\7.gif)

   > 注意：如果选项内容一多，会出现默认的滚动条

5. 使用提交按钮，提交数据

   在表单中有两种按钮可以使用，分别为：提交按钮(submit)和重置按钮(reset)。

   当用户需要提交表单信息到服务器时，需要用到提交按钮。

   语法：

   ```
   <input type='submit' value='提交'>
   
   ```

   - type:只有当type值设置为submit时，按钮才会有提交作用
   - value:按钮上显示的文字

   例子：

   ```
   <form method="post" action="active.php">    <label for='myName'>姓名:</label>    <input type="text" value="" id='myName'  name="myName">    <input type="submit" value="提交"></form>
   
   ```

##### 11.3.1.9 label标签

 标识用户项目中的标题

- 它通常关联一个控件,label中的for属性，与某表单控件中的id对应。作为label元素的标签控件。
- 另外，当用户单击该label标签时，浏览器就会自动将焦点转到和标签相关的表单控件上。

> 注意：label标签的for属性中的值一定与相关标签的id属性值要相同

重置按钮也非常简单，当用户在填写表单信息时，发现书写错误，可以使用`重置按钮`使输入框回到初始化状态。我们只需要把type设置为`reset`即可。

语法：

```
<input type='reset' value='重置'>

```

#### 11.3.2 html 总结

主要有 :

1. 双闭和标签
2. 单闭合标签
3. 结构
   - head
     - title 网站的标题
     - meta 基本网站源信息标签
     - link  连接css文件
     - script 连接javascript 文件
     - style 内嵌样式
4. body标签 

- 标题和段落

  - h1 ~ h6

  - p(paragraph)

    `&nbsp;&nbsp;&nbsp;&nbsp;`是html'的4个空格

- a anchor 锚点 超链接标签

  - href 链接的网址/回到顶部/跳转邮箱/下载文件
  - title 鼠标悬浮上的标题
  - style 行内样式  -->color    text-decoration下划线
  - target 目标
    - 默认是_self 在当前页面中打开新的链接_
    - blank 在新的空白页面打开新的链接

- img (指向图片资源)

  - src 链接的图片资源
  - title 标题
  - style
  - alt 图片加载失败的时候 显示文本

- ul 无序列表

  - li

- ol 有序列表

  - li

- table (表格)
  `id name age sex
  1 沛期 19 女
  2 女神 18 男
  3 太白 38 未知`

  ```html
  /*简易的有边框线的表格*/
  <table border='1' cellspacing=0>
    <th>
      <td>id</td>
      <td>name</td>
    </th>
    <tr>
      <td>1</td>
      <td>mjj</td>
  </tr>
  </table>
  
  ```

- form

  1. 属性action -- 提交到服务器的地址，如果是空的字符串，它表示当前服务器地址

  2. method:提交的方式---get和post

     - get:明文不安全，地址栏只允许提交**2kb**的内容，提交的内容会在地址上显示 。显示的方式`http://127.0.0.1:8080/index.html?name=value&name2=value2`
     - post:密文提交安全，可以提交任意内容
     - post:密文提交安全，可以提交任意内容
     - 后期要把HTTP协议的内容（重点）

  3. 表单控件中的name和value属性的意义

     ```html
     name属性值：提交到当前服务器的名称
     value属性值：提交到当前服务器的值
     以后给服务器来使用
     
     ```

  - input

    - type 控件的类型
      - text 单行文本输入框
      - password 密码框
      - radio 单选框
        - 互斥效果--设置相同的name值
        - 如何默认选中--checked属性
      - checkbox 多选框
        - 默认选中添加checked属性
      - submit 提交按钮--value改按钮名
      - file 上传文件
      - datetime_local/date/datetime
    - name  ---->名称 提交服务器的键值对的 name
    - value  ---->值 提交服务器的键值对的 value

  - select name multiple:多选框

    - option

      - value

      - selected 默认选中

        ```html
        <select>
          <option name  value selected >抽烟</option>
          <option name  value selected >喝酒</option>
          <option name  value selected >烫头</option>
        </select>
        
        ```

  - textarea --多行文本输入框

    - name
    - value
    - cols --列
    - rows --行

  - div--将网站分割成独立的逻辑区域 division 分割

  - span: 小区域标签,在不影响文本正常显示的情况下，单独设置对应的样式

##### 11.3.2.1 行标签 行内标签  行内块

1. 在一行内显示的标签

   `b strong i em a img input td`

2. 独占一行的标签

   `h1-h6` ul ol li table form tr p div

### 11.2  CSS

- HTML:超文本标记语言。从**语义**的角度描述页面**结构**
- CSS:层叠样式表。从**审美**的角度负责页面**样式**
- JS:Javascript。从**交互**的角度描述页面的**行为**

#### 11.2.1   css 层叠样式表

css它是一个很神奇的东西，设计者可以通过修改样式表的定义从而使我们的网页呈现出完全不同的外观。比如大家耳熟能详的购物网站：淘宝、京东、小米等等。

CSS全称为“层叠样式表”(`Cascading Style Sheets`),它主要是用于定义HTML内容在浏览器内的显示样式，比如文字大小、颜色、字体加粗等等。

```html
p {
    font-size:12px;
    color:red;
    font-weight:bold;
}

```

![img](D:\md_down_png\image-20190218102637946.png)

1. ### css的语法

   那么上节课咱们讲解完css的优势之后，我们重点来看一下，如果编写我们的css。

   css样式由**选择符**和**声明**组成，而声明又由**属性**和**值**组成，如下图所示：

   ![img](D:\md_down_png\image-20190218120250643.png)

   - **选择符**:我们又称为选择器，指明网页中应用样式规则的元素，如上述图中网页的所有(p)的文本变为蓝色，而其它元素(如ul,ol等等)不会受到影响。

   - **声明**：在英文大括号”{}”中的就是声明，属性和值之间用英文冒号”：“分割。当有多条声明时，中间可以英文分号”;”分割，如下所示：

     ```
     p{font-size：12px;color:red;}
     
     ```

   再举个例子有助于大家理解什么是**选择符**、**属性**、**值**

   比如我家隔壁有三个人，并且有两个人同名，都叫王大大，并且身高是175，年龄都是20岁。还有一个人叫李小小，年龄18岁。那么我就可以这样表示。

   ```
   王大大{身高:175cm;年龄:20岁;}
   
   ```

   那么。王大大，就是选中了隔壁三个中的其中同名的两个人。叫王大大的这个两个人，他们有共同的特征，比如身高和年龄一样。那么这些特征我们称为叫声明。身高和年龄表示属性，175cm和20岁表示值。那么我们就能将这个人表示出来了。那么对比上述的例子，我们来看css的话，是否变得很简单呢？答案是的。

2. ### css注释

   在之前将HTML的注释的时候，我们讲解了它的作用，在这里就不一一阐述了，大家可以翻看html的注释，在css也有注释语句：

   用`/*注释语句*/`来表明(html中使用`<!--注释语句-->`)。就像下面代码：

   ```css
   /*设置段落默认样式*/p {    font-size：12px;    color: red;}
   
   ```

注意---CSS也有注释嵌套

#### 11.2.3 css三种引入方式

- 行内样式(内联式)

  1. css样式表把css代码直接写在现有的HTML标签中 , 把对应的style属性，写在p标签的开始标签中。css样式代码要写在style=“”双引号中，如果有多条css样式代码设置可以写在一起，中间用分号隔开。

  ```html
  <div style='color:red;'>mjj</div>
  
  ```

- 嵌入式

  1. 嵌入式css样式，就是可以把css样式代码写在`<style type="text/css"></style>`标签之间。

  ```html
  在head标签内部书写style
  <style>
  /*css代码*/
  </style>
  ```

- 外接式

  1. 外部式css样式(也可称为外链式)就是把css代码写一个单独的外部文件中，这个css样式文件以”.css”为扩展名，在``内（不是在style标签内）使用``标签将css样式文件链接到HTML文件内，

  ```html
  <link href='css/index.css' rel='stylesheet'
  ```

  ```
  <link rel="stylesheet" href="index.css" type="text/css"/>
  ```

  - css样式文件名称以有意义的英文字母命名，如main.css、index.css、base.css等。
  - rel=”stylesheet”
    - rel:relationship的缩写，rel属性用于定义链接的文件和HTML文档之间的关系
    - stylesheet：文档的外部样式表
  - href：Hypertext Reference的缩写。意思是指定超链接(之前学习a标签的时候)目标的URL。是css代码的一种。href属性的值为样式表文件的地址。
  - `<link/>`标签位置一般写在``标签之内

  **行内样式的优先级 > 内嵌式 和 外接式**

  **内嵌 和 外接 要看谁在后面 , 在后面的优先级高**

  **总结:** 就近原则“（离被设置元素越近优先级别越高）

#### 11.2.4 **css 选择器**

什么是选择器--选中标签

1. 每一条css样式声明由两部分组成，如下：

   ```css
   选择器{
   	样式;
   	}
   
   ```

##### 11.2.4.1 基础选择器**

1. id 选择器 --id 是唯一的(id的命名规则和py的变量命名规则一致--数字不可在前)

   #xxx

2. 类选择器

   class ---可以重复 ,归类  ,类也可设置多个

   ```html
   /*以下是内嵌样式*/ --放在head内
   <style>
     .box{
       width:200px;
       height:200px;
       background-color:yellow;
    }
     .active{
       border-radius: 200px;
    }
   </style>
   <div class='box active'></div>
   <div class='box'></div>
   <div class='box'></div>
   
   ```

   语法 -- `.xxx`

   **公共类的概念**

   如果我们有公共类的概念，我们会通过需求发现，p1和p2有公共属性字体颜色为绿色，p1和p3有公共属性字体大小为20px，p2和p3有公共属性字体粗细为更粗。那么我们可以给每个p标签设置相应的类，代码如下：

   ```
   <p class="lv big">小猿圈</p>
   <p class="lv bold">小猿圈</p>
   <p class="big bold">小猿圈</p>
   
   ```

   > 其中lv代表绿色，big代表字体大小20px,bold代表字体更粗

   css代码

   ```
   <style>
       .lv{
           color:green;
       }
       .big{
           font-size:20px
       }
       .bold{
           font-weight:bold;
       }
   </style>
   
   ```

   那么，会发现明显更有效的使用类，能有效的减少一些冗余性的代码。

3. 标签选择器

   ```html
   div{}
   p{}
   ul{}
   ol{}
   ​```
   
   ```

##### 11.2.4.2 高级选择器

1. 后代选择器

   - 顾名思义，所谓后代，就是父亲的所有后代(包括儿子、孙子、重孙子等)。
   - 使用空格表示后代选择器，上面表示 div是父元素，而p是div的后代元素。

   ```html
   div p{
     color: red;
   }     /*有空格*/
   
   ```

2. 子代选择器

   - 子代，仅仅表示父亲的亲儿子，只有亲儿子。使用`>`表示子代选择器。

   ```html
   div>p{
     color:red;
   }
   
   ```

3. 组合选择器

   ```html
   div,p,body,html,ul,ol....{
     padding: 0;
     margin: 0;
   }
   
   ```

4. 交集选择器

   ```html
   div.active{
    
   }
   
   ```

5. 通用选择器

   通用选择器是功能最强大的选择器，它使用一个*号来表示，它的作用是匹配html中所有标签元素。使用它，我们可以对网页进行重置样式，以按照产品需求来开发对应的网页。

   对页面中所有的文本设置为红色。

   ```css
   *{color:red;}
   
   ```

#### 11.2.5 选择器深入

1. 伪类选择器

   对于a标签，如果想设置a标签的样式，要作用于a标签上，对于继承性来说，a标签不起作用的

   ```css
   /*LoVe HAte*/
   
   /*a标签没有被访问时候设置的属性*/
   a:link{
     /*color: red;*/
   }
   /*a标签被访问时候设置的属性*/
   a:visited{
     color:yellow;
   }
   /*a标签悬浮时设置的属性*/
   a:hover{
     color: deeppink;
   }
   /*a标签被摁住的时候设置的属性*/
   a:active{
     color: deepskyblue;
   }
   
   ```

2. 属性选择器

   ```css
   input[type='text']{
     background-color: red;
   }
   input[type='checkbox']{
   }
   input[type='submit']{
   }
   
   ```

3. 伪元素选择器

   ```css
   p::first-letter{
     color: red;
     font-size: 20px;
     font-weight: bold;
   }
   
   p::before{
     content:'@';
   }
   /*解决浮动布局常用的一种方法*/
   p::after{
     /*通过伪元素添加的内容为行内元素*/
     content:'$';
   }
   
   ```

#### 11.2.6层叠性和继承性

- 继承性：在css有某些属性是可以继承下来，color,text-xxx,line-height,font-xxx是可以继承下来

##### 11.2.6.1 权重比较规则：

1. 继承来的属性权重为0

   ```html
   前提是选中了标签
   权重比较；(100 , 10 , 1  )
      1.数选择器数量： id 类 标签 谁大它的优先级越高，如果一样大，后面的会覆盖掉前面的属性
      2.选中的标签的属性优先级用于大于继承来的属性，它们是没有可比性
      3.同是继承来的属性
        3.1 谁描述的近，谁的优先级越高
        3.1 描述的一样近，这个时候才回归到数选择器的数量
   
   ```

### 11.2.7 css的盒模型(重点)

1. 元素分类

   在css中，html中的标签元素大体被分为三种不同的类型：块状元素、内联元素(也叫行内元素)和内联块元素。

   1. 常用的块状元素有：

      - 独自占据整一行,可以设置宽高

      ```css
      <div>、<p>、<h1>~<h6>、<ol>、<ul>、<li>、<dl>、<dt>、<table>、<form>
      
      ```

   2. 常用的内联元素有：

      - 所有的内联元素在一行内显示 , 不可设置宽高

      ```css
      <a>、<span>、<i>、<em>、<strong>、<label>
      
      ```

   3. 常见的内联块状元素有：

      - 在一行内显示 , 可以设置宽高

      ```css
      <input>、<img>
      
      ```

      **注意：标签分类的特点是对现有的HTML常用标签进行分类，那么这些特点在后面的学习过程中我们还可以通过display属性进行强制修改规则。**

2. 盒模型介绍 -->所有html标签都可以看做是盒子

   ```css
   width: 内容的宽度
   height：内容的高度
   padding：内边距，border到内容的距离
   border：边框
   margin：外边距
   ```

![img](file:///C:\Users\Administrator\AppData\Roaming\Tencent\Users\1821333144\QQ\WinTemp\RichOle\NBWW{IEZ6LDKEC6_4~ZRVUF.png)

3. padding 的设置

   ```css
   1. 4个单独方法设置:
           padding-top:10px;
           padding-right:3px;
           padding-bottom:50px;
           padding-left:70px;
   2. 综合属性设置 : 多个属性用空格隔开。
           /*上 右 下 左 四个方向*/  -->顺时针方向
           padding: 20px 30px 40px 50px ;
           /*上 左右  下*/
           padding: 20px 30px 40px;
           /* 上下 左右*/
           padding: 20px 30px;
           /*上下左右*/
           padding: 20px;
   ```

4. border 边框 -->**粗细  线性样式 颜色**

   ```css
   1. 三要素书写
           border-width:3px;
           border-style:solid;
           border-color:red;
   2. 综合 
           border:3px solid red;
   3. 清除默认边框
           border:none;或者border:0;
   input输入框它有默认的边框，如果我们想制作好看的输入框的话，首先先得把默认的清除掉，然后我们再按照需求来更改，并且我们会发现焦点选中输入框的时候有一条蓝色的外线。那么我们也需要将它清除 ,
           使用属性outline:none;
   
   ```

5. 外边距 margin -->表示盒子到另一个盒子的距离。既然是两者之间的距离，4个方向

   ```css
   垂直方向外边距
   
   盒模型的外边距水平方向上不会出现问题，在垂直方向上会出现“外边距合并”的现象。
   只有在垂直方向上，当两个同级的盒子，在垂直方向上设置了margin之后，那么以较大者为准。
   我们如果想让上下的两个盒子中间有间距，只需要设置一个盒子的一个方向即可。没必要去碰触外边距塌的问题
   
   ```

### 11.2.8  常用的格式化排版

##### 11.2.8.1 字体属性

1. 字体--`body{font-family:'微软雅黑','宋体','斜体';}`

   - 备选字体是为了防止用户电脑上没有”微软雅黑“这个字体。

2. 字体大小 --`font-size`

   px--像素是指由图像的小方格组成的，这些小方块都有一个明确的位置和被分配的色彩数值，小方格颜色和位置就决定该图像所呈现出来的样子。

3. 字体颜色--由三原色组合出万彩色

   1. 英文单词表示法
   2. rgb表示法
   3. 十六进制表示法

4. 字体样式 --'font-style : '

   1. normal --默认的，文本设置为普通字体
   2. italic --如果当前字体的斜体版本可用，那么文本设置为斜体版本；如果不可用，那么会利用 oblique 状态来模拟 italics。常用
   3. oblique --将文本设置为斜体字体的模拟版本，也就是将普通文本倾斜的样式应用到文本中。

5. 字体粗细 -- `font-weight`

   1. normal  --普通的字体粗细，默认
   2. bold --加粗的字体粗细
   3. lighter --比普通字体更细的字体
   4. bolder --比bold更粗的字体
   5. 100~900  normal 取400

##### 11.2.8.2 文本属性

1. 文本修饰-下划线,删除线 --`text-decoration`

   1. none --无文本修饰
   2. underline --下划线
   3. overline -- 上划线
   4. line -through 删除线

2. 文本缩进 --首行缩进2字符等等 --`text-ident`

   ```css
   text-ident : 2em ; 
   推荐
   ```

3. 行间距 --`line-height`--2em   -->2倍行距

4. 中文字间距 --`letter-spacing`     英文单词间距 --`word-spacing`

5. 文本对齐 --`text-align`-->`left,center,right,justify(仅限于英文，两端对齐)`

##### 11.2.8.3. html 的嵌套关系

```html
<!--块级标签：1.独占一行 2.可以设置宽高，如果不设置宽，默认是父标签的100%宽度-->    h1-h6` ul ol li table form tr p div
<!--行内标签：1.在一行内显示 2.不可以设置宽高，如果不设置宽高，默认是字体的大小 -->  b strong i em a img input td`
    <!--行内块标签： 1.在一行内显示 2.可以设置宽高 --> input img
   
 在网页中：
 行内转块和行内块是非常使用 
 display:
      inline
      inline-block
      block
嵌套关系：
  1.块级标签可以嵌套块级和行内以及行内块
  2.行内标签尽量不要嵌套块级
  3.p标签不要嵌套div，也不要嵌套p
   可以嵌套a/img/表单控件
```

### 11.2.9 CSS 布局

##### 11.2.9.1 浮动

浮动是网页布局中非常重要的一个属性。那么`浮动`这个属性一开始设计的初衷是为了网页的`文字环绕效果`

**总结：如果想实现网页中排版布局，比如一行内显示对应的标签元素，可以使用浮动属性。浮动可以实现元素并排。**

1. 浮动属性-->float来表示

   1. none--不浮动,所有之前讲解的HTML标签默认不浮动
   2. left
   3. right
   4. inherit --继承父辈的浮动属性

2. 浮动的现象

   ```css
   1.脱离标准文档流，不在页面上占位置 “脱标”
   2.文字环绕 设置浮动属性的初衷
   3.“贴边” 现象： 给盒子设置了浮动，会找浮动盒子的边，如果找不到浮动盒子的边，会贴到父元素的边，如果找到了，就会贴到浮动盒子的边上
   4.收缩效果
   
   类似于 -->一个块级元素转成行内块
   ```

3. 标准文档流

   文档流指的是元素排版布局过程中，元素会**默认**自动从左往后，从上往下的流式排列方式。

   即不对页面进行任何布局控制时，浏览器默认的HTML布局方式，这种布局方式从左往右，从上往下，有点像流水的效果，我们称为`流式布局`。

4. 浮动元素贴靠

   *总结：当一个元素浮动之后，它会被移出正常的文档流，然后向左或者向右平移，一直平移直到碰到了所处的容器的边框，或者碰到***另外一个浮动的元素**

##### 11.2.9.2 浮动的破坏性-->撑不起父盒子的高度

浮动之后，蓝色的盒子因为脱离了标准文档流，它撑不起父盒子的高度，导致父盒子`高度塌陷`。如果网页中出现了这种问题，会导致我们整个网页的布局紊乱。我们一定要去解决这种父盒子高度塌陷的问题。

***总结：浮动能网页实现排版布局，但是同样也会给网页带来一定的问题(父盒子高度塌陷)，只要我们解决了浮动给网页带来的问题，就能对网页进行高效的排版布局。那么下节课知晓。***

1. 清楚浮动的方式

   - 父盒子设置固定高度

     ```css
     给父元素添加固定高度 （不灵活，后期不易维护）如果未来子盒子的高度需求发生了改变(网页的多处地方)，那么我们得手动需要更改父盒子的高度。后期不易维护。
     
     例如-->小米的万年不变导航栏
     应用 :  网页中盒子固定高度区域，比如固定的导航栏。
     缺点 : 使用不灵活，后期不易维护
     ```

   - 内墙法

     ```css
     内墙法：给最后一个浮动元素的后面添加一个空的块级标签，并且设置该标签的属性为clear:both;
     问题：冗余
     ```

   - 伪元素清除法(推荐使用)-->完美利用伪元素选择器

     ```css
     .clearfix::after{
         content:'';
         display: block;
         clear: both;
         /*visibility: hidden;*/
         /*height: 0;*/
     }
     content:'.';表示给.clearfix元素内部最后添加一个内容，该内容为行内元素。
     display:block;设置该元素为块级元素，符合内墙法的需求。
     clear:both；清除浮动的方法。必须要写
     overflow:hidden;
     height:0;
     	如果用display:none;,那么就不能满足该元素是块级元素了。overflow:hidden;表示隐藏元素，与		display:none;不同的是，前者隐藏元素，该元素占位置，而后者不占位置。
     ```

   - overflow:hidden (常用)

     ```css
     因为overflow:hidden;会形成BFC区域，形成BFC区域之后，内部有它的布局规则
     计算BFC的高度时，浮动元素也参与计算
     但是小心overflow：hidden它自己的本意
     ```

     1. overflow -->定义一个元素的内容太大而无法适应盒子的时候该做什么。
        1. visible : 默认值。内容不会被修剪，会呈现在元素框之外
        2. hidden : 内容会被修剪，并且其余内容不可见
        3. scroll : 内容会被修剪，浏览器会显示滚动条以便查看其余内容
        4. auto : 由浏览器定夺，如果内容被修剪，就会显示滚动条
        5. inherit : 规定从父元素继承overflow属性的值
     2. overflow 设置了hidden|auto|scroll属性后,它会形成一个BFC区域，我们叫做它为`块级格式化上下文`。BFC只是一个规则。它对浮动定位都很重要。浮动定位和清除浮动只会应用于同一个BFC的元素。
     3. 浮动不会影响其它BFC中元素的布局，而清除浮动只能清除同一BFC中在它前面的元素的浮动。

##### 11.2.9.3 BFC 定义 

1. Box和Formatting Context

2. B: BOX即盒子，页面的基本构成元素。分为 inline 、 block 和 inline-block三种类型的BOX

3. FC: Formatting Context是W3C的规范中的一种概念。它是页面中的一块渲染区域，并且有一套渲染规则，它决定了其子元素将如何定位，以及和其他元素的关系和相互作用。

4. 块级格式化上下文 --它是一个独立的渲染区域，只有Block-level box参与， 它规定了内部的Block-level Box如何布局，并且与这个区域外部毫不相干。

5. BFC 布局规则(部分)

   ```csss
   1.内部的Box会在垂直方向，一个接一个地放置。
   2.Box垂直方向的距离由margin决定。属于同一个BFC的两个相邻Box的margin会发生重叠
   3.每个元素的margin box的左边， 与包含块border box的左边相接触(对于从左往右的格式化，否则相反)。即使存在浮动也是如此。
   4.BFC的区域不会与float 元素重叠。
   5.BFC就是页面上的一个隔离的独立容器，容器里面的子元素不会影响到外面的元素。反之也如此。
   6.计算BFC的高度时，浮动元素也参与计算
   ```

6. 那些元素会生成BFC

   ```css
   1.根元素
   2.float属性不为none
   3.position为absolute或fixed
   4.display为inline-block
   5.overflow不为visible
   ```

##### 11.2.9.4 定位

1. 标准文档流下的布局 :
   - `盒模型`，元素的内容宽高、padding、border以及margin
   - 元素的分类：块级元素、行内元素、行内块元素之间的区别。
   - 盒模型的margin，垂直方向上会出现外边距合并的问题
2. 定位的整个想法是允许我们**覆盖**上述描述的基本标准文档流的行为，以产生有趣的效果。
3. css **position**属性用于指定一个元素在文档中的定位方式。`top`，`right`，`bottom`，`left`属性则决定了该元素的最终位置。
   1. static --**默认。静态定位**， 指定元素使用正常的布局行为，即元素在文档常规流中当前的布局位置。此时 `top`, `right`, `bottom`, `left` 和 `z-index`属性无效。
   2. relative --**相对定位**。 元素先放置在未添加定位时的位置，在不改变页面布局的前提下调整元素位置（因此会在此元素未添加定位时所在位置留下空白）
      - 相对定位的元素是在文档中的正常位置的偏移，但是不会影响其他元素的偏移。
      - 给一个标准文档流下的盒子单纯的设置相对定位，与普通的盒子没有任何区别
      - 不脱离标准文档流，单独设置盒子相对定位之后，如果不用`top,left,right,bottom`对元素进行偏移，那么与普通的盒子没什么区别。
      - 有压盖现象。用`top,left,right,bottom`对元素进行偏移之后，明显定位的元素的层级高于没有定位的元素
   3. absolute -- **绝对定位**。不为元素预留空间，通过指定元素相对于最近的非 static 定位祖先元素的偏移，来确定元素位置。绝对定位的元素可以设置外边距（margins），且不会与其他边距合并
      - 相对定位的元素并没有脱离标准文档流，而绝对定位的元素则脱离了文档流。在标准文档流中，如果一个盒子设置了绝对定位，那么该元素不占据空间。并且绝对定位元素相对于最近的非static祖先元素定位。当这样的祖先元素不存在时，则相对于根元素页面的左上角进行定位。
      - 绝对定位的盒子是以最近的非static定位的父元素进行定位
      - 相对于最近的非static祖先元素定位，如果没有非static祖先元素，那么以页面左上角进行定位。
      - 脱离了标准文档流，不在页面中占位置
      - 层级提高，做网页压盖效果
   4. fixed --**固定定位**。 不为元素预留空间，而是通过指定元素相对于屏幕视口（viewport）的位置来指定元素位置。元素的位置在屏幕滚动时不会改变
      - 它跟绝对定位基本相似，只有一个主要区别：绝对定位固定元素是相对于html根元素或其最近的定位祖先元素，而固定定位固定元素则是相对于浏览器视口本身。这意味着你可以创建固定的有用的网页效果，比如固定导航栏、回到顶部按钮，小广告等。

```css
position:static | relative | absolute | fixed;
		静态      相对        绝对        固定

```

1. 相对定位 relative

   **特征 :**与标准文档流下的盒子没有任何区别 . 留“坑”，会影响页面布局

   **作用 : **做“子绝父相”布局方案的参考

   **参考点 :** 以原来的盒子作为参考点

2. 绝对定位 absolute

   **参考点 : ** 1. 如果单独设置一个盒子为绝对定位

   ```css
   以top描述，它的参考点是以body的（0，0）为参考点
   以bottom描述，它的参考点是以浏览器的左下角为参考点
   
   ```

   **子绝父相 : **以最近的父辈元素的左上角为参考点进行定位

   **特征 : ** 1.脱标
   			2.压盖
   			3.子绝父相

3. 固定定位

   1.脱标
   2.固定不变
   3.提高层级

   参考点：
   	以浏览器的左上角为参考点

###### 11.2.9.4.1 子绝父相

1. **注意：**子绝父绝，子绝父固，都是以最近的非static父辈元素作为参考点。父绝子绝，子绝父固，没有实战意义，布局网站的时候不会出现父绝子绝。

   因为绝对定位脱离标准流，影响页面的布局。

   相反`父相子绝`在我们页面布局中，是常用的布局方案。因为父亲设置相对定位，不脱离标准流，子元素设置绝对定位，仅仅的是在当前父辈元素内调整该元素的位置。****

###### 11.2.9.4.2  z_index

只适用与定位的元素，z-index:auto;

```csss
z-index只应用在定位的元素，默认z-index:auto;
z-index取值为整数，数值越大，它的层级越高
如果元素设置了定位，没有设置z-index，那么谁写在最后面的，表示谁的层级越高。(与标签的结构有关系)
从父现象。通常布局方案我们采用子绝父相，比较的是父元素的z-index值，哪个父元素的z-index值越大，表示子元素的层级越高。

```

##### 11.2.9.5 背景属性和边框

1. background --背景

   ```css
   /*设置背景图*/
   background-image: url("xiaohua.jpg");
   background-repeat: no-repeat;
   /*调整背景图的位置*/
   background-position: -164px -106px;
   
   
   ```

2. border-radius

   ```css
   border-radius 设置圆角或者圆
   
   ```

   传统的圆角生成方案，必须使用多张图片作为背景图案。css3的出现，使得我们再也不必浪费时间去制作这些图片，并且还有其他多个有点：

   - 减少维护的工作量。图片文件的生成、更新、编写网页代码，这些工作都不再需要了。

   - 提高网页性能。由于不必再发出多条的HTTP请求，网页的载入速度将变快

   - 增加视觉可靠性。（网络拥堵、服务器出错、网速过慢等等），背景图片会下载失败，导致视觉效果不佳。CSS3就不会发生这种情况。

     ![1559302452789](D:\md_down_png\1559302452789.png)

     半圆--

     ![1559302491668](D:\md_down_png\1559302491668.png)

3. 阴影

   ```css
   box-shadow: 水平距离 垂直距离 模糊程度 阴影颜色 inset
   box-shadow: h-shadow v-shadow blur color inset;
   
           .box{
               width: 200px;
               height: 200px;
               background-color: red;
               box-shadow: 0 0 30px gray;
           }
   
   ```

   阴影方法 ![1559302625324](D:\md_down_png\1559302625324.png)

4. 雪碧图

   **使用雪碧图的使用场景**

   - 静态图片，不随用户信息的变化而变化

   - 小图片，图片容量比较小(2~3k)

   - 加载量比较大

     一些大图不建议制作雪碧图

   **优点**

   - 有效的减少HTTP请求数量
   - 加速内容显示

   每次请求一次，就会和服务器连接一次，建立连接是需要额外的时间开销的

   **雪碧图的实现原理**

   它通过css的背景属性的backrground-position的来控制雪碧图的显示。

   控制一个层，可显示的区域范围大消息，通过一个窗口，进行背景图的移动。

##### 11.2.9.6 css 常见布局方案

在前端开发中页面布局总是最开始的工作，就像盖楼时，先搭建框架，然后再舔砖，最后再上颜色。前端也是一样的，如果想制作一款网站，首先先做好页面的布局工作。

第一种： 单列布局，这是最简洁的一种。整个页面感觉很干净。目前主流的电商网站基本上都是使用这种布局。

第二种：两列布局，一般技术分享站点比较常见

![img](https://book.apeland.cn/media/images/2019/05/26/image-20190326165654482.png)

第三种：三列布局。这种三列、四列、甚至五列…..都是我们常见的布局。很多网站中都会存在这种多列的布局方案

###### 11.2.9.6.1 网页中的常见问题

###### css命名规范

参考此链接<http://www.divcss5.com/jiqiao/j4.shtml#no2> 大家不需要花大量的时间背。

项目目录规范

![1559302768290](D:\md_down_png\1559302768290.png)

###### 确定错误发生的位置

假如错误影响了整体布局，则可以逐个删除div块，直到删除某个div块后显示恢复正常，即可确定错误发生的位置。这样我们可以更精准的找到错误点，进行排错。

###### 是否重设了默认的样式?

制作网页时，我们要清除掉默认的元素的padding和margin，使得我们更易去计算盒模型的大小。

**行内元素水平居中显示**

第一种line-height+text-align

```
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <style>
        p{
            width: 200px;
            height: 200px;
            background: #666;
            color:#fff;
            line-height: 200px;
            text-align: center;
        }
    </style>
</head>
<body>
    <p>
        MJJ
    </p>
</body>
</html>

```

第二种 给父元素设置`display:table-cell;`,并且设置`vertical-align:middle`

```
div{
    position: relative;
    width: 200px;
    height: 200px;
    background: #666;
    color:#fff;
    text-align: center;
    display: table-cell;
    vertical-align: middle;
}

```

**块级元素水平垂直居中**

用css让一个容器元素水平垂直居中有多种解决方法，在这里呢，我给大家介绍几种。

**方法一：position+margin**

```
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <style type="text/css">
        .father{
            width: 200px;
            height: 200px;
            background-color: red;
            position: relative;
        }
        .child{
            position: absolute;
            width: 100px;
            height: 100px;
            background-color: green;
            margin: auto;
            left:0;
            right: 0;
            top: 0;
            bottom: 0;
        }
    </style>
</head>
<body>
    <div class="father">
        <div class="child">我是个居中的盒子</div>
    </div>
</body>
</html>

```

**方法二：display:table-cell**

```
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <style type="text/css">
        .father{
            width: 200px;
            height: 200px;
            background-color: red;
            display: table-cell;
            vertical-align: middle;
            text-align: center;
        }
        .child{
            width: 100px;
            height: 100px;
            background-color: green;
            display: inline-block;
            vertical-align: middle;
        }
    </style>
</head>
<body>
    <div class="father">
        <div class="child">我是个居中的盒子</div>
    </div>
</body>
</html>

```

**第三种：纯position**

```
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <style type="text/css">
        .father{
            width: 200px;
            height: 200px;
            background-color: red;
            position: relative;
        }
        .child{
            width: 100px;
            height: 100px;
            background-color: green;
            position: absolute;
            left: 50%;
            top: 50%;
            margin-left: -50px;
        }
    </style>
</head>
<body>
    <div class="father">
        <div class="child">我是个居中的盒子</div>
    </div>
</body>
</html>
```

### 11.3 javascript 核心编程

#### 11.3.1 前言

1. Javascript是一种运行在浏览器中的解释型的编程语言。

   ```css
   1. 在解释型语言中，代码自上而下运行，且实时返回运行结果。代码在执行前，无需由浏览器将其转化为其他形式。
   2. 与此同时，编译型语言代码能够运行之前需要先转化（编译）成另一种形式。比如 C/C++ 先被编译成汇编语言，然后才能由计算机运行。
   ```

2. 学习js的目的

   ```cs
   1. 因为你没有选择。在web世界里，只有Javascript能跨平台、跨浏览器驱动网页，与用户交互。
   2. Flash背后的ActionScript曾经流行过一阵子，不过随着移动应用的兴起，没有人用Flash开发手机App，所以它目前已经边缘化了。
   3. 相反，随着HTML5在PC和移动端越来越流行，JavaScript变得更加重要了。并且，新兴的Node.js把JavaScript引入到了服务器端，JavaScript已经变成了全能型选手。
   4. 它可以在网页上实现复杂的功能，网页展示不再是单纯的简单的静态信息，而是实施的内容更新，交互式的地图，2D/3D的动画，滚动播放的音视频等等。这都是Javascript可以做的
   ```

3. javascript 是 web 技术的第三层,html是第一层   ,css 是第二层

#### 11.3.2 正式学习js

##### 11.3.2.1 引入方式

1. 内部嵌入

   javascript只需要一个script标签来引入--可任意放在html任意位置

   ```js
   <script type="text/javascript">
       </script>
   ```

2. 外部引入

   必须是js文件后缀名

   ```js
   <script type="text/javascript" src="script.js">
       </script>
   ```

   

##### 11.3.2.2 语句规范

1. 每一句都以分号结尾`;`
2. 注释--
   1. 单行注释: `//注释语句`
   2. 多行注释:  `/*注释语句可换行*/`
3. 变量声明使用var

##### 11.3.2.3 js的数据类型

1. 变量名的规范

   ```js
   1. 变量必须使用字母、下划线(_)或者美元符($)开始。
   2. 可以使用任意多个英文字母、数字、下划线()或者美元符($)组成。
   3. 不能使用Javascript关键字和Javascript保留字来进行命名
   4. 变量名严格区别大小写，如A何a是两个不同的变量
   ```

2. var 更新变量

3. 数据类型

   1. Number--js不区分整形和浮点型,同一用Number表示
   2. String--字符串
   3. boolean--布尔值true  or  false
   4. null  --空
   5. unidified   --未定义

4. 引用数据类型

   1. 数组--就是列表[]

      ```js
      var arr = [];
      ```

   2. object

   3. function

##### 11.2.3.4 字符串的常用方法

1. 定义

   ```js
   var name = "小明去上学";//可以使用单双引号
   //若字符串内包含' 和'' 则使用转义符`/`来解决
   ```

2. 拼接字符串

   ```js
   var name = '' + 456 //js会吧456转为string类型
   var name2 = '79' + 789//一样会拼接
   ```

3. cs6的模板字符串字符串的格式化--${变量}

   ```js
   var str2 = `${name}今年是${age}岁了，快要结婚了，娶了个黑姑娘`;
   console.log(str2);
   
   1. 使用的是```
   2. ${name}  来格式化
   ```

4. slice 方法**和**substr**和**substring**

   ```js
   //slice()、substr()和 substring()也不会修改字符串本身的值——它们只是 返回一个基本类型的字符串值，对原始字符串没有任何影响
   第一个参数指定字符串的开始位置
   
   slice()和 substring()的第二个参数指定的是字符串最后一个字符后面的位置。
   substr()的第二个参数指定的则是返回的字符个数。如果没有给这些方法传递第二个参数，则将字符串的长度作为结束位置。
   
   ar stringValue = "hello world";
   alert(stringValue.slice(3));//"lo world"
   alert(stringValue.substring(3));//"lo world"
   alert(stringValue.substr(3));//"lo world"
   alert(stringValue.slice(3, 7));//"lo w"
   alert(stringValue.substring(3,7));//"lo w"
   alert(stringValue.substr(3, 7));//"lo worl"
   
   
   这个例子比较了以相同方式调用 slice()、substr()和 substring()得到的结果，而且多数情 况下的结果是相同的。在只指定一个参数3的情况下，这三个方法都返回"lo world"，因为"hello"中的第二个"l"处于位置 3。而在指定两个参数 3 和 7 的情况下，slice()和 substring()返回"lo w" (“world”中的”o”处于位置 7，因此结果中不包含”o”)，但 substr()返回"lo worl"，因为它的第二 个参数指定的是要返回的字符个数。
   
   在传递给这些方法的参数是负值的情况下，它们的行为就不尽相同了。其中，slice()方法会将传 入的负值与字符串的长度相加，substr()方法将负的第一个参数加上字符串的长度，而将负的第二个 参数转换为 0。最后，substring()方法会把所有负值参数都转换为 0。下面来看例子
   
   var stringValue = "hello world";
   alert(stringValue.slice(-3));//"rld" 
   alert(stringValue.substring(-3));//"hello world"
   alert(stringValue.substr(-3)); //"rld"
   alert(stringValue.slice(3, -4));//"lo w" 
   alert(stringValue.substring(3, -4));//"hel"
   alert(stringValue.substr(3, -4)); //""(空字符串)
   这个例子清晰地展示了上述三个方法之间的不同行为。在给 slice()和 substr()传递一个负值 参数时，它们的行为相同。这是因为-3 会被转换为 8(字符串长度加参数 11+(3)=8)，实际上相当 于调用了 slice(8)和 substr(8)。但 substring()方法则返回了全部字符串，因为它将-3 转换 成了 0。
   
   当第二个参数是负值时，这三个方法的行为各不相同。slice()方法会把第二个参数转换为 7，这 就相当于调用了 slice(3,7)，因此返回”lo w”。substring()方法会把第二个参数转换为 0，使调 用变成了 substring(3,0)，而由于这个方法会将较小的数作为开始位置，将较大的数作为结束位置， 因此最终相当于调用了 substring(0,3)。substr()也会将第二个参数转换为 0，这也就意味着返回 包含零个字符的字符串，也就是一个空字符串。
   ```

5. charAt()获取字符串 和 charCodeAt()获取字符串的ascii码--参数为基于0 的索引

6. `toLowerCase()`和 `toUpperCase()`大小写转换,原字符串不影响

7. 重点**trim()**删除字符串的前后空格.,原字符串不影响

##### 11.2.3.5 字符串和 数组的公共的方法

1. 使用`typeof`来检测变量的类

2. 都有   `.length`属性,字符串中包含多个字符

3. concat() `用于将一或多个字符串拼接起来， 7 返回拼接得到的新字符串。先来看一个例子。

   ```js
   var stringValue = "hello ";
   var result = stringValue.concat("world", "!");
   alert(result); //"hello world!" 
   alert(stringValue); //"hello"
   ```

4. 获取索引`indexOf()`和 `lastIndexOf()`

##### 11.2.3.6  数组及常用方法

1. 定义(列表)

   ```js
   var arr = [1,2,3,"封起来",[1,2,3,4],true];
   //注意 -- 解释器 遇到var声明的变量 会把var声明的变量提升到全局作用域下
   // js只有2中作用域---全局  和   函数
   //在数组中可以包含任何类型的元素—字符串、数字、对象(后面会讲到)、另一个变量，甚至是另一个数组。
   ```

1. 常规方法(独有)

   ```js
   //1,判断当前数组是否为数组，返回值是true,则证明是数组
   
   var num = 123;
   var arr = ['red','green','yellow'];
   console.log(Array.isArray(arr));//判断是否为数组
   console.log(arr.toString());//red,green,yellow,转为字符串,用`,`连接
   //返回由数组中每个值的字符串形式拼接而成的一个以逗号分隔的字符串
   
   console.log(num.toString());//123 没有变化
   console.log(typeof num.toString());//string
   console.log(arr.join('^'));//连接符是^,red^green^yellow
   
   
   //栈方法(后进先出)
   console.log(arr.push('purple')); //返回了数组的最新的长度,压入一个元素
   console.log(arr);//打印
   console.log(arr.pop());//返回删除的内容
   console.log(arr);
   
   
   
   //往数组的第一项上添加内容
   console.log(arr.unshift('gray','black'));
   console.log(arr);
   console.log(arr.shift());//删除并拿到第一个元素,缩影为0
   console.log(arr);
   
   var names = ['女神','wusir','太白'];
   // names.splice() //对数组进行添加，删除，替换操作
   //name.slice(1) //对数组进行分割
   
   reverse方法翻转数组
   sort()即最小的值位于最前面，最大的值排在最后面。 
   //sort()方法会调用每个数组项的toString()转型方法，然后比较得到的字符串，以确定如何排序 。即使数组中的每一项都是数值，sort()方法比较的也是字符串
   
   1. 通过索引来查/改
   2. shopping.length;//获取数组的长度,字符串也可以使用
   ```

2. concat() --数组合并方法，一个数组调用concat()方法去合并另一个数组，返回一个新的数组。concat()接收的参数是可以是任意的。

   ```js
   参数为一个或多个数组，则该方法会将这些数组中每一项都添加到结果数组中。
   参数不是数组，这些值就会被简单地添加到结果数组的末尾
   ```

3. slice--切片--字符串也能用

   能够基于当前数组中一个或多个项创建一个新数组。`slice()`方法可以接受一或两个参数，既要返回项的起始和结束位置。

   ```js
   1. 一个参数的情况下，slice()方法会返回从该参数指定位置开始到当前数组默认的所有项
   2. 两个参数的情况下，该方法返回起始和结束位置之间的项——但不包括结束位置的项。
   3. 注意： slice()方法不会影响原始数组
   ```

4. indexOf()`和 `lastIndexOf()`。这两个方法都接收两个参数:要查找的项和(可选的)表示查找起点位置的索引。其中，**indexOf()方法从数组的开头(位置 0)开始向后查找，lastIndexOf()方法则从数组的末尾开始向前查找**。

5. **splice**重点(数组独有)

   的主要用途是向数组的中路插入

   ```js
   1. 删除：可以删除任意数量的项，只需指定2个参数：要删除的第一项的位置和要删除的个数。例如splice(0,2)会删除数组中的前两项
   2. 插入：可以向指定位置插入任意数量的项，只需提供3个参数：起始位置、0（要删除的个数）和要插入的项。如果要插入多个项，可以再传入第四、第五、以至任意多个项。例如，splice(2,0,'red','green')会从当前数组的位置2开始插入字符串'red'和'green'。
   3. 替换：可以向指定位置插入任意数量的项，且同时删除任意数量的项，只需指定 3 个参数:起始位置、要删除的项数和要插入的任意数量的项。插入的项数不必与删除的项数相等。例如，splice (2,1,"red","green")会删除当前数组位置 2 的项，然后再从位置 2 开始插入字符串"red"和"green"。
   ```

   ```js
   splice()方法始终都会返回一个数组，该数组中包含从原始数组中删除的项(如果没有删除任何 项，则返回一个空数组)。
   
   var colors = ["red", "green", "blue"];
   var removed = colors.splice(0,1); 
   alert(colors); // green,blue 
   alert(removed); // red，返回的数组中只包含一项
   removed = colors.splice(1, 0, "yellow", "orange"); 
   alert(colors); // green,yellow,orange,blue alert(removed); // 返回的是一个空数组
   removed = colors.splice(1, 1, "red", "purple"); 
   alert(colors); // green,red,purple,orange,blue alert(removed); // yellow，返回的数组中只包含一项
   ```

6. 迭代方法(filter  map   forEach)

   ```js
   1.filter()方法
   
   filter()函数，它利用指定的函数确定是否在返回的数组中包含某一项。例如要返回一个所有数值都大于2的数组，可以使用如下代码。
   
   var numbers = [1,2,3,4,5,4,3,2,1];
   var filterResult = numbers.filter(function(item, index, array){
       return (item > 2);
   });
   alert(filterResult); //[3,4,5,4,3]
   
   
   
   //---------------------------------------------------------------
   2. map()方法
   
   map()方法也返回一个数组，而这个数组的每一项都是在原始数组中的对应项上运行输入函数的结果。例如，可以给数组中的每一项乘以2，然后返回这些乘积组成的数组，如下所示
   
   var numbers = [1,2,3,4,5,4,3,2,1];
   var filterResult = numbers.map(function(item, index, array){
       return item * 2;
   });
   alert(filterResult); //[2,4,6,8,10,8,6,4,2]
   
   //------------------------------------------------------------
   3. forEach()方法--只能在数组对象使用(函数的arguements是伪数组,只能for循环)
   
   它只是对数组中的每一项运行传入的函数。这个方法没有返回值， 本质上与使用 for 循环迭代数组一样。来看一个例子 。
   
   //执行某些操作 10
   var numbers = [1,2,3,4,5,4,3,2,1];
   numbers.forEach(function(item, index, array){
   });
   ```

##### 11.2.3.7 数据类型转换

1. string<-->number

   ```js
   1. 字符串转数字
   var str = '123.0000111';
   console.log(parseInt(str));
   console.log(typeof parseInt(str));
   console.log(parseFloat(str));
   console.log(typeof parseFloat(str));
   console.log(Number(str));
   
   //parseint
   //parsefloat
   //-------------------------------------------------------------
   parseFloat 将它的字符串参数解析成为浮点数并返回。如果在解析过程中遇到了正负号（+ 或 -）、数字 (0-9)、小数点，或者科学记数法中的指数（e 或 E）以外的字符，则它会忽略该字符以及之后的所有字符，返回当前已经解析到的浮点数。同时参数字符串首位的空白符会被忽略。
   
   如果参数字符串的第一个字符不能被解析成为数字，则 parseFloat 返回 NaN。
   
   2. 数字转字符串
   var num  = 1233.006;
   // 强制类型转换
   console.log(String(num));
   console.log(num.toString());
   数字+""转化
   // 隐式转换
   console.log(''.concat(num));
   // toFixed()方法会按照指定的小数位返回数值的字符串 四舍五入
   console.log(num.toFixed(2));
   
   3. 特殊的
   isNaN  (2.555sad)
   infinity 无限大的(6/0) 
   ```

   

#### 11.3.3 条件判断和循环

1. console.log()   --控制台打印

2. alert() --打开页面是弹窗

3. prpomt

4. ```css
   // console.log('hello world');
   // alert('hello world');
   // console.log(window);
   // var name = prompt('请输入今天的天气？');
   // console.log(name);
   ```

##### 11.3.4.1  循环

```js
var arr = [8,9,0];
//1.初始化循环变量  2.循环条件  3.更新循环变量
for(i = 0;i < arr.length;i++){
    console.log(arr[i]);
}

//--------我是华丽的分割线-----------------

//while 语句
var a = 1;//初始化条件
while(a <= 100){
    console.log(a);
    a+=1;//递增条件
}


// for(var i = 0; i < names.length; i++){
//     names[i]
// }
names.forEach(function (index,item) {
    console.log(index);
    console.log(item);
});

//使用break 和 continue来退出循环
```

##### 11.3.4.2 swith(){case:)

```js
 var weather = prompt('请输入今天的天气');
    switch (weather) {
        case '晴天':
            console.log('可以去打篮球');
            break;
        case '下雨':
            console.log('可以睡觉');
            break;
        default:
            console.log('学习');
            break;
    }
```

##### 11.3.4.3  if 语句判断

1. if (){}else{}语句(可以嵌套使用)

   ```js
   if (条件)  {
       
   } else {
       
   }
   1. if 后跟括号,里面是条件
   2. 一组大括号执行代码
   
   
   //嵌套
   var weather = 'sunny';
   if(weather == 'sunny'){
       if(temperature > 30){
           //还是在家里吹空调吧
       }else if(temperature<=30){
           //天气非常棒，可以出去玩耍了
       }
   }
   
   ```

2. else  if 语句

   ```js
   var weather = 'sunny';
   if(weather == 'sunny'){
       //天气非常棒，可以出去玩耍了
   }else if(weather == 'rainy'){
       //天气下雨了，只能在家里呆着
   }else if(weather == 'snowing'){
       //天气下雪了，可以出去滑雪了
   }
   
   //多个选择值是使用
   ```

3. 逻辑运算符

   ```js
   &&--and
   ||--or
   ! --not
   ```

##### 11.3.4.4 三元运算符

```js
var isResult  =  1 > 2 ? '真的' : '假的' ;
```

##### 11.3.4.5 赋值运算符 和 逻辑运算符

1. 与或非

   ```js
   &&  ||   !
   ```

2. 赋值和运算

   ```js
   = 给变量赋值
   == 比较2变量的值  如 2 == "2" //true
   === 严格比较2 变量的值和数据类型  如 2 === "2" //flase
   
   
   //注意-- 
   var a = 4;
   //先让a的值赋值给c 再对a++
   // var c = a ++;
   /* console.log(c);//4
        console.log(a);//5*/
   var c = ++a;
   console.log(c);//5
   console.log(a);//5
   ```

   

#### 11.3.5函数 和 对象

##### 11.3.5.1 函数

1. 函数格式

   ```js
   //解决冗余代码,为了封装,提高代码重用性.
   
   //1.普通函数 
   function  函数名(形参) {
       函数体   
   	}
   
   //2.函数表达式
   var sum = function () {
   	函数体
   }
   
   //3. 自执行函数
   ;(function (形参) {
       函数体(this一定是指向指向window)
   })(实参)
   
   
   //全局作用域,函数作用域,自治性函数this都指向window ,函数作用域中this指向可以发生改变，可以使用call()或者apply()
   
   var obj = {name : 'mjj'}
   function fn() {
       console.log(this.name)
   }
   fn.();//空值,函数this只想window
   fn.call(obj);//此时函数内部this指向了obj
   
   
   //javascript的作用域只有全局作用域和函数作用域
   
   //函数实例
   //例如
   function fn() {
           switch (arguments.length) {
               case 2:
                   console.log('2个参数')
                   break;
               case 3:
                   console.log('3个参数')
                   break;
               default:
                   break;
           }
   }
   
   //函数的参数
   function  fn(a,b) {
       //arguments.length 代指的实参的个数
       //arguments它不是一个数组，它被称为叫伪数组
       console.log(arguments);
       for(var i = 0; i < arguments.length; i++){
           console.log(arguments[i]);
       }
   }
   fn(2,3,4);//可以根据传入的参数的个数选择不同的方法
   console.log(fn.length);//形参的个数
   ```

2. 构造函数

   ```js
   //构造函数
   new Object();
   new Array();
   new String();
   new Number();
   
   //使用构造函数来创建对象
   function Point(x, y) {
     this.x = x;
     this.y = y;
   }
   
   Point.prototype.toString = function () {
     return '(' + this.x + ', ' + this.y + ')';
   };
   
   var p = new Point(1, 2);
   
   //es6用class来创建对象
   class Person{
       constructor(x,y){
           this.x = x;
           this.y = y
       }
       toString(){
           
       }
       
   }
   var p = new Person();
   
   ```

   

##### 11.3.5.2 对象

1. 一切皆对象,函数也是一个对象

2. 对象obj的创建方式

   ```js
   //1. 字面量创建方式
   var obj = {};
   obj.name = 'mjj';
   obj.fav = function(){
       //obj
       console.log(this); //这里的this 一般是obj自身
   }
   obj.fav(); //执行fav方法
   
   // ----------我是分割线-------
   
   var person = {
       name : 'jack';
       age : 28,
       fav : function(){
           console.log('泡妹子');
       }
   }//属性名可以使用字符串即'name','age',"fav"都可以
   
   
   //点语法  set   和   get
   console.log(obj.name);
   obj.name和obj['name']都可以
   
   
   //2. 构造函数创建--使用new语句
   var obj2 = new Object();//创建一个对象
   和 var obj2 = {}相同
   
   
   //esm 6.0用类来创建对象
   class Person{
       constructor(name,age){
           //初始化方法,类似__init__
           this.name = name;
           this.age = age;
       }
       fav(){
           console.log(this.name);
       }
       showName(){
   
       }
   }
   var p = new Person('mjj',18);
   p.fav();
   ```

3. 遍历对象

   ```js
   for (var key in obj ) {
       obj[key]
   }
   ```

   

##### 11.3.5.3  常用的对象

###### 1. date日期对象

```js
var date = new Date();
console.log(date);
console.log(date.getDate());// 月份的第几天(1-31)
console.log(date.getMonth()+1);//获取月份,特别(0-11),月份需要加1
console.log(date.getFullYear());//获取年
console.log(date.getDay());//
console.log(date.getHours());//获取小时0-23
console.log(date.getMinutes());//获取分组0-59
console.log(date.getSeconds());//获取秒0-59

console.log(date.toLocaleString());//2019/6/3 下午10:00:51
/*var weeks = ['星期天','星期一','星期二','星期三','星期四','星期五','星期六'];
console.log(weeks[date.getDay()]);
var day = weeks[date.getDay()];
document.write(`<a href="#">${day}</a>`);*/

var myDate = new Date();
myDate.toDateString();//"Mon Apr 15 2019"
myDate.toTimeString();//"10:11:53 GMT+0800 (中国标准时间)"
myDate.toLocaleDateString();//"2019/4/15"
myDate.toLocaleTimeString();//"上午10:11:53"
myDate.toUTCString();//"Mon, 15 Apr 2019 02:11:53 GMT"

//我是华丽的分割线--------------------------------------

//数字时钟重要

var timeObj = document.getElementById('time');
    console.log(time);

function getNowTime() {
    var time = new Date();
    var hour = time.getHours();
    var minute = time.getMinutes();
    var second = time.getSeconds();
    var temp = "" + ((hour > 12) ? hour - 12 : hour);
    if (hour == 0) {
        temp = "12";
    }
    temp += ((minute < 10) ? ":0" : ":") + minute;
    temp += ((second < 10) ? ":0" : ":") + second;
    temp += (hour >= 12) ? " P.M." : " A.M.";
    timeObj.innerText = temp;
}
setInterval(getNowTime, 20)
```

![1559580769218](D:\md_down_png\1559580769218.png)

###### 2.window 对象--全局对象

```js
var name = 'mjj';
console.log(window);
console.log(window.name);
//默认是在全局的
```

###### 3. math对象 --数学运算

```js
var obj = {name:'mjj'};
    function add() {
        console.log(this.name);
    }
    // add();
    // add.call(obj);
    // add.apply(obj,[]);

    //求最大值和最小值
    var values = [1,2,36,23,43,3,41];
    var max = Math.max.apply(null,values);
    console.log(max);
	

	//四舍五入
    var a = 1.49999999;
    console.log(Math.ceil(a));//天花板函数向上舍入，即它总是将数值向上舍入为最接近的整数;
    console.log(Math.floor(a))//地板函数执行向下舍入，即它总是将数值向下舍入为最接近的整数;
    console.log(Math.round(a))//四舍五入执行标准舍入，即它总是将数值四舍五入为最接近的整数(这也是我们在数学课上学到的舍入规则)。

//    随机数
    console.log(Math.random());

//    min~  max
    //0.113131313
    function random(min,max) {
       return min+Math.floor(Math.random()*(max-min))
    }
    console.log(random(100, 400));

//获取随机验证码
function createCode(){
    //首先默认code为空字符串
    var code = '';
    //设置长度，这里看需求，我这里设置了4
    var codeLength = 4;
    //设置随机字符
    var random = new Array(0,1,2,3,4,5,6,7,8,9,'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R', 'S','T','U','V','W','X','Y','Z');
    //循环codeLength 我设置的4就是循环4次
    for(var i = 0; i < codeLength; i++){
        //设置随机数范围,这设置为0 ~ 36
        var index = Math.floor(Math.random()*36);
        //字符串拼接 将每次随机的字符 进行拼接
        code += random[index]; 
    }
    //将拼接好的字符串赋值给展示的Value
    return code
}
```

![1559581153668](D:\md_down_png\1559581153668.png)

#### 11.3.6 BOM简介

##### 11.3.6.1 简介

1. BOM --Browser Object Model--浏览器对象模型

   在 Web 中使用 JavaScript，那么 BOM(浏览器对象模型)则无疑才是真正的核心。BOM 提供了很多对象，用于访问浏览器的功能，这些功能与任意网页内容无关。多年来，缺少事实上的规范导致 BOM 既有意思又有问题，因为浏览器提供商会按照各自的想法随意去扩展它。

2. window 对象

   BOM 的核心对象是 window，它表示浏览器的一个实例。在浏览器中，window 对象有双重角色， 它既是通过 JavaScript 访问浏览器窗口的一个接口，又是 ECMAScript 规定的 Global 对象。这意味着 在网页中定义的任何一个对象、变量和函数，都以 window 作为其 Global 对象，因此有权访问 `parseInt()`等方法。

   ```js
   
   ```

   

##### 11.3.6.2 系统对话框方法

1. `alert()` 警告框

2. `confirm()`确认框,需要一个值来接受,true or  false

3. `prompt()` 弹出框,和用户交互使用,需要一个值来接受用户输入的内容

   ```js
   <script type="text/javascript">
       window.alert('mjj');
   
       var a = window.confirm('你确定要删除?');
   	console.log(a);
   
       var weather = window.prompt('今天天气如何??');
   	console.log(weather); 
   </script>
   
   //prompt可以接受2个参数,第二个参数为弹出框默认的文本
   ```

##### 11.3.6.3 定时器方法

在ECMAScript语法中为window对象提供了两个非常有用的定时任务的方法

1. **setTimeout()** >>>>一次性任务

   setTimeout()方法表示一次性定时任务做某件事情,它接收两个参数,第一个参数为执行的函数,第二个参数为时间(毫秒计时:1000毫秒==1秒)

   ```js
   var timer = setTimeout(callback,2000);//第一个参数为函数名.不带括号,是一个回调函数,是异步执行的.
   clearTimeout(timer);//清除
   ```

2. **setInterval()**

   `setInterval()`方法表示周期性循环的定时任务.它接收的参数跟`setTimeout()`方法一样.

   ```js
   var num = 0;
   var timer = null;
   timer = setInterval(function(){
       num++;
       if (num > 5) {
           clearInterval(timer);
           return;
       }
       console.log('num:'+ num);
   },1000);
   
   //clearTimeout(timer);//清除当前的定时任务
   
   //实例
   window.setTimeout(function(){
       console.log('111');
   },0);//注意>>这里是0,也是2222先打印出来.
   console.log('2222');
   
   /*因为第二个参数是一个表示等待多长时间的毫秒数，但经过该时间后指定的代码不一定会执行。 JavaScript 是一个单线程序的解释器，因此一定时间内只能执行一段代码。为了控制要执行的代码，就 有一个 JavaScript 任务队列。这些任务会按照将它们添加到队列的顺序执行。setTimeout()的第二个 参数告诉 JavaScript 再过多长时间把当前任务添加到队列中。如果队列是空的，那么添加的代码会立即 执行;如果队列不是空的，那么它就要等前面的代码执行完了以后再执行。*/
   
   // 2.周期性定时器
   var num = 0;
   var timer = null;
   // 开启定时器
   timer = window.setInterval(function(){
       num++;
       if(num === 10){
           // 清除定时器
           clearInterval(timer);
       }
       console.log(num);
   },1000);
   ```

##### 11.3.6.4 location 对象

1. location是最有用的BOM对象之一,它提供了与当前窗口中加载的文档有关的信息，location 对象是很特别的一个对象，因为它既是 window 对象的属性，也是document 对象的属性;

2. window.location 和document.location 引用的是同一个对象。 location 对象的用处不只表现在它保存着当前文档的信息，还表现在它将 URL 解析为独立的片段，让 开发人员可以通过不同的属性访问这些片段。

   ![1559644040102](D:\md_down_png\1559644040102.png)

3. 一些方法

   ```js
   //1. reload()方法
   location.reload();//重新加载(有可能从缓存中加载,如果缓存有的话)
   location.reload(true);//重新加载(强制从服务器加载)
   
   //2.位置操作
   //2秒后自动跳转到百度页面
   setTimeout(function(){
       location.href = 'https://www.baidu.com.com';
   },2000)
   
   //2秒后自动跳转到百度页面,不会有记录(不能后退)
   setTimeout(function(){
       location.replace('https://www.apeland.cn/web');
   },2000)
   ```

### 11.3.7 DOM简介

1. DOM>**Document Object Model**>>文档对象模型

   DOM代表着被加载到浏览器窗口里的当前网页：浏览器由我们提供了当前的地图（或者说模型），而我们可以通过JavaScript去读取这张地图。

2. 节点

   1. 元素节点element mode

      `<body>`、`<p>`、`<ul>`之类的元素这些我们都称为叫元素节点（element node）

   2. 文本节点

      元素只是不同节点类型中的一种。如果一份文档完全由一些空白元素构成，它将有一个结构，但这份文档本身将不会包含什么内容。在网上，内容决定着一切，**没有内容的文档是没有任何价值的，而绝大数内容都是有文本提供**。

   3. 属性节点attribute mode

      还存在着其他的一些节点类型。例如，注释就是另外一种节点类型。但这里我们介绍的是属性节点。

       元素都或多或少的有一些属性，属性的作用是对元素做出更具体的描述。例如，几乎所有的元素都有一个title属性，而我们可以利用这个尚需经对包含在元素里的东西做出准确的描述：

      ```js
      <p title='请您选择购买的课程'>本课程是web全栈课程，期待你的购买！</p>
      ```

      **注意:** 属性节点包含在元素节点当中(文本节点也是如此). 并非所属地额元素都包含着属性，但所有的属性都会被包含在元素里。

##### 11.3.7.1 获取节点的方法

1. 要相对网页动态操作,要先获取元素节点

2. 获取元素节点的方式

   1. `gerElementById()`方法

      ```js
      //是与document对象相关联的函数。`getElemntById()`
      var 变量名 = document.getElementById(id);//用个名字来接受该元素节点,返回一个对象
      ```

   2. `getElementsByTagName() `方法

      ```js
      //通过标签名字获取html中所有的该标签对象,返回一个元素对象集合
      var listItems = document.getElementsByTagName('li')
      
      //可以使用.length来获取长度
      //用for循环来遍历()
      ```

   3. `getElementsClassName()`方法

      ```js
      //通过类名来获取一类对象
      var classItems = document.getElementsByClassName('item');
      ```

   4. `.children`属性获取元素节点的子代

##### 11.3.7.2  对元素节点操作(属性节点的操作)

1. `getAttribute()`方法

   ```js
   //getAttribute()方法只接收一个参数——你打算查询的属性的名字。
   var oP = document.getElementsByTagName('p')[0];
   var title = oP.getAttribute('title');
   console.log(title);
   //获取p标签的第一个对象,打印他的title属性值,查不到则是null
   ```

2. `setAttribute()`方法

   ```js
   //。此方法传递两个参数。第一个参数为属性名，第二个参数为属性值
   
   var classList = document.getElementById('classList');
   classList.setAttribute('title','这是我们最新的课程');
   
   /*通过setAttribute()方法对文档做出的修改，如果我们查看源代码发现看到的依旧是原来的属性值——也就是说，setAttribute()方法做出的修改不会反映为文档本身的源码里。这种“表里不一”的现象源自于DOM的工作模式：先加载文档的静态内容、再以动态方式对他们进行刷新，动态刷新不影响文档的静态内容。这正是DOM的真正威力和诱人之处：对页面内容的刷新不需要最终用户在他们的浏览器里执行页面刷新操作就可以实现。*/
   
   
   // 如果是自定义的属性,要在文档上能看到,通过setAttribute设置属性
   var p1 = document.getElementById('p1');
   console.log(p1.getAttribute('title'));
   console.log(p1.getAttribute('class'));
   p1.setAttribute('class','abc');
   p1.setAttribute('adadad','1321313');
   ```

##### 11.3.7.3 事件

1. 网页主要的事件有

   ![1559646522533](D:\md_down_png\1559646522533.png)

2. 通过事件对样式进行操作

   ```js
   //1.获取事件源对象
   var box = document.getElementById('box');
   
   //2.绑定事件
   box.onmouseover = function (){
   	// 3.让标签的背景色变绿
   				
   	box.style.backgroundColor = 'green';
   	box.style.fontSize = '30px';				
   }
   box.onmouseout = function (){
   	// 3.让标签的背景色变绿		
   	box.style.backgroundColor = 'red';
   	box.style.fontSize = '16px';
   }
   
   //onfouns光标聚焦和失焦事件--表单控件
   //当文本框或者文本域中的文字被选中时，触发onselect事件，同时带调用的程序就会被执行。
   //通过改变文本框的内容来触发onchange事件，同时执行被调用的程序。
   //onload事件会在页面加载完成后，立即发生，同时执行被调用的程序。注意：加载页面时，触发onlaod事件，事件写在body标签内。
   <!DOCTYPE html>
   <html lang="en">
   <head>
       <meta charset="UTF-8">
       <title>Title</title>
       <script type="text/javascript">
           function message() {
               console.log('加载中，请稍后......');
           }
       </script>
   </head>
   <body onload="message();">
   </body>
   </html>
   //==---------=-=-=-=================================第二种方法
   window.onload = function(){
   ```

##### 11.3.7.4设置样式

1. 动态设置样式,通过`HTMLElement.style`属性来实现

   ```js
    var para = document.getElementById('box');
   para.style.color = 'white';
   para.style.backgroundColor = 'black';
   para.style.padding = '10px';
   para.style.width = '250px';
   para.style.textAlign = 'center';
   //点语法
   ```

2. 操作属性的类来控制样式(事先在head标签内的style中写好类`.类名`)

   然后动态操作,添加属性 `setAttribute("class","类名")`

   或者是`para.className += " 类名"

   ```js
   var para = document.getElementById('box');
   para.setAttribute('class','highlight');
   
   //----------------
   var p1 = document.getElementById('p1');
   p1.onclick = function(){
       this.className = this.className +' active';//这里的this指向对象p1
       this.setAttribute()
   }
   ```

   ##### 11.3.7.5  节点和节点的方法,属性

   ###### 11.3.7.5.1 节点的属性

   1. nodeName>节点的名称,只读
      1. 元素节点的nodeName与标签名相同(p div ul  li等等)
      2. 属性节点的nodeName与属性的名称相同
      3. 文本节点的nodeName永远是#text
      4. 文档节点的nodeName永远是#document
   2. nodevalue>节点的值
      1. 元素节点的 nodeValue 是 undefined 或 null
      2. 文本节点的 nodeValue 是文本自身
   3. 属性节点的 nodeValue 是属性的值

3. nodeType>节点的类型,只读

   ![1559648218385](D:\md_down_png\1559648218385.png)

4. 其他常用的属性

   1. `clientWidth和clientHeigh 、 clientTop和clientLeft`

      1. clientWidth的实际宽度

         clientWidth = width+左右padding

      2. clientHeigh的实际高度

         clientHeigh = height + 上下padding 

      3. clientTop的实际宽度

         clientTop = boder.top(上边框的宽度)

      4. clientLeft的实际宽度

         clientLeft = boder.left(左边框的宽度)

      5. offsetWidth和offsetHight 、 offsetTop和offsetLeft

         1. offsetWidth的实际宽度

      6. offsetWidth = width + 左右padding + 左右boder

         3. offsetHeith的实际高度
         4. offsetHeith = height + 上下padding + 上下boder
         5. offsetTop实际宽度
         6. offsetTop：当前元素 上边框 外边缘 到 最近的已定位父级（offsetParent） 上边框 内边缘的 距离。如果父级都没有定位，则分别是到body 顶部 和左边的距离

      7. offsetLeft实际宽度

5. offsetLeft：当前元素 左边框 外边缘 到 最近的已定位父级（offsetParent） 左边框 内边缘的            距离。如果父级都没有定位，则分别是到body 顶部 和左边的距离
         

   3. scrollWidth和scrollHeight 、 scrollTop和scrollLeft
      1. scrollWidth实际宽度
      2. scrollWidth：获取指定标签内容层的真实宽度（可视区域宽度+被隐藏区域宽度）。
      3. scrollHeight的实际高度
      4. scrollHeight：获取指定标签内容层的真实高度（可视区域高度+被隐藏区域高度）
      5. scrollTop
      6. scrollTop :内容层顶部 到 可视区域顶部的距离。 
      7. scrollLeft
      8. scrollLeft:内容层左端 到 可视区域左端的距离.

   ###### 11.3.7.5.2 节点的方法

   1. 创建节点`createElement()`

   ```js
      var newNode = document.createElement(tagName); //返回一个element对象(ul/div/p/ul/li等)
   
      //假设设置一个p标签节点
   //1.设置文本
      p.innerText = "小敏";//但这是不能插入其他标签的
      pinnerHTML = '<p>mjj</p>';//既可以设置文本又可以设置标签
      
      注意：如果想获取节点对象的文本内容，可以直接调用innerText或者innerHTML属性来获取
   ```

   2. 插入节点appendChild()

      ```js
      //在指定的节点的最后一个子节点之后添加一个新的子节点
      //如ul的最后一个li插入一个li
      ul.appendChild(li);
      ```

   ```
   
   ```

6. 插入节点`insertBefore()`

   ```js
     //insertBefore()方法可在已有的子节点前插入一个新的子节点
     //2个参数   insertBefore(newNode,node);要插入的新节点 / 指定此节点前插入节点
   ```

   4. 删除节点 `removeChild`

      ```js
      //removeChild()方法从子节点列表中删除某个节点。如果删除成功，此方法可返回被删除的节点，如失败，则返回NULL
      nodeObject.removeChild(node);
      
      //---------------------------
      var oBox = document.getElementById('box');
      var x = oBox.removeChild(oBox.childNodes[0]);
      //可以使用children[0]来删除
      //所有子节点的数组,注意:所有子节点的数组,包含文本节点
      
      
      console.log(x);
      
      注意：把删除的子节点赋值给 x，这个子节点不在DOM树中，但是还存在内存中，可通过 x =null来删除对象。
      ```

   5. 替换元素节点`replaceChild()`

      replaceChild实现子节点(对象)的替换。返回被替换对象的引用

      ```js
      node.replaceChild(newnode,oldnew);
      //newnode：必需，用于替换的oldnew的对象
      //oldnew：必需，被newnode替换的对象
      
      var oldnew = document.getElementById('text');
      var newNode  = document.createElement('p');
      newNode.innerHTML = 'Vue';
      oldnew.parentNode.replaceChild(newNode,oldnew);//parentNode父节点,只有一个
      ```

   6. 创建文本节点`createTextNode`(没必要)

      createTextNode()方法创建新的文本节点，返回新创建的Text节点

      ```js
      document.createTextNode(data);
      //data: 字符串值，可规定此节点的文本
      
      var newNode = document.createElement('div');
      var textNode  = document.createTextNode('我是一条文本信息');
      newNode.appendChild(textNode);
      document.body.appendChild(newNode);
      ```

##### 11.3.7.5 重点 DOM语语句

1. 函数思想根据id名返回该对象

   ```js
   function $(id){
       return {typeof id === 'string'} ? {document.getElementById(id)} : {null;}
   }
   //3元运算符,3个括号分别包裹条件,真值 和 假值
   //同样的
   function C$(classcame){
       return typeof classname === 'string' ? document.getElementsByClassName(classname) : null;
   }
   
   //
   function $T(tagname){
       return typeof tagname === 'string' ? document.getElementsByTagName(tagname) : null;
   }
   ```

   

### 11.3.8 jQuery

1. jquery的使用

   在官网下载jquery.js文件,导入即可使用.主要是方法

   ```js
   //1.导入模块
   <script src="js/jquery.js" type="text/javascript" charset="utf-8"></script>
   //2.可以直接使用模块内的方法
   ```

2. jQuery是一个快速，小巧，功能丰富的JavaScript库。
   它通过易于使用的API在大量浏览器中运行，
    		使得HTML文档遍历和操作，事件处理，动画和Ajax变得更加简单。通过多功能性和可扩展性的结合，jQuery改变了数百万人编写JavaScript的方式。
    		操作: 获取节点元素对象，属性操作，样式操作，类名，节点的创建，删除，添加，替换
    		jquery核心：write less,do more

3. jquery对象转换js对象

   `$('button')[0]`

4. js对象转换jquery对象

   `$(js对象)`

#### 11.3.8.1 选择器

1. 基础选择器

   `$()`>>括号内可以放`#id .类名 标签名`,来得到一个jquery对象

2. 可以使用高级选择器

   属性选择器

   `$('input[type=submit]')`

   后代选择器

   `$('#box .active')`

3. **基本过滤器**

   `$('ul li:eq(1)')`获取li的第一个对象

   `.parent()`获取父节点

   `.children()`获取所有子节点伪数组

   1. `.eq()`选中一个 索引从0开始
   2. `.children()` 获取亲儿子
   3. `.find() `获取的后代
   4. `.parent()` 获取父级对象
   5. `.siblings() `获取除它之外的兄弟元素

#### 11.3.8.2样式的设置操作

1. jquery-obj

   ```js
   obj.css('color','red')
   obj.css({
       'color':'red',
       'position':'relative'
   }
   //获取样式的值
    var t = obj.css('color')
   )
   
   注意:在jquery的事件是dom对象的事件(不加on)
   //click mouseover  mouseout load focus blus等等
   ```

#### 11.3.8.3  jquery的动画效果

1. 普通动画

   ```js
   1. jobj.show(毫秒,可选callback)缓慢由小到大出现
   2. jobj.hide()缓慢由打到小消失
   3. jobj.stop().toggle()开关方法
   ```

2. 卷帘门动画

   ```js
   jobj.slidDown()
   jobj.slidUp()
   ```

3. 淡入淡出动画

   ```js
   jobj.fadeIn()     .fadeOut()淡入淡出效果
   ```

4. 自定义动画`.animate`

   `.animate({params},speed,callback)`

   ```js
   jQuery(function () {
               $("button").click(function () {
                   var json = {"width": 500, "height": 500, "left": 300, "top": 300, "border-radius": 100};
                   var json2 = {
                       "width": 100,
                       "height": 100,
                       "left": 100,
                       "top": 100,
                       "border-radius": 100,
                       "background-color": "red"
                   };
   
                   //自定义动画
                   $("div").animate(json, 1000, function () {
                       $("div").animate(json2, 1000, function () {
                           alert("动画执行完毕！");
                       });
                   });
   
               })
           })
   ```

   

#### 11.3.8.4 类操作

1. `addClass()`
2. `removeClass()`
3. `toggleClass()`

#### 11.3.8.5 属性操作

1. `attr(name , value);`设置属性

2. `removeAttr(name);`删除属性

   ```js
   $('p').attr('title','mjj');
   $('p').attr({'a':1,'b':2});
   
   $('p').removeAttr('title id a');
   ```

#### 11.3.8.6 文档操作

1. 后置追加`append/appendTo`

   ```js
   var oH3 = document.createElement('h3');
   oH3.innerText = '女神';
   // 父.append(子)
   $('#box').append('mjj');
   $('#box').append('<h2>wusir</h2>');//*****
   $('#box').append(oH3);
   // append()如果参数是jq对象,那么相当于移动操作
   $('#box').append($('h4'));
   
   //我是分割线
   // 子.appendTo(父)
   $('<a href="#">百度一下</a>').appendTo('#box').css('color','yellow');
   //好处在于可以链式操作
   ```

2. 前置追加`prepend/prependTo`

3. 外部插入操作`before/insertBefore`

   ```js
   $('h2').before('mjj2');
   				
   ('<h3>女神</h3>').insertBefore('h2');
   ```

4. `remove() and  detach()`

   ```js
   $('#btn').click(function() {
       alert(1);
       // 	// 即移除标签,事件也跟着移除
       // 	/* $(this).remove();链式的,返回值是该对象//****
       console.log($(this).remove());
       $('#box').after($(this).remove());
       
       //.detach() 方法和.remove()一样, 除了 .detach()保存所有jQuery数据和被移走的元素相关联。当需要移走一个元素，不久又将该元素插入DOM时，这种方法很有用。 
   ```

5. `empty()`

   ```js
   //从DOM中移除集合中匹配元素的所有子节点。 
   $('#box').html(' ');
   $('#box').empty();
   ```

#### 11.3.8.7鼠标事件

1. `mouseover  和 mouseout`鼠标移入移出事件

   ```js
   //mouseover 鼠标穿过父级元素和子元素都会调用方法
   
   mouseenter()鼠标进入事件
   mouseleave()
   
   $('#box').mouseenter(function(){
       console.log('进来了');
   
       $('#child').stop().slideDown(1000);
   })
   $('#box').mouseleave(function(){
       console.log('离开了');
   
       $('#child').stop().slideUp(1000);
   })
   
   //``````````````````````````````````````````````````````````
   
   $('#box').hover(function(){
       $('#child').stop().slideDown(1000);
   },function(){
       $('#child').stop().slideUp(1000);
   })
   ```

2. `foucs 和 blusr`

   ```js
   $('input[type=text]').focus();
   //默认加载页面聚焦行为
   ```

3. `keydown 和 keyup 和 .keyCode属性`

   ```js
   $('input[type=text]').keydown(function(e){
   				console.log(e.keyCode);
   				switch (e.keyCode){
   					case 8:
   						$(this).val(' ')
   						break;
   					default:
   						break;
   				}	
   			})
   ```

#### 11.3.8.8 表单提交事件

1. 表单提交转换方向

   ```js
   <body>
       <a href="javascript:;">百度游戏啊</a>
   <form action="">
       <input type="text" placeholder="用户名">
           <input type="password" placeholder="密码">
               <input type="submit">
                   </form>
   <script src="js/jquery.js" type="text/javascript" charset="utf-8"></script>
   <script type="text/javascript">
       $('form').submit(function(e){
       e.preventDefault();
       console.log('11111');
   })
   </script>
   </body>
   ```

#### 11.3.8.9 jQuery插件的使用

```js
www.jq22.com
//1.直接通过下载,使用即可
//2.审查元素,sourse.手动爬取-->需要q币的就这样爬取
```

### 11.3.9 ajax

1. 获取首页数据

   ```js
   $(function(){
       // 获取首页的数据
       $.ajax({
           url:'https://api.apeland.cn/api/banner/',
           methods:'get',
           success:function(res){
               console.log(res);
               if(res.code === 0){
                   var cover = res.data[0].cover;
                   var name = res.data[0].name;
                   console.log(cover);
                   $('#box').append(`<img src=${cover} alt=${name}>`)
               }
           },
           error:function(err){
               console.log(err);
           }
       })
   })
   ```

   

### 11.3.9 bootstrap

1. 别人帮我们已经写好了代码,我们拿过来,稍作修改.布局

2. 第一步:先引入bootstrap,基本模板

   ```js
   //使用以下给出的这份超级简单的 HTML 模版，或者修改这些实例。我们强烈建议你对这些实例按照自己的需求进行修改，而不要简单的复制、粘贴。<!DOCTYPE html>
   <html lang="zh-CN">
     <head>
       <meta charset="utf-8">
       <meta http-equiv="X-UA-Compatible" content="IE=edge">
       <meta name="viewport" content="width=device-width, initial-scale=1">
       <!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
       <title>Bootstrap 101 Template</title>
   
       <!-- Bootstrap -->
       <link href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap.min.css" rel="stylesheet">
   
       <!-- HTML5 shim 和 Respond.js 是为了让 IE8 支持 HTML5 元素和媒体查询（media queries）功能 -->
       <!-- 警告：通过 file:// 协议（就是直接将 html 页面拖拽到浏览器中）访问页面时 Respond.js 不起作用 -->
       <!--[if lt IE 9]>
         <script src="https://cdn.jsdelivr.net/npm/html5shiv@3.7.3/dist/html5shiv.min.js"></script>
         <script src="https://cdn.jsdelivr.net/npm/respond.js@1.4.2/dest/respond.min.js"></script>
       <![endif]-->
     </head>
     <body>
       <h1>你好，世界！</h1>
   
       <!-- jQuery (Bootstrap 的所有 JavaScript 插件都依赖 jQuery，所以必须放在前边) -->
       <script src="https://cdn.jsdelivr.net/npm/jquery@1.12.4/dist/jquery.min.js"></script>
       <!-- 加载 Bootstrap 的所有 JavaScript 插件。你也可以根据需要只加载单个插件。 -->
       <script src="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/js/bootstrap.min.js"></script>
     </body>
   </html>
   ```

##### 11.3.10.1 bootstrap的全局css样式

```js
body设置了全局背景色白色
使用@font-family-base、@font-size-base 和 @line-height-base 变量作为排版的基本参数
为所有链接设置了基本颜色 @link-color ，并且当链接处于 :hover 状态时才添加下划线
```

1. 布局容器container and container-fluid,前者用于固定长度1170px默认,支持响应式的容器

2. **栅格系统(重点)**

   ```js
   .row(一行,必须包裹在congtainer内)
   .col-lg- .col-md- .col-sm- .col-xs
   
   //列偏移:>>>.col-md-offset-* 类可以将列向右侧偏移n个列的宽度
   //`col-md-4 col-md-offset-4`
   
   //row是可以嵌套的
   ```

   ![1560171082442](D:\md_down_png\1560171082442.png)

3. 排版

   1. hx标签内科加入small标签加入副标题

      `<h1>养乐维的漫画上月球<small>是假的</small></h1>`

   2. mark标签,高亮文本

      `<p>你是我的<mark>眼</mark>眼</p>`

      p夹上类`lead`可以让段落突出显示

      ```js
      del删除文本标签
      s无用文本标签
      ins插入文本标签
      u下划线文本
      strong加粗
      em斜体文本
      文本对齐text-left center right justified nowrap
      	<p class="text-left">Left aligned text.</p>
          <p class="text-center">Center aligned text.</p>
          <p class="text-right">Right aligned text.</p>
          <p class="text-justify">Justified text.</p>
          <p class="text-nowrap">No wrap text.</p>
      大小写
      	text-lowercase uppercase capitalize
      
      文本颜色
          text-muted
          text-primary
          text-success
          text-danger
          text-waring
          text-info
      
      背景颜色
      bg-primary
      bg-success
      ....
      
      按钮
      btn btn-default
      btn btn-link
      btn btn-success
      btn btn-primary
      ....
      
      ```

   3. 无样式列表`list-unstyled`,内联列表`list-inline`

   4. 自定义列表dl

      `.dl-horizontal` 可以让 `<dl>` 内的短语及其描述排在一行。开始是像 `<dl>` 的默认样式堆叠在一起，随着导航条逐渐展开而排列在一行。

   5. 表格table

      ```js
      必须加table类,其赋予基本的样式 — 少量的内补（padding）
      .table-striped 类可以给 <tbody> 之内的每一行增加斑马条纹样式。
      添加 .table-bordered 类为表格和其中的每个单元格增加边框。
      .table-hover 类可以让 <tbody> 中的每一行对鼠标悬停状态作出响应。
      
         //只有table内套一个tbody即可
      
      .table-condensed 类可以让表格更加紧凑，单元格中的内补（padding）均会减半。
      
      ```

      ![1560173579820](D:\md_down_png\1560173579820.png)

   6. 表单

      把lable标签和控件放在`form-control`可以获得最好的排列

      `所有设置了 `.form-control` 类的 `<input>`、`<textarea>` 和 `<select>` 元素都将被默认设置宽度属性为 `width: 100%;`

      小技巧:>>

      ```js
      <label for="exampleInputEmail1">Email address</label>
      <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Email">
          
          和
      <label>用户名
      <input type="text" class="form-control" id="username" placeholder="请输入用户名">		</label>
      
      是一样的
      
      ```

      

      ```js
      <form action="">
          <div class="form-group">
              <label for="username">用户名</label>
      		<input type="text" class="form-control" id="username" placeholder="请输入用户名">
          </div>
      	<div class="form-group">
          	<label for="exampleInputPassword1">Password</label>
      		<input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
          </div>
      	<div class="form-group">
          	<label for="exampleInputFile">File input</label>
      		<input type="file" id="exampleInputFile">
          	<p class="help-block">Example block-level help text here.</p>
      	</div>
      	<div class="checkbox">
          	<label for="ddde"></label>
          	<input id='ddde' type="checkbox"> Check me out
              
      	</div>
      <button type="submit" class="btn btn-default">Submit</button>
      <button type="submit" class="btn btn-success">Submit</button>
      <button type="submit" class="btn btn-info">Submit</button>
      <button type="submit" class="btn btn-danger">Submit</button>
      <button type="submit" class="btn btn-warning">Submit</button>
      </form>
      ```

      **内联表单**为 `<form>` 元素添加 `.form-inline` 类可使其内容左对齐并且表现为 `inline-block` 级别的控件。

      **只适用于视口（viewport）至少在 768px 宽度时（视口宽度再小的话就会使表单折叠）。**

      ![1560175751617](D:\md_down_png\1560175751617.png)

      **只适用于视口（viewport）至少在 768px 宽度时（视口宽度再小的话就会使表单折叠）。**

      #### 一定要添加 `label` 标签

      

      如果你没有为每个输入控件设置 `label` 标签，屏幕阅读器将无法正确识别。对于这些内联表单，你可以通过为 `label` 设置 `.sr-only` 类将其隐藏。还有一些辅助技术提供label标签的替代方案，比如 `aria-label`、`aria-labelledby` 或 `title` 属性。如果这些都不存在，屏幕阅读器可能会采取使用 `placeholder` 属性，如果存在的话，使用占位符来替代其他的标记，但要注意，这种方法是不妥当的。

      

      **水平表单**通过为表单添加 `.form-horizontal` 类，并联合使用 Bootstrap 预置的栅格类，可以将 `label` 标签和控件组水平并排布局。这样做将改变 `.form-group` 的行为，使其表现为栅格系统中的行（row），因此就无需再额外添加 `.row` 了。

   7. 表单其他控件

      ```js
      通过将 .checkbox-inline 或 .radio-inline 类应用到一系列的多选框（checkbox）或单选框（radio）控件上，可以使这些控件排列在一行。
      ```

      ![1560175521255](D:\md_down_png\1560175521255.png)

   8. 下拉列表

      select  multiple class='form-control'

      `multiple` 属性的 `<select>` 控件来说，默认显示多选项。

   9. 通过 `.input-lg` 类似的类可以为控件设置高度，通过 `.col-lg-*` 类似的类可以为控件设置宽度。

      ![1560176279418](D:\md_down_png\1560176279418.png)

4. 按钮button

   为 `<a>`、`<button>` 或 `<input>` 元素添加按钮类（button class）即可使用 Bootstrap 提供的样式。

   `btn`

   ```js
   按钮
   btn btn-default
   btn btn-link
   btn btn-success
   btn btn-primary
   ....
   尺寸设置
   btn-ig sm  xs 和默认尺寸
   
   ```

   ![1560176509004](D:\md_down_png\1560176509004.png)

   

5. 图片img

   ```js
   图片设置
   .img-rounded
   .img-circle
   .img-thumbnail
   ```

6. 关闭按钮`close`

   ```js
   <button type="button" class="close" aria-label="Close"><span aria-hidden="true">&times;</span></button>
   ```

7. 三角符号

   ```js
   <span class="caret"></span>
   ```

8. 显示隐藏内容

   ```js
   显示和隐藏内容
   show/hidden
   ```

9. 快速浮动

   ```js
   快速浮动
   .pull-left 左浮动
   .pull-right 右浮动
   清除浮动
   .clearfix
   内容块居中
   .center-block
   ```

##### 11.3.10.2 bootstrap 的组件

组件(html+css+js的整合)化开发

```js
1.字体图标
	注意：不要和其它的组件混合使用，嵌套一个span标签，加上对应字体图标的类名
	
2.导航和导航条
3.下拉菜单
4.按钮式的下拉
```

1. 字体图标

   ```js
   不要和其他图标混用,应该创建一个嵌套的 <span> 标签，并将图标类应用到这个 <span> 标签上。
   图标类只能应用在不包含任何文本内容或子元素的元素上。支队空内容的元素起作用
   
   <span class="glyphicon glyphicon-king" aria-hidden="true"></span>
   ```

2. 下拉菜单

   ```js
   将下拉菜单触发器和下拉菜单都包裹在 .dropdown 里，或者另一个声明了 position: relative; 的元素。然后加入组成菜单的 HTML 代码。
   
   <div class="dropdown">
     <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
       Dropdown
       <span class="caret"></span>
     </button>
     <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
       <li><a href="#">Action</a></li>
       <li><a href="#">Another action</a></li>
       <li><a href="#">Something else here</a></li>
       <li role="separator" class="divider"></li>
       <li><a href="#">Separated link</a></li>
     </ul>
   </div>
   
   
   //把div的class换位dropup,上拉菜单
   <li class="dropdown-header">Dropdown header</li>为下拉菜单添加标题
   <li role="separator" class="divider"></li>为下拉菜单提供分割线
   为li添加disabled,禁用选项
   ```

3. 导航

   ```js
   导航依赖1个基类nav
   
   ```

   1. 标签页`.nav-tabs`

      ![1560184608170](D:\md_down_png\1560184608170.png)

   2. 胶囊式导航`nav-pills`和前者一致

      胶囊是标签页也是可以垂直方向堆叠排列的。只需添加 `.nav-stacked` 类。

      ![1560184687545](D:\md_down_png\1560184687545.png)

   3. 导航添加下拉菜单

      ![1560184799302](D:\md_down_png\1560184799302.png)

4. 导航条

   #### 导航条的可访问性

   务必使用 `<nav>` 元素，或者，如果使用的是通用的 `<div>` 元素的话，务必为导航条设置 `role="navigation"` 属性，这样能够让使用辅助设备的用户明确知道这是一个导航区域。

   ```js
   <nav class="navbar navbar-default">
     <div class="container-fluid">
       <!-- Brand and toggle get grouped for better mobile display -->
       <div class="navbar-header">
         <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
           <span class="sr-only">Toggle navigation</span>
           <span class="icon-bar"></span>
           <span class="icon-bar"></span>
           <span class="icon-bar"></span>
         </button>
         <a class="navbar-brand" href="#">Brand</a>
       </div>
   
       <!-- Collect the nav links, forms, and other content for toggling -->
       <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
         <ul class="nav navbar-nav">
           <li class="active"><a href="#">Link <span class="sr-only">(current)</span></a></li>
           <li><a href="#">Link</a></li>
           <li class="dropdown">
             <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
             <ul class="dropdown-menu">
               <li><a href="#">Action</a></li>
               <li><a href="#">Another action</a></li>
               <li><a href="#">Something else here</a></li>
               <li role="separator" class="divider"></li>
               <li><a href="#">Separated link</a></li>
               <li role="separator" class="divider"></li>
               <li><a href="#">One more separated link</a></li>
             </ul>
           </li>
         </ul>
         <form class="navbar-form navbar-left">
           <div class="form-group">
             <input type="text" class="form-control" placeholder="Search">
           </div>
           <button type="submit" class="btn btn-default">Submit</button>
         </form>
         <ul class="nav navbar-nav navbar-right">
           <li><a href="#">Link</a></li>
           <li class="dropdown">
             <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
             <ul class="dropdown-menu">
               <li><a href="#">Action</a></li>
               <li><a href="#">Another action</a></li>
               <li><a href="#">Something else here</a></li>
               <li role="separator" class="divider"></li>
               <li><a href="#">Separated link</a></li>
             </ul>
           </li>
         </ul>
       </div><!-- /.navbar-collapse -->
     </div><!-- /.container-fluid -->
   </nav>
   ```

   

## 第十二章 Django框架

### 12.1 web 框架

1. 所有web应用本质就是一个socket的server端
2. 所有的web服务都遵循同一个标准--http协议,来收发文件
3. HTTP协议主要规定了客户端和服务器之间的通信格式

### 12.2 HTTP协议

1. 超文本传输协议--http协议,是一种用于分布式、协作式和超媒体信息系统的应用层协议。HTTP是万维网的数据通信的基础。HTTP有很多应用，但最著名的是用于web浏览器和web服务器之间的双工通信。(广泛使用的版本是http1.1)
2. HTTP是一个客户端终端（用户）和服务器端（网站）请求和应答的标准（TCP）。默认端口为80时自动隐藏端口号.
3. 通常，由HTTP客户端发起一个请求，创建一个到服务器指定端口（默认是80端口）的TCP连接。HTTP服务器则在那个端口监听客户端的请求。一旦收到请求，服务器会向客户端返回一个状态，比如"HTTP/1.1 200 OK"，以及返回的内容，如请求的文件、错误消息、或者其它信息。

#### 12.2.1 http 工作原理

HTTP协议定义Web客户端如何从Web服务器请求Web页面，以及服务器如何把Web页面传送给客户端。HTTP协议采用了请求/响应模型。客户端向服务器发送一个请求报文，请求报文包含请求的方法、URL、协议版本、请求头部和请求数据。服务器以一个状态行作为响应，响应的内容包括协议的版本、成功或者错误代码、服务器信息、响应头部和响应数据。

http响应请求的步骤

1. 客户端连接到Web服务器
   一个HTTP客户端，通常是浏览器，与Web服务器的HTTP端口（默认为80）建立一个TCP套接字连接。例如，http://www.luffycity.com。
2. 发送HTTP请求
   通过TCP套接字，客户端向Web服务器发送一个文本的请求报文，一个请求报文由请求行、请求头部、空行和请求数据4部分组成。
3. 服务器接受请求并返回HTTP响应
   Web服务器解析请求，定位请求资源。服务器将资源复本写到TCP套接字，由客户端读取。一个响应由状态行、响应头部、空行和响应数据4部分组成。
4. 释放连接TCP连接
   若connection 模式为close，则服务器主动关闭TCP连接，客户端被动关闭连接，释放TCP连接;若connection 模式为keepalive，则该连接会保持一段时间，在该时间内可以继续接收请求;
5. 客户端浏览器解析HTML内容
   客户端浏览器首先解析状态行，查看表明请求是否成功的状态代码。然后解析每一个响应头，响应头告知以下为若干字节的HTML文档和文档的字符集。客户端浏览器读取响应数据HTML，根据HTML的语法对其进行格式化，并在浏览器窗口中显示。

```python
在浏览器地址栏键入URL，按下回车之后会经历以下流程：

浏览器向 DNS 服务器请求解析该 URL 中的域名所对应的 IP 地址;
解析出 IP 地址后，根据该 IP 地址和默认端口 80，和服务器建立TCP连接;
浏览器发出读取文件(URL 中域名后面部分对应的文件)的HTTP 请求，该请求报文作为 TCP 三次握手的第三个报文的数据发送给服务器;
服务器对浏览器请求作出响应，并把对应的 html 文本发送给浏览器;
释放 TCP连接;
浏览器将该 html 文本并显示内容; 
```

#### 12.2.2 http请求的方法

1. 主要有八种
   1. get --> 获取一个页面、图片（资源）-->请求没有请求数据
   2. post -->提交数据
   3. head 
   4. put 
   5. delete 
   6. options 
   7. trace 
   8. connect

#### 12.2.3http状态码

所有http的响应的第一行都是状态行，依次是当前HTTP版本号，3位数字组成的状态代码，以及描述状态的短语，彼此由空格分隔。

状态代码的第一个数字代表当前响应的类型：

1. 1xx消息——请求已被服务器接收，继续处理
2. 2xx成功——请求已成功被服务器接收、理解、并接受(200 ok 成功)
3. 3xx重定向——需要后续操作才能完成这一请求(重定向)
4. 4xx请求错误——请求含有词法错误或者无法被执行(404 403 402)
5. 5xx服务器错误——服务器在处理某个正确请求时发生错误

> **注意 : **RFC 2616 中已经推荐了描述状态的短语，例如"200 OK"，"404 Not Found"，但是WEB开发者仍然能够自行决定采用何种短语，用以显示本地化的状态描述或者自定义信息。 

#### 12.2.4 url

超文本传输协议（HTTP）**统一资源定位符**将从因特网获取信息的五个基本元素包括在一个简单的地址中：

1. 传送协议。

2. 层级URL标记符号(为[//],固定不变)

3. 访问资源需要的凭证信息（可省略）

4. 服务器。（通常为域名，有时为IP地址）

5. 端口号。（以数字方式表示，若为HTTP的默认值“:80”可省略）

6. 路径。（以“/”字符区别路径中的每一个目录名称）

7. 查询。（GET模式的窗体参数，以“?”字符为起点，每个参数以“&”隔开，再以“=”分开参数名称与数据，通常以UTF8的URL编码，避开字符冲突的问题）

8. 片段。以“#”字符为起点

   ```python
   以 http://www.luffycity.com:80/news/index.html?id=250&page=1为例
   1. http--.表明协议http(https加密)http(80),https(443)
   2. www.luffycity.com --服务器
   3. :80 --服务器上的网络端口号；80的时候默认隐藏
       “80”是超文本传输协议文件的常用端口号，因此一般也不必写明。一般来说用户只要键入统一资源定位符	的一部分（www.luffycity.com/news/index.html?id=250&page=1）就可以了。
   4. /news/index.html 是路径；
   5. ?id=250&page=1，是查询。
   ```

#### 12.2.5 http 的请求格式 和 响应 格式

![1560219121217](D:\md_down_png\1560219121217.png)

浏览器向服务端请求数据,get请求没有请求数据

```python
1. 请求requset

"(请求方式)get (url路径)/ (协议版本)HTTP/1.1\r\n
k1:v1\r\n
k2:v2\r\n
k3:v3\r\n
\r\n
请求数据(请求体)"  -->get没有请求数据

2.响应(response)

服务器发送给 浏览器

"(协议版本)HTTP/1.1 状态码 状态码描述\r\n
k1:v1\r\n
k2:v2\r\n
k3:v3\r\n
\r\n
响应的数据(响应体response)"  -->html文本
```

![1560243848099](D:\md_down_png\1560243848099.png)

##### 12.2.5.1 浏览器发送请求和接受响应的过程

1. 在浏览器上的地址栏种输入URL，回车，发送get请求；
2. 服务器接受到请求,获取URL路径,根据路径做不同的操作,把返回的数据封装到响应体中,发送给浏览器.
3. 浏览器接受到响应,双方断开连接
4. 浏览器从响应体获取数据,进行解析渲染;

#### 12.2.6  web 框架的功能

1. **本质是socket 服务端**

2. 对于真实开发中的python web程序来说，一般会分为两部分：服务器程序和应用程序。

   服务器程序负责对socket服务端进行封装，并在请求到来时，对请求的各种数据进行整理。

   应用程序则负责具体的逻辑处理。为了方便应用程序的开发，就出现了众多的Web框架，例如：Django、Flask、web.py 等。不同的框架有不同的开发方式，但是无论如何，开发出的应用程序都要和服务器程序配合，才能为用户提供服务。

3. **WSGI协议**（Web Server Gateway Interface）就是一种规范，它定义了使用Python编写的web应用程序与web服务器程序之间的接口格式，实现web应用程序与web服务器程序间的解耦。

   常用的WSGI服务器有uWSGI、Gunicorn。而Python标准库提供的独立WSGI服务器叫wsgiref，Django开发环境用的就是这个模块来做服务器。

4. **主要功能有**

   1. socket收发消息    - wsgiref 测试用  uwsgi 上线用
   2. 根据不同的路径返回不同的内容
   3. 返回动态页面（字符串的替换） —— jinja2

   > django 2 3   /  flask   2   /tornado 1  2   3

   4. 我们一般用django框架(功能很多很全),其他都是轻量化的,需要引入其他模块

5. wsgiref模块(起socket服务端)的使用

   ```python
   from wsgiref.simple_server import make_server   
   # 导入模块
        
   # 将返回不同的内容部分封装成函数   
   def index(url):   
       # 读取index.html页面的内容   
       with open("index.html", "r", encoding="utf8") as f:   
           s = f.read()   
       # 返回字节数据   
       return bytes(s, encoding="utf8")   
        
        
   def home(url):   
       with open("home.html", "r", encoding="utf8") as f:   
           s = f.read()   
       return bytes(s, encoding="utf8") 
   	#return s.encode('utf-8')
        
        
   def timer(url):   
       import time   
       with open("time.html", "r", encoding="utf8") as f:   
           s = f.read()   
           s = s.replace('@@time@@', time.strftime("%Y-%m-%d %H:%M:%S"))   
       return bytes(s, encoding="utf8")   
        
        
   # 定义一个url和实际要执行的函数的对应关系   
   list1 = [   
       ("/index/", index),   
       ("/home/", home),   
       ("/time/", timer),   
   ]   
        
        
   def run_server(environ, start_response):   
   #注意>>>    
   	start_response('200 OK', [('Content-Type', 'text/html;charset=utf8'), ])  # 设置HTTP响应的状态码和头信息   
   #注意>>>    
   	url = environ['PATH_INFO']  # 取到用户输入的url   
       func = None   
       for i in list1:   
           if i[0] == url:   
               func = i[1]   
               break   
       if func:   
           response = func(url)   
       else:   
           response = b"404 not found!"   
       return [response, ]   
        
        
   if __name__ == '__main__':   
       httpd = make_server('127.0.0.1', 8090, run_server)   #注意这个
       print("我在8090等你哦...")   
       httpd.serve_forever()  
   ```

6. jinja2模块的使用(模板渲染工具,模板就是html)

   ```python
   from wsgiref.simple_server import make_server  
   from jinja2 import Template  
     
     
   def index(url):  
       # 读取HTML文件内容  
       with open("index2.html", "r", encoding="utf8") as f:  
           data = f.read()  
           template = Template(data)   # 生成模板文件  
           ret = template.render({'name': 'alex', 'hobby_list': ['抽烟', '喝酒', '烫头']})   # 把数据填充到模板中  
       return bytes(ret, encoding="utf8")  
     
     
   def home(url):  
       with open("home.html", "r", encoding="utf8") as f:  
           s = f.read()  
       return bytes(s, encoding="utf8")  
     
     
   # 定义一个url和实际要执行的函数的对应关系  
   list1 = [  
       ("/index/", index),  
       ("/home/", home),  
   ]  
     
     
   def run_server(environ, start_response):  
       start_response('200 OK', [('Content-Type', 'text/html;charset=utf8'), ])  # 设置HTTP响应的状态码和头信息  
       url = environ['PATH_INFO']  # 取到用户输入的url  
       func = None  
       for i in list1:  
           if i[0] == url:  
               func = i[1]  
               break  
       if func:  
           response = func(url)  
       else:  
           response = b"404 not found!"  
       return [response, ]  
     
     
   if __name__ == '__main__':  
       httpd = make_server('127.0.0.1', 8090, run_server)  
       print("我在8090等你哦...")  
       httpd.serve_forever() 
   ```

   ```html
   <!DOCTYPE html>
   <html lang="zh-CN">
   <head>
     <meta charset="UTF-8">
     <meta http-equiv="x-ua-compatible" content="IE=edge">
     <meta name="viewport" content="width=device-width, initial-scale=1">
     <title>Title</title>
   </head>
   <body>
       <h1>姓名：{{name}}</h1>
       <h1>爱好：</h1>
       <ul>
           {% for hobby in hobby_list %}
           <li>{{hobby}}</li>
           {% endfor %}
       </ul>
   </body>
   </html
   ```

   ```python
   # 使用mysql来获取数据
   conn = pymysql.connect(host="127.0.0.1", port=3306, user="root", passwd="xxx", db="xxx", charset="utf8")
   cursor = conn.cursor(cursor=pymysql.cursors.DictCursor)
   cursor.execute("select name, age, department_id from userinfo")
   user_list = cursor.fetchall()
   cursor.close()
   conn.close()
   ```

### 12.3  django 框架 的使用(重点)

#### 12.3.1 配置

1. 下载安装

   ```py
   1. https://www.djangoproject.com/download/  官网下载
   2. pip3 install django==1.11.21  //pip包管理器安装//注意LTS意指长期支持的版本
   3. 通过pycharm调用pip安装
   	setting ——》 解释器 ——》 点+号 ——》 输入Django ——》 选择版本 ——》 下载安装
   4. pip install django==1.11.21 -i 源
   ```

2. 创建一个django项目

   ```python
   1. 命令行创建,打开控制台,创建了一个mysite的django项目,目录结构如下
   django-admin startproject 项目名称
   ```

   ![1560226751493](D:\md_down_png\1560226751493.png)

   > **注意:** Django 启动时报错 UnicodeEncodeError ...
   >
   > 报这个错误通常是因为计算机名为中文，改成英文的计算机名重启下电脑就可以了。

   ```python
   2. pycharmIDE创建django项目
   ```

   ​	![1560227015628](D:\md_down_png\1560227015628.png)

3. 运行django项目

   `1.一路cd到项目的根目录,manger.py文件目录下`

   ​		`python3 manager.py runserver 127.0.0.1:8080`

   `2. pycharm运行`

4. 模板文件的配置setting.py

   ```python
   TEMPLATES = [
       {
           'BACKEND': 'django.template.backends.django.DjangoTemplates',
           'DIRS': [os.path.join(BASE_DIR, "template")],  # template文件夹位置
           'APP_DIRS': True,
           'OPTIONS': {
               'context_processors': [
                   'django.template.context_processors.debug',
                   'django.template.context_processors.request',
                   'django.contrib.auth.context_processors.auth',
                   'django.contrib.messages.context_processors.messages',
               ],
           },
       },
   ]
   
   #静态文件配置
   STATIC_URL = '/static/'  # HTML中使用的静态文件夹前缀,别名
   STATICFILES_DIRS = [
       os.path.join(BASE_DIR, "static"),
       os.path.join(BASE_DIR, 'static'),
       os.path.join(BASE_DIR, 'static2'),     # 静态文件存放位置
   ]
   
   ```

   静态文件配置图解

   ```html
   <link rel="stylesheet" href="/static/css/login.css">   # 别名开头
   按照STATICFILES_DIRS列表的顺序进行查找。
   
   ```

   

   ![1560254220109](D:\md_down_png\1560254220109.png)

   > **注意:**若用pycharm创建时,要注意模板文件配置

##### 12.3.1.2 简单登录实例

form表单提交数据注意的问题：

1. 提交的地址action=""  请求的方式 method="post"
2. 所有的input框有name属性
3. 有一个input框的type="submit" 或者 有一个button
4. **刚开始学习时可在配置文件中暂时禁用csrf中间件，方便表单提交测试。**提交POST请求，把settings中MIDDLEWARE的'django.middleware.csrf.CsrfViewMiddleware'注释掉, 注释掉之后就可以提交POST请求

#### 12.3.2 django 基础三件套

1. `from django.shortcuts import HttpResponse, render, redirect`

2. `HttpResponse`内部传入一个字符串参数，返回给浏览器。

   ```python
   def index(request):
       # 业务逻辑代码
       return HttpResponse("OK")
   
   ```

3. `render`除request参数外还接受一个待渲染的模板文件和一个保存具体数据的字典参数。

   将数据填充进模板文件，最后把结果返回给浏览器。（类似于我们上面用到的jinja2）

   ```python
   def index(request):
       # 业务逻辑代码
       return render(request, "index.html", {"name": "alex", "hobby": ["烫头", "泡吧"]})
   
   ```

4. `redirect`接受一个URL参数，表示跳转到指定的URL。

   ```python
   def index(request):
       # 业务逻辑代码
       return redirect("/home/")
      #return redirect("https://www.baidu.com/")
   # 本质  Location：地址
   
   ```

#### 12.3.3 django的MVC 和 MTV 框3

MVC ,全名是Model View Controller，是软件工程中的一种软件架构模式，把软件系统分为三个基本部分：模型(Model)、视图(View)和控制器(Controller)，具有耦合性低、重用性高、生命周期成本低等优点。

M: model  模型

V：view   视图   - HTML 

C: controller  控制器   ——路由 传递指令  业务逻辑

Django框架的设计模式借鉴了MVC框架的思想，也是分成三部分，来降低各个部分之间的耦合性。

Django框架的不同之处在于它拆分的三部分为：Model（模型）、Template（模板）和View（视图），也就是MTV框架。

MTV：

M： model  模型 ORM,负责业务对象与数据库的对象(ORM)

T:    tempalte  模板  - HTML,负责如何把页面展示给用户

V：view   业务逻辑,负责业务逻辑，并在适当的时候调用Model和Template

#### 12.3.4 django 的 app

1. 一个Django项目可以分为很多个APP，用来隔离不同功能模块的代码。

2. 创建app

   ```python
   1. 命令行创建
   python manage.py startapp app01
   2.pycharm创建
   tools  ——》  run manage.py task  ——》  startapp  app名称
   
   ```

   ![1560327250145](D:\md_down_png\1560327250145.png)

   ![1560310077499](D:\md_down_png\1560310077499.png)

   然后注册app,需要在settings.py中注册

   ```python
   INSTALLED_APPS = [
   	...
       'app01',
       'app01.apps.App01Config',  # 推荐写法
   ]
   
   ```

#### 12.3.5 django  的 urls.py

写url路径和函数的对应关系

```python
from django.conf.urls import url
from app01 import views

urlpatterns = [
    url(r'^index/', views.index),
    url(r'^login/', views.login),
    url(r'^orm_test/', views.orm_test),
]

```

#### 12.3.6 get 和post

get  获取到一个资源

发get的途径：

1.在浏览器的地址栏种输入URL，回车

2.a标签

3.form表单   不指定 method    method=‘get’

传递参数     url路径?id=1&name=alex

django种获取url上的参数    request.GET   {'id':1,'name':'alex'}   request.GET.get(key)     request.GET[key]

post   提交数据.

form表单   method=‘post’   

参数不暴露在URL    在请求体中  

django中获取post的数据  request.POST 

### 12.4  django 的views系统

#### 12.4.1 CBV和FBV

FBV   function based  view    

CBV   class based  biew

定义CBV：

```python
from django.views import View

class AddPublisher(View):
    
    def get(self,request):
        """处理get请求"""
        return response
    
    
    def post(self,request):
        """处理post请求"""
        return response

```

  使用CBV：

```python
form app01 import views

url(r'^add_publisher/', views.AddPublisher.as_view())

```

#### 12.4.2 as_view的流程

1.项目启动 加载ur.py时，执行类.as_view()    ——》  view函数

​			url(r'add_publisher/',views.AddPublisher.as_view())

​			url(r'add_publisher/', view )

2.请求到来的时候执行view函数：

1. 实例化类AddPublisher  ——》 self 

   ​	self.request = request 

2. 执行seld.dispatch(request, *args, **kwargs)

   1. 判断请求方式是否被允许：http_method_names  = []
   2. 允许    通过反射获取到对应请求方式的方法   ——》 handler
   3. 不允许   self.http_method_not_allowed  ——》handler
   4. 执行handler（request，*args,**kwargs）

    返回响应   —— 》 浏览器

#### 12.4.3.视图加装饰器

 FBV  直接加装饰器

```python
def timer(func):
    def inner(request, *args, **kwargs):
        start = time.time()
        ret = func(request, *args, **kwargs)
        print("函数执行的时间是{}".format(time.time() - start))
        return ret

    return inner



@timer
def publisher_list(request):
    # 从数据库中查询到出版社的信息
    all_publishers = models.Publisher.objects.all().order_by('pk')
    print(all_publishers.values())
    # 返回一个包含出版社信息的页面
    return render(request, 'publisher_list.html', {'all_publishers': all_publishers})

```

CBV 

```python
from django.utils.decorators import method_decorator #方法装饰器

```

1.加在方法上

```python
@method_decorator(timer)
def get(self, request, *args, **kwargs):
    """处理get请求"""

```

2.加在dispatch方法上

```python
@method_decorator(timer)
def dispatch(self, request, *args, **kwargs):
    # print('before')
    ret = super().dispatch(request, *args, **kwargs)
    # print('after')
    return ret


@method_decorator(timer,name='dispatch')
class AddPublisher(View):


```

3.加在类上

```python
@method_decorator(timer,name='post')
@method_decorator(timer,name='get')
class AddPublisher(View):

```

区别：

不适用method_decorator

func   ——》 <function AddPublisher.get at 0x000001FC8C358598>

args  ——》 (<app01.views.AddPublisher object at 0x000001FC8C432C50>, <WSGIRequest: GET '/add_publisher/'>)



使用method_decorator之后：

func ——》 <function method_decorator.<locals>._dec.<locals>._wrapper.<locals>.bound_func at 0x0000015185F7C0D0>

args ——》 (<WSGIRequest: GET '/add_publisher/'>,)

#### 12.4.4 request对象

```python
# 属性
request.methot   请求方式  GET
request.GET    url上携带的参数
request.POST   POST请求提交的数据
request.path_info   URL的路径    不包含ip和端口  不包含参数
request.body    请求体  b'',字节类型
request.FILES   上传的文件
	1.enctype = "multipart/form_data"
	2.f1.chunks()
request.META    请求头 全大写    HTTP_      -  ——》 _

request.COOKIES  cookie
request.session	 session

# 方法
request.get_host()  获取主机的ip和端口
request.get_full_path()   URL的路径   不包含ip和端口 包含参数
request.is_ajax()    判断是否是ajax请求

```

#### 12.4.5 response对象

```python
from django.shortcuts import render, redirect, HttpResponse

HttpResponse('字符串')    ——》  ’字符创‘
render(request,'模板的文件名',{k1:v1})   ——》 返回一个完整的TML页面
redirect('重定向的地址')    ——》 重定向   Location ： 地址

from django.http.response import JsonResponse
JsonResponse({})  , content-type ='application/json' 

JsonResponse([],safe=False)    -->非字典

```

写url对应的函数,功能

```python
def login(request):
    
request.method   ——》 请求方式 GET  POST 
request.POST     ——》 form表单提交POST请求的数据  {}  request.POST['xxx'] request.POST.get('xxx',)

返回值
from django.shortcuts import HttpResponse, render, redirect
HttpResponse   —— 》 字符串 
render(request,'模板的文件名')  ——》 返回一个HTML页面 
redirect('重定向的地址')   ——》 重定向  /  响应头  Location：‘地址’  

```

> **注意:**get请求--->获取到一个页面,提交的数据暴露在URL上的,传递参数 <http://127.0.0.1:8000/index/?id=2&name=alex> ,获取数据  request.GET
>
> post请求--->提交数据,数据隐藏在请求体,获取数据  request.POST



### 12.5 django 的 模型系统ORM

#### 12.5.1 简介

对象关系映射（Object Relational Mapping，简称ORM）模式是一种为了解决面向对象与关系数据库存在的互不匹配的现象的技术。ORM是通过使用描述对象和数据库之间映射的元数据，将程序中的对象自动持久化到关系数据库中。django辅助我们写数据库的

1. 它通常将一个类和一张表一一对应，类的每个实例对应表中的一条记录，类的每个属性对应表中的每个字段。 

   ORM提供了对数据库的映射，不用直接编写SQL代码，只需操作对象就能对数据库操作数据。

   让软件开发人员专注于业务逻辑的处理，提高了开发效率。

2. ORM的缺点是会在一定程度上牺牲程序的执行效率。

   ORM的操作是有限的，也就是ORM定义好的操作是可以完成的，一些复杂的查询操作是完成不了。

   ORM用多了SQL语句就不会写了，关系数据库相关技能退化...

#### 12.5.2 django使用ORM的流程

创建一个MySQL数据库。在控制台创建,django无法帮助我们创建库.

1. 在mysql数据库内创建一个库.ORM不能创库

2. 在settings中配置，Django连接MySQL数据库：

   ```python
   DATABASES = {
       'default': {
           'ENGINE': 'django.db.backends.mysql',    # 引擎	
           'NAME': 'day53',						# 数据库名称
           'HOST': '127.0.0.1',					# ip地址
           'PORT':3306,							# 端口
           'USER':'root',							# 用户
           'PASSWORD':'123'						# 密码
           
       }
   }
   
   ```

3. 在与settings同级目录下的 init文件中写：

   ```python
   #告诉django使用pymysql连接数据库
   
   import pymysql
   pymysql.install_as_MySQLdb()
   #使用pymysql模块连接mysql数据库
   
   ```

4. 创建表,在app下的models.py中写类(必须注册了app)

   ```python
   from django.db import models
   
   class Book(models.Model):
       name = models.CharField(max_length=32)
       pub = models.ForeignKey('Publisher',on_delete=models.CASECADE) # -->外建,默认以id为外键
   	
   class User(models.Model):
       username = models.CharField(max_length=32)   # username varchar(32)
       password = models.CharField(max_length=32)   # username varchar(32)
       
      
   
   ```

   多对多关系的创建

   ![1560737933527](D:\md_down_png\1560737933527.png)

5. 执行数据库迁移的命令

   ```python
   在终端迁移数据库
   
   python manage.py makemigrations   #  检测每个注册app下的model.py   记录model的变更记录
   
   python manage.py migrate   #  同步变更记录到数据库中
   
   ```

#### 12.5.3 orm操作

```python
# 获取表中所有的数据
ret = models.User.objects.all()  # QuerySet 对象列表  【对象】
# 获取一个对象（有且唯一）
obj = models.User.objects.get(username='alex')   # 获取不到或者获取到多个对象会报错
# 获取满足条件的对象
ret = models.User.objects.filter(username='alex1',password='dasb')  # QuerySet 对象列表
```

7. orm的对应关系

   类     ——》   表

   对象  ——》  数据行（记录）

   属性  ——》  字段

8. orm的操作:

   1. 操作表

   2. 操作数据行(查)

      ```python
      from app01 import models 
      
      models.User.objects.all()  # ——》  queryset  对象列表# 查找所有的数据
      obj = models.User.objects.get(name='alex')  # 没有或者多个 就报错,# 查找一条数据
      obj.name   ——》 'alex'
      models.User.objects.filter(name='alex')  # queryset  对象列表# 查询满足条件的所有数据
      
      book_obj.pub  #  ——》 出版社对象 ,外键获取到的是对象
      book_obj.pub.pk 
      book_obj.pub_id
      ```

   3. 增删改

      ```python
      ### 新增
      
      obj = models.Publisher.objects.create(name=publisher_name
      
      obj = models.Publisher(name='xxx')  #——》 存在在内存中的对象
      obj.save()      # ——》   提交到数据库中  新增
                                            
      author_obj = models.Author.objects.create(name=author_name) # 只插入book表中的内容
      author_obj.books.set(books)  # 设置作者和书籍多对多的关系,这里的books是一个列表
         
      ### 删除
         
      
      obj_list = models.Publisher.objects.filter(pk=pk)
      obj_list.delete()
         
      obj = models.Publisher.objects.get(pk=pk)
      obj.delete()
      
      ### 编辑
      # 修改数据
      book_obj.name='xxxxx'
      book_obj.pub = 出版社对象
      book_obj.pub_id = 出版社的id
      book_obj.save()   # ——》 保存到数据库中
      
                                            
                                            
      ### 多对多的修改                                     
      books = request.POST.getlist('books')
      
      # 修改对象的数据
      author_obj.name = name
      author_obj.save()
      # 多对多的关系
      author_obj.books.set(books)  #  每次重新设置
      
      ```

#### 12.5.4  设置多对多关系的3方式

1.django帮我们生成第三张表

```
class Author(models.Model):
    name = models.CharField(max_length=32)
    books = models.ManyToManyField('Book')  # 不在Author表中生产字段，生产第三张表

```

2.自己创建第三张表

```python
class AuthorBook(models.Model):
    author = models.ForeignKey(Author, on_delete=models.CASCADE)
    book = models.ForeignKey(Book, on_delete=models.CASCADE)
    date = models.DateField()

```

3.自建的表和 ManyToManyField 联合使用

```python
class Author(models.Model):
    name = models.CharField(max_length=32)
    books = models.ManyToManyField('Book',through='AuthorBook')  
    # 不在Author表中生产字段，生产第三张表


class AuthorBook(models.Model):
    author = models.ForeignKey(Author, on_delete=models.CASCADE)
    book = models.ForeignKey(Book, on_delete=models.CASCADE)
    date = models.DateField()

```

#### 12.5.5 ORM的字段和参数

```python
(Big)AutoField    # 主键,自增的整形字段，必填参数primary_key=True，则成为数据库的主键。
			# 无该字段时，django自动创建,一个model(类/表单)不能有两个AutoField字段。
    		# 加Big,自增列，必须填入参数 primary_key=True,则是大范围,类似的有
IntegerField # 整数,数值的范围是 -2147483648 ~ 2147483647。
			# Small --->小整数 -32768 ～ 32767-32768 ～ 32767
    		# PositiveSmall --->正小整数 0 ～ 32767
        	# Big --->长整型(有符号的) -9223372036854775808 ～ 9223372036854775807
CharField   -->varchar # 字符串,必须提供max_length参数。max_length表示字符的长度。对应varchar类型 -->
BooleanField  布尔值 -->布尔值类型
			# gender = models.BooleanField('性别', choices=((0, '女'), (1, '男')))
			# NullBooleanField(Field) -->可以为空的布尔值
TextField    文本
DateTimeField DateField  日期时间
# 日期类型，日期格式为YYYY-MM-DD，相当于Python中的datetime.date的实例。
# 日期时间字段，格式为YYYY-MM-DD HH:MM[:ss[.uuuuuu]][TZ]，相当于Python中的datetime.datetime的实例。
    auto_now_add=True    # 新增数据的时候会自动保存当前的时间
    auto_now=True        # 新增、修改数据的时候会自动保存当前的时间
    参数 :auto_now：每次修改时修改为当前日期时间。
          auto_now_add：新创建对象时自动添加当前日期时间。
          auto_now和auto_now_add和default参数是互斥的，不能同时设置
DecimalField   十进制的小数
	max_digits       小数总长度   5
    decimal_places   小数位长度   2
FloatField(Field) --> 浮点型

```

##### 1. 自定义字段 char

```python
class MyCharField(models.Field):
    """
    自定义的char类型的字段类
    """
    def __init__(self, max_length, *args, **kwargs):
        self.max_length = max_length
        super(MyCharField, self).__init__(max_length=max_length, *args, **kwargs)
 
    def db_type(self, connection):
        """
        限定生成数据库表的字段类型为char，长度为max_length指定的值
        """
        return 'char(%s)' % self.max_length
    	# char(32)

# 使用
Class Class(models.Model):
    id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=25)
    # 使用自定义的char类型的字段
    cname = MyCharField(max_length=25)
    
# --------------------分割线----------------------------# 
class UnsignedIntegerField(models.IntegerField):
    def db_type(self, connection):
        return 'integer UNSIGNED'
# PS: 返回值为字段在数据库中的属性。
# Django字段与数据库字段类型对应关系如下：
    'AutoField': 'integer AUTO_INCREMENT',
    'BigAutoField': 'bigint AUTO_INCREMENT',
    'BinaryField': 'longblob',
    'BooleanField': 'bool',
    'CharField': 'varchar(%(max_length)s)',
    'CommaSeparatedIntegerField': 'varchar(%(max_length)s)',
    'DateField': 'date',
    'DateTimeField': 'datetime',
    'DecimalField': 'numeric(%(max_digits)s, %(decimal_places)s)',
    'DurationField': 'bigint',
    'FileField': 'varchar(%(max_length)s)',
    'FilePathField': 'varchar(%(max_length)s)',
    'FloatField': 'double precision',
    'IntegerField': 'integer',
    'BigIntegerField': 'bigint',
    'IPAddressField': 'char(15)',
    'GenericIPAddressField': 'char(39)',
    'NullBooleanField': 'bool',
    'OneToOneField': 'integer',
    'PositiveIntegerField': 'integer UNSIGNED',
    'PositiveSmallIntegerField': 'smallint UNSIGNED',
    'SlugField': 'varchar(%(max_length)s)',
    'SmallIntegerField': 'smallint',
    'TextField': 'longtext',
    'TimeField': 'time',
    'UUIDField': 'char(32)',

```

##### 2. 字段的参数

```python
null                #  数据库中字段是否可以为空
blank=True          #  form表单填写时可以为空
db_column           #  数据库中字段的列名
default             #  字段的默认值
db_index            #  数据库中字段是否可以建立索引
unique=True              #  唯一
choices=((0, '女'), (1, '男'))    #   可填写的内容和提示
	gender = models.BooleanField('性别', choices=((0, '女'), (1, '男')))
verbosename   显示中文
null=True         数据库中该字段可以为null
blank=True      填写form表单时该字段可以为空


```

##### 3. 表的参数 Class Meta

```python
class Meta:
    db_table = "person"  # 表名
    
	# admin中显示的表名称
    verbose_name = '个人信息'
    
	# verbose_name加s
    verbose_name_plural = '个人信息'
    
	# 联合索引 
    index_together = [
        ("name", "age"),  # 应为两个存在的字段
    ]
    # 联合唯一索引
    unique_together = (("name", "age"),)   # 应为两个存在的字段

```

#### 12.5.6 ORM查询13条必备

```python
返回Queryset,对象列表
1.ret = models.Person.objects.all() #  -->all()   获取所有的数据   ——》 QuerySet   对象列表
2..filter() # -->获取满足条件的所有数据   ——》 QuerySet   对象列表
3..exclude(pk=1) # -->exclude()    获取不满足条件的所有数据   ——》 QuerySet   对象列表
4...values('pid','name') # -->values() 拿到对象所有的字段和字段的值    QuerySet  [{},{}]字典
					   # -->values('字段') 拿到对象指定的字段和字段的值    QuerySet  [{} ,{}]字典
5..values_list('name','pid') # -->values_list()     拿到对象所有的字段的值 QuerySet  [ () ,() ],元组
						# -->values_list("字段")  拿到对象所有的字段的值 QuerySet  [ () ,() ],元	
6..order_by('age','-pid') # -->先根据字段排序,在排序,排- 降序    ——》 QuerySet  [ () ,() ]
7..reverse() #--># 反向排序   只能对已经排序的QuerySet进行反转
8..distinct() #-->distinct 去重   不支持按字段去重   想去重 数据必须完全一致

返回对象
9..get() # -->获取满足条件的一个数据   ——》 对象,获取不到或大于1个报错
10.first() #-->first  取第一元素   没有元素 None
11..last()  取最后一元素   没有元素 None

返回数字
10..count()  # -->count()  计数  <-->len

返回布尔值
13..exists() #-->True/False

```

#### 12.5.7 双下划线

```python
# 大于 小于
ret = models.Person.objects.filter(pk__gt=1)   # gt  greater than   大于
ret = models.Person.objects.filter(pk__lt=3)   # lt  less than   小于
ret = models.Person.objects.filter(pk__gte=1)   # gte  greater than   equal    大于等于
ret = models.Person.objects.filter(pk__lte=3)   # lte  less than  equal  小于等于
2.
# in 和 range
ret = models.Person.objects.filter(pk__range=[2,3])   # range  范围,范围bettwen and
ret = models.Person.objects.filter(pk__in=[1,3,10,100])   # in  成员判断
ret = models.Person.objects.exclude(pk__in=[1,3,10,100])  # not in

#contains
ret = models.Person.objects.filter(name__contains='A')    #包含
ret = models.Person.objects.filter(name__icontains='A')   # 忽略大小写

#其他 startswith，istartswith, endswith, iendswith,
ret = models.Person.objects.filter(name__startswith='a')  # 以什么开头
ret = models.Person.objects.filter(name__istartswith='A')

ret = models.Person.objects.filter(name__endswith='a')  # 以什么结尾
ret = models.Person.objects.filter(name__iendswith='I')

# order by
models.Tb1.objects.filter(name='seven').order_by('id')    # asc
models.Tb1.objects.filter(name='seven').order_by('-id')   # desc


# isnull
ret  = models.Person.objects.filter(phone__isnull=False)


# group by
from django.db.models import Count, Min, Max, Sum
models.Tb1.objects.filter(c1=1).values('id').annotate(c=Count('num'))


ret  = models.Person.objects.filter(birth__year='2019')
ret  = models.Person.objects.filter(birth__contains='2018-06-24')


# limit 、offset
models.Tb1.objects.all()[10:20]

# regex正则匹配，iregex 不区分大小写
Entry.objects.get(title__regex=r'^(An?|The) +')
Entry.objects.get(title__iregex=r'^(an?|the) +')



```

#### 12.5.8 外键操作

##### 1. 表示一对多的关系

```python
class Publisher(models.Model):
    name = models.CharField(max_length=32, verbose_name="名称")
    def __str__(self):
        return self.name

class Book(models.Model):
    title = models.CharField(max_length=32)
    pub=models.ForeignKey(Publisher,
                         related_name='books',
                         related_query_name='xxx',
                         on_delete=models.CASCADE)
   def __str__(self):
            return self.title

    # 基于对象的查询
    # 正向
    book_obj = models.Book.objects.get(title='菊花怪大战MJJ')
    print(book_obj.pub) -->所关联的对象

    # 反向
    pub_obj = models.Publisher.objects.get(pk=1)
    pub_obj.book_set   -->一个关系管理对象,（类名小写_set）
    print(pub_obj.book_set.all())  # 没有指定related_name时,使用(类名小写_set.all())获取所有  		
    pub_obj.book_set  -->一个关系管理对象,（类名小写_set）
    print(pub_obj.books.all())  #  指定related_name='books'


    # 基于字段的查询
    #正向
    ret = models.Book.objects.filter(title='菊花怪大战MJJ')
    #  查询老男孩出版的书
    ret = models.Book.objects.filter(pub__name='老男孩出版社')
    # pub=Publisher Object/pub_id = 1`````


    #反向
    # 查询出版菊花怪大战MJJ的出版社

    # 没有指定related_name   ,使用类名的小写
    ret= models.Publisher.objects.filter(book__title='菊花怪大战MJJ')

    # 指定了related_name='books'
    ret= models.Publisher.objects.filter(books__title='菊花怪大战MJJ')

    # related_query_name='xxx'
    ret= models.Publisher.objects.filter(xxx__title='菊花怪大战MJJ')
    #优先级由低到高

```

##### 2.关系管理对象的特殊方法

```python
print(pub_obj.books.all()) #-->关系管理对象,relative_name =books
1.set方法
pub_obj.books.set(models.Book.objects.filter(pk__in=[4,5]))  
	# 不能用id 只能用对象
2.add方法
pub_obj.books.add(*models.Book.objects.filter(pk__in=[1,2]))
	
3.remove方法 和 clear方法
pub = models.ForeignKey(Publisher, 
                        related_name='books', 		
                        related_query_name='xxx',
                        null=True, 
                        on_delete=models.CASCADE)
# 外键可为空时有remove  clear
pub_obj.books.remove(*models.Book.objects.filter(pk__in=[1,2]))

pub_obj.books.clear()

4.create方法通过关联创建
obj = pub_obj.books.create(title='用python养猪')

```

##### 3.多对多的关系

```python
mjj = models.Author.objects.get(pk=1)
1.all()  所关联的所有的对象
	mjj.books.all()
2.set  设置多对多的关系    [id,id]    [ 对象，对象 ]
	mjj.books.set([1,2])
	mjj.books.set(models.Book.objects.filter(pk__in=[1,2,3]))
3.add  添加多对多的关系   (id,id)   (对象，对象)
    mjj.books.add(4,5)
    mjj.books.add(*models.Book.objects.filter(pk__in=[4,5]))
4.remove 删除多对多的关系  (id,id)   (对象，对象)
    mjj.books.remove(4,5)
    mjj.books.remove(*models.Book.objects.filter(pk__in=[4,5]))
5.clear()   清除所有的多对多关系
	mjj.books.clear()
6.create()
obj = mjj.books.create(title='跟MJJ学前端',pub_id=1)
print(obj)
book__obj = models.Book.objects.get(pk=1)

obj = book__obj.authors.create(name='taibai')
print(obj)


```

#### 15.5.9 聚合分组

```python
from app01 import models
from django.db.models import Max, Min, Avg, Sum, Count     # 先导入


ret = models.Book.objects.filter(pk__gt=3).aggregate(Max('price'),
                                                     avg=Avg('price'))
print(ret)

# 分组
# 统计每一本书的作者个数
ret = models.Book.objects.annotate(count=Count('author')) # annotate 注释


# 统计出每个出版社的最便宜的书的价格
# 方式一
ret = models.Publisher.objects.annotate(Min('book__price')).values()
# 方式二
ret = models.Book.objects.values('pub_id').annotate(min=Min('price'))


```

#### 12.5.10 F 和 Q

```python
from django.db.models import F

# 比较两个字段的值
ret=models.Book.objects.filter(sale__gt=F('kucun'))

# 只更新sale字段
models.Book.objects.all().update(sale=100)

# 取某个字段的值进行操作
models.Book.objects.all().update(sale=F('sale')*2+10)

```

Q(条件)

|  或

&  与 

~ 非

```python
from django.db.models import Q

ret = models.Book.objects.filter(Q(Q(pk__gt=3) | Q(pk__lt=2)) & Q(price__gt=50))
print(ret)

```

#### 12.5.11 事务

```python
from django.db import transaction

try:
    with transaction.atomic():
        # 进行一系列的ORM操作

        models.Publisher.objects.create(name='xxxxx')
        models.Publisher.objects.create(name='xxx22')

except Exception as e :
    print(e)

```



### 12.6 django的模板系统 template

#### 12.6.1. 常用语法

```html
   1.{{ 变量 }}  -->{{ }}表示变量，在模板渲染的时候替换成值,变量名由字母数字和下划线组成
   2.{%   %}   -->{% %}表示逻辑相关的操作。
   3. (.) -->在模板语言中有特殊的含义，用来获取对象的相应属性值。
   	{# 取l中的第一个参数 #}
       {{ l.0 }}
       {# 取字典中key的值 #}
       {{ d.name }}
       {# 取对象的name属性 #}
       {{ person_list.0.name }}
       {# .操作只能调用不带参数的方法 #}
       {{ person_list.0.dream }}
   
   	注意:当模板系统遇到一个（.）时，会按照如下的顺序去查询：
                   在字典中查询
                   属性或者方法
                   数字索引
   .索引  .key .属性  .方法

```

#### 12.6.2.过滤器

   `filter过滤器,来改变变量的显示结果`

   `{{ value|filter_name:参数 }}`**:冒号左右没有空格,没有空格,没有空格**

1. defalut
   `{{ value|default:"nothing"}}`
     	如果value值没传的话or bool(value)的值为空就显示nothing,bool()的值为空

     	注：TEMPLATES的OPTIONS可以增加一个选项：string_if_invalid：'找不到'，可以替代default的的作用。

2. filesizeformat

   `{{value|filesizeformat}}`

   ​	将值格式化为一个 “人类可读的” 文件尺寸 （例如 '13 KB', '4.1 MB', '102 bytes', 等等）
   ​	{{ value|filesizeformat }}这里的value是以字节为单位的int或者str,float会舍弃小数部分.
   ​	如果 value 是 123456789，输出将会是 117.7 MB。

3. add 给变量加参数
   `{{ value|add:"2" }}`-->value是数字4，则输出结果为6。
     	`{{ first|add:second }}`-->如果first是 [1,.2,3] ，second是 [4,5,6] ，那输出结果是[1,2,3,4,5,6] 
      优先转换为数值运算,add过滤器

4. length--返回value的长度`{{[1,2,3,4]|length}}   display:4`

   `length_is:arg`

5. slice切片-->`{{value|slice:"2:-1"}}`,取首元素{{ value|first }},取最后元素{{ value|last }},可以切片和步长,一般是列表.**使用与Python列表切片相同的语法**。

6. dictsort and dictsortreversed(反序)
   获取字典列表并返回按参数中给出的键对应的值排序的列表。

   ```python
   1. [
      	   {'name': 'zed', 'age': 19},
          {'name': 'amy', 'age': 22},
          {'name': 'joe', 'age': 31},
      ]
          {{value|dictsort:'age'}},
   2. 更高级的用法
   [
           {'title': '1984', 'author': {'name': 'George', 'age': 45}},
           {'title': 'Timequake', 'author': {'name': 'Kurt', 'age': 75}},
           {'title': 'Alice', 'author': {'name': 'Lewis', 'age': 33}},
   ]
   {% for book in books|dictsort:"author.age" %}
   	{{ book.title }} ({{ book.author.name }})
   {% endfor %} 
   
   3. [
       ('a', '42'),
       ('c', 'string'),
       ('b', 'foo'),
   ]
   {{ value|dictsort:0 }} display:根据索引处的元素排序列表,此时arg必须是int,str则输出为空
   
   ```

7. floatformat 

   floatformat,没有arg时,舍入到1个小数位

   - 和数字整数参数arg一起使用，则将数字floatformat四舍五入到arg个小数位,不足的补0
   - 有用的是传递0（零）作为参数，它将浮点数舍入为最接近的整数。
   - 传递给的参数floatformat为负数，它会将数字四舍五入到多个小数位 - 但只有在显示小数部分时才会这样。,整数还是整数,小数补足道arg个小数位

8. join 拼接,和str.join(eq)类似,`{{value|join:"//"}}`使用字符串连接列表，如Python `str.join(list)`

9. random,返回给定列表的随机项

```python
   1.divisibleby,如果value可以被arg整除，则返回True.
   2.get_digit,给定一个整数，返回请求的数字，其中1是最右边的数字，2是第二个最右边的数	字，等等,value和arg都是int,返回int,从右向左,以1为起始数字.
   3.lower小写/upperdaxie/title标题/capfirst首字母大写-->{{value|upper/lower/title/capfirst}}
   4.ljust左对齐/右对齐rjust/center居中-->{{value|center:"15"}}
   5.wordcount 返回单词数,"Joel is a slug"4
   9.truncatechars截断,参数为：截断的字符数,如果字符串字符多于指定的字符数量，那么会被截断。截断的字符串将以可翻译的省略号序列（“...”）结尾。
       {{ value|truncatechars:9}}
   10.date 日期格式化-->{{ value|date:"Y-m-d H:i:s"}}
   	更多字符串格式化关注
       https://docs.djangoproject.com/en/1.11/ref/templates/builtins/#date
   11.若value="<a href='#'>点我</a>",添加{{ value|safe}},
   	是一个单独的变量我们可以通过过滤器“|safe”的方式告诉Django这段代码是安全的不必转义。
   12.cut:arg,从给定的字符串中删除arg的所有值。 
   

```

   ![1560859985675](D:\md_down_png\1560859985675.png)

   ![1560860005470](D:\md_down_png\1560860005470.png)

   ![1560860052069](D:\md_down_png\1560860052069.png)

#### 12.6.3.for 循环

```python
   <ul>
   {% for user in user_list %}
       <li>{{ user.name }}</li>
   {% endfor %}
   </ul>
   
   #-------------------------#
   一些可用参数
       forloop.counter	当前循环的索引值（从1开始）
       forloop.counter0	当前循环的索引值（从0开始）
       forloop.revcounter	当前循环的倒序索引值（到1结束）
       forloop.revcounter0	当前循环的倒序索引值（到0结束）
       forloop.first	当前循环是不是第一次循环（布尔值）
       forloop.last	当前循环是不是最后一次循环（布尔值）
       forloop.parentloop{}	本层循环的外层循环,父循环的字典
   #循环完了执行empty的代码
   <ul>
   {% for user in user_list %}
       <li>{{ user.name }}</li>
   {% empty %} -->for循环没有循环出内容
       <li>空空如也</li>
   {% endfor %}
   </ul>
   

```

#### 12.6.4.if-else-elif

```python
   {% if user_list %}
     用户人数：{{ user_list|length }}
   {% elif black_list %}
     黑名单数：{{ black_list|length }}
   {% else %}
     没有用户
   {% endif %}
   
   注意:if语句支持 and 、or、==、>、<、!=、<=、>=、in、not in、is、is not判断。
   注意:if语句支持 and 、or、==、>、<、!=、<=、>=、in、not in、is、is not判断。
   注意:if语句支持 and 、or、==、>、<、!=、<=、>=、in、not in、is、is not判断。
   注意 : 模板中  不支持连续连续判断  也不支持算数运算（过滤器）

```

#### 12.6.5. csrf_token 标签(跨站请求伪造)

   放到一个form表单里面,放在form表单内   有一个隐藏的input标签   name=‘csrfmiddlewaretoken’  

   ![1560911471554](D:\md_down_png\1560911471554.png)   ![1560911934451](D:\md_down_png\1560911934451-1561376183656.png)

   这样注释的中间件就解除

#### 12.6.6. with 定义一个智能关键变量

```python
   {% with total=business.employees.count %}
       {{ total }} employee{{ total|pluralize }}
   {% endwith %}
   
   {%  with 变量 as new  %}
   	{{ new }}
   {% endwith %}

```

#### 12.6.7. 注意事项

```python
   1. Django的模板语言不支持连续判断，即不支持以下写法：
   
   {% if a > b > c %}
   ...
   {% endif %}
   2. Django的模板语言中属性的优先级大于方法
   
   def xx(request):
       d = {"a": 1, "b": 2, "c": 3, "items": "100"}
       return render(request, "xx.html", {"data": d})
   如上，我们在使用render方法渲染一个页面的时候，传的字典d有一个key是items并且还有默认的 d.items() 方法，此时在模板语言中:
   
   {{ data.items }}
   默认会取d的items key的值。

```

#### 12.6.8. 母版和继承,

1. 母版	

   就是一个普通HTML提取多个页面的公共部分 定义block块

2. 继承：

   1. {% extends ‘base.html’ %}
   2. 重写block块   —— 写子页面独特的内容

3. 注意的点：

   1. {% extends 'base.html' %} 写在第一行   前面不要有内容 有内容会显示
   2. {% extends 'base.html' %}  'base.html' 加上引号   不然当做变量去查找
   3. 把要显示的内容写在block块中
   4. 定义多个block块，定义 css  js 块

4. 继承母版

   在子页面中在页面最上方使用下面的语法来继承母板。

   ```
   {% extends 'layouts.html' %}
   
   ```

5. 块bolck

   ```
   {% block page-main %}
     <p>世情薄</p>
     <p>人情恶</p>
     <p>雨送黄昏花易落</p>
   {% endblock %}
   
   ```

6. 组件

   一小段HTML代码段   ——》 nav.html 

   {% include ‘nav.html ’  %}

   可以将常用的页面内容如导航条，页尾信息等组件保存在单独的文件中，然后在需要使用的地方按如下语法导入即可。

   ```
   {% include 'navbar.html' %}
   
   ```

7. 静态文件相关

   ```
   {% load static %}
   
    <link rel="stylesheet" href="{% static '/plugins/bootstrap-3.3.7/css/bootstrap.css' %}">
    <link rel="stylesheet" href="{% static '/css/dsb.css' %}">
    
   {%  static ‘静态文件的相对路径’ %}   这样使用
   {% static '/plugins/bootstrap-3.3.7/css/bootstrap.css' %} 
   
   {% get_static_prefix %}   ——》 获取别名
   
   {%  get_static_prefix  %}      ——》   STATIC_URL  
         
   引用JS文件时使用：
   {% load static %}
   <script src="{% static "mytest.js" %}"></script>
   
   某个文件多处被用到可以存为一个变量
   {% load static %}
   {% static "images/hi.jpg" as myphoto %}
   <img src="{{ myphoto }}"></img>
   
   {% load static %}
   {% get_static_prefix as STATIC_PREFIX %}
   
   <img src="{{ STATIC_PREFIX }}images/hi.jpg" alt="Hi!" />
   <img src="{{ STATIC_PREFIX }}images/hi2.jpg" alt="Hello!" />
   ```

#### 12.6.9.自定义simpletag,filter,inclusiontag

1. 自定义过滤器`filter`

   ```python
   # 	前提,自定义过滤器
   
   1.在app下创建一个名为tempplatetags的python包；#必须是这个名字
   
   2.在python中创建py文件,文件名自定义（my_tags.py）;#随意起名
   
   3.在py文件中写：
   
   from django import template
   register = template.Library()  # register也不能变
   
   4.写函数+装饰器:
   @register.filter
   def add_xx(value, arg):  # 最多有两个参数
   	#逻辑代码
       return '{}-{}'.format(value, arg)
   
   ##----我是华丽的分割线-----##
   
   #	使用
   # 在用使用的html文件内先导入,再使用
   {% load my_tags %}
   {{ 'alex'|add_xx:'dsb' }}
   
   ```

2. 自定义方法`simplet tag`

   和自定义filter类似，只不过接收更灵活的参数。

   ```python
   # 定义注册simple tag
   @register.simple_tag(name="plus")
   def plus(a, b, c):
       return "{} + {} + {}".format(a, b, c)
   
   #使用自定义simple tag
   {% load app01_demo %}
   
   {# simple tag #}
   {% plus "1" "2" "abc" %} # --><参数间空格隔开
   
   ```

3. 自定义标签`inclusion_tag`,多用于返回html代码片段

   ```python
   # 1.定义注册
   from django import template
   register = template.Library()
   @register.inclusion_tag('result.html')
   def show_results(n):
       n = 1 if n < 1 else int(n)
       data = ["第{}项".format(i) for i in range(1, n+1)]
       return {"data": data}
   
   # 2.在模板文件内-->templates/result.html
   
   <ul>
     {% for choice in data %}
       <li>{{ choice }}</li>
     {% endfor %}
   </ul>
   
   # 3.在要返回的html页面内使用---->templates/index.html
   <!DOCTYPE html>
   <html lang="en">
   <head>
     <meta charset="UTF-8">
     <meta http-equiv="x-ua-compatible" content="IE=edge">
     <meta name="viewport" content="width=device-width, initial-scale=1">
     <title>inclusion_tag test</title>
   </head>
   <body>
   
   {% load my_inclusion %}
   
   {% show_results 10 %}
   </body>
   </html>
   
   ```

   ​		![1560992896941](D:\md_down_png\1560992896941.png)

   ​				![1560917495352](D:\md_down_png\1560917495352.png)

### 12.7 django的路由系统

#### 12.7.1 urlconf 配置

```python
urlpatterns = [
    # url(r'^admin/', admin.site.urls),
    url(r'^blog/$', views.blog),
    url(r'^blog/[0-9]{4}/$', views.blogs),
]

#----------------------分割线---------------------------------#
urlpatterns = [
     url(正则表达式, views视图，参数，别名),
]
正则表达式：一个正则表达式字符串
views视图：一个可调用对象，通常为一个视图函数
参数：可选的要传递给视图函数的默认参数（字典形式）
别名：一个可选的name参数

```

> **注意: django2.0版本路由系统的写法变更了,也支持re_path写法,和1.11版本的url一致**
>
> urlconf在请求的URL 上查找，将它当做一个普通的Python 字符串。不包括GET和POST参数以及域名。
>
> urlconf不检查请求的方法。换句话讲，所有的请求方法 —— 同一个URL的`POST`、`GET`、`HEAD`等等 —— 都将路由到相同的函数。

##### 1. 通过url的字典参数传递额外的参数

可以接收一个可选的第三个参数，它是一个字典，表示想要传递给视图函数的额外关键字参数。

该优先级是最高的,当传递额外参数的字典中的参数和URL中捕获值的命名关键字参数同名时，函数调用时将使用的是字典中的参数，而不是URL中捕获的参数。

#### 12.7.2 正则表达式

```python
^   $    [0-9]   [a-zA-Z]     \d   \w   .    *  +   ？

```

1. urlpatterns中的元素按照书写顺序从上往下逐一匹配正则表达式，一旦匹配成功则不再继续。

2. 若要从URL中捕获一个值，只需要在它周围放置一对圆括号（分组匹配）。

3. 不需要添加一个前导的反斜杠，因为每个URL 都有。例如，应该是^articles 而不是 ^/articles。

4. 每个正则表达式前面的'r' 是可选的但是建议加上。

5. Django settings.py配置文件中默认没有 APPEND_SLASH 这个参数，但 Django **默认这个参数为 APPEND_SLASH = True**。 其作用就是自动在网址结尾加'/'。

6. 如果在settings.py中设置了 **APPEND_SLASH=False**，此时我们再请求 http://www.example.com/blog 时就会提示找不到页面。

   > **补充说明:是否开启URL访问地址后面不为/跳转至带有/的路径的配置项
   > APPEND_SLASH=True**

#### 12.7.3 分组和命名分组

```python
1. 分组
	url(r'^blog/([0-9]{4})/$', views.blogs)
    # —— 》 分组  将捕获的参数按位置传参传递给视图函数
2.命名分组
	url(r'^blog/(?P<year>[0-9]{4})/$', views.blogs)
    # —— 》 命名分组  将捕获的参数按关键字传参传递给视图函数
类似于 -->views.month_archive(request, year="2017", month="12")
注意 -->捕获到的永远是字符串,另外可以在view视图处设置year和month的默认值,page()将使用正则表达式捕获到的num值的优先级更高.但在url的字典参数中,更高的优先级.

```

#### 12.7.4 路由分发include其他的url

```python
from django.conf.urls import include, url

urlpatterns = [
    # url(r'^admin/', admin.site.urls),
    url(r'^app01/', include('app01.urls')),# 可以包含其他的URLconfs文件
]

#-------------------分割线-----------------------#
# app01目录下的urls.py

from django.conf.urls import url
from . import views
# from app01 import views
urlpatterns = [
    # url(r'^admin/', admin.site.urls),
    url(r'^blog/$', views.blog),
    url(r'^blog/(?P<year>[0-9]{4})/(?P<month>\d{2})/$', views.blogs),
]

```

#### 12.7.5 url反向解析

​	为了不要硬编码这些url（费力、不可扩展且容易产生错误）或者设计一种与URLconf 毫不相关的专门的URL 生成机制，因为这样容易导致一定程度上产生过期的URL。django提供了一种方法,双向映射,URL 映射是URL .

```python
1.根据用户/浏览器发起的URL 请求，它调用正确的Django 视图，并从URL 中提取它的参数需要的值。
2.根据Django 视图的标识和将要传递给它的参数的值，获取与之关联的URL。

url反查 -->反向解析
	1.在模板中：使用url模板标签。
	2.在Python 代码中：使用django.core.urlresolvers.reverse() 函数。
	3.在更高层的与处理Django 模型实例相关的代码中：使用get_absolute_url() 方法。
    
简单来说就是可以给我们的URL匹配规则起个名字，一个URL匹配模式起一个名字。
这样我们以后就不需要写死URL代码了，只需要通过名字来调用当前的URL。

```

1. 对静态的url

   ```python
   url(r'^blog/$', views.blog, name='blog'), -->别名
   
   1. 反向解析
   	templates
       	{% url 'blog' %}    -->  /blog/
   	py文件-->views
       	from django.urls import reverse
   		reverse('blog')   --> '/blog/'
           #例如 return redirect(reverse("blog"))
   
   ```

2. 普通分组

   ```python
   url(r'^blog/([0-9]{4})/(\d{2})/$', views.blogs, name='blogs'),
   	templates
       	{% url 'blogs' 2222 12 %}"   ——》  /blog/2222/12/
       py文件 -->views
       	from django.urls import reverse
   		reverse('blogs',args=('2019','06',))   ——》 /app01/blog/2019/06/ 
   # 注意args=("12345",)  --->这个逗号不能少
   
   ```

3. 命名分组

   ```python
   url(r'^blog/(?P<year>[0-9]{4})/(?P<month>\d{2})/$', views.blogs, name='blogs')
   	templates
       	{% url 'blogs' 2222 12 %}"   ——》  /blog/2222/12/
   		{% url 'blogs' year=2222 month=12 %}"   ——》  /blog/2222/12/
       py文件 -->views
       	from django.urls import reverse
   		reverse('blogs',args=('2019','06'))   ——》 /app01/blog/2019/06/ 
   		reverse('blogs',kwargs={'year':'2019','month':'06'})   ——》 /app01/blog/2019/06/ 
   
   ```

4. 总结

   1. url上捕获参数  都是字符串
   2. 命名你的URL 模式时，请确保使用的名称不会与其它应用中名称冲突。如果你的URL 模式叫做`comment`，而另外一个应用中也有一个同样的名称，当你在模板中使用这个名称的时候不能保证将插入哪个URL。

   在URL 名称中加上一个前缀，比如应用的名称，将减少冲突的可能。我们建议使用`myapp-comment` 而不是`comment`。

#### 12.7.6 url反向解析 -命名空间模式namespace

即使不同的APP使用相同的URL名称，URL的命名空间模式也可以让你唯一反转命名的URL。

```python
urlpatterns = [
    # url(r'^admin/', admin.site.urls),
    url(r'^app01/', include('app01.urls', namespace='app01')),
    url(r'^app02/', include('app02.urls', namespace='app02')),
]
# 在app01 和 app02 中都有url的name="blogs"
# 我的两个app中 url名称重复了，我反转URL的时候就可以通过命名空间的名称得到我当前的URL
语法 : "命名空间名称:URL名称"
templates
	{% url 'app01:blogs' 2222 12 %}
py文件
	reverse('app01:blogs',args=('2019','06'))
# 这样即使app中URL的命名相同，我也可以反转得到正确的URL了。

```

### 12.8 cookie 和 session

#### 12.8.1 简介

http是无状态的,即每次请求都是独立的，它的执行情况和结果与前面的请求和之后的请求都无直接关系，它不会受前面的请求响应情况直接影响，也不会直接影响后面的请求响应情况。对服务器来说，每次的请求都是全新的。

我们需要在连接过程中保持一些数据,cookie就诞生了

#### 12.8.2 cookie

1. 保存在浏览器本地上一组组键值对,    具体指的是一段小信息，它是服务器发送出来存储在浏览器上的一组组键值对，下次访问服务器时浏览器会自动携带这些键值对，以便服务器提取有用信息。

2. 原理 ：由服务器产生内容，浏览器收到请求后保存在本地；当浏览器再次访问时，浏览器会自动带上Cookie，这样服务器就能通过Cookie的内容来判断这个是“谁”了。

3. 特点

   1. 由服务器让浏览器进行设置的
   2. 浏览器保存在浏览器本地
   3. 下次访问时自动携带相应的cookie

4. 应用

   1. 登录,cookie就保存is_login = True/1等等信息
   2. 保存浏览习惯
   3. 简单的投票,count:num,限定投票次数.

   ![1561515454645](D:\md_down_png\1561515454645.png)

#### 12.8.3 django 操作 cookie

##### 1.设置 cookie

```python
rep = HttpResponse(...)
rep ＝ render(request, ...)
# 本质 本质   set-cookie ： ‘key:value’

rep.set_cookie(key,value,...) # -->简单设置
rep.set_signed_cookie(key,value,salt='加密盐',...) # 加密设置
#可以有其他参数

max_age=None, 超时时间,秒数,会自动转化成expires
expires=None, 超时时间(IE requires expires, so set it if hasn't been already.)
path='/', Cookie生效的路径，/ 表示根路径，特殊的：根路径的cookie可以被任何url的页面访问
domain=None, Cookie生效的域名
secure=False, https传输
httponly=False 只能http协议传输，无法被JavaScript获取（不是绝对，底层抓包可以获取到也可以被覆盖）

```

##### 2.获取cookie

```python
ret = request.COOKIES   -->是一个字典,[key]获取   ,get 方法获取

is_login = request.get_signed_cookie('is_login', salt='s21', default='sss')
# 加密方法获取

```

##### 3.删除 cookie

```python
ret = redirect('/login/')
ret.delete_cookie('is_login')
# 其删除的本质是 调用set_cookie方法,设置对应的值为"",max_age=0

```

#### 12.8.4 session

##### 1.定义

保存在服务器上一组组键值对(必须依赖cookie)

##### 2. cookie的缺点

1.cookie保存在浏览器本地,不安全

2.大小个数受到限制,4096字节

##### 3.django中操作session

```python
request.session[key]  = value
.set_default方法

request.session[key]    request.session.get(key)

# 将所有Session失效日期小于当前日期的数据删除
request.session.clear_expired()

# 检查会话session的key在数据库中是否存在
request.session.exists("session_key")

# 删除当前会话的所有Session数据
del request.session[key] 
request.session.pop()
request.session.delete()

　　
# 删除当前的会话数据并删除会话的Cookie。
request.session.flush() 
    这用于确保前面的会话数据不可以再次被用户的浏览器访问
    例如，django.contrib.auth.logout() 函数中就会调用它。

# 设置会话Session和Cookie的超时时间
request.session.set_expiry(value)

```

### 12.9 dajngo的csrf 中间件

#### 12.9.1. csrf 装饰器

```python
from django.views.decorators.csrf import csrf_exempt,csrf_protect，ensure_csrf_cookie
from django.utils.decorators import method_decorator # -->方法装饰器
csrf_exempt   某个视图不需要进行csrf校验
csrf_protect  某个视图需要进行csrf校验
ensure_csrf_cookie  确保生成csrf的cookie
#     @method_decorator(ensure_csrf_cookie)
# @method_decorator(csrf_protect)  只能用在dispatch方法上
	@method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        ret = super().dispatch(request, *args, **kwargs)
        return ret

```

#### 12.9.2.csrf 功能

##### 1.csrf 中间件执行process_request :

```python
1. 从cookie 中获取到csrftoken的值
2. csrftoken的值放路到request.META

```

##### 2. 执行process_view

```python
1. 查询视图函数是否使用csrf_exempt装饰器，使用了就不进行csrf的校验
2. 判断请求方式：
   	1. 如果是'GET', 'HEAD', 'OPTIONS', 'TRACE',就不进行csrf校验
    2. 其他的请求方式（post，put）
      进行csrf校验：
      1.获取cookie中csrftoken的值
      	获取csrfmiddlewaretoken的值
			能获取到  ——》 request_csrf_token
			获取不到   ——》 获取请求头中X-csrftoken的值 ——》request_csrf_token
      比较上述request_csrf_token和cookie中csrftoken的值，比较成功接收请求，比较不成功拒绝请求。

```

##### 3.中间件是处理django的请求和响应的框架级别的钩子，本质上就是一个类。

中间件可以定义五个方法:

- process_request(self,request)
- process_view(self, request, view_func, view_args, view_kwargs)
- process_template_response(self,request,response)
- process_exception(self, request, exception)
- process_response(self, request, response)

 4个特征：  执行时间、执行顺序、参数、返回值

###### process_request(self,request)

执行时间：视图函数之前

参数：request   —— 》 和视图函数中是同一个request对象

执行顺序：按照注册的顺序  顺序执行

返回值：

​		None ： 正常流程

​		HttpResponse： 后面的中间的process_request、视图函数都不执行，直接执行当前中间件中的process_response方法，倒叙执行之前的中间中process_response方法。

###### process_response(self, request, response)

执行时间：视图函数之后

参数：

​	request   —— 》 和视图函数中是同一个request对象

​	response   ——》  返回给浏览器响应对象

执行顺序：按照注册的顺序  倒叙执行

返回值：

​		HttpResponse：必须返回response对象

###### process_view(self, request, view_func, view_args, view_kwargs)

执行时间：视图函数之前，process_request之后

参数：

​		request   —— 》 和视图函数中是同一个request对象

​		view_func  ——》 视图函数

​		view_args   ——》 视图函数的位置参数

​		view_kwargs  ——》 视图函数的关键字参数

执行顺序：按照注册的顺序  顺序执行

返回值：

​		None ： 正常流程

​		HttpResponse： 后面的中间的process_view、视图函数都不执行，直接执行最后一个中间件中的process_response方法，倒叙执行之前的中间中process_response方法。

###### process_exception(self, request, exception)

执行时间（触发条件）：视图层面有错时才执行

参数：

​		request   —— 》 和视图函数中是同一个request对象

​		exception   ——》 错误对象

执行顺序：按照注册的顺序  倒叙执行

返回值：

​		None ： 交给下一个中间件取处理异常，都没有处理交由django处理异常

​		HttpResponse： 后面的中间的process_exception不执行，直接执行最后一个中间件中的process_response方法，倒叙执行之前的中间中process_response方法。

###### process_template_response(self,request,response)

执行时间（触发条件）：视图返回的是一个templateResponse对象

参数：

​	request   —— 》 和视图函数中是同一个request对象

​	response   ——》  templateResponse对象

执行顺序：按照注册的顺序  倒叙执行

返回值：

​		HttpResponse：必须返回response对象

###### django请求的生命周期

![1561783661582](C:/Users/Administrator/Documents/Tencent%20Files/1821333144/FileRecv/day65-1/day65-1/asset/1561783661582.png)

### 12.10 ajax

##### 1.发请求的方式

有 地址让输入url / form表单的get 和 post 请求  / a 标签的get请求

##### 2.ajax 和 json

1. json 是各系统,各编程语言间传输数据的一种规定格式

2. AJAX（Asynchronous Javascript And XML）翻译成中文就是“异步的Javascript和XML”。即使用Javascript语言与服务器进行异步交互，传输的数据为XML（当然，传输的数据不只是XML）。

   1. 优点是在不重新加载整个页面的情况下，可以与服务器交换数据并更新部分网页内容。（这一特点给用户的感受是在不知不觉中完成请求和响应过程）
   2. AJAX 不需要任何浏览器插件，但需要用户允许JavaScript在浏览器上执行。是一个异步的过程

3. javascript 对 json对象的方法

   ```html
   JSON.parse(),# 将用于将一个 JSON 字符串转换为 JavaScript 对象　
   	JSON.parse('{"name":"alex"}');
   JSON.stringify(),用于将 JavaScript 值转换为 JSON 字符串。
   	JSON.stringify({"name":"alex"})
   
   ```

4. AJAX 就是使用js的技术发送请求和接受响应的

   特点:

   ​	1.异步的  ,AJAX使用JavaScript技术向服务器发送异步请求；

   ​	2.是局部刷新的,AJAX请求无须刷新整个页面

   ​	3.传输的数据量少,因为服务器响应内容不再是整个页面，而是页面中的部分内容，所以AJAX性能高；

##### 3. AJAX常用应用场景

1. 搜索引擎根据用户输入的关键字，自动提示检索关键字。
2. 注册时候的用户名的查重。
   - 这里就使用了AJAX技术！当文件框发生了输入变化时，使用AJAX技术向服务器发送一个请求，然后服务器会把查询到的结果响应给浏览器，最后再把后端返回的结果展示出来。
   - 整个过程中页面没有刷新，只是刷新页面中的局部位置而已！
   - 当请求发出后，浏览器还可以进行其他操作，无需等待服务器的响应！
   - 当输入用户名后，把光标移动到其他表单项上时，浏览器会使用AJAX技术向服务器发出请求，服务器会查询名为lemontree7777777的用户是否存在

##### 4. jquery 实现ajax请求

​	**注意 : 对于发送给服务器的data数据中,若值不是字符串形式,需要将其转换成字符串类型。**

```python
 data:{"i1":$("#i1").val(),"i2":$("#i2").val(),"hehe": JSON.stringify([1, 2, 3])}

```

```html
先导入jquery.js
$.ajax({
    url: '/calc/',
    type: 'post',
    data: {
        a: $("[name='i1']").val(),
        b: $("[name='i2']").val(),
    },
    success: function (res) {   --->成功接收请求,res就是就收到的项响应体
        $("[name='i3']").val(res)
    },
    error:function (error) {  -->接受失败了的错误信息
        console.log(error)
    }
})

<--ajax上传文件的实现-->
<input type="file" id="f1">
<button id="b1">上传</button>

$('#b1').click(function () {
        var  formobj =  new FormData();
        formobj.append('file',document.getElementById('f1').files[0]);
        // formobj.append('file',$('#f1')[0].files[0]); //添加键值对,值为该文件对象
        formobj.append('name','alex');

        $.ajax({
            url: '/upload/',
            type: 'post',
            data:formobj ,
            processData:false,  // 告诉jQuery不要去处理发送的数据
            contentType:false,  // 告诉jQuery不要去设置Content-Type请求头
            success: function (data) {
                console.log(data)
            },
        })
    })  

    
    //py文件
 def upload(request):
    print(request.FILES)  -->file
    print(request.POST)  -->name
    return render(request,'upload.html')   

```

##### 5.ajax 通过crsf 的校验

1. 前提条件 ：确保有csrftoken的cookie

   ​	1. 在页面中使用{%  crsf_token%}

   ​	2. 加装饰器ensure_crsf_cookie

   ```python
   from django.views.decorators.csrf import csrf_exempt,csrf_protect,  ensure_csrf_cookie
   
   ```

2. 给data中添加csrfmiddlewaretoken的值,在ajax请求中

   ```python
   data: {
       'csrfmiddlewaretoken':$('[name="csrfmiddlewaretoken"]').val(),
       a: $("[name='i1']").val(),
       b: $("[name='i2']").val(),
   },
   
   ```

3. **或者 --> :** 加请求头

   ```python
   headers:{
       'x-csrftoken':$('[name="csrfmiddlewaretoken"]').val(),
   },
   
   ```

4. **或者 --> :** 使用文件

   ```html
   //ajax_setup.js文件
   //自己写一个getCOOKIE方法
   function getCookie(name) {
       var cookieValue = null;
       if (document.cookie && document.cookie !== '') {
           var cookies = document.cookie.split(';');
           for (var i = 0; i < cookies.length; i++) {
               var cookie = jQuery.trim(cookies[i]);
               // Does this cookie string begin with the name we want?
               if (cookie.substring(0, name.length + 1) === (name + '=')) {
                   cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                   break;
               }
           }
       }
       return cookieValue;
   }
   var csrftoken = getCookie('csrftoken');
   
   //每一次都这么写太麻烦了，可以使用$.ajaxSetup()方法为ajax请求统一设置。
   function csrfSafeMethod(method) {
     // these HTTP methods do not require CSRF protection
     return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
   }
   
   $.ajaxSetup({
     beforeSend: function (xhr, settings) {
       if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
         xhr.setRequestHeader("X-CSRFToken", csrftoken);
       }
     }
   });
   
   ```

   **注意: ** 使用从cookie中取csrftoken的方式，需要确保cookie存在csrftoken值。

   如果你的视图渲染的HTML文件中没有包含 {% csrf_token %}，Django可能不会设置CSRFtoken的cookie。这个时候需要使用ensure_csrf_cookie()装饰器强制设置Cookie。

   ```python
   django.views.decorators.csrf import ensure_csrf_cookie
   
   
   @ensure_csrf_cookie
   def login(request):
       pass
   
   ```

##### 6. ajax总结

1. 使用jq技术发送ajax请求

2. 导入jquery

3. ```python
   $.ajax({
   	url：发送的地址，
   	type:   请求方式，
   	data:  {},   
   	successs: funcation(res){    res  ——>响应体
   	}
   })
   
   或者使用
   success:(res)=>{
       函数体
   }
   # 上传文件时
   
   ```

   

### 12.11 form 组件

##### 1.form组件的功能

1. 生产input标签
2. 对提交的数据可以进行校验
3. 提供错误提示

##### 2.定义form 组件

```python
# 在view视图py文件中(一般)
from django import forms

class RegForm(forms.Form):
    username = forms.CharField()
    pwd = forms.CharField()

```

##### 3. 使用

```python
# 视图 通过form组件实现注册方式
def register2(request):
    form_obj = RegForm()
    if request.method == "POST":
        form_obj = RegForm(request.POST)
        if form_obj.is_valid():  # 对数据进行校验
        	print(form_obj.cleaned_data)  # 通过校验的数据
            print("注册成功")
            return HttpResponse("注册成功")
return render(request, 'reg2.html', {'form_obj': form_obj})
```

```html
<?模板?>
{{ form_obj.as_p }}    __>   生产一个P标签  input  label,都被包裹在里面
{{ form_obj.errors }}    ——》   form表单中所有字段的错误
{{ form_obj.username }}     ——》 一个字段对应的input框,只有input框,没有label
{{ form_obj.username.label }}    ——》  该字段的中午提示
{{ form_obj.username.id_for_label }}    ——》 该字段input框的id
{{ form_obj.username.errors }}   ——》 该字段的所有的错误
{{ form_obj.username.errors.0 }}   ——》 该字段的第一个错误的错误

```

##### 4. 常用字段及其参数

创建form类时,主要涉及到 【字段】 和 【插件】，字段用于对用户请求数据的验证，插件用于自动生成HTML;

```python
# 常用字段
Field
    required=True,               是否允许为空
    widget=None,                 HTML插件
    label=None,                  用于生成Label标签或显示内容
    initial=None,                初始值
    help_text='',                帮助信息(在标签旁边显示)
    error_messages=None,         错误信息 {'required': '不能为空', 'invalid': '格式错误'}
    validators=[],               自定义验证规则
    localize=False,              是否支持本地化
    disabled=False,              是否可以编辑
    label_suffix=None            Label内容后缀


CharField(Field)
	max_length=None,             最大长度
    min_length=None,             最小长度
    strip=True                   是否移除用户输入空白
    
    
    
ChoiceField(Field)
    ...
    choices=(),                选项，如：choices = ((0,'上海'),(1,'北京'),)
    required=True,             是否必填
    widget=None,               插件，默认select插件
    label=None,                Label内容
    initial=None,              初始值
    help_text='',              帮助提示
       
DecimalField(IntegerField)
    max_value=None,              最大值
    min_value=None,              最小值
    max_digits=None,             总长度
    decimal_places=None,         小数位长度

RegexField(CharField)
    regex,                      自定制正则表达式
    max_length=None,            最大长度
    min_length=None,            最小长度
    error_message=None,         忽略，错误信息使用 error_messages={'invalid': '...'}
#时间
BaseTemporalField(Field)
    input_formats=None          时间格式化   
 
DateField(BaseTemporalField)    格式：2015-09-01
TimeField(BaseTemporalField)    格式：11:12
DateTimeField(BaseTemporalField)格式：2015-09-01 11:12
 
DurationField(Field)            时间间隔：%d %H:%M:%S.%f
    

MultipleChoiceField

IntegerField(Field)
    max_value=None,              最大值
    min_value=None,              最小值

# 字段参数
required=True,               是否允许为空
widget=None,                 HTML插件
label=None,                  用于生成Label标签或显示内容
initial=None,                初始值,input的默认值
error_messages=None,         错误信息 {'required': '不能为空', 'invalid': '格式错误'}
validators=[],               自定义验证规则,里面放函数名
disabled=False,              是否可以编辑

# eg:
class LoginForm(forms.Form):
    username = forms.CharField(
        min_length=8,   # 设置最小长度6
        label="用户名",  # 设置标签,不设置默认是字段名
        initial="张三"    # 设置默认值
        error_messages={
            "required": "不能为空",
            "invalid": "格式错误",
            "min_length": "用户名最短8位"
        } # 重写错误信息
    )
    pwd = forms.CharField(min_length=6,
                          label="密码",
                          widget=forms.PasswordInput)
    re_pwd = forms.CharField(label='确认密码', widget=forms.PasswordInput)
    gender = forms.ChoiceField(    # 单选框
        choices=((1, '男'), (2, '女')),
        widget=forms.RadioSelect)
    hobby = forms.ModelMultipleChoiceField(  # 多选框
        # choices=((1, "篮球"), (2, "足球"), (3, "双色球"),),爱好是固定的
        queryset=models.Hobby.objects.all(),  # 从数据库取数据,可以实时更新
    )
    phone = forms.CharField(
        validators=[RegexValidator(r'^1[3-9]\d{9}$', '手机号格式不正确')]
    )
    
    # 注意 :  
```

##### 5.验证

1. 内置验证

   ```python
   required=True
   min_length
   max_length
   
   ```

2. 自定义验证器

   ```python
   1.写函数
   def checkname(value):
       # 通过校验规则 不做任何操作
       # 不通过校验规则   抛出异常
       if 'alex' in value:
           raise ValidationError('不符合社会主义核心价值观')
   2.使用内置的校验器
   from django.core.validators import RegexValidator
   
   ```

3. 钩子函数

   ```python
   1.局部钩子
   def clean_字段名(self):
       # 局部钩子
       # 通过校验规则  必须返回当前字段的值
       # 不通过校验规则   抛出异常
       raise ValidationError('不符合社会主义核心价值观')
       
   2.全局钩子 -->可校验2次输入密码是否一致
   def clean(self):
       # 全局钩子
       # 通过校验规则  必须返回当前所有字段的值
       # 不通过校验规则   抛出异常   '__all__'
       # self.add_error('re_pwd','两次密码不一致!!!!!')
       pass
   
   ```

4. is_valid 的流程

   ```python
   1.执行full_clean()的方法：
   定义错误字典
   定义存放清洗数据的字典
   
   2.执行_clean_fields方法：
   循环所有的字段
   获取当前的值
   对字段进行校验 （ 内置校验规则   自定义校验器,反射局部钩子执行）
   	1. 通过校验
      		self.cleaned_data[name] = value 
      		如果有局部钩子，执行进行校验：
      		1.通过校验——》 self.cleaned_data[name] = value 
      		2.不通过校验——》self._errors  添加当前字段的错误,并且self.cleaned_data中当前字段的值删除掉
   
      2. 没有通过校验
      self._errors  添加当前字段的错误
   3.执行全局钩子clean方法
   
   ```

##### 6. 使用 form 和model终极结合

1. 使forms.modelForm 代替 forms.Form

   ```python
   class BookForm(forms.ModelForm):
       
   
       class Meta:
           model = models.Book
           fields = "__all__"
           exclude = ['username']  # 排除的字段的列表
           labels = {
               "title": "书名",
               "price": "价格"
           }
           widgets = {
               "password": forms.widgets.PasswordInput(attrs={"class": "c1"}),
                'mobile': forms.TextInput(attrs={'placeholder': '您的手机号', 'autocomplete': 'off', 'class': 'login_txtbx'}),
               'name': forms.TextInput(attrs={'placeholder': '您的真实姓名', 'autocomplete': 'off', 'class': 'login_txtbx'}),
               'department': forms.Select(attrs={'class': 'selectinput'})
   
           }
           error_messages = {
               'username': {
                   'required': '必填',
                   'invalid': '邮箱格式不正确'
               } #自定义错误信息
   ```

   





### 12.12 dajngo的admin组件

```python
python manage.py createsuperuser
键入 username 和 pwd

#在app下的admin.py下

```







### 12.3.8 导入js/css的路径问题

- **`./`**：代表目前所在的目录
- **`. ./`**：代表上一层目录
- **`/`**：代表根目录**

1. 案例解析

   ```html
   1.文件在当前目录下,以当前项目文件为中心
   “./example.jpg” 或 “example.jpg”
   
   2.文件在上层目录
   （1）在上层目录下
   “../example.jpg”
   
   （2）在上层目录下的一个Image文件夹下
   “../Image/example.jpg”
   
   （3）在上上层目录下
   "../../example.jpg"
   
   3、文件在下一层目录
   “./Image/example.jpg”
   
   4、根目录
   “C:/Image/example.jpg”
   
   ```



## 第十三章异常处理

### 13.1基本格式

```python
try:         # 试运行try缩进的语句,若出错,运行except Exception as e:后面的语句,避免程序出错.
    val = input('请输入数字:')
    num = int(val) 
except Exception as e:
    print('操作异常') 
#################################################################
try:
    v = []
    v[11111] # IndexError
except ValueError as e: # 小弟先上场捕捉错误,捕捉成功则执行这个语句
    pass
except IndexError as e:
    pass
except Exception as e:  # 最nb的boss,
    print(e)   # # e是Exception类的对象，中有一个错误信息。
    # 或者做其他事
```

### 13.2 finally 语句

```python
try:
    int('邓益新')
except Exception as e:
    print(e)   # # e是Exception类的对象，中有一个错误信息。
    # 或者做其他事
finally:
    print('无论语句运行是否有错,finally语句都会执行')
    

###################t  特殊情况  ###########################

# 在函数中return之后,有finally语句,则会执行finally语句,然后再return

def func():
    print('函数开始执行')
    try:
        v = []
        print(v[5])
        return 0
    except Exception as e:
        print(e)
        return   # 这里函数应该已经终止,但finally 语句会执行
    finally :
        for i in '123':
            print(i)

```

### 13.3 主动触发异常

```python
try:
    int('123')
    raise Exception('阿萨大大是阿斯蒂') # 代码中主动抛出异常
except Exception as e:
    print(e)
    
    
    ######################
def func():
    result = True
    try:
        with open('x.log',mode='r',encoding='utf-8') as f:
            data = f.read()
        if 'alex' not in data:
            raise Exception()
    except Exception as e:
        result = False
    return result
```

### 13.4自定义异常

```python
class MyException(Exception):   # 继承Exception
    pass

try:
    raise MyException('asdf')
except MyException as e:
    print(e)
```

```python
class MyException(Exception):
    def __init__(self,message):
        super().__init__()
        self.message = message

try:
    raise MyException('asdf')
except MyException as e:
    print(e.message)
```



## 附录 常见错误和单词

### 

### 错误记录

1. 编码错误unicode/decode error![截图python0408164437](Python学习笔记.assets/截图python0408164437.png)

注意: 有\x,表面他是十六进制,因为二机制太长,用十六进制表达德更多.

2. 参数位置错误

   ![1555753962628](Python学习笔记.assets/1555753962628.png)

## 附录:邮箱发送示例

![1555753477758](Python学习笔记.assets/1555753477758.png)

![1555753491522](Python学习笔记.assets/1555753491522.png)

![1555753505290](Python学习笔记.assets/1555753505290.png)

![1555753528586](Python学习笔记.assets/1555753528586.png)





~~~python
###假如：管理员/业务员/老板用的是同一个邮箱。# #
def send_email(to):
	import smtplib
    from email.mime.text import MIMEText
    from email.utils import formataddr
msg = MIMEText('导演，我想演男一号，你想怎么着都行。', 'plain', 'utf-8')
msg['From'] = formataddr(["李邵奇", '15776556369@163.com'])
msg['To'] = formataddr(["导演", to])
msg['Subject'] = "情爱的导演"

server = smtplib.SMTP("smtp.163.com", 25)
server.login("15776556369@163.com", "qq1105400511")
server.sendmail('15776556369@163.com', [to, ], msg.as_string())
server.quit()


"""
def send_email(to):
    template = "要给%s发送邮件" %(to,)
    print(template)



user_input = input('请输入角色：')

if user_input == '管理员':
    send_email('xxxx@qq.com')
elif user_input == '业务员':
    send_email('xxxxo@qq.com')
elif user_input == '老板':
    send_email('xoxox@qq.com')
        ```
~~~


