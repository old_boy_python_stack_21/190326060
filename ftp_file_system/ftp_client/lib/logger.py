#!/usr/bin/env python        	
# -*- coding:utf-8 -*-	 
#@Project  : s21day30 
#@Author : Jack Deng       
#@Time   :  2019-05-09 19:25
#@File   : logger.py

import logging
from conf import settingclient
from logging import handlers



logger = logging.Logger('ftp_sever' , level=logging.ERROR)
fh = handlers.TimedRotatingFileHandler(when= 'd', filename= settingclient.LOG_PATH, interval=1 , encoding='utf-8')
sh = logging.StreamHandler()
fmt = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                        datefmt = '%Y-%m-%d %H-%M-%S',)
sh.setFormatter(fmt)
fh.setFormatter(fmt)
logger.addHandler(fh)
logger.addHandler(sh)
