#!/usr/bin/env python        	
# -*- coding:utf-8 -*-	 
# @Project  : s21day30
# @Author : Jack Deng
# @Time   :  2019-05-09 18:07
# @File   : client.py

import os
import json
import socket
import struct
import hashlib
from lib.processbar import processBar
from conf.settingclient import DEFAULTPATH , CLIENT_CONNECT



class Client:

    def __init__(self):
        sk = socket.socket()
        sk.connect(CLIENT_CONNECT)
        self.sk = sk
        self.info = {"login": self.login,
                     'register': self.register,
                     'download': self.download,
                     'upload': self.upload,
                     'ls': self.ls,
                     'chdir': self.chdir,
                     'del_free_dir': self.del_free_dir,
                     'create_dir': self.create_dir}
        self.current_user = {'result': '0', 'color': '%s', 'status': None}
        self.color = self.current_user['color']

    def my_send(self, data_dic):
        """
        自定义协议发信息
        :param data_dic:
        :return:
        """
        str_dic = json.dumps(data_dic).encode('utf-8')  # 字典转为的字节
        len_dic = struct.pack('i', len(str_dic))  # 4个字节
        self.sk.send(len_dic)
        self.sk.send(str_dic)

    def my_recv(self):
        """
        自定义协议收信息
        :return: data_dic
        """
        len_dic = self.sk.recv(4)  # 接受4个字节
        len_dic = struct.unpack('i', len_dic)[0]  # 获取接下来接受的字典的指定长度
        str_dic = self.sk.recv(len_dic)  # 接受指定长度的字节('utf-8')
        data_dic = json.loads(str_dic.decode('utf-8'))
        return data_dic

    def upload(self):
        """
        文件上传
        :return:
        """
        if not self.current_user['status']:
            print(self.color % '请先登录!!!!'.center(40, '*'))
            return
        file_path = input(self.color % '输入文件路径>>>')
        if os.path.isabs(file_path):
            if os.path.isfile(file_path):
                filename = os.path.basename(file_path)  # 文件名
                filesize = os.path.getsize(file_path)  # 文件大小
                data_dic = {
                    'operate': 'upload',
                    'filename': filename,
                    'filesize': filesize}
                self.my_send(data_dic)
                data_dic = self.my_recv()
                if data_dic['filecount'] == filesize:
                    print('您网盘中已有该文件,不用上传')
                    return
                else:
                    filecount = data_dic['filecount']
                with open(file_path, mode='rb') as f:
                    f.seek(0, filecount)
                    filesize = filesize - filecount
                    while filesize > 0:
                        content = f.read(2048)
                        self.sk.send(content)
                        filecount += len(content)
                        processBar(
                            filecount, filesize, self.current_user['color'])
                print(self.color % '上传%s完成' % filename)
                return
        print(self.color % '文件路径错误,请重试')

    def download(self):
        """
        文件下载
        :return:
        """
        if not self.current_user['status']:
            print(self.color % '请先登录!!!!'.center(40, '*'))
            return
        filename = input(self.color % '请输入要下载的文件>>>')
        filepath = os.path.join(DEFAULTPATH, filename)
        if os.path.exists(filepath):
            filecount = os.path.getsize(filepath)
        else:
            filecount = 0
        data_dic = {
            'operate': 'download',
            'filename': filename,
            'filecount': filecount}
        self.my_send(data_dic)
        data_dic = self.my_recv()
        if data_dic['result'] == '40':
            print(self.color % '文件不存在,请重新输入')
            return
        elif data_dic['result'] == '42':
            print(self.color % '文件已下载完成,不用重复下载')
            return
        t = data_dic['filecount']
        with open(filepath, 'wb') as f:
            file_md5 = hashlib.md5()
            total = data_dic['filesize']
            while data_dic['filesize'] > 2048:
                content = self.sk.recv(2048)
                file_md5.update(content)
                f.write(content)
                processBar(
                    total - data_dic['filesize'],
                    total,
                    self.current_user['color'])
                data_dic['filesize'] -= len(content)
            else:
                while data_dic['filesize']:
                    content = self.sk.recv(data_dic['filesize'])
                    file_md5.update(content)
                    f.write(content)
                    processBar(
                        total - data_dic['filesize'],
                        total,
                        self.current_user['color'])
                    data_dic['filesize'] -= len(content)
            data_dic = self.my_recv()
            file_md5 = file_md5.hexdigest()
        if data_dic['file_md5'] == file_md5:
            processBar(100, 100, self.current_user['color'])
            print(self.color % '>>>>>>下载%s完成<<<<<<' % filename)
        else:
            print(self.color % '网络波动,文件受损,请稍后再下载')

    def login(self):
        """
        用户登录
        :return:
        """
        if self.current_user['status']:
            print(self.color % '无需重复登录')
            return
        while True:
            print(self.color % '***********欢迎登录***********')
            user = input(self.color % '请输入QQ号(输入Q返回上一级)')
            if user.upper() == 'Q':
                return
            pwd = input(self.color % '请输入密码')  # 明文
            data_dic = {'operate': 'login', 'QQ': user, 'pwd': pwd}
            self.my_send(data_dic)
            data_dic = self.my_recv()
            if data_dic['result'] == '01':
                self.current_user.update(data_dic)
                self.current_user.update({'status': '1'})
                self.color = self.current_user['color']
                del self.info['login']
                del self.info['register']
                print(self.color % '登录成功')
                return
            else:
                print(self.color % '用户名或者密码错误,请重新登陆')

    def register(self):
        """
        用户注册
        :return:
        """
        if self.current_user['status']:
            print(self.color % '你已经登录')
            return
        while True:
            print('欢迎注册'.center(40, '*'))
            name = input('请输入您的昵称(注册成功分配QQ号,输入Q返回上一级)')
            if name.upper() == 'Q': return
            pwd = input('请输入密码').strip()
            new_pwd = input('请再次输入密码').strip()
            if pwd != new_pwd:
                print('两次密码错误,请重新注册')
                continue
            data_dic = {'operate': 'register', 'pwd': pwd, 'name': name}
            self.my_send(data_dic)
            data_dic = self.my_recv()
            if data_dic['result'] == '10':
                print('你的昵称包含敏感字符,请重新注册')
            else:
                print('您已注册成功,QQ号为   %s  ,请注意保存' % data_dic['QQ'])
                return

    def del_free_dir(self):
        """
        删除所有空文件
        :return:
        """
        if not self.current_user['status']: print(self.color % '请先登录!!!!'.center(40, '*')); return
        print('这是咋整')
        data_dic = {'operate': 'del_free_dir', }
        self.my_send(data_dic)
        data_dic = self.my_recv()
        if data_dic['result'] == '21':
            print(self.color % '删除成功')
        else:
            print('删除失败,请稍候再试')

    def create_dir(self):
        """
        创建目录
        :return:
        """
        if not self.current_user['status']: print(self.color % '请先登录!!!!'.center(40, '*')); return
        create_dir = input('请输入建立的目录,以&隔开')
        for item in ["/", "\\", " "]:
            if item in create_dir: print(self.color % '目录名不能包含"/","\\"," "'); return
        data_dic = {'operate': 'create_dir', 'creat_dir': create_dir}
        self.my_send(data_dic)
        data_dic = self.my_recv()
        if data_dic['result'] == '31':
            print(self.color % '创建成功')
        else:
            print(self.color % '创建失败')

    def ls(self):
        """
        查看网盘目录
        :return:
        """
        if not self.current_user['status']:
            print(self.color % '请先登录!!!!'.center(40, '*'))
            return
        data_dic = {'operate': 'ls', 'dict': None}
        low_dir = ''
        while True:
            self.my_send(data_dic)
            data_dic = self.my_recv()
            for catalog in data_dic['ls']['128196']:
                print(chr(int('128193')), catalog)
            for file in data_dic['ls']['128193']:
                print(chr(int('128196')), file)
            catalog = input(self.color % '请输入下级目录来查看(回车则退出)')
            if catalog == '':
                return
            elif catalog not in data_dic['ls']['128196']:
                print(self.color % '输入错误,目录不存在')
                return
            low_dir = low_dir + '/' + catalog
            data_dic.update({'dict': low_dir})

    def change_pwd(self):
        """
        更改密码
        :return:
        """
        if self.current_user['result'] == '0': print(self.color % '请先登录!!!!'.center(40, '*')); return
        while True:
            print(self.color % '=============欢迎使用更改密码服务=============')
            pwd = input(self.color % '>>>请输入原密码(输入Q退出)>>>')
            if pwd.upper() == 'Q': return
            new_pwd = input(self.color % '>>>请输入新密码>>>')
            new_pwd2 = input(self.color % '>>>请再次输入新密码>>>')
            if not new_pwd == new_pwd2: print(self.color % '2次密码输入不一致,请重新输入'); continue
            data_dic = {'operate': 'change_pwd', 'new_pwd': new_pwd, 'pwd': pwd}
            self.my_send(data_dic)
            data_dic = self.my_recv()

    def chdir(self):
        """切换子目录"""
        if not self.current_user['status']:
            print(self.color % '请先登录!!!!'.center(40, '*'))
            return
        chdir = input(self.color % '请指定切换的子目录')
        for item in ["/", "\\", " "]:
            if item in chdir.upper(): print(self.color % '目录名不能包含"/","\\"," "'); return
        data_dic = {'operate': 'chdir', 'chdir': chdir.upper()}
        self.my_send(data_dic)
        data_dic = self.my_recv()
        if data_dic['result'] == '41':
            print(self.color % '切换成功')
        else:
            print(self.color % '切换失败,目录不存在')

    def run(self):
        print(self.current_user['color'] % "************请选择功能***************")
        if not os.path.exists(DEFAULTPATH):
            os.makedirs(DEFAULTPATH)
        while True:
            for item in self.info:
                print(self.color % item)
            choice = input(self.color % '请输入功能(输入Q退出)>>>')
            if choice.strip().upper() == 'Q':
                break
            elif hasattr(self, choice.strip()):
                getattr(self, choice.strip())()
            else:
                print(self.color % '输入错误,请重新输入')
                continue
        self.sk.close()


def run():
    obj = Client()
    obj.run()