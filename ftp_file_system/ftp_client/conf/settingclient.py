#!/usr/bin/env python        	
# -*- coding:utf-8 -*-	 
#@Project  : s21day30 
#@Author : Jack Deng       
#@Time   :  2019-05-09 19:24
#@File   : settings.py

import sys
import os
BASE_PATH = os.path.dirname(os.path.dirname(__file__))
sys.path.append(BASE_PATH)
LOG_PATH= os.path.join(BASE_PATH,'log','ftp_client.log')
DEFAULTPATH = os.path.join(BASE_PATH,'QQdownload')
CLIENT_CONNECT = ('127.0.0.1', 8888)