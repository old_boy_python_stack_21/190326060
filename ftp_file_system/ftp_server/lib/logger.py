#!/usr/bin/env python        	
# -*- coding:utf-8 -*-	 
#@Project  : s21day30 
#@Author : Jack Deng       
#@Time   :  2019-05-09 19:25
#@File   : logger.py

import logging
from conf import settings
from  logging import handlers



logger = logging.Logger('ftp_sever' , level=logging.ERROR)
fh = handlers.TimedRotatingFileHandler(when=settings.TIME_LOG['when'],
                                       filename=settings.LOG_PATH ,
                                       interval=settings.TIME_LOG['interval'] ,
                                       encoding=settings.TIME_LOG['encoding'])
sh = logging.StreamHandler()
fmt = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                        datefmt = '%Y-%m-%d %H-%M-%S',)

fh.setFormatter(fmt)
sh.setFormatter(fmt)
logger.addHandler(fh)
logger.addHandler(sh)
logger.error('14142727827')