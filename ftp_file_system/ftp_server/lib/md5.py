#!/usr/bin/env python        	
# -*- coding:utf-8 -*-	 
# @Project  : 选课系统
# @Author : Jack Deng
# @Time   :  2019-04-30 15:29
# @File   : md5.py

import hashlib
import json
import pickle
import random


def pwd_md5( qq , pwd  ):
    md5 = hashlib.md5(qq.encode('utf-8'))
    md5.update(pwd.encode('utf-8'))
    return md5.hexdigest()
