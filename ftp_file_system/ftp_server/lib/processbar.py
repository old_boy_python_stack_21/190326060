#!/usr/bin/env python        	
# -*- coding:utf-8 -*-	 
#@Project  : ftp_file_system 
#@Author : Jack Deng       
#@Time   :  2019-05-10 17:17
#@File   : proprecess.py


def processBar(num, total,color ):
    rate = num / total
    rate_num = int(rate * 100)
    if rate_num == 100:
        r =color %'\r%s>%d%%\n' % ('=' * rate_num, rate_num,)
    else:
        r =color %'\r%s>%d%%' % ('=' * rate_num, rate_num,)
    print(r, end='')
    # sys.stdout.write(r)
    # sys.stdout.flush