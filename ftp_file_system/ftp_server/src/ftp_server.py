#!/usr/bin/env python        	
# -*- coding:utf-8 -*-	 


import os
import sys
import json
import shutil
import struct
import random
import socketserver
import hashlib
import openpyxl
from openpyxl import load_workbook
from lib.md5 import pwd_md5
from conf import settings
from lib.logger import logger


def login(data_dic):
    """
    登录程序{'operate':'login' ,'QQ':用户的QQ号,'pwd':用户的密码明文}
    :param data_dic
    :return:
    """
    f = open(settings.DBDATA, 'r', encoding='utf-8')
    qq = data_dic['QQ']
    pwd = data_dic['pwd']
    for line in f:
        line = line.strip().split('|')
        if qq == line[0] and pwd_md5(line[0], pwd) == line[2]:
            data_dic = {'operate': 'login', 'QQ': qq, 'name': line[1], 'result': '01', 'color': line[3]}
            break
    else:
        data_dic = {'QQ': qq, 'result': '00', 'color': None, }
    return data_dic


def login2(data_dic):
    """用xlsx存储数据"""
    wb = load_workbook(settings.DBDATA_XLSX)
    ws = wb['user_data']
    for item in ws:
        if item[0].value == data_dic['QQ'] and \
                item[2].value == pwd_md5(data_dic['QQ'], data_dic['pwd']):
            data_dic = {'operate': 'login', 'QQ': data_dic['QQ'],
                        'name': item[1].value, 'result': '01',
                        'color': '\033' + item[3].value, }
            break
    else:
        data_dic = {'QQ': data_dic['QQ'], 'result': '00', 'color': '', }
    return data_dic




def register(data_dic):
    """
    注册功能{'operate': 'register' , 'pwd' : 密码 ,'name':昵称}
    :param data_dic
    :return:
    """
    for item in settings.SENDITIVE_STR:
        if item in data_dic['name']:
            data_dic = {'operate': 'register', 'name': data_dic['name'], 'result': '10'}
            return data_dic
    new_qq = get_new_qq()
    pwd = pwd_md5(new_qq, data_dic['pwd'])
    name = data_dic['name']
    color = '[1;%sm %%s[0m' % random.choice([x for x in range(30, 38)])
    line = '|'.join([new_qq, name, pwd, color]) + '\n'
    with open(settings.DBDATA, 'a', encoding='utf-8') as f:
        f.write(line)
    data_dic = {'operate': 'register', 'name': name, 'result': '11', 'QQ': new_qq}
    return data_dic


def get_new_qq():
    """
    获取最新的没人用的qq号,给注册者使用
    :param:
    :return:
    """
    with open(settings.DBDATA, 'rb') as f:
        off = -100
        while True:
            f.seek(off, 2)
            val = f.readlines()
            if len(val) >= 2:
                last_line = val[-1]
                break
            off *= 2
        return str((int(last_line.decode('utf-8').split('|')[0])) + 1)


def register2(data_dic):
    """
    注册功能{'operate': 'register' , 'pwd' : 密码 ,'name':昵称}
    :param data_dic:
    :return:
    """
    wb = load_workbook(settings.DBDATA_XLSX)
    ws = wb['user_data']
    last_qq = int(ws[ws.max_row][0].value)
    new_qq = str(last_qq + 1)
    pwd = pwd_md5(new_qq, data_dic['pwd'])
    name = data_dic['name']
    color = '[1;%sm %%s [0m' % random.choice([x for x in range(30, 38)])
    ttt = ws.max_row + 1
    ws['A%s' % str(ttt)].value = new_qq
    ws['B%s' % str(ttt)].value = name
    ws['C%s' % str(ttt)].value = pwd
    ws['D%s' % str(ttt)].value = color
    wb.save(settings.DBDATA_XLSX)
    data_dic = {
        'operate': 'register',
        'name': name,
        'result': '11',
        'QQ': new_qq}
    return data_dic


class My_Server(socketserver.BaseRequestHandler):

    def __my_send(self, data_dic):
        """
        自定义协议发信息
        :param data_dic:
        :return:
        """
        str_dic = json.dumps(data_dic).encode('utf-8')  # 字典转为的字节
        len_dic = struct.pack('i', len(str_dic))  # 4个字节
        self.request.send(len_dic)
        self.request.send(str_dic)

    def __my_recv(self):
        """
        自定义协议收信息
        :param
        :return: data_dic
        """
        len_dic = self.request.recv(4)  # 接受4个字节
        len_dic = struct.unpack('i', len_dic)[0]  # 获取接下来接受的字典的指定长度
        str_dic = self.request.recv(len_dic)  # 接受指定长度的字节('utf-8')
        data_dic = json.loads(str_dic.decode('utf-8'))
        return data_dic

    def __send_file(self, filepath, filesize, filecount):
        with open(filepath, 'rb', ) as f:
            file_md5 = hashlib.md5()
            while filesize > 0:
                content = f.read(2048)
                self.request.send(content)
                file_md5.update(content)
                filesize -= len(content)
            return file_md5.hexdigest()

    def __recv_file(self, filepath, filesize, filecount):
        with open(filepath, 'ab') as f:
            filesize = filesize - filecount
            while filesize > 2048:
                content = self.request.recv(2048)
                f.write(content)
                filesize -= len(content)
            else:
                while filesize:
                    content = self.request.recv(filesize)
                    f.write(content)
                    filesize -= len(content)

    def handle(self):
        while True:
            data_dic = self.__my_recv()
            if hasattr(sys.modules[__name__], data_dic['operate']):
                data_dic = getattr(sys.modules[__name__], data_dic['operate'])(data_dic)
                logger.error((self.client_address,data_dic['operate'],data_dic['result']))
                self.__my_send(data_dic)
                if data_dic['result'] == '01':   # 只有登录成功才能退出循环,注册完必须登录一次
                    self.data_dic = data_dic
                    self.id = data_dic['QQ']
                    self.base_path = os.path.join(settings.LOCALPATH, self.id)
                    if not os.path.exists(self.base_path):
                        os.makedirs(self.base_path)
                    break
            else:
                data_dic = {'operate': None}
                self.__my_send(data_dic)
        while True:
            data_dic = self.__my_recv()
            self.data_dic.update(data_dic)
            if hasattr(self, data_dic['operate']):
                getattr(self, data_dic['operate'].strip())()
                logger.error((self.client_address,data_dic))


    def upload(self):
        """
        从客户端接受文件
        :return:
        """
        filepath = self.base_path
        filepath = os.path.join(filepath, self.data_dic['filename'])
        filesize = self.data_dic['filesize']
        if os.path.exists(filepath):
            if os.path.getsize(filepath) == filesize:
                filecount = filesize
                self.__my_send(self.data_dic)
                return
            else:
                filecount = filesize - os.path.getsize(filepath)
        else:
            filecount = 0
        self.data_dic.update({'filecount': filecount})
        self.__my_send(self.data_dic)
        self.__recv_file(filepath, filesize, filecount)

    def download(self):
        """
        发送文件给 客户端
        :return :
        """
        filename = self.data_dic['filename']
        filepath = os.path.join(
            settings.LOCALPATH,
            self.id,
            filename)  # 用户1821333144\(绝对目录)
        if os.path.exists(filepath) and os.path.isfile(filepath):
            filesize = os.path.getsize(filepath)
            if filesize == self.data_dic['filecount']:
                self.data_dic.update({'result': '42'})
                return
            self.data_dic.update(
                {'filesize': filesize, 'result': '41', 'filecount': self.data_dic['filecount']})
            self.__my_send(self.data_dic)
            file_md5 = self.__send_file(
                filepath, filesize, self.data_dic['filecount'])
            self.data_dic.update({'file_md5': file_md5})
        else:
            self.data_dic.update({'result': '40'})
        self.__my_send(self.data_dic)

    def ls(self):
        path = os.path.join(settings.LOCALPATH, self.id)  # 以local\182133144\文件名
        if not self.data_dic['dict']:
            pass
        else:
            line = self.data_dic['dict'].split('/')
            catalog_tuple = tuple(line)
            path = os.path.join(path, *catalog_tuple)
        lstdir = os.listdir(path)
        lst = {128193: [], 128196: []}
        for item in lstdir:
            path2 = os.path.join(path, item)
            if os.path.isfile(path2):
                lst[128193].append(item)
            else:
                lst[128196].append(item)
        self.data_dic.update({'ls': lst})
        self.__my_send(self.data_dic)

    def del_free_dir(self):
        """
        删除所有空文件
        :return:
        """
        print('删除空文件')
        lst_dir = [self.base_path]
        while lst_dir:
            path = lst_dir.pop()
            list_dir = os.listdir(path)
            for item in list_dir:
                abs_path = os.path.join(path, item)
                if os.path.isdir(abs_path):
                    if not os.listdir(abs_path):
                        shutil.rmtree(abs_path)
                    else:
                        lst_dir.append(abs_path)
        self.data_dic.update({'result': '21'})
        self.__my_send(self.data_dic)

    def create_dir(self):
        """
        在网盘创建空目录
        :return:
        """
        path = self.data_dic['creat_dir']
        if '&' in path:
            path = path.split('&')
            path = tuple(path)
        else:
            path = (path,)
        path = os.path.join(self.base_path, *path)
        try:
            os.makedirs(path)
            flag = '31'
        except:
            flag = '30'
        self.data_dic.update({'result': flag})
        self.__my_send(self.data_dic)

    def chdir(self):
        """
        切换到子目录
        :return:
        """
        chdir = self.data_dic['chdir']
        path = os.path.join(self.base_path, chdir)
        if not os.path.exists(path):
            self.data_dic.update({'result': '40'})
            self.__my_send(self.data_dic)
            return
        self.base_path = path
        os.chdir(path)
        # print(os.getcwd())
        self.data_dic.update({'result': '41'})
        self.__my_send(self.data_dic)

def server():
    server = socketserver.ThreadingTCPServer(('127.0.0.1', 8888), My_Server)
    server.serve_forever()