#!/usr/bin/env python        	
# -*- coding:utf-8 -*-	 
#@Project  : s21day30 
#@Author : Jack Deng       
#@Time   :  2019-05-09 19:45
#@File   : run.py
import sys
import os
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

from src import ftp_server

if __name__ == '__main__':
    ftp_server.server()