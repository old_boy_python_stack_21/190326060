#!/usr/bin/env python        	
# -*- coding:utf-8 -*-	 
#@Project  : s21day30 
#@Author : Jack Deng       
#@Time   :  2019-05-09 19:24
#@File   : settings.py

import sys
import os
BASE_PATH = os.path.dirname(os.path.dirname(__file__))
sys.path.append(BASE_PATH)
LOG_PATH= os.path.join(BASE_PATH,'log','ftp_server.log')
TIME_LOG = {'when':'s','filename': LOG_PATH ,'interval':1 ,'encoding':'utf-8'}
DBDATA = os.path.join(BASE_PATH,'db','user_data.txt')
LOCALPATH = os.path.join(BASE_PATH,'local')               # 文件路径
SENDITIVE_STR   = [' ','sb','ooxx','反清复明']
DBDATA_XLSX = os.path.join(BASE_PATH,'db','user_data.xlsx')
