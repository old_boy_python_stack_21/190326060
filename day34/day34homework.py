#!/usr/bin/env python        	
# -*- coding:utf-8 -*-	 
#@Project  : s21day34 
#@Author : Jack Deng       
#@Time   :  2019-05-15 19:17
#@File   : day34homework.py

# 1.生产者消费者模型是指通过一个容器来平衡生产者和消费者之间的强耦合问题,
# 生产者和消费者不直接进行通讯,而通过阻塞队列来进行通讯,生产者生产完数据
# 不等待,而是直接扔到阻塞队列,消费者也直接从阻塞队列取值,阻塞队列就是一个缓冲区,
# 平衡了生产者和消费者之间的处理问题.

# 2.cpython特殊的垃圾回收机制,引入了一个全局解释器锁,GIL锁,为了解决多线程之间数据
# 的完整性和状态同步的方法就是加锁,只允许拥有GIL锁餐能运行程序,这样每个线程在执行的时候
# 都需要获取GIL才能执行代码,这样保证同一时刻只有一个线程使用CPU,多线程只能并发,不能实现并行.

# 3.线程是进程的一部分,每个进程至少包含一个线程.
 # 进程是计算机最小的资源分配单位
 # 线程是能被计算机CPU调度的最小单位.
 # 线程创建,销毁,切换的开销远小于进程.
 # 理论上:进程和线程都能并行和并发 .

 # 4.
 # 多进程之间是数据隔离的,可以实现数据共享,通过multiprocessing包的Queue或者
# JoinableQueue模块实现数据共享,

# 5.已实现ftp需求5 ,正在断点续传.


# 生成器1
g1 = filter(lambda n:n%2==0,range(10))  # (0,2,4,6,8)
g2 = map(lambda m:m*2,range(3))
for i in g1:    # 对g1循环
    for j in g2:     # 对g2循环
        print(i,j)  # (0,0/2/4)   # (2,0/2/4)   ----.....(8,0/2/4)












## 生成器2
def multipliers():
    return [lambda x:i*x for i in range(4)]


tt = [m for m in multipliers()]              # 列表生成式,马上执行函数
# tt1 = [m(2)for m in [lambda x:i*x for i in range(4)]]
# tt2 = [m(2)for m in ['4 个  lambda x:i*x ,函数,内存地址不相同  ']]
# tt3 = [m(2),m(2),m(2),m(2),此时i=3]   ---> [6,6,6,6]

# 修改multipliers()为生成器

def multipliers():
    for i  in  range(4):
        yield  lambda x:i*x    # 生成器函数

mm = [m(2) for m in multipliers()] # 每次yield返回值是,i的值为0,1,2,3,x参数都为2,则
# mm =[0,2,4,6]
