## 第九章 并发编程

### 9.1 基础知识

#### 9.1.1 操作系统发展史

##### 9.1.1.1 人机矛盾

- cpu利用率低

##### 9.1.1.2 磁带存储+批处理

- 降低数据的读取时间
- 提高cpu利用率

##### 9.1.1.3 多道操作系统

- 数据隔离
- 时空复用
- 能够在一个任务遇到io操作时，主动把cpu让出来给其他任务使用。
- 任务切换过程也占用时间（操作系统来进行切换）

##### 9.1.1.4 分时操作系统

给时间分片，让多个任务轮流使用cpu

- 时间分片
  - cpu的轮转
  - 每个程序分配一个时间片
- 切换占用了时间，降低了cpu的利用率
- 提高了用户体验

##### 9.1.1.5 分时操作系统+多道操作系统

多个程序一起在计算机中执行

- 一个程序如果遇到了io操作，就切出去让出cpu
- 一个程序没有遇到io操作，但时间片到了，就让出cpu

补充：python中的分布式框架celery

#### 9.1.2 进程

##### 9.1.2.1 定义

运行中的程序就是一个进程。

##### 9.1.2.2 程序和进程的区别

程序：是一个文件。

进程：这个文件被cpu运行起来了

- 进程和程序之间的区别

  - 运行的程序就是一个进程

- 进程的调度 ：由操作系统完成

-  三状态 ：

  - 就绪 -system call-><-时间片到了- 运行 -io-> 阻塞-IO结束->就绪

  ​     阻塞 影响了程序运行的效率

##### 9.1.2.3 基础知识

- 进程是计算机中最小的资源分配单位
- 进程在操作系统中的唯一标识符：pid
- 操作系统调度进程的算法
  - 短作业优先算法
  - 先来先服务算法
  - 时间片轮转算法
  - 多级反馈算法

##### 9.1.2.4 并发和并行区别

并行：两个程序，两个cpu，每个程序分别占用一个cpu，自己执行自己的。

并发：多个程序，一个cpu，多个程序交替在一个cpu上执行，看起来在同时执行，但实际上仍然是串行。

#### 9.1.3 同步和异步

同步：一个动作执行时，要想执行下一个动作，必须将上一个动作停止。

异步：一个动作执行的同时，也可以执行另一个动作

#### 9.1.4 阻塞和非阻塞

阻塞：cpu不工作

非阻塞：cpu工作

同步阻塞：如conn.revc()

同步非阻塞：调用一个func()函数，函数内没有io操作

异步非阻塞：把func()函数(无io操作)放到其他任务里去执行，自己执行自己的任务。

异步阻塞：

####  9.1.5并发并行

- 并发 ：并发是指多个程序 公用一个cpu轮流使用
-  并行 ：多个程序 多个cpu 一个cpu上运行一个程序，
  - 在一个时间点上看，多个程序同时在多个cpu上运行

### 9.2 进程和线程

#### 9.2.1进程是计算机中最小的资源分配单位

- 进程是数据隔离的
  - 歪歪 陌陌 飞秋 qq 微信 腾讯视频
- 一个进程

​        1.和一个人通信2.一边缓存 一边看另一个电影的直播

#### 9.2.2进程

- 创建进程 时间开销大

- 销毁进程 时间开销大

- 进程之间切换 时间开销大

  ```python
  如果两个程序 分别要做两件事儿
      起两个进程
  如果是一个程序 要分别做两件事儿
      视频软件
          下载A电影
          下载B电影
          播放C电影
      启动三个进程来完成上面的三件事情，但是开销大
  ```

2. - 

#### 9.2.4进程模块

```python
from multiprocessing import Process
# pid   process id
# ppid  parent process id
# os.getpid()    /   os.getppid()  找到进程的id  ,找到父进程id

```

1. 在pycharm中启动的所有py程序都是pycharm的子进程

2. 父进程 在父进程中创建子进程

   ```python
   import os
   import time
   from multiprocessing import Process
   
   def func():
       print('start',os.getpid())
       time.sleep(1)
       print('end',os.getpid())
   
   if __name__ == '__main__':
       p = Process(target=func)
       p.start()   异步 调用开启进程的方法 但是并不等待这个进程真的开启
       print('main :',os.getpid())
   ```

   ```python
   import os
   import time
   from multiprocessing import Process
   
   def eat():
       print('start eating',os.getpid())
       time.sleep(1)
       print('end eating',os.getpid())
   
   def sleep():
       print('start sleeping',os.getpid())
       time.sleep(1)
       print('end sleeping',os.getpid())
   
   if __name__ == '__main__':
       p1 = Process(target=eat)    # 创建一个即将要执行eat函数的进程对象
       p1.start()                  # 开启一个进程
       p2 = Process(target=sleep)  # 创建一个即将要执行sleep函数的进程对象
       p2.start()                  # 开启进程
       print('main :',os.getpid())
   ```

##### 9.2.4.1注意win和linux系统在创建进程是的区别

1. 操作系统创建进程的方式不同
2. windows操作系统执行开启进程的代码
3. 实际上新的子进程需要通过import父进程的代码来完成数据的导入工作
   - 所以有一些内容我们只希望在父进程中完成，就写在if `__name__` == `__main__`:缩进处即可

##### 8.2.4.2父进程和子进程的关系

```python
import os
import time
from multiprocessing import Process
def func():
    print('start',os.getpid())
    time.sleep(10)
    print('end',os.getpid())
    
if __name__ == '__main__':
    p = Process(target=func)
    p.start()   #  异步 调用开启进程的方法 但是并不等待这个进程真的开启
    print('main :',os.getpid())
    #  主进程没结束 ：等待子进程结束
    #  主进程负责回收子进程的资源
    #  如果子进程执行结束，父进程没有回收资源，那么这个子进程会变成一个僵尸进程
```

1. 主进程的结束逻辑
   1. 主进程的代码结束
   2. 所有的子进程结束
   3. 给子进程回收资源
   4. 主进程结束

8.2.4.3 Process模块的join方法

1. join方法 ：阻塞，直到子进程结束就结束

   ```python
   # 开启10个进程，给公司的5000个人发邮件，发送完邮件之后，打印一个消息“5000封邮件已发送完毕”
   
   import time
   import random
   from multiprocessing import Process
   def send_mail(a):
       time.sleep(random.random())
       print('发送了一封邮件',a)
   
   if __name__ == '__main__':
       l = []
       for i in range(10):
           p = Process(target=send_mail,args=(i,))
           p.start()   #  # 异步 非阻塞
           l.append(p)
       print(l)
       for p in l:p.join()     # # 同步 阻塞 直到p对应的进程结束之后才结束阻塞  
       print('5000封邮件已发送完毕')
   ```

##### 9.2.4.3 总结

1. 开启一个进程

   - 函数名(参数1,参数2)
   - from multiprocessing import Process
   - p = Process(target=函数名,args=(参数1,参数2))
   - p.start()

2. 父进程  和 子进程

3. 父进程会等待着所有的子进程结束之后才结束

   - 为了回收资源

4. 进程开启的过程中windows和 linux/ios之间的区别

   - 开启进程的过程需要放在if `__name__` == '`__main__`'下

   ​        windows中 相当于在子进程中把主进程文件又从头到尾执行了一遍
               除了放在if `__name__` == '`__main__`'下的代码
           linux中 不执行代码,直接执行调用的func函数

5. join方法

   ​    把一个进程的结束事件封装成一个join方法
       执行join方法的效果就是 阻塞直到这个子进程执行结束就结束阻塞

```python
#  在多个子进程中使用join
p_l= []
for i in range(10):
    p = Process(target=函数名,args=(参数1,参数2))
    p.start()
    p_l.append(p)
for p in p_l:p.join()
# 所有的子进程都结束之后要执行的代码写在这里
```
##### 9.2.4.4 `__name__`补充讲解

```python
if __name__ == '__main__':
    # 控制当这个py文件被当作脚本直接执行的时候，就执行这里面的代码
    # 当这个py文件被当作模块导入的时候，就不执行这里面的代码
    print('hello hello')
# __name__ == '__main__'
    # 执行的文件就是__name__所在的文件
# __name__ == '文件名'
    # __name__所在的文件被导入执行的时候
```

##### 9.2.4.5协程相关

1. 并发编程

   - 不会有大量的例子和习题

   ​        1. 进程 在我们目前完成的一些项目里是不常用到的
           2.线程 后面的爬虫阶段经常用
               前端的障碍
           3.协程
               异步的框架 异步的爬虫模块

2. 为什么进程用的不多，但是还要来讲

   ​        1.你可能用不到，但是未来你去做非常复杂的数据分析或者是高计算的程序
           2.进程和线程的很多模型很多概念是基本一致的

#### 9.2.5进程模块进阶

1. 线程

   - 线程是进程的一部分，每个进程中至少有一个线程, 是能被CPU调度的最小单位 .   

   - 一个进程中的多个线程是可以共享这个进程的数据的  —— 数据共享
   - 线程的创建、销毁、切换 开销远远小于进程  —— 开销小

2. multiprocessing 进程

   1. 使用方法 : p = Process(target=函数名,args=(参数1,))

   2. 如何创建一个进程对象

      - 对象和进程之间的关系
      - 进程对象和进程并没有直接的关系
      - 只是存储了一些和进程相关的内容
      - 此时此刻，操作系统还没有接到创建进程的指令

   3. 如何开启一个进程

      - 通过p.start()开启了一个进程--这个方法相当于给了操作系统一条指令
      -  start方法 的 非阻塞和异步的特点
      -  在执行开启进程这个方法的时候
      -  我们既不等待这个进程开启，也不等待操作系统给我们的响应
      -  这里只是负责通知操作系统去开启一个进程
      -  开启了一个子进程之后，主进程的代码和子进程的代码完全异步

   4. 父进程和子进程之间的关系

      - 父进程会等待子进程结束之后才结束
      - 为了回收子进程的资源

   5. 不同操作系统中进程开启的方式

      -  windows 通过（模块导入）再一次执行父进程文件中的代码来获取父进程中的数据
        -  所以只要是不希望被子进程执行的代码，就写在if `__name__` == `__main__`下
      -  因为在进行导入的时候父进程文件中的if `__name__` == `__main__`
      - linux/ios
        -  正常的写就可以，没有if __name__ == '__main__'这件事情了

      ​    5.如何确认一个子进程执行完毕
              Process().join方法
              开启了多个子进程，等待所有子进程结束

#### 9.2.6守护进程

```python
# 有一个参数可以把一个子进程设置为一个守护进程
if __name__ == '__main__':
    p = Process(target=son1,args=(1,2))
    p.daemon = True    # daemon = True   
    p.start()      # 把p子进程设置成了一个守护进程
    p2 = Process(target=son2)
    p2.start()
    time.sleep(2)
# 守护进程是随着主进程的代码结束而结束的
    # 生产者消费者模型的时候
    # 和守护线程做对比的时候
# 所有的子进程都必须在主进程结束之前结束，由主进程来负责回收资源
```

1. 会随着主进程的结束而结束。

   主进程创建守护进程

   　　其一：守护进程会在主进程代码执行结束后就终止

   　　其二：守护进程内无法再开启子进程,否则抛出异常：AssertionError: daemonic processes are not allowed to have children

   注意：进程之间是互相独立的，主进程代码运行结束，守护进程随即终止

#### 9.2.7   terminate()方法

```python
#  p.terminate():强制终止进程p，不会进行任何清理操作，如果p创建了子进程，该子进程就成了僵尸进程，使用该方法需要特别小心这种情况。如果p还保存了一个锁那么也将不会被释放，进而导致死锁
```

```python
import time
from multiprocessing import Process

def son1():
    while True:
        print('is alive')
        time.sleep(0.5)

if __name__ == '__main__':
    p = Process(target=son1)
    p.start()      # 异步 非阻塞
    print(p.is_alive())
    time.sleep(1)
    p.terminate()   # 异步的 非阻塞
    print(p.is_alive())   # 进程还活着 因为操作系统还没来得及关闭进程
    time.sleep(0.01)
    print(p.is_alive())   # 操作系统已经响应了我们要关闭进程的需求，再去检测的时候，得到的结果是进程已经结束了
```

### 9.3 Process模块面向对象

```python
# Process类
# 开启进程的方式
    面向函数
        def 函数名:要在子进程中执行的代码
        p = Process(target= 函数名,args=(参数1，))
    面向对象
        class 类名(Process):
            def __init__(self,参数1，参数2):   如果子进程不需要参数可以不写
                self.a = 参数1
                self.b = 参数2
                super().__init__()
            def run(self):      # 必须为run方法
                要在子进程中执行的代码
        p = 类名(参数1，参数2)
    Process提供的操作进程的方法
        p.start() 开启进程      异步非阻塞
        p.terminate() 结束进程  异步非阻塞

        p.join()     同步阻塞
        p.isalive()  获取当前进程的状态
        daemon = True 设置为守护进程，守护进程永远在主进程的代码结束之后自动结束
```

####  9.3.1 并发和加锁

- 实现并发完成了许多,但如果当多个进程使用同一份数据资源的时候，就会引发数据安全或顺序混乱问题。 

1. 在这里我们通过给程序加锁(Lock),这种情况虽然使用加锁的形式实现了顺序的执行，但是程序又重新变成串行了，这样确实会浪费了时间，却保证了数据的安全。 

   ```python
   # 由并发变成了串行,牺牲了运行效率,但避免了竞争
   import os
   import time
   import random
   from multiprocessing import Process,Lock
   
   def work(lock,n):
       lock.acquire()            # 
       print('%s: %s is running' % (n, os.getpid()))
       time.sleep(random.random())
       print('%s: %s is done' % (n, os.getpid()))
       lock.release()            #
   if __name__ == '__main__':
       lock=Lock()
       for i in range(3):
           p=Process(target=work,args=(lock,i))
           p.start()
           
    # 在主进程中实例化 lock = Lock()
   # 把这把锁传递给子进程
   # 在子进程中 对需要加锁的代码 进行 with lock：
       # with lock相当于lock.acquire()和lock.release()
   # 在进程中需要加锁的场景
       # 共享的数据资源（文件、数据库）
       # 对资源进行修改、删除操作
   # 加锁之后能够保证数据的安全性 但是也降低了程序的执行效率
   
   
   
   # lock.acquire()  /  lock.release() 支持上下文.可以with lock:
   ```

#### 9.3.2进程间的通信**IPC**(Inter-Process Communication)

**队列 : (Queue  --> 先进先出)**

1. 进程间是数据隔离的

2. 创建共享的进程队列，Queue是多进程安全的队列，可以使用Queue实现多进程之间的数据传递。 

3. ```python
   # Queue基于 天生就是数据安全的
       # 文件家族的socket pickle lock
   # pipe 管道(不安全的) = 文件家族的socket pickle
   # 队列 = 管道 + 锁
   ```

4. Queue的使用

   ```python
   import queue
   
   from multiprocessing import Queue
   q = Queue(5)
   q.put(1)
   q.put(2)
   q.put(3)
   q.put(4)
   q.put(5)   # 当队列为满的时候再向队列中放数据 队列会阻塞
   print('5555555')
   try:
       q.put_nowait(6)  # 当队列为满的时候再向队列中放数据 会报错并且会丢失数据.推荐使用
   except queue.Full:
       pass
   print('6666666')
   
   print(q.get())
   print(q.get())
   print(q.get())   # 在队列为空的时候会发生阻塞
   print(q.get())   # 在队列为空的时候会发生阻塞
   print(q.get())   # 在队列为空的时候会发生阻塞
   try:
       print(q.get_nowait())   # 在队列为空的时候 直接报错  ,推荐使用
   except queue.Empty:pass
   
   ```

   我们记住使用get_nowait()  和  put_nowait()   

#### 9.3.3多进程实现并发socket服务端

```python
import socket
from multiprocessing import Process
def chat(conn):
    while True:
        try:
            ret = conn.recv(1024).decode('utf-8')
            conn.send(ret.upper().encode('utf-8'))
        except ConnectionResetError:
            break
a
if __name__ == '__main__':
    sk = socket.socket()
    sk.bind(('127.0.0.1', 9000))
    sk.listen()
    while True:
        conn,_ = sk.accept()
        Process(target=chat,args=(conn,)).start()
```

#### 9.3.4 ipc机制

- 进程之间相互通信
- ipc机制-->队列   管道
- 第三方工具(软件)提供给我们的IPC机制
  - redis
  - memcache
  - kafka等等
  - 并发需求   ,高可用  ,断电保存数据

### 9.4生产者和消费者模式

**在并发编程中使用生产者和消费者模式能够解决绝大多数并发问题。该模式通过平衡生产线程和消费线程的工作能力来提高程序的整体处理数据的速度。**

- 一个进程就是一个生产者,一个进程就是一个消费者

- 队列:生产者和消费者之间的容器就是队列

  什么是生产者消费者模式

  **生产者消费者模式是通过一个容器来解决生产者和消费者的强耦合问题。生产者和消费者彼此之间不直接通讯，而通过阻塞队列来进行通讯，所以生产者生产完数据之后不用等待消费者处理，直接扔给阻塞队列，消费者不找生产者要数据，而是直接从阻塞队列里取，阻塞队列就相当于一个缓冲区，平衡了生产者和消费者的处理能力。**

- 在生产者在生产完后就结束了,往队列中再发一个结束信号，这样消费者在接收到结束信号后就可以break出死循环。 
  结束信号可以生产者发送,也可以主进程在生产者进程结束后发送.

- 但若是有多个生产者和消费者时 , 就必须得在多个生产者全部生产完毕后,才能发送结束信号,并且有多少个消费者,就需要发送几次结束信号.

#### 9.4.1 共享队列(**JoinableQueue([maxsize])**

我们使用共享队列,创建可连接的共享进程队列。这就像是一个Queue对象，但队列允许项目的使用者通知生产者项目已经被成功处理。通知进程是使用共享的信号和条件变量来实现的。  

```python
"""JoinableQueue的实例p除了与Queue对象相同的方法之外，还具有以下方法：

q.task_done() 
使用者使用此方法发出信号，表示q.get()返回的项目已经被处理。如果调用此方法的次数大于从队列中删除的项目数量，将引发ValueError异常。

q.join() 
生产者将使用此方法进行阻塞，直到队列中所有项目均被处理。阻塞将持续到为队列中的每个项目均调用q.task_done()方法为止。 
下面的例子说明如何建立永远运行的进程，使用和处理队列上的项目。生产者将项目放入队列，并等待它们被处理。"""
```

```python
from multiprocessing import Process,JoinableQueue
import time,random,os
def consumer(q):
    while True:
        res=q.get()
        time.sleep(random.randint(1,3))
        print('\033[45m%s 吃 %s\033[0m' %(os.getpid(),res))
        q.task_done() #向q.join()发送一次信号,证明一个数据已经被取走了

def producer(name,q):
    for i in range(10):
        time.sleep(random.randint(1,3))
        res='%s%s' %(name,i)
        q.put(res)
        print('\033[44m%s 生产了 %s\033[0m' %(os.getpid(),res))
    q.join() #生产完毕，使用此方法进行阻塞，直到队列中所有项目均被处理。


if __name__ == '__main__':
    q=JoinableQueue()
    #生产者们:即厨师们
    p1=Process(target=producer,args=('包子',q))
    p2=Process(target=producer,args=('骨头',q))
    p3=Process(target=producer,args=('泔水',q))

    #消费者们:即吃货们
    c1=Process(target=consumer,args=(q,))
    c2=Process(target=consumer,args=(q,))
    c1.daemon=True
    c2.daemon=True

    #开始
    p_l=[p1,p2,p3,c1,c2]
    for p in p_l:
        p.start()

    p1.join()
    p2.join()
    p3.join()
    print('主') 
    
    #主进程等--->p1,p2,p3等---->c1,c2
    #p1,p2,p3结束了,证明c1,c2肯定全都收完了p1,p2,p3发到队列的数据
    #因而c1,c2也没有存在的价值了,不需要继续阻塞在进程中影响主进程了。应该随着主进程的结束而结束,所以设置成守护进程就可以了。
```

#### 9.4.2 使用manager 来进程间互相通信

```python
from multiprocessing import Manager,Process,Lock

def func(dic,lock):
    with lock:
        dic['count'] -= 1

if __name__ == '__main__':
    # m = Manager()
    with Manager() as m:
        l = Lock()
        dic = m.dict({'count':100})
        p_l = []
        for i in range(100):
            p = Process(target=func,args=(dic,l))
            p.start()
            p_l.append(p)
        for p in p_l:p.join()
        print(dic)


# mulprocessing中有一个manager类
# 封装了所有和进程相关的 数据共享 数据传递
# 相关的数据类型
# 但是对于 字典 列表这一类的数据操作的时候会产生数据不安全
# 需要加锁解决问题，并且需要尽量少的使用这种方式
```



## 9.5 线程

#### 9.5.1线程

1. 是进程中的一部分，每一个进程中至少有一个线程

- 进程是计算机中最小的资源分配单位（进程是负责圈资源）
- 线程是计算机中能被CPU调度的最小单位 （线程是负责执行具体代码的）

1. 开销

   - 线程的创建，也需要一些开销（一个存储局部变量的结构，记录状态）
   - 创建、销毁、切换开销远远小于进程

2. python中的线程比较特殊，所以进程也有可能被用到

   - 进程 ：数据隔离 开销大 同时执行几段代码
   - 线程 ：数据共享 开销小 同时执行几段代码

3. cpython解释器 不能实现多线程利用多核

   **原因 :** 锁 ：GIL 全局解释器锁

   ```python
   #     保证了整个python程序中，只能有一个线程被CPU执行
   """原因：cpython解释器中特殊的垃圾回收机制
   GIL锁导致了线程不能并行，可以并发
   所以使用所线程并不影响高io型的操作
   只会对高计算型的程序由效率上的影响
   遇到高计算 ： 多进程 + 多线程
                 分布式"""
   ```

   

   