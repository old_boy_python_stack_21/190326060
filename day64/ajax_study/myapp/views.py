from django.shortcuts import render, HttpResponse
from myapp import models


# Create your views here.

def login(request):
    error = ""
    if request.method == "POST":
        user = request.POST.get("user")
        pwd = request.POST.get("pwd")
        obj = models.User.objects.filter(user=user)
        if obj:
            if obj[0].user == user and obj[0].pwd == pwd:
                return HttpResponse('登陆成功')
        else:
            error = "用户名或密码错误"
    return render(request, "login.html", {"error": error})


def register(request):
    return render(request, "register.html")


def ensure_register(request):
    user = request.POST.get('user')
    if models.User.objects.filter(user=user):
        return HttpResponse('用户名已被占用')
    return HttpResponse('这个用户名ok')


def user_list(request):
    user_list = models.User.objects.all().order_by('uid')
    return  render(request,"user_list.html",{"user_list":user_list})


def del_user(request):
    pk = request.POST.get("id")
    obj = models.User.objects.filter(pk=pk)
    if obj:
        obj.delete()
        return  HttpResponse('ok')
    return HttpResponse('error')
