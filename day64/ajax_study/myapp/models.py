from django.db import models

# Create your models here.
class User(models.Model):
    uid = models.AutoField(primary_key=True)
    user = models.CharField(max_length=32,blank=False)
    pwd = models.CharField(max_length=32,blank=False)

    class Meta:
        db_table = 'user'

