# -*- coding:utf-8 -*-
# __author__ = Deng Jack

from flask import Flask, request, render_template,Markup
from serv.file_manager import fm
from serv.user import ubp


app = Flask(__name__)
app.register_blueprint(fm)
app.register_blueprint(ubp)

@app.route('/')
def index():
    return Markup('<h1>Server Is Running</h1>')

if __name__ == '__main__':
    # app.run('192.168.12.188',8888,debug=True)
    app.run('0.0.0.0',8888,debug=True)
