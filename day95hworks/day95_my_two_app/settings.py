# -*- coding:utf-8 -*-

# 目录相关配置
AVATAR_PATH = 'avatar'
RECO = 'reco'
# 数据库配置,默认配置,本机
from pymongo import MongoClient
MC = MongoClient()
UserTable = MC.USER_INFO.UserTable


# 返回协议
RET = {
    'code':0,
    'msg':'',
    'data':{}
}