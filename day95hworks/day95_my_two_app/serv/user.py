# -*- coding:utf-8 -*-
# __author__ = Deng Jack
import os

from bson import ObjectId
from flask import Blueprint, request
from settings import UserTable,RET

ubp = Blueprint('user',__name__)


@ubp.route('/reg/',methods=['post'])
def reg():
    user_info = request.form.to_dict()
    print(user_info) # 打印一下
    user_exist = UserTable.find_one({'username':user_info.get('username')})
    if user_exist:
        RET['msg'] = '用户名已存在'
        RET['code'] = 1
        return RET
    res = UserTable.insert_one(user_info)
    RET['data'].update({'user_id':str(res.inserted_id)})
    RET['msg'] = '注册成功'
    return RET

@ubp.route('/login/',methods=['post'])
def login():
    user_info = request.form.to_dict()
    print(user_info)# 打印一下
    user_info_dict = UserTable.find_one(user_info)
    if user_info_dict:
        user_info_dict['_id'] = str(user_info_dict.get('_id')) # 强制转化为str
        user_info_dict.pop('pwd') # 删除pwd字符串
        RET['msg'] = '登陆成功'
        RET['data'] = user_info_dict
        return RET
    RET['msg'] = '登陆失败'
    RET['code'] = 1
    return RET


@ubp.route("/auto_login",methods=["POST"])
def auto_login():
    user_info = request.form.to_dict()
    user_info["_id"] = ObjectId(user_info.pop("user_id"))
    user_dict = UserTable.find_one(user_info)
    if user_dict:
        user_dict["_id"] = str(user_dict.get("_id"))
        return {"code": 0, "msg": "登录成功","data":user_dict}
    else:
        return {"code": 9999, "msg": "登陆失败"}