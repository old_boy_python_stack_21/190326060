# -*- coding:utf-8 -*-
# __author__ = Deng Jack
import os

from flask import Blueprint,request,send_file
from settings import AVATAR_PATH ,RECO

fm = Blueprint('file_manager',__name__)

@fm.route('/upload',methods=['post'])
def uploader():
    avatar = request.files.get('avatar')
    avatar_file_path = os.path.join(AVATAR_PATH,avatar.filename)
    avatar.save(avatar_file_path)
    return {'status':200,'filename':avatar.filename}

@fm.route('/upload_reco/',methods=['post'])
def upload_reco():
    upload_reco = request.files.get('recofile')
    avatar_file_path = os.path.join(RECO,upload_reco.filename)
    upload_reco.save(avatar_file_path)
    return {'status':200,'filename':upload_reco.filename}

@fm.route('/get_avatar/<avatar_file_path>',methods=['get'])
def get_avatar(avatar_file_path):
    avatar_file_path  = os.path.join(AVATAR_PATH,avatar_file_path)
    return send_file(avatar_file_path)