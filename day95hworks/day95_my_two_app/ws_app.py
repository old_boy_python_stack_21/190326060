# -*- coding:utf-8 -*-
# __author__ = Deng Jack
import json

from flask import Flask, Markup, request
from geventwebsocket.server import WSGIServer
from geventwebsocket.handler import WebSocketHandler
from geventwebsocket.websocket import WebSocket


ws_app = Flask(__name__)

user_socket_dict = {}

@ws_app.route('/')
def index():
    return Markup('<h1>Server Is Running</h1>')


@ws_app.route('/<user_id>')
def ws(user_id):
    user_socket = request.environ.get('wsgi.websocket') # type:WebSocket
    if user_socket:
        user_socket_dict['user_id'] = user_socket

    print(user_socket)

    while True:
        msg = user_socket.receive()
        msg_dict = json.loads(msg)
        receiver = msg_dict.get('receiver')
        receiver_socl = user_socket_dict.get('receiver') # type:WebSocket
        receiver_socl.send(msg_dict)

if __name__ == '__main__':
    http_serv = WSGIServer(('0.0.0.0',8889),ws_app,handler_class=WebSocketHandler)
    http_serv.serve_forever()