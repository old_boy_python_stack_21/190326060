function createUpload(filepath,filekey) {
	var task = plus.uploader.createUpload( "http://www.test.com/upload.do", 
		{ method:"POST",blocksize:204800,priority:100 },
		function ( t, status ) {
			// 上传完成
			if ( status == 200 ) { 
				console.log('上传成功');
				console.log(t.responseText);
			} else {
				console.log('上传失败');
			}
		}
	);
	task.addFile( filepath, {key:filekey} );
	
	// task.addData( "string_key", "string_value" );
	//task.addEventListener( "statechanged", onStateChanged, false );
	
	task.start();
}