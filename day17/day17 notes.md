# python day 17

## 1. 迭代器

1. 自己不用写迭代器,只.

   ```python
   # 一一展示列表中的元素
   # 1.for 循环    2. while + 索引 + 计数器
   # 使用迭代器--->对 某种对象(str/list/tuple/dict/set类创建的对象)-可迭代对象中的元素进行逐一获取，表象：具有`__next__`方法且每次调用都获取可迭代对象中的元素（从前到后一个一个获取）。
   # 可被for 循环的---> 可迭代对象
   
   
   #***************************************************************#
   v1 = [1,2,3,4]
   v2 = iter(v1)    # --->v2是一个迭代器
   #或者  v2 = v1.__iter__()
   
   # 迭代器想要获取每个值:-->反复调用
   while 1 :
   	val = v1.__next__() 
       print(val)
    
   ##### *********   ######
   v3 = "alex"
   v4 = iter(v1)
   while True:
       try:
           val = v2.__next__()
           print(val)
       except Exception as e:
           break
   
           
           
   # 直到报错：StopIteration错误，表示已经迭代完毕。
   
   ```

2. 判定是否是迭代器  --> 内部是否有`__next__方法` 

3. for 循环---->为这简便有了for 循环

   ```python
   v1 = [11,22,33,44]
   
   # 1.内部会将v1转换成迭代器
   # 2.内部反复执行 迭代器.__next__()
   # 3.取完不报错
   
   for item in v1:
       print(item)
   ```

## 2. 可迭代对象

- 内部具有 `__iter__()` 方法且返回一个迭代器。（*）

  ```python
  v1 = [11,22,33,44]
  result = v1.__iter__()
  ```

- 可以被for循环

## 3.生成器(变异函数---->特殊的迭代器)

1. 函数样式

   ```python
   # 函数
   def func():
       return 123
   func()
   
   ```

2. 生成器

   ```python
   # 生成器函数（内部是否包含yield）---->只要有yeild就是生成器,不论是否有return
   def func():
       print('F1')
       yield 1
       print('F2')
       yield 2
       print('F3')
       yield 100
       print('F4')
   # 函数内部代码不会执行，返回一个 生成器对象 。
   v1 = func()
   # 生成器是可以被for循环，一旦开始循环那么函数内部代码就会开始执行。
   for item in v1:
       print(item)
   ```

- 总结：函数中如果存在yield，那么该函数就是一个生成器函数，调用生成器函数会返回一个生成器，生成器只有被for循环时，生成器函数内部的代码才会执行，每次循环都会获取yield返回的值

  ```python
  def func():
      count = 1
      while 1 :
          yield count
          count += 1
          if count == 100 :
              return
  val = func()     # --->调用生成器函数 ---->返回值时一个生成器 --->内部代码没有执行
  for item in val :
      print(item)  # 此时才执行生成器内部代码,并yield.就冻结,直到下次再继续运行.
  ```

  - 示例 读文件

    ```python
    def func():
        """
            分批去读取文件中的内容，将文件的内容返回给调用者。
        """
        cursor = 0
        while 1 :
            f =  open(r'D:\Python学习\python\s21day16\goods.txt','r',encoding='utf-8-sig')
            f.seek(cursor)
            data_list = []                                            # 光标移到最前
            for i in range(10):
                line = f.readline()                                      # 一次读一行
                if not line:
                    return
                data_list.append(line)
            cursor = f.tell()             #获取光标
            f.close()                                                 # 关闭与redis的连接
            for row in data_list:
                yield  row
    
    
    for itemm in func():
        p = input('qingsr123456498')
        print(itemm)
    ```

    

## 4.总结

- 迭代器，对可迭代对象中的元素进行逐一获取，迭代器对象的内部都有一个 __next__方法，用于以一个个获取数据。
- 可迭代对象，可以被for循环且此类对象中都有 __iter__方法且要返回一个迭代器（生成器）。
- 生成器，函数内部有yield则就是生成器函数，调用函数则返回一个生成器，循环生成器时，则函数内部代码才会执行。

