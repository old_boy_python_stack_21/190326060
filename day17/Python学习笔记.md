# Python学习笔记

## 第一章 计算机基础

### 1.1  硬件

计算机基本的硬件由：CPU / 内存 / 主板 / 硬盘 / 网卡  / 显卡 / 显示器 等组成，只有硬件但硬件之间无法进行交流和通信。

### 1.2 操作系统

操作系统用于协同或控制硬件之间进行工作，常见的操作系统有那些:

- windows系统,应用最广泛的操作系统.
  - win xp 系统
  - win 7系统
  - win 10 系统
- linux系统,免费开源,占用内存小,运行速度快
  - centos .**公司线上服务器使用,图形界面较ubuntu差**
  - ubuntu,用于开发.图形界面较好
  - renhat,主要用于企业级服务器
- mac(苹果系统,对办公和开发都很好)

### 1.3  解释器或编译器

编程语言的开发者写的一个工具，将用户写的代码转换成010101交给操作系统去执行。

#### 1.3.1  解释和编译型语言

解释型语言就类似于： 实时翻译，代表：Python / PHP / Ruby / Perl

- 特点:写完代码交给解释器，解释器会从上到下一行行代码执行：边解释边执行。

编译型语言类似于：说完之后，整体再进行翻译，代表：C / C++ / Java / Go ...

- 特点:代码写完后，编译器将其变成成另外一个文件，然后交给计算机执行。

#### 1.3.2 学习编程语言

1. 安装解释器或编译器,工具准备.
2. 学习该语言的语法规则

### 1.4 软件（应用程序）

软件又称为应用程序，就是我们在电脑上使用的工具，类似于：记事本 / 图片查看 / 游戏.是由程序员编写的.

### 1.5 进制

对于计算机而言无论是文件存储 / 网络传输输入本质上都是：二进制（010101010101ainc），如：电脑上存储视频/图片/文件都是二进制； QQ/微信聊天发送的表情/文字/语言/视频 也全部都是二进制。

进制：

- 2进制，计算机内部。二进制数据是用0和1两个数码来表示的数。它的基数为2，进位规则是“逢二进一”

  计算机内部都是以二进制存储数据的.

- 8进制: 采用0，1，2，3，4，5，6，7八个数字，逢八进1 .

- 10进制，人来进行使用一般情况下计算机可以获取10进制，然后再内部会自动转换成二进制并操作。

- 16进制，一般用于表示二进制（用更短的内容表示更多的数据），一般是：\x 开头。

| 二进制 | 八进制 | 十进制 | 十六进制 |      |
| ------ | ------ | ------ | -------- | ---- |
| 0      | 0      | 0      | 0        |      |
| 1      | 1      | 1      | 1        |      |
| 10     | 2      | 2      | 2        |      |
| 11     | 3      | 3      | ```      |      |
| 100    | ```    | ```    | 9        |      |
|        | 7      | 8      | A        |      |
|        |        | 9      | ```      |      |
|        |        |        | F        |      |



## 第二章 Python入门

### 2.1 环境的安装

- 下载软件(官网下载)并安装软件
  - python 2.7.16 （2020年官方不在维护）
  - python 3.6.8 （推荐）

- 解释器：py2 / py3 （环境变量）,添加环境变量,以便以后快速调用程序![截图python0408135813](Python学习笔记.assets/截图python0408135813.png)
- 开发工具：pycharm的安装,激活(界面的调整,防止伤眼睛)

### 2.2 编码

#### 2.2.1 编码基础

- ascii码:表示英文和标点符号,1字节表示一个字符.
- unicode码:能表示世界上所有的语言,4字节表示一个字符,现在用到了27位bit.
- utf-8码:对unicode码的压缩,中文3个字节表示.
- gbk码:亚洲地区使用,gb2312码的升级版,2字节表示中文.
- gb2312码:亚洲地区使用,2字节表示中文.

#### 2.2.2 python编码相关

对于Python默认解释器的编码：

- py2： ascii
- py3： utf-8

如果想要修改默认编码，则可以使用：

```python
# -*- coding:utf-8 -*- 
```

**注意：对于操作文件时，要按照：以什么编写写入，就要用什么编码去打开。**

在linux系统中,py的文件开头有:

```python
#!/usr/bin/env python  在Linux中指定解释器的路径
# -*- coding:utf-8 -*-
```

运行： 解释器   文件路径 

在linux上有一种特殊的执行方法：

- 给文件赋予一个可执行的权限
- ./a.py  自动去找文件的第一行 =  /usr/bin/env/python  a.py

#### 2.2.3单位换算

​	             8 bit = 1 bype

​                    1024 bype = 1 KB

​                    1024 KB = 1 MB

​                    1024 MB = 1 GB

​                    1024 GB = 1 TB

### 2.3 变量

问：为什么要有变量？

为某个值创建一个“外号”，以后在使用时候通过此外号就可以直接调用。

#### 2.3.1变量的命名规则

1. 变量名由数字,字母和下划线组成.

2. 变量名不能以数字开头

3. 变量名要避开python的关键字,如[‘and’, ‘as’, ‘assert’, ‘break’, ‘class’, ‘continue’, ‘def’, ‘del’, ‘elif’, ‘else’, ‘except’, ‘exec’, ‘finally’, ‘for’, ‘from’, ‘global’, ‘if’, ‘import’, ‘in’, ‘is’, ‘lambda’, ‘not’, ‘or’, ‘pass’, ‘print’, ‘raise’, ‘return’, ‘try’, ‘while’, ‘with’, ‘yield’]等等.

4. 建议:  见名知意:用简单明了,意思相近的单词做变量名.

   ​          单词间用下划线连接,如变量名: deng_dad.

### 2.4 python基础语句

#### 2.4.1输出/输入语句

1. 输出语句

```python
print（你想输出的内容）
```

python2中，输出是: print ”你想输出的“（注意：print和引号间有空格）

python3中，输出是: print（“你想输出的”）

2. 输入语句输入

   input语句：

   ```python
   name=input('请输入你的用户名:')
   password=input('请输入你的密码')
   print(content)
   print(password)
   ```

   注意：

   1. input语句输入得到的内容永远是字符串。
   2. python2的输入语句是:raw_input('')。
   3. python3的输入语句是;input('')。

#### 2.4.2编程的注释

编程代码一定要做注释，注释不参与代码运行,编程代码行数太多了。分为二类，如

```python
# 单行注释，不参与代码运算

"""
多行注释，
不参与程序运算
"""
```

#### 2.4.3条件判断语句

- 最简单条件判断

```python
age = input('请输入你的年龄：')
new_age=int(age)
# input输入的数据类型是字符串，需要用int语句把字符串数据转化为整型数据。
if new_age >= 18:
	print('你已经是成年了人了')
```

- 初级语句

```python
gender = input('请输入你的性别：')
# 默认不是男性就是女性
if gender == '男':
	print('走开')
else:
	print('来呀，快活呀')
```

- elif语句

```
gender = input('请输入你的性别：')
# 性别有男、女、人妖多种选择
if gender == '男':
	print('走开')
elif gender == '女':
	print('来呀，快活呀')
else：
	print（'找##去，他是gay'）
```

elif语句可以用无限次使用，如果次数过多会有其他语句使用,语句过于冗长.

- and语句，python的关键字之一，表示并且的意思。

#### 2.4.4循环语句

- while 用法

  ```python
  num = 1
  while num <= 10:
      print(num)
      num += 1
  ```

- while else 用法

  ```python
  num = 1
  while num <= 10:
      print(num)
      num += 1
  else:
      print("end")
  # else 表示while循环语句不满足组条件后执行的代码
  
  #  pass  占位符,不做任何事.
  ```

- break、continue关键字的用法,以及与if   pass语句的嵌套

#### 2.4.5运算符

- 算术运算符:加减乘除的运用，+、-、*、/ 在程序中用于算术运算。还有类似于：

  1. % ，取除法的余数，如15%4 = 3
  2. // ，取除法的商的整数，如20//6 = 3
  3. ** ，取指数的值，如2**8 = 2的8次方，为256.

- 赋值运算符：

  1. c += 1 等价于 c = c+ 1

  2. c -= 1等价于c = c - 1

  3. c *= 2 等价于 c = c * 2

     等等诸如此类

- 逻辑运算符: and 、or、not

  1. 一般用法：表示逻辑中的于、或、非，用于条件的判断

  2. 二般用法：

     - 3种数据类型的转化，int str boolen的转化

       ```python
       test=bool('')
       test1=bool(0)
       print(test)
       print(test1)
       #####   注意：只有空字符串和0转化为布尔值时为false，否则都为 true
       ```

     - ```
       value = x and y
       print(value)
       ####: 从左到右， x转化为布尔值为真，value = y，否则value= x 。
       value1 = x or y
       print(value1)
       ####: 从左到右， x转化为布尔值为真，value1 = x，否则value= x 。
       ```

     - 运算的优先顺序为： ( ) >not > and >or

- in ，逻辑运算符，判断某字符或某字符串是否在一个大的字符串中，输出得到bool型数据。

  ```python
  value = '我是中国人'
  v = '我'
  if v in value:
      print(v)
  else:
      print('出错')  #   我
  ```

  

## 第三章 数据类型

### 3.1 整型（int）

#### 3.1.1 整型的长度

py2中有：int有取值范围，对于32位系统而言：-2^31~2^31-1

​                                          对于64位系统而言：-2^63~2^63-1

​       超出范围后，py2自动转换成long(长整型)数据。

py3中有：int （int/long）只有int数据.

#### 3.1.2 整除

py2和py3中整除是不一样.

- py2:整除只保留整数,小数位会舍去.若要保留小数.则在文件头加

  ```python
  from __future__ import division
  ```

- py3整除保留所有.

### 3.2 布尔（bool）

布尔值就是用于表示真假。True和False。

其他类型转换成布尔值：只有''/0/[]/{}/()/set()转换为bool值为False,其余都是True.

### 3.3 字符串（str）

字符串是写代码中最常见的 :

- 单引号，如'王飞'
- 双引号，如”王大“
- 三引号，如“”“王小”“”，三引号支持换行。

注意：整型数据可以+和×，字符串数据也可以+和×。如

```python
name='五五开'
new_name=name*3
print（new_name）   #  '五五开五五开五五开'
```

python内存中的字符串是按照：unicode 编码存储。对于字符串是不可变数据类型。

#### 3.3.1字符串的格式化

1. 字符串格式化的意义，大部分字符过于冗长，使用字符串格式化能大大加快效率，方便程序员调用数据。

2. %s 、 %d、%%

   - ```python
     red_dad = '大红的爸爸'
     do = '教学生上课'
     thing = '%s在操场%s' %(red_dad，do,)
     print(thing)
     ```

   - 直接做占位符

     ```python
     temper = '%s在太空中%s' %('等大侠','打飞机',)
     print(temper)
     thing = '盖伦，年龄%d，喜欢在池塘里%s' %(15,'打水仗'，)
     print(thing)
     #####  #s和#d表示的类型不同，前者表示字符串数据，后者表示整型数据。
     name = '小明'
     template = "%s拿出了100%%的力气" %(name,)
     print(template)
     ######   %%，为了和字符串格式化做区分，百分号要写成%%形式。
     ```

#### 3.3.2字符串的方法

字符串自己有很多方法，如：

1. 大写： upper/isupper

   ```python
   v = 'DEng'     
   v1 = v.upper()    # DENG
   print(v1)
   v2 = v.isupper() # 判断是否全部是大写
   print(v2)  # False
   ```

2. 小写：lower/islower

   ```python
   v = 'yixin'
   v1 = v.lower()
   print(v1)
   v2 = v.islower() # 判断是否全部是小写
   print(v2)
   
   
   ############ 了解即可
   v = 'ß'  #德语大小写的转换
   # 将字符串变小写（更牛逼）
   v1 = v.casefold()
   print(v1) # ss   
   v2 = v.lower()
   print(v2)
   #lower() 方法只对ASCII编码，也就是‘A-Z’有效，对于其他语言（非汉语或英文）中把大写转换为小写的情况只能用 casefold() 方法。
   ```

3. 判断是否是数字： isdecimal(推荐使用)

   ```python
   v = '1'
   # v = '二'
   # v = '②'
   v1 = v.isdigit()  # '1'-> True; '二'-> False; '②' --> True
   v2 = v.isdecimal() # '1'-> True; '二'-> False; '②' --> False
   v3 = v.isnumeric() # '1'-> True; '二'-> True; '②' --> True
   print(v1,v2,v3)
   # 以后推荐用 isdecimal 判断是否是 10进制的数。
   
   # ############## 应用 ##############
   v = ['deng','yi','xin']
   num = input('请输入序号：')
   if num.isdecimal():
       num = int(num)
       print(v[num])
   else:
       print('你输入的不是数字')
   ```

4. split() ,去空白+\t+\n + 指定字符串

   ```python
   # 注意;该方法只能删除开头或是结尾的字符，不能删除中间部分的字符.类似的有rstip/lstrip,从字符串右端/左端来操作.
   v1 = "alex "
   print(v1.strip())
   v2 = "alex\t"    #\t等价于 Tab键,4个空格
   print(v2.strip())
   v3 = "alex\n"     #\n等价于 换行符
   print(v3.strip())
   v1 = "dnengyixin"
   print(v1.strip('dn'))  #  输出'engyixi'     # 只要头尾包含有指定字符序列中的字符就删除
   ```

5. 替换 replace

   ```python
   v = 'dengyixin'
   v1 = v.replace('i','123')
   print(v1)  #  dengy123x123n  ,从左到右替换所有字符
   v = 'dengyixindengyixin'
   v1 = v.replace('i','123',3)
   print(v1)  # dengy123x123ndengy123xin  ,从左到右替换前 3 个字符字符.
   ```

6. 开头 / 结尾startswith/endswith

   ```python
   v = 'dengyixindengyixin'    #判断以``字符开头/结尾
   flag1 = v.startswith('de')
   flag2 = v.endswith('in')
   print(flag1,flag2)   #  True  True
   ```

7. 编码encode，把字符串转换成二进制

   ```python
   
   v = '邓益新'   # 解释器读取到内存后，按照unicode编码方式存储
   v1 = v.encode('utf-8')
   print(v1)               #输出'邓益新'以utf-8码的二进制
   v2 = v1.decode('gbk')
   print(v2)    #报错,编码问题,以什么码写,就用什么码读.
   # 解码 decode，把二进制转换成字符串
   
   ```

8. format(格式化)

   ```python
   name = "我叫{0},年龄:{1}".format('老男孩',73)    #  对应的是索引位置
   print(name) 
   name = '我是{x1},爱{xx}'.format_map({'x1':"邓",'xx':18})    #format_map对应的键
   print(name)
   print(name)
   ```

9. join

   ```python
   name = 'dengixn'
   result = "_".join(name)       # 循环每个元素，并在元素和元素之间加入'_'。
   print(result)    #  d_e_n_g_i_x_n
   ```

10. split 

   ```python
   str.split("根据什么东西进行切割"，从左到右对前多少个东西进行切割)
   str.rsplit("根据什么东西进行切割"，从右到左对前多少个东西进行切割)
   name = 'dengixindeng'
   name_new = name.split('n')  #不填数字默认切割所有,从左到右切割.
   print(name_new)  #  ['de', 'gixi', 'de', 'g']
   ```

### 3.4 列表

#### 3.4.1列表介绍

```python
users = ["代永进","李林元","邓益新",99,22,88]   #表示多个事物，用列表.是有序的,可变类型
```

#### 3.4.2列表方法

1. append，在列表的最后追加一个元素

   ```python
   users = []
   while True:
       name = input('请输入姓名:')   #  利用无限循环添加用户名到列表users
       users.append(name)
       print(users)
   ```

2. insert,在列表索引位置添加元素

   ```python
   users = ["代永进","李林元","邓益新",99,22,88]
   users.insert(2,'大笨蛋')    #  在列表索引2位置添加'大笨蛋'
   ```

3. remove,从左到右删除列表第一个元素

   ```python
   users = ["代永进","李林元","邓益新",99,22,88]
   users.remove("邓益新")     #   从列表中从左到右删除第一个"邓益新",
   ```

4. pop,删除列表索引位置元素,可以赋值给一个新变量,得到被删除的值.

   ```python
   users = ["代永进","李林元","邓益新",99,22,"邓益新",88]
   result =  users.pop(2)
   print(result,users)#   从列表中删除对应索引位置 /邓益新 ['代永进', '李林元', 99, 22, '邓益新', 88]
   
   ```

5. clear,清除列表所有元素

   ```python
   users = ["代永进","李林元","邓益新",99,22,88]
   users.clear()    #   从列表清除所有元素    ,得到[]
   ```

6. reverse,反转列表元素

   ```python
   users = ["代永进","李林元","邓益新",99,22,88]
   users.reverse()
   print(users)  #  [88, '邓益新', 22, 99, '邓益新', '李林元', '代永进']
   ```

7. sort,从小到大排列列表元素

   ```python
   users = [1,2,3,8,4,6,5,9,0]
   users.sort()   # ()里默认reverse = False
   print(users)   # [0, 1, 2, 3, 4, 5, 6, 8, 9]
   users.sort(reverse = True)     # 从大到下排列列表元素
   ```

8. extent.用于在列表末尾一次性追加另一个序列中的多个值 

   ```python
   users = ["代永进","李林元","邓益新",99,22,88]
   users.extend('deng')
   users.extend([11,22,33])
   users.extend((44,55,66))
   users.extend({77,88,99})
   print(users)   # ['代永进', '李林元', '邓益新', 99, 22, 88, 'd', 'e', 'n', 'g', 11, 22, 33, 44, 55, 66, 88, 99, 77],在后面添加.
   ```

### 3.5元组tuple

#### 3.5.1元组的特点

1. ```python
   users = ["代永进","李林元","邓益新",99,22,88] # 列表可变类型[]
   users = ("代永进","李林元","邓益新",99,22,88) # 元组不可变类型()
   ```

2. 元组没有自己独有的方法.

3. 元组可索引,切片,步长, 不可删除,修改 ..

4. 注意:元组中的元素(儿子)不可被修改/删除

   ```python
   # 元组可以嵌套，可以有list数据,多重嵌套
   v1 = (11,22,33,(44,55,66),(11,2,(99,88,),[666,999,888],["代永进","李林元","邓益新"]))
   注意：元组中嵌套列表，
        元组：只读列表，可循环查询，可切片。儿子不能改，孙子(列表的元素)可能可以改
   
   v2 = (1,2,3,'alex',[2,3,4,'abnowen'],'deng')
   v2[3] = 666      # 错误
   v2[4][3] = 123   #正确
   print(v2)        #  (1, 2, 3, 'alex', [2, 3, 4, 123], 'deng')
   ```

### 3.6字典dict

#### 3.6.1字典介绍

- 是一种可变容器模型，且可存储任意类型对象 ，帮助用户去表示一个事物的信息。各种属性。

- 用{}表示,如

  ```python
  d = {key1 : value1, key2 : value2,键:值 }    #  键值对
  # 键一般是唯一的，如果重复最后的一个键值对会替换前面的。
  # 值可以取任何数据类型，但键必须是不可变的，如字符串，数字或元组，键不可以是列表和字典。
  ```

#### 3.6.2字典方法介绍

1. keys / values / items /

   ```python
   dict = {'Name': 'deng', 'Age':18, 'Class': 'First'}
   print(dict.keys())   #获取dict中所有键  
   print(dict.values())   ##获取dict中所有值
   print(dict.items())    #获取dict中所有键值对
   ```

2. ***get(key, default=None)，获取某键的值，如果字典中不包含该键，就返回得到None或自定义内容***

   ```python
   info = {'k1':'v1','k2':'v2'}
   v1 = info.get('k1')       #   v1
   v2 = info.get('k123456')    #  None,数据类型，该类型表示空（无任何功能，专门用于提供空值）字典中不存在该键是默认得到None
   v3 = info.get('k123478','不存在该键')  #不存在该键时,可自定义返回的值
   print(v1,v2,v3)   #    v1 None 不存在该键  
   ```

3. pop(key),删除字典的键值对.如果字典中没有该键，会报错。

   ```python
   info = {'k1':'v1','k2':'v2'}
   result = info.pop('k1')      #返回得到  'v1'
   print(info)                #{'k2':'v2'}
   ```

4. .update(), 字典添加元素,可添加多个.

   ```python
   info = {'k1':'v1','k2':'v2'}
   info.update({22:33}) 
   info.update({'k1':'123456'})   #  没有的键，键值对就添加进去，有的键，键对应的值就更新
   print(info)   #  {'k1': '123456', 'k2': 'v2', 22: 33}
   ```

### 3.7集合set()

#### 3.7.1集合介绍

- 集合无序，元素唯一，不允许重复.
- 用{},为和字典做区分,空集合表示为set(),类似于d = {0,1,2,3,4}或者d = set()
- 引申:
  - 其他数据类型的也可以类似表示,如:srt()/list()/tuple()/dict()/int()等等

#### 3.7.2集合方法

1. add,为集合添加单个元素

   ```python
   tiboy = set()
   tiboy.add('boy')
   print(tiboy)  #  tiboy = {1,2,3,4,56,7,8,9,'boy'}  不存在元素就添加.存在该元素集合不变/
   ```

2. .update(x), ,添加元素.x 可以是列表，元组，字典

   ```python
   print(tiboy)    #  update  x 可以是列表，元组，字典。
   tiboy.update([(1,2,3,4,5,6,7),'deng','yi','xin',1,2,3])
   print(tiboy)   
   #  {1, 2, 3, 4, 7, 8, 9, 'xin', 'deng', 'yi', 56, (1, 2, 3, 4, 5, 6, 7), 'boy'}   列表/字典/集合不能放在集合中，但元组可以。
   ```

3. .discard(x)  ,移除集合的元素x，若集合没有x，返回值为None，区别于.remove(x) ,若集合没有x,则会报错。.clear()  清除集合所有元素。

   ```python
   tiboy = {1,2,3,4,56,7,8,9,'boy'}
   tiboy.discard(1)
   print(tiboy,t)   # {2, 3, 4, 7, 8, 9, 'boy', 56}
   ```

4. .intersection()    /     .union( )      /     .difference()   /.symmetric_difference（求交集，并集 差集)

   ```python
   A = {1,2,3,4,5,6}
   B = {5,6,7,8,9,10}
   集合1和2的并集 =  A.union(B)
   print(集合1和2的并集)    #A∪B
   集合1减去2的差集 = A.difference(B)
   print(集合1减去2的差集)    # 集合A-B
   集合1和2的交集 = A.intersection(B)
   print(集合1和2的交集)    #A∩B
   deng123 = A.symmetric_difference(B)
   print(deng123)      #  A∪B-A∩B
   ```

### 3.8 公共功能

- len(),求长度.(排除 int  / bool)
  - str,求字符的个数.len("deng") = 4 / len("邓益新") = 3
  - 对列表/元组/集合,求元素的个数
  - 对字典,求键值对的个数

- 索引 ( int / bool  /set  )
  - 从左到右,从0开始,取前不取后.
  - str , 'deng'[0] = 'd'  /  deng'[2] = 'n'   /也可以最后一位,以-1为起始.'deng'[-1]  = 'g'
  - 对列表/元组,类似于字符串,索引对应里面的元素,   因集合无序,不具有索引功能.
  - 对字典,索引的是它的键,得到键对应的值.  {}[key] = value

- 切片 ( int / bool  /set  /dict)
  - 通过索引取单个元素,切片可以取一片
  - str ,'deng'[0:2] = 'den'   ,其他列表/元组与此类似.
  - 字典 无切片 功能,  集 合 无序,没有切片功能.

- 步长  ( int / bool  /set  /dict )
  - str ,'dengyixin'[::2]  = 'dnyxn'  每2个字符取前一个,步长为2.步长为负数,则反转,从右到左切片.
  - list / tuple / 都与此类似.
  - 字典和集合没有此功能

- for循环
  - for i in str/list/tuple/set ,都是对里面的元素循环一次

  - for item in dict,这里指的是字典的键.

  - 注意：for和while的应用场景：有穷尽优先使用for，无穷尽用while。

    ```python
    name = 'dengxin'
    for item in name:
        print(item)    # 竖向打印d e n g x i n
        
    name = 'dengxin'   
    for item in name:
        print(item)
        break           # for循环的break语句
        print('123')
        
    name = 'dengxin'
    for item in name:
        print(item)
        continue        # for循环的continue语句
        print('123')
    ```

- 删除del    del user[索引]     数字/字符串/布尔值除外

- 1. 索引（排除：int/bool）

     ```python
     users = (11,22,33,"老男孩")
     
     print(users[0])
     print(users[-1])
     ```

  2. 切片（排除：int/bool）

     ```python
     users = (11,22,33,"老男孩")
     print(users[0:2])
     ```

  3. 步长（排除：int/bool）

     ```python
     users = (11,22,33,"老男孩")
     print(users[0:2:2])
     ```

  4. 删除（排除：tuple/str/int/bool）

  5. 修改（排除：tuple/str/int/bool）

  6. for循环（排除：int/bool）

     ```python
     users = (11,22,33,"老男孩")
     for item in users:
         print(item)
     ```

  7. len（排除：int/bool）

     ```python
     users = (11,22,33,"老男孩")
     print(len(users))
     ```

  

### 3.9额外内容

#### 3.9.1判断敏感字符

- str： 使用in 即可判断

- list/tuple

  ```python
  list1 = ['alex','oldboy','deng','xin']
  if 'deng' in list1：
  	print('含敏感字符')   #  同样，对于元组也是这样
  
  ```

- dict

  ```python
  dict1 = {'k1':'v1','k2':'v2','k3':'v3'}
  #默认按照键判断
  if 'x' in dict1 ：     #  判断x是否是字典的键。
  	print('dict1的键包含x')
      
  #判断字典的值x   #第一种
  dict1 = {'k1':'v1','k2':'v2','k3':'v3'}
  if 'x' in list(dict1.values()）:    #  ，强制转化为list判断x是否是字典的值。
      print('dict1的值包含x')   
      
  #判断字典的值v2   #第二种循环
  for v in dict1.values()：
  	if v == 'v2'
      print('存在')
                 
  #判断字典的键值对 k2:v2 是否在其中
  value = list1.get('k2')
  if value == 'v2':
  	print('包含')   
  ```

### 3.10 嵌套

#### 3.10.1列表嵌套

- 列表可以嵌套多层,int、str、bool、list都可以有

#### 3.10.2元组嵌套

- 元组可以嵌套，可以有list数据,多重嵌套

- 元组和列表可混合嵌套

- 注意:元组中的元素(儿子)不可被修改/删除

  ```python
  # 元组可以嵌套，可以有list数据,多重嵌套
  v1 = (11,22,33,(44,55,66),(11,2,(99,88,),[666,999,888],["代永进","李林元","邓益新"]))
  注意：元组中嵌套列表，
       元组：只读列表，可循环查询，可切片。儿子不能改，孙子(列表的元素)可能可以改
  
  v2 = (1,2,3,'alex',[2,3,4,'abnowen'],'deng')
  v2[3] = 666      # 错误
  v2[4][3] = 123   #正确
  print(v2)        #  (1, 2, 3, 'alex', [2, 3, 4, 123], 'deng')
  ```

#### 3.10.3字典嵌套(重点)

- **列表/字典/集合（可变类型） -> 不能放在集合中+不能作为字典的key（unhashable）**

- hash函数，在内部会将值进行哈希算法并得到一个数值（对应内存地址），以后用于快速查找。值必须不可变。

- 特殊情况，True和False在集合和字典可能会和0， 1 重复。hash函数对2者得到一样的值。如：

  ```python
  
  info = {0, 2, 3, 4, False, "国风", None, (1, 2, 3)}
  print(info)  # {0, 2, 3, 4, "国风", None, (1, 2, 3)}
  
  info1 = {
       True:'alex',
       1:'oldboy'
   }
  print(info1)      #  {True:'oldboy'}
  ```

### 3.11内存问题

- 变量

  1. 第一次赋值时，即创建它，之后赋值将会改变变量的值。
  2. 变量名本身是没有类型的，类型只存在对象中，变量只是引用了对象而已。

- 对象

  1. 对象是有类型的，例如各种数据类型。
  2. 对象是分配的一块内存空间，来表示它的值。

- 引用

  在Python中从变量到对象的连接称作引用。
  引用是一种关系，以内存中的指针的形式实现。

  1. 简单引用

     ```python
     v1 = [11,22,33]  #解释器创建了一块内存空间（地址），v1指向这里，v1引用此内存位置
     v1 = [44,55,66] 
     v1 = [1,2,3]    # v1被多次赋值，即修改了v1的引用，把v1的指向关系变了，指向改为另一块内存地址。
     v2 = [1,2,3]   #解释器创建了另一块内存空间（地址），v2指向这里。
     v1 = 666   # 同理，v1，v2指向不同的内存地址
     v2 = 666
     v1 = "asdf"   # 特殊，对于赋值给字符串，v1，v2指向一个内存地址                  （字符串不可变）
     v2 = "asdf"
     
     v1 = 1 
     v2 = 1   #特殊：v1，v2指向相同的内存地址
              #解释：1. 整型：  -5 ~ 256 ，py认为它是常用的，有缓存机制，就把v1和v2指向相同的内存地址，以节省内存。
              #####  2. 字符串："alex",'asfasd asdf asdf d_asd'（字符串不可变 ），指向同一内存地址   
              #####  3."f_*" * 3  - 重新开辟内存,一旦字符串*数字,数字不为1的运算，就重新开辟内存空间
     ```

  2. 共享引用

     ```python
     #示例一
     a = 3  #解释器创建了一块内存空间（地址），a指向此处
     b = a  # b也指向次内存地址
     #示例二(内部修改)
     v1 = [11,22,33]   #解释器创建了一块内存空间（地址），v1指向此处
     v2 = v1  #v2也指向这里，如果语句为：v2 = [11,22,33]，则是解释器创建了另一块内存空间（地址），v2指向这里。只是v1和v2的值相同。
     v1.append(123) #v1指向的内存地址添加上来元素666，v2跟着变更。
     print(v2)     # [11,22,33,123] 
     #示例三（赋值）
     v1 = [11,22,33]
     v2 = [11,22,33]
     v1.append(123)
     print(v2)     #  [11,22,33] 
     #示例四（重新赋值）
     v1 = [11,22,33]  # v1指向一个内存地址
     v2 = v1  # v2指向同一个内存地址
     v1 = [44,55,66,77]  #v1的指向关系改变，解释器创建了内存地址，并将v1的指向改为指向它。
     print(v2)  # v2的指向没有变化，输出[11,22,33]
     ```

  3. == 和 is d的区别(重点)

     - == 用于比较值是否相等

     - is 用于比较内存地址是否相等/比较指向关系是否相同/比较指向的内存空间是否是同一个。

       ```python 
       id(v1)   # 返回得到 变量名v1指向的内存地址
       ```

     - 小总结

       - 变量是一个系统表的元素，拥有指向对象的连接的空间。
       - 对象是分配的一块内存，有足够的空间去表示它们所代表的值。
       - 引用是自动形成的从变量到对象的指针

### 3.12 深浅拷贝

1. 简介

   ```python
   v = [1,2,3,[4,5,6],7,8,9]
   import copy
   v1 = copy.copy(v)               #浅拷贝
   v2 = copy.deepcopy(v)           #深拷贝
   ```

2. 对str\int\bool类型而言,深浅拷贝没有区别.(不可变)

   ```python
   a = 'deng'   #a指向内存中第一个内存地址
   import copy
   b = copy.copy(a)   #b指向内存中第一个内存地址
   c= copy.deepcopy(a)   #c指向内存中第一个内存地址
   #注意:这里变量abc理应指向不同的内存地址,但因为小数据尺的缘故,内存地址相同.
   ```

3. list/dict/set类型来说,如果没有嵌套,深浅拷贝没有区别.(可变)

   ```python
   v = [1,2,3,[4,5,6],7,8,9]  有嵌套,有7个元素
   v1 = [1,2,3,4,5,6,7,8,9]   没嵌套,有9个元素
   import copy
   v2 = copy.copy(v1)               
   v3 = copy.deepcopy(v1)           
   
   ```

4. 对嵌套的list/dict/set类型来说,深浅拷贝才有意义.

   ```python
   v = [1,2,3,[4,5,6],7,8,9]  有嵌套,有7个元素
   import copy
   v1 = copy.copy(v)                 
   v2 = copy.deepcopy(v)
   ```

   - 对于其中的元素都是不可变类型时，深拷贝和浅拷贝的结果都是一样的，都是只拷贝第一层
   - 对于其中元素存在可变类型时，浅拷贝只拷贝第一层，深拷贝要拷贝所有的可变类型

5. 特殊:tuple元组(不可变)

   - 如果元组中不含有可变类型，同理字符串的深浅拷贝.
   - 如果元组中含有可变类型，同理列表的深浅拷贝.

6. 深拷贝和浅拷贝

   - 浅拷贝：只拷贝第一层
   - 深拷贝：拷贝嵌套层次中的所有可变类型
   - 拷贝只针对可变类型:在内存中重新创建内存空间,对不可变数据类型无法操作.

7. 可变数据类型和不可变数据类型

   - 可变数据类型：在id不变的情况下，value可改变（列表/字典/集合是可变类型，但是字典中的key值必须是不可变类型） 
   - 不可变数据类型：value改变，id也跟着改变。（数字，字符串，布尔类型，都是不可变类型） 



## 第四章 文件操作

### 4.1 文件基本操作

```python
obj = open('路径',mode='模式',encoding='编码')   #  打开文件
obj.write()    #  把内容写入文件
obj.read()     #  读取文件内容
obj.close()    #  关闭文件(保存文件,把内存上的数据写入到文件上-->硬盘上,01010101存储的)

# 3步:1.打开文件.  2. 操作文件   3. 关闭文件
```

### 4.2打开文件

#### 4.2.1语句

```python
file = open('文件路径',mode = 'r',encoding = 'utf-8')
# 文件路径:     D:\文件夹名字\文件夹
# encoding = 'utf-8',以utf-8编码方式打开.
#########或者用另外一种语句:
with open(''文件路径',mode = 'r',encoding = 'utf-8') as file:
#或者 同时打开2个文件.下面的代码需要缩进,且不需要.close()语句.
with open(''文件路径',mode = 'r',encoding = 'utf-8'') as file ,open(''文件路径',mode = 'r',encoding = 'utf-8'') as file2 :
	v1 = file.read()
    v2 = file2.read()
```

#### 4.2.2打开文件的模式(mode)

- r / w / a      只读只写字符串

- r+ / w+ / a+   可读可写字符串

  1. 读取：r，只能读文件,默认的模式.文件不存在就报错.
  2. 写入：w，只能写文件，文件不存在则创建，文件存在则清空内容在写入. .
  3. 追加：a，只能追加,文件不存在则创建，文件存在则不会覆盖，写内容会以追加的方式写 (写日志文件的时候常用 ).
  4. 可读可写：r+
     - 读：默认从0的光标开始读，也可以通过 seek 函数调整光标的为位置
     - 写：从光标所在的位置开始写，也可以通过 seek 调整光标的位置
  5. 可读可写：w+
     - 读：默认光标永远在写入的最后，也可以通过 seek 函数调整光标的位置
     - 写：先清空
  6. 可读可写：a+
     - 读：默认光标在最后，也可以通过 seek 函数 调整光标的位置。然后再去读取
     - 写：永远写到最后

- rb / wb / ab  只读只写二进制

  ```python
  file = open('文件路径',mode = 'wb')  # rb/wb/ab模式,不需要encoding = 'utf-8'
    #注意,如果是/rb/ab/wb模式,写入和读取的必须是二进制,即010100010 010110101 0101000,否则报错.写入数据是
  data = '你好,世界'.encode('utf-8')    #将字符串转化为utf-8编码方式的二进制数据.
  file.write(data)
  file.close()  
  print(v)
  ########
  ```

- r+b / w+b / a+b   可读可写二进制  ,  r+b/w+b/a+b模式同上.

### 4.3 文件操作

- read() , 全部读到内存

- read(1) 

  - 1表示一个字符

    ```python
    obj = open('a.txt',mode='r',encoding='utf-8')
    data = obj.read(1) # 1个字符
    obj.close()
    print(data)
    ```

  - 1表示一个字节

    ```python
    obj = open('a.txt',mode='rb')
    data = obj.read(3) # 1个字节
    obj.close()
    ```

- readlins()  

  ```python
  date_list = file.readlines() #  读取整个文件所有行，保存在一个列表(list)变量中，每行作为一个元素，但读取大文件会比较占内存.
  ```

- 大文件读取(50g的文件,内存没有这么大)

  ```python
  file = open('文件路径',mode = 'r',encoding = 'utf-8')
  ###  2.如果以后读取一个特别大的文件，可以一行一行读取
  for line in file:
      line = line.strip()     #去除换行符(默认去除换行符\n),也可以填其他.
      print(line)             #一行一行读取,
  ```

- write(字符串,utf-8)

  ```python
  obj = open('a.txt',mode='w',encoding='utf-8')
  obj.write('中午你')
  obj.close()
  ```

- write(二进制)

  ```python
  obj = open('a.txt',mode='wb')
  
  # obj.write('中午你'.encode('utf-8'))
  v = '中午你'.encode('utf-8')
  obj.write(v)
  
  obj.close()
  ```

- seek函数:(光标字节位置) **用于移动文件读取光标到指定位置**，无论模式是否带b，都是按照字节进行处理。

  ```python
  obj = open('a.txt',mode='r',encoding='utf-8')
  obj.seek(3) # 跳转到指定3字节位置
  data = obj.read()
  obj.close()
  
  print(data)
  ##################################################
  obj = open('a.txt',mode='rb')
  obj.seek(3) # 跳转到指定字节位置
  data = obj.read()
  obj.close()
  
  print(data)
  ######################################
  fileObject.seek(offset[, whence])
  #  offset -- 开始的偏移量，也就是代表需要移动偏移的字节数.以字节为单位.
  #  whence：可选，默认值为 0。给offset参数一个定义，表示要从哪个位置开始偏移；0代表从文件开头开始算起，1代表从当前位置开始算起，2代表从文件末尾算起。如seek(0,0)
  ```

- tell(), 获取光标当前所在的字节位置

  ```python
  obj = open('a.txt',mode='rb')
  # obj.seek(3) # 跳转到指定字节位置
  obj.read()
  data = obj.tell()
  print(data)
  obj.close()
  ```

- flush，强制将内存中的数据写入到硬盘

  ```python
  v = open('a.txt',mode='a',encoding='utf-8')
  while True:
      val = input('请输入：')
      v.write(val)
      v.flush()      # 避免内存泄漏
  
  v.close()
  ```

### 4.4 关闭文件

一般方法

```python
v = open('a.txt',mode='a',encoding='utf-8')
# 文件操作
v.close()
```

避免忘记输入.close()的方法.

```python
with open('a.txt',mode='a',encoding='utf-8') as v:
    v.write('这是另一种文件打开方法')
	# 缩进中的代码执行完毕后，自动关闭文件
```

### 4.5  文件内容的修改

```python
with open('a.txt',mode='r',encoding='utf-8') as f1:
    data = f1.read()
new_data = data.replace('飞洒','666')

with open('a.txt',mode='w',encoding='utf-8') as f1:
    data = f1.write(new_data)
```

- 大文件修改

```python
f1 = open('a.txt',mode='r',encoding='utf-8')
f2 = open('b.txt',mode='w',encoding='utf-8')

for line in f1:
    new_line = line.replace('阿斯','死啊')
    f2.write(new_line)
f1.close()
f2.close()
```

```python
with open('a.txt',mode='r',encoding='utf-8') as f1, open('c.txt',mode='w',encoding='utf-8') as f2:
    for line in f1:
        new_line = line.replace('阿斯', '死啊')
        f2.write(new_line)
```

## 第五章 函数

### 5.1三元运算/三目运算

```python
v = 前面  if 条件语句  else   后面
#如果条件成立,"前面"赋值给v,否则后面赋值给v.
v = a if a>b  else b  # 取a和b中值较大的赋值给v

# 让用户输入值，如果值是整数，则转换成整数，否则赋值为None

data = input('请输入值:')
value =  int(data) if data.isdecimal() else None 
```

### 5.2 函数

#### 5.2.1.函数介绍

1. 截止目前为止,都是面向过程式编程.可读性差,重复性高,容易出错.

2. 对于函数编程：

   - 本质：将N行代码拿到别处并给他起个名字，以后通过名字就可以找到这段代码并执行。
   - 场景：
     - 代码重复执行。
     - 代码量特别多超过一屏，可以选择通过函数进行代码的分割。

3. 函数是组织好的，可重复使用的，用来实现单一，或相关联功能的代码段。

   函数能提高应用的模块性，和代码的重复利用率.

   py3中给我们提供了许多内建函数,如len()/print()等等,我们也可以自定义函数,这叫做用户自定义函数 .

#### 5.2.2.函数定义和表达

1. 定义如下:

   - 函数代码块以 **def** 关键词开头，后接函数标识符名称和圆括号 **()**。

   - 任何传入参数和自变量必须放在圆括号中间，圆括号之间可以用于定义参数。

   - 函数的第一行语句可以选择性地使用文档字符串—用于存放函数说明。

   - 函数内容以冒号起始，并且缩进。

   - **return [表达式]** 结束函数，选择性地返回一个值给调用方。不带表达式的return相当于返回 None。

     注意:  遇到return语句,后面的代码不执行.

2. Python 定义函数使用 def 关键字，一般格式如下： 

   ```python
   def 函数名称(形参): #算列表中所有元素的和,括号中可以添加形式参数.简称形参,也可以不加.
       函数代码
   ```

   - 最简单的形式

     ```python
     def hello() :
        print("Hello World!")  #定义函数
     
     hello()  # 调用函数,输出"Hello World!"
     
     ```

   - 复杂一些,加上参数

     ```python
     #  计算列表中的元素的和
     def sum_list_element(list1):
         sum = 0
         for i in list1:
             sum += 1
          print(sum)
         
     #  调用函数
     sum_list_element([1,2,3,4,5,6,7,8,9])  # 输出45
             
     
     ```

   - 加上return的应用(重点) --> 返回多个值自动装换为元组

     ```python
     # 比较2个数的大小,并返回得到较大的数
     def compare_num(a,b):
         v = a if a > b else b
         return v   # 如果没有return语句,  print(compare_num(5,10)) 输出的是None
     print(compare_num(5,10))   # 调用函数,得到a,b中较大的数,并打印出来.
     
     
     #函数没有返回值时,默认返回None
     #函数有返回值时,语句为:return 返回值.返回值可以是任意东西.
     def compare_num(a,b):
         return a if a>b else b  #和三目运算一起,返回a和b中较大的数.
     #############################################
     #  函数执行到return 语句,就返回,后面的语句不执行.
     #  若返回值为多个,默认将其添加到元组中,返回一个元组.
     def func(a,b,c,d)
     	return a,b,c,d
     
     v = func(1,'邓益新',[11,22,33,44,55],{'k1':'v1'})
     print(v)            # (1,'邓益新',[11,22,33,44,55],{'k1':'v1'})  元组,
     ```



#### 5.2.3函数参数解析

1. 参数

   ```python
   def func(a1,a2):
       函数体(可调用a1和a2)
       return 返回值
   # 严格按照顺序传参数：位置方式传参。
   # 实际参数可以是任意类型。
   
   def check_age(age):  #只有1个参数:age,参数数量可以是无穷多个.
       if age >= 18:
           print('你是成年人')
       else:
           print('你是未成年人')
           
   check_age(18)    #调用函数,输出你是成年人.
   
   ```

2. 位置参数

   ```python
   def func(a1,a2,a3,a4)
   	print(a1,a2,a3,a4)
     
   func(1,2,3,4)
   # 严格按顺序输入参数,顺序是固定的.
   ```

3. 关键字传参

   ```python
   def func(a1,a2)
   	print(a1,a2)
     
   func(a1 = 1 ,a2 = 2) 
   func(a2 = 2,a1 = 1)  # 俩者完全一样,全是关键字可以打乱顺序.
   ```

4. 位置参数和关键字传参混用

   ```python
   ######  这时必须位置参数在前,关键字参数在后
   def func(a1,a2,a3,a4)
   	print(a1,a2,a3,a4)
      
   func(1,10,a3 = 15,a4 = 88) 
   func(1, 10, a4=15, a3=88)  # 俩者等价,正确.
   func(a1=1,a2=2,a3=3,a4=4)  #正确
   func(a1=1,a2=2,15,a4 = 88)  # 错误
   func(1,2,a3 = 15,88)  #错误
   #################################
   #    必须位置参数在前,关键字参数在后, 位置参数 > 关键字参数  #
   
   ```

5. 默认参数

   ```python
   def func(a1,a2,a3=9,a4=10):   #  默认a3=9,a4=10,这时可以不输入a3 和 a4 参数.默认值为 9 和 10 
       print(a1,a2,a3,a4)
   
   func(11,22)   #正确
   func(11,22,10)  #  正确,修改默认参数a3 = 10 ,参数a4不输入.默认为10 .
   func(11,22,10,100)  #正确
   func(11,22,10,a4=100)   #正确
   func(11,22,a3=10,a4=100)    #正确
   func(11,a2=22,a3=10,a4=100)   #正确
   func(a1=11,a2=22,a3=10,a4=100)    #正确
   
   ```

6. **万能参数(重点)**

   - args(打散)**可以接受任意个数的位置参数，并将参数转换成元组。**

     - 没有调用函数   *    字符

       ```python
       def func(*args):
           print(args)
       func(1,2,3,True,[11,22,33,44])
       #  输出为(1,2,3,True,[11,22,33,44])  元组
       func(*(1,2,3,4,5)) # 如果不加*号,函数会将序列当成一个元素加入到元组中,*相当于拆包.
       ```

     - 有调用函数   *    字符

       ```python
       def func(*args):
           print(args)
       func(*(1,2,3,True,[11,22,33,44]))    
       #  输出为(1,2,3,True,[11,22,33,44])  元组
       ```

   - *args  只能用位置传参

     ```python
     def func4(*args):
         print(args)
         
     func4(1)    元组(1,)
     func4(1,2)   元组(1,2)
     
     func4(1,2)
     func4(1,2)
     
     ```

   - **kwags(打散)  可以接受任意个数的关键字参数，并将参数转换成字典。

     - 没有调用函数   **    字符

       ```py
       def func(**kwargs):   
           print(kwargs)
       
       func(k1=1,k2="alex")
       ```

     - 有调用函数   **    字符

       ```python
       def func(**kwargs):
           print(kwargs)
       func(**{'k1':'v2','k2':'v2'}) # kwargs={'k1':'v2','k2':'v2'}func(*(1,2,3,4,5)) # 如果不加**号,函数会将序列当成一个元素加入到元组中,相当于只有键没没有值,就会报错.
       ```

     - **kwags(打散)只能用关键字传参 

#### 5.2.4函数参数默认值使用可变类型--->有坑(重点)

1. 补充：对于函数的默认值慎用可变类型。

```python
# 如果要想给value设置默认是空列表

# 不推荐(坑)
def func(data,value=[]): 
    pass 

# 推荐
def func(data,value=None):
    if not value:
        value = []

```

```python
def func(data,value=[]): 
    value.append(data)
    return value 

v1 = func(1) # [1,]
v2 = func(1,[11,22,33]) # [11,22,33,1]
```

- 原因

  ```python
  def func(data,value=[]): 
      value.append(data)
      return value 
  
  v1 = func(1) # [1,]     # 调用函数,作用域得到value=[],添加value=[1,]
  v2 = func(2)  # [1,2,]   #调用函数,value=[1,]再次添加元素2,
  
  v2 = func(1,[11,22,33]) # [11,22,33,1]
  
  ##############    def func(a,b=[]) 有什么陷阱？   #######
  答: Python函数在定义的时候。默认参数b的值就被计算出来了，即[],因为默认参数b也是一个变量，它指向对象即[],每次调用这个函数，如果改变b的内容每次调用时候默认参数也就改变了，不在是定义时候的[]了.应该换成None  .
  
  ```

  

#### 5.2.5函数参数混用

```python
def func(*args,**kwargs):
    print(args,kwargs)

# func(1,2,3,4,5,k1=2,k5=9,k19=999)
func(*[1,2,3],k1=2,k5=9,k19=999)
func(*[1,2,3],**{'k1':1,'k2':3})
func(111,222,*[1,2,3],k11='alex',**{'k1':1,'k2':3})

```

#### 5.2.6函数作用域

1. 函数的作业域

   - 作用于全局,对任意一个py文件来说,全局作用域

   - 对函数来说:局部作用域

     ```python
     #  在函数中定义的值,只在局部作用域中
     #  在全局作用域中找不到#
     a = 1
     b = 2
     def func():
         a = 10
         b = 20
         print(a) 
         print(b)
     func()     #  输出10  ,20
     print(a)
     print(b)   #输出 1    , 2
     
     
     ```

2. 作用域查找数据规则::优先在自己的作用域找数据，自己没有就去  "父级" ->  "父级" -> 直到全局，全部么有就报错。

   ```python
   a = b = 1
   def func():
       b = 5
       print(a,b)
   func()     #  1 ,5
   print(a,b)  #   1, 1
   ```

3. 子作用域只能找到父级作用域中的变量的值,不能重新为赋值父级作用域的变量赋值.

   - 例外,特别:  global 变量名,强制把全局的变量赋值

     ```python
     a = b = 1
     def func():
     	global b
         b = 5     # 更改全局变量b = 5
         print(a,b)
     func()     #  1 ,5
     print(a,b)  #   1, 5
     ```

   - nonlocal   ,强制把父级的变量赋值 ,一级一级向上找寻,不找全局域.找不到则报错.

     ```python
     a = b = 1
     def func():
     	nonlocal b
         b = 5     # 更改父级变量,如果找不到,再到父级的父级找寻,一直找到全局域之前(不包含全局域)
         print(a,b)
     func()      #会报错,父级就是全局域,nonlocal不包含全局域,故会报错,找不到b 
     print(a,b)  
     
     a = b = 1
     def func():
         a = 3
         b = 99
         print(a,b)
         def func1():
             nonlocal b
             b = 88
             print(a,b)
         func1()
     func()
     print(a,b)
     
     ```

   - 补充: 全局变量大写

4. 递归函数(效率低下,不推荐)

   ```python
   # 递归的返回值
   ##函数在内部调用自己--->  递归函数
   # 效率很低,python对栈,即函数递归的次数有限制-->1000次
   # 尽量不要使用 递归函数
   # 递归的返回值
   def func(a):
       if a == 5:
           return 100000
       result = func(a+1) + 10
   
   v = func(1)
   name = 'alex'
   def func():
       def inner():
           print(name)
        return inner
   v =func()
   ```

   ```python
   def func(i):
       print(i)
       func(i+1)
       
   func(1)
   
   
   递归次数有限制 1000次
   ```

   

#### 5.2.7执行函数

- 函数不被调用，内部代码永远不执行。

  ```python
  def func():
      return i
  func_list = []
  for i in range(10):
      func_list.append(func)
  
  print(i) # 9
  v1 = func_list[4]()
  v2 = func_list[0]()
  ```

  ```python
  func_list = []
  for i in range(10):
      # func_list.append(lambda :x) # 函数不被调用，内部永远不执行（不知道是什么。）
      func_list.append(lambda :i) # 函数不被调用，内部永远不执行（不知道是什么。）
  
  print(func_list)
  
  func_list[2]()=
  ```

  - 执行函数时，会新创建一块内存保存自己函数执行的信息 => 闭包

    ```python
    def base():
        return i 
    
    def func(arg):
        def inner():
            return arg
        return inner
    
    base_list = [] # [base,base,]
    func_list = [] # [由第一次执行func函数的内存地址，内部arg=0 创建的inner函数，有arg=1的inner函数 ]
    for i in range(10): # i = 0 ，1
        base_list.append(base)
        func_list.append(func(i))
        
    # 1. base_list 和 func_list中分别保存的是什么？
    """
    base_list中存储都是base函数。
    func_list中存储的是inner函数，特别要说的是每个inner是在不同的地址创建。
    """
    # 2. 如果循环打印什么？
    for item in base_list:
    	v = item() # 执行base函数
        print(v) # 都是9
    for data in func_list:
        v = data()
        print(v) # 0 1 2 3 4 
    ```

总结：

- 传参：位置参数 > 关键字参数 
- 函数不被调用，内部代码永远不执行。 
- 每次调用函数时，都会为此次调用开辟一块内存，内存可以保存自己以后想要用的值。
- 函数是作用域，如果自己作用域中没有，则往上级作用域找。 



### 5.3 函数高阶

#### 5.3.1 函数中高阶(重点)

1. 函数的变换

   ```python
   def func():
       print('小米手机真的好')  #  这里func--------->指向函数的内存地址
   print(id(func))   #  33627808
   a = '小米手机真的好'
   print(id(a))     #  35196048
   #  函数的名字和变量类似,有类似的指向关系.变量指向一块内存地址放置数据,函数也指向一块内存地址放置数据
   
   ```

2. 函数名当做变量使用

   ```python
   def func():
       print('小米手机真的好')  
   print(id(func))   #  33627808
   a = '小米手机真的好'
   print(id(a))     #  35196048
   
   fuck_world = func
   print(id(fuck_world))    #33627808 ,2者内存地址一致.
   fuck_world()  # 调用函数,打印'小米手机真的好'
   
   
   ```

3. 函数名作为列表/元组/字典元素(是不可变类似,可以做字典的键<-------->str)

   ```python
   def func1():
       print('快活啊')
       
   func_list = [func1, func1, func1]
   func_list[0]()
   func_list[1]()
   func_list[2]()   # 类似的用法,调用函数运行,注意: 加上括号就可以运行该函数了
   for item in func_list:
       v = item()    #  函数语句没有retun,返回值默认为None
       print(v)   
   ```

4. 函数可以当作参数进行传递    --->构造字典和函数的对应关系,避免重复if else 

   ```python
   def func3(arg):
       v = arg()
       print(arg())
       print(arg)
   
   
   def show():
       return 999
   
   func3(show)    # 调用函数,把show函数当做参数使用.
   
   ```

   - 面试题相关

     ```python
     def func3():
         print('话费查询')
     def func4():
         print('流量办理')
     def func5():
         print('充值')
     def func6():
         print('人工服务')
     
     info = {
         'k1':func3,
         'k2':func4,
         'k3':func5,
         'k4':func6
     }
     
     content = input('请输入你要办理的业务:')
     function_name = info.get(content)
     function_name()          # 运用字典调用函数
     ```

5. 函数可以做返回值

   ```python
   def func():
   	print('name')
       
   def show()
   	return func
   # ******************  
   v = show()
   
   v()   # 执行打印name     
   #********************
   show()()  # 相当于show()()
   ```

6. 函数的闭包--->封装了值,内层函数调用才是闭包,缺一不可 .

   - 闭包概念：为函数创建一块区域并为其维护自己数据，以后执行时方便调用。【应用场景：装饰器 / SQLAlchemy源码】

     ```python
     name = 'oldboy'
     def bar(name):
         def inner():
             print(name)
         return inner
     v1 = bar('alex') # { name=alex, inner }  # 闭包，为函数创建一块区域（内部变量供自己使用），为他以后执行提供数据。
     v2 = bar('eric') # { name=eric, inner }  ,2者不干扰运行.
     v1()
     v2()
     
     # 不是闭包
     def func1(name):
         def inner():
             return 123
         return inner 
     
     # 是闭包：封装值 + 内层函数需要使用。
     def func2(name):
         def inner():
             print(name)
             return 123
         return inner 
     ```

7. 数据类型中那些有返回值

   ```python
   # 无返回值
     v = [11,22,33]
     v.append(99) # 无返回值
     
   # 仅有返回值：
     v = "alex"
     result = v.split('l')
     
     v = {'k1':'v2'}
     result1 = v.get('k1')
     result2 = v.keys()
   # 有返回+修改数据 (少)
     v = [11,22,33]
     result = v.pop()
   ```

8. 常用数据类型方法返回值(重点)

   ```python
   - str
     - strip，返回字符串
     - split，返回列表
     - replace，返回字符串
     - join，返回字符串。
   - list
     - append，无
     - insert，无
     - pop，返回要删除的数据
     - remove，无
     - find/index，返回索引的位置。
   - dict
     - get
     - keys
     - values
     - items
   ```

   

#### 5.3. 2 lambda函数(匿名函数)

用于表示非常简单的函数,就如三元运算和if 语句一样.

- lambad函数只能用一行代码

  ```python
  # lambda表达式，为了解决简单函数的情况，如：
  
  def func(a1,a2):
      return a1 + 100 
  
  func = lambda a1,a2: a1+100       #   2者等价,注意冒号后接代码,自动return 
  
  ```

- 应用

  ```python
  func = lambda a,b:a if a > b else b
  t =  func(1,5)
  print(t)   #  求2个数里面较大的数
  
  # 练习题1
  USER_LIST = []
  def func0(x):
      v = USER_LIST.append(x)
      return v 
  
  result = func0('alex')
  print(result)
  
  
  # 练习题2
  
  def func0(x):
      v = x.strip()
      return v 
  
  result = func0(' alex ')
  print(result)
  
  ############## 总结：列表所有方法基本上都是返回None；字符串的所有方法基本上都是返回新值 
  
  ```

### 5.4  python内置函数简介

#### 5.4.1自定义函数

#### 5.4.2内置函数

- 输入输出print  input

- 强制转换类型

  - dict()
  - list()
  - tuple()
  - int()
  - str()
  - bool()
  - set()

- 其他

  - len
  - open
  - range
  - id
  - type

- 数学相关

  - abs  求绝对值

  - float ,转换为浮点型(小数)

  - max  /min

  - sum  求和

  - divmod  ,获取2数字相除的商和余数

  - pow ,指数函数,获取指数

    ```python
    v = pow(2,5)
    print(v)  # 2^5 = 32
    
    ```

  - round函数,四舍五入函数

    ```python
    v= round(1.24879,2) #小数点后取2位
    print(v)  #  1.25
    ##############################################
    # 注意:由于计算机精度问题,round函数可能会出错
    round(2.355,2)   # 算得的是 2.35  ,py版本3.6.8
    ```

- 编码相关

  - chr()  ,将十进制转化unicode编码中对应的字符串

    ```python
    v =chr(152)
    print(v)
    
    i = 1
    while i < 1000:
        print(chr(i))
        i += 1
    ```

    

  - ord(), 根据字符在unicode编码的对应关系,找到对应的十进制.

    ```python
    v= ord("字")   #  一次只能索引一个字符
    print(v)
    
    content = """一天，我正在路上走着，突然一声
    雄浑有力的声音传进我的耳朵里。"""
    for i in content:
        print(ord(i))
    ```

  - 应用:   随机验证码(重点)

    ```python
    import random     # 调用随机数函数
    
    def get_random_code(length=6):
        data = []
        for i in range(length):
            v = random.randint(65,90)  # 65-90对应的是ABC-----Z
            data.append(chr(v)
        return  ''.join(data)
    
    code = get_random_code()
    print(code)
    ```

- 进制转换

  - bin() 10进制转为2进制   ,''0b"

  - oct()   10进制转为8进制   ." 0o"

  - hex()    10进制转为16进制    ."0x"

  - int()    把````数转为10进制数    ,base默认为10 ,转二进制数base = 2,base = 8,  base = 16

    ```python
    num= 192
    n1 = bin(num)
    n2 = oct(num)
    n3 - hex(num)
    
    print(n1,n2,n3)
    ```

    ```python
    # 二进制转化成十进制
    v1 = '0b1101'
    result = int(v1,base=2)
    print(result)
    
    # 八进制转化成十进制
    v1 = '0o1101'
    result = int(v1,base=8)
    print(result)
    
    # 十六进制转化成十进制
    v1 = '0x1101'
    result = int(v1,base=16)
    print(result)                #  注意,转换时0b   0o   0x可以不用加
    ```

#### 5.4.3 高级函数(必考)

- map()  循环每个元素（第二个参数），然后让每个元素执行函数（第一个参数），将每个函数执行的结果保存到新的列表中，并返回。

  第一个参数 function 以参数序列中的每一个元素调用 function 函数，返回包含每次 function 函数返回值的新列表。 

  ```python
  content = [11,22,33,44,55,66,77,88,99]
  t = map(lambda x: x*x,content)
  print(list(t))  #列表元素的个数是相同的
  ```

- filter(筛选/过滤)**filter()** 函数用于过滤序列，过滤掉不符合条件的元素，返回由符合条件元素组成的新列表。

  该接收两个参数，第一个为函数，第二个为序列，序列的每个元素作为参数传递给函数进行判，然后返回 True 或 False，最后将返回 True 的元素放到新列表中。

  ```python
  content = [11,0,33,55,66,77,88,99]
  t = filter(lambda x: x < 50,content)
  print(list(t))  #列表元素的个数是相同的
  ```

- reduce()**reduce()** 函数会对参数序列中元素进行累积。

  函数将一个数据集合（链表，元组等）中的所有数据进行下列操作：用传给 reduce 中的函数 function（有两个参数）先对集合中的第 1、2 个元素进行操作，得到的结果再与第三个数据用 function 函数运算，最后得到一个结果。

  ```python
  import functools
  
  t = [3,2,3,4,5,6,8,9]
  v = functools.reduce(lambda x,y: x ** y,t)
  print(v)
  ```

  **注意**: 在py3中,reduce的函数移到了functools模组,要用这个功能,必须 import functools

### 5.5  装饰器(重点)

装饰器：在不改变原函数内部代码的基础上，在函数执行之前和之后自动执行某个功能。 

#### 5.5.1装饰器

```python
示例:
# #########################################
def func(arg):
    def inner():
        arg()
    return inner

def f1():
    print(123)

v1 = func(f1)
v1()
# ###########################################
def func(arg):
    def inner():
        arg()
    return inner

def f1():
    print(123)
    return 666

v1 = func(f1)   # 运行func(),创建一块空间,定义 inner函数.返回值 inner
result = v1() # 执行inner函数 / f1含函数 -> 123 
print(result) # None
# ###########################################
def func(arg):
    def inner():
        return arg()
    return inner

def f1():
    print(123)
    return 666

v1 = func(f1)
result = v1() # 执行inner函数 / f1含函数 -> 123
print(result) # 666
```

```python
示例:
# #########################################
def func(arg):
    def inner():
        arg()
    return inner

def f1():
    print(123)

v1 = func(f1)
v1()
# ###########################################
def func(arg):
    def inner():
        arg()
    return inner

def f1():
    print(123)
    return 666

v1 = func(f1)   # 运行func(),创建一块空间,定义 inner函数.返回值 inner
result = v1() # 执行inner函数 / f1含函数 -> 123 
print(result) # None
# ###########################################
def func(arg):
    def inner():
        return arg()
    return inner

def f1():
    print(123)
    return 666

v1 = func(f1)
result = v1() # 执行inner函数 / f1含函数 -> 123
print(result) # 666

```

#### 5.5.2装饰器的本质

```python
def func(arg):
    def inner():
        print('before')  
        v = arg()
        print('after')
        return v 
    return inner 

def index():
    print('123')
    return '666'

index = func(index)  # --->指向inner函数.
index()    # ---> 运行inner函数-->运行打印'before'/原index函数()/运行打印'after'

```

1. 装饰器的格式

   ```python
   def func(arg):
       def inner():
           v = arg()
           return v 
       return inner 
   
   # 第一步：执行func函数并将下面的函数参数传递，相当于：func(index)
   # 第二步：将func的返回值重新赋值给下面的函数名。 index = func(index)
   @func    #  @func等价于上面2步
   def index():
       print(123)
       return 666
   
   print(index)
   
   ```

2. 应用

   ```python
   # 计算函数执行时间
   def wrapper(func):
       def inner():
           start_time = time.time()
           v = func()
           end_time = time.time()
           print(end_time-start_time)
           return v
       return inner
   
   @wrapper
   def func1():
       print(123454987+865484)
       
   func1()   # --->自动启动装饰器
       
   
   ```

3. 总结

   - 目的：在不改变原函数的基础上，再函数执行前后自定义功能。 

   - 编写装饰器 和应用

     ```python
     # 装饰器的编写
     def x(func):
         def y():
             # 前
             ret = func()
             # 后
             return ret 
        	return y 
     
     # 装饰器的应用
     @x
     def index():
         return 10
     
     @x
     def manage():
         pass
     
     # 执行函数，自动触发装饰器了
     v = index()
     print(v)
     
     
     @xx  # index = xx(index)
     def index():
         pass
     index()
     ```

   - 应用场景：想要为函数扩展功能时，可以选择用装饰器。

4. 重点内容装饰器(重点)

   ```python
   def 外层函数(参数): 
       def 内层函数(*args,**kwargs):   # -->无论被装饰函数有多少个参数,都能用万能参数传入.
           return 参数(*args,**kwargs)
       return 内层函数
   
   @外层函数
   def index():
       pass
   
   index()    # --->自动调用装饰器
   ```

5. 装饰器建议写法

   ```python
   def x1(func):
       def inner(*args,**kwargs):
           data = func(*args,**kwargs)
           return data
       return inner 
   
   def x1(func):
       def inner(*args,**kwargs):
           print('调用原函数之前')
           data = func(*args,**kwargs) # 执行原函数并获取返回值
           print('调用员函数之后')
           return data
       return inner 
   
   @x1
   def index():
       print(123)
       
   index()  
   ```

#### 5.5.3带参数的装饰器

1. 基本格式

   ```python
   def wrapper(func):
       def inner(*args,**kwargs):
           
           v = func()
           
           return v
       return i
   ```

2. 装饰器加上参数(外层函数必须以函数名作为参数,要给装饰器加上参数,在外层再套一层函数)

   ```python
   def super_wrapper(counter):  # counter 装饰器参数
       print(123)
       def wrapper(func):
           print(456)
           def inner(*args, **kwargs):  # 参数统一的目的是为了给原来的index函数传参
               data = []
               for i in range(counter):
                   v = func(*args, **kwargs)  # # 参数统一的目的是为了给原来的index函数传参
                   data.append(v)
               return data
           return inner
       return wrapper
   
   @super_wrapper(9):    # 这时已经打印 123   456   # # func = 原来的index函数u
   											# index = inner
                                     # 已经运行super_wrapper函数和wrapper函数
           
   def index(i):
       print(i)
       return i+1000
   
   print(index(456))          # 调用index函数(带装饰器)
   ```

   ```python
   # ################## 普通装饰器 #####################
   def wrapper(func):
       def inner(*args,**kwargs):
           print('调用原函数之前')
           data = func(*args,**kwargs) # 执行原函数并获取返回值
           print('调用员函数之后')
           return data
       return inner 
   
   @wrapper
   def index():
       pass
   
   # ################## 带参数装饰器 #####################
   def x(counter):
       def wrapper(func):
           def inner(*args,**kwargs):
               data = func(*args,**kwargs) # 执行原函数并获取返回值
               return data
           return inner 
   	return wrapper 
   
   @x(9)
   def index():
       pass
   
   ```

   - 基本装饰器更重要8成
   - 带参数的装饰器2成
   - 装饰器本质上就是一个python函数,它可以让其他函数在不需要任何代码变动的前提下增加额外的功能 ，装饰器的返回值也是一个函数对象，她有很多的应用场景，比如：插入日志，事物处理，缓存，权限装饰器就是为已经存在的对象 添加额外功能 

### 5.6 推导式

#### 5.6.1列表推导式

- 目的: 为了方便生成一个列表

  ```python
  #####基本格式#####
  v1 = [i for i in range(9)]    # 创建列表10个元素
  v2 = [i for i in range(9) if i > 5]   #对每个i,判断if i > 5,为真则添加到列表中
  v3 = [99 if i > 55 else 66 for i in range(9)]   # 三木运算
  """
  v1 = [i for i in 可迭代对象 ]
  v2 = [i for i in 可迭代对象 if 条件 ] # 条件为true才进行append
  """
  ######函数######
  
  def func():
      return i
  v6 = [func for i in range(10)]  # 10个func组成的列表,内存地址一致.
  result = v6[5]()     # 运行报错,提示 i 没有被定义,
  
  v7 = [lambda :i for i in range(10)]  # 10个lambda函数组成的列表,内存地址不一致,有10个
  result = v7[5]()   # 运行不报错,输出result 为 9
  
  ## 第一个func和列表生成器函数作用域都在第一级,找不到 i 
  ## 第二个lambda上级是列表生成器函数作用域
  
  
  ###面试题
  v8 = [lambda x:x*i for i in range(10)] # 新浪微博面试题
  # 1.请问 v8 是什么？    10个lambda函数,内存地址不一样,构建的列表
  # 2.请问 v8[0](2) 的结果是什么？    i 在列表生成式函数作用域里面,lambda是奇子集,i = 9,输出18
  
  ### 面试题
  def num():
      return [lambda x:i*x for i in range(4)]
  # num() -> [函数,函数,函数,函数]
  print([ m(2) for m in num() ]) # [6,6,6,6]
  
  ```

  

#### 5.6.2集合推导式

- 和列表推导式类似,这不再赘述.

#### 5.6.3字典推导式

```python
v1 = { 'k'+str(i):i for i in range(10) }  # 和列表类似,中间有:区分键和值.
```



### 5.7 迭代器

#### 5.7.1自己不用写迭代器,只.

```python
# 一一展示列表中的元素
# 1.for 循环    2. while + 索引 + 计数器
# 使用迭代器--->对 某种对象(str/list/tuple/dict/set类创建的对象)-可迭代对象中的元素进行逐一获取，表象：具有`__next__`方法且每次调用都获取可迭代对象中的元素（从前到后一个一个获取）。
# 可被for 循环的---> 可迭代对象


#***************************************************************#
v1 = [1,2,3,4]
v2 = iter(v1)    # --->v2是一个迭代器
#或者  v2 = v1.__iter__()

# 迭代器想要获取每个值:-->反复调用
while 1 :
	val = v1.__next__() 
    print(val)
 
##### *********   ######
v3 = "alex"
v4 = iter(v1)
while True:
    try:
        val = v2.__next__()
        print(val)
    except Exception as e:
        break

        
        
# 直到报错：StopIteration错误，表示已经迭代完毕。

```

1. 判定是否是迭代器  --> 内部是否有`__next__方法` 

2. for 循环---->为这简便有了for 循环

   ```python
   v1 = [11,22,33,44]
   
   # 1.内部会将v1转换成迭代器
   # 2.内部反复执行 迭代器.__next__()
   # 3.取完不报错
   
   for item in v1:
       print(item)
   ```

#### 5.7.2可迭代对象

- 内部具有 `__iter__()` 方法且返回一个迭代器。（*）

  ```python
  v1 = [11,22,33,44]
  result = v1.__iter__()
  ```

- 可以被for循环

#### 5.7.3 小结

- 迭代器，对可迭代对象中的元素进行逐一获取，迭代器对象的内部都有一个 __next__方法，用于以一个个获取数据。
- 可迭代对象，可以被for循环且此类对象中都有 __iter__方法且要返回一个迭代器（生成器）。

### 5.8 生成器

生成器(变异函数---->特殊的迭代器)

生成器，函数内部有yield则就是生成器函数，调用函数则返回一个生成器，循环生成器时，则函数内部代码才会执行。

1. 函数样式

   ```python
   # 函数
   def func():
       return 123
   func()
   
   ```

2. 生成器

   ```python
   # 生成器函数（内部是否包含yield）---->只要有yeild就是生成器,不论是否有return
   def func():
       print('F1')
       yield 1
       print('F2')
       yield 2
       print('F3')
       yield 100
       print('F4')
   # 函数内部代码不会执行，返回一个 生成器对象 。
   v1 = func()
   # 生成器是可以被for循环，一旦开始循环那么函数内部代码就会开始执行。
   for item in v1:
       print(item)
   ```

- 总结：函数中如果存在yield，那么该函数就是一个生成器函数，调用生成器函数会返回一个生成器，生成器只有被for循环时，生成器函数内部的代码才会执行，每次循环都会获取yield返回的值

  ```python
  def func():
      count = 1
      while 1 :
          yield count
          count += 1
          if count == 100 :
              return
  val = func()     # --->调用生成器函数 ---->返回值时一个生成器 --->内部代码没有执行
  for item in val :
      print(item)  # 此时才执行生成器内部代码,并yield.就冻结,直到下次再继续运行.
  ```

  - 示例 读文件

    ```python
    def func():
        """
            分批去读取文件中的内容，将文件的内容返回给调用者。
        """
        cursor = 0
        while 1 :
            f =  open(r'D:\Python学习\python\s21day16\goods.txt','r',encoding='utf-8-sig')
            f.seek(cursor)
            data_list = []                       # 光标移到最前
            for i in range(10):
                line = f.readline()             # 一次读一行
                if not line:
                    return
                data_list.append(line)
            cursor = f.tell()             #获取光标
            f.close()                             # 关闭与redis的连接
            for row in data_list:
                yield  row
    
    
    for itemm in func():
        p = input('qingsr123456498')
        print(itemm)
    ```

- range 函数说明：range([start,] stop[, step])，根据start与stop指定的范围以及step设定的步长，生成一个序列。 

- xrange 函数说明：用法与range完全相同，所不同的是生成的不是一个数组，而是一个生成器。 



## 第六章 模块

### 6.1 模块初步(import)

```python
# import 模块名  # 调用py的内置模块,
# PY调用hashlib模块对用户的密码进行加密,hashlib模块有很多种加密方式.这里选用MD5加密

import hashlib

def md5(arg):  # 加密函数   
    hash  = hashlib.md5('dengyxiin'.encode('utf-8'))    # 加盐,避免给黑客撞库得到密码.
    hash.update(arg.encode('utf-8'))
    return hash.hexdigest()     # 返回加密的md5,<----->t = hash.hexdigest();return t

def register(user,pwd):  # 注册函数,参数为用户名和密码
    with open('data.txt','a',encoding='utf-8') as file:    #  追加模式写入文件
        file.write(user+'|'+md5(pwd)+'\n')  #用户名和密码用竖杠隔开,方便以后找寻

def login(user,pwd):  # 登录函数
    with open('data.txt','r',encoding='utf-8') as file:    # 以只读模式打开文件,遍历文件,
        for line in file:
            line = line.strip()
            line = line.split('|')
            if user == line[0] and md5(pwd) == line[1]:    #遍历存储登录数据的文件,看能否比对的上.
                return  True

item = input('1表示注册 ,2表示登录 . 请输入')
if  not item.isdecimal():
    print('输入错误字符,请重新输入')
item = int(item)
if item ==1:
    user = input('请输入用户名:')
    pwd = input('请输入密码:')
    register(user,pwd)
elif item ==2:
    user = input('请输入用户名:')
    pwd = input('请输入密码:')
    v = login(user,pwd)
    if v:
        print('登陆成功')
    else:
        print('登陆失败')
else:
    print('输入错误序号,请重新输入')
```

```python
#  让密码在键入的时候隐藏
import getpass

pwd = getpass.getpass('请输入密码：')
if pwd == '123456789':
    print('输入正确')

    
import time
print(time.time())
time.sleep(2)    # --> 睡 2秒

```

#### 6.1.1 sys.模块

```python
### py  解释器相关的数据
import sys
sys.getrefcount()   # 获取一个值的引用次数,用得少,不重要
a = [11,22,33]
b = a
print(sys.getrefcount(a))

###sys.getrecursionlimit()
print(sys.getrecursionlimit())  # 1000次,python默认支持的递归数量.实际使用用个人的电脑配置有关

###sys.stdout.write  --> print    (进度)
import time
for i in range(100):
    msg = '进度条跑到%s%%\r' %i    # \r 光标回到当前行的起始位置(不是覆盖.,是清空)
    print(msg , end = '')
    time.sleep(0.5)  # 暂停0.5s后再运行
    
### sys.agrv  #### 获取用户执行脚本时，传入的参数。得到的是一个列表,第一个元素是脚本文件.py
##  Sys.argv[ ]其实就是一个列表，里边的项为用户输入的参数，关键就是要明白这参数是从程序外部输入的，而非代码本身的什么地方，要想看到它的效果就应该将程序保存了，从外部来运行程序并给出参数。
```

```python
#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
让用户执行脚本传入要删除的文件路径，在内部帮助用将目录删除。
C:\Python36\python36.exe D:/code/s21day14/7.模块传参.py D:/test
C:\Python36\python36.exe D:/code/s21day14/7.模块传参.py

"""
import sys

# 获取用户执行脚本时，传入的参数。
# C:\Python36\python36.exe D:/code/s21day14/7.模块传参.py D:/test
# sys.argv = [D:/code/s21day14/7.模块传参.py, D:/test]
path = sys.argv[1]

# 删除目录
import shutil
shutil.rmtree(path)
```

```python
import os

# 1. 读取文件大小（字节）
file_size = os.stat('20190409_192149.mp4').st_size

# 2.一点一点的读取文件
read_size = 0
with open('20190409_192149.mp4',mode='rb') as f1,open('a.mp4',mode='wb') as f2:
    while read_size < file_size:
        chunk = f1.read(1024) # 每次最多去读取1024字节
        f2.write(chunk)
        read_size += len(chunk)
        val = int(read_size / file_size * 100)
        print('%s%%\r' %val ,end='')
```

1. sys.argv
2. sys.path    # 默认Python去导入模块时，会按照sys.path中的路径挨个查找。 ---> 本质是列表
3. sys.exit( 0 )    是正常退出 sys.exit( 1) 异常退出

#### 6.1.2 os 模块

和操作系统相关的数据。

- os.path.exists(path)      ， 如果path存在，返回True；如果path不存在，返回False
- os.stat('20190409_192149.mp4').st_size  ， 获取文件大小
- os.path.abspath()   ， 获取一个文件的绝对路径

```python
import os

os.path.exists(path)      #如果path存在，返回True；如果path不存在，返回False.文件路径

os.stat('20190409_192149.mp4').st_size   #  获取文件大小(字节大小)

os.path.abspath()   #  获取一个文件的绝对路径

os.path.dirname # 获取路径的上级目录(文件夹)
```

- **os.path.join ，路径的拼接** 

  ```python
  import os
  path = "D:\code\s21day14" # user/index/inx/fasd/
  v = 'n.txt'     
  
  result = os.path.join(path,v)
  print(result)        # # D:\code\s21day14\n.txt
  result = os.path.join(path,'n1','n2','n3')
  print(result)      # D:\code\s21day14\n1\n2\n3
  ```

- os.listdir ， 查看一个目录下所有的文件【第一层】

  ```python
  import os
  
  result = os.listdir(r'D:\code\s21day14')     # r,在字符创=串前面,转义符,把\n\t\r当做字符串.
  for path in result:
      print(path)
  ```

- **os.walk ， 查看一个目录下所有的文件【所有层】**

  ```python
  import os
  
  result = os.walk(r'D:\code\s21day14')
  for a,b,c in result:
      # a,正在查看的目录 b,此目录下的文件夹  c,此目录下的文件
      for item in c:
          path = os.path.join(a,item)
          print(path)   # 还有其他很多用途
  ```

- 转义符的使用

  ```python
  v1 = r"D:\code\s21day14\n1.mp4"  (推荐)
  print(v1)
  
  
  v2 = "D:\\code\\s21day14\\n1.mp4"
  print(v2)
  ```

- os.makedirs,创建目录和子目录--->比makedir 好用,只能创建一层目录

  ```python
  import os
  file_path = r'E:\小莫的黄片库\绝对不能偷看\请输入密码\****.MP4'
  
  file_folder = os.path.dirname(file_path)
  if not os.path.exists(file_folder):
      os.makedirs(file_folder)
  
  with open(file_path,mode='w',encoding='utf-8') as f:
      f.write('asdf')
  
  ```

- os.rename  重命名

  ```python
  import os
  os.rename('db','sb')    # 原文件 ,现文件
  ```

  

#### 6.1.3 shutil 模块

- 删除目录

  ```python
  import shutil
  shutil.rmtree(path)
  ```

- 

  ```python
  import shutil
  
  
  ##删除目录(只能是文件夹)
  shutil.rmtree('test')
  
  ## 重命名
  shutil.move(old,new)
  
  ##压缩文件
  shutil.make_archive('压缩后文件名可以加文件路径','压缩格式zip等等','被压缩的文件/文件夹')
  
  shutil.make_archive('zzh','zip','D:\code\s21day16\lizhong')
  
  ##解压文件
  shutil.unpack_archive('要解压的文件夹',extract_dir=r'D:\code\xxxxxx\xxxx',格式:'zip')
   #                                    解压的路径
  shutil.unpack_archive('zzh.zip',extract_dir=r'D:\code\xxxxxx\xxxx',format='zip')
  
  ```

  举例

  ```python
  import shutil
  
  shutil.rmtree('4567')     # 删除目录(只能是文件夹)
  
  shutil.move(前文件名,后文件名)   # 重命名/移动,看后一个文件是文件夹还是文件名
  
  shutil.make_archive(r'E:\159','zip','456')   # 把py文件目录的456压缩到E:\159,即E盘的根目录下159.zip
  
  shutil.unpack_archive('159.zip',r'F:\159\158','zip')  # 把159.zip解压文件到F:\159\158,解压格式是zip.
  ```

#### 6.1.4 json 模块

1. 用法

   - son的本质(为着各程序间信息的传递而规定的一种通用格式)

     1. 本质是一个字符串(看起来像[],{},int/ bool )
     2. 最外层必须是list/dict
     3. 如果包含字符串,必须双引号--->其他程序字符串只识别双引号

     用法

     ```python
     import json
     # 序列化，将python的值转换为json格式的字符串。
     # v = [12,3,4,{'k1':'v1'},True,'asdf']
     # v1 = json.dumps(v)
     # print(v1)
     json.dumps()    ---->返回值是json格式--->字符串
     
     
     # 反序列化，将json格式的字符串转换成python的数据类型
     # v2 = '["alex",123]'
     # print(type(v2))
     # v3 = json.loads(v2)
     # print(v3,type(v3))
     json.loads()   ---->返回值是python能识别的格式,外层一定是[]或{}
     
     ```

     python与json的对照表

     ```python
         +-------------------+---------------+
         | Python            | JSON          |
         +===================+===============+
         | dict              | object        |
         +-------------------+---------------+
         | list, tuple       | array         |
         +-------------------+---------------+
         | str               | string        |
         +-------------------+---------------+
         | int, float        | number        |
         +-------------------+---------------+
         | True              | true          |
         +-------------------+---------------+
         | False             | false         |
         +-------------------+---------------+
         | None              | null          |
         +-------------------+---------------+
     ```

2. - dumps  把python数据转换为json型的字符串  序列化

   - loads 把json字符串转换为python能够识别的数据类型  反序列还

   - 注意 :若字典或列表中有中文,序列化是想要保留中文显示

     ```python
     v = {'k1':'alex','k2':'李杰'}   #e nsure_ascii=False
     
     import json
     val = json.dumps(v,ensure_ascii=False)
     print(val)
     
     ```

   - dump(内容,文件句柄)   有2个参数

     ```python
     import json
     
     v = {'k1':'alex','k2':'李杰'}
     
     f = open('x.txt',mode='w',encoding='utf-8')
     val = json.dump(v,f)
     print(val)
     f.close()
     
     ## Json.dump()这个方法：它在底层做了两件事，一件事是将对象（列表）转换为字符串，第二件事是转换## 成功以后，将转换后的数据写入到文件中。
     
     ```

   - load

     ```python
     import json
     
     v = {'k1':'alex','k2':'李杰'}
     
     f = open('x.txt',mode='r',encoding='utf-8')
     
     data = json.load(f)
     f.close()
     
     print(data,type(data))
     
     # 做了两件事，一件事：先来读取文件里的内容，第二件事是：将读取出来的内容进行数据类型的转换。
     ```

#### 6.1.5 pickle 模块

- json，优点：所有语言通用；缺点：只能序列化基本的数据类型 list/dict/int...   

- pickle，优点：python中所有的东西都能被他序列化（除了socket对象）；缺点：序列化的内容只有python认识。

  ```python
  import pickle
  
  ######################   dumps / loads  #######################3
  v = {1,2,3,4}
  val = pickle.dumps(v)
  print(val)
  data = pickle.loads(val)
  print(data,type(data))
  ########################  dump  / load  用法和json类似,这里不再赘述####
  ```

#### 6.1.6time 和 datetime 模块

1. time模块

   time.time()   时间戳,显示的是1970.1.1.凌晨0.00到现在经过的秒数,得到的是float数据类型

   time.sleep()   程序运行等待的秒数

   time.timezone   :  属 性time.timezone是当地时区（未启动夏令时）距离格林威治的偏移秒数（>0，美洲;<=0大部分欧洲，亚洲，非洲） ,和电脑设置的逝去==时区有关

2. datetime 模块

   ```python
   #!/usr/bin/env python
   # -*- coding:utf-8 -*-
   import time
   from datetime import datetime,timezone,timedelta
   
   # ######################## 获取datetime格式时间 ##############################
   """
   v1 = datetime.now() # 当前本地时间
   print(v1)
   tz = timezone(timedelta(hours=7)) # 当前东7区时间
   v2 = datetime.now(tz)
   print(v2)
   v3 = datetime.utcnow() # 当前UTC时间
   print(v3)
   """
   
   # ######################## 把datetime格式转换成字符串 ##############################
   # v1 = datetime.now()
   # print(v1,type(v1))
   # val = v1.strftime("%Y-%m-%d %H:%M:%S")
   # print(val)
   
   # ######################## 字符串转成datetime ##############################
   # v1 = datetime.strptime('2011-11-11','%Y-%m-%d')
   # print(v1,type(v1))
   
   # ######################## datetime时间的加减 ##############################
   # v1 = datetime.strptime('2011-11-11','%Y-%m-%d')
   # v2 = v1 - timedelta(days=140)
   # date = v2.strftime('%Y-%m-%d')
   # print(date)
   
   # ######################## 时间戳和datetime关系 ##############################
   # ctime = time.time()
   # print(ctime)
   # v1 = datetime.fromtimestamp(ctime)
   # print(v1)
   
   # v1 = datetime.now()
   # val = v1.timestamp()
   # print(val)
   ```

   

### 6.2 模块分类

#### 6.2.1内置模块

1. random

2. hashlib

3. getpass 

4. time

5. copy

6. os

   - os.makedirs
   - os.path.join
   - rename    重命名
   - os.path.dirname
   - os.path.abspath
   - os.path.exists
   - os.stat('文件路径')
   - os.listdir
   - os.walk

7. sys

   - sys.argv
   - sys.path

8. shutil

9. json

   - json 模块

     json的本质(为着各程序间信息的传递而规定的一种通用格式)

     1. 本质是一个字符串(看起来像[],{},int/ bool )
     2. 最外层必须是list/dict
     3. 如果包含字符串,必须双引号--->其他程序字符串只识别双引号

     用法

     ```python
     import json
     # 序列化，将python的值转换为json格式的字符串。
     # v = [12,3,4,{'k1':'v1'},True,'asdf']
     # v1 = json.dumps(v)
     # print(v1)
     json.dumps()    ---->返回值是json格式--->字符串
     
     
     # 反序列化，将json格式的字符串转换成python的数据类型
     # v2 = '["alex",123]'
     # print(type(v2))
     # v3 = json.loads(v2)
     # print(v3,type(v3))
     json.loads()   ---->返回值是python能识别的格式,外层一定是[]或{}
     
     ```

     python与json的对照表

     ```python
         +-------------------+---------------+
         | Python            | JSON          |
         +===================+===============+
         | dict              | object        |
         +-------------------+---------------+
         | list, tuple       | array         |
         +-------------------+---------------+
         | str               | string        |
         +-------------------+---------------+
         | int, float        | number        |
         +-------------------+---------------+
         | True              | true          |
         +-------------------+---------------+
         | False             | false         |
         +-------------------+---------------+
         | None              | null          |
         +-------------------+---------------+
     ```

10. 内置模块补充

    - sys

      1. 

      

#### 6.2.2第三方模块

1. 下载和安装

   pip的使用

   - ```python
     # 把pip.exe 所在的目录添加到环境变量中。
     
     pip install 要安装的模块名称  # pip install requests
     ```

   - python36  -m pip install --upgrade pip 语句的使用

   - python3 -m pip install -U --force-reinstall pip   若pip更新失败,则如此

   - 网络不好一直抱超时错误的话可以换一个源，粘贴下边代码运行
     pip3 install xlrd -i http://pypi.douban.com/simple/ —trusted-host pypi.douban.com

     #### 注意 : 这里的pip  /  pip3     都是自己定义的环境变量

   - 安装完成后，如果导入不成功。

     - 重启pycharm。
     - 安装错了。

2. 你了解的第三方模块：

   - xlrd
   - requests

#### 6.2.3自定义模块

1. 自定义模块的导入

   xxxx.py  # 自定义模块,实现某功能

   ```python
   def func():
       print(123)  #实现打印123
   ```

   x2.py 

   ```python
   # 在x2.py中希望调用xxxx.py的函数
   ####1. xxxx.py 和  x2.py 在同一文件夹下
   import xxxx
   xxxx.func()
   
   ####2. 不在同一文件夹下
   import sys
   sys.path.append('xxxx.p文件的绝对路径')
   import xxxx
   xxxx.func()
   
   #####在cmd中运行x2.py文件
   python3 x2.py
   
   ```

2. 1. 模块和执行的py在同一目录,且需要调用很多函数

      ```python
      # 在1.py文件中调用xxxx.py
      import xxxx
      xxxx.func()     #调用函数
      xxxx.func2()    #调用函数2
      
      
      ```

   2. 包和执行的py在同一目录

      ```python
      from 包 import jd模块
      jd模块.func()    # 调用包内的jd模块的所有函数---
      
      from 包.jd模块 import func
      func()           #调用jd的func函数
      
      ################################
      from 包.jd模块 import func , show ,func2     #可以用都逗号连接
      func()
      show()
      func2() 
      
      
      #################################
      from 包.jd模块 import *  # 调用包.jd模块的全部功能
      
      ```

   3. - 模块和要执行的py文件在同一目录 且 需要 模块中的很多功能时，推荐用： import 模块
      - 其他推荐：from 模块 import 模块       模块.函数()
      - 其他推荐：from 模块.模块 import 函数   函数() 

   4. 注意 : 要从其他路径调用模块时的用法

      ```python
      import sys
      sys.path.append(r'D:\自建模块包\项目1模块')  # ---->把路径添加到python寻找模块时会去寻找的路径中,在调用模块
      import xxxx  # xxxx.py在'D:\自建模块包\项目1模块'目录下
      xxxx.func()
      
      # sys,path 本质是一个列表,是py解释器寻找模块时会去寻找的路径.path指定用于模块搜索路径的字符串列表。
      ```

   5. 模组调用

   6. import 

      - import 模块1                                    模块1.函数()
      - import 模块1.模块2.模块3                模块1.模块2.模块3.函数()

   7. from  xx import xxx

      - from 模块.模块 import 函数		函数()
      - from 模块.模块 import 函数 as  f         f()
      - from 模块.模块 import *                      函数1()    函数2()
      - from 模块 import  模块                        模块.函数()
      - from 模块 import  模块  as m              m.函数()
      - 特殊情况：
        - import 文件夹                        加载```__init__.py```
      - from 文件 import * 

### 6.3 异常处理

```python
try:         # 试运行try缩进的语句,若出错,运行except Exception as e:后面的语句,避免程序出错.
    val = input('请输入数字:')
    num = int(val) 
except Exception as e:
    print('操作异常') 
```





## 第七章 面向对象

## 第八章 网络编程

## 第九章 并发编程

## 第十章 数据库

## 第十一章 前端开发

## 第十二章 Django框架



## 附录 常见错误和单词

### 

### 错误记录

1. 编码错误unicode/decode error![截图python0408164437](Python学习笔记.assets/截图python0408164437.png)

注意: 有\x,表面他是十六进制,因为二机制太长,用十六进制表达德更多.

2. 参数位置错误

   ![1555753962628](Python学习笔记.assets/1555753962628.png)

## 附录:邮箱发送示例

![1555753477758](Python学习笔记.assets/1555753477758.png)

![1555753491522](Python学习笔记.assets/1555753491522.png)

![1555753505290](Python学习笔记.assets/1555753505290.png)

![1555753528586](Python学习笔记.assets/1555753528586.png)





~~~python
###假如：管理员/业务员/老板用的是同一个邮箱。# #
def send_email(to):
	import smtplib
    from email.mime.text import MIMEText
    from email.utils import formataddr
msg = MIMEText('导演，我想演男一号，你想怎么着都行。', 'plain', 'utf-8')
msg['From'] = formataddr(["李邵奇", '15776556369@163.com'])
msg['To'] = formataddr(["导演", to])
msg['Subject'] = "情爱的导演"

server = smtplib.SMTP("smtp.163.com", 25)
server.login("15776556369@163.com", "qq1105400511")
server.sendmail('15776556369@163.com', [to, ], msg.as_string())
server.quit()


"""
def send_email(to):
    template = "要给%s发送邮件" %(to,)
    print(template)



user_input = input('请输入角色：')

if user_input == '管理员':
    send_email('xxxx@qq.com')
elif user_input == '业务员':
    send_email('xxxxo@qq.com')
elif user_input == '老板':
    send_email('xoxox@qq.com')
        ```
~~~


