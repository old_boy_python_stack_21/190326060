Day04notes

### 一、解释器/编译器

- 补充：编译型语言和解释型语言？

  ```pytohn
  # 编译型：代码写完后，编译器将其变成成另外一个文件，然后交给计算机执行。
  c c++，c#  ，java 
  # 解释型：写完代码交给解释器，解释器会从上到下一行行代码执行：边解释边执行。 【实时翻译】python，php，ruby
  ```

### 二、字符型数据str的变换

1. 独有功能

   ```python
   str.upper()  # 对str中的字符转为大写，得到str数据。
   str.lower()  # 对str中的字符转为大写，得到str数据。
   str.strip()  # 对str中的字符串首尾去除空格，类似的lstrip和                            rstrip，分别是字符串左边和右边去除空格，得到str数                        据。
   
   str.replace(("被替换的字符/子序列","要替换为的内容",数字)
               #   对str字符串中的前多少个子序列进行替换。得到str数据。
   
   str.split("根据什么东西进行切割"，对前多少个东西进行切割)             
               #得到列表数据。从左到右。str.rsplit则是从右到左切割。 
   ```

   - 独有功能补充
     - startswith   和    endswith

     ```python
     name = 'alex'
     flag = name.startswith('al')
     print(flag)    # 判断name是否以al开头,输出数据为bool值(True/False)
     name = 'alex'
     flag2 = name.endswith('ex')
     print(flag1)   # 判断name是否以ex开头,输出数据为bool值(True/False)
     ```

     -  format(字符串的格式化)

     ```python
     name = "我叫{0},年龄:{1}".format('老男孩',73)    #  对应的是索引位置
     print(name)
     ```

     - encode(以指定的编码格式编码字符串)

       ```python
       
       name = '王飞'           # 解释器读取到内存后，按照unicode编码存储：8个字节,4字节表示一中文字符
       v1 = name.encode('utf-8')
       print(v1)               #输出name代指的字符串以utf- 8编码，     
       v2 = name.encode('gbk')
       print(v2)
       ```

     - join

       ```python
       name = 'alex' 
       result = "**".join(name)       # 循环每个元素，并在元素和元素之间加入连接符。
       print(result)                  # a_l_e_x
       ```

2. 公共功能

   - ```python
     len('这个世界') = 4   #输出长度，输出类型为数字
     
     #索引
     name = '这个世界'
     print(name[0])        # '这'  ,  从做到右，以位置0其起始
     #切片
     name = '这个世界'
     print(name[0：3])     # '这个世'  从做到右，以位置0其起始，取前不取后。   	
     ```

   - 补充

     1. 步长longth

        ```python
        count = '123456789'
        
        val = count[0:-1:2]     # 由1-8，每隔一个字符选中一个，'1357'
        val = count[1:-1:2]     # 由2-8，每隔一个字符选中一个，'246'
        val = count[1::2]       # 由2-9，每隔一个字符选中一个，'2468'
        val = count[::2]        # 由1-9(包含9)，每隔一个字符选中一个，'13579'
        val = count[-1:0:-2]    # 由9-1(包含9，不包含1)，每隔一个字符选中一个，从右向左选择，'9753'
        print(val)
        val = count[::-1]       # 由1-9，反向输出字符串，'987654321'
        print(val)
        ```

     2. for循环

        ```python
        name = 'dengxin'
        for item in name:
            print(item)    # 竖向打印d e n g x i n
            
        name = 'dengxin'   
        for item in name:
            print(item)
            break           # for循环的break语句
            print('123')
            
        name = 'dengxin'
        for item in name:
            print(item)
            continue        # for循环的continue语句
            print('123')
        ```

        注意：for和while的应用场景：有穷尽优先使用for，无穷尽用while。

### 三、列表list

```python
users = ["代永进","李林元","邓益新",99,22,88]   #表示多个事物，用列表
```

- 公共功能

  - len

    ```pytohn
    users = ["代永进","李林元","邓益新",99,22,88]
    val = len(users)
    print(val)      # 5
    ```

  - 索引

    ```python
    users = ["代永进","李林元","邓益新",99,22,88]
    val = users[0]
    print(val)       # "代永进"
    ```

  - 切片

    ```python
    users = ["代永进","李林元","邓益新",99,22,88]
    val = users[0:2]
    print(val)       # ["代永进","李林元"]   也是取前不取后
    ```

  - 删除（数字/布尔/字符串除外）

    ```python
    users = ["代永进","李林元","邓益新",99,22,88]
    # 方式一,    .pop()括号内索引位置，以0起始
    users.pop(1)
    print(users)     #  ['代永进', '邓益新', 99, 22, 88]
    
    # 方式二：
    del users[1]
    print(users)
    ```

    注意：

    - 字符串本身不能修改或删除【不可变类型】
    - 列表是可变类型。

  - 修改（字符串/数字/布尔除外）

    ```python
    users = ["代永进","李林元","邓益新",99,22,88]
    users[2] = 66
    print(users)    # ，'邓益新'修改为66， ['代永进', '李林元', 66, 99, 22, 88]
    
    users[0] = '代永进'
    print(users[0][1])     #  输出"永"
    ```

  - 步长

    ```python
    users = ["代永进","李林元","邓益新",99,22,88]
    val = users[0:4:2]      #   ["代永进","邓益新"]
    ```

  - for循环

    ```python
    users = ["代永进","李林元","邓益新",99,22,88]
    for i in users:
        print(i)     # 循环打印users列表的元素，for循环可嵌套
    ```

- 列表独有功能

  - append，在列表的最后追加一个元素

    ```python
    users = []
    users.append('alex')
    print(users)
    ```

    ```python
    users = []
    while True:
        name = input('请输入姓名:')   #  利用无限循环添加用户名到列表users
        users.append(name)
        print(users)
    ```

  - insert

    ```python
    users = ["代永进","李林元","邓益新",99,22,88]
    users.insert(2,'大笨蛋')    #  在列表索引2位置添加'大笨蛋'
    ```

  - remove

    ```python
    users = ["代永进","李林元","邓益新",99,22,88]
    users.remove("邓益新")     #   从列表中从左到右删除第一个"邓益新"
    ```

  - pop

    ```python
    users = ["代永进","李林元","邓益新",99,22,88]
    users.pop(2)     #   从列表中删除对应索引位置
    ```

  - clear

    ```python
    users = ["代永进","李林元","邓益新",99,22,88]
    users.clear()    #   从列表清除所有元素
    ```

- 总结：

  - 增：

    - append / insert 

  - 删：

    - .remove() / .pop() / .clear() / del  users[2]

  - 改：

    - users[3] = "新值"  ，列表可变，可以被修改。

  - 查：

    - 索引/切片

  - 列表嵌套

      列表可以嵌套多层,int、str、bool、list都可以有

### 四、 元组tuple

1. 元组

   ```python
   users = ["代永进","李林元","邓益新",99,22,88] # 列表可变[]
   users = ("代永进","李林元","邓益新",99,22,88) # 元组不可变()
   ```

2. 公共功能

   1. 索引[]

   2. 切片[:]

   3. 步长[::1]

   4. 删除（排除：tuple/str/int/bool），tuple不能变，不能删除。

   5. 修改（排除：tuple/str/int/bool）

   6. for循环（排除：int/bool）

      ```python
      users = ("代永进","李林元","邓益新",99,22,88)
      for item in users:
          print(item)     # 循环打印出users的元素
      ```

   7. len（排除：int/bool）

      ```python
      users = ("代永进","李林元","邓益新",99,22,88)
      print(len(users))    #  6
      ```

3. 独有功能（tuple没有独有的功能）

4. ***特殊：元组中的元素(儿子)不可被修改/删除。***

   ```python
   # 元组可以嵌套，可以有list数据,多重嵌套
   v1 = (11,22,33,(44,55,66),(11,2,(99,88,),[666,999,888],["代永进","李林元","邓益新"]))
   注意：元组中嵌套列表，
        元组：只读列表，可循环查询，可切片。儿子不能改，孙子(列表的元素)可能可以改
   
   v2 = (1,2,3,'alex',[2,3,4,'abnowen'],'deng')
   v2[3] = 666      # 错误
   v2[4][3] = 123   #正确
   print(v2)        #  (1, 2, 3, 'alex', [2, 3, 4, 123], 'deng')
   ```








