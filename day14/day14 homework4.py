#!/usr/bin/env python
# -*- coding:utf-8 -*-

# 1 .为函数写一个装饰器，在函数执行之后输入 after
"""
def wrapper (arg):
    def inner():
        v = arg()
        print('after')
        return  v
    return inner

@wrapper
def func():
    print(123)

func()
"""


# 2.为函数写一个装饰器，把函数的返回值 +100 然后再返回。
"""
def wrapper(arg):
    def  inner():
        return arg()+100
    return inner


@wrapper
def func():
    return 7

result = func()
print(result)
"""


# 3. 为函数写一个装饰器，根据参数不同做不同操作。
# flag为True，则 让原函数执行后返回值加100，并返回。
# flag为False，则 让原函数执行后返回值减100，并返回。
"""
def x(flag):
    def wrapper(func):
        def inner(*args,**kwargs):
            if flag:
                return func(*args,**kwargs) + 100
            else:
                return func(*args, **kwargs) -100
        return inner
    return wrapper



@x(True)
def f1():
    return 11

@x(False)
def f2():
    return 22

r1 = f1()
"""

# 4. 写一个脚本，接收两个参数。
# 第一个参数：文件
# 第二个参数：内容
# 请将第二个参数中的内容写入到 文件（第一个参数）中。

# # 执行脚本： python test.py  oldboy.txt 你好
"""
# 脚本名python test.py oldboy.txt 你好
import sys
path = sys.argv[1]
data = sys.argv[2]
with open(path,'w',encoding='utf-8') as file :
    file.write(data)
"""
# 5. 递归的次数1000次
"""
print(sys.getrecursionlimit())   # 1000
"""
# 6. 看代码写结果
#
# print("你\n好")   #你   换行     好
# print("你\\n好")     # 你\n好
# print(r"你\n好")      #  你\n好

# 7. 写函数实现，查看一个路径下所有的文件【所有】
"""
import os
result = os.walk('文件路径')
for a ,b ,c in  result:  # a当前目录,b目录下子文件夹  c目录下的文件
    for item in  c :
        print(os.path.join(a,item))
"""

# 8. 写代码
"""
path = r"D:\code\test.pdf"
# # 请根据path找到code目录下所有的文件【单层】，并打印出来。
import  os
result = os.path.dirname(path)    # 上层目录
print(os.listdir(result))     #打印上层目录下的所有文件
"""

# 9.1题
"""
a = 1
b = 2
i = 1  #  --->b 是第1个数
while b < 4000000:
    a,b = b,a+b  # 2,3  #5,8
    i += 1
print(a)   # 3524578
print(i)   # 32个数
"""

# 9.2题
dicta = {'a': 1, 'b': 2, 'c': 3, 'd': 4, 'f': 'hello'}
dictb = {'b': 3, 'd': 5, 'e': 7, 'm': 9, 'k': 'world'}
dictc = {}

for i in  dicta:
    if  i in dictb:
        dictc.update({ i : dicta[i] + dictb[i]})
    else :
        dictc.update({i: dicta[i]})
for i in dictb:
    if  i  not in dicta:
        dictc.update({ i : dictb[i]})
print(dictc)

# 10.
def extend(val,list = []):
    list.append(val)
    return list

list1 = extend(10)  # [10,]
list2 = extend(123,[])  # [123,]
list3 = extend('a')  # [10,'a',]
print(list1,list2,list3)   # [10,'a']  [123,]  [10,'a',]

# 11.面试题
#11.1  ABC  不变类型可以做键
#11.2
res = {}
A = ('a','b','c','d','e')
B = (1,2,3,4,5)
for i in range(len(A)):
    res.update({A[i]:B[i]})
print(res)

# 11.3
import sys
sys.argv()

# 11.4

ip = '192.168.0.100'
list = ip.split('.')

#11. 5
Alist = ['a','b','c']
print(','.join(Alist))

# 11. 6
StrA = '1234A6'
print(StrA[-2:])
print(StrA[2])
print(StrA[3])

# 11.7
Alist = [1,2,3,1,3,1,2,1,3]
print(Alist[:3])

# 11.编程题 5
### 11.编程题 55.1
"""
import os
def func(path):
    result = os.walk(path)
    for a,b,c in result:
        for item in c:
            print(os.path.join(a,item))

func('E:\Python学习')
"""

###11.编程题.55.2
"""
for a in range(1,1000):
    c = 0
    for b in range(1,a-1):
        if a % b == 0:
            c += b
    if c == a:
        print('找到了完美数%d' %c)
#  6   28   496
"""

###11.编程题.55.3  超纲,暂时放弃

###11.编程题.55.4
"""
def compare_list(a1,a2):
    for i in range(len(a1)):
        if a1[i] > a2[i]:
            return a1
        elif a1[i] < a2[i]:
            return a2
        elif a1[i] == a2[i]:
            return a1
        else:
            return a2
        
compare_list(list1,list2)

"""

###11.编程题.55.5
"""

with open('etl_log.txt','rb') as file:
    for line in file:
        print(line)
        
"""