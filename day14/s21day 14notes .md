# Python全栈day 14

## 一. 昨日补充

- **可变类型做默认参数的坑**

  ```python
  # 如果要想给value设置默认是空列表
  
  # 不推荐(坑)
  def func(data,value=[]): 
      pass 
  
  # 推荐
  def func(data,value=None):
      if not value:
          value = []
  
  ```

  - 原因

  ```python
  def func(data,value=[]): 
      value.append(data)
      return value 
  
  v1 = func(1) # [1,]     # 调用函数,作用域得到value=[],添加value=[1,]
  v2 = func(2)  # [1,2,]   #调用函数,value=[1,]再次添加元素2,
  
  v2 = func(1,[11,22,33]) # [11,22,33,1]
  
  ```

  - 面试题

  ```python
  def func(a,b=[]):
      b.append(a)
      return b
  
  l1 = func(1)
  l2 = func(2,[11,22])
  l3 = func(3)
  
  # [1,3]   [11,22,2]   [1,3]
  print(l1,l2,l3)
  
  ##############    def func(a,b=[]) 有什么陷阱？   #######
  答: Python函数在定义的时候。默认参数b的值就被计算出来了，即[],因为默认参数b也是一个变量，它指向对象即[],每次调用这个函数，如果改变b的内容每次调用时候默认参数也就改变了，不在是定义时候的[]了.应该换成None  .
  ```

- 闭包解析

  ```python
  # 不是闭包
  def func1(name):
      def inner():
          return 123
      return inner 
  
  # 是闭包：封装值 + 内层函数需要使用。
  def func2(name):
      def inner():
          print(name)
          return 123
      return inner 
  
  # 注意 :只有作用域里封装值   +   内层函数需要调用  ---> 闭包
  ```

- 递归函数

  ```python
  ##函数在内部调用自己--->  递归函数
  # 效率很低,python对栈,即函数递归的次数有限制-->1000次
  # 尽量不要使用 递归函数
  # 递归的返回值
  def func(a):
      if a == 5:
          return 100000
      result = func(a+1) + 10
  
  v = func(1)
  name = 'alex'
  def func():
      def inner():
          print(name)
       return inner
  v =func()
  ```

  

## 二. 装饰器带参数

### 1. 装饰器

1. 基本格式

   ```python
   def wrapper(func):
       def inner(*args,**kwargs):
           
           v = func()
           
           return v
       return i
   ```

2. 装饰器加上参数(外层函数必须以函数名作为参数,要给装饰器加上参数,在外层再套一层函数)

   ```python
   def super_wrapper(counter):  # counter 装饰器参数
       print(123)
       def wrapper(func):
           print(456)
           def inner(*args, **kwargs):  # 参数统一的目的是为了给原来的index函数传参
               data = []
               for i in range(counter):
                   v = func(*args, **kwargs)  # # 参数统一的目的是为了给原来的index函数传参
                   data.append(v)
               return data
           return inner
       return wrapper
   
   @super_wrapper(9):    # 这时已经打印 123   456   # # func = 原来的index函数u
   											# index = inner
                                     # 已经运行super_wrapper函数和wrapper函数
           
   def index(i):
       print(i)
       return i+1000
   
   print(index(456))          # 调用index函数(带装饰器)
   ```

   ```python
   # ################## 普通装饰器 #####################
   def wrapper(func):
       def inner(*args,**kwargs):
           print('调用原函数之前')
           data = func(*args,**kwargs) # 执行原函数并获取返回值
           print('调用员函数之后')
           return data
       return inner 
   
   @wrapper
   def index():
       pass
   
   # ################## 带参数装饰器 #####################
   def x(counter):
       def wrapper(func):
           def inner(*args,**kwargs):
               data = func(*args,**kwargs) # 执行原函数并获取返回值
               return data
           return inner 
   	return wrapper 
   
   @x(9)
   def index():
       pass
   
   ```

   - 基本装饰器更重要8成
   - 带参数的装饰器2成
   - 装饰器本质上就是一个python函数,它可以让其他函数在不需要任何代码变动的前提下增加额外的功能 ，装饰器的返回值也是一个函数对象，她有很多的应用场景，比如：插入日志，事物处理，缓存，权限装饰器就是为已经存在的对象 添加额外功能 

## 三. 模块讲解

#### 3.1   sys  模块

```python
### py  解释器相关的数据
import sys
sys.getrefcount()   # 获取一个值的引用次数,用得少,不重要
a = [11,22,33]
b = a
print(sys.getrefcount(a))

###sys.getrecursionlimit()
print(sys.getrecursionlimit())  # 1000次,python默认支持的递归数量.实际使用用个人的电脑配置有关

###sys.stdout.write  --> print    (进度)
import time
for i in range(100):
    msg = '进度条跑到%s%%\r' %i    # \r 光标回到当前行的起始位置(不是覆盖.,是清空)
    print(msg , end = '')
    time.sleep(0.5)  # 暂停0.5s后再运行
    
### sys.agrv  #### 获取用户执行脚本时，传入的参数。得到的是一个列表,第一个元素是脚本文件.py
##  Sys.argv[ ]其实就是一个列表，里边的项为用户输入的参数，关键就是要明白这参数是从程序外部输入的，而非代码本身的什么地方，要想看到它的效果就应该将程序保存了，从外部来运行程序并给出参数。



```

```python
#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
让用户执行脚本传入要删除的文件路径，在内部帮助用将目录删除。
C:\Python36\python36.exe D:/code/s21day14/7.模块传参.py D:/test
C:\Python36\python36.exe D:/code/s21day14/7.模块传参.py

"""
import sys

# 获取用户执行脚本时，传入的参数。
# C:\Python36\python36.exe D:/code/s21day14/7.模块传参.py D:/test
# sys.argv = [D:/code/s21day14/7.模块传参.py, D:/test]
path = sys.argv[1]

# 删除目录
import shutil
shutil.rmtree(path)
```



#### 3.2  os  模块

```python
import os

os.path.exists(path)      #如果path存在，返回True；如果path不存在，返回False.文件路径

os.stat('20190409_192149.mp4').st_size   #  获取文件大小(字节大小)

os.path.abspath()   #  获取一个文件的绝对路径

os.path.dirname # 获取路径的上级目录(文件夹)
```

- **os.path.join ，路径的拼接** 

  ```python
  import os
  path = "D:\code\s21day14" # user/index/inx/fasd/
  v = 'n.txt'     
  
  result = os.path.join(path,v)
  print(result)        # # D:\code\s21day14\n.txt
  result = os.path.join(path,'n1','n2','n3')
  print(result)      # D:\code\s21day14\n1\n2\n3
  ```

- os.listdir ， 查看一个目录下所有的文件【第一层】

  ```python
  import os
  
  result = os.listdir(r'D:\code\s21day14')     # r,在字符创=串前面,转义符,把\n\t\r当做字符串.
  for path in result:
      print(path)
  ```

- **os.walk ， 查看一个目录下所有的文件【所有层】**

  ```python
  import os
  
  result = os.walk(r'D:\code\s21day14')
  for a,b,c in result:
      # a,正在查看的目录 b,此目录下的文件夹  c,此目录下的文件
      for item in c:
          path = os.path.join(a,item)
          print(path)   # 还有其他很多用途
  ```

#### 3.3 shutil 模块

```python
import shutil
shutil.rmtree(path)
```

#### 3.4总结至今的模块

- random
- hashlib
- getpass 
- time
- os
- sys
- shutil