#!/usr/bin/env python        	
# -*- coding:utf-8 -*-	 
# @Project  : s21day28
# @Author : Jack Deng
# @Time   :  2019-05-07 16:25
# @File   : tcp_client.py

import socket
import pickle
from day28homework.md5 import md5

sk = socket.socket()


def login():
    while True:
        print('请先登录')
        user = input('请输入学号(输入Q退出客户端)')
        if user.upper() == 'Q': return
        pwd = md5(input('请输入密码'))
        lst = pickle.dumps([user, pwd])
        sk.send(lst)
        ret = sk.recv(1024)
        if pickle.loads(ret):
            print(pickle.loads(ret)[0],'登录成功')
            return pickle.loads(ret)
        print('用户名或者密码错误,请重新登陆')


def run():

    sk.connect(('127.0.0.1', 8989))
    CURRENT_USER  = login()
    if not CURRENT_USER:
        exit(0)
    while True:
        inp = input('请输入消息')
        sk.send(inp.encode('utf-8'))
        if inp.upper() == 'Q': break
        ret = sk.recv(1024 * 1024)
        print(CURRENT_USER[1] %ret.decode('utf-8'))
        if ret.decode('utf-8').upper() == 'Q':
            break
    sk.close()

if __name__ == '__main__':
    run()