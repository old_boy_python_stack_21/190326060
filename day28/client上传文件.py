#!/usr/bin/env python        	
# -*- coding:utf-8 -*-	 
#@Project  : day28 
#@Author : Jack Deng       
#@Time   :  2019-05-07 11:51
#@File   : server_client.py

import  socket
sk = socket.socket()
sk.connect(('127.0.0.1',8780))
f = open('123456.mp4','rb')

while True:
    val = f.read(512)
    if len(val) ==0:
        sk.send(''.encode('utf-8'))
        break
    sk.send(val)
    sk.recv(1024)
sk.close()