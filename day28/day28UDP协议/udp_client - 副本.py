#!/usr/bin/env python        	
# -*- coding:utf-8 -*-	 
# @Project  : s21day28
# @Author : Jack Deng
# @Time   :  2019-05-07 16:25
# @File   : tcp_client.py

import socket
import pickle
from day28homework.md5 import md5

sk = socket.socket(type=socket.SOCK_DGRAM)
in_p =('127.0.0.1',8989)


def run():
    while True:
        inp = '代永进: '+input('请输入消息')
        sk.sendto(inp.encode('utf-8'),in_p)
        data , addr = sk.recvfrom(1024)
        print(data.decode('utf-8'))

if __name__ == '__main__':
    run()