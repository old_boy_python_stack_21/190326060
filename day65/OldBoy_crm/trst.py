#!/usr/bin/env python
# -*- coding:utf-8 -*-
import hashlib
def get_md5(pwd, salt):
    md5 = hashlib.md5(salt.encode("utf-8"))
    md5.update(pwd.encode('utf-8'))
    return md5.hexdigest()

print(get_md5('alex@qq.com',"123"))
print(get_md5('wusir@qq.com',"123"))