from django.shortcuts import render, redirect, HttpResponse, reverse
from crm import models
import hashlib
from crm.forms import RegForm
from . import models


def get_md5(pwd, salt):
    if not pwd or not salt:
        return None
    md5 = hashlib.md5(salt.encode('utf-8'))
    md5.update(pwd.encode('utf-8'))
    return md5.hexdigest()


def login(request):
    error = ""
    if request.method == "POST":
        username = request.POST.get('username')
        password = request.POST.get('password')

        obj = models.UserProfile.objects.filter(username=username, password=get_md5(password, username),
                                                is_active=True).first()
        if obj:
            return redirect("index")
        else:
            error = "用户名或密码错误"
    return render(request, 'login.html', {"error": error})


def index(request):
    return render(request, "index.html")


# def login(request):
#     if request.method == 'POST':
#         username = request.POST.get('username')
#         password = request.POST.get('password')
#         md5 = hashlib.md5()
#         md5.update(password.encode('utf-8'))
#
#         obj = models.UserProfile.objects.filter(username=username, password=md5.hexdigest(), is_active=True).first()
#         if obj:
#             return redirect(reverse('index'))
#             # return redirect('index')
#         else:
#             return render(request, 'login.html', {'error': '用户名或密码错误'})
#     return render(request, 'login.html')


def reg(request):
    form_obj = RegForm()
    if request.method == 'POST':
        print(request.POST)
        form_obj = RegForm(request.POST)
        # 对提交的数据进行校验
        if form_obj.is_valid():
            # 校验成功  把数据插入数据中
            # models.UserProfile.objects.create(**form_obj.cleaned_data)
            form_obj.save()
            return redirect('login')
        print(form_obj.errors)
    return render(request, 'reg.html', {'form_obj': form_obj})

# def index(request):
#     return render(request, 'index.html')
