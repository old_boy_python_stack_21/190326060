# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html

import json
import time
from day06work.utils.artical_process import arcica_topic,artical_keyword

class Day06WorkPipeline(object):
    def open_spider(self,spider):
        self.file = open('wangyi.json','w',encoding='utf-8')


    def process_item(self, item, spider):
        data = {
            'title':item['title'],
            'content':item['content'],
        }

        time.sleep(0.2) # 睡0.2秒,规避QPS限制
        item['topic'] = arcica_topic(data).get('item').get('lv1_tag_list')
        item['keyword'] = artical_keyword(data).get('items')

        line = json.dumps(dict(item),ensure_ascii=False) + "\n"
        self.file.write(line)
        return item

    def close_spider(self,spider):
        self.file.close()