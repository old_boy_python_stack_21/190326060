# -*- coding:utf-8 -*-
# __author__ = Deng Jack

# Automated Speech Recognition
import os

from aip import AipSpeech

APP_ID = '16980626'
API_KEY = 'AoqdnxMgh3t4rtDDgBMdZGIk'
SECRET_KEY = 'pPIZx6eeYx0OmwYn4Qks0dnnQ99aGmVj'


import urllib, sys
import ssl
import json
from urllib.request import Request
import requests

# client_id 为官网获取的AK， client_secret 为官网获取的SK
host = f'https://aip.baidubce.com/oauth/2.0/token?grant_type=client_credentials&client_id={API_KEY}&client_secret={SECRET_KEY}'

request = urllib.request.Request(host)
request.add_header('Content-Type', 'application/json; charset=UTF-8')
response = urllib.request.urlopen(request)
content = response.read()
if (content):
    content = json.loads(content)
    access_token = content.get('access_token')


def artical_keyword(data):
    url = "https://aip.baidubce.com/rpc/2.0/nlp/v1/keyword"
    params = {
        "charset": 'UTF-8',
        "access_token":access_token,
    }
    headers = {
        "Content-Type":'application/json'
    }
    response = requests.post(url,json=data,headers=headers,params=params)

    print(response.json())
    return response.json()


def arcica_topic(data):
    url = "https://aip.baidubce.com/rpc/2.0/nlp/v1/topic"
    params = {
        "charset": 'UTF-8',
        "access_token":access_token,
    }
    headers = {
        "Content-Type":'application/json'
    }
    response = requests.post(url,json=data,headers=headers,params=params)
    return response.json()


data = {
    'title':'经贸摩擦难挡美企青睐中国市场',
    'content':"""美方不断升级对华经贸摩擦，甚至发出鼓动美国企业撤出中国的“杂音”。而事实上，很多美国企业正寻求与中国建立更为密切的经贸联系。



美国消费者新闻与商业频道9月1号报道称，没有多少美国公司会计划完全撤出中国，因为这么做会明显损害它们的竞争优势。中国是这些企业供应链条上的关键部分。



美国荣鼎咨询公司日前公布的数据也显示，中国正在扩张的消费市场对美国企业很有吸引力。尽管贸易紧张局势加剧，美国公司在中国的投资仍在增长。2019年上半年，美国企业在华投资68亿美元，比过去两年同期均值高出1.5%。



总部位于美国亚特兰大的全球铝材生产巨头诺贝丽斯公司近日就宣布了在华拓展计划。该公司表示看好中国作为全球最大汽车产销市场的发展前景，以及中国汽车产业向新能源汽车转型升级的机遇。



美中贸易全国委员会会长克雷格·艾伦表示，在华经营的大多数美国企业都明白，在可预见的未来，中国仍将是全球增长的主要引擎之一。""",
}


artical_keyword(data)