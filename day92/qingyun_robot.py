# -*- coding:utf-8 -*-
# __author__ = Deng Jack

import requests

# 青云客智能机器人
qyk_url = 'http://api.qingyunke.com/api.php'

"""http://api.qingyunke.com/api.php?key=free&appid=0&msg=关键词"""

def rebot(msg):
    params = {
        'key':'free',
        'appid':'0',
        'msg':'来个笑话'
    }
    params['msg']=msg
    res = requests.get(url=qyk_url,params=params)
    ret = res.json()
    return ret

print('开始聊天')
while 1:
    msg = input('我: ')
    result = rebot(msg)
    try:
        res = result.get('content').format(br='\n').replace('★ ','')
    except Exception as e:
        print(result,'>>>',e)
        res = '我不知道你在说什么?'
    print('机器人:'+ res)