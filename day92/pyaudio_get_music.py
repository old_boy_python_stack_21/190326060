# -*- coding:utf-8 -*-
# __author__ = Deng Jack

import pyaudio
import wave

CHUNK = 1024  # 1024字节,一次写入的字节大小
FORMAT = pyaudio.paInt16   # 16 bit int,位深
CHANNELS = 2  # 双声道?
RATE = 16*1000   # 采样率
RECORD_SECONDS = 4  # 录音时间


def rec(file_name,record_seconds=RECORD_SECONDS):
    """
    :param file_name: 录音的文件名
    :param record_seconds: 录音的时间
    :return:
    """
    p = pyaudio.PyAudio()  # 实例化类,创建音乐类

    stream = p.open(format=FORMAT,
                    channels=CHANNELS,
                    rate=RATE,
                    input=True,
                    frames_per_buffer=CHUNK)

    print("开始录音,请说话......")

    frames = []

    for i in range(0, int(RATE / CHUNK * record_seconds)):
        data = stream.read(CHUNK)
        frames.append(data)

    print("录音结束,请闭嘴!")

    stream.stop_stream()
    stream.close()
    p.terminate()

    wf = wave.open(file_name, 'wb')
    wf.setnchannels(CHANNELS)
    wf.setsampwidth(p.get_sample_size(FORMAT))
    wf.setframerate(RATE)
    wf.writeframes(b''.join(frames))
    wf.close()