# -*- coding:utf-8 -*-
# __author__ = Deng Jack
import json
import subprocess

import pyaudio
import wave
import os

# 百度 api 语音 接口
import requests
from aip import AipSpeech

APP_ID = '16980626'
API_KEY = 'AoqdnxMgh3t4rtDDgBMdZGIk'
SECRET_KEY = 'pPIZx6eeYx0OmwYn4Qks0dnnQ99aGmVj'

client = AipSpeech(APP_ID, API_KEY, SECRET_KEY)

# 机器人的接口
qyk_url = 'http://api.qingyunke.com/api.php'


class Talk:
    CHUNK = 1024  # 1024字节,一次写入的字节大小
    FORMAT = pyaudio.paInt16  # 16 bit int,位深
    CHANNELS = 2  # 双声道?
    RATE = 16 * 1000  # 采样率
    RECORD_SECONDS = 4  # 录音时间

    # 开始录音
    def get_audio(self, file_name, record_seconds=RECORD_SECONDS):
        """
        录音函数,录制用户从设备的声音
        :param file_name: 录音的文件名
        :param record_seconds: 录音的时间
        :return:
        """
        p = pyaudio.PyAudio()  # 实例化类,创建音乐类

        stream = p.open(format=self.FORMAT,
                        channels=self.CHANNELS,
                        rate=self.RATE,
                        input=True,
                        frames_per_buffer=self.CHUNK)

        print("开始录音,Start")

        frames = []

        for i in range(0, int(self.RATE / self.CHUNK * record_seconds)):
            data = stream.read(self.CHUNK)
            frames.append(data)

        print("录音结束,End")

        stream.stop_stream()
        stream.close()
        p.terminate()

        wf = wave.open(file_name, 'wb')
        wf.setnchannels(self.CHANNELS)
        wf.setsampwidth(p.get_sample_size(self.FORMAT))
        wf.setframerate(self.RATE)
        wf.writeframes(b''.join(frames))
        wf.close()

    # 语音转为文字
    def ASP_process(self):
        result = client.asr(self.get_file_content('template.pcm'), 'pcm', 16000, {
            'dev_pid': 1536,
        })
        """
        {
        	'corpus_no': '6723149099852859868',
        	'err_msg': 'success.',
        	'err_no': 0,
        	'result': ['今晚去玩英雄联盟吧'],
        	'sn': '814759077551565355131'
        }
        """
        return result  # type:dict

    # 读取文件
    def get_file_content(self, filePath):
        with open(filePath, 'rb') as fp:
            return fp.read()

    # 将录音文件template.wav转换为pcm格式
    def translate_pcm(self):
        """
        将录制的音频文件转换为pcm格式,供语音识别使用
        :return:
        """
        res = os.system('ffmpeg -y  -i template.wav  -acodec pcm_s16le -f s16le -ac 1 -ar 16000 template.pcm')
        if res == 0:
            return True

    def TTS_process(self, audio):
        """
        将文本转换为 语音mp3
        :params:audio : 要装换的话语
        :return:
        """
        result = client.synthesis(audio, 'zh', 1,
                                  {
                                      'vol': 5,
                                  })

        # 识别正确返回语音二进制 错误则返回dict 参照下面错误码
        if not isinstance(result, dict):
            with open('robot_speak.mp3', 'wb') as f:
                f.write(result)

    def Robot_talk(self, msg):
        params = {
            'key': 'free',
            'appid': '0',
            'msg': '来个笑话'
        }
        params['msg'] = msg
        res = requests.get(url=qyk_url, params=params)
        ret = res.json()
        return ret

    def run(self):

        self.get_audio('template.wav')
        # 在当前目录,录音文件为  template.wav
        if not self.translate_pcm():
            print('转换文件出错,清检查后再试')
            return
        # 目录已经存在  template.pcm 开始语音识别为文字
        ret = self.ASP_process()
        msg = ret.get('result')[0]
        print('我 : ' + msg)

        # 传输给机器人
        result = self.Robot_talk(msg)

        try:
            res = result.get('content').format(br='\n').replace('★ ', '')
        except Exception as e:
            print(result, '>>>', e)
            res = '我不知道你在说什么?'

        print(res)
        if res=='我不知道你在说什么?':
            os.system('start.bat not_know.wma')
        else:
            self.TTS_process(res)
            os.system('start.bat robot_speak.mp3')



obj = Talk()
while 1:
    obj.run()
def get_time():
    command = "ffprobe -loglevel quiet -print_format json -show_format -show_streams -i %s" % (
        'robot_speak.mp3')
    result = subprocess.Popen(command, shell=True,
                              stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    out = result.stdout.read()
    return out.decode('gbk')
    temp = str(out.decode('utf-8'))
    info = json.loads(temp)



