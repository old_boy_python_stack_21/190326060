# -*- coding:utf-8 -*-
# __author__ = Deng Jack

from flask import Blueprint

bp = Blueprint('app01',__name__)

@bp.route('/user')
def user():
    return 'user app01'
