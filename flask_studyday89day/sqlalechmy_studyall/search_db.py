# -*- coding:utf-8 -*-
# __author__ = Deng Jack

from sqlalchemy.orm import sessionmaker
from sqlalechmy_studyall.sqlalechmy_study import engine ,User

Session = sessionmaker(engine)

db_session = Session()

# 1 取出所有,select * from user 查询user表中的所有数据
"""
all_user = db_session.query(User).all()  # -->list

for i in all_user:
    print(i,type(i),i.id,i.name)

"""

# 2. select * from user where id >= 20
all_user = db_session.query(User).filter(User.id >=2)

# 原生sql语句
"""
SELECT user.id AS user_id, user.name AS user_name 
FROM user 
WHERE user.id >= %(id_1)s
"""


all_user = db_session.query(User).filter(User.id >= 3).all()

#　[3+带泳, 4+带镜, 5+带泳镜3]


# 3 .first()， 除了取出全部还可以只取出一条

all_user = db_session.query(User).filter(User.id >= 3).first()


