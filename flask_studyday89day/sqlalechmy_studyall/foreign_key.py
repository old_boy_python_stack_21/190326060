# -*- coding:utf-8 -*-
# __author__ = Deng Jack

from sqlalchemy.ext.declarative import declarative_base

from sqlalchemy import Column, String, Integer, create_engine, ForeignKey
from sqlalchemy.orm import relationship

Base = declarative_base()

engine = create_engine('mysql+pymysql://root:123@localhost:3306/sql_study')


class ClassTable(Base):
    __tablename__ = "classtable"

    id = Column(Integer, primary_key=True)
    name = Column(String(32), index=True)


class Student(Base):
    __tablename__ = "student"

    id = Column(Integer, primary_key=True)
    name = Column(String(32), index=True)

    # 关联字段,让class_id 与 class 的 id 进行关联,主外键关系(这里的ForeignKey一定要是表名.id不是对象名)
    class_id = Column(Integer, ForeignKey("classtable.id"))

    # 将student 与 classtable 创建关系 这个不是字段,只是关系,backref是反向关联的关键字
    to_class = relationship("ClassTable", backref="stu2class")


# Base 自动检索所有继承Base的ORM 对象 并且创建所有的数据表
Base.metadata.create_all(engine)
