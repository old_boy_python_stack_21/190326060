# -*- coding:utf-8 -*-
# __author__ = Deng Jack


from sqlalechmy_studyall.sqlalechmy_study import User ,engine


user1 = User(name='带泳镜')
user2 = User(name='带泳')
user3 = User(name='带镜')
user4 = User(name='带泳镜3')


# 写入数据库


# # 首先打开数据库会话 , 说白了就是创建了一个操纵数据库的窗口
# 导入 sqlalchemy.orm 中的 sessionmaker
from sqlalchemy.orm import sessionmaker

# 创建 sessionmaker 会话对象,将数据库引擎 engine 交给 sessionmaker
Session = sessionmaker(engine)

# 打开会话对象
db_session = Session()

# 在db_session会话中添加一条 UserORM模型创建的数据
db_session.add_all([user1,user2,user3,user4])

# 22 # 使用 db_session 会话提交 , 这里的提交是指将db_session中的所有指令一次性提交
db_session.commit()

db_session.close()
"""
或者使用
user_list = [user1,user2,user3]
db_session.add_all(user_list)
db_session.commit()
"""