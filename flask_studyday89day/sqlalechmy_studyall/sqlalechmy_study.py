# -*- coding:utf-8 -*-
# __author__ = Deng Jack

from sqlalchemy import Column, String, Integer, create_engine ,DATE ,Boolean
from sqlalchemy.orm import sessionmaker, relationship
from sqlalchemy.ext.declarative import declarative_base

# 初始化数据库连接:
engine = create_engine('mysql+pymysql://root:123@localhost:3306/sql_study')
"""
'数据库类型+数据库驱动名称://用户名:口令@机器地址:端口号/数据库名'
"""
# 创建对象的基类
Base = declarative_base()



# String Integer Text Boolean SmallInteger Datetime
nullable = True # 该列为空
index = True  # 在该列创建索引


class User(Base):
    # 表名
    __tablename__ = 'user'

    # 表的字段

    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(32),)

    def __repr__(self):
        return f"{self.id}+{self.name}"


# 创建DBSession类型:
# DBSession = sessionmaker(bind=engine)

# Base 自动检索所有继承Base的ORM 对象 并且创建所有的数据表
Base.metadata.create_all(engine)
