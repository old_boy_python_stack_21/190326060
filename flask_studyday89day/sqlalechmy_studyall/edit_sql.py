# -*- coding:utf-8 -*-
# __author__ = Deng Jack

from sqlalechmy_studyall.sqlalechmy_study import engine ,User

from sqlalchemy.orm import sessionmaker

Session = sessionmaker(engine)
db_session = Session()

#  UPDATE user SET name="NBDragon" WHERE id=20 更新一条数据
# 语法是这样的 :
# 使用 db_session 执行User表 query(User) 筛选 User.id = 20 的数据 filter(User.id == 20)
# 将name字段的值改为NBDragon update({"name":"NBDragon"})

res = db_session.query(User).filter(User.id > 2).update({"name":"NBDragon"})

print(res) # res就是我们当前这句更新语句所更新的行数

# 注意,这里一定要将 db_session的执行语句提交才行
db_session.commit()
db_session.close()

# 可以 一次更新多条数据