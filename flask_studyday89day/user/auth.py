# -*- coding:utf-8 -*-
# __author__ = Deng Jack

from flask import  Blueprint , request ,session ,redirect ,url_for , render_template
user = Blueprint('auth',__name__)

@user.route('/login/', methods=['GET', "POST"],endpoint='login')
def login():
    if request.method == 'POST':
        user = request.form.get('user')
        pwd = request.form.get('pwd')
        next = request.args.get('next')
        if user == 'admin' and pwd == '123':
            # 登陆成功,保存信息和登陆次数
            session['user'] = request.form.get('user')
            session[f'{user}_count'] = session.get(f'{user}_count', 0) + 1
            if next:
                return redirect(next)
            return redirect(url_for('stu_views.stu_pro'))
    return render_template('login.html')


@user.route('/logout/')
def logout():
    session.pop('user')
    return redirect('/login/')