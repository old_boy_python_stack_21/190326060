# -*- coding:utf-8 -*-
# __author__ = Deng Jack

from flask import Blueprint, render_template ,url_for

stu = Blueprint('stu_views',__name__)

STUDENT_DICT = {
    1: {'name': 'Old', 'age': 38, 'gender': '中'},
    2: {'name': 'Boy', 'age': 73, 'gender': '男'},
    3: {'name': 'EDU', 'age': 84, 'gender': '女'},
}

@stu.route('/stu_pro/', endpoint='stu_pro')
def stu_pro():
    return render_template('stu_pro.html', stu_p=STUDENT_DICT, title='学生概况')


@stu.route('/stu_info/<int:id>/', endpoint='stu_info')
def stu_info(id):
    msg = STUDENT_DICT.get(id, None)
    return render_template('stu_info.html', msg=msg, id=id, title='学生详情')

print()

