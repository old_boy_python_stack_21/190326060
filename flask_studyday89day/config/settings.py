# -*- coding:utf-8 -*-

class DebugSettings:
    DEBUG = True
    SESSION_COOKIE_NAME = 'I am debug session'


class TestingSetting:
    DEBUG = False
    SESSION_COOKIE_NAME = 'I am Not session'


# 白名单
WHITE_LIST = [
    '/login/',
    '/logout/',
]
