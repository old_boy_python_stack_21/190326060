# -*- coding:utf-8 -*-
# __author__ = Deng Jack
from re import match
from flask import Flask, session, request, redirect, url_for , render_template

from stu_list.stu_views import stu
from user.auth import user

app = Flask(__name__)

app.secret_key = '\xc3i\x80\x84\x0f\x0f~!Io\x01\xbaypC\x9e.R\xe6\xd0lF50\xd9\xde\x11\xc5\xbaA\xb6}'

app.config.from_object('config.settings.DebugSettings')

from werkzeug.utils import import_string

import functools


def auth(func):
    #  @functools.wraps(func)
    def inner(*args, **kwargs):
        if session.get('user'):
            # 是登陆用户
            return func(*args, **kwargs)
        return redirect(url_for('login'))

    return inner


app.register_blueprint(stu)
app.register_blueprint(user)

"""
中间件的方法
@app.before_request
app.before_request_funcs={None:[one]}
"""

# 直接永久重定向308到login
@app.route('/',redirect_to='/login/')

@app.before_request
def process_request():
    path = request.path

    # 白名单,
    white_list = import_string('config.settings.WHITE_LIST')
    for pattern in white_list:
        if match(rf'^{pattern}$', path):
            return

    # 是否登录用户
    if not session.get('user', None):
        # 如果不存在,则没有登陆,转去登陆
        return redirect(url_for('auth.login',next=path))

    # 如果存在user,是登陆用户,ok,return None ,正常流程



@app.errorhandler(404)
def not_found404(error):
    return render_template('404not_found.html',error=error)


if __name__ == '__main__':
    app.run()
