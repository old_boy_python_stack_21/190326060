## 智能玩具 30分

### 1..简述智能玩具项目背景 2

```html
答:	你好,这个项目起源于我去年的一个经历,去年春节去我姐家串门,发现我4岁的外甥在玩一款智能玩具,只要连接上wifi,就可以通过配套的app传输幼儿早教内容到玩具并播放出来.外甥玩得很开心,玩具爱不释手.
	我在陪小外甥玩耍的时候他也没有放开那个玩具,通过对这个玩具的了解,我也发现了一些不足之处,玩具只能接受app传输过来的音乐.功能太过单调.于是就萌生了自己设计一款智能玩具,在听幼儿早教内容的基础上,实现玩具和app之间的通讯,玩具和玩具之间的通讯.
	后来回到北京,我把想法和我的团队谈了一下,他们对这个也很感兴趣.我们又找了美工设计师,由他负责设计玩具外形,我们编写代码,处理玩具的逻辑 和 页面的展示.
	于是我们就开始正式开发该项目了
```

### 2.项目应用技术以及技术实现功能 5

```html
答 : 我们使用了python语言 构建服务器后端和 mui框架 编写书籍 app.
1.服务器应用了最新的语音合成 和 语音识别技术,通过和百度公司的洽谈,调用百度的API 接口实现:
	玩具语音点播歌曲
	玩具语音选择和谁发语音消息  (和app(爸爸/妈妈..)或者toy(绑定的好友的玩具))
2.通过和图灵机器人的API接口,实现:
	玩具和机器人之间进行语音对话.
3.通过websocket协议,创建websocket连接,实现:
	玩具和app,玩具和玩具之间的通讯.
4.通过Python的gensim模块,调用jieba分词模块,和gensim中的稀疏矩阵相似度,来实现一个简单的自然语言处理:
	使得玩具语音点播歌曲变得更加智能,提高用户体验.
5. 通过python的pypinyin模块,将语音合成的汉字转为拼音,从而规避同音字的问题.
6.使用mongodb数据库存储聊天数据,便于后期处理数据,在数据层面上做出人物画像.
	同时查询数据方便.

手机app方面
1.使用了mui框架来制作手机app,并且通过 html5+ 规范来调用手机系统内的硬件api,实现了播放语音,录制音频,拍照,扫描	二维码,推送音乐到玩具等功能
```



### 3.产品功能简述 5

app:

```python
1.可以播放音频,幼儿早教内容,选择音乐.
2.绑定自己的玩具
3.添加玩具好友
4.给玩具发送语音消息
5.向玩具推送音频,幼儿早教内容,玩具会自动播放.
```

toy:

```python
1.自动播放app推送过来的音乐
2.通过向ai发送音频,ai响应后,自动播放对应的音乐,或者和ai进行对话.
3.通过向ai发送音频,ai响应后,自动联通app或者玩具,就可以2者之间进行通话了.
4.实现未读语音消息的收取和播放.
```

后台:

```python
提供所有的API接口.返回相应的json格式的响应和文件的响应
1.用户相关接口
	提供用户注册  登陆  以及打开app时的自动登陆接口
2.内容相关接口
	返回给app 响应的音乐内容资源供app选择
  	返回给app 对应的音乐资源供app播放音频
    返回给app 图片资源
    返回给app 录制的语音消息
    返回给app 玩具的二维码图片
3.音频上传相关接口
	app录制音频上传
	toy录制音频上传
4.获取语音消息相关接口
	app获取历史消息
    toy获取未读消息
5.硬件设备扫二维码接口
	app扫描绑定玩具的接口
    app获取它所有绑定的玩具接口
    设备开机时启动登陆的接口
6.通讯录相关接口
	app扫描添加玩具好友
    app 获取app 和 该app绑定的玩具的所有好友
    app 获取好友请求的接口
    app 同意好友 以及 拒绝好友的接口
```

### 4.Flask 中的 Response 及作用 5

​	基本三剑客  :  

1. 三剑客

   1. 返回一个字符串给浏览器渲染,Httpresponse("xxx")
   2. 返回一个html页面给浏览器渲染,render响应模板
   3. 返回一个重定向的请求给浏览器,浏览器根据重定向的内容发出请求.redirect,重定向

2. json格式的响应.返回一个json格式的响应内容.是一个标准格式的JSON字符串

3. 返回具体的文件,返回二进制流的文件内容.供浏览器使用

   



### 5.回答以下属性的作用 5

request.args --?  # 获取URL中的数据 to_dict().  

request.form 	 # formdata中的数据 # 

request.files  # 获取Form中的文件,返回 FileStorage ,存在 save()方法, 保存文件 filename 原始文件名

request.data  , # 请求中 Content-Type 中不包含 Form 或 FormData 保留请求体中的原始数据 ,是b" 字节类型"

request.json  #  请求中 Content-Type：application/json 请求体中的数据 被序列化到 request.json 中 以字典的形式存放 json



### 6.before_request 和 after_request 的正常和异常执行顺序 3

```python
@before_request 装饰器,在请求到视图函数之前执行,按照添加@before_request 的顺序按顺序执行,
	正常情况是返回 None 类型 ,会经由视图函数 和 @after_request函数,倒序执行,返回给浏览器
    异常时 ,返回的是一个响应类型 ,会倒序执行所有的@after_request函数,,返回给浏览器
@after_request 装饰器,在请求返回给浏览器之前执行,按照添加@after_request 的顺序按倒序执行, 
	# 注意:**必须接受一个参数 response  .是返回给浏览器的响应对象**
    正常情况,必须返回一个响应对象,倒序执行返回给浏览器.
    异常时 ,返回None ,flask会报错,错误码500,服务器错误,不应返回None.
    
```



### 7.简述 Flask 路由中 endpoint 和 methods 的作用 2

```python
endpoint  给视图函数起别名,用于url_for 反向解析,使用.未给视图函数设置endpoint时,默认以函数名作为该视图函数的	别名.在模板文件中使用url_for,更加方便.

methods ,为视图函数设置允许接受请求的请求方式,未给视图函数设置methods时,默认只允许GET请求.
	请求是html协议规定的八种请求之一.
```



### 8.MongoDB中的增删改查方法 3

```python
# 假设MongoDB存在users的collection表名
命令行操作
1.增加一条或者多条数据
	db.users.insert_one({name:"alexander.dsb.li"})
	db.users.insert_many([{name:"alexander.dsb.li"},
                         {name:"alexander.dsb.li"},
                          {name:"alexander.dsb.li"}
                         ])

2.删除
	db.tablename.deleteOne({查询条件})	# 删除符合条件的第一条数据
	db.tablename.deleteMany({查询条件}) # 删除所有符合条件的数据
    
    db.tablename.remove({})  # 删除当前tablename表内的所有数据
    
3. 更新/修改
	# 格式 : db.tablename.update({查询符合条件的数据},{$修改器:{修改的内容}})  批量修改
	db.users.update({name:"DSB"},{"$set":{age:73,gender:1}})
    
    1.$set  强制将某值修改覆盖
    2.$unset  强制删除一个字段
    3.$inc  引用增加,在原来的基础上自增,可为-1实现自减效果
    4.针对于array的修改器
    	# 格式: db.users.update({name:"MJJ"},{"$push":{"hobby":"抽烟"}})
    	1.$push 在array中追加数据 
        2.$pushAll 在Array中批量增加数据 [] == list.extends
        3.$pull 删除符合条件的所有元素
        4.$pop 删除array中第一条或最后一条数据
        	db.users.update({name:"MJJ"},{"$pop":{"hobby":1}}) 为正整数, 删除最后一条数据,>=0都可以,
            									注意python的区分.
            db.users.update({name:"MJJ"},{"$pop":{"hobby":-1}}) 为负整数, 删除第一条数据
        5.$pullAll 批量删除符合条件的元素 []
4.查询
	1.find()  ,返回该当前数据表中的所有数据
    2.findOne() ,查询符合条件的第一条数据
    3.db.tablename.find({符合条件}) 查询符合条件的所有数据
    
    # 特殊符号的用法
    `$and` 并列查询 db.users.find({"$and":[{name:"wpq"},{age:1000}]}) ,2者都满足才查询得到
    `$or` 或者 	db.users.find({"$or":[{age:1000},{name:"alex"}]})
    `$in` 包含或者 同一字段 或者条件 db.users.find({"age":{"$in":[999,1000]}})  ,只要在列表内就OK
    `$all` 子集或者 必须是子集 绝对不可以超集 交集是不可查询的,子集查询
    	db.users.find({"hobby":{"$all":[5,1,3]}})  #查询hobby必须包含135的数据
    `$ne` exclue 修改器,排除该项的其他所有项,排除查询,不等于
     $regex  find({ "name": { "$regex": "^R" } })  # 以R开头,正则查询符合条件的数据
        
    # 数学比较符号的使用
     $gt		大于
     $lt		小于
     $gte	大于等于
     $lte	小于等于
     $eq		等于 : 是用在 "并列" "或者" 等条件中
     :        等于

     $ne		不等于 不存在和不等于 都是 不等于
     eg: db.tablename.find({'score':{$gte:80}})  # 查询大于等于80的所有数据
        
        
5.skip 和 limit 和 sort 用法
	1.limit 限制选取
		db.test.find().limit(2)  # 从头取2条
    2.skip 跳过跳过多少个Document
		db.test.find().skip(2)  # 跳过2后取所有
    3.sort 排序
        db.users.find({}).sort({age:-1}) 依照age字段进行倒序
        db.users.find({}).sort({age:1}) 依照age字段进行正序
	4.联合使用# 要注意他们的优先级
    无所谓顺序,优先级为
    sort  >  skip  > limit,
    # limit 永远最后执行,出现sort最先执行.
    eg:
        db.users.find().skip(5).limit(10)  <=> db.users.find().limit(10).skip(5) # 是等价的,先跳过5条,获取10条
    # 但是若出现了sort ,则一定sort先执行后.即先排序 , 跳过 ,最后选取limit
    
6.pymongodb模块的使用 和 mongodb命令行操作基本相似.
	由于python中一般不采用驼峰式的命名,他是通过下划线连接的方式命名的
    1.find()  find_one() 查询
    2.update_one()  update_many() 更新
    3.insert_one()  insert_many()  新增数据
    4.delete_one({})  delete_many({})  删除来使用
		
 	

```

