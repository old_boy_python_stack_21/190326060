from bson import ObjectId

from Config import CHAT_ROOM
from utils.Baidu_Api import text_to_speech


def process_data(item):
    item = CHAT_ROOM.Toys.find_one(item)
    item['_id'] = str(item.get('_id'))
    return item

def content_process_data(item):
    item['_id'] = str(item.get('_id'))
    return item

from datetime import datetime

def get_audio_message(sender,receiver,filed ="friend_msg_reminder"):
    audio_message_dic = {"friend_msg_reminder": "no_know_msg_reminder.mp3",  # 你有来自小伙伴的新消息(不知道的消息提醒)
                         "friend_recvmsg_reminder": "no_know_recvmsg_reminder.mp3",  # 以下来自小伙伴的新消息(不知道的消息提醒)
                         }
    try:
        toy_info = CHAT_ROOM.Toys.find_one({"_id": ObjectId(receiver), "friend_list.friend_id": sender},
                                        {"friend_list.$": 1})
        # 若获取到toy_info,则取值 "friend_msg_reminder"对应的值,存在就直接返回
        audio_message = toy_info.get("friend_list")[0].get(filed)
        # 则取值 "friend_msg_reminder"对应的值,不存在就调用百度API接口,通过friend_remark得到文件流audio_message
        # 并将文件名更新到数据库中后,返回该文件名
        if not audio_message:
            remark = toy_info.get("friend_list")[0].get("friend_remark")
            if filed == "friend_msg_reminder":
                audio_message = text_to_speech(f"你有来自{remark}的新消息")
            else:
                audio_message = text_to_speech(f"以下来自{remark}的新消息")

            # 将其存入数据库中
            CHAT_ROOM.Toys.update_one({"_id": ObjectId(receiver), "friend_list.friend_id": sender},
                                   {"$set": {f"friend_list.$.{filed}": audio_message}})
    except AttributeError:
        remark = "小伙伴"
        audio_message = audio_message_dic.get("filed")

    return audio_message


def get_now():
    now_time = datetime.now()
    time_str = now_time.strftime("%Y%m%d%H%M%S")
    return time_str