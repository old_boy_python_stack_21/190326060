import os
from uuid import uuid4
from bson import ObjectId
from flask import jsonify
import time
import requests
from Config import BAIDUAPICLIENT, CHAT_PATH, BAIDU_NLP_APICLENT, CHAT_ROOM, QYK_ROBOTRUL
from Config import tl_data, tl_url

import pypinyin


# import jieba
# import gensim  # 机器学习综合库 - NLP 语言模型 相似度算法
# from gensim import corpora
# from gensim import models
# from gensim import similarities
#
#
# l1 = list(CHAT_ROOM.Content.find({}))
# all_doc_list = []  # 问题库的分词列表
# for doc in l1:
#     doc_list = list(jieba.cut_for_search(doc.get("title")))  # ["你","的","名字","是","什么","是什么"]
#     all_doc_list.append(doc_list)
# dictionary = corpora.Dictionary(all_doc_list)  # 制作词袋
# corpus = [dictionary.doc2bow(doc) for doc in all_doc_list]
# lsi = models.LsiModel(corpus)  # 数据量小时相对精确 大了就非常的不精确 500万内
# index = similarities.SparseMatrixSimilarity(lsi[corpus], num_features=len(dictionary.keys()))


# index.save("CHAT_ROOM_Content.index")
# index = similarities.SparseMatrixSimilarity.load("CHAT_ROOM_Content.index")


# def my_gensim_nlp(a):
#     doc_test_list = list(jieba.cut_for_search(a))  # ["今年","多","大","了"]
#     doc_test_vec = dictionary.doc2bow(doc_test_list)  # 用户输入转换为 corpus 通过 dictionary 转换
#     sim = index[lsi[doc_test_vec]]
#     cc = sorted(enumerate(sim), key=lambda item: -item[1])
#     if cc[0][0] >= 0.55 :
#         text = l1[cc[0][0]]
#         return text


def text_to_speech(text, type="toy"):
    # 如果是机器人和娃娃互动的语音可以不必保留,添加额外参数type,在服务器
    # 起一个守护进程 or 线程 ,每天0 点删除 以 ai开头的文件(互动语音消息)
    # 其他上传的 以创建时间为准,保留7天,也是0点扫描一次,创建时间在7天外的删除
    # 注意 : 一些特殊的mp3 要予以保留.
    result = BAIDUAPICLIENT.synthesis(text, 'zh', 1, {'vol': 5, })
    filename = f"{uuid4()}.mp3" if type == 'toy' else f"ai{uuid4()}.mp3"

    filepath = os.path.join(CHAT_PATH, filename)
    if isinstance(result, dict):
        return "I_not_know_speak.mp3"

    # 识别正确返回语音二进制 错误则返回dict 参照下面错误码
    with open(filepath, 'wb') as f:
        f.write(result)
    return filename


def speech_to_text(filePath):
    os.system(f"ffmpeg -y  -i {filePath}  -acodec pcm_s16le -f s16le -ac 1 -ar 16000 {filePath}.pcm")
    # os.remove(filePath)  # 是否需要删除,
    result = BAIDUAPICLIENT.asr(open(f"{filePath}.pcm", "rb").read(), 'pcm', 16000, {
        'dev_pid': 1536,
    })
    """
    {
    	'corpus_no': '6723149099852859868',
    	'err_msg': 'success.',
    	'err_no': 0,
    	'result': ['今晚去玩英雄联盟吧'],
    	'sn': '814759077551565355131'
    }
    """
    return result["result"][0]


def BAIDU_NLP(text1, text2):
    result = BAIDU_NLP_APICLENT.simnet(text1, text2)
    print(result)
    return result.get("score")


# 短文本相似度匹配获取响应
def My_NLP(Q, toy_id):
    # 1.点播歌曲
    # Q = 我想听xxxxx ,文本相似度实现
    if any(["我想听" in Q, "我要听" in Q, "请播放" in Q]):
        q = Q
        q = q.replace("我想听", "")
        q = q.replace("我要听", "")
        q = q.replace("请播放", "")
        # print(q)
        # content_dict = my_gensim_nlp(q) # 自己的库进行
        # print(Q)
        # if content_dict:
        #     return {"from_user": "ai", "music": content_dict.get("music")}

        content_list = list(CHAT_ROOM.Content.find())
        for content in content_list:
            time.sleep(0.3)
            if BAIDU_NLP(f"我要听{content.get('title')}", Q) >= 0.6:
                return {"from_user": "ai",
                        "music": content.get("music")}

    # 2.主动发起聊天
    # Q = 我要给爸爸发消息 ,聊天 ...

    if any(["发消息" in Q, "聊天" in Q]):
        Q_py = "".join(pypinyin.lazy_pinyin(Q, pypinyin.TONE3))
        toy = CHAT_ROOM.Toys.find_one({"_id": ObjectId(toy_id)})
        for friend in toy.get("friend_list"):
            print(friend)
            nick_py = "".join(pypinyin.lazy_pinyin(friend.get("friend_nick"), pypinyin.TONE3))
            remark_py = "".join(pypinyin.lazy_pinyin(friend.get("friend_remark"), pypinyin.TONE3))
            if nick_py in Q_py or remark_py in Q_py:
                filename = text_to_speech(f"现在可以给{friend.get('friend_remark')}发消息了")
                return jsonify({
                    "from_user": friend.get("friend_id"),
                    "chat": filename,
                    "friend_type": friend.get("friend_type")
                })

    # 3 先对接青云客机器人,图灵API账号暂未申请
    print("我:" + Q)
    return REBOT_TALK(Q)

    # 3 对接图灵机器人
    tl_data["perception"]["inputText"]["text"] = Q
    tl_data["userInfo"]["userId"] = toy_id
    res = requests.post(tl_url, json=tl_data)
    res_json = res.json()
    question = res_json.get("results")[0].get("values").get("text")
    filename = text_to_speech(question)
    return jsonify({
        "from_user": "ai",
        "chat": filename
    })


# 青云客机器人聊天
def _rebot(msg):
    params = {
        'key': 'free',
        'appid': '0',
        'msg': '来个笑话'
    }
    params['msg'] = msg
    res = requests.get(url=QYK_ROBOTRUL, params=params)
    ret = res.json()
    return ret


def REBOT_TALK(Q, sender_id):
    result = _rebot(Q)
    print("机器人的响应:", result)
    try:
        res = result.get('content').format(br='\n').replace('★ ', '')
        filename = text_to_speech(res, type="ai")
        # filename 会加上 ai 的前缀
    except Exception as e:
        res = '我不知道你在说什么?'
        filename = "I_not_know_speak.mp3"
    return {
        "code": 0,
        "msg": "上传成功",
        "AIRECV":
            {
                "message": filename,
                "chat_type": "audio",
            }
    }


def tuling(Q, sender_id):
    """
    # tl_data  老李的key
    # TL_DATA  老师的配置
    :param Q:
    :param sender_id: 原来的sender*5 作为id
    :return:
    """

    tl_data["perception"]["inputText"]["text"] = Q
    tl_data["userInfo"]["userId"] = sender_id * 5
    res = requests.post(tl_url, json=tl_data)
    res_json = res.json()
    question = res_json.get("results")[0].get("values").get("text")
    filename = text_to_speech(question)
    return {
        "code": 0,
        "msg": "上传成功",
        "DATA":
            {
                "filename": filename,
                "chat_type": "audio",
            }
    }


def tuling_text(Q, sender_id):
    """
    # tl_data  老李的key
    # TL_DATA  老师的配置
    :param Q:
    :param sender_id: 原来的sender*5 作为id
    :return:
    """

    tl_data["perception"]["inputText"]["text"] = Q
    tl_data["userInfo"]["userId"] = sender_id * 5
    res = requests.post(tl_url, json=tl_data)
    res_json = res.json()
    try:
        res = res_json.get("results")[0].get("values").get("text")
    except Exception as e:
        res = '我不知道你在说什么?'
    return {
        "code": 0,
        "msg": "上传成功",
        "AIRECV":
            {
                "message": res,
                "chat_type": "text",
            }
    }

