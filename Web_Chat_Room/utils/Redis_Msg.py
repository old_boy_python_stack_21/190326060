import json
import random

from Config import RDB


# 设置未读消息
# sender 向receiver发消息是,向receiver的数据中 + 1, 在插入数据库后设置,sender 只能 app  or toy 发送给 toy
def set_msg(sender, receiver):
    # 1当期有没有receiver的数据
    msg_count = RDB.get(receiver)
    if msg_count:
        # 2.当receiver收到sender的消息,对应当前sender + 1
        msg_count_dict = json.loads(msg_count)
        # msg_count_dict是否存在sender对应的消息数量
        msg_count_dict[sender] = msg_count_dict.get(sender, 0) + 1
        msg_count = json.dumps(msg_count_dict)
    else:
        # 不存在时,设置msg_count_dict = {sender:1}
        msg_count = json.dumps({sender: 1})

    RDB.set(receiver, msg_count)


# 获取未读消息的数量 ##如果使用 hash 存储会不会更好些
def get_msg_one(sender, receiver,only=False):
    """

    :param sender:
    :param receiver:
    :param only: 是否只获取当前sender 对应的未读消息总数
    :return:
    """
    msg_count = RDB.get(receiver)
    if msg_count:
        msg_count_dict = json.loads(msg_count) # type:dict
        count = msg_count_dict.get(sender, 0)
        if count == 0 and not only:
            for k,v in msg_count_dict.items():
                if v != 0:
                    sender = k
                    count = v
                    msg_count_dict[sender] = 0
                    break
        msg_count_dict[sender] = 0
    else:
        # 如果不存在
        msg_count_dict = {sender: 0}
        count = 0
    # print(msg_count_dict,count)
    RDB.set(receiver, json.dumps(msg_count_dict))
    return count,sender



def get_msg_counts(receiver):
    msg_count = RDB.get(receiver) # receiver:xxxxxx -->{sender1:1,sender1:1,}
    msg_count_dict = {"count":0}
    if msg_count:
        msg_count_dict = json.loads(msg_count)  # type:dict
        msg_count_dict["count"] = sum(msg_count_dict.values())
    return msg_count_dict




# 测试使用 hasn存储会不会更好些,不使用hasn存储
def set_msg2(sender, receiver):
    RDB.hincrby(receiver, sender)  # 自增,若不存在,设置起始值为1,存在着对应value + 1


def get_msg_one2(sender, receiver):
    count = RDB.hget(receiver, sender)  # type:bytes
    count = count if count else b"0" # 不存在改写为b"0"
    RDB.hset(receiver, sender,0) # 清空存储的值
    return int(count.decode("utf-8"))