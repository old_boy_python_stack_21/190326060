# -*- coding:utf-8 -*-
# __author__ = Deng Jack
import os

import requests
from flask import Blueprint, jsonify, send_file
from utils.process_data import content_process_data
from Config import MUSIC_PATH,AVATAR_PATH,COVER_PATH ,CHAT_PATH

contbp = Blueprint('content', __name__)


# App首页内容资源获取



# 用于App获取图片资源
@contbp.route('/get_cover/<filename>', methods=['get'])
def get_cover(filename):
    filepath = os.path.join(COVER_PATH, filename)
    return send_file(filepath)


# 用于App/Toy播放内容
@contbp.route('/get_music/<musicname>', methods=['get'])
def get_music(musicname):
    filepath = os.path.join(MUSIC_PATH, musicname)
    return send_file(filepath)


@contbp.route('/avatar/<avatarname>', methods=['get'])
def avatar(avatarname):
    filepath = os.path.join(AVATAR_PATH, avatarname)
    return send_file(filepath)


# 用于App/Toy播放语音消息
@contbp.route('/get_chat/<musicname>', methods=['get'])
def get_chat(musicname):
    filepath = os.path.join(CHAT_PATH, musicname)
    return send_file(filepath)
