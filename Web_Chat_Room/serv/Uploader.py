import os
import time
from uuid import uuid4
from bson import ObjectId

from flask import Blueprint, jsonify, request
from Config import CHAT_PATH, CHAT_ROOM, COVER_PATH
from utils.Baidu_Api import text_to_speech, speech_to_text, REBOT_TALK, tuling_text
from utils.Redis_Msg import set_msg
from utils.Baidu_Api import tuling, REBOT_TALK, _rebot
from utils.process_data import get_audio_message

upbp = Blueprint('uploader', __name__)


## 音频上传相关API

# 用于Toy录制语音消息上传
@upbp.route('/image_uploader', methods=["post"])
def image_uploader():
    get_data = request.form.to_dict()
    sender = get_data.get("sender")
    receiver = get_data.get("receiver")
    file = request.files.get("image_file")
    filename = f"{uuid4()}image{file.filename}"

    # 文件数据保存
    file.save(os.path.join(COVER_PATH, filename))

    # chat-list = [
    chat_info = {
        "sender": sender,  # 信息发送方ID
        "receiver": receiver,  # 信息接收方ID
        "chat_type": "image",  # 语音消息文件名
        "createTime": time.time(),  # 聊天创建时间
        "message": filename,
    }
    # 查询聊天窗口,子集查询
    user_list = [sender, receiver]
    print(user_list)
    # ret = CHAT_ROOM.Chats.find_one({"user_list": {"$all": user_list}})
    # 没有找到聊天窗口,先创建聊天窗口
    # print(ret)
    # if not ret:
    #     chat_window = CHAT_ROOM.Chats.insert_one({"user_list": user_list, "chat_list": []})
    # 此时一定存在了聊天窗口,开始更新聊天数据
    CHAT_ROOM.Chats.update_one({"user_list": {"$all": user_list}}, {"$push": {"chat_list": chat_info}})

    # set_msg(sender,receiver)
    # 返回值
    return jsonify({
        "code": 0,
        "msg": "上传成功",
        "DATA":
            {
                "filename": filename,
                "chat_type": "image",
            }
    })


@upbp.route('/audio_uploader', methods=["post"])
def audio_uploader():
    get_data = request.form.to_dict()
    sender = get_data.get("sender")
    receiver = get_data.get("receiver")
    print(get_data)
    file = request.files.get("audio_file")
    filename = f"{uuid4()}audio{file.filename}.wav"

    # 文件数据保存
    file.save(os.path.join(CHAT_PATH, filename))

    chat_info = {
        "sender": sender,  # 信息发送方ID
        "receiver": receiver,  # 信息接收方ID
        "chat_type": "audio",  # 语音类型
        "createTime": time.time(),  # 聊天创建时间
        "message": filename,
    }
    # 查询聊天窗口,子集查询

    # print(user_list)
    # ret = CHAT_ROOM.Chats.find_one({"user_list": {"$all": user_list}})
    # 没有找到聊天窗口,先创建聊天窗口
    # print(ret)
    # if not ret:
    #     chat_window = CHAT_ROOM.Chats.insert_one({"user_list": user_list, "chat_list": []})
    # 此时一定存在了聊天窗口,开始更新聊天数据
    if  receiver: # 是单聊的上传
        user_list = [sender, receiver]
        CHAT_ROOM.Chats.update_one({"user_list": {"$all": user_list}}, {"$push": {"chat_list": chat_info}})
    # set_msg(sender,receiver)
    # 返回值
    return jsonify({
        "code": 0,
        "msg": "上传成功",
        "DATA":
            {
                "filename": filename,
                "chat_type": "audio",
            }
    })


# 用于Toy录制语音消息上传至AI接口
@upbp.route('/aiaudio_uploader', methods=["post"])
def aiaudio_uploader():
    # 获取发送来参数
    get_data = request.form.to_dict()
    chat_type = get_data.get("chat_type")
    sender = get_data.get("sender")
    print(get_data, type(get_data))
    # 文本消息
    if chat_type == "text":
        msg = get_data.get("message")
        # 使用 图灵机器人
        # result = tuling_text(msg)
        # result["AIRECV"]["sender"] = "ai"
        # result["AIRECV"]["receiver"] = sender
        # return jsonify(result)

        result = _rebot(msg)

        try:
            res = result.get('content').format(br='\n').replace('★ ', '')
            # filename 会加上 ai 的前缀
        except Exception as e:
            res = '我不知道你在说什么?'
        return jsonify({
            "code": 0,
            "msg": "上传成功",
            "AIRECV":
                {
                    "sender": 'ai',
                    "receiver": sender,
                    "message": res,
                    "chat_type": "text",
                }
        })
    # 语音消息
    else:
        # receiver = get_data.get("receiver") # ai
        file = request.files.get("audio_file")

        filename = f"AI{uuid4()}.wav"  # 用户自己和ai的通讯,大写 Ai文件名前缀
        filepath = os.path.join(CHAT_PATH, filename)
        file.save(filepath)  # wav 格式的
        # print(filename, request.path, "已保存")

        # 是否需要存入和数据库中

        # 调用接口 将wav 转化为 pcm ,语音识别转换为文字 Q来接受它
        Q = speech_to_text(filepath)  #

        # 青云客
        ret = REBOT_TALK(Q, sender)  # type:dict

        # 图灵
        # ret = tuling(Q,sender)

        ret["AIRECV"]["sender"] = 'ai'
        ret["AIRECV"]["receiver"] = sender
        ret["DATA"] = {
            "sender": sender,
            "receiver": 'ai',
            "message": filename,
            "chat_type": "audio",
        }
        return jsonify(ret)


# 　主动发起聊天
# 我要给爸爸发消息


"""
//1.ai响应播放音乐
{
"from_user": "ai",
"music": music_name
}
"""

"""
//2.ai响应语音消息
{
    "from_user": "ai", 
    "chat": filename
}
//3.ai响应主动发起消息
{
    "from_user":friend_id,
    "chat":filename,
    "friend_type":app/toy
}
"""
