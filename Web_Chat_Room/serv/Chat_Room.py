# -*- coding:utf-8 -*-

from flask import Blueprint, request, render_template, session
from Config import CHAT_ROOM
from bson import ObjectId

cbp = Blueprint("Chat_Room", __name__)


@cbp.route("/wechat")
def wechat():
    user_id = session.get("user_id")
    user = session.get("user")
    print(user, user_id)
    friend_list = []
    friend_dict = CHAT_ROOM.Users.find_one({"_id": ObjectId(user_id),}, {"friend_list": 1})
    for item in friend_dict.get("friend_list"):
        if item.get("friend_form") != "group":
            friend_list.append(item)
    return render_template("wechat.html", user_id=user_id, user=user, friend_list=friend_list)


@cbp.route("/wechatmany")
def wechatmany():
    user_id = session.get("user_id")
    user = session.get("user")
    print(user_id, user)

    # 获取通讯录内的群聊
    group_list = []
    group_dict = CHAT_ROOM.Users.find_one({"_id": ObjectId(user_id)},
                                          {"friend_list": 1})
    for item in group_dict.get("friend_list"):
        if item.get("friend_form") == "group":
            group_list.append(item)

    print(group_list)
    return render_template("wechatmany.html", user_id=user_id, user=user, group_list=group_list)


@cbp.route("/aichat")
def aichat():
    user_id = session.get("user_id")
    user = session.get("user")
    print(user_id, user)
    return render_template("aichat.html", user_id=user_id, user=user)
