import os
import uuid

from bson import ObjectId
from flask import Blueprint, request, render_template, redirect, url_for, session, jsonify
from flask import make_response
from Config import AVATAR_PATH
from Config import CHAT_ROOM  # 库

fbp = Blueprint("Friend", __name__)


# 添加群聊
@fbp.route("/add_group", methods=["get", "post"])
def add_group():
    user_id = session.get("user_id")
    user = session.get("user")
    if request.method == "GET":
        return render_template("add_group.html", user=user, user_id=user_id)
    else:
        add_info = request.form.to_dict()
        add_user = add_info.get('add_user')  # 添加者,用户,发添加群聊者
        rece_user = add_info.get('rece_user')  # 被加群聊的名称 title

        # 从群聊中查看是否存在
        group_dict = CHAT_ROOM.Chats.find_one({"title": rece_user, "chat_form": "group"})

        if not group_dict:
            response_data = {
                "code": 1,
                "msg": "不存在该群聊"
            }
            return jsonify(response_data)

        # 如果存在该群聊,判断群主是否是该用户
        if group_dict.get("owner_id") == user_id:
            response_data = {
                "code": 2,
                "msg": "您已经是群主了,不需要添加该群"
            }
            return jsonify(response_data)

        # 获取当前添加用户信息,判断通讯录内是否有该群聊# 通讯录已经添加群聊了
        add_user_info = CHAT_ROOM.Users.find_one({"_id": ObjectId(user_id), "friend_list.friend_form": 'group',
                                                  "friend_list.friend_user": rece_user,
                                                  })
        if add_user_info:
            response_data = {
                "code": 3,
                "msg": "您已经添加该群聊了"
            }
            return jsonify(response_data)

        # 开始添加群聊
        # chats 更新
        CHAT_ROOM.Chats.update_one({"_id": group_dict.get("_id")}, {"$push": {"user_list": add_user}})
        # 对应user的通讯录更新
        friend_info = {
            "friend_id": str(group_dict.get("_id")),  # 聊听窗口
            "friend_user": rece_user,  # 使用标题
            "friend_avatar": "",  # 空
            "friend_chat": str(group_dict.get("_id")),  # 聊听窗口
            "friend_form": "group"  # 是群聊
        }
        CHAT_ROOM.Users.update_one({"_id": ObjectId(user_id)}, {"$push": {"friend_list": friend_info}})

        return jsonify({
            "code": 0,
            "msg": "群聊添加成功"
        })


# 添加好友
@fbp.route("/add_frinend", methods=["get", "post"])
def add_frinend():
    if request.method == "GET":
        user_id = session.get("user_id")
        user = session.get("user")
        return render_template("add_frinend.html", user=user, user_id=user_id)
    else:
        add_info = request.form.to_dict()
        add_user = add_info.get('add_user')  # 添加者
        rece_user = add_info.get('rece_user')  # 被加好友者
        print(add_info)
        add_user_info = CHAT_ROOM.Users.find_one({"user": add_user})
        rece_user_info = CHAT_ROOM.Users.find_one({"user": rece_user})
        if rece_user_info:
            # CHAT_ROOM.Request.insert_one()
            add_info['avatar'] = add_user_info.get("user_avatar")
            add_info['status'] = 0
            CHAT_ROOM.Request.insert_one(add_info)
            response_data = {
                "code": 0,
                "msg": "添加好友请求成功"
            }
        else:
            response_data = {
                "code": 1,
                "msg": "不存在该好友信息"
            }
        return jsonify(response_data)


# 用于用户查询好友请求
@fbp.route("/req_list", methods=['post'])
def req_list():
    """
    发送过来的{"_id":user_id, //app用户Id}
    :return:
    """
    user_info = request.form.to_dict()  # user:user
    user_info["rece_user"] = user_info.pop("add_user")
    user_info["status"] = 0
    Req_list = list(CHAT_ROOM.Request.find(user_info, {"_id": 0}))

    # $in 方式查询,且是没有处理的请求,排除已经拒绝的 和 已经同意的.接收方的id是app的绑定的toy_id 列表中的 数据
    ## ps: 可以设置 拒绝的i请求直接场数据库删除
    # Req_list = AI_TOY.Request.find({"toy_id": {"$in": bind_toys}, "status": 0})

    # 返回数据
    ret = {
        "code": 0,
        "msg": "查询好友请求",
        "DATA": Req_list}
    return jsonify(ret)


# 拒绝
#  App拒绝好友请求接口:
@fbp.route("/ref_req", methods=['post'])
def ref_req():
    ref_req_info = request.form.to_dict()
    # 更新对应的好友请求信息设置status : 2 ,拒绝状态.
    # ps : 直接删除吧
    CHAT_ROOM.Request.update_one(ref_req_info, {"$set": {"status": 2}})
    ret = {
        "code": 0,
        "MSG": "拒绝添加好友",
        "DATA": {}}
    return jsonify(ret)


#  同意好友请求接口:
@fbp.route("/acc_req", methods=['post'])
def acc_req():
    acc_req_info = request.form.to_dict()

    req_info = CHAT_ROOM.Request.find_one(acc_req_info)

    # 发送请求方消息
    add_user_info = CHAT_ROOM.Users.find_one({"user": acc_req_info.get("add_user")}, {"_id": 1})
    # 请求方消息
    rece_user_info = CHAT_ROOM.Users.find_one({"user": acc_req_info.get("rece_user")})
    """
    {
        "_id" : ObjectId("5ca5bfbaea512d269449ed1b"), // 自动生成ID
        "add_user" : "5ca17c7aea512d26281bcb8d", // 发起好友申请方
        "rece_user" : "5ca17f85ea512d215cd9b079", // 收到好友申请方
        "req_info" : "我是仇视单", // 请求信息
        "avatar" : "toy.jpg", // 发起方的头像
        "status" : 1, // 请求状态 1同意 0未处理 2拒绝
    }
"""

    # 构建聊天窗口,获取chat_id,单聊无所谓title
    chat_window = CHAT_ROOM.Chats.insert_one(
        {"user_list": [acc_req_info.get("add_user"), acc_req_info.get("rece_user")], "chat_list": [],
         "chat_form": "single", "title": str(uuid.uuid4())})
    chat_window_id = str(chat_window.inserted_id)
    #
    # # 构建frinend_list的 保存的名片
    # 发起方的通讯录名片,存储接收方的信息
    send_data = {
        "friend_id": rece_user_info.get("_id").__str__(),
        "friend_user": acc_req_info.get("rece_user"),  # 接收方的昵称
        "friend_avatar": rece_user_info.get("user_avatar"),  # 接收方的avatar
        "friend_chat": chat_window_id,  # 聊天窗口,暂时为空
        "friend_form": "single",  # 聊天类型为单聊
    }

    # 同样接收方的user绑定toy的信息,存储发起方的信息
    receiver_data = {
        "friend_id": add_user_info.get("_id").__str__(),  # 发起方的id
        "friend_user": acc_req_info.get("add_user"),  #
        "friend_avatar": req_info.get("avatar"),  # 发起方的avatar
        "friend_chat": chat_window_id,  # 连天窗口,暂时为空
        "friend_form": "single",  # 聊天类型为单聊
    }

    # 发起方数据更新
    CHAT_ROOM.Users.update_one({"_id": add_user_info.get("_id")}, {"$push": {"friend_list": send_data}})

    # 接收方数据更新
    CHAT_ROOM.Users.update_one({"_id": rece_user_info.get("_id")}, {"$push": {"friend_list": receiver_data}})

    # Request 数据更新,将好友请求改为已处理
    CHAT_ROOM.Request.update_one({"_id": ObjectId(req_info.get("_id"))}, {"$set": {"status": 1}})

    ret = {
        "code": 0,
        "MSG": "同意添加好友",
        "DATA": {}}
    return jsonify(ret)

# 收取消息
@fbp.route("/chat_list", methods=['post'])
def chat_list():
    # 获取传递过来的参数
    user_info = request.form.to_dict()

    chat_window_id = user_info.get("chat_id")

    chat_dict = CHAT_ROOM.Chats.find_one({"_id": ObjectId(chat_window_id)}, {"chat_list": 1})

    chat_list = chat_dict.get("chat_list")
    chat_list.reverse()
    # count,sender = get_msg_one(user_info.get("from_user"), user_info.get("to_user"))

    # # 通过子集查询 获取聊天窗口
    # chat_window = AI_TOY.Chats.find_one({"user_list": {"$all": user_list}}, {"chat_list": 1, "_id": 0})

    return jsonify({"code": 0, "chat_list": chat_list})


# 创建聊天类接口
@fbp.route("/create_group", methods=["get", "post"])
def create_group():
    user_id = session.get("user_id")
    user = session.get("user")
    if request.method == "GET":
        return render_template("create_group.html", user_id=user_id, user=user, )
    else:
        group_info = request.form.to_dict()
        print(group_info)
        group_info.update({
            "chat_form": "group",
            'owner': user,
            "owner_id": user_id,
            "user_list": [user, ],
            'chat_list': []
        })
        if CHAT_ROOM.Chats.find_one(group_info):
            return jsonify({"code": 1, "msg": "群聊名称重复"})

        # 创建群聊成功
        chat_window = CHAT_ROOM.Chats.insert_one(group_info)
        chat_window_id = str(chat_window.inserted_id)

        # 在通讯录内加上那个信息
        friend_info = {
            "friend_id": chat_window_id,  # 聊听窗口
            "friend_user": group_info.get("title"),  # 使用标题
            "friend_avatar": "",  # 空
            "friend_chat": chat_window_id,  # 聊听窗口
            "friend_form": "group"  # 是群聊
        }

        # 在对应user表内
        CHAT_ROOM.Users.update_one({"_id": ObjectId(user_id)}, {"$set": {"bind_group": [chat_window_id, ]}})
        CHAT_ROOM.Users.update_one({"_id": ObjectId(user_id)}, {"$push": {"friend_list": friend_info}})

        return jsonify({"code": 0, "msg": "创建群聊成功"})
