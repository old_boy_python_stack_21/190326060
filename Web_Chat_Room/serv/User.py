# -*- coding:utf-8 -*-
# __author__ = Deng Jack
import os

from flask import Blueprint, request, render_template, redirect, url_for, session
from flask import make_response
from Config import AVATAR_PATH
from Config import CHAT_ROOM  # 库

from uuid import uuid4

ubp = Blueprint("User", __name__)


@ubp.route("/reg.html", methods=["post", "get"])
def reg():
    if request.method == "GET":
        return render_template('reg.html')
    else:
        user_info = request.form.to_dict()

        if user_info.get("pwd") != user_info.get("repwd") or not bool(user_info.get("pwd")) or not bool(
                user_info.get("repwd")):
            error = "两次密码输入不一致"
            return render_template("reg.html", error=error)
        elif "&" in user_info.get("user"):
            error = "用户名内不能包含 & 等特殊字符"
            return render_template("reg.html", error=error)
        user_info.pop("repwd")
        user_avatar = request.files.get("user_avatar")
        filename = f"{uuid4()}.jpg"
        user_avatar.save(os.path.join(AVATAR_PATH, filename))
        user_info["user_avatar"] = filename
        user_info["friend_list"] = []
        CHAT_ROOM.Users.insert_one(user_info)
        return redirect(url_for("User.login"))


@ubp.route("/login.html", methods=["post", "get"])
def login():
    if request.method == "GET":
        return render_template('login.html')
    else:
        user_data = request.form.to_dict()
        user_info = CHAT_ROOM.Users.find_one(user_data)
        if user_info:
            session["user_id"] = user_info.get("_id").__str__()
            session["user"] = user_info.get("user")
            # 跳转至首页
            return redirect(url_for("index"))
        error = "用户名或者密码错误"
        return render_template('login.html', error=error)


@ubp.route("/logout", methods=["get"])
def logout():
    session.pop("user")
    session.pop("user_id")
    return redirect(url_for("User.login"))
