# -*- coding:utf-8 -*-
# __author__ = Deng Jack
from redis import Redis

# app 配置
class DebugConfig:
    DEBUG = True
    SESSION_TYPE = "redis"
    SESSION_REDIS = Redis(host="127.0.0.1",port=6379,db=10)


# app url 白名单的设置
WHITE_LIST = [
    "^/$",
    "^/reg.html$",
    "^/login.html$",
    "^/favicon.ico",
    "^/static/.*",
]
# 免认证的校验
NO_PERMISSION_LIST = [

]

# MongoDB 数据库配置
from pymongo import MongoClient
MC = MongoClient("127.0.0.1",27017)
CHAT_ROOM = MC.CHAT_ROOM    # 库名

# Redis的数据库配置
from redis import Redis  # Redis放在内网中,有bug
RDB = Redis("127.0.0.1",6379)


APP_HOST = "192.168.11.47"
APP_PORT = 8000

WS_APP_HOST = "0.0.0.0"
WS_APP_PORT = 8889



# 文件保存
AVATAR_PATH = "Avatar"
CHAT_PATH = "Chat"
MUSIC_PATH = 'Music'
COVER_PATH = 'Cover'


# 联图二维码接口调用
QR_URl = "http://qr.liantu.com/api.php"

# 响应格式
RET = {"code": 0, "msg": "", "data": {}}
UPRET = {"CODE": 0, "MSG": "", "DATA": {}}


# 百度API接口
from aip import AipSpeech ,AipNlp

APP_ID = '16980626'
API_KEY = 'AoqdnxMgh3t4rtDDgBMdZGIk'
SECRET_KEY = 'pPIZx6eeYx0OmwYn4Qks0dnnQ99aGmVj'

BAIDUAPICLIENT = AipSpeech(APP_ID, API_KEY, SECRET_KEY)
BAIDU_NLP_APICLENT = AipNlp(APP_ID, API_KEY, SECRET_KEY)


# 青云客智能机器人
QYK_ROBOTRUL = 'http://api.qingyunke.com/api.php'

# 图灵机器人配置
tl_url = "http://openapi.tuling123.com/openapi/api/v2"
tl_data = {
    "perception": {
        "inputText": {
            "text": ""
        }
    },
    "userInfo": {
        "apiKey": "f5eeea1d929b448ba37aed5b933b8038",
        "userId": ""
    }
}


# 老师的配置
TL_URL = "http://openapi.tuling123.com/openapi/api/v2"
TL_DATA = {
    "perception": {
        "inputText": {
            "text": "文本内容"
        }
    },
    "userInfo": {
        "apiKey": "51ff3d2dd9464ba6bba97ff1bb9427ab",
        "userId": "替换成ToyId"
    }
}