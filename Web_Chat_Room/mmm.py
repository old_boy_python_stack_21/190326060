# -*- coding:utf-8 -*-
# __author__ = Deng Jack

import requests
import json
import time

headers = {
    "User-Agent": "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36",
}
proxies= {
    "http":"http://117.28.96.198:9999",
    "https":"https://117.28.96.198:9999",
}
# 评论的url地址 ,get请求获取,
urls = "http://125.35.6.84:81/xk/itownet/portalAction.do?method=getXkzsList"
company_list = []
datas = {
    'on': 'true',
    'page': "",
    'pageSize': '15',
    'productName': '',
    'conditionType': '1',
    'applyname': '',
    'applysn': '',
}
for page in range(1, 6):
    datas["page"] = str(page)
    responses = requests.post(url=urls, data=datas, headers=headers,proxies =proxies)
    time.sleep(3)
    print(responses)
    print(responses.json())
    for data_dict in responses.json()['list']:
        url = 'http://125.35.6.84:81/xk/itownet/portalAction.do?method=getXkzsById'
        data = {
            'id': data_dict['ID']
        }
        response = requests.post(url=url, data=data, headers=headers,proxies =proxies).json()
        print(response)
        comapny_dic = {
        '企业名称':response['epsName'],
        '许可证编号':response['productSn'],
        '许可项目':response['certStr'],
        '企业住所':response['epsAddress'],
        '生产地址':response['epsProductAddress'],
        '社会信用代码':response['businessLicenseNumber'],
        '法定代表人':response['legalPerson'],
        '企业负责人':response['businessPerson'],
        '质量负责人':response['qualityPerson'],
        '发证机关':response['qfManagerName'],
        '签发人':response['xkName'],
        '日常监督管理机构':response['rcManagerDepartName'],
        '日常监督管理人员':response['rcManagerUser'],
        '有效期至':response['xkDate'],
        '发证日期':response['xkDateStr']
        }
        company_list.append(comapny_dic)
with open("company_list.json","w",encoding="utf-8") as f:
    json.dump(company_list,f,ensure_ascii=False)