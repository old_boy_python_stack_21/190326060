# -*- coding:utf-8 -*-
# __author__ = Deng Jack
import time

from flask import Flask, Markup, request, render_template, jsonify
import os
import json
from threading import Thread

from geventwebsocket.handler import WebSocketHandler
from geventwebsocket.server import WSGIServer
from geventwebsocket.websocket import WebSocket
from geventwebsocket.exceptions import WebSocketError
from Config import UPRET, WS_APP_HOST, WS_APP_PORT

ws_app = Flask(__name__)

user_websocket_dict = {}
from Config import CHAT_ROOM
from bson import ObjectId


@ws_app.before_request
def process_request():
    ws_app.logger.error(f"{request.method}  {request.url}")


def get_websocket_dict():
    while True:
        time.sleep(30)
        print(f"当前web链接个数为{len(user_websocket_dict)}", user_websocket_dict)


# 单聊 和 群聊
@ws_app.route("/ws/<username>")
def my_ws(username):
    # print(request.environ)

    if "&" in username:
        # 此时要出创建群聊空间,以群聊chid_id为键 的结构
        user, chat_id = username.split("&")  # 要求用户名内不能存在 &
        print(user, chat_id)
        ws_socket = request.environ.get("wsgi.websocket")  # type:WebSocket
        print(ws_socket)
        if user_websocket_dict.get(chat_id, None):
            # 存在的话
            user_websocket_dict.get(chat_id).update({user: ws_socket})
        else:
            user_websocket_dict[chat_id] = {user: ws_socket}
        # user_websocket_dict = {
        #   alex:  ws_socket,
        #     deng:ws_socket,  -->为单聊服务
        #    chat_id:{     user:ws_socket,
        #                    user :ws_socket,        }  结构 -->为群聊服务,方便去重
        # }

        print(len(user_websocket_dict), user_websocket_dict)
    else:
        ws_socket = request.environ.get("wsgi.websocket")  # type:WebSocket
        user_websocket_dict[username] = ws_socket
        print(len(user_websocket_dict), user_websocket_dict)
    while True:
        try:
            msg = ws_socket.receive()
            msg_dict = json.loads(msg)
            if not msg_dict.get("receiver"):
                msg_dict["count"] = len(user_websocket_dict.get(chat_id, {}))
                msg_dict["createTime"] = time.time()
                # print("是一个群聊", msg_dict)
                # 跟新群聊数据,或者批量更新要好些
                CHAT_ROOM.Chats.update_one({"_id": ObjectId(chat_id)},
                                           {"$push": {"chat_list": msg_dict}})
                # 循环更新群数据
                for sock_item in user_websocket_dict.get(chat_id, {}).values():
                    if sock_item == ws_socket:
                        continue
                    sock_item.send(json.dumps(msg_dict))

            else:
                receiver = msg_dict.get("receiver")
                msg_dict["createTime"] = time.time()
                print("是一个单聊", msg_dict)
                CHAT_ROOM.Chats.update_one({"_id": ObjectId(msg_dict.get("chat_id"))},
                                           {"$push": {"chat_list": msg_dict}})
                receiver_socket = user_websocket_dict.get(receiver)
                try:
                    receiver_socket.send(msg)
                except AttributeError:
                    print("对象不在线上")  # 可以在这里系统提示 好友是离线的
        except (WebSocketError, TypeError):
            try:
                user_websocket_dict.pop(username)
            except KeyError:
                user_websocket_dict.get(chat_id).pop(user)
            # 这里的return 是因为 用户关闭浏览器 了,websocket退出了
            print("该用户退出了")
            return jsonify({"code":1,"msg":"用户已关闭连接"})


if __name__ == '__main__':
    http_serv = WSGIServer((WS_APP_HOST, WS_APP_PORT), ws_app, handler_class=WebSocketHandler)
    ws_app.logger.error(f"Running on http://{WS_APP_HOST}:{WS_APP_PORT}/")
    tp = Thread(target=get_websocket_dict)
    tp.daemon = True
    tp.start()
    http_serv.serve_forever()
