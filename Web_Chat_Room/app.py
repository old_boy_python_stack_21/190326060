# -*- coding:utf-8 -*-
from re import match

from flask import Flask ,request,session ,redirect ,url_for ,render_template


from Config import APP_HOST,APP_PORT,WHITE_LIST,NO_PERMISSION_LIST

from flask_session import Session


from serv.User import ubp
from serv.Chat_Room import cbp
from serv.Uploader import upbp # 音频上传相关API
from serv.Content import contbp # 获取内容相关API
from serv.Friend import fbp # 获取好友相关API

# 配置
app = Flask(__name__,template_folder="templates",static_folder="static")

app.config.from_object("Config.DebugConfig")

# 蓝图
app.register_blueprint(ubp)  # 用户类,群主类

app.register_blueprint(cbp)  # 聊天室类

app.register_blueprint(upbp)  # 上传文件类类

app.register_blueprint(contbp)  # 获取内容

app.register_blueprint(fbp)  # 获取好友类

Session(app)


@app.before_request
def process_request():
    # 获取当前访问的URL
    path = request.path  # 不带参数

    print(request.remote_addr)

    # 白名单放过
    for pattern in WHITE_LIST:
        if match(pattern, path):
            return  # 后面的代码不执行

    # 登录状态的校验
    if not session.get('user_id'):
        return redirect(url_for("User.login"))  # 让用户登陆

    # 免认证的校验
    for pattern in NO_PERMISSION_LIST:
        if match(pattern, path):
            return

@app.route("/index",methods=["get"])
def index():
    return render_template("index.html")

if __name__ == '__main__':
    app.run(APP_HOST,APP_PORT)