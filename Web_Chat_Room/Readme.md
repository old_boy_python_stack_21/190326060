# 版本说明

## 1.聊天室 1.0版本

1. 实现单聊

   - 点对点之间的聊天,只要用对方用户名即可实现

2. 实现群聊天

   - 所有人在一个群里面实现聊天,一人发出的消息所有人都能看到

3. 实现和机器人聊天

   - 可以个机器人发送语音消息,机器人也会发语音消息给你,基于百度API和图灵机器人实现
   - 可以个机器人发送文本消息,机器人也会发文本消息给你,基于百度API和图灵机器人实现

4. 基于html的前端websocket和python服务端代码实现单聊和群聊天的发送文本消息.

5. 基于html的前端websocket和python服务端代码实现单聊和群聊天的发送语音消息.

6. 基于html的前端websocket和python服务端代码实现单聊和群聊天的发送图片

7. 通过@app.before_request 实现权限的验证,白名单的验证和登陆的验证

   未登录自动跳转到登陆界面

   ```html
   <input type="file" id="file" accept="image/gif,image/jpeg,image/x-png" />
   基于前端是input只接受gif , jpg  和 png格式的图片文件
   
   //下面，是常用26中文件类型格式……可以收藏备查！
   1.accept="application/msexcel"
   2.accept="application/msword"
   3.accept="application/pdf"
   4.accept="application/poscript"
   5.accept="application/rtf"
   6.accept="application/x-zip-compressed"
   7.accept="audio/basic"
   8.accept="audio/x-aiff"
   9.accept="audio/x-mpeg"
   10.accept="audio/x-pn/realaudio"
   11.accept="audio/x-waw"
   12.accept="image/gif"
   13.accept="image/jpeg"
   14.accept="image/tiff"
   15.accept="image/x-ms-bmp"
   16.accept="image/x-photo-cd"
   17.accept="image/x-png"
   18.accept="image/x-portablebitmap"
   19.accept="image/x-portable-greymap"
   20.accept="image/x-portable-pixmap"
   21.accept="image/x-rgb"
   22.accept="text/html"
   23.accept="text/plain"
   24.accept="video/quicktime"
   25.accept="video/x-mpeg2"
   26.accept="video/x-msvideo"
   ```

8. 实现图片消息 和 音频消息的存储,使用MongoDB数据库存储.

9. 实现群聊在线人数的显示.基于服务端len(user_websocket_dict)实现,并展示到前端页面上.

10. bug:

   ```python
   1.目前服务端所有websocket全部在一个字典中
   	{user:websocket....}
     所以只要到单聊输入对方用户名,发送消息,web
     此时对方在群聊界面,该对方websocket就可以收取到消息.并显示在当前群聊界面.
     虽然其他群聊的用户不会收到该信息.但此时用户体验不好.
   ```

## 1.1版本

1. 实现mongodb的request表存添加好友请求.
2. 同意或者拒绝好友,只能和好友单聊
3. 实现添加好友,互相绑定,创建聊天窗口.保存2人的聊天信息.
4. 收取历史消息chat_list实现.基于俩者之间的聊天.
5. 实现创建群聊功能,单聊和群聊子啊不同的空间中,单聊只能面对面发,群聊能够发给群里面.
6. 浏览器开2个窗口,即可实现单聊和群聊的功能呢,2则发消息不相干.
7. 实现收取历史消息.
8. 添加群聊功能实现,-->直接添加群聊,暂时没有实现群主同意后才能加入群聊,以后看情况补充.
9. 将1.0版本的bug完善,隔离了单聊和群聊的消息联通

## 1.2版本(暂定)

- 欲实现功能
  1. 群主同意后才能加入到群聊当中
  2. 未读消息的实现-->通过 redis来实现
  3. 首页文字内容的完善.
  4. 未登录跳转到登陆界面后,登陆成功自动转到 用户I型安要登陆的界面,而不是首页index.
  5. 文件的上传和下载(聊天群之间 和 好友之间 通过网络 传递文件)
  6. 收取消息时,加对方发送信息的时间显示出来
  7. 实现前后端分离,只提供接口
     1. 得事先写好html页面,但浏览器页面间的传值不清楚如何实现
     2. 预编写手机app版本,敬请期待.(ps:这是不可能的)
  8. `......需求永无止尽`

# MongoDB数据结构

## 1.用户表

- Users

- ```python
  {
      "_id" : ObjectId("5c9d8da3ea512d2048826260"),  //自动生成ID
  	"user" : "asdf", //用户名
  	"pwd" : "962012d09b8170d912f0669f6d7d9d07", //密码
  	"user_avatar" : "baba.jpg", // 用户头像'
      "bind_group":["5d61dc6d3f2cbb7ca20sadx",] #  创建的群聊,是群主
      "friend_list":[
         	   {
                  "friend_id": "5d61db028840eedfa6f33a11",
                  "friend_user": "wusir",
                  "friend_avatar": "4763aa2c-de92-4769-ad8f-b31090b28795.jpg",
                  "friend_chat": "5d61dc6d3f2cbb7ca209975a",
                  "friend_form": "single"  
              }, # 通讯录中的好友
           {
                  "friend_id": "5d61dc6d3f2cbb7ca20sadx",
                  "friend_user": "wusir",
                  "friend_avatar": "",
                  "friend_chat": "5d61dc6d3f2cbb7ca20sadx",
                  "friend_form": "group"
              } # 通讯录中的群聊
      ],
       
      
  }
  ```

## 2.聊天信息表

- Chats

- ```python
  {
      "_id" : ObjectId("5c9d8da3ea512d2048826260"),,
      user_list:[sender,receiver,xxxx,xxxx],
      title: "群聊名称", 
      chat_form:"single or group",  # 单聊 或者群聊 窗口
      owner: -->user, # 所有者,群主是谁(好友间的聊天窗口无此信息)
      owner_id: -->user_id, # 群主的user id,(好友间的聊天窗口无此信息)
      chat_list:[
           {
          "sender": sender,  # 信息发送方ID
          "receiver": receiver,  # 信息接收方ID
          "chat_type": "image",  # 语音消息文件名
          "createTime": time.time(),  # 聊天创建时间
          "message": filename,
      },
           {
          "sender": sender,  # 信息发送方ID
          "receiver": receiver,  # 信息接收方ID
          "chat_type": "audio",  # 语音类型
          "createTime": time.time(),  # 聊天创建时间
          "message": filename,
      },
           {
          "sender": sender,  # 信息发送方ID
          "receiver": receiver,  # 信息接收方ID
          "chat_type": "text",  # xaipxi类型
          "createTime": time.time(),  # 聊天创建时间
          "message": xxxxxx,}
      ] # 聊天信息
  }
  ```

## 3.好友请求信息数据表

- Request

- ```python
  {
  	"_id" : ObjectId("5ca5bfbaea512d269449ed1b"), // 自动生成ID
  	"add_user" : "5ca17c7aea512d26281bcb8d", // 发起好友申请方
  	"rece_user" : "5ca17f85ea512d215cd9b079", // 收到好友申请方
  	"req_info" : "我是仇视单", // 请求信息
  	"avatar" : "toy.jpg", // 发起方的头像
  	"status" : 1, // 请求状态 1同意 0未处理 2拒绝
  }  # 好友发起添加请求表
  ```

# 后端相应API接口

## 1.wesocket连接服务端

1. ```python
   "/ws/<username>"
   
   通过该函数实现单聊和群聊的websocket的通信
   user_websocket_dict = {
   	user1:websocket,
   	user2:websocket,
   	
   	# 以上是单聊
       chat_id:{
           user3:websocket,
           user4:websocket,
           user5:websocket,
           user6:websocket,
       }
   	# 以上是群聊,以群聊窗口的_id 作为键,值为字典
   }
   ```

## 2.app 后端API

### 1.返回html页面

1. /index   首页,返回index.html页面
2. 用户登陆登出注册
   	1. /login  登陆
    	2. /reg   注册
    	3. /logout  登出
 3. 聊天室类
     1. /wechat  单聊页面
     2. /wechatmany  群聊页面
     3. /aichat    和机器人互动页面

### 2.API接口,返回json类型

#### 1.上传文件API

1. /image_uploader 上传聊天图片

   ```python
   {
           "code": 0,
           "msg": "上传成功",
           "DATA":
               {"filename": filename, # 上传的文件名
                "chat_type": "image",}
       }
   ```

2. /audio_uploader  上传聊天语音

   ```python
   {
       "code": 0,
       "msg": "上传成功",
       "DATA":
       {
           "filename": filename, # 上传的文件名
           "chat_type": "audio",
       }
   }
   ```

3. /aiaudio_uploader  和ai聊天上传语音或者文字信息

   ```python
   {
       "code": 0,
       "msg": "上传成功",
       "AIRECV":
       {
           "sender": 'ai',
           "receiver": sender,
           "message": res,
           "chat_type": "text",
       }  # ai的响应的文本信息
   }
   
   
   {
       "code": 0,
       "msg": "上传成功",
       "DATA":{
           "sender": sender,
           "receiver": 'ai',
           "message": filename,
           "chat_type": "audio",
       },    # 用户上传得到的语音消息
       "AIRECV":
       {
           "sender": 'ai',
           "receiver": sender,
           "message": filename,
           "chat_type": "audio",
       }  # ai响应用户的语音消息   >>结合百度API TTS 和 图灵Robot
   }
   ```

#### 2.好友 和群聊之间的API

1. /add_group   添加群聊

   ```python
   1.GET请求
   返回  "add_group.html" 页面
   
   
   2.POST请求
   response_data = {
       "code": 1,
       "msg": "不存在该群聊"
   }
   response_data = {
       "code": 2,
       "msg": "您已经是群主了,不需要添加该群"
   }
   response_data = {
       "code": 3,
       "msg": "您已经添加该群聊了"
   }
   {
       "code": 0,
       "msg": "群聊添加成功"
   }
   
   ```

2. /add_frinend 添加好友

   ```python
   1.GET请求
   返回  "add_frinend.html" 页面
   
   2.POST 请求
   response_data = {
       "code": 0,
       "msg": "添加好友请求成功"
   }
   response_data = {
       "code": 1,
       "msg": "不存在该好友信息"
   }
   ```

3. /req_list 用于用户查询自己收到的添加好友请求

   ```python
   POST 请求
   ret = {
       "code": 0,
       "msg": "查询好友请求",
       "DATA": Req_list,   # 好友请求的列表
   }
   ```

4. /ref_req  拒绝添加好友

   ```python
   POST 请求
   ret = {
       "code": 0,
       "MSG": "拒绝添加好友",
       "DATA": {}}
   ```

5. /acc_req  同意好友请求接口

   ```python
   POST 请求
   ret = {
       "code": 0,
       "MSG": "同意添加好友",
       "DATA": {}}
   ```

6. / chat_list   收取消息请环球

   ```python
   POST 请求
   {"code": 0, "chat_list": chat_list} # 聊天消息的列表
   ```

7. /create_group  用户创建群聊

   ```python
   1.GET 请求
   返回 "create_group.html" 页面
   
   2.POST 请求
   {"code": 0, "msg": "创建群聊成功"}
   {"code": 1, "msg": "群聊名称重复"}
   ```

   

### 3.返回内容类

1. /get_cover/<filename>'    # 用于用户获取聊天的图片,返回文件二进制流

2. /get_music/<musicname>' ,# 用于用户上传成功的提示,预删除

3. /avatar/<avatarname>   #   # 用于用户获取注册时的头像

4. /get_chat/<musicname>  # 用户用于用户获取聊天的语音信息,返回文件二进制流

   

