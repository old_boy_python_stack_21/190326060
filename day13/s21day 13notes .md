# Python全栈day 13

## 一 . 昨日内容回顾

1. 函数的重点

   - 参数位置传参/关键字参数

   - 万能参数*args/**kwargs

   - map/filter/reduce

   - 函数可以做参数,变量,返回值

   - 函数的参数传递的是内存地址

   - 返回值,默认返回 none  .返回多个数据,自动转为元组.

   - 注意: 函数不被调用,内部代码永不执行 .

   - 执行函数时，会新创建一块内存保存自己函数执行的信息 => 闭包

     都会为此次调用开辟一块内存，内存可以保存自己以后想要用的值。

   - hasnlib加密用模块

   - getpass,输入密码时隐藏,把密码自动换成****

   - random 随机数模块

## 二 .今日内容

### 1. 装饰器

装饰器：在不改变原函数内部代码的基础上，在函数执行之前和之后自动执行某个功能。 

1. ```python
   示例:
   # #########################################
   def func(arg):
       def inner():
           arg()
       return inner
   
   def f1():
       print(123)
   
   v1 = func(f1)
   v1()
   # ###########################################
   def func(arg):
       def inner():
           arg()
       return inner
   
   def f1():
       print(123)
       return 666
   
   v1 = func(f1)   # 运行func(),创建一块空间,定义 inner函数.返回值 inner
   result = v1() # 执行inner函数 / f1含函数 -> 123 
   print(result) # None
   # ###########################################
   def func(arg):
       def inner():
           return arg()
       return inner
   
   def f1():
       print(123)
       return 666
   
   v1 = func(f1)
   result = v1() # 执行inner函数 / f1含函数 -> 123
   print(result) # 666
   
   ```

2. 装饰器的本质

   ```python
   def func(arg):
       def inner():
           print('before')  
           v = arg()
           print('after')
           return v 
       return inner 
   
   def index():
       print('123')
       return '666'
   
   index = func(index)  # --->指向inner函数.
   index()    # ---> 运行inner函数-->运行打印'before'/原index函数()/运行打印'after'
   
   ```

3. 装饰器的格式

   ```python
   def func(arg):
       def inner():
           v = arg()
           return v 
       return inner 
   
   # 第一步：执行func函数并将下面的函数参数传递，相当于：func(index)
   # 第二步：将func的返回值重新赋值给下面的函数名。 index = func(index)
   @func    #  @func等价于上面2步
   def index():
       print(123)
       return 666
   
   print(index)
   
   ```

4. 应用

   ```python
   # 计算函数执行时间
   def wrapper(func):
       def inner():
           start_time = time.time()
           v = func()
           end_time = time.time()
           print(end_time-start_time)
           return v
       return inner
   
   @wrapper
   def func1():
       print(123454987+865484)
       
   func1()   # --->自动启动装饰器
       
   
   ```

5. 总结

   - 目的：在不改变原函数的基础上，再函数执行前后自定义功能。 

   - 编写装饰器 和应用

     ```python
     # 装饰器的编写
     def x(func):
         def y():
             # 前
             ret = func()
             # 后
             return ret 
        	return y 
     
     # 装饰器的应用
     @x
     def index():
         return 10
     
     @x
     def manage():
         pass
     
     # 执行函数，自动触发装饰器了
     v = index()
     print(v)
     ```

   - 应用场景：想要为函数扩展功能时，可以选择用装饰器。

6. 重点内容装饰器

   ```python
   def 外层函数(参数): 
       def 内层函数(*args,**kwargs):   # -->无论被装饰函数有多少个参数,都能用万能参数传入.
           return 参数(*args,**kwargs)
       return 内层函数
   
   @外层函数
   def index():
       pass
   
   index()    # --->自动调用装饰器
   ```

   

### 2.推导式

#### 2.1列表推导式

- 目的: 为了方便生成一个列表

  ```python
  #####基本格式#####
  v1 = [i for i in range(9)]    # 创建列表10个元素
  v2 = [i for i in range(9) if i > 5]   #对每个i,判断if i > 5,为真则添加到列表中
  v3 = [99 if i > 55 else 66 for i in range(9)]   # 三木运算
  """
  v1 = [i for i in 可迭代对象 ]
  v2 = [i for i in 可迭代对象 if 条件 ] # 条件为true才进行append
  """
  ######函数######
  
  def func():
      return i
  v6 = [func for i in range(10)]  # 10个func组成的列表,内存地址一致.
  result = v6[5]()     # 运行报错,提示 i 没有被定义,
  
  v7 = [lambda :i for i in range(10)]  # 10个lambda函数组成的列表,内存地址不一致,有10个
  result = v7[5]()   # 运行不报错,输出result 为 9
  
  ## 第一个func和列表生成器函数作用域都在第一级,找不到 i 
  ## 第二个lambda上级是列表生成器函数作用域
  
  
  ###面试题
  v8 = [lambda x:x*i for i in range(10)] # 新浪微博面试题
  # 1.请问 v8 是什么？    10个lambda函数,内存地址不一样,构建的列表
  # 2.请问 v8[0](2) 的结果是什么？    i 在列表生成式函数作用域里面,lambda是奇子集,i = 9,输出18
  
  ### 面试题
  def num():
      return [lambda x:i*x for i in range(4)]
  # num() -> [函数,函数,函数,函数]
  print([ m(2) for m in num() ]) # [6,6,6,6]
  
  ```

  

#### 2.2集合推导式

- 和列表推导式类似,这不再赘述.

#### 2.3字典推导式

```python
v1 = { 'k'+str(i):i for i in range(10) }  # 和列表类似,中间有:区分键和值.
```



### 3.重点

```python
#装饰器
@外层函数名  # index = xx(index)
def index():
    pass
index()

# import time 模块
```



