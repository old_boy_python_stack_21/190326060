#!/usr/bin/env python
# -*- coding:utf-8 -*-	 
# @Project  : 选课系统
# @Author : Jack Deng
# @Time   :  2019-04-30 16:09
# @File   : settings.py

import sys
import os

BASEPATH = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(BASEPATH)

LOG_FILE = os.path.join(BASEPATH,'log\log.log')
LoggerName = '选课系统'
Loggerleval = 20
LoggerTimeWhen = 'd'
LoggerTimeInterval = 1

COURSE_PATH = os.path.join(BASEPATH, 'db\course.txt')
USER_COURSE_PATH = os.path.join(BASEPATH, r'db\user_course.txt')
USER_PATH = os.path.join(BASEPATH, r'db\user.txt')

ADMIN_NAME = 'admin'
ADMIN_PWD = '2019'
