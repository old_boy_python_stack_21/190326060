#!/usr/bin/env python        	
# -*- coding:utf-8 -*-	 
# @Project  : 选课系统
# @Author : Jack Deng
# @Time   :  2019-05-03 09:41
# @File   : admin.py

import os
from src.student import *
from lib.logger import logger
from config import settings


class Admin(User):

    def __init__(self):
        self.name = 'admin'
        self.pwd = '2019'


    def create_courses(self):
        """
        创建课程
        :return:
        """
        try :
            course_dict = {}
            while True:
                name = input('请输入课程名字(输入N结束录入):')
                if name.upper() == "N":
                    break
                price = input('请输入课程价格:')
                period = input('请输入课程周期:')
                teacher = input('请输入教学老师:')
                course_obj = Course(name, price, period, teacher)
                course_dict.update({ name : course_obj})
            if not course_dict : print('没有课程录入'); return
            with open(settings.COURSE_PATH, 'ab') as f:           # 没有文件自动创建,ab模式
                f.seek(0)                                         # 把光标移到开头
                try:
                    val = pickle.load(f)
                except Exception as e:
                    logger.error(e, exc_info=True)                # 意味着f是空文件,load会报错,走except语句
                    val = {}
                val.update(course_dict)
            with open(settings.COURSE_PATH, 'wb')  as f:          # 清空,重写入
                pickle.dump(val, f, 0)
                print('成功录入课程')
        except Exception as e :
            logger.error(e, exc_info=True)


    def create_students(self):
        """
        创建学生名单{学号:student_obj}
        :return:
        """
        try :
            student_dict = {}
            while True :
                name = input('请输入学生姓名(输入N停止录入):')
                if name.upper() == 'N':
                    break
                id = input('请输入学生学号:')
                student_dict.update({id: Student(name ,id)})                # {学号 : sudent_obj}
            with open(settings.USER_PATH, 'ab') as f:                       # 没有文件自动创建
                try:
                    val = pickle.load(f)
                except Exception as e:
                    logger.error(e, exc_info=True)
                    val = {}
            with open(settings.USER_PATH, 'wb') as f:                       # 清空所有文件内容
                val.update(student_dict)
                pickle.dump(val, f, 0)
        except Exception as e :
            logger.error(e, exc_info=True)


    def look_students(self):
        """
        查看所有学生名单{学号:student_obj}
        :return:
        """
        try:
            with open(settings.USER_PATH, 'rb') as f:
                val = pickle.load(f)
                print('学号    学生姓名 ')
                for item in val.values():
                    print( item.student_id ,item.name)
        except Exception as e:
            logger.error(e, exc_info=True)
            print('文件是空的,找不到学生名单')


    def look_allstudents_courses(self):
        """
        查看学生所选课程
        :return :
        """
        try :
            if not os.path.exists(settings.USER_COURSE_PATH):
                print('****文件不存在,没有学生选择课程****')
                return
            with open(settings.USER_COURSE_PATH, 'rb') as f:
                try:
                    val = pickle.load(f)
                except Exception as  e:
                    logger.error(e, exc_info=True)
                    print('****文件是空的,没有学生选择课程****')
                    return
            for key in val:
                print('****%s选择课程如下****\n    课程名  价格  周期  老师    ' % key)
                for item in val[key] :
                    print('    ', item, item.price, item.period, item.teacher, '    ')
        except Exception as e :
            logger.error(e, exc_info=True)


    def admin_run(self):
        """
        管理员主程序
        :return:
        """
        content = """************请选择功能************
    1.创建课程
    2.创建学生信息
    3.查看所有课程
    4.查看所有学生
    5.查看所有学生的选课情况"""
        info = {'1': self.create_courses, '2': self.create_students, '3': self.look_courses, '4': self.look_students,
                '5': self.look_allstudents_courses}
        try :
            while True:
                print(content)
                index = input('请输入序号(N退出)：')
                if index.upper() == 'N':
                    return
                func = info.get(index)
                if not func:
                    print('输入有误，请重新输入')
                func()
        except Exception as e :
            logger.error(e,exc_info = True)