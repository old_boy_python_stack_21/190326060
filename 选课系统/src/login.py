#!/usr/bin/env python        	
# -*- coding:utf-8 -*-	 
# @Project  : 选课系统
# @Author : Jack Deng
# @Time   :  2019-04-30 20:09
# @File   : login.py

import os
from src.admin import *
from config import settings
from lib.logger import logger


def login():
    """登录选课系统"""
    try :
        while True:
            print('************欢迎来到选课系统************')
            user = input('请输入管理员账号/学号(输入N退出):')
            if user.upper() == 'N':
                return
            pwd = input('请输入密码:')
            if user == settings.ADMIN_NAME and pwd == settings.ADMIN_PWD:
                print('管理员登录成功')
                logger.error('%s登录系统' % settings.ADMIN_NAME)
                admin_obj = Admin()
                admin_obj.admin_run()
            else :
                if not os.path.exists(settings.USER_PATH):
                    with open(settings.USER_PATH,'rb') as f:
                        f.close()
                with open(settings.USER_PATH, 'rb') as f:
                    try :
                        val = pickle.load(f)                     #文件内容为空load会报错 字典 {学号:student_obj}
                    except Exception as e:
                        logger.error(e,exc_info = True)
                        val = {}
                if  not val.get(user):
                    print('学号不存在,请重新登录')
                    continue
                if md5(pwd) == getattr(val[user],'pwd') :
                    print('学生登录成功')
                    student_obj = val[user]
                    logger.error('学生%s:%s登录系统' %(student_obj,student_obj.name))
                    student_obj.student_run()
                else:
                    print('用户名或密码错误,请重新登录')
                    continue
    except Exception as e:
        logger.error(e,exc_info = True)
