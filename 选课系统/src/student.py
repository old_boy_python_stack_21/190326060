#!/usr/bin/env python        	
# -*- coding:utf-8 -*-	 
# @Project  : 选课系统
# @Author : Jack Deng
# @Time   :  2019-05-03 09:41
# @File   : student.py

import  os
from src.user import *
from lib.md5 import md5
from lib.logger import logger
from config import settings


class Student(User):
    def __init__(self, name, id):
        self.name = name
        self.student_id = id
        self.pwd = md5('666666')                                 # 默认密码666666

    def __str__(self):
        return self.name


    def choice_courses(self):
        """
        学生选择课程
        :return:
        """
        try :
            if not os.path.exists(settings.COURSE_PATH) :
                print('请联系管理员,没有课程可供选取')
                return
            try :
                with open(settings.COURSE_PATH, 'ab') as f :
                    f.seek(0)
                    content = pickle.load(f)                 #  { 课程名称 : course_obj}
            except Exception as e :
                logger .error(e, exc_info=True)
                print('请联系管理员,没有课程可供选取')
                return
            for item in content.values():
                print( item ,item.period, item.teacher)
            t = set()
            while True:
                choice = input('请输入您要选择的课程名称(输入N结束选课):')
                if choice.upper() == 'N':
                    break
                if not content.get(choice):
                    print('没有该课程,请重新选择')
                    continue
                t.add(content.get(choice))
            if not t:
                print('您没有选择任何课程')
                return
            try:
                f2 = open(settings.USER_COURSE_PATH, 'ab')
                f2.seek(0)
                val = pickle.load(f2)
            except Exception as e:                      # user_course.txt是空文件,pickle.load(f1)会报错
                logger.error(e , exc_info=True)
                val = {}
            if not val.get(self.student_id):            # 该学生没有选课
                val[self.student_id] = t
            else:
                val[self.student_id].update(t)          # 看能否去重复课程?
            with open(settings.USER_COURSE_PATH, 'ab') as f2 :
                pickle.dump(val, f2, 0)                 # {学号:(选择的course_obj)}
                print('已成功录入选择的课程')
                return
        except Exception as e :
            logger.error( e , exc_info = True)


    def lool_self_courses(self):
        """
        查看所选课程
        :return:
        """
        try :
            print('****%s的选课情况如下****' %self)
            with open(settings.USER_COURSE_PATH, 'ab') as f:
                f.seek(0)
                try :
                    val = pickle.load(f)                          # 字典
                except Exception as e:
                    print('你没有选择课程,无法查看')
                    logger.error(e,exc_info= True)
                    return
            if  not val.get(self.student_id):
                print('%s没有选择任何课程\n' % (self))
                return
            print('    课程名  周期  老师    ')
            for item in val.get(self.student_id):
                print('    ', item , item.period, item.teacher, '    ')
        except Exception as e:
            logger.error(e, exc_info=True)


    def change_pwd(self):
        """
        修改密码
        :return:
        """
        try:
            print('----您现在正要修改密码----')
            f = open(settings.USER_PATH, 'rb')
            val = pickle.load(f)
            f.close()
            old_pwd = input('请输入旧密码(输入N返回上一级)')
            if old_pwd.upper() == 'N' : return
            new_pwd = input('请输入新密码')
            new2_pwd = input('请再次输入新密码')
            if md5(old_pwd) == val[self.student_id].pwd and new_pwd == new2_pwd:
                val[self.student_id].pwd = md5(new_pwd)
                f = open(settings.USER_PATH, 'wb')
                pickle.dump(val, f, 0)
                f.close()
            else :
                print('密码输入错误,返回上一级')
                return
        except Exception as e:
            logger.error(e, exc_info=True)


    def student_run(self):
        content = '1.查看可选课程\n2.选择课程\n3.查看所选课程\n4修改登录密码'
        info = {'1': self.look_courses, '2': self.choice_courses, '3': self.lool_self_courses, '4': self.change_pwd}
        try :
            while True:
                print('****欢迎来到学生选课管理系统****\n%s' % content)
                index = input('请输入序号(N退出)：')
                if index.upper() == 'N':
                    return
                func = info.get(index)
                if not func:
                    print('输入有误，请重新输入')
                func()
        except Exception as e:
            logger.error( e, exc_info=True)