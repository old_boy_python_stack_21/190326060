#!/usr/bin/env python        	
# -*- coding:utf-8 -*-	 
#@Project  : 选课系统 
#@Author : Jack Deng       
#@Time   :  2019-05-03 09:41
#@File   : user.py
import pickle
from config import settings
from lib.logger import logger


class Course:
    def __init__(self, name, price, period, teacher):
        self.name = name
        self.price = price
        self.period = period
        self.teacher = teacher

    def __str__(self):
        return self.name

class User  :
    def look_courses(self):
        """
        查看所有课程
        :return:
        """
        try :
            with open(settings.COURSE_PATH,'rb') as f:
                print('课程名  价格  周期  老师')
                val = pickle.load(f)                                    #  字典
                for item in val.values():
                    print(item.name,item.price,item.period,item.teacher)
        except Exception as  e :
            print('请联系管理员,系统没有课程数据')
            logger.error( e ,exc_info = True)