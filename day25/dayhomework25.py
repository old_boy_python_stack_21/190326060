#!/usr/bin/env python        	
# -*- coding:utf-8 -*-	 
#@Project  : s21day25 
#@Author : Jack Deng       
#@Time   :  2019-04-30 17:37
#@File   : dayhomework25.py

# 一. 正则表达式练习
# 1 匹配整数或者小数（包括正数和负数）
"(-?\d+\.\d+)|(-?\d+)"
"-?\d+(\.\d+)?"

# 2 匹配年月日日期 格式2018-12-6
"[12]\d{3}-(1[0-2]|[1-9])-([1-3]\d|\d)"

# 3匹配qq号
"[1-9]\d{4,10}"

#4 11位的电话号码
"[1][3-9]\d{9}"

# 5 .长度为8-10位的用户密码 ： 包含数字字母下划线
"\w{8,10}"

# 6.匹配验证码：4位数字字母组成的
"[\da-zA-Z]{4}"

# 7.匹配邮箱地址
"^[A-Za-z\d]+([-_.][A-Za-z\d]+)*@([A-Za-z\d]+[-.])+[A-Za-z\d]{2,4}$"

# 8.
t = 1-2*((60-30+(-40/5)*(9-2*5/3+7/3*99/4*2998+10*568/14))-(-4*3)/(16-3*2))
"\(-?\d+[\+\-\*\/]\d+([\+\-\*\/]\d+)*\)"
"""

(-40/5)
(9-2*5/3+7/3*99/4*2998+10*568/14)
(-4*3)
(16-3*2)

"""

# 9. 从类似9-2*5/3+7/3*99/4*2998+10*568/14的表达式中匹配出乘法或除法
#   "(\d+[*/]\d+)([\*/]\d+)*"

"""2*5/3
7/3*99/4*2998
10*568/14
"""

# 10.从类似
# \<a>wahaha\</a>
# \<b>banana\</b>
# \<h1>qqxing\</h1>
# 这样的字符串中，
# 1）匹配出\<a>,\<b>,\<h1>这样的内容
"\\<[a-zA-Z\d]+>"
"""
\<a>
\<b>
\<h1>
"""

# 2)匹配出wahaha，banana，qqxing内容。(思考题)
import  re
ret = re.search("<\w+>(?P<name>\w+)</\w+>","\<a1>wahaha</a1>")
print(ret.group('name'))              # 打印  wahaha  , 同样地,打印banana/qqxing


# 二.
import os
def func(path):
    sum = 0
    set = os.listdir(path)
    for item in set :
        file_path = os.path.join(path, item)
        if os.path.isfile(file_path):
            sum += os.path.getsize(file_path)
        else :
            sum += func(file_path)
    return sum
v = func(r'D:\Python学习')
print(v)


# 在网上看的,堆栈的思想,太帅了.得用这种方法.

import os
basepath = r'D:\Python学习'
dir_list = [basepath]                        # 构造1个列表,暂时只有一个元素,只存放目录
file_size = 0
while dir_list:
    path = dir_list.pop()
    ret = os.listdir(path)
    for item in  ret:
        abs_path = os.path.join(path,item)      # 对于列表的每个元素,构成绝对路径
        if os.path.isfile(abs_path):
            file_size += os.path.getsize(abs_path)
        else:
            dir_list.append(abs_path)       # 如果是文件夹,则加入到只是文件夹的列表中
print(file_size)

# 三 .选课系统