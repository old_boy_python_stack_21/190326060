#!/usr/bin/env python        	
# -*- coding:utf-8 -*-	 
# @Project  : 选课系统
# @Author : Jack Deng
# @Time   :  2019-05-03 09:41
# @File   : student.py
from src.user import *
from lib.md5 import md5
from log.logger import logger
from config import settings


class Student(User):
    def __init__(self, name, id):
        self.name = name
        self.name = id
        self.pwd = md5('666666')  # 默认密码666666

    def __str__(self):
        return self.name

    def choice_courses(self, ):
        """
        学生选择课程
        :return:
        """
        user_course = {}
        with open(settings.COURSE_PATH, 'rb') as f:
            val1 = pickle.load(f)  # 列表
            for item in val1:
                print(item, item.price, item.period, item.teacher)
            t = set()
            while True:
                choice = input('请输入您要选择的课程名称(输入N结束选课):')

                if choice.upper() == 'N':
                    if not t:
                        print('您没有选择任何课程')
                        return
                    with open(settings.USER_COURSE_PATH, 'rb+') as f1:
                        try:
                            val = pickle.load(f1)
                        except Exception as e:
                            print('文件为空')
                            val = {}
                            val[self.name] = t         # 一个学生都没有选课
                            print(val,type(val))
                            print(t,type(t))
                        if not val.get(self.name):     #  该学生没有选课
                            val[self.name] = t
                        else:
                            val[self.name].update(t)   # 看能否去重复课程?
                            f1.seek(0)
                            f1.truncate()
                            pickle.dump(val, f1, 0)
                            print('这里会走嘛?')
                            return
                flag = False
                for item in val1:
                    print(item)
                    if item.name == choice:
                        t.add(item)
                        print(t,type(t))
                        flag = True
                        print(flag)
                if not flag:
                    print('没有该课程,请重新选择')
                    continue

    def lool_self_courses(self):
        """
        查看所选课程
        :return:
        """
        print('****%s的选课情况如下****' %self)
        with open(settings.USER_COURSE_PATH, 'rb') as f:
            try :
                val = pickle.load(f)  # 字典
            except :
                print('文件为空,不能查看')
                return
        if  not val.get(self.name):
            print('%s没有选择任何课程\n\n退出' % (self))
            return
        print('****课程名  价格  周期  老师****')
        for item in val.get(self.name):
            print('****', item, item.price, item.period, item.teacher, '****')

    def change_pwd(self):
        """
        修改密码
        :return:
        """
        try:
            print('----您现在正要修改密码----')
            f = open(settings.USER_PATH, 'rb+')
            val = pickle.load(f)
            for item in val:
                if self.name == item.name:
                    break
            index = val.index(item)
            old_pwd = input('请输入旧密码')
            new_pwd = input('请输入新密码')
            new2_pwd = input('请再次输入新密码')
            if md5(old_pwd) == val[index].pwd and new_pwd == new2_pwd:
                val[index].pwd = md5(new_pwd)
                f.seek(0)
                f.truncate()
                pickle.dump(val, f)
                f.close()
        except Exception as e:
            logger.error(e, exc_info=True)

    def student_run(self):
        content = '1.查看可选课程\n2.选择课程\n3.查看所选课程\n4修改登录密码'
        info = {'1': self.look_courses, '2': self.choice_courses, '3': self.lool_self_courses, '4': self.change_pwd}
        while True:
            print('****欢迎来到学生选课管理系统****\n%s' % content)
            index = input('请输入序号(N退出)：')
            if index.upper() == 'N':
                return
            func = info.get(index)
            if not func:
                print('输入有误，请重新输入')
            func()
