#!/usr/bin/env python        	
# -*- coding:utf-8 -*-	 
# @Project  : 选课系统
# @Author : Jack Deng
# @Time   :  2019-05-03 09:41
# @File   : admin.py

from src.user import *
from src.student import *
from log.logger import logger
from lib.md5 import md5
from config import settings
import os
import sys



class Admin(User):
    def __init__(self):
        self.name = 'admin'
        self.pwd = '2019'

    def create_courses(self):
        """
        创建课程
        :return:
        """
        t = []
        while True:
            name = input('请输入课程名字(输入N结束录入):')
            if name.upper() == "N":
                break
            price = input('请输入课程价格:')
            period = input('请输入课程周期:')
            teacher = input('请输入教学老师:')
            course_obj = Course(name, price, period, teacher)
            t.append(course_obj)
        msg = '成功录入课程'
        with open(settings.COURSE_PATH, 'ab+') as f:
            f.seek(0)
            try:
                val = pickle.load(f)
            except Exception as e:
                pass
                pickle.dump(t, f, 0)
                print(msg)
                return
            if not val:
                val += t
                f.seek(0)
                f.truncate()  # 清空,重写入
                pickle.dump(val, f, 0)


    def create_students(self):
        """
        创建学生名单
        :return:
        """
        student_list = []
        while True:
            name = input('请输入学生姓名(输入N停止录入):')
            if name.upper() == 'N':
                break
            id = input('请输入学生学号:')
            student_list.append(Student(name, id))
        with open(settings.USER_PATH, 'ab+') as f:
            f.seek(0)
            try:
                val = pickle.load(f)
            except Exception as e :
                pickle.dump(student_list, f, 0)
            else:
                val += student_list              # 列表相加
                f.seek(0)
                f.truncate()
                pickle.dump(val, f, 0)  # 清空,重写入


    def look_students(self):
        """
        查看所有学生名单
        :return:
        """
        try :
            with open(settings.USER_PATH, 'rb') as f:
                print('学号     学生姓名 ')
                val = pickle.load(f)                                # 列表
                for item in val:
                    print(item.student_id, item.name)
        except Exception as e:
            logger.error(e,exc_info = True)
            print('数据库没有数据')


    def look_allstudents_courses(self):
        """
        查看学生所选课程
        :return:
        """

        with open(settings.USER_COURSE_PATH, 'rb') as f:
            try:
                val = pickle.load(f)
            except Exception as  e:
                logger.error(e,exc_info = True)
                print('****没有学生选择课程****')
                return
            for key in val:
                print('****%s选择课程如下****\n    课程名  价格  周期  老师    ' % key)
                for item in val[key]:
                    print('    ', item, item.price, item.period, item.teacher, '    ')


    def admin_run(self):
        """
        管理员主程序
        :return:
        """
        content = """************请选择功能************
    1.创建课程
    2.创建学生信息
    3.查看所有课程
    4.查看所有学生
    5.查看所有学生的选课情况"""
        info = {'1': self.create_courses, '2': self.create_students, '3': self.look_courses, '4': self.look_students,
                '5': self.look_allstudents_courses}
        while True:
            print(content)
            index = input('请输入序号(N退出)：')
            if index.upper() == 'N':
                return
            func = info.get(index)
            if not func:
                print('输入有误，请重新输入')
            func()


