#!/usr/bin/env python        	
# -*- coding:utf-8 -*-	 
# @Project  : 选课系统
# @Author : Jack Deng
# @Time   :  2019-04-30 20:09
# @File   : login.py


admin_name = 'admin'
admin_pwd = '2019'

from lib.md5 import md5
from src.user import *
from src.student import *
from src.admin import *
from config import settings


def login():
    """登录选课系统"""
    while True :
        print('************欢迎来到选课系统************')
        user = input('请输入用户名(输入N退出):')
        if user.upper() == 'N':
            exit(0)
        pwd = input('请输入密码:')
        if user == admin_name and pwd == admin_pwd:
            print('管理员登录成功')
            admin_obj = Admin()
            admin_obj.admin_run()
        else:
            try:
                with open(settings.USER_PATH, 'rb') as f:
                    val = pickle.load(f)
                flag = False
                for item in val :
                    if user == item.name and md5(pwd) == item.pwd :
                        print('学生登录成功')
                        flag = True
                        student_obj = item
                        student_obj.student_run()
                        break
                if not flag:
                    print('用户名或密码错误')
                    continue
            except Exception as e:
                print(e)
                print('怎么回事')

