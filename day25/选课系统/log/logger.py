#!/usr/bin/env python        	
# -*- coding:utf-8 -*-	 
# @Project  : 选课系统
# @Author : Jack Deng
# @Time   :  2019-04-30 15:56
# @File   : logger.py

import logging
from logging import handlers
from config import settings

path = settings.LOG_FILE
name = settings.LoggerName
level = settings.Loggerleval

logger = logging.Logger(name,level = level)
fh = handlers.TimedRotatingFileHandler(path, when='d', interval=1, encoding='utf-8')
fmt = logging.Formatter(fmt='%(asctime)s - %(name)s - %(levelname)s -%(module)s: %(message)s',
                        datefmt='%Y-%m-%d %H:%M:%S')
fh.setFormatter(fmt)
logger.addHandler(fh)


# fh = handlers.TimedRotatingFileHandler(path, when='d', interval=1, encoding='utf-8')
# logging.basicConfig( format='%(asctime)s - %(name)s - %(levelname)s -%(module)s:  %(message)s',
#     datefmt='%Y-%m-%d %H:%M:%S %p',
#     handlers=[fh,],
#     level=logging.ERROR,)

